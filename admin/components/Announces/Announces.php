<?php

require_once DOCROOT.LIBS_PATH.'Email.php';

class Announces_Component extends BaseComponent
{

	//put your code here

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		//$this->view->set('url_b_list', $this->getActionUrl("b_list"));

		$this->ActionEditUrl = Utils::getComponentUrl('Announces/announces_edit/');

		$this->view->set('url_announces_list', $this->getActionUrl('announces_list'));
		$this->view->set('url_announces_edit', $this->getActionUrl('announce_edit'));
		$this->view->set('url_announces_delete', $this->getActionUrl('announce_delete'));
		$this->view->set('url_announces_enable', $this->getActionUrl('announce_enable'));
		$this->view->set('url_announces_disable', $this->getActionUrl('announce_disable'));
		$this->view->set('url_announces_details', $this->getActionUrl('announce_details'));
		$this->view->set('url_category_edit', $this->getActionUrl('category_edit'));
		$this->view->set('url_category_delete', $this->getActionUrl('category_delete'));
		$this->view->set('url_addtImage_delete', $this->getActionUrl('addtImage_delete'));

		$this->view->set('settings', $this->ComponentSettings);
		if (isset($this->ComponentSettings['image_baseurl'])) {
			$this->view->set('baseurl', $this->ComponentSettings['image_baseurl']);
		} else {
			$this->view->set('baseurl', '');
		}
	}

	public function announces_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Announces'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$Announces_Table = TABLE_PREFIX.'BusinessAnnounces';
		$filter = '';
		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$announce = $this->model->getAnnounce($id);
				$res = $this->model->deleteAnnounce($id);
			}
			if ($res !== false) {
				$this->view->AddNotice('Announce has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('announces_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'BusinessAnnounces.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'BusinessAnnounces.title', 'peso' => 90),
				array('field' => TABLE_PREFIX.'Announces_Category.category_id', 'peso' => 80),
			);
			$announces = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$announces = $this->model->getList($elements_per_page, $offset, $filter);
		}
		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($announces) == 0) {
			Utils::RedirectTo($this->getActionUrl('announces_list'));
			return;
		}

		foreach ($announces as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddAnnouncement]', $this->getActionUrl('announce_edit')));
		$this->view->BreadCrumb->addDir('[$Announces]', $this->getActionUrl('announces_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('announces', $announces);
		$this->view->setTitle('[$Announces]');
		$this->view->render('announces-list');
	}

	public function announce_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Announces]', $this->getActionUrl('announces_list'));
		if (!is_null($id)) {
			$announce = $this->model->getAnnounce($id);
			HeadHTML::setTitleTag('Edit Announces - '.$announce->title.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditAnnounce]', $this->getActionUrl('announce_edit')."/$id");
		} else {
			$announce = new BusinessAnnounces_Entity();
			HeadHTML::setTitleTag('New Announce'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$NewAnnounce]', $this->getActionUrl('announce_edit'));
		}
		$logged_user = UserAuth::getLoginSession();
		if ($announce === false) {
			$this->AddError("Announce $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['title'] == '') ? $this->AddError('Title is required!') : '';
			($_POST['description'] == '') ? $this->AddError('Content is required!') : '';
			($_POST['id_category'] == '') ? $this->AddError('Category is required!') : '';
			($_POST['publish_date'] == '') ? $this->AddError('Publish Date is required!') : '';
			($_POST['name_a'] == '') ? $this->AddError('Name is required!') : '';
			($_POST['email'] == '') ? $this->AddError('Email is required!') : '';
			($_POST['tel'] == '') ? $this->AddError('Tel is required!') : '';
			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}
			if (count($this->view->getErrors()) == 0) {
				$imagefile = '';
				if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
					$imagefile = time().'_'.Utils::clean($_POST['title']);
					$imagefile = $this->saveImage($_FILES['image']['tmp_name'], $imagefile, 'announce');
				}

				if (!is_null($id) && $announce->category_id != $_POST['id_category']) {
					$old_category = $announce->category_id;
				} else {
					$old_category = null;
				}
				if (isset($_SESSION['user_auth']['id'])) {
					$announce->user_id = $_SESSION['user_auth']['id'];
				} else {
					$announce->user_id = $_SESSION['auth']['id'];
				}
				$announce->views = 0;
				$announce->category_id = $_POST['id_category'];
				$announce->enabled = $_POST['enabled'];
				($imagefile == '') ?: $announce->image = $imagefile;
				$announce->name = $_POST['name_a'];
				$announce->title = $_POST['title'];
				$announce->description = $_POST['description'];
				$announce->email = $_POST['email'];
				$announce->tel = $_POST['tel'];
				$announce->publish_date = $_POST['publish_date'];
				$announce->expiration_date = $_POST['expiration_date'];
				$this->model->startTransaction();
				$inserted_id = $this->model->saveAnnounce($announce);
				if (!is_array($inserted_id)) {
					$announce = $this->model->getAnnounce($inserted_id);
					if ($announce->enabled == 1) {
						if ($announce->notified == 0) {
							//Njoftojme userin
							$sended = $this->notifyAnnounceEnabled($announce);
							if ($sended) {
								$announce->notified = 1;
							}
							$this->model->saveAnnounce($announce);
						}
					}

					//I dergojme email
					$message = $this->newAnnounceBodyMessage($_POST);
					$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, 'noreply@'.CMSSettings::$webdomain, 'New announce has been inserted', $message);
					$msg = $this->newAnnounceBodyMessageUser($_POST);
					Email::sendMail($_POST['email'], CMSSettings::$webdomain, 'noreply@'.CMSSettings::$webdomain, 'Njoftimi juaj ne faqen '.CMSSettings::$webdomain, $msg);

					$this->AddNotice('Announce has been saved successfully.');
					$this->model->commit();
					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Announce', 'insert', 'Announce inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('announce_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Announce', 'update', 'Announce updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('announce_edit')."/$id");
					}
				} else {
					$this->AddError('Saving failed!');
					$this->model->rollback();
				}
			}
		}

		if (isset($_POST['AddImage'])) {
			if ($_FILES['images']['tmp_name'][0] == '') {
				$this->view->AddError('Please upload a file...');
			}
			$file_names = $_FILES['images']['name'];
			foreach ($file_names as $file_name) {
				(!Utils::allowedFileType($file_name)) ? $this->view->AddError('Please upload an image!') : '';
			}
			if (count($this->view->getErrors()) == 0) {
				$failures = 0;
				for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
					$path = $_FILES['images']['name'][$i];
					$ext = pathinfo($path, PATHINFO_EXTENSION);
					$imagefile = time().'_'.Utils::clean($announce->title).'.'.$ext;
					$imagefile = $this->saveImage($_FILES['images']['tmp_name'][$i], $imagefile, 'announce/additional');
					$addImage = new AnnouncesImages_Entity();
					$addImage->id_announce = $id;
					$addImage->image = $imagefile;
					$addImage->size = $_FILES['images']['size'][$i];
					$res = $this->model->saveAnnounceImage($addImage);
					//var_dump($res);exit;
					if (!is_array($res)) {
					} else {
						$failures++;
					}
				}
				if ($failures > 0) {
					$this->AddError('There was '.$failures.' failures during image upload!');
				} else {
					$this->view->AddNotice('Image has been uploaded successfully.');
				}
			}
			Utils::RedirectTo($this->getActionUrl('announce_edit').DS.$id);
		}

		$categories = $this->model->getCategories(50);
		$this->view->set('categories', $categories);
		$this->view->set('announce', $announce);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('announces_list'))));
		$title = (!is_null($id)) ? '[$EditAnnounce]' : '[$NewAnnounce]';
		$this->view->setTitle($title);
		$this->view->render('announce-edit');
	}

	public function announce_delete($id)
	{
		$announce = $this->model->getAnnounce($id);
		$result = $this->model->deleteAnnounce($id);
		//var_dump($result);exit;
		if ($result !== false) {
			$this->LogsManager->registerLog('Announcement', 'delete', "Announcement deleted with id : $id and title : ".$announce->title, $id);
			$this->AddNotice('Announcement deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('announces_list'));
	}

	public function announce_enable($id)
	{
		if (!is_null($id)) {
			$announce = $this->model->getAnnounce($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$announce->enabled = 1;

		if ($announce->notified == 0) {
			//Njoftojme userin
			$sended = $this->notifyAnnounceEnabled($realestate);
			if ($sended) {
				$announce->notified = 1;
			}
		}

		$res = $this->model->saveAnnounce($announce);

		if (!is_array($res)) {
			$this->view->AddNotice('Announcement has been enabled successfully.');
			$this->LogsManager->registerLog('Announcement', 'update', 'Announcement enabled with id : '.$announce->id, $announce->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('announces_list'));
	}

	public function announce_disable($id)
	{
		if (!is_null($id)) {
			$announce = $this->model->getAnnounce($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$announce->enabled = 0;
		$res = $this->model->saveAnnounce($announce);

		if (!is_array($res)) {
			$this->view->AddNotice('Announce has been disabled successfully.');
			$this->LogsManager->registerLog('Announce', 'update', 'Announce disabled with id : '.$announce->id, $announce->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('announces_list'));
	}

	public function announce_details($id)
	{
		Utils::saveCurrentPageUrl();
		$announce = $this->model->getAnnounement($id);
		$this->view->set('announce', $announce);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('announces_list'))));
		$this->view->BreadCrumb->addDir('[$Arnnounces]', $this->getActionUrl('announces_list'));
		$this->view->BreadCrumb->addDir('[$AnnouncesDetails]', $this->getActionUrl('announce_details')."/$id");
		$this->view->setTitle('[$Details]');
		$this->view->render('announce-details');
	}

	public function categories_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Categories'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);

			foreach ($elements as $id) {
				$res = $this->model->deleteCategory($id);
				$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Categories has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::backRedirect($this->getActionUrl('categories_list'));
		}

		$categories = $this->model->getCategories($elements_per_page, $offset, $filter, $sorting);

		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($categories) == 0) {
			Utils::RedirectTo($this->getActionUrl('categories_list'));
			return;
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', $this->getActionUrl('category_edit')));
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));
		if (!is_null($id)) {
			$category = $this->model->getCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$category->category.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditCategory]', $this->getActionUrl('category_edit')."/$id");
		} else {
			$category = new AnnouncesCategory_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddCategory]', $this->getActionUrl('category_edit'));
		}

		if ($category === false) {
			$this->AddError("Category $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['category'] == '') ? $this->AddError('Category Name is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$category->category = $_POST['category'];
				$category->description = $_POST['description'];
				$inserted_id = $this->model->saveCategory($category);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Category has been saved successfully!');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Category', 'insert', 'Category inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Category', 'update', 'Category updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('category', $category);

		$categories = $this->model->getCategories(50);
		$this->view->set('categories', $categories);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('categories_list'))));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id)
	{
		$category = $this->model->getCategory($id);

		$result = $this->model->deleteCategory($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id, name : {$category->category}", $id);
			$this->AddNotice('Category deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('categories_list'));
	}

	public function newAnnounceBodyMessage($data)
	{
		$logged_user = UserAuth::getLoginSession();
		$message = 'At '.date('d/m/Y H:i').' has been inserted a new announce. <br/>';
		$message .= 'Announce '.' : '.$data['title'].'<br/>';
		$message .= 'Description '.' : '.$data['description'].'<br/>';
		$message .= 'Tel '.' : '.$data['tel'].'<br/>';
		//$message .= 'User ' . ' : ' . $logged_user['email'] . '<br/>';
		$message .= 'Email'.' : '.$data['email'].'<br/>';
		$message .= 'Tel'.' : '.$data['tel'].'<br/>';
		return $message;
	}

	public function notifyAnnounceEnabled($announce)
	{

		//print_r($realestate);exit;
		//generate token
		$token = Utils::GenerateRandomPassword(10);
		//keep genereting until the token is unique
		$user_md = Loader::getModel('Users');
		$user = $user_md->getUser($announce->id_user);
		//I Dergojme email userit me passwordin e resetuar.
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH.'Announces'.DS.'views'.DS);
		$message->setLangPath(COMPONENTS_PATH.'Announces'.DS.'lang'.DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->set('announce', $announce);
		$message->set('user', $user);
		$message->renderTemplate(false);
		$txt = $message->render('mails/email_announce_online', true);
		$sended = Email::sendMail($announce->email, CMSSettings::$webdomain, 'noreply@'.CMSSettings::$webdomain, $announce->title.$this->view->getTerm('is_online'), $txt);
		if ($sended) {
			$this->view->AddNotice('The client has been notified.');
			return true;
		} else {
			$this->view->AddError('There was a problem notifing the client.');
			return false;
		}
	}

	public function newAnnounceBodyMessageUser($data)
	{
		$logged_user = UserAuth::getLoginSession();
		$message = 'I nderuar klient , njoftimi juaj eshte shtuar me sukses ne faqen '.CMSSettings::$webdomain.' me te dhenat e meposhteme : <br/>';
		$message .= 'Njoftimi '.' : '.$data['title'].'<br/>';
		$message .= 'Pershkrimi '.' : '.$data['description'].'<br/>';
		//$message .= 'User ' . ' : ' . $logged_user['email'] . '<br/>';
		$message .= 'Email'.' : '.$data['email'].'<br/>';
		$message .= 'Tel'.' : '.$data['tel'].'<br/>';
		return $message;
	}

	public function announcesGraber()
	{
		require DOCROOT.XLIBS_PATH.'simple_html_dom.php';

		ini_set('max_execution_time', 10000);

		for ($i = 1; $i <= 30; $i++) {
			$html = file_get_html('http://www.njoftime.com/forumdisplay.php?15-Turiz%EBm/page'.$i);
			//$html = file_get_html('http://www.njoftime.com/forumdisplay.php?14-ofroj-vende-pune/page' . $i . '&pp=30&daysprune=100&field3[0]=Tirane');
			$links = $html->find('.title');

			foreach ($links as $link) {
				$link = 'http://www.njoftime.com/'.$link->href;
				$html = file_get_html($link);
				$title = $html->find('.posttitle', 0)->plaintext;
				$content = $html->find('.postcontent', 0)->plaintext;

				$phone = $this->getPhone($content);
				$email = $this->getEmails($content);

				$this->model->startTransaction();
				$announce = new BusinessAnnounces_Entity();
				$announce->name = 'Udhetime';
				$announce->title = $title;
				$announce->description = $content;
				$announce->publish_date = date('Y-m-d H:i:s');
				$announce->expiration_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d  H:i:s', time()).' + 180 day'));
				$announce->user_id = 118;
				if ($email != null) {
					$announce->email = $email;
				} else {
					$announce->email = '';
				}
				if ($phone != null) {
					$announce->tel = $phone;
				} else {
					$announce->tel = '';
				}
				$announce->image = '';
				$announce->category_id = 19;
				$this->model->saveAnnounce($announce);
				$this->model->commit();
			}
		}
	}

	public function addtImage_delete($id)
	{
		$image = $this->model->getAnnounceImage($id);

		$res = $this->model->deleteAnnounceImage($id);
		if ($res !== false) {
			$this->LogsManager->registerLog('AnnounceImage', 'delete', "Image deleted with id : $id", $id);
			$this->AddNotice('Image has been deleted successfully.');
			$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'announce'.DS.'additional'.DS.$image->image;
			unlink(DOCROOT.$filename);
			$this->deleteImage($image->image, 'announce/additional');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('announce_edit').DS.$image->id_announce);
	}

	public function getEmails($text = '')
	{
		//$text = 'QENDRA EUROPIANE E STUDIMIT, KERKON TE PUNESOJE PART TIME NJE SPECIALIST PER KURSE AUTOKADI (MASHKULL) ME KETO KARAKTERISTIKA: -TE KETE EKSPERIENCE NE MESIMDHENIE NE PROGRAMET E AUTOKADIT. -TE JETE I DISPONUESHEM CDO DITE NGA E HENA NE TE SHTUNE PARADITE. TE INTERESUARIT TE DERG CV ME FOTO NE ADRESEN: E-MAIL:info.esc2008@gmail.com OSE TELEFONONI PA HEZITIM NE NR. CEL.068 20 41 009 CEL.069 72 94 402';
		$pattern = '/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}/i';
		preg_match_all($pattern, $text, $matches);
		$email = $matches[0][0];
		if ($email != '') {
			return $email;
		} else {
			return null;
		}
	}

	public function getPhone($text = '')
	{
		//$text = 'Përshkrimi LUFRA shpk është një kompani Shqiptare, e krijuar rreth 23 vite më parë, me qendër në Lushnjë dhe produkte prezente në të gjithë Shqipërinë. Ajo është sot kompania më e madhe e bulmetit në vend duke ofruar mbi 40 produkte të ndryshme për konsumatorin. Aktualisht jemi duke kërkuar të rekrutojmë një MANAXHER SHITJESH, i/e cila do të jetë përgjegjës për: 1. Mirëmbajtjen e rrjetit ekzistues të klientëve dhe identifikimin e klientëve të rinj 2. Informimin e klientëve mbi kompaninë, produktet dhe shërbimet e saj, përfitimet, bonuset, skemat e ndryshme dhe çdo lloj komunikimi në lidhje me mbledhjen e informacionit të tregut; 3. Ofrimin e shërbimit të shkëlqyer ndaj klientëve; 4. Ndjekjen e proceseve të punës sic përcaktohet në udhëzimet e shitjeve dhe në përputhje me synimet e caktuara nga kompania; 5. Bashkëpunimin me sektorë të tjerë brenda kompanisë për të mundësuar kryerjen e sukseshme të procesit të shitjes sic përcaktohet në rregulloret dhe politikat e shitjeve; 6. Kryerjen aktivitetet e shitjes me qëllim përmbushjen e nevojave të klientëve arritjen e objektivave të paracaktuara të shitjes. 7. Kryerjen e shitjes së produkteve dhe mbledh edhe informacion nga klientët lidhur me nevojat, shërbimet, performancën dhe imazhin e kompanisë. 8. Kujdesin ndaj pajisjeve dhe mjedisit të punës, duke krijuar një atmosferë të ngrohtë në komunikimin me klientët. 9. Ndjekjen e kërkesave të pazgjidhura të klientëve duke i adresuar ato tek sektorët përkatës në kompani; 10. Njohjen, dokumentimin dhe lajmërimin e kolegëve dhe eprorëve rreth ndodhive dhe cështjeve problematike të ngritura nga klientët ose nga vetë ai; 11. Implementimin e politikave dhe procedurave në fuqi të kompanisë; 12. Rekomandimin e përmirësimeve të proceseve, duke identifikuar problemet dhe rreziqeve; 13. Ofrimin e trajnimit për punonjësit e rinj. Kualifikimet / Kërkesat Kandidati duhet të plotësojë kriteret e mëposhtme: • Diplomë Universitare; • Eksperiencë e mëparshme në fushën e shitjeve përbën avantazh; • Njohuri të mira të gjuhës angleze; • Aftësi të mira në kompjuter dhe aftësi për të mësuar programet/sistemet aplikative të shitjes; • Aftësi të shkëlqyera komunikimi; person energjik dhe i motivuar; • Të jetë i përgjegjshëm dhe i kujdesshëm ndaj detajeve; • Paraqitje profesionale e të jetë i/e orientuar drejt shërbimit të klientit; • I/e gatshëm për të punuar me turne; • Ambicioz/e, i/e besueshëm dhe i aftë për të ndërmarrë iniciativa; • Aftësi për të punuar individualisht si dhe në grup; • Aftësi për të trajnuar punonjësit e rinj rreth punës aktuale. Shënime Aplikoni për të qenë pjesë e Skuadrës së LUFRA shpk, duke dërguar CV-në tuaj së bashku me një përgjigje të argumentuar të pyetjes: “PSE DUHET TË JENI JU MANAXHERI I RI I SHITJEVE në LUFRA shpk?” në adresën e e-mail-it hr@lufra.al . Vetëm kandidatët e përzgjedhur për intervistë do të kontaktohen. LUFRA ju siguron që aplikimi juaj do të trajtohet me konfidencialitet të plotë dhe të dhënat tuaja janë të sigurta. Kontakt: 0696069800 ';
		$pattern = '/[0-9]{3}[\-][0-9]{7}|[0-9]{3}[\s][0-9]{6}|[0-9]{3}[\s][0-9]{3}[\s][0-9]{4}|[0-9]{10}|[0-9]{9}|[0-9]{3}[\-][0-9]{3}[\-][0-9]{4}/';
		preg_match_all($pattern, $text, $matches);
		$phone = $matches[0][0];
		if ($phone != '') {
			return $phone;
		} else {
			return null;
		}
	}

	private function saveImageToDb($file, $filename, $path, $todelete = false)
	{
		require_once DOCROOT.ENTITIES_PATH.'Business/TempImage.php';
		$tempImage = new TempImage_Entity();
		$tempImage->image = file_get_contents($file);
		$tempImage->filename = $filename;
		$tempImage->path = $path;
		$tempImage->delete_action = ($todelete) ? '1' : '0';
		$inserted_temp_image = $this->model->saveTempImage($tempImage);
		return $inserted_temp_image;
	}

	public function syncImagesFromDb()
	{
		$images = $this->model->getTempImages(-1);
		$target = DOCROOT.MEDIA_ROOT;

		foreach ($images as $image) {
			$filename = $target.$image->path.DS.$image->filename;
			$filename = str_replace('//', '/', $filename);

			if ($image->delete_action == '1') {
				$file_to_delete = $target.rtrim($image->path, DS).DS.$image->filename;
				if (file_exists($file_to_delete)) {
					if (unlink($file_to_delete)) {
						$this->model->deleteTempImage($image->id);
					}
				} else {
					$this->model->deleteTempImage($image->id);
				}
			} else {
				if (!is_dir(dirname($filename))) {
					mkdir(dirname($filename), 0755, true) or die('Errore durante la creazione della folder!');
				}
				$result = file_put_contents($filename, $image->image);
				if ($result) {
					$this->model->deleteTempImage($image->id);
				}
			}
		}
	}

	private function saveImage($tmpfile, $newfile, $folder)
	{
		$path = DS.rtrim(ltrim($folder, DS), DS).DS;
		$newfile = rand(0, 999).DS.$newfile;
		if (isset($this->ComponentSettings['image_baseurl'])) {
			$this->saveImageToDb($tmpfile, $newfile, $path);
			$ch = curl_init('http://www.neshqiperi.al/com/Announces/syncImagesFromDb');
			curl_exec($ch);
			curl_close($ch);
		} else {
			$target = DOCROOT.MEDIA_ROOT.$path;
			if (!is_dir(dirname($target.$newfile))) {
				mkdir(dirname($target.$newfile), 0755, true) or die('Errore durante la creazione della folder!');
			}
			move_uploaded_file($tmpfile, $target.$newfile);
		}
		return $newfile;
	}

	private function deleteImage($filename, $folder)
	{
		if (isset($this->ComponentSettings['image_baseurl'])) {
			$this->saveImageToDb(null, $filename, $folder, true);
			$ch = curl_init('http://www.neshqiperi.al/com/Announces/syncImagesFromDb');
			curl_exec($ch);
			curl_close($ch);
		} else {
			$target = DOCROOT.MEDIA_ROOT.$folder.DS;
			if (file_exists($target.$filename)) {
				unlink($target.$filename);
			}
		}
	}
}
