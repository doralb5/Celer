<?php
$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $announce->category_id;
$publish_date = isset($_POST['publish_date']) ? $_POST['publish_date'] : (!is_null($announce->publish_date) ? $announce->publish_date : date('Y-m-d H:i:s'));
$expire_date = isset($_POST['expiration_date']) ? $_POST['expiration_date'] : $announce->expiration_date;
$enabled = isset($_POST['enabled']) ? $_POST['enabled'] : $announce->enabled;
$title = isset($_POST['title']) ? $_POST['title'] : $announce->title;
$name = isset($_POST['name_a']) ? $_POST['name_a'] : $announce->name;
$description = isset($_POST['description']) ? $_POST['description'] : $announce->description;
$email = isset($_POST['email']) ? $_POST['email'] : $announce->email;
$tel = isset($_POST['tel']) ? $_POST['tel'] : $announce->tel;
?>

<form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
      novalidate="" enctype="multipart/form-data">
    <div class="col-md-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$GeneralInformation]</h4>
            </div>
            
            <div class="panel-body panel-form">
                
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Title]</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $title ?>"
                               name="title" placeholder="[$Title]">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                    <div class="col-md-6 col-sm-6">
                        <input type="file" name="image">
                        <?php if ($announce->image != '') {
	?>
                            <p>
                                <img
                                    src="<?= Utils::genThumbnailUrl('announce/'.$announce->image, 70, 70, array('zc' => 1), true, $baseurl) ?>"
                                    width="100px" alt="Image"/></p>
                            <?php
} ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Category]</label>

                    <div class="col-md-8 col-sm-8">
                        <select class="form-control" name="id_category">
                            <?php foreach ($categories as $categ) {
		?>
                                <option
                                    value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
                                <?php
	} ?>
                        </select>
                    </div>
                </div>
            </div>
            
        </div>
    

    <!-- begin panel -->
    <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$Content]</h4>
        </div>
        <div class="panel-body panel-form">
            <textarea class="ckeditor" id="editor1" name="description"><?= $description ?></textarea>
        </div>
    </div>    </div>

    <!-- end panel -->


    <div class="col-md-4">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$PublishSettings]</h4>
            </div>
            <div class="panel-body panel-form">
                <div class="form-group">
                    <label class="col-md-4 control-label">[$Enabled]</label>

                    <div class="col-md-8">
                        <select name="enabled" class="form-control">
                            <option value="1" <?= ($enabled == '1') ? 'selected' : '' ?>>[$yes_option]
                            </option>
                            <option value="0" <?= ($enabled == '0') ? 'selected' : '' ?>>[$no_option]
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$PublishDate]</label>

                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" name="publish_date" id="datetimepicker4"
                               placeholder="[$SelectDate]" value="<?= $publish_date ?>" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$ExpirationDate]</label>

                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" name="expiration_date" id="datetimepicker5"
                               placeholder="[$SelectDate]" value="<?= $expire_date ?>">
                    </div>
                </div>
            </div>
        </div>
        <!-- end panel -->

        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$Options]</h4>
            </div>
            <div class="panel-body panel-form">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Name] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $name ?>"
                               name="name_a" placeholder="[$Name]">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Email] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $email ?>"
                               name="email" placeholder="[$Email]">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Tel] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $tel ?>"
                               name="tel" placeholder="[$Tel]">
                    </div>
                </div>

            </div>
        </div>
        <!-- end panel -->
    </div>


    <div class="col-md-12">
        <p class="text-center">
            <button type="submit" name="save" class="btn btn-info">
                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
            </button>
        </p>
    </div>
    <p>&nbsp;</p>
</form>

<?php if (!is_null($announce->id)) {
		?>

    <div class="col-md-8">

        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$AdditionalImages]</h4>
            </div>
            <div class="panel-body">

                <p>&nbsp;</p>
                <?php
				include(VIEWS_PATH.'TableList_class.php');
		$table = new TableListView('Articles', 'list');
		foreach ($announce->images as &$row) {
			$row->rows_buttons = array(
//                        new Button('<i class="fa fa-eye"></i>', $url_addtImage_show . '/additional/' . $row->image, 'xs', '', '[$Show]', '_blank'),
//                        new Button('<i class="fa fa-download"></i>', $url_addtImage_download . '/' . $row->id, 'xs', '', '[$Download]')
					);
			$row->row_imagePath = ($baseurl == '') ? substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.DS.'announce'.DS.'additional': $baseurl.'/announce/additional';
			//$row->row_imagePath = substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')) . MEDIA_ROOT . DS . 'announce' .  DS . "additional";
		}

		$table->setElements($announce->images);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
					new TableList_Field('id', '[$Id]', 'int', 'left'),
					new TableList_Field('image', '[$Filename]', 'string', 'left'),
					new TableList_Field('image', '[$Image]', 'image', 'left'),
				));
		$table->setUrl_delete($url_addtImage_delete);
		$table->setUrl_delete_params(array('id_announce' => $announce->id));
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->render(); ?>


                <div class="col-md-12">
                    <form id="fileupload" method="POST" enctype="multipart/form-data">
                        <div class="form-group col-md-9">
                            <label>[$SelectFile]</label>

                            <div class="box">
                                <input type="file" name="images[]" id="file-7" class="inputfile inputfile-6"
                                       data-multiple-caption="{count} files selected" multiple/>
                                <label for="file-7"><span></span><strong>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                             viewBox="0 0 20 17">
                                            <path
                                                d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                        </svg>
                                        [$Choose]&hellip;</strong></label>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <br>
                            <button class="btn btn-info" name="AddImage" type="submit"><i class="fa fa-plus"></i>&nbsp;&nbsp;[$AddFile]
                            </button>

                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
<?php
	} ?>

<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>

<script>
<?php foreach (CMSSettings::$available_langs as $lang) {
	?>

        CKEDITOR.replace('editor_<?= $lang ?>', {
            imageBrowser_listUrl: "<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).LIBS_PATH ?>JsonDirImages.php?media_path=<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).MEDIA_ROOT ?>&docroot=<?= rtrim(DOCROOT, '/') ?>",
                    height: 232
                });

<?php
} ?>

</script>

<script>
    $(document).ready(function () {
        $('#datetimepicker4').datetimepicker();
        $('#datetimepicker5').datetimepicker();
    });
</script>
<style>
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
    }

    .inputfile:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .box {
        padding: 0px;
    }
</style>
<script>
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
                labelVal = label.innerHTML;
        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();
            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });</script>