<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Announces');
$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';

foreach ($announces as &$announce) {
	if ($announce->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_announces_disable.'/'.$announce->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_announces_enable.'/'.$announce->id, 'xs', 'btn-success', '[$Enable]');
	}
	$announce->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_announces_edit.'/'.$announce->id, 'xs', '', '[$Edit]')
	);

	$imgtype = ($image_baseurl == '') ? 'image' : 'extimg';
	$announce->image_base_url = $image_baseurl;
	$announce->row_imagePath = 'announce/';
}

$table->setElements($announces);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);
$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('category_id', '[$Category]', 'string', 'left'),
	new TableList_Field('publish_date', '[$PublishDate]', 'date', 'left'),
	new TableList_Field('expiration_date', '[$ExpireDate]', 'date', 'left'),
	new TableList_Field('image', '[$Imge]', $imgtype, 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
));
$table->setUrl_action($url_announces_list);
$table->setUrl_delete($url_announces_delete);
$table->setImagesPath('announce');
$table->render();
