<?php

class Articles_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Articles/article_edit/');

		$this->view->set('url_articles_list', $this->getActionUrl('articles_list'));
		$this->view->set('url_article_edit', $this->getActionUrl('article_edit'));
		$this->view->set('url_article_delete', $this->getActionUrl('article_delete'));
		$this->view->set('url_article_enable', $this->getActionUrl('article_enable'));
		$this->view->set('url_article_disable', $this->getActionUrl('article_disable'));
		$this->view->set('url_article_details', $this->getActionUrl('article_details'));

		$this->view->set('url_categories_list', $this->getActionUrl('categories_list'));
		$this->view->set('url_category_edit', $this->getActionUrl('category_edit'));
		$this->view->set('url_category_delete', $this->getActionUrl('category_delete'));

		$this->view->set('url_sections_list', $this->getActionUrl('sections_list'));
		$this->view->set('url_section_edit', $this->getActionUrl('section_edit'));
		$this->view->set('url_section_delete', $this->getActionUrl('section_delete'));

		$this->view->set('url_roles_list', $this->getActionUrl('roles_list'));
		$this->view->set('url_role_edit', $this->getActionUrl('role_edit'));
		$this->view->set('url_role_delete', $this->getActionUrl('role_delete'));

		//Additional Images
		$this->view->set('url_addtImage_delete', $this->getActionUrl('addtImage_delete'));
		$this->view->set('url_addtImage_download', $this->getActionUrl('addtImage_download'));
		$this->view->set('url_addtImage_show', substr(WEBROOT, 0, -6).MEDIA_ROOT.'articles');

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function articles_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Articles'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$ArticleDetail_Table = TABLE_PREFIX.'ArticleDetail';
		$filter = Entity::getFieldName('ArticleDetail', 'lang')." = '".FCRequest::getLang()."' OR cms_ArticleDetail.lang IS NULL";

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$article = $this->model->getArticle($id);
				$additionalImages = $this->model->getArticleImages(100, 0, "id_article = $id");
				$res = $this->model->deleteArticle($id);

				if ($res !== false) {
					$target = DOCROOT.MEDIA_ROOT.'articles'.DS.$article->id_category;
					if ($article->image != '' && file_exists($target.DS.$article->image)) {
						unlink($target.DS.$article->image);
					}
					foreach ($additionalImages as $image) {
						if (file_exists($target.DS.'additional'.DS.$image->image)) {
							unlink($target.DS.'additional'.DS.$image->image);
						}
					}
					$this->LogsManager->registerLog('Article', 'delete', 'Article deleted with id : '.$id, $id);
				}
			}
			if ($res !== false) {
				$this->view->AddNotice('Articles has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('articles_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'Article.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'ArticleDetail.title', 'peso' => 90),
				array('field' => TABLE_PREFIX.'ArticleDetail.subtitle', 'peso' => 90),
				array('field' => TABLE_PREFIX.'ArticleCategory.category', 'peso' => 80),
			);
			$articles = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$articles = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($articles) == 0) {
			Utils::RedirectTo($this->getActionUrl('articles_list'));
			return;
		}

		foreach ($articles as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddArticle]', $this->getActionUrl('article_edit')));

		$this->view->BreadCrumb->addDir('[$Articles]', $this->getActionUrl('articles_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('articles', $articles);
		$this->view->setTitle('[$Articles]');
		$this->view->render('articles-list');
	}

	public function article_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Articles]', $this->getActionUrl('articles_list'));
		if (!is_null($id)) {
			$article = $this->model->getArticle($id);
			HeadHTML::setTitleTag($this->view->getTerm('EditArticle').' - '.$article->title.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditArticle]', $this->getActionUrl('article_edit')."/$id");
		} else {
			$article = new Article_Entity();
			HeadHTML::setTitleTag($this->view->getTerm('NewArticle').' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$NewArticle]', $this->getActionUrl('article_edit'));
		}

		$logged_user = UserAuth::getLoginSession();

		if ($article === false) {
			$this->AddError("Article $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['title_'.CMSSettings::$default_lang] == '') ? $this->AddError('Title is required!') : '';
			($_POST['content_'.CMSSettings::$default_lang] == '') ? $this->AddError('Content is required!') : '';
			($_POST['id_category'] == '') ? $this->AddError('Category is required!') : '';
			($_POST['publish_date'] == '') ? $this->AddError('Publish Date is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $article->image;
				} else {
					$_POST['image'] = $article->image;
					$old_image = null;
				}

				if (!is_null($id) && $article->id_category != $_POST['id_category']) {
					$old_category = $article->id_category;
				} else {
					$old_category = null;
				}

				$this->model->startTransaction();

				$article->id_category = $_POST['id_category'];
				$article->id_section = $_POST['id_section'];
				$article->author = $_POST['author'];
				$article->id_user = $logged_user['id'];
				$article->image = $_POST['image'];
				$article->enabled = $_POST['enabled'];
				$article->featured = $_POST['featured'];
				$article->featured_until = (@$_POST['featured_until'] && $_POST['featured'] == '1') ? $_POST['featured_until'] : null;
				$article->publish_date = $_POST['publish_date'];
				$article->expire_date = $_POST['expire_date'];
				$article->show_title = $_POST['show_title'];
				$article->show_subtitle = $_POST['show_subtitle'];
				$article->show_date = $_POST['show_date'];
				$article->show_author = $_POST['show_author'];
				$article->show_share = $_POST['show_share'];
				$article->show_comments = $_POST['show_comments'];
				$inserted_id = $this->model->saveArticle($article);

				if (!is_array($inserted_id)) {
					foreach (CMSSettings::$available_langs as $lang) {
						if (is_null($id)) {
							$detail = new ArticleDetail_Entity();
							$detail->id_article = $inserted_id;
						} else {
							$det = $this->model->getArticleDetails(1, 0, "id_article = {$id} AND lang = '{$lang}'");
							if (count($det)) {
								$detail = $det[0];
							} else {
								$detail = new ArticleDetail_Entity();
							}
							$detail->id_article = $id;
						}

						$is_empty = ($_POST['title_'.$lang] == '' && $_POST['subtitle_'.$lang] == '' && $_POST['content_'.$lang] == '') ? true : false;

						$detail->title = (!$is_empty) ? $_POST['title_'.$lang] : $_POST['title_'.CMSSettings::$default_lang];
						$detail->subtitle = (!$is_empty) ? $_POST['subtitle_'.$lang] : $_POST['subtitle_'.CMSSettings::$default_lang];
						$detail->content = (!$is_empty) ? $_POST['content_'.$lang] : $_POST['content_'.CMSSettings::$default_lang];
						$detail->lang = $lang;

						$result = $this->model->saveArticleDetail($detail);

						if (is_array($result)) {
							$this->AddError('Problem creating article multilanguage details!');
							$this->model->rollback();
							if (is_bool($inserted_id)) {
								Utils::RedirectTo($this->getActionUrl('article_edit')."/$id");
							} else {
								Utils::RedirectTo($this->getActionUrl('article_edit'));
							}
						}
					}

					$this->AddNotice('Article has been saved successfully.');
					$this->model->commit();

					if (!is_null($id) && !is_null($old_category)) {
						$target = DOCROOT.MEDIA_ROOT.DS.'articles'.DS;
						Utils::createDirectory($target.$article->id_category);
						$this->moveAdditionalImages($id, $old_category, $article->id_category);
						if (!is_null($old_image)) {
							rename($target.$old_category.DS.$old_image, $target.$article->id_category.DS.$old_image);
						} elseif ($article->image != '') {
							rename($target.$old_category.DS.$article->image, $target.$article->id_category.DS.$article->image);
						}
					}

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'articles'.DS.$article->id_category;
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);

						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Article', 'insert', 'Article inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('article_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Article', 'update', 'Article updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('article_edit')."/$id");
					}
				} else {
					$this->AddError('Saving failed!');
					$this->model->rollback();
				}
			}
		} elseif (isset($_POST['AddImage'])) {
			if ($_FILES['images']['tmp_name'][0] == '') {
				$this->view->AddError('Please upload a file...');
			}

			if (count($this->view->getErrors()) == 0) {
				$failures = 0;
				for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
					$tmp_name = time().'_'.$_FILES['images']['name'][$i];

					$addImage = new ArticleImage_Entity();

					$addImage->id_article = $id;
					$addImage->image = $tmp_name;
					$addImage->size = $_FILES['images']['size'][$i];
					$res = $this->model->saveArticleImage($addImage);

					if (!is_array($res)) {
						$target = DOCROOT.MEDIA_ROOT.'articles'.DS.$article->id_category.DS.'additional';
						Utils::createDirectory($target);
						$filename = $target.DS.$tmp_name;
						move_uploaded_file($_FILES['images']['tmp_name'][$i], $filename);
					} else {
						$failures++;
					}
				}
				if ($failures > 0) {
					$this->AddError('There was '.$failures.' failures during image upload!');
				} else {
					$this->view->AddNotice('Image has been uploaded successfully.');
				}
			}
			Utils::RedirectTo($this->getActionUrl('article_edit')."/$id");
		} elseif (isset($_POST['AddTag'])) {
			if ($_POST['tag'] != '') {
				if ($article->tags != '') {
					$allTags = explode(';', $article->tags);
					array_push($allTags, $_POST['tag']);
				} else {
					$allTags = array($_POST['tag']);
				}
				$article->tags = implode(';', $allTags);

				$res = $this->model->saveArticle($article);
				if (!is_array($res)) {
					$this->AddNotice('Tag added successfully.');
				} else {
					$this->AddError('Something went wrong!');
				}
			}
			Utils::RedirectTo($this->getActionUrl('article_edit')."/$id");
		}

		$categories = $this->model->getCategories(-1);
		$this->view->set('categories', $categories);
		$sections = $this->model->getSections(-1);
		$this->view->set('sections', $sections);

		if (!is_null($id)) {
			$additionalImages = $this->model->getArticleImages(-1, 0, "id_article = $id");
			$this->view->set('additionalImages', $additionalImages);
		}

		foreach (CMSSettings::$available_langs as $lang) {
			$detail = $this->model->getArticleDetails(1, 0, "id_article = {$id} AND lang = '{$lang}'");
			$article->{$lang} = count($detail) ? $detail[0] : null;
		}

		//var_dump($article->featured);
		$this->view->set('article', $article);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('articles_list'))));
		$title = (!is_null($id)) ? '[$EditArticle]' : '[$NewArticle]';
		$this->view->setTitle($title);
		if (isset($_GET['view'])) {
			$this->view->render($_GET['view']);
		} else {
			$this->view->render('article-edit');
		}
	}

	//Private
	private function moveAdditionalImages($id_article, $old_category, $new_category)
	{
		$additionalImages = $this->model->getArticleImages(-1, 0, "id_article = $id_article");
		$target = DOCROOT.MEDIA_ROOT.DS.'articles'.DS;
		Utils::createDirectory($target.$new_category.DS.'additional');
		foreach ($additionalImages as $img) {
			rename($target.$old_category.DS.'additional'.DS.$img->image, $target.$new_category.DS.'additional'.DS.$img->image);
		}
	}

	public function article_details($id)
	{
		Utils::saveCurrentPageUrl();
		$article = $this->model->getArticle($id);
		$this->view->set('article', $article);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('articles_list'))));
		$this->view->BreadCrumb->addDir('[$Articles]', $this->getActionUrl('articles_list'));
		$this->view->BreadCrumb->addDir('[$ArticleDetails]', $this->getActionUrl('article_details')."/$id");
		$this->view->setTitle('[$Details]');
		$this->view->render('article-details');
	}

	public function article_delete($id)
	{
		$article = $this->model->getArticle($id);
		$additionalImages = $this->model->getArticleImages(50, 0, "id_article = $id");

		$result = $this->model->deleteArticle($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'articles'.DS.$article->id_category;
			if ($article->image != '' && file_exists($target.DS.$article->image)) {
				unlink($target.DS.$article->image);
			}

			foreach ($additionalImages as $image) {
				if (file_exists($target.DS.'additional'.DS.$image->image)) {
					unlink($target.DS.'additional'.DS.$image->image);
				}
			}

			$this->LogsManager->registerLog('Article', 'delete', "Article deleted with id : $id and title : ".$article->title, $id);
			$this->AddNotice('Article deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('articles_list'));
	}

	public function article_enable($id)
	{
		if (!is_null($id)) {
			$article = $this->model->getArticle($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$article->enabled = 1;
		$res = $this->model->saveArticle($article);

		if (!is_array($res)) {
			$this->view->AddNotice('Article has been enabled successfully.');
			$this->LogsManager->registerLog('Article', 'update', 'Article enabled with id : '.$article->id, $article->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('articles_list'));
	}

	public function article_disable($id)
	{
		if (!is_null($id)) {
			$article = $this->model->getArticle($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$article->enabled = 0;
		$res = $this->model->saveArticle($article);

		if (!is_array($res)) {
			$this->view->AddNotice('Article has been disabled successfully.');
			$this->LogsManager->registerLog('Article', 'update', 'Article disabled with id : '.$article->id, $article->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('articles_list'));
	}

	public function categories_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Categories'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteCategory($id);
				$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Categories has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::backRedirect($this->getActionUrl('categories_list'));
		}

		$categories = $this->model->getCategories($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($categories) == 0) {
			Utils::RedirectTo($this->getActionUrl('categories_list'));
			return;
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', $this->getActionUrl('category_edit')));
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));
		if (!is_null($id)) {
			$category = $this->model->getCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$category->category.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditCategory]', $this->getActionUrl('category_edit')."/$id");
		} else {
			$category = new ArticleCategory_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddCategory]', $this->getActionUrl('category_edit'));
		}

		if ($category === false) {
			$this->AddError("Category $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['category'] == '') ? $this->AddError('Category Name is required!') : '';
			($_POST['id_section'] == '') ? $this->AddError('Section is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $category->image;
				} else {
					$_POST['image'] = $category->image;
				}

				$category->category = $_POST['category'];
				$category->description = $_POST['description'];
				$category->id_section = $_POST['id_section'];
				$category->image = $_POST['image'];
				$category->parent_id = $_POST['parent_id'];
				$category->view = $_POST['view'];
				$inserted_id = $this->model->saveCategory($category);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Category has been saved successfully!');

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'articles'.DS.'categories';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Category', 'insert', 'Category inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Category', 'update', 'Category updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!<br/>'.$inserted_id[2]);
				}
			}
		}
		$this->view->set('category', $category);

		$categories = $this->model->getCategories(-1);
		$this->view->set('categories', $categories);

		$sections = $this->model->getSections(-1);
		$this->view->set('sections', $sections);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('categories_list'))));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id)
	{
		$category = $this->model->getCategory($id);
		$result = $this->model->deleteCategory($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'articles'.DS.'categories';
			if ($category->image != '' && file_exists($target.$category->image)) {
				unlink($target.$category->image);
			}
			$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id, name : {$category->category}", $id);
			$this->AddNotice('Category deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('categories_list'));
	}

	public function sections_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Sections'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteSection($id);
				$this->LogsManager->registerLog('ArticleSection', 'delete', "Section deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Sections has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('sections_list'));
		}

		$sections = $this->model->getSections($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($sections) == 0) {
			Utils::RedirectTo($this->getActionUrl('sections_list'));
			return;
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddSection]', $this->getActionUrl('section_edit')));
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('sections_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('sections', $sections);
		$this->view->setTitle('[$Sections]');
		$this->view->render('sections-list');
	}

	public function section_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Sections]', $this->getActionUrl('sections_list'));
		if (!is_null($id)) {
			$section = $this->model->getSection($id);
			HeadHTML::setTitleTag('Edit Section - '.$section->section.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditSection]', $this->getActionUrl('section_edit')."/$id");
		} else {
			$section = new ArticleSection_Entity();
			HeadHTML::setTitleTag('New Section'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddSection]', $this->getActionUrl('section_edit'));
		}

		if ($section === false) {
			$this->AddError("Section $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['section'] == '') ? $this->AddError('Section Name is required!') : '';
			if (count($this->view->getErrors()) == 0) {
				$section->section = $_POST['section'];
				$section->view = $_POST['view'];

				$inserted_id = $this->model->saveSection($section);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Section has been saved successfully!');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('ArticleSection', 'insert', 'Section inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('section_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('ArticleSection', 'update', 'Section updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('section_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('section', $section);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('sections_list'))));
		$title = (!is_null($id)) ? '[$EditSection]' : '[$NewSection]';
		$this->view->setTitle($title);
		$this->view->render('section-edit');
	}

	public function section_delete($id)
	{
		$section = $this->model->getSection($id);
		$result = $this->model->deleteSection($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('ArticleSection', 'delete', "Section deleted with id : $id, name : {$section->section}", $id);
			$this->AddNotice('Section deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('sections_list'));
	}

	public function addtImage_delete($id)
	{
		if (!isset($_GET['id_article'])) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$image = $this->model->getArticleImage($id);

		$res = $this->model->deleteArticleImage($id);
		if ($res) {
			$this->LogsManager->registerLog('ArticleImage', 'delete', "Image deleted with id : $id", $id);
			$this->AddNotice('Image has been deleted successfully.');
			$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'articles'.DS.$image->id_category.DS.'additional'.DS.$image->image;
			unlink(DOCROOT.$filename);
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('article_edit')."/{$image->id_article}");
	}

	public function addtImage_download($id)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('articles_list'));
		}

		$image = $this->model->getArticleImage($id);
		$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'articles'.DS.$image->id_category.DS.'additional'.DS.$image->image;

		//Shkarkimi i Files
		header('Content-Type: '.mime_content_type(DOCROOT.$filename));
		header('Content-Disposition: attachment; filename="'.$image->image.'""');
		readfile(DOCROOT.$filename);
	}

	public function deleteArticleTag($tag)
	{
		if (!isset($_GET['id_article'])) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
			return;
		}

		$article = $this->model->getArticle($_GET['id_article']);
		$allTags = explode(';', $article->tags);

		if (($key = array_search($tag, $allTags)) !== false) {
			unset($allTags[$key]);
		}

		$article->tags = implode(';', $allTags);
		$res = $this->model->saveArticle($article);
		if (!is_array($res)) {
			$this->AddNotice('Tag deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('article_edit')."/{$article->id}");
	}

	public function roles_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Articles Roles'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteRole($id);
				$this->LogsManager->registerLog('ArticleRole', 'delete', "Article role deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Roles has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('roles_list'));
		}

		$roles = $this->model->getRoles($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($roles) == 0) {
			Utils::RedirectTo($this->getActionUrl('roles_list'));
			return;
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddRole]', $this->getActionUrl('role_edit')));
		$this->view->BreadCrumb->addDir('[$Roles]', $this->getActionUrl('roles_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('roles', $roles);
		$this->view->setTitle('[$Roles]');
		$this->view->render('roles-list');
	}

	public function role_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Roles]', $this->getActionUrl('roles_list'));
		if (!is_null($id)) {
			$role = $this->model->getRole($id);
			HeadHTML::setTitleTag('Edit Role - '.$role->role.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditRole]', $this->getActionUrl('role_edit')."/$id");
		} else {
			$role = new ArticleRole_Entity();
			HeadHTML::setTitleTag('New Role'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddRole]', $this->getActionUrl('role_edit'));
		}

		if ($role === false) {
			$this->AddError("Role $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['role'] == '') ? $this->AddError('Role is required!') : '';

			if (is_null($id)) {
				($_POST['id_user'] == '') ? $this->AddError('User is required!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				$role->role = $_POST['role'];

				if (is_null($id)) {
					$role->id_user = $_POST['id_user'];
				}

				$inserted_id = $this->model->saveRole($role);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Role has been saved successfully!');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('ArticleRole', 'insert', 'Article role inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('role_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('ArticleRole', 'update', 'Article role updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('role_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}

		$users_md = Loader::getModel('Users');
		$users = $users_md->getList(50, 0, "admin = '1' AND enabled = '1' AND deleted = '0'");
		$this->view->set('users', $users);

		$this->view->set('role', $role);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('roles_list'))));
		$title = (!is_null($id)) ? '[$EditRole]' : '[$NewRole]';
		$this->view->setTitle($title);
		$this->view->render('role-edit');
	}

	public function role_delete($id)
	{
		$role = $this->model->getRole($id);
		$result = $this->model->deleteRole($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('ArticleRole', 'delete', "Section deleted with id : $id, role : {$role->role} and user : {$role->user_fullname}", $id);
			$this->AddNotice('Role deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('roles_list'));
	}

	public function news_import()
	{

		//old Db
		$olddb = new Database('mysql', 'vm-web0', 'bluehat_mondial', 'bluehat_mondial', 'kkDlPQws0rI');

		$sql = 'SELECT id ,  title, subtitle,  content ,  publish_date, creation_date, visible AS enabled , category_id AS id_category ,tags as tags FROM news ';

		$result = $olddb->select($sql);

		foreach ($result as $res) {
			$item = new Article_Entity();

			$item->id_category = $res['id_category'];
			$item->publish_date = $res['publish_date'];
			$item->title = $res['title'];
			$item->content = $res['content'];
			$item->lang = 'it';
			$item->enabled = $res['enabled'];
			$item->tags = $res['tags'];
//
			//            $filename = Utils::url_slug($item->title) . ".jpg";
			//            $item->image = $item->id_category . '/' . $filename;
			$r = $this->model->saveArticle($item);
			if (!is_array($r)) {
				echo '---STATUS OK---</br></br>';
			//                $filedir = DOCROOT . MEDIA_ROOT . "articles/" . $item->id_category . "/";
//                Utils::createDirectory($filedir);
//                file_put_contents($filedir . $filename, base64_decode($res['image']));
			} else {
				echo '</br>---STATUS KO---</br>';
			}
		}
	}
}
