<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-5">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i
                            class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i
                            class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$ArticleDetails] - <span class=""><?= $article->title ?></span></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered personalinfo">
                        <tbody>
                        <tr>
                            <td>[$Id]</td>
                            <td><b><?= $article->id ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Title]</td>
                            <td><b><?= $article->title ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Subtitle]</td>
                            <td><b><?= $article->subtitle ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Image]</td>
                            <td><?php if ($article->image != '') {
	?>
                                    <img
                                        src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'articles'.DS.$article->id_category.DS.$article->image ?>"
                                        alt="Image" width="100px"/>
                                <?php
} else {
		?>
                                    No Image
                                <?php
	} ?>
                            </td>
                        </tr>
                        <tr>
                            <td>[$Category]</td>
                            <td><b><?= $article->category ?></b></td>
                        </tr>
                        <tr>
                            <td>[$User]</td>
                            <td><b><?= $article->user_fullname ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Author]</td>
                            <td><b><?= $article->author ?></b></td>
                        </tr>
                        <tr>
                            <td>[$CreationDate]</td>
                            <td><?= date('d/m/Y H:i', strtotime($article->creation_date)) ?></td>
                        </tr>
                        <tr>
                            <td>[$PublishDate]</td>
                            <td><?= date('d/m/Y H:i', strtotime($article->publish_date)) ?></td>
                        </tr>
                        <tr>
                            <td>[$ExpirationDate]</td>
                            <td><?= (is_null($article->expire_date)) ? 'No Expiration' : date('d/m/Y H:i', strtotime($article->creation_date)) ?></td>
                        </tr>
                        <tr>
                            <td>[$Enabled]</td>
                            <td><?= ($article->enabled == '1') ? '<i class="fa fa-check-square" style="color:green"></i>' : '<i class="fa fa-square-o" style="color:red"></i>' ?></td>
                        </tr>
                        <tr>
                            <td>[$Featured]</td>
                            <td><?= ($article->featured == '1') ? '<i class="fa fa-check-square" style="color:green"></i>' : '<i class="fa fa-square-o" style="color:red"></i>' ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <p>[$Content]</p>

                <div class="col-md-12">
                    <?= $article->content ?>
                </div>
                <p>&nbsp;</p>

                <div class="col-md-12 text-right">
                    <a class="btn btn-danger" href="<?= $url_article_delete."/{$article->id}" ?>"><i
                            class="fa fa-minus-circle"></i>&nbsp;&nbsp;[$Delete]</a>
                </div>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>
</div>