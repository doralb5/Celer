<?php
$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $article->id_category;
$id_section = isset($_POST['id_section']) ? $_POST['id_section'] : $article->id_section;
$id_user = isset($_POST['id_user']) ? $_POST['id_user'] : $article->id_user;
$author = isset($_POST['author']) ? $_POST['author'] : $article->author;
$publish_date = isset($_POST['publish_date']) ? $_POST['publish_date'] : (!is_null($article->publish_date) ? $article->publish_date : date('Y-m-d H:i:s'));
$expire_date = isset($_POST['expire_date']) ? $_POST['expire_date'] : $article->expire_date;
$expire_date = isset($_POST['expire_date']) ? $_POST['expire_date'] : $article->expire_date;
$featured = isset($_POST['featured']) ? $_POST['featured'] : $article->featured;
$enabled = isset($_POST['enabled']) ? $_POST['enabled'] : $article->enabled;
$show_title = isset($_POST['show_title']) ? $_POST['show_title'] : $article->show_title;
$show_subtitle = isset($_POST['show_subtitle']) ? $_POST['show_subtitle'] : $article->show_subtitle;
$show_date = isset($_POST['show_date']) ? $_POST['show_date'] : $article->show_date;
$show_author = isset($_POST['show_author']) ? $_POST['show_author'] : $article->show_author;
$show_share = isset($_POST['show_share']) ? $_POST['show_share'] : $article->show_share;
$show_comments = isset($_POST['show_comments']) ? $_POST['show_comments'] : $article->show_comments;

foreach (CMSSettings::$available_langs as $lang) {
	$title_{$lang} = isset($_POST['title_'.$lang]) ? $_POST['title_'.$lang] : ((!is_null($article->{$lang})) ? $article->{$lang}->title : '');
	$subtitle_{$lang} = isset($_POST['subtitle_'.$lang]) ? $_POST['subtitle_'.$lang] : ((!is_null($article->{$lang})) ? $article->{$lang}->subtitle : '');
	$content_{$lang} = isset($_POST['content_'.$lang]) ? $_POST['content_'.$lang] : ((!is_null($article->{$lang})) ? $article->{$lang}->content : '');
}
?>



<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<?php
HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css');
HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js');
HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js');
HeadHTML::AddStylesheet($this->template_path.'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
HeadHTML::AddJS($this->template_path.'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');

HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<script>
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker();
    });
</script>


<style>
    .bootstrap-tagsinput { width: 100%; }
</style>



<form method="post" class="form-horizontal" enctype="multipart/form-data">
    <div class="col-md-8">
        
        <div class="panel-group">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">[$GeneralInformation]</h4>
                </div>
                <div class="panel-body">
                    
                    <ul  class="nav nav-tabs">
                        <?php foreach (CMSSettings::$available_langs as $lang) {
	?>
                        <li class="<?= ($lang == CMSSettings::$default_lang) ? 'active' : '' ?>"><a
                                href="#tabcontent_<?= $lang ?>" data-toggle="tab"><?= strtoupper($lang) ?></a></li>
                        <?php
} ?>
                    </ul>

                    <div class="tab-content clearfix">
                        <?php foreach (CMSSettings::$available_langs as $lang) {
		?>
                        
                            <div class="tab-pane fade <?= ($lang == CMSSettings::$default_lang) ? 'active in' : '' ?>" id="tabcontent_<?= $lang ?>">
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-2 required">[$Title] <span>*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="title_<?= $lang ?>" placeholder="[$Title]" value="<?= $title_{$lang} ?>" <?= ($lang == CMSSettings::$default_lang) ? 'required' : ''?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">[$Subtitle]</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="subtitle_<?= $lang ?>" placeholder="[$Subtitle]" value="<?= $subtitle_{$lang} ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <textarea class="editor" id="editor_<?= $lang ?>" name="content_<?= $lang ?>"><?= $content_{$lang} ?></textarea>
                                        <script>
                                            CKEDITOR.replace('editor_<?=$lang?>', {
                                        //        imageBrowser_listUrl: "<?=substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).LIBS_PATH?>JsonDirImages.php?media_path=<?=substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).MEDIA_ROOT?>&docroot=<?=  rtrim(DOCROOT, '/')?>",
                                                filebrowserBrowseUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                                                filebrowserImageBrowseUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
                                                height: 300
                                            });
                                        </script>
                                    </div>
                                </div>
                                
                            </div>
                        
                        <?php
	}?>
                    </div>
                    
                </div>

            </div>
            
            <!-- Images Panel -->

            <style>
                #image_preview img { margin: 4px; border: 1px solid #dfdfdf; }
                .auto-clear .col-md-1:nth-child(12n+1){clear:left;}
                .auto-clear .col-md-2:nth-child(6n+1){clear:left;}
                .auto-clear .col-md-3:nth-child(4n+1){clear:left;}
                .auto-clear .col-md-4:nth-child(3n+1){clear:left;}
                .auto-clear .col-md-6:nth-child(odd){clear:left;}
            </style>
            <div class="panel panel-inverse">
             <div class="panel-heading">
                 <div class="panel-heading-btn">
                     <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                 </div>
                 <h4 class="panel-title">[$Images]</h4>
             </div>
             <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-1 control-label">[$Image]</label>
                    <div class="col-md-6 ">
                        
                        <?php
						HeadHTML::AddJS($this->template_path.'assets/jQuery-File-Upload/js/vendor/jquery.ui.widget.js');
						HeadHTML::AddJS($this->template_path.'assets/jQuery-File-Upload/js/jquery.iframe-transport.js');
						HeadHTML::AddJS($this->template_path.'assets/jQuery-File-Upload/js/jquery.fileupload.js');
						?>
                        <input id="fileupload" type="file" name="files[]" data-url="server/php/" multiple>
                        <script>
                        $(function () {
                            $('#fileupload').fileupload({
                                dataType: 'json',
                                done: function (e, data) {
                                    $.each(data.result.files, function (index, file) {
                                        //$('#image_preview').append(file.name);
                                        $('<p/>').text(file.name).appendTo(document.body);
                                    });
                                }
                            });
                        });
                        </script>
                        
                        <div class="form-group">
                            <div id="image_preview" class=" auto-clear"></div>
                        </div>
                    </div>
                </div>
             </div>
            </div>

            <!-- end Images Panel -->
            
        </div>
        
    </div>
    <div class="col-md-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">[$PublishSettings]</h4>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-5 control-label">[$Enabled]</label>

                    <div class="col-md-7">
                        <input <?= ($enabled) ? 'checked' : '' ?> data-toggle="toggle" data-on="Enabled" data-off="Disabled" type="checkbox" name="enabled">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5 control-label">[$Category]</label>
                    <div class="col-md-7 ">
                        <select class="form-control" name="id_category">
                            <?php foreach ($categories as $categ) {
							?>
                                <option
                                    value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
                            <?php
						} ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5 control-label">[$Section]</label> 
                    <div class="col-md-7">
                        <select class="form-control" name="id_section">
                            <?php foreach ($sections as $section) {
							?>
                                <option
                                    value="<?= $section->id ?>" <?= ($section->id == $id_section) ? 'selected' : '' ?>><?= $section->section ?></option>
                            <?php
						} ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-5" for="author">[$Author]</label>

                    <div class="col-md-7">
                        <input class="form-control" type="text" value="<?= $author ?>"
                            name="author" placeholder="[$Author]">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-5">[$PublishDate]</label>

                    <div class="col-md-7">
                        <input type="text" class="form-control" name="publish_date" id="datetimepicker1"
                               placeholder="[$SelectDate]" value="<?= $publish_date ?>" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-5">[$ExpirationDate]</label>

                    <div class="col-md-7">
                        <input type="text" class="form-control" name="expire_date" id="datetimepicker2"
                               placeholder="[$SelectDate]" value="<?= $expire_date ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5 control-label">[$Featured]</label>
                    <div class="col-md-7">           
                        <input <?= ($featured) ? 'checked' : '' ?> data-toggle="toggle" data-on="[$yes_option]" data-off="[$no_option]" type="checkbox" name="featured">                        
                    </div>
                </div>
            </div>
        </div>
        
        
        
        <!-- Tags Panel -->
        <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">[$Tags]</h4>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-12">[$AddTags]</label>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <select multiple data-role="tagsinput" name="tags">
                            <option value="test">test</option>
                            <?php
							if ($article->tags != '') {
								$tags = explode(';', $article->tags);
								foreach ($tags as $tag) {
									?>
                                    <option value="<?= $tag ?>"><?= $tag ?></option>
                                <?php
								}
							} ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Tags Panel -->
        
        
        <!-- Options Panel -->
            <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">[$Options]</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group" >
                        <label class="col-md-5 control-label" > [$post_layout] </label>
                        <div class="col-md-7" >
                            <select name="post_layout" class="form-control">
                                <option value="0">[$default]</option>

                            </select>
                        </div>
                    </div>    

                    <div class="form-group">
                        <label class="col-md-5 control-label">[$ShowTitle]</label>

                        <div class="col-md-7">
                            <select name="show_title" class="form-control">
                                <option value="0" <?= ($show_title == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_title == 'yes') ? 'selected' : '' ?>>
                                    [$yes_option]
                                </option>
                                <option value="no" <?= ($show_title == 'no') ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">[$ShowSubtitle]</label>

                        <div class="col-md-7">
                            <select name="show_subtitle" class="form-control">
                                <option value="0" <?= ($show_subtitle == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_subtitle == 'yes') ? 'selected' : '' ?>>
                                    [$yes_option]
                                </option>
                                <option value="no" <?= ($show_subtitle == 'no') ? 'selected' : '' ?>>
                                    [$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">[$ShowDate]</label>

                        <div class="col-md-7">
                            <select name="show_date" class="form-control">
                                <option value="0" <?= ($show_date == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_date == 'yes') ? 'selected' : '' ?>>[$yes_option]
                                </option>
                                <option value="no" <?= ($show_date == 'no') ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">[$ShowAuthor]</label>

                        <div class="col-md-7">
                            <select name="show_author" class="form-control">
                                <option value="0" <?= ($show_author == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_author == 'yes') ? 'selected' : '' ?>>
                                    [$yes_option]
                                </option>
                                <option value="no" <?= ($show_author == 'no') ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">[$ShowShare]</label>

                        <div class="col-md-7">
                            <select name="show_share" class="form-control">
                                <option value="0" <?= ($show_share == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_share == 'yes') ? 'selected' : '' ?>>
                                    [$yes_option]
                                </option>
                                <option value="no" <?= ($show_share == 'no') ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">[$ShowComments]</label>

                        <div class="col-md-7">
                            <select name="show_comments" class="form-control">
                                <option value="0" <?= ($show_comments == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_comments == 'yes') ? 'selected' : '' ?>>
                                    [$yes_option]
                                </option>
                                <option value="no" <?= ($show_comments == 'no') ? 'selected' : '' ?>>
                                    [$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end Options Panel -->
        
        
        
    </div>
    
    
    <div class="col-md-12">
        <p class="text-center">
            <button type="submit" name="save" class="btn btn-info">
                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
            </button>
        </p>
    </div>
    <p>&nbsp;</p>
    
</form>