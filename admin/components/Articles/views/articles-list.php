<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Articles');

foreach ($articles as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_article_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_article_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_article_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
	);
	$row->row_imagePath = 'articles'.DS.$row->id_category;
}
$table->setElements($articles);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('title', '[$Title]', 'text', 'left'),
	new TableList_Field('image', '[$Image]', 'image', 'left'),
	new TableList_Field('category', '[$Category]', 'string', 'left'),
	new TableList_Field('publish_date', '[$PublishDate]', 'date', 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
));
$table->setUrl_action($url_articles_list);
$table->setUrl_delete($url_article_delete);
$table->render();
