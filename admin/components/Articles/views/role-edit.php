<?php
$role_name = isset($_POST['role']) ? $_POST['role'] : $role->role;
$id_user = isset($_POST['id_user']) ? $_POST['id_user'] : $role->id_user;
?>

<div class="col-md-8">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$ArticleRole]</h4>
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
                  novalidate="" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$User]</label>
                    <div class="col-md-8 col-sm-8">
                        <select name="id_user" <?= (!is_null($role->id) ? 'disabled' : '') ?> class="form-control">
                            <?php foreach ($users as $user) {
	?>
                                <option
                                    value="<?= $user->id ?>" <?= ($id_user == $user->id) ? 'selected' : '' ?>><?= $user->fullname ?></option>
                            <?php
} ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Role]</label>
                    <div class="col-md-8 col-sm-8">
                        <select name="role" class="form-control">
                            <option value="author" <?= ($role_name == 'author') ? 'selected' : '' ?>>[$Author]
                            </option>
                            <option value="editor" <?= ($role_name == 'editor') ? 'selected' : '' ?>>[$Editor]
                            </option>
                            <option value="collaborator" <?= ($role_name == 'collaborator') ? 'selected' : '' ?>>
                                [$Collaborator]
                            </option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12 form-group">
                    <p>&nbsp;</p>
                    <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
