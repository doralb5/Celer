<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Articles');

foreach ($roles as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_role_edit.'/'.$row->id, 'xs', '', '[$Edit]')
	);
}
$table->setElements($roles);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('role', '[$Role]', 'string', 'left'),
	new TableList_Field('user_fullname', '[$User]', 'string', 'left'),
));
$table->setUrl_action($url_roles_list);
$table->setUrl_delete($url_role_delete);

$table->renderTopBar(false);
$table->render();
