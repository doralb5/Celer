<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Articles');

foreach ($sections as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_section_edit.'/'.$row->id, 'xs', '', '[$Edit]')
	);
}
$table->setElements($sections);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('section', '[$Name]', 'string', 'left'),
	new TableList_Field('view', '[$View]', 'string', 'left'),
));
$table->setUrl_action($url_sections_list);
$table->setUrl_delete($url_section_delete);

$table->multipleDeletion(false);
$table->renderTopBar(false);
$table->render();
