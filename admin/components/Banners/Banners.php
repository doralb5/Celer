<?php

class Banners_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Banners/banner_edit/');

		$this->view->set('url_banners_list', $this->getActionUrl('banners_list'));
		$this->view->set('url_banner_edit', $this->getActionUrl('banner_edit'));
		$this->view->set('url_banner_delete', $this->getActionUrl('banner_delete'));
		$this->view->set('url_banner_enable', $this->getActionUrl('banner_enable'));
		$this->view->set('url_banner_disable', $this->getActionUrl('banner_disable'));

		$this->view->set('url_categories_list', $this->getActionUrl('categories_list'));
		$this->view->set('url_category_edit', $this->getActionUrl('category_edit'));
		$this->view->set('url_category_delete', $this->getActionUrl('category_delete'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function banners_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Banners'.' | '.CMSSettings::$website_title);
		$elements_per_page = 15;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$banner = $this->model->getBanner($id);
				$res = $this->model->deleteBanner($id);
				if ($res !== false) {
					$target = DOCROOT.MEDIA_ROOT.'banners';
					if ($banner->filename != '' && file_exists($target.DS.$banner->filename)) {
						unlink($target.DS.$banner->filename);
					}
					$this->LogsManager->registerLog('Banner', 'delete', 'Banner deleted with id : '.$id, $id);
				}
			}
			if ($res !== false) {
				$this->view->AddNotice('Banners has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('banners_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'Banner.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'Banner.name', 'peso' => 90),
				array('field' => TABLE_PREFIX.'BannerCategory.category', 'peso' => 80),
			);
			$banners = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$banners = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($banners) == 0) {
			Utils::RedirectTo($this->getActionUrl('banners_list'));
			return;
		}

		foreach ($banners as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddBanner]', $this->getActionUrl('banner_edit')));

		$this->view->BreadCrumb->addDir('[$Banners]', $this->getActionUrl('banners_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('banners', $banners);
		$this->view->setTitle('[$Banners]');
		$this->view->render('banners-list');
	}

	public function banner_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Banners]', $this->getActionUrl('banners_list'));
		if (!is_null($id)) {
			$banner = $this->model->getBanner($id);
			HeadHTML::setTitleTag('Edit Banner - '.$banner->name.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditBanner]', $this->getActionUrl('banner_edit')."/$id");
		} else {
			$banner = new Banner_Entity();
			HeadHTML::setTitleTag('New Banner'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddBanner]', $this->getActionUrl('banner_edit'));
		}

		if ($banner === false) {
			$this->AddError("Banner $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['name'] == '') ? $this->AddError('Banner Name is required!') : '';

			if (is_null($id)) {
				if (!isset($_FILES['image']['tmp_name']) || strlen($_FILES['image']['tmp_name']) == 0) {
					(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
				}
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['filename'] = time().'_'.$_FILES['image']['name'];
					$old_image = $banner->filename;
				} else {
					$_POST['filename'] = $banner->filename;
				}

				$banner->name = $_POST['name'];
				$banner->id_category = $_POST['id_category'];
				$banner->filename = $_POST['filename'];
				$banner->publish_date = ($_POST['publish_date'] != '') ? $_POST['publish_date'] : date('Y-m-d H:i:s');
				$banner->expire_date = ($_POST['expire_date'] != '') ? $_POST['expire_date'] : null;
				$banner->target_url = $_POST['target_url'];
				$banner->text1 = $_POST['text1'];
				$banner->text2 = $_POST['text2'];
				$banner->text3 = $_POST['text3'];
				$inserted_id = $this->model->saveBanner($banner);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Banner has been saved successfully.');

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'banners';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['filename'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Banner', 'insert', 'Banner inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('banner_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Banner', 'update', 'Banner updated with id : '.$id, $id);
					}
				} else {
					$this->AddError('Saving failed!');
				}
			}
		}

		$bCategories = $this->model->getCategories(50);
		$this->view->set('bCategories', $bCategories);

		$this->view->set('banner', $banner);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('banners_list'))));
		$title = (!is_null($id)) ? '[$EditBanner]' : '[$NewBanner]';
		$this->view->setTitle($title);
		$this->view->render('banner-edit');
	}

	public function banner_delete($id)
	{
		$banner = $this->model->getBanner($id);
		$result = $this->model->deleteBanner($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'banners';
			if ($banner->filename != '' && file_exists($target.DS.$banner->filename)) {
				unlink($target.DS.$banner->filename);
			}

			$this->LogsManager->registerLog('Banner', 'delete', "Banner deleted with id : $id and name : ".$banner->name, $id);
			$this->AddNotice('Banner deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('banners_list'));
	}

	public function banner_enable($id)
	{
		if (!is_null($id)) {
			$banner = $this->model->getBanner($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$banner->enabled = 1;
		$res = $this->model->saveBanner($banner);

		if (!is_array($res)) {
			$this->view->AddNotice('Banner has been enabled successfully.');
			$this->LogsManager->registerLog('Banner', 'update', 'Banner enabled with id : '.$banner->id, $banner->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('banners_list'));
	}

	public function banner_disable($id)
	{
		if (!is_null($id)) {
			$banner = $this->model->getBanner($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$banner->enabled = 0;
		$res = $this->model->saveBanner($banner);

		if (!is_array($res)) {
			$this->view->AddNotice('Banner has been disabled successfully.');
			$this->LogsManager->registerLog('Banner', 'update', 'Banner disabled with id : '.$banner->id, $banner->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('banners_list'));
	}

	public function categories_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Categories'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteCategory($id);
				$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Categories has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('categories_list'));
		}

		$categories = $this->model->getCategories($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($categories) == 0) {
			Utils::RedirectTo($this->getActionUrl('categories_list'));
			return;
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', $this->getActionUrl('category_edit')));
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));
		if (!is_null($id)) {
			$category = $this->model->getCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$category->category.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditCategory]', $this->getActionUrl('category_edit')."/$id");
		} else {
			$category = new BannerCategory_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddCategory]', $this->getActionUrl('category_edit'));
		}

		if ($category === false) {
			$this->AddError("Category $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['category'] == '') ? $this->AddError('Category Name is required!') : '';
			if (count($this->view->getErrors()) == 0) {
				$category->category = $_POST['category'];
				$inserted_id = $this->model->saveCategory($category);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Category has been saved successfully!');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Category', 'insert', 'Category inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Category', 'update', 'Category updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('category', $category);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('categories_list'))));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id)
	{
		$category = $this->model->getCategory($id);
		$result = $this->model->deleteCategory($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id, name : {$category->category}", $id);
			$this->AddNotice('Category deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('categories_list'));
	}
}
