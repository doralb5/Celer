<?php
$name = isset($_POST['name']) ? $_POST['name'] : $banner->name;
$text1 = isset($_POST['text1']) ? $_POST['text1'] : $banner->text1;
$text2 = isset($_POST['text2']) ? $_POST['text2'] : $banner->text2;
$text3 = isset($_POST['text3']) ? $_POST['text3'] : $banner->text3;
$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $banner->id_category;
$target_url = isset($_POST['target_url']) ? $_POST['target_url'] : $banner->target_url;
$publish_date = isset($_POST['publish_date']) ? $_POST['publish_date'] : (!is_null($banner->publish_date) ? $banner->publish_date : date('Y-m-d H:i:s'));
$expire_date = isset($_POST['expire_date']) ? $_POST['expire_date'] : (!is_null($banner->expire_date) ? $banner->expire_date : date('Y-m-d H:i:s', strtotime('+1 years')));

?>
<div class="col-md-6">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$Banner]</h4>
        </div>
        <div class="panel-body panel-form">

            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true"
                  name="demo-form"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Name] * :</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $name ?>"
                               name="name" placeholder="[$Name]" data-parsley-required="true"
                               data-parsley-id="6524" required>
                        <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                    <div class="col-md-6 col-sm-6">
                        <input type="file" name="image">
                        <?php if ($banner->filename != '') {
	?>
                            <p>
                                <img
                                    src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'banners/'.$banner->filename ?>"
                                    alt="Image" width="100px"/></p>
                        <?php
} ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Category]</label>

                    <div class="col-md-6 col-sm-6">
                        <select class="form-control" name="id_category">
                            <?php foreach ($bCategories as $categ) {
		?>
                                <option
                                    value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
                            <?php
	} ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$TargetUrl]</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="url" value="<?= $target_url ?>"
                               name="target_url" placeholder="[$TargetUrl]">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" >[$Text 1]</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= addslashes(htmlentities($text1)); ?>"
                               name="text1" placeholder="Text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" >[$Text 2]</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= addslashes(htmlentities($text2)); ?>"
                               name="text2" placeholder="Text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" >[$Text 3]</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= addslashes(htmlentities($text3)); ?>"
                               name="text3" placeholder="Text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$PublishDate]</label>

                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" name="publish_date" id="datetimepicker1"
                               placeholder="[$SelectDate]" value="<?= $publish_date ?>" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$ExpirationDate]</label>

                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" name="expire_date" id="datetimepicker2"
                               placeholder="[$SelectDate]" value="<?= $expire_date ?>">
                    </div>
                </div>
                <p>&nbsp;</p>

                <div class="col-md-12 text-right">
                    <button type="submit" name="save" class="btn btn-info">
                        <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                </div>
                <p>&nbsp;</p>
            </form>
        </div>
    </div>
</div>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/js/form-plugins.demo.min.js'); ?>
<script>
    $('#datetimepicker1').datetimepicker();
    $('#datetimepicker2').datetimepicker();
</script>
