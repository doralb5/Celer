<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Banners');

foreach ($categories as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_category_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
	);
}
$table->setElements($categories);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('category', '[$Category]', 'string', 'left')
));

$table->setUrl_action($url_categories_list);
$table->setUrl_delete($url_category_delete);
$table->renderTopBar(false);
$table->render();
