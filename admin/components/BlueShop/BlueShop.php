<?php

class BlueShop_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		define('BS_DB_HOST', $this->ComponentSettings['DB_HOST']);
		define('BS_DB_NAME', $this->ComponentSettings['DB_NAME']);
		define('BS_DB_USER', $this->ComponentSettings['DB_USER']);
		define('BS_DB_PASS', $this->ComponentSettings['DB_PASS']);
		parent::__construct($name, $package);
	}

	public function listProducts()
	{
		$elements_per_page = 20;
		$page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = 'visibile = 1 ';

		$products = $this->model->getProducts($filter, $elements_per_page, $offset, $sorting);
		$totalElements = $this->model->getLastCounter();
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('products', $products);
		HeadHTML::setTitleTag('Products'.' | '.CMSSettings::$website_title);
		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		//BreadCrumb
		//WebPage::$breadcrumb->addDir('[$Products]', $this->getActionUrl('products_list'));
		//HeadHTML::setTitleTag('Products' . ' | ' . CMSSettings::$website_title);
		$this->view->render('product-list');
	}

	public function editProduct($id = null)
	{
	}
}
