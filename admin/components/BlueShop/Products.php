<?php

class Products_Component extends BaseComponent
{
	public function __construct($name = 'Products', $package = 'BlueShop')
	{
		parent::__construct($name, $package);
		$this->model->initialization($this->ComponentSettings);
	}

	public function listProducts()
	{
		if (isset($_GET['extract'])) {
			$elements_per_page = -1;
			$page = 1;
		} else {
			$elements_per_page = 20;
			$page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
		}
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = 'visibile = 1 ';

		$products = $this->model->getProducts($filter, $elements_per_page, $offset, $sorting);
		$totalElements = $this->model->getLastCounter();

		foreach ($products as &$p) {
			$p->listino1 = number_format($p->listino1, 2, ',', '.');
		}

		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('elements', $products);
		HeadHTML::setTitleTag('Products'.' | '.CMSSettings::$website_title);
		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		//BreadCrumb
		//WebPage::$breadcrumb->addDir('[$Products]', $this->getActionUrl('products_list'));
		//HeadHTML::setTitleTag('Products' . ' | ' . CMSSettings::$website_title);
		if (isset($_GET['extract'])) {
			//TODO ...............

			$this->view->render('csv');
		} else {
			$this->view->addButton(new Button('Extract', $this->getActionUrl('listProduct').'?extract=1'));
			$this->view->render('product-list');
		}
	}

	public function editProduct($id = null)
	{
	}
}
