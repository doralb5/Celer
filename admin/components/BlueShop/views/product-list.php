<?php

require_once VIEWS_PATH.'TableList_class.php';

$table = new TableListView('products');

$table->setElements($elements);
$table->setFields(array(
	new TableList_Field('codice', 'Codice', 'string', 'left'),
	new TableList_Field('titolo_it', 'Titolo', 'string', 'left'),
	new TableList_Field('id_categoria', 'Categoria', 'string', 'left'),
	new TableList_Field('listino1', 'Listino1', 'curr', 'right'),
));

$table->setElements_per_page($elements_per_page);
$table->setTotalElements($totalElements);

$table->multipleDeletion(false);
$table->deleteOption(false);

$table->render();
