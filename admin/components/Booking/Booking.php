<?php

class Booking_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_booking_delete', $this->getActionUrl('booking_delete'));
		$this->view->set('url_booking_list', $this->getActionUrl('booking_list'));
		$this->view->set('url_booking_edit', $this->getActionUrl('booking_edit'));
		$this->view->set('url_booking_details', $this->getActionUrl('booking_details'));

		$this->view->set('url_room_type_edit', $this->getActionUrl('room_type_edit'));
		$this->view->set('url_room_type_list', $this->getActionUrl('room_type_list'));
		$this->view->set('url_room_type_delete', $this->getActionUrl('room_type_delete'));

		//addRoomTypeImages
		$this->view->set('url_RoomTypeImage_delete', $this->getActionUrl('RoomTypeImage_delete'));
		$this->view->set('url_RoomTypeImage_download', $this->getActionUrl('RoomTypeImage_download'));
		$this->view->set('url_RoomTypeImageDefault_enable', $this->getActionUrl('RoomTypeImageDefault_enable'));
		$this->view->set('url_RoomTypeImageDefault_disable', $this->getActionUrl('RoomTypeImageDefault_disable'));
		$this->view->set('url_RoomTypeImage_show', substr(WEBROOT, 0, -6).MEDIA_ROOT.'booking/rooms');
	}

	public function EditHotel($id = null)
	{
		// add or edit hotel info
	}

	public function EditRoom($id = null)
	{
		// add or edit room
	}

	public function ListRooms($id_hotel = null)
	{
		//list all rooms of a hotel . if id_hotel it is not specified than get all the rooms
	}

	public function EditRoomType()
	{
		echo 'Hello';
	}

	public function booking_list()
	{
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$BookingList = $this->model->getList($elements_per_page, $offset, $filter);
		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddBooking]', $this->getActionUrl('booking_edit')));
		$this->view->set('bookinglist', $BookingList);
		$this->view->render('booking-list');
	}

	public function booking_delete($id = null)
	{
		$booking = $this->model->getBooking($id);
		$result = $this->model->deleteBooking($id);
		Utils::RedirectTo($this->getActionUrl('booking_list'));
	}

	public function booking_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Booking]', $this->getActionUrl('booking_list'));
		if (!is_null($id)) {
			$booking = $this->model->getBooking($id);
			HeadHTML::setTitleTag($this->view->getTerm('EditBooking').' - '.$booking->id.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditBooking]', $this->getActionUrl('booking_edit')."/$id");
		} else {
			$booking = new Booking_Entity();
			HeadHTML::setTitleTag($this->view->getTerm('NewBooking').' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$NewBooking]', $this->getActionUrl('booking_edit'));
		}
		if ($booking === false) {
			$this->AddError("Booking $id not found");
		} elseif (isset($_POST['save'])) {
			$booking->guest_firstname = $_POST['firstname'];
			$booking->guest_lastname = $_POST['lastname'];
			$booking->guest_email = $_POST['email'];
			$booking->guest_address = $_POST['address'];
			$booking->guest_city = $_POST['city'];
			$booking->guest_zipcode = $_POST['zipcode'];
			$booking->guest_country = $_POST['country'];
			$booking->guest_phone = $_POST['phone'];
			$booking->guest_birthday = $_POST['birthday'];
			$booking->guest_personalid = $_POST['personalid'];
			$booking->status_id = $_POST['status'];
			$booking->reservation_start = $_POST['reservation_start'];
			$booking->reservation_end = $_POST['reservation_end'];
			$booking->final_price = $_POST['final_price'];
			$booking->guests_qty = $_POST['sleeps'];
			$booking->note = $_POST['note'];
			$inserted_id = $this->model->saveBooking($booking);

			if (!is_array($inserted_id)) {
				$this->AddNotice('Booking has been saved successfully.');
			}
		}
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('booking_list')));
		$title = (!is_null($id)) ? '[$EditBooking]' : '[$NewBooking]';
		$this->view->setTitle($title);
		$this->view->set('booking', $booking);
		$this->view->render('booking-edit');
	}

	public function booking_details($id)
	{
		$booking = $this->model->getBooking($id);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('booking_list')));
		$this->view->BreadCrumb->addDir('[$Booking]', $this->getActionUrl('booking_list'));
		$this->view->BreadCrumb->addDir('[$BookingDetails]', $this->getActionUrl('booking_details')."/$id");
		$this->view->setTitle('[$Details]');
		$this->view->set('booking', $booking);
		$this->view->render('booking-details');
	}

	public function room_type_list()
	{
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$RoomTypeList = $this->model->getRoomTypeList($elements_per_page, $offset, $filter);
		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddRoom]', $this->getActionUrl('room_type_edit')));
		$this->view->set('roomtypelist', $RoomTypeList);
		$this->view->render('room-type-list');
	}

	public function room_type_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$RoomType]', $this->getActionUrl('room_type_list'));
		if (!is_null($id)) {
			$room_type = $this->model->getRoomType($id);
			$otheroption = false;
			HeadHTML::setTitleTag($this->view->getTerm('EditRoom').' - '.$room_type->id.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditRoom]', $this->getActionUrl('room_type_edit')."/$id");
		} else {
			require_once DOCROOT.ENTITIES_PATH.'Booking/BookingRoomType.php';
			$room_type = new BookingRoomType_Entity();
			$room_type->place_id = 2;
			$otheroption = true;
			HeadHTML::setTitleTag($this->view->getTerm('NewRoom').' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$NewRoom]', $this->getActionUrl('room_type_edit'));
		}

		$logged_user = UserAuth::getLoginSession();
		if ($room_type === false) {
			$this->AddError("Room Type $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['name'] == '') ? $this->AddError('Name is required!') : $room_type->name = $_POST['name'];
			($_POST['bedrooms'] == '') ? $this->AddError('Bedrooms is required!') : $room_type->bedrooms = $_POST['bedrooms'];
			($_POST['sleeps'] == '') ? $this->AddError('Sleeps is required!') : $room_type->sleeps = $_POST['sleeps'];
			($_POST['sorting'] == '') ? $this->AddError('Sorting is required!') : $room_type->sorting = $_POST['sorting'];
			if (count($this->view->getErrors()) == 0) {
				$old_rooms = $this->model->getRoomsByType($id);

				$inserted_id = $this->model->saveRoomType($room_type);

				if (!is_array($inserted_id)) {
					require_once DOCROOT.ENTITIES_PATH.'Booking/BookingRoom.php';

					if (isset($_POST['rooms'])) {
						$room = new BookingRoom_Entity();
						$room_post = $_POST['rooms'];
						$room_post = array_values($room_post);
						$room_id = $_POST['id'];
						$room_id = array_values($room_id);
						foreach ($room_post as $index => $r) {
							if ($_POST['id'][$index] != '') {
								$room->id = $room_id[$index];
							}
							$room->name = $r;
							$room->room_type_id = $id;
							$insert = $this->model->saveRoom($room);
						}
					}
					if (isset($_POST['addrooms'])) {
						$add_rooms = $_POST['addrooms'];
						$add_rooms = array_values($add_rooms);
						foreach ($add_rooms as $add) {
							if ($add != '') {
								$room = new BookingRoom_Entity();
								$room->name = $add;
								$room->room_type_id = $id;
								$insert = $this->model->saveRoom($room);
							}
						}
					}
				}
				foreach ($old_rooms as $curr) {
					if (!in_array($curr->id, $room_id)) {
						$this->model->deleteRoom($curr->id);
						$this->AddNotice("Room $curr->id was deleted!");
					}
				}

				$this->AddNotice('Room type has been saved successfully.');
				if ($otheroption == true) {
					Utils::RedirectTo($this->getActionUrl('room_type_edit')."/$inserted_id");
				}
			}
		} elseif (isset($_POST['AddImage'])) {
			if ($_FILES['images']['tmp_name'][0] == '') {
				$this->view->AddError('Please upload a file...');
			}

			if (count($this->view->getErrors()) == 0) {
				$failures = 0;
				for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
					$tmp_name = time().'_'.$_FILES['images']['name'][$i];
					require_once DOCROOT.ENTITIES_PATH.'Booking/BookingRoomTypeImage.php';
					$addRoomTypeImage = new BookingRoomTypeImage_Entity();

					$addRoomTypeImage->room_type_id = $id;
					$addRoomTypeImage->filename = $tmp_name;
					$addRoomTypeImage->is_default = 0;
					$res = $this->model->saveRoomTypeImage($addRoomTypeImage);
					if (!is_array($res)) {
						$target = DOCROOT.MEDIA_ROOT.'booking/rooms'.DS.$room_type->id;
						Utils::createDirectory($target);
						$filename = $target.DS.$tmp_name;
						move_uploaded_file($_FILES['images']['tmp_name'][$i], $filename);
					} else {
						$failures++;
					}
				}
				if ($failures > 0) {
					$this->AddError('There was '.$failures.' failures during image upload!');
				} else {
					$this->view->AddNotice('Image has been uploaded successfully.');
				}
			}
			Utils::RedirectTo($this->getActionUrl('room_type_edit')."/$id");
		}
		$roomtypeimages = $this->model->getRoomImagesByType($id);
		$rooms = $this->model->getRoomsByType($id);
		$this->view->set('roomtypeimages', $roomtypeimages);
		$this->view->set('room_type', $room_type);
		$this->view->set('rooms', $rooms);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('room_type_list')));
		$title = (!is_null($id)) ? '[$EditRoom]' : '[$NewRoom]';
		$this->view->setTitle($title);
		$this->view->render('booking-room-edit');
	}

	public function room_type_delete($id)
	{
		$res = $this->model->deleteRoomType($id);
		Utils::RedirectTo($this->getActionUrl('room_type_list'));
		$this->view->AddNotice('Room with'.$id.' successfully deleted');
	}

	public function RoomTypeImage_delete($id)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('room_type_list'));
		}
		$image = $this->model->getRoomImagesById($id);

		$res = $this->model->deleteRoomTypeImage($id);
		if ($res) {
			$this->LogsManager->registerLog('RoomTypeImage', 'delete', "Image deleted with id : $id", $id);
			$this->AddNotice('Image has been deleted successfully.');
			$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'booking'.DS.'rooms/'.$image->room_type_id.DS.$image->filename;
			var_dump($filename);
			unlink(DOCROOT.$filename);
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('room_type_edit')."/{$image->id}");
	}

	public function RoomTypeImage_download($id)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('room_type_list'));
		}

		$image = $this->model->getRoomImagesById($id);

		$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'booking/rooms/'.$image->room_type_id.'/'.$image->filename;
		//Shkarkimi i Files
		header('Content-Type: '.mime_content_type(DOCROOT.$filename));
		header('Content-Disposition: attachment; filename="'.$image->filename.'""');
		readfile(DOCROOT.$filename);
	}

	public function RoomTypeImageDefault_enable($id)
	{
		if (!is_null($id)) {
			$roomtypeimage = $this->model->getRoomImagesById($id);
			$currTypeImage = $this->model->getRoomImagesByType($roomtypeimage->room_type_id);
			foreach ($currTypeImage as $curr) {
				if ($curr->is_default) {
					$change_old_default = $this->model->getRoomImagesById($curr->id);
					$change_old_default->is_default = 0;
					$res = $this->model->saveRoomTypeImage($change_old_default);
				}
			}
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$roomtypeimage->is_default = 1;
		$res = $this->model->saveRoomTypeImage($roomtypeimage);

		if (!is_array($res)) {
			$this->view->AddNotice('Image default has been enabled successfully.');
			$this->LogsManager->registerLog('Image room type', 'update', 'Image default enabled with id : '.$roomtypeimage->id, $roomtypeimage->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('room_type_edit')."/{$id}");
	}

	public function RoomTypeImageDefault_disable($id)
	{
		if (!is_null($id)) {
			$roomtypeimage = $this->model->getRoomImagesById($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$roomtypeimage->is_default = 0;
		$res = $this->model->saveRoomTypeImage($roomtypeimage);

		if (!is_array($res)) {
			$this->view->AddNotice('Image default has been disabled successfully.');
			$this->LogsManager->registerLog('Image room type', 'update', 'Image default enabled with id : '.$roomtypeimage->id, $roomtypeimage->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('room_type_edit')."/{$id}");
	}

	public function EditBooking($id = null)
	{
		//add or edit a booking
	}

	// Delete functions below

	public function DeleteHotel($id = null)
	{
	}

	public function DeleteRoom($id = null)
	{
	}

	public function DeleteRoomType($id = null)
	{
	}
}
