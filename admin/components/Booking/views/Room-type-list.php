<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('BookingRoomType');
foreach ($roomtypelist as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_room_type_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_room_type_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_room_type_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		new Button('<i class="fa fa-eye"></i>', $url_room_type_details.'/'.$row->id, 'xs', '', '[$Details]'),
	);
}
$table->setElements($roomtypelist);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$name]', 'string', 'left'),
	new TableList_Field('bedrooms', '[$bedrooms]', 'int', 'left'),
	new TableList_Field('sleeps', '[$sleeps]', 'int', 'left'),
	new TableList_Field('sorting', '[$sorting]', 'int', 'left'),
));
$table->setUrl_action($url_room_type_list);
$table->setUrl_delete($url_room_type_delete);
$table->render();
