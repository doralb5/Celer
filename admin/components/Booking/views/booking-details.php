<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-5">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i
                            class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i
                            class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$BookingDetails] - <span class=""><?= $booking->title ?></span></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered personalinfo">
                        <tbody>
                        <tr>
                            <td>[$id]</td>
                            <td><b><?= $booking->id ?></b></td>
                        </tr>
                        <tr>
                            <td>[$reservation-start]</td>
                            <td><?= date('d/m/Y', strtotime($booking->reservation_start)) ?></td>
                        </tr>
                        <tr>
                            <td>[$reservation-end]</td>
                            <td><?= date('d/m/Y', strtotime($booking->reservation_end)) ?></td>
                        </tr>
                        
                        <tr>
                            <td>[$guests_qty]</td>
                            <td><b><?= $booking->guests_qty ?></b></td>
                        </tr>
                        <tr>
                            <td>[$guest_firstname]</td>
                            <td><b><?= $booking->guest_firstname ?></b></td>
                        </tr>
                        <tr>
                            <td>[$guest_lastname]</td>
                            <td><b><?= $booking->guest_lastname ?></b></td>
                        </tr>
                        <tr>
                            <td>[$guest_address]</td>
                            <td><b><?= $booking->guest_address ?></b></td>
                        </tr>
                        <tr>
                            <td>[$guest_city]</td>
                            <td><b><?= $booking->guest_city ?></b></td>
                        </tr>
                        <tr>
                            <td>[$guest_zipcode]</td>
                            <td><b><?= $booking->guest_zipcode ?></b></td>
                        </tr>
                        <tr>
                            <td>[$guest_country]</td>
                            <td><b><?= $booking->guest_country ?></b></td>
                        </tr>
                        <tr>
                            <td>[$guest_phone]</td>
                            <td><b><?= $booking->guest_phone ?></b></td>
                        </tr>
                        <tr>
                            <td>[$guest_email]</td>
                            <td><b><?= $booking->guest_email ?></b></td>
                        </tr>
                        <tr>
                            <td>[$guest_birthday]</td>
                            <td><b><?= $booking->guest_birthday ?></b></td>
                        </tr>
                        <tr>
                            <td>[$guest_personalid]</td>
                            <td><b><?= $booking->guest_personalid ?></b></td>
                        </tr>
                        <tr>
                            <td>[$final_price]</td>
                            <td><b><?= $booking->final_price ?></b></td>
                        </tr>
                        <tr>
                            <td>[$status]</td>
                            <td><?= ($booking->status_id == '2') ? '<i class="fa fa-check-square" style="color:green"></i>' : '<i class="fa fa-square-o" style="color:red"></i>' ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <p>[$note]</p>

                <div class="col-md-12">
                    <?= $booking->note ?>
                </div>
                <p>&nbsp;</p>

                <div class="col-md-12 text-right">
                    <a class="btn btn-danger" href="<?= $url_booking_delete."/{$booking->id}" ?>"><i
                            class="fa fa-minus-circle"></i>&nbsp;&nbsp;[$Delete]</a>
                </div>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>
</div>