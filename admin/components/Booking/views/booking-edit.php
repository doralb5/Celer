<?php

$id_user = isset($_POST['id_user']) ? $_POST['id_user'] : $booking->user_id;
$guest_firstname = isset($_POST['guest_firstname']) ? $_POST['guest_firstname'] : $booking->guest_firstname;
$reservation_start = isset($_POST['reservation_start']) ? $_POST['reservation_start'] : $booking->reservation_start;
$reservation_end = isset($_POST['reservation_end']) ? $_POST['reservation_end'] : $booking->reservation_end;
$price = isset($_POST['price']) ? $_POST['price'] : $booking->final_price;
$guest_lastname = isset($_POST['guest_lastname']) ? $_POST['guest_lastname'] : $booking->guest_lastname;
$guest_address = isset($_POST['guest_address']) ? $_POST['guest_address'] : $booking->guest_address;
$guest_city = isset($_POST['guest_city']) ? $_POST['guest_city'] : $booking->guest_city;
$guest_zipcode = isset($_POST['guest_zipcode']) ? $_POST['guest_zipcode'] : $booking->guest_zipcode;
$guest_country = isset($_POST['guest_country']) ? $_POST['guest_country'] : $booking->guest_country;
$guest_phone = isset($_POST['guest_phone']) ? $_POST['guest_phone'] : $booking->guest_phone;
$guest_email = isset($_POST['email']) ? $_POST['email'] : $booking->guest_email;
$guest_birthday = isset($_POST['guest_birthday']) ? $_POST['guest_birthday'] : $booking->guest_birthday;
$guest_personalid = isset($_POST['guest_personalid']) ? $_POST['guest_personalid'] : $booking->guest_personalid;
$final_price = isset($_POST['final_price']) ? $_POST['final_price'] : $booking->final_price;
$sleeps = isset($_POST['sleeps']) ? $_POST['sleeps'] : $booking->guests_qty;
$status_id = isset($_POST['status_id']) ? $_POST['status_id'] : $booking->status_id;
$note = isset($_POST['note']) ? $_POST['note'] : $booking->note;
// $room_id = isset($_POST['room_id']) ? $_POST['room_id'] : $booking->room_id;

?>
<form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
      novalidate="" enctype="multipart/form-data">
        <div class="col-md-8">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$GeneralInformation]</h4>
                </div>
                <div class="panel-body panel-form">

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="firstname">[$firstname]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $guest_firstname ?>"
                                   name="firstname" placeholder="[$firstname]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="lastname">[$lastname]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $guest_lastname ?>"
                                   name="lastname" placeholder="[$lastname]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="email">[$email]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $guest_email ?>"
                                   name="email" placeholder="[$email]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="address">[$address]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $guest_address ?>"
                                   name="address" placeholder="[$address]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="city">[$city]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $guest_city ?>"
                                   name="city" placeholder="[$city]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="zipcode">[$zipcode]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $guest_zipcode ?>"
                                   name="zipcode" placeholder="[$zipcode]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="country">[$country]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $guest_country ?>"
                                   name="country" placeholder="[$country]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="phone">[$phone]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $guest_phone ?>"
                                   name="phone" placeholder="[$phone]">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="birthday">[$birthday]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $guest_birthday ?>"
                                   name="birthday" placeholder="[$birthday]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="personalid">[$personalid]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $guest_personalid ?>"
                                   name="personalid" placeholder="[$personalid]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="note">[$note]</label>

                        <div class="col-md-8 col-sm-8">
                            <textarea class="form-control" rows="5" name="note" placeholder="[$note]"><?= $note ?>
                            </textarea>
                        </div>
                    </div>
                    
                </div>
            </div>


        </div>

        <div class="col-md-4">


            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$PublishSettings]</h4>
                </div>
                <div class="panel-body panel-form">
                    <div class="form-group">
                        <label class="col-md-4 control-label">[$status]</label>

                        <div class="col-md-8">
                            <select name="status" class="form-control">
                                <option value="1" <?= ($status_id == '1') ? 'selected' : '' ?>>[$new]
                                </option>
                                <option value="2" <?= ($status_id == '2') ? 'selected' : '' ?>>[$confirmed]
                                </option>
                                <option value="3" <?= ($status_id == '3') ? 'selected' : '' ?>>[$canceled]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="name">[$reservation-start] *</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text"  value="<?= date('d/m/Y', strtotime($booking->reservation_start)) ?>"
                                   id="datetimepicker1" name="reservation_start" placeholder="[$reservation-start]" <?= ($lang == CMSSettings::$default_lang) ? 'required' : ''?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">[$reservation-end]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text"  value="<?= date('d/m/Y', strtotime($booking->reservation_end)) ?>"
                                   id="datetimepicker2" name="reservation_end" placeholder="[$reservation-end]" <?= ($lang == CMSSettings::$default_lang) ? 'required' : ''?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="price">[$price]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?=$final_price ?>"
                                   name="final_price" placeholder="[$price]">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="sleeps">[$sleeps]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?=$sleeps ?>"
                                   name="sleeps" placeholder="[$sleeps]">
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- end panel -->

           
        </div>


    <div class="col-md-12">
        <p class="text-center">
            <button type="submit" name="save" class="btn btn-info">
                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
            </button>
        </p>
    </div>
    <p>&nbsp;</p>
</form>

<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>

<script>
    $(function () {
        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker();
    });
</script>
<style>
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
    }

    .inputfile:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .box {
        padding: 0px;
    }
</style>
<script>
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;
        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();
            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });</script>