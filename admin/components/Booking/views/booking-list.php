<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Booking');
foreach ($bookinglist as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_booking_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_booking_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_booking_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		new Button('<i class="fa fa-eye"></i>', $url_booking_details.'/'.$row->id, 'xs', '', '[$Details]'),
	);
}
$table->setElements($bookinglist);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('reservation_start', '[$reservation_start]', 'date', 'left'),
	new TableList_Field('reservation_end', '[$reservation_end]', 'date', 'left'),
	new TableList_Field('final_price', '[$price]', 'string', 'left'),
	new TableList_Field('created_time', '[$created_time]', 'date', 'left'),
	new TableList_Field('status_id', '[$Status]', 'bool', 'left'),
));
$table->setUrl_action($url_booking_list);
$table->setUrl_delete($url_booking_delete);
$table->render();
