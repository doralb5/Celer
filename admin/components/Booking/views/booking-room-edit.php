<style>
    .entry:not(:first-of-type)
    {
        margin-top: 10px;
    }

    .glyphicon
    {
        font-size: 12px;
    }
</style>

<?php
$name = isset($_POST['name']) ? $_POST['name'] : $room_type->name;
$bedrooms = isset($_POST['bedrooms']) ? $_POST['bedrooms'] : $room_type->bedrooms;
$sleeps = isset($_POST['sleeps']) ? $_POST['sleeps'] : $room_type->sleeps;
$sorting = isset($_POST['sorting']) ? $_POST['sorting'] : $room_type->sorting;

?>

<form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
      novalidate="" enctype="multipart/form-data">
        <div class="col-md-8">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$GeneralInformation]</h4>
                </div>
                <div class="panel-body panel-form">

                    <div class="tab-content">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$name] *</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $name ?>"
                                       name="name" placeholder="[$name]" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$bedrooms]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $bedrooms ?>"
                                       name="bedrooms" placeholder="[$bedrooms]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$sleeps]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $sleeps ?>"
                                       name="sleeps" placeholder="[$sleeps]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$sorting]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $sorting ?>"
                                       name="sorting" placeholder="[$sorting]">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php if (!is_null($room_type->id)) {
	?>
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$Rooms]</h4>
                </div>
                <div class="panel-body panel-form">


                    <table class="table table-hover" id="addRoomBoxes">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($rooms as $index => $r) {
		?>
                            <?php $id = isset($_POST['id'][$index]) ? $_POST['id'][$index] : $r->id; ?>
                            <?php $room_name = isset($_POST['rooms'][$index]) ? $_POST['rooms'][$index] : $r->name; ?>
                            <tr>
                                <td><input class="form-control" type="hidden" value="<?=$id ?>" name="id[]"/><?=$id ?></td>
                                <td><input class="form-control" type="text" value="<?=$room_name ?>" name="rooms[]"/></td>
                                <td><input class="btn btn-danger" type="button" value="Delete" onclick="deleteRoom(this)"/></td>
                            </tr>
                            
                            <div id="listTable"></div>
                            <?php
	} ?>
                            <tr>
                                <td>
                                    <input class="btn btn-success" type="button" id="addButton" value="Add New Room" />
                                </td>
                                <td>
                                    <input class="btn btn-warning" type="button" id="removeButton" value="Remove" />
                                </td>
                                <td></td>
                            </tr>
                            <tr class="addroom">
                                <td>
                                    <label>Name of room</label>
                                </td>
                                <td>
                                    <input class="form-control" type="text" name="addrooms[]" />
                                </td>
                                <td/>
                            </tr>
                        </tbody>
                    </table>
                    
            <script>
            $(function () {
                $("#addButton").on("click", function () {
                    var newName = $(".addroom").first().clone().addClass("newAdded");

                    newName.appendTo("#addRoomBoxes");
                });

                $("#removeButton").click(function () {
                    var counter = $(".addroom").length;

                    if (counter == 1) {
                        alert("No more textbox to remove");

                        return false;
                    }
                    $(".addroom").last().remove();
                });
                
                
            });
            function deleteRoom(room)
                {
                    var i=room.parentNode.parentNode.rowIndex;
                    document.getElementById('addRoomBoxes').deleteRow(i);
                }
            </script>
            
                    
                </div>
            </div>
            <!-- end panel -->
        </div>
<?php
} ?>
        


    <div class="col-md-12">
        <p class="text-center">
            <button type="submit" name="save" class="btn btn-info">
                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
            </button>
        </p>
    </div>
    <p>&nbsp;</p>
</form>


<?php if (!is_null($room_type->id)) {
		?>

    <div class="row">
        <div class="col-md-8">

            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i
                                class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i
                                class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$RoomTypeImages]</h4>
                </div>
                <div class="panel-body">

                    <p>&nbsp;</p>
                    <?php
					include(VIEWS_PATH.'TableList_class.php');
		$table = new TableListView('RoomTypeImages', 'list');
		foreach ($roomtypeimages as &$row) {
			if ($row->is_default) {
				$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_RoomTypeImageDefault_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
			} else {
				$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_RoomTypeImageDefault_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
			}
			$row->rows_buttons = array(
							$btn1,
							new Button('<i class="fa fa-eye"></i>', $url_RoomTypeImage_show.'/'.$row->room_type_id.'/'.$row->filename, 'xs', '', '[$Show]', '_blank'),
							new Button('<i class="fa fa-download"></i>', $url_RoomTypeImage_download.'/'.$row->id, 'xs', '', '[$Download]')
						);

			$row->row_imagePath = substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'booking/rooms/'.$row->id;
		}

		$table->setElements($roomtypeimages);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
						new TableList_Field('id', '[$Id]', 'int', 'left'),
						new TableList_Field('filename', '[$Filename]', 'string', 'left'),
						new TableList_Field('is_default', '[$default]', 'bool', 'left'),
					));
		$table->setUrl_delete($url_RoomTypeImage_delete);
		$table->setUrl_delete_params(array('id' => $$roomtypeimages->id));
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->render(); ?>


                    <div class="col-md-12">
                        <form id="fileupload" method="POST" enctype="multipart/form-data">
                            <div class="form-group col-md-9">
                                <label>[$SelectFile]</label>

                                <div class="box">
                                    <input type="file" name="images[]" id="file-7" class="inputfile inputfile-6"
                                           data-multiple-caption="{count} files selected" multiple/>
                                    <label for="file-7"><span></span><strong>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                                 viewBox="0 0 20 17">
                                                <path
                                                    d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                            </svg>
                                            [$Choose]&hellip;</strong></label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <br>
                                <button class="btn btn-info" name="AddImage" type="submit"><i class="fa fa-plus"></i>&nbsp;&nbsp;[$AddFile]
                                </button>

                            </div>
                        </form>
                    </div>
                    <!--<div class="col-md-12 note note-info">
                        <h4>Notes</h4>
                        <ul>
                            <li>The maximum file size for uploads is <strong>20 MB</strong></li>
                        </ul>
                    </div>-->
                </div>
            </div>

        </div>
       
    </div>

<?php
	} ?>



<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>

<script>
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker();
    });
</script>
<style>
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
    }

    .inputfile:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .box {
        padding: 0px;
    }
</style>
<script>
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;
        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();
            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });</script>



