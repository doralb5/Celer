<?php
$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $article->id_category;
$id_user = isset($_POST['id_user']) ? $_POST['id_user'] : $article->id_user;
$author = isset($_POST['author']) ? $_POST['author'] : $article->author;
$publish_date = isset($_POST['publish_date']) ? $_POST['publish_date'] : (!is_null($article->publish_date) ? $article->publish_date : date('Y-m-d H:i:s'));
$expire_date = isset($_POST['expire_date']) ? $_POST['expire_date'] : $article->expire_date;
$expire_date = isset($_POST['expire_date']) ? $_POST['expire_date'] : $article->expire_date;
$featured = isset($_POST['featured']) ? $_POST['featured'] : $article->featured;
$enabled = isset($_POST['enabled']) ? $_POST['enabled'] : $article->enabled;
$show_title = isset($_POST['show_title']) ? $_POST['show_title'] : $article->show_title;
$show_subtitle = isset($_POST['show_subtitle']) ? $_POST['show_subtitle'] : $article->show_subtitle;
$show_date = isset($_POST['show_date']) ? $_POST['show_date'] : $article->show_date;
$show_author = isset($_POST['show_author']) ? $_POST['show_author'] : $article->show_author;
$show_share = isset($_POST['show_share']) ? $_POST['show_share'] : $article->show_share;
$show_comments = isset($_POST['show_comments']) ? $_POST['show_comments'] : $article->show_comments;

foreach (CMSSettings::$available_langs as $lang) {
	$title_{$lang} = isset($_POST['title_'.$lang]) ? $_POST['title_'.$lang] : ((!is_null($article->{$lang})) ? $article->{$lang}->title : '');
	$subtitle_{$lang} = isset($_POST['subtitle_'.$lang]) ? $_POST['subtitle_'.$lang] : ((!is_null($article->{$lang})) ? $article->{$lang}->subtitle : '');
	$content_{$lang} = isset($_POST['content_'.$lang]) ? $_POST['content_'.$lang] : ((!is_null($article->{$lang})) ? $article->{$lang}->content : '');
}
?>

<form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
      novalidate="" enctype="multipart/form-data">
        <div class="col-md-8">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$GeneralInformation]</h4>
                </div>
                <div class="panel-body panel-form">

                    <ul class="nav nav-tabs">
                        <?php foreach (CMSSettings::$available_langs as $lang) {
	?>
                            <li class="<?= ($lang == CMSSettings::$default_lang) ? 'active' : '' ?>"><a
                                    href="#tab_<?= $lang ?>" data-toggle="tab"><?= strtoupper($lang) ?></a></li>
                        <?php
} ?>
                    </ul>
                    <div class="tab-content">

                        <?php foreach (CMSSettings::$available_langs as $lang) {
		?>

                            <div
                                class="tab-pane fade <?= ($lang == CMSSettings::$default_lang) ? 'active in' : '' ?>"
                                id="tab_<?= $lang ?>">


                                <div class="form-group">
                                    <label class="control-label col-md-4 col-sm-4" for="name">[$Title] *</label>

                                    <div class="col-md-8 col-sm-8">
                                        <input class="form-control" type="text" value="<?= $title_{$lang} ?>"
                                               name="title_<?= $lang ?>" placeholder="[$Title]" <?= ($lang == CMSSettings::$default_lang) ? 'required' : ''?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-sm-4">[$Subtitle]</label>

                                    <div class="col-md-8 col-sm-8">
                                        <input class="form-control" type="text" value="<?= $subtitle_{$lang} ?>"
                                               name="subtitle_<?= $lang ?>" placeholder="[$Title]">
                                    </div>
                                </div>
                            </div>
                        <?php
	} ?>

                    </div>
                    <hr>

                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 control-label">[$Category]</label>

                        <div class="col-md-8 col-sm-8">
                            <select class="form-control" name="id_category">
                                <?php foreach ($categories as $categ) {
		?>
                                    <option
                                        value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->section.' => '.$categ->category ?></option>
                                <?php
	} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="author">[$Author]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $author ?>"
                                   name="author" placeholder="[$Author]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                        <div class="col-md-6 col-sm-6">
                            <input type="file" name="image">
                            <?php if ($article->image != '') {
		?>
                                <p>
                                    <img
                                        src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT."articles/{$article->id_category}/".$article->image ?>"
                                        width="100px" alt="Image"/></p>
                            <?php
	} ?>
                        </div>
                    </div>
                </div>
            </div>


            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$Content]</h4>
                </div>
                <div class="panel-body panel-form">


                    <ul class="nav nav-tabs">
                        <?php foreach (CMSSettings::$available_langs as $lang) {
		?>
                            <li class="<?= ($lang == CMSSettings::$default_lang) ? 'active' : '' ?>"><a
                                    href="#ctab_<?= $lang ?>" data-toggle="tab"><?= strtoupper($lang) ?></a></li>
                        <?php
	} ?>
                    </ul>
                    <div class="tab-content">

                        <?php foreach (CMSSettings::$available_langs as $lang) {
		?>

                            <div
                                class="tab-pane fade <?= ($lang == CMSSettings::$default_lang) ? 'active in' : '' ?>"
                                id="ctab_<?= $lang ?>">

                                    <textarea class="editor" id="editor_<?= $lang ?>"
                                              name="content_<?= $lang ?>"><?= $content_{$lang} ?></textarea>

                            </div>
                        <?php
	} ?>

                    </div>

                </div>
            </div>
            <!-- end panel -->
        </div>

        <div class="col-md-4">


            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$PublishSettings]</h4>
                </div>
                <div class="panel-body panel-form">
                    <div class="form-group">
                        <label class="col-md-4 control-label">[$Enabled]</label>

                        <div class="col-md-8">
                            <select name="enabled" class="form-control">
                                <option value="1" <?= ($enabled == '1') ? 'selected' : '' ?>>[$yes_option]
                                </option>
                                <option value="0" <?= ($enabled == '0') ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">[$PublishDate]</label>

                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" name="publish_date" id="datetimepicker1"
                                   placeholder="[$SelectDate]" value="<?= $publish_date ?>" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">[$ExpirationDate]</label>

                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" name="expire_date" id="datetimepicker2"
                                   placeholder="[$SelectDate]" value="<?= $expire_date ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">[$Featured]</label>

                        <div class="col-md-8">
                            <select name="featured" class="form-control">
                                <option value="1" <?= ($featured == '1') ? 'selected' : '' ?>>[$yes_option]
                                </option>
                                <option value="0" <?= ($featured == '0') ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->

            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$Options]</h4>
                </div>
                <div class="panel-body panel-form">
                    <div class="form-group">
                        <label class="col-md-4 control-label">[$ShowTitle]</label>

                        <div class="col-md-8">
                            <select name="show_title" class="form-control">
                                <option value="0" <?= ($show_title == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_title == 'yes') ? 'selected' : '' ?>>
                                    [$yes_option]
                                </option>
                                <option value="no" <?= ($show_title == 'no') ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">[$ShowSubtitle]</label>

                        <div class="col-md-8">
                            <select name="show_subtitle" class="form-control">
                                <option value="0" <?= ($show_subtitle == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_subtitle == 'yes') ? 'selected' : '' ?>>
                                    [$yes_option]
                                </option>
                                <option value="no" <?= ($show_subtitle == 'no') ? 'selected' : '' ?>>
                                    [$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">[$ShowDate]</label>

                        <div class="col-md-8">
                            <select name="show_date" class="form-control">
                                <option value="0" <?= ($show_date == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_date == 'yes') ? 'selected' : '' ?>>[$yes_option]
                                </option>
                                <option value="no" <?= ($show_date == 'no') ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">[$ShowAuthor]</label>

                        <div class="col-md-8">
                            <select name="show_author" class="form-control">
                                <option value="0" <?= ($show_author == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_author == 'yes') ? 'selected' : '' ?>>
                                    [$yes_option]
                                </option>
                                <option value="no" <?= ($show_author == 'no') ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">[$ShowShare]</label>

                        <div class="col-md-8">
                            <select name="show_share" class="form-control">
                                <option value="0" <?= ($show_share == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_share == 'yes') ? 'selected' : '' ?>>
                                    [$yes_option]
                                </option>
                                <option value="no" <?= ($show_share == 'no') ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">[$ShowComments]</label>

                        <div class="col-md-8">
                            <select name="show_comments" class="form-control">
                                <option value="0" <?= ($show_comments == '0') ? 'selected' : '' ?>>[$default]
                                </option>
                                <option value="yes" <?= ($show_comments == 'yes') ? 'selected' : '' ?>>
                                    [$yes_option]
                                </option>
                                <option value="no" <?= ($show_comments == 'no') ? 'selected' : '' ?>>
                                    [$no_option]
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>


    <div class="col-md-12">
        <p class="text-center">
            <button type="submit" name="save" class="btn btn-info">
                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
            </button>
        </p>
    </div>
    <p>&nbsp;</p>
</form>


<?php if (!is_null($article->id)) {
		?>

    <div class="row">
        <div class="col-md-8">

            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i
                                class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i
                                class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$AdditionalImages]</h4>
                </div>
                <div class="panel-body">

                    <p>&nbsp;</p>
                    <?php
					include(VIEWS_PATH.'TableList_class.php');
		$table = new TableListView('Articles', 'list');
		foreach ($additionalImages as &$row) {
			$row->rows_buttons = array(
							new Button('<i class="fa fa-eye"></i>', $url_addtImage_show.'/'.$article->id_category.'/additional/'.$row->image, 'xs', '', '[$Show]', '_blank'),
							new Button('<i class="fa fa-download"></i>', $url_addtImage_download.'/'.$row->id, 'xs', '', '[$Download]')
						);

			$row->row_imagePath = substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.DS.'articles'.DS.$row->id_category.DS.'additional';
		}

		$table->setElements($additionalImages);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
						new TableList_Field('id', '[$Id]', 'int', 'left'),
						new TableList_Field('image', '[$Filename]', 'string', 'left'),
						new TableList_Field('image', '[$Image]', 'image', 'left'),
					));
		$table->setUrl_delete($url_addtImage_delete);
		$table->setUrl_delete_params(array('id_article' => $article->id));
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->render(); ?>


                    <div class="col-md-12">
                        <form id="fileupload" method="POST" enctype="multipart/form-data">
                            <div class="form-group col-md-9">
                                <label>[$SelectFile]</label>

                                <div class="box">
                                    <input type="file" name="images[]" id="file-7" class="inputfile inputfile-6"
                                           data-multiple-caption="{count} files selected" multiple/>
                                    <label for="file-7"><span></span><strong>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                                 viewBox="0 0 20 17">
                                                <path
                                                    d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                            </svg>
                                            [$Choose]&hellip;</strong></label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <br>
                                <button class="btn btn-info" name="AddImage" type="submit"><i class="fa fa-plus"></i>&nbsp;&nbsp;[$AddFile]
                                </button>

                            </div>
                        </form>
                    </div>
                    <!--<div class="col-md-12 note note-info">
                        <h4>Notes</h4>
                        <ul>
                            <li>The maximum file size for uploads is <strong>20 MB</strong></li>
                        </ul>
                    </div>-->
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$AddTags]</h4>
                </div>
                <div class="panel-body panel-form">

                    <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true"
                          name="demo-form">
                        <div class="form-group">
                            <label class="control-label col-md-4">[$Tag]</label>

                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="tag" required/>
                                </div>
                            </div>
                        </div>
                        <p>&nbsp;</p>

                        <div class="col-md-12">
                            <?php
							if ($article->tags != '') {
								$tags = explode(';', $article->tags);
								foreach ($tags as $tag) {
									?>
                                    <span class="label label-primary"><?= $tag ?>
                                        <a class="btn btn-sm btn-toolbar"
                                           href="<?= Utils::getComponentUrl("Articles/deleteArticleTag/$tag?id_article={$article->id}") ?>"><i
                                                class="fa fa-times"></i></a>
                            </span>&nbsp;
                                <?php
								}
							} ?>
                        </div>
                        <p>&nbsp;</p>

                        <div class="col-md-12 text-center">
                            <button type="submit" name="AddTag" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;[$Add]
                            </button>
                        </div>
                        <p>&nbsp;</p>
                    </form>

                </div>
            </div>
            <!-- end panel -->
        </div>
    </div>

<?php
	} ?>



<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>

<script>
    <?php foreach (CMSSettings::$available_langs as $lang) {
	?>

    CKEDITOR.replace('editor_<?=$lang?>', {
//        imageBrowser_listUrl: "<?=substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).LIBS_PATH?>JsonDirImages.php?media_path=<?=substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).MEDIA_ROOT?>&docroot=<?=  rtrim(DOCROOT, '/')?>",
        filebrowserBrowseUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
        height: 232
    });

    <?php
} ?>

</script>

<script>
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker();
    });
</script>
<style>
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
    }

    .inputfile:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .box {
        padding: 0px;
    }
</style>
<script>
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;
        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();
            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });</script>