<?php

require_once DOCROOT.LIBS_PATH.'Email.php';

class Businesses_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('settings', $this->ComponentSettings);
		$this->view->set('url_businesses_list', $this->getActionUrl('businesses_list'));
		$this->view->set('url_business_edit', $this->getActionUrl('business_edit'));
		$this->view->set('url_business_delete', $this->getActionUrl('business_delete'));
		$this->view->set('url_business_enable', $this->getActionUrl('business_enable'));
		$this->view->set('url_business_disable', $this->getActionUrl('business_disable'));
		$this->view->set('url_business_details', $this->getActionUrl('business_details'));

		$this->view->set('url_categories_list', $this->getActionUrl('categories_list'));
		$this->view->set('url_category_edit', $this->getActionUrl('category_edit'));
		$this->view->set('url_category_delete', $this->getActionUrl('category_delete'));

		$this->view->set('url_profiles_list', $this->getActionUrl('profiles_list'));
		$this->view->set('url_profile_edit', $this->getActionUrl('profile_edit'));
		$this->view->set('url_profile_delete', $this->getActionUrl('profile_delete'));

		$this->view->set('url_subdomains_list', $this->getActionUrl('subdomains_list'));
		$this->view->set('url_subdomain_edit', $this->getActionUrl('subdomain_edit'));
		$this->view->set('url_subdomain_delete', $this->getActionUrl('subdomain_delete'));

		$this->view->set('url_sticker_done', $this->getActionUrl('sticker_done'));
		$this->view->set('url_sticker_undone', $this->getActionUrl('sticker_undone'));
		$this->view->set('url_stickers_list', $this->getActionUrl('stickers_list'));

		//Comments
		$this->view->set('url_comment_disable', $this->getActionUrl('comment_disable'));
		$this->view->set('url_comment_enable', $this->getActionUrl('comment_enable'));
		$this->view->set('url_comment_delete', $this->getActionUrl('comment_delete'));
		$this->view->set('url_comment_list', $this->getActionUrl('comments'));

		//Additional Images
		$this->view->set('url_addtImage_delete', $this->getActionUrl('addtImage_delete'));
		$this->view->set('url_addtImage_download', $this->getActionUrl('addtImage_download'));
		$this->view->set('url_addtImage_show', substr(WEBROOT, 0, -6).MEDIA_ROOT.'businesses');

		$this->model = Loader::getModel('Businesses_sub', 'Businesses');

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function businesses_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Businesses'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '1 ';

		$status = isset($_GET['status']) ? $_GET['status'] : '';
		$enabled = ($status == 'enabled') ? 1 : ($status == 'disabled' ? 0 : '');
		$id_profile = isset($_GET['id_profile']) ? $_GET['id_profile'] : '';

		$Business_tbl = TABLE_PREFIX.'Business';
		if ($enabled !== '') {
			$filter .= "AND $Business_tbl.enabled = '$enabled'";
		}

		if ($id_profile !== '') {
			$filter .= "AND $Business_tbl.id_profile = $id_profile";
		}

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$business = $this->model->getBusiness($id);
				$additionalImages = $this->model->getBusinessImages(100, 0, "id_business = $id");
				$res = $this->model->deleteBusiness($id);

				if ($res !== false) {
					$target = DOCROOT.MEDIA_ROOT.'businesses'.DS.$business->id_category;
					if ($business->image != '' && file_exists($target.$business->image)) {
						unlink($target.$business->image);
					}

					foreach ($additionalImages as $image) {
						if (file_exists($target.DS.'additional'.DS.$image->image)) {
							unlink($target.DS.'additional'.DS.$image->image);
						}
					}
					$this->LogsManager->registerLog('Business', 'delete', 'Business deleted with id : '.$id, $id);
				}
			}
			if ($res !== false) {
				$this->view->AddNotice('Businesses has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('businesses_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'Business.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'Business.company_name', 'peso' => 90),
				array('field' => TABLE_PREFIX.'BusinessCategory.category', 'peso' => 80),
			);
			$businesses = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$businesses = $this->model->getList($elements_per_page, $offset, $filter);
		}

		//Krijimi i aliaseve
		/* $pages_md = Loader::getModel('Pages');
		  foreach ($businesses as $bus) {

		  $alias = $pages_md->getPageAliases(1, 0, "object='Business' AND id_object={$bus->id}");
		  if (count($alias) == 0) {
		  $alias = new PageAlias_Entity();
		  $alias->object = 'Business';
		  $alias->id_object = $bus->id;

		  $alias->alias = Utils::url_slug($bus->category) . '/' . Utils::url_slug($bus->company_name) . "-{$bus->id}";

		  $alias->id_component = $this->ComponentID;
		  $alias->action = 'b_show';
		  $alias->parameters = Utils::url_slug($bus->company_name) . "-{$bus->id}";
		  $res = $pages_md->savePageAlias($alias);
		  }

		  } */

		/* foreach ($businesses as $bus) {
		  $target = DOCROOT . MEDIA_ROOT . DS . 'businesses';

		  if (file_exists($target . DS . $bus->image)) {
		  Utils::createDirectory($target . DS . $bus->id_category);
		  rename($target . DS . $bus->image, $target . DS . $bus->id_category . DS . $bus->image);
		  }


		  $add_img = $this->model->getBusinessImages(100, 0, "id_business = {$bus->id}");
		  foreach ($add_img as $img) {
		  $target1 = DOCROOT . MEDIA_ROOT . DS . 'businesses' . DS . 'additional';
		  $target2 = DOCROOT . MEDIA_ROOT . DS . 'businesses';
		  if (file_exists($target1 . DS . $img->image)) {
		  Utils::createDirectory($target2 . DS . $bus->id_category . DS . 'additional');
		  rename($target1 . DS . $img->image, $target2 . DS . $bus->id_category . DS . 'additional' . DS . $img->image);
		  }
		  }
		  } */

		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddBusiness]', $this->getActionUrl('business_edit')));

		$enabledSelectFil = new SelectList('status');
		$enabledSelectFil->setSelectLabel('label');
		$enabledSelectFil->setSelectValue('value');
		$enabs = array('enabled' => '[$Enabled]', 'disabled' => '[$Disabled]');
		foreach ($enabs as $key => $type) {
			$element = new stdClass();
			$element->label = $type;
			$element->value = $key;
			$enabledSelectFil->addElement($element);
		}
		$enabledSelectFil->setDefault('[$Status]');
		$this->view->set('enabledSelectFil', $enabledSelectFil);

		$profileSelectFil = new SelectList('id_profile');
		$profileSelectFil->setSelectLabel('profile');
		$profileSelectFil->setSelectValue('id');

		$profiles = $this->model->getProfiles();
		foreach ($profiles as $profile) {
			$profileSelectFil->addElement($profile);
		}
		$profileSelectFil->setDefault('[$Profile]');
		$this->view->set('profileSelectFil', $profileSelectFil);
		$this->view->BreadCrumb->addDir('[$Businesses]', $this->getActionUrl('businesses_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('businesses', $businesses);
		$this->view->setTitle('[$Businesses]');
		$this->view->render('businesses-list');
	}

	public function business_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Businesses]', $this->getActionUrl('businesses_list'));
		if (!is_null($id)) {
			$business = $this->model->getBusiness($id);
			HeadHTML::setTitleTag('Edit Business - '.$business->company_name.' | '.CMSSettings::$website_title);
			$subdomain = $this->model->getSubdomainByBizId($id);
			if ($subdomain) {
				$business->subdomain = $subdomain->subdomain;
			} else {
				require_once DOCROOT.ENTITIES_PATH.'Business/Subdomain.php';
				$subdomain = new Subdomain_Entity;
				$subdomain->target_id = $business->id;
				$subdomain->meta_description = $business->description;
				$subdomain->type = 'B';
			}
		} else {
			$business = new Business_Entity();
			HeadHTML::setTitleTag('New Business'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$NewBusiness]', $this->getActionUrl('business_edit'));

			$subdomain = new Subdomain_Entity;
			$subdomain->type = 'B';
		}

		$logged_user = UserAuth::getLoginSession();
		if ($business === false) {
			$this->AddError("Business $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['company_name'] == '') ? $this->AddError('Company Name is required!') : '';
			($_POST['id_category'] == '') ? $this->AddError('Category is required!') : '';
			($_POST['publish_date'] == '') ? $this->AddError('Publish Date is required!') : '';
			($_POST['id_profile'] == '') ? $this->AddError('Profile is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				$imagefile = '';
				if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
					$oldimage = $business->image;
					$path = $_FILES['image']['name'];
					$ext = pathinfo($path, PATHINFO_EXTENSION);
					$imagefile = time().'_'.Utils::clean($_POST['company_name']).'.'.$ext;
					$imagefile = $this->saveImage($_FILES['image']['tmp_name'], $imagefile, 'businesses');
					if ($imagefile) {
						$this->deleteImage($oldimage, 'businesses');
					}
				}

				if (!is_null($id) && $business->id_category != $_POST['id_category']) {
					$old_category = $business->id_category;
				} else {
					$old_category = null;
				}

				$this->model->startTransaction();
				$business->company_name = $_POST['company_name'];
				$business->description = $_POST['description'];
				($imagefile == '') ?: $business->image = $imagefile;
				$business->id_category = $_POST['id_category'];
				$business->publish_date = $_POST['publish_date'];
				$business->expire_date = ($_POST['expire_date'] != '') ? $_POST['expire_date'] : $business->expire_date;
				$business->location = $_POST['location'];
				$business->address = $_POST['address'];
				$business->phone = $_POST['phone'];
				$business->mobile = $_POST['mobile'];
				$business->email = $_POST['email'];
				$business->website = $_POST['website'];
				$business->coordinates = ltrim(rtrim($_POST['coordinates'], ')'), '(');
				$business->featured = isset($_POST['featured']) ? $_POST['featured'] : 0;
				$business->enabled = isset($_POST['enabled']) ? $_POST['enabled'] : 0;
				$business->id_user = (isset($_POST['id_user']) && $_POST['id_user'] != '') ? $_POST['id_user'] : $logged_user['id'];
				$business->facebook = $_POST['facebook'];
				$business->instagram = $_POST['instagram'];
				$business->linkedin = $_POST['linkedin'];
				$business->twitter = $_POST['twitter'];
				$business->google = $_POST['google'];
				$business->youtube_link = $_POST['youtube_link'];
				$business->subdomain = $_POST['subdomain'];
				$business->business_mail = $_POST['business_mail'];
				$business->id_profile = $_POST['id_profile'];
				$inserted_id = $this->model->saveBusiness($business);
				if (!is_null($id)) {
					$subdomain->subdomain = $_POST['subdomain'];
					$subdomain->target_id = $id;
					$saved_subdomain = $this->model->saveSubdomain($subdomain);
				}

				if (!is_array($inserted_id)) {
					if (isset($saved_subdomain)) {
						if (is_array($saved_subdomain)) {
							$this->AddError('Error while saving subdomain');
						} else {
							$this->AddNotice('Subdomain saved!');
						}
					}

					if (is_null($id)) {
						$business->id = $inserted_id;
					}

					if ($business->enabled == 1) {
						if ($business->notified == 0) {
							//Njoftojme userin
							$sended = $this->notifyBusinessEnabled($business);
							if ($sended) {
								$business->notified = 1;
							}
							$this->model->saveBusiness($business);
						}
					}

					//Shtojme kategorite shtese
					$errors = 0;
					if (isset($_POST['additional_categ']) && count($_POST['additional_categ'])) {
						foreach ($_POST['additional_categ'] as $categ) {
							if ($categ == $_POST['id_category']) {
								continue;
							}

							if (!is_null($id)) {
								$addCateg = $this->model->getBusinessCategories(1, 0, "id_business = $id AND id_category={$categ}");
								if (count($addCateg) == 1) {
									continue;
								}
							}

							$bc = new BusinessCateg_Entity();

							if (!is_null($id)) {
								$bc->id_business = $id;
							} else {
								$bc->id_business = $inserted_id;
							}

							$bc->id_category = $categ;

							$res = $this->model->saveBusinessCategory($bc);
							if (is_array($res)) {
								$errors++;
							}
						}

						if (!is_null($id)) {
							$additionalCategories = $this->model->getBusinessCategories(30, 0, "id_business = $id");
							foreach ($additionalCategories as $addC) {
								if (!in_array($addC->id_category, $_POST['additional_categ'])) {
									$this->model->deleteBusinessCategory($addC->id);
								}
							}
						}
					} else {
						if (!is_null($id)) {
							$additionalCategories = $this->model->getBusinessCategories(30, 0, "id_business = $id");
							foreach ($additionalCategories as $addC) {
								$this->model->deleteBusinessCategory($addC->id);
							}
						}
					}

					if ($errors == 0) {
						$this->model->commit();
						$this->AddNotice('Business has been saved successfully.');

						//Ne rastin kur ndryshojme kategorine duhet te zhvendosen imazhet
						if (!is_null($id) && !is_null($old_category)) {
							$target = DOCROOT.MEDIA_ROOT.DS.'businesses'.DS;
							Utils::createDirectory($target.$business->id_category);
							$this->moveAdditionalImages($id, $old_category, $business->id_category);
							if (!is_null($old_image)) {
								rename($target.$old_category.DS.$old_image, $target.$business->id_category.DS.$old_image);
							} elseif ($business->image != '') {
								rename($target.$old_category.DS.$business->image, $target.$business->id_category.DS.$business->image);
							}
						}

						if (!is_bool($inserted_id)) {
							$this->LogsManager->registerLog('Business', 'insert', 'Business inserted with id : '.$inserted_id, $inserted_id);
							Utils::RedirectTo($this->getActionUrl('business_edit')."/$inserted_id");
						} else {
							$this->LogsManager->registerLog('Business', 'update', 'Business updated with id : '.$id, $id);
							Utils::RedirectTo($this->getActionUrl('business_edit')."/$id");
						}

						//I dergojme email
						$message = $this->newBusinessBodyMessage($_POST);
						$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, 'noreply@'.CMSSettings::$webdomain, 'New business has been inserted', $message);

						$msg = 'I nderuar klient, biznesi juaj eshte shtuar me sukses ne faqen '.CMSSettings::$webdomain;
						Email::sendMail($_POST['email'], CMSSettings::$webdomain, 'noreply@'.CMSSettings::$webdomain, 'Biznesi juaj ne faqen '.CMSSettings::$webdomain, $msg);

						$this->LogsManager->registerLog('Business', 'insert', 'Business inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('insert_business'));
					} else {
						$this->model->rollback();
						$this->view->AddError('Problem while adding additional categories.');
					}
				} else {
					$this->AddError('Saving failed!');
					$this->model->rollback();
				}
			}
		} elseif (isset($_POST['AddImage'])) {
			if ($_FILES['images']['tmp_name'][0] == '') {
				$this->view->AddError('Please upload a file...');
			}
			$file_names = $_FILES['images']['name'];
			foreach ($file_names as $file_name) {
				(!Utils::allowedFileType($file_name)) ? $this->view->AddError('Please upload an image!') : '';
			}
			if (count($this->view->getErrors()) == 0) {
				$failures = 0;
				for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
					$path = $_FILES['images']['name'][$i];
					$ext = pathinfo($path, PATHINFO_EXTENSION);
					$imagefile = time().'_'.Utils::clean($business->company_name).'.'.$ext;
					$imagefile = $this->saveImage($_FILES['images']['tmp_name'][$i], $imagefile, 'businesses/additional');
					$addImage = new BusinessImage_Entity();
					$addImage->id_business = $id;
					$addImage->image = $imagefile;
					$addImage->size = $_FILES['images']['size'][$i];
					$res = $this->model->saveBusinessImage($addImage);
					if (!is_array($res)) {
					} else {
						$failures++;
					}
				}
				if ($failures > 0) {
					$this->AddError('There was '.$failures.' failures during image upload!');
				} else {
					$this->view->AddNotice('Image has been uploaded successfully.');
				}
			}
			Utils::RedirectTo($this->getActionUrl('business_edit')."/$id");
		} elseif (isset($_POST['AddTag'])) {
			if ($_POST['tag'] != '') {
				if ($business->tags != '') {
					$allTags = explode(';', $business->tags);
					array_push($allTags, $_POST['tag']);
				} else {
					$allTags = array($_POST['tag']);
				}
				$business->tags = implode(';', $allTags);

				$res = $this->model->saveBusiness($business);
				if (!is_array($res)) {
					$this->AddNotice('Tag added successfully.');
				} else {
					$this->AddError('Something went wrong!');
				}
			}
			Utils::RedirectTo($this->getActionUrl('business_edit')."/$id");
		} elseif (isset($_POST['submit-offer']) && !is_null($id)) {
			$this->addOffer($_POST, $id);
		}

		$categories = $this->model->getCategories(500);
		$this->view->set('categories', $categories);

		$profiles = $this->model->getProfiles(50);
		$this->view->set('profiles', $profiles);

		if (!is_null($id)) {
			$additionalImages = $this->model->getBusinessImages(50, 0, "id_business = $id");
			$this->view->set('additionalImages', $additionalImages);

			$additionalCategories = $this->model->getBusinessCategories(30, 0, "id_business = $id");
			$this->view->set('additionalCategories', $additionalCategories);
		}

		$this->view->set('ComponentSettings', $this->ComponentSettings);

		if (isset($this->ComponentSettings['currency']) && $this->ComponentSettings['currency'] != '') {
			$this->view->set('Currencies', explode(';', $this->ComponentSettings['currency']));
		} else {
			$this->view->set('Currencies', array('EUR'));
		}
		$this->view->set('business', $business);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('businesses_list'))));
		$title = (!is_null($id)) ? '[$EditBusiness]' : '[$NewBusiness]';
		$this->view->setTitle($title);
		$this->view->render('business-edit');
	}

	//Private
	private function moveAdditionalImages($id_business, $old_category, $new_category)
	{
		$additionalImages = $this->model->getBusinessImages(50, 0, "id_business = $id_business");
		$target = DOCROOT.MEDIA_ROOT.DS.'businesses'.DS;
		Utils::createDirectory($target.$new_category.DS.'additional');
		foreach ($additionalImages as $img) {
			rename($target.$old_category.DS.'additional'.DS.$img->image, $target.$new_category.DS.'additional'.DS.$img->image);
		}
	}

	public function business_details($id)
	{
		Utils::saveCurrentPageUrl();
		$business = $this->model->getBusiness($id);
		$this->view->set('business', $business);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('businesses_list'))));
		$this->view->BreadCrumb->addDir('[$Businesses]', $this->getActionUrl('businesses_list'));
		$this->view->BreadCrumb->addDir('[$BusinessDetails]', $this->getActionUrl('business_details')."/$id");
		$this->view->setTitle('[$Details]');
		$this->view->render('business-details');
	}

	public function business_delete($id)
	{
		$business = $this->model->getBusiness($id);
		$additionalImages = $this->model->getBusinessImages(50, 0, "id_business = $id");
		$result = $this->model->deleteBusiness($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'businesses'.DS.$business->id_category;
			if ($business->image != '' && file_exists($target.$business->image)) {
				unlink($target.$business->image);
			}

			foreach ($additionalImages as $image) {
				if (file_exists($target.DS.'additional'.DS.$image->image)) {
					unlink($target.DS.'additional'.DS.$image->image);
				}
			}

			$this->LogsManager->registerLog('Business', 'delete', "Business deleted with id : $id and title : ".$business->title, $id);
			$this->AddNotice('Business deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('businesses_list'));
	}

	public function business_enable($id)
	{
		if (!is_null($id)) {
			$business = $this->model->getBusiness($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$business->enabled = 1;

		if ($business->notified == 0) {
			//Njoftojme userin
			$sended = $this->notifyBusinessEnabled($business);
			if ($sended) {
				$business->notified = 1;
			}
		}

		$res = $this->model->saveBusiness($business);

		if (!is_array($res)) {
			$this->view->AddNotice('Business has been enabled successfully.');
			$this->LogsManager->registerLog('Business', 'update', 'Business enabled with id : '.$business->id, $business->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('businesses_list'));
	}

	public function business_disable($id)
	{
		if (!is_null($id)) {
			$business = $this->model->getBusiness($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$business->enabled = 0;
		$res = $this->model->saveBusiness($business);

		if (!is_array($res)) {
			$this->view->AddNotice('Business has been disabled successfully.');
			$this->LogsManager->registerLog('Business', 'update', 'Business disabled with id : '.$business->id, $business->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('businesses_list'));
	}

	public function categories_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag($this->view->getTerm('Categories').' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteCategory($id);
				$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Categories has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('categories_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'BusinessCategory.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'BusinessCategory.category', 'peso' => 90),
			);
			$categories = $this->model->searchCategory($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$categories = $this->model->getCategories($elements_per_page, $offset, $filter);
		}

		//Krijimi i aliaseve
		$pages_md = Loader::getModel('Pages');
		foreach ($categories as $bus) {
			$alias = $pages_md->getPageAliases(1, 0, "object='BusinessCategory' AND id_object={$bus->id}");
			if (count($alias) == 0) {
				$alias = new PageAlias_Entity();
				$alias->object = 'BusinessCategory';
				$alias->id_object = $bus->id;

				$alias->alias = Utils::url_slug($bus->category);

				$alias->id_component = $this->ComponentID;
				$alias->action = 'b_list';
				$alias->parameters = Utils::url_slug($bus->category)."-{$bus->id}";
				$res = $pages_md->savePageAlias($alias);
			}
		}

		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', $this->getActionUrl('category_edit')));
		$this->view->BreadCrumb->addDir($this->view->getTerm('Categories'), $this->getActionUrl('categories_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));
		if (!is_null($id)) {
			$category = $this->model->getCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$category->category.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditCategory]', $this->getActionUrl('category_edit')."/$id");
		} else {
			$category = new BusinessCategory_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddCategory]', $this->getActionUrl('category_edit'));
		}

		if ($category === false) {
			$this->AddError("Category $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['category'] == '') ? $this->AddError('Category Name is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $category->image;
				} else {
					$_POST['image'] = $category->image;
				}

				$category->category = $_POST['category'];
				$category->description = $_POST['description'];
				$category->image = $_POST['image'];
				$category->parent_id = $_POST['parent_id'];
				$category->sorting = $_POST['sorting'];
				$category->tags = $_POST['tags'];

				$inserted_id = $this->model->saveCategory($category);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Category has been saved successfully!');

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'businesses'.DS.'categories';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Category', 'insert', 'Category inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Category', 'update', 'Category updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('category', $category);

		$categories = $this->model->getCategories(50);
		$this->view->set('categories', $categories);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('categories_list'))));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id)
	{
		$category = $this->model->getCategory($id);
		$result = $this->model->deleteCategory($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'businesses'.DS.'categories';
			if ($category->image != '' && file_exists($target.$category->image)) {
				unlink($target.$category->image);
			}
			$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id, name : {$category->category}", $id);
			$this->AddNotice('Category deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('categories_list'));
	}

	public function profiles_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Profiles'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteProfile($id);
				$this->LogsManager->registerLog('Category', 'delete', "Profile deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Profiles has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('profiles_list'));
		}

		$profiles = $this->model->getProfiles($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddProfile]', $this->getActionUrl('profile_edit')));
		$this->view->BreadCrumb->addDir('[$Profiles]', $this->getActionUrl('categories_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('profiles', $profiles);
		$this->view->setTitle('[$Profiles]');
		$this->view->render('profiles-list');
	}

	public function profile_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Profiles]', $this->getActionUrl('profiles_list'));
		if (!is_null($id)) {
			$profile = $this->model->getProfile($id);
			HeadHTML::setTitleTag('Edit Profile - '.$profile->profile.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditProfile]', $this->getActionUrl('profile_edit')."/$id");
		} else {
			$profile = new BusinessProfile_Entity();
			HeadHTML::setTitleTag('New Profile'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddProfile]', $this->getActionUrl('profile_edit'));
		}

		if ($profile === false) {
			$this->AddError("Profile $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['profile'] == '') ? $this->AddError('Profile Name is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$profile->profile = $_POST['profile'];
				$profile->description = $_POST['description'];
				$profile->relevance = ($_POST['relevance'] != '') ? $_POST['relevance'] : 2;
				$profile->payment = ($_POST['payment'] != '') ? $_POST['payment'] : 0;
				$profile->max_photo = $_POST['max_photo'];
				$profile->max_video = $_POST['max_video'];
				$profile->max_offers = $_POST['max_offers'];
				$profile->show_stats = ($_POST['show_stats'] != '') ? $_POST['show_stats'] : 0;

				$inserted_id = $this->model->saveProfile($profile);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Profile has been saved successfully!');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('BusinessProfile', 'insert', 'Profile inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('profile_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('BusinessProfile', 'update', 'Profile updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('profile_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('profile', $profile);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('profiles_list'))));
		$title = (!is_null($id)) ? '[$EditProfile]' : '[$NewProfile]';
		$this->view->setTitle($title);
		$this->view->render('profile-edit');
	}

	public function profile_delete($id)
	{
		$profile = $this->model->getProfile($id);
		$result = $this->model->deleteProfile($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('BusinessProfile', 'delete', "Profile deleted with id : $id, name : {$profile->profile}", $id);
			$this->AddNotice('Profile deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('profiles_list'));
	}

	public function addtImage_delete($id)
	{
		if (!isset($_GET['id_business'])) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$image = $this->model->getBusinessImage($id);

		$res = $this->model->deleteBusinessImage($id);
		if ($res !== false) {
			$this->LogsManager->registerLog('BusinessImage', 'delete', "Image deleted with id : $id", $id);
			$this->AddNotice('Image has been deleted successfully.');
			$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'businesses'.DS.$image->id_category.DS.'additional'.DS.$image->image;
			unlink(DOCROOT.$filename);
			$this->deleteImage($image->image, 'businesses/additional');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('business_edit')."/{$image->id_business}");
	}

	public function addtImage_download($id)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('businesses_list'));
		}

		$image = $this->model->getBusinessImage($id);
		$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'businesses'.DS.$image->id_category.DS.'additional'.DS.$image->image;

		//Shkarkimi i Files
		header('Content-Type: '.mime_content_type(DOCROOT.$filename));
		header('Content-Disposition: attachment; filename="'.$image->image.'""');
		readfile(DOCROOT.$filename);
	}

	public function deleteBusinessTag($tag)
	{
		if (!isset($_GET['id_business'])) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
			return;
		}

		$business = $this->model->getBusiness($_GET['id_business']);
		$allTags = explode(';', $business->tags);

		if (($key = array_search($tag, $allTags)) !== false) {
			unset($allTags[$key]);
		}

		$business->tags = implode(';', $allTags);
		$res = $this->model->saveBusiness($business);
		if (!is_array($res)) {
			$this->AddNotice('Tag deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('article_edit')."/{$business->id}");
	}

	public function newBusinessBodyMessage($data)
	{
		$logged_user = UserAuth::getLoginSession();
		$message = 'At '.date('d/m/Y H:i').' has been inserted a new business. <br/>';

		$message .= 'Company Name '.' : '.$data['company_name'].'<br/>';
		$message .= 'Phone '.' : '.$data['phone'].'<br/>';
		$message .= 'User '.' : '.$logged_user['email'].'<br/>';
		$message .= 'Address'.' : '.$data['address'].'<br/>';
		$message .= 'Website'.' : '.$data['website'].'<br/>';
		return $message;
	}

	public function notifyBusinessEnabled($business)
	{
		//generate token
		$token = Utils::GenerateRandomPassword(10);
		//keep genereting until the token is unique
		while ($this->model->checkTokenSticker($token) != null) {
			$token = Utils::GenerateRandomPassword(10);
		}
		//create record of business
		$business_sticker = new BusinessSticker_Entity();
		$business_sticker->id_business = $business->id;
		$business_sticker->token = $token;
		$this->model->saveStickerRecord($business_sticker);

		$user_md = Loader::getModel('Users');

		$user = $user_md->getUser($business->id_user);

		//I Dergojme email userit me passwordin e resetuar.
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH.'Businesses'.DS.'views'.DS);
		$message->setLangPath(COMPONENTS_PATH.'Businesses'.DS.'lang'.DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->set('business', $business);
		$message->set('user', $user);
		$message->set('token', $business_sticker->token);
		$message->renderTemplate(false);

		$txt = $message->render('mails/email_business_online', true);

		$sended = Email::sendMail($business->email, CMSSettings::$webdomain, 'noreply@'.CMSSettings::$webdomain, $business->company_name.$this->view->getTerm('is_online'), $txt);

		if ($sended) {
			$this->view->AddNotice('The client has been notified.');
			return true;
		} else {
			$this->view->AddError('There was a problem notifing the client.');
			return false;
		}
	}

	public function subdomains_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Subdomains'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '1 ';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$subdomain = $this->model->getSubdomain($id);
				$res = $this->model->deleteSubdomain($id);

				if ($res !== false) {
					$this->LogsManager->registerLog('Subdomain', 'delete', 'Subdomain deleted with id : '.$id, $id);
				}
			}
			if ($res !== false) {
				$this->view->AddNotice('Subdomains have been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('subdomains_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'Subdomain.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'Subdomain.subdomain', 'peso' => 90),
				array('field' => TABLE_PREFIX.'Subdomain.target_url', 'peso' => 80),
			);
			$subdomains = $this->model->searchSubdomain($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$subdomains = $this->model->getSubdomains($elements_per_page, $offset, $filter, 'id DESC');
		}

		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddSubdomain]', $this->getActionUrl('subdomain_edit')));

		$this->view->BreadCrumb->addDir('[$Subdomains]', $this->getActionUrl('subdomains_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('subdomains', $subdomains);
		$this->view->setTitle('[$Subdomains]');
		$this->view->render('subdomains-list');
	}

	public function subdomain_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Subdomains]', $this->getActionUrl('subdomains_list'));
		if (!is_null($id)) {
			$subdomain = $this->model->getSubdomain($id);
			// var_dump($subdomain);exit;
			HeadHTML::setTitleTag('Edit Subdomain - '.$subdomain->subdomain.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditSubdomain]', $this->getActionUrl('subdomain_edit')."/$id");
		} else {
			$subdomain = new Subdomain_Entity();
			HeadHTML::setTitleTag('New Subdomain'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddSubdomain]', $this->getActionUrl('subdomain_edit'));
		}

		if ($subdomain === false) {
			$this->AddError("Subdomain $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['subdomain'] == '') ? $this->AddError('Subdomain Name is required!') : '';

			if ($_POST['type'] == 'B') {
				($_POST['id_business'] == '') ? $this->AddError('Business is required!') : '';
			} elseif ($_POST['type'] == 'C') {
				($_POST['id_category'] == '') ? $this->AddError('Category is required!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if ($_POST['type'] == 'B') {
					$subdomain->type = 'B';
					$subdomain->target_id = $_POST['id_business'];
				} elseif ($_POST['type'] == 'C') {
					$subdomain->type = 'C';
					$subdomain->target_id = $_POST['id_category'];
				} else {
					$subdomain->type = 'R';
					$subdomain->target_id = $_POST['target_id'];
				}

				$subdomain->subdomain = $_POST['subdomain'];
				$subdomain->meta_keywords = ($_POST['meta_keywords'] != '') ? $_POST['meta_keywords'] : '';
				$subdomain->meta_description = ($_POST['meta_desc'] != '') ? $_POST['meta_desc'] : '';
				$subdomain->target_url = (isset($_POST['target_url']) && $_POST['target_url'] != '') ? $_POST['target_url'] : $subdomain->target_url;

				$inserted_id = $this->model->saveSubdomain($subdomain);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Subdomain has been saved successfully!');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Subdomain', 'insert', 'Subdomain inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('subdomain_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Subdomain', 'update', 'Subdomain updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('subdomain_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('subdomain', $subdomain);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('subdomains_list'))));
		$title = (!is_null($id)) ? '[$EditSubdomain]' : '[$NewSubdomain]';
		$this->view->setTitle($title);
		$this->view->render('subdomain-edit');
	}

	public function subdomain_delete($id)
	{
		$profile = $this->model->getSubdomain($id);
		$result = $this->model->deleteSubdomain($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Subdomain', 'delete', "Subdomain deleted with id : $id, name : {$subdomain->subdomain}", $id);
			$this->AddNotice('Subdomain deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('subdomains_list'));
	}

	public function ajx_BusinessesSelectionList()
	{
		$Businesses_Tbl = TABLE_PREFIX.'Business';
		if (isset($_POST['query']) && $_POST['query'] != '') {
			$searchFields = array(
				array('field' => "$Businesses_Tbl.id", 'peso' => 100),
				array('field' => "$Businesses_Tbl.company_name", 'peso' => 90),
			);
			$companies = $this->model->search($_POST['query'], $searchFields, '', '', 100, 0);
		} else {
			$companies = $this->model->getList(20, 0);
		}

		if (count($companies)) {
			require_once DOCROOT.WEBROOT.VIEWS_PATH.'SelectionList_class.php';
			$table = new SelectionList_Table('Businesses');

			$table->setElements($companies);
			$table->setFields(array(
				new SelectionList_Field('id', $this->view->getTerm('Id'), 'int', 'left'),
				new SelectionList_Field('company_name', $this->view->getTerm('CompanyName'), 'string', 'left'),
			));
			$table->renderTable();
		} else {
			echo '<p class="text-center">No results</p>';
		}
	}

	public function ajx_CategoriesSelectionList()
	{
		$Categories_Tbl = TABLE_PREFIX.'BusinessCategory';
		if (isset($_POST['query']) && $_POST['query'] != '') {
			$searchFields = array(
				array('field' => "$Categories_Tbl.id", 'peso' => 100),
				array('field' => "$Categories_Tbl.category", 'peso' => 90),
			);
			$categories = $this->model->searchCategory($_POST['query'], $searchFields, '', '', 100, 0);
		} else {
			$categories = $this->model->getCategories(20, 0);
		}

		if (count($categories)) {
			require_once DOCROOT.WEBROOT.VIEWS_PATH.'SelectionList_class.php';
			$table = new SelectionList_Table('BusinessCategory');

			$table->setElements($categories);
			$table->setFields(array(
				new SelectionList_Field('id', $this->view->getTerm('Id'), 'int', 'left'),
				new SelectionList_Field('category', $this->view->getTerm('Category'), 'string', 'left'),
			));
			$table->renderTable();
		} else {
			echo '<p class="text-center">No results</p>';
		}
	}

	public function stickers_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Stickers'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = ' done ASC, id DESC ';
		$filter = " required='1' ";

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$sticker = $this->model->getSticker($id);
				$res = $this->model->deleteSticker($id);

				if ($res !== false) {
					$this->LogsManager->registerLog('Sticker', 'delete', 'Sticker deleted with id : '.$id, $id);
				}
			}
			if ($res !== false) {
				$this->view->AddNotice('Stickers have been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('stickers_list'));
		}

		$stickers = $this->model->getStickers($elements_per_page, $offset, $filter, $sorting);

		$totalElements = $this->model->getLastCounter();

		$this->view->BreadCrumb->addDir('[$Stickers]', $this->getActionUrl('stickers_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('stickers', $stickers);
		$this->view->setTitle('[$Stickers]');
		$this->view->render('stickers_list');
	}

	public function sticker_done($id)
	{
		$profile = $this->model->getSticker($id);
		$result = $this->model->doneSticker($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Sticker', 'done', "Sticker done with id : $id", $id);
			$this->AddNotice('Sticker done successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('stickers_list'));
	}

	public function sticker_undone($id)
	{
		$profile = $this->model->getSticker($id);
		$result = $this->model->undoneSticker($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Sticker', 'undone', "Sticker undone with id : $id", $id);
			$this->AddNotice('Sticker undone successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('stickers_list'));
	}

	public function comments()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Comments'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '  id DESC ';
		$filter = 'comment IS NOT NULL ';
		$comments = $this->model->getReview($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();
		$this->view->setTitle('[$Comments]');
		$this->view->BreadCrumb->addDir('[$Comments]', $this->getActionUrl('comments-list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('comments', $comments);
		$this->view->render('comments-list');
	}

	public function comment_disable($id)
	{
		if (!is_null($id)) {
			$comment = $this->model->getComment($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$comment->enabled = '0';
		$res = $this->model->saveComment($comment);
		if (!is_array($res)) {
			$this->view->AddNotice('Comment has been disabled successfully.');
			$this->LogsManager->registerLog('Comment', 'update', 'Comment disabled with id : '.$comment->id, $comment->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('comments-list'));
	}

	public function comment_enable($id)
	{
		if (!is_null($id)) {
			$comment = $this->model->getComment($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$comment->enabled = '1';
		$res = $this->model->saveComment($comment);

		if (!is_array($res)) {
			$this->view->AddNotice('Comment has been enabled successfully.');
			$this->LogsManager->registerLog('Comment', 'update', 'Comment enabled with id : '.$comment->id, $comment->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('comments-list'));
	}

	public function comment_delete($id)
	{
		$comment = $this->model->getComment($id);
		$result = $this->model->deleteComment($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Comment', 'delete', "Comment deleted with id : $id and the name : ".$coment->name, $id);
			$this->AddNotice('Business deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('comments'));
	}

	public function createMassSubdomains()
	{
		exit;
		$errors = 0;
		$domain = 'netirane.al';
		$rows = $this->model->getList(2000);
		foreach ($rows as $row) {
			$new = new Subdomain_Entity();
			$newSub = $this->createSubdomainName($row->company_name, $domain);
			$newSub = strtolower($newSub);
			$new->subdomain = $newSub;
			$row->subdomain = $newSub;
			$new->target_id = $row->id;
			$new->type = 'B';
			$new->target_url = '';
			$new->meta_keywords = str_replace(' ', ',', $row->description);
			$new->meta_description = $this->limit_text($row->description, 22).' '.$row->category.' ne Tirane ';
			if ($this->model->saveSubdomain($new)) {
				echo 'SAVED SUBDOMAIN '.$newSub.'<br><br>';
			} else {
				$errors++;
			}
			$this->model->saveBusiness($row);
			echo $errors;
		}
	}

	public function limit_text($text, $limit)
	{
		if (str_word_count($text, 0) > $limit) {
			$words = str_word_count($text, 22);
			$pos = array_keys($words);
			$text = substr($text, 0, $pos[$limit]).'...';
		}
		return $text;
	}

	public function createSubdomainName($text, $domain)
	{
		$text = trim($text);
		$text = preg_replace('/[^A-Za-z0-9\-]/', '', $text);
		$text = str_replace(' ', '-', $text);

		$newSubdomainName = $text.'.'.$domain;
		$filter = ' subdomain = '.$newSubdomainName;

		$check = $this->model->getSubdomains($limit = 1, $offset = 0, $filter);
		if (count($check) != 0) {
			$newSubdomainName = str_replace('-', '', $text).'.'.$domain;
		}

		return $newSubdomainName;
	}

	private function saveImage($tmpfile, $newfile, $folder)
	{
		$path = DS.rtrim(ltrim($folder, DS), DS).DS;
		$newfile = rand(0, 999).DS.$newfile;
		$newfile = rand(0, 999).DS.$newfile;
		if (isset($this->ComponentSettings['image_baseurl'])) {
			$this->saveImageToDb($tmpfile, $newfile, $path);
			$ch = curl_init('http://www.neshqiperi.al/com/Businesses/syncImagesFromDb');
			curl_exec($ch);
			curl_close($ch);
		} else {
			$target = DOCROOT.MEDIA_ROOT.$path;
			$target = str_replace('//', '/', $target);
			if (!is_dir(dirname($target.$newfile))) {
				mkdir(dirname($target.$newfile), 0755, true) or die('Errore durante la creazione della folder!');
			}
			move_uploaded_file($tmpfile, $target.$newfile);
		}
		return $newfile;
	}

	private function deleteImage($filename, $folder)
	{
		if (isset($this->ComponentSettings['image_baseurl'])) {
			$this->saveImageToDb(null, $filename, $folder, true);
			$ch = curl_init('http://www.neshqiperi.al/com/Businesses/syncImagesFromDb');
			curl_exec($ch);
			curl_close($ch);
		} else {
			$target = DOCROOT.MEDIA_ROOT.$folder.DS;
			if (file_exists($target.$filename)) {
				unlink($target.$filename);
			}
		}
	}

	private function saveImageToDb($file, $filename, $path, $todelete = false)
	{
		$tempImage = new TempImage_Entity();
		$tempImage->image = file_get_contents($file);
		$tempImage->filename = $filename;
		$tempImage->path = $path;
		$tempImage->delete_action = ($todelete) ? '1' : '0';
		$inserted_temp_image = $this->model->saveTempImage($tempImage);
		return $inserted_temp_image;
	}

	private function addOffer($post = array(), $id_business)
	{
		$offer = new BusinessOffer_Entity();

		$offer->id_biz = $id_business;
		$offer->title = $post['offer-title'];
		$offer->description = $post['description'];
		$offer->creation_date = date('Y-n-d H:i:s', time());
		$offer->expiration_date = $post['offer-expiration'];
		$offer->price = $post['offer-price'];
		$offer->final_price = $post['offer-final-price'];

		$saved_offer = $this->model->saveOffer($offer);

		if (!is_array($saved_offer)) {
			return $saved_offer;
		} else {
			return false;
		}
	}
}
