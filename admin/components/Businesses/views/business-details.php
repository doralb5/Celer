<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-5">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$EventDetails] - <span class=""><?= $business->company_name ?></span></h4>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered personalinfo">
                    <tbody>
                    <tr>
                        <td>[$Id]</td>
                        <td><b><?= $business->id ?></b></td>
                    </tr>
                    <tr>
                        <td>[$CompanyName]</td>
                        <td><b><?= $business->company_name ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Image]</td>
                        <td><?php if ($business->image != '') {
	?>
                                <img
                                    src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'businesses'.DS.$business->id_category.DS.$business->image ?>"
                                    alt="Image" width="100px"/>
                            <?php
} else {
		?>
                                No Image
                            <?php
	} ?>
                        </td>
                    </tr>
                    <tr>
                        <td>[$Category]</td>
                        <td><b><?= $business->category ?></b></td>
                    </tr>
                    <tr>
                        <td>[$CreationDate]</td>
                        <td><?= date('d/m/Y H:i', strtotime($business->creation_date)) ?></td>
                    </tr>
                    <tr>
                        <td>[$PublishDate]</td>
                        <td><?= date('d/m/Y H:i', strtotime($business->publish_date)) ?></td>
                    </tr>
                    <tr>
                        <td>[$ExpireDate]</td>
                        <td><?= (is_null($business->expire_date)) ? 'No Expiration' : date('d/m/Y H:i', strtotime($business->expire_date)) ?></td>
                    </tr>
                    <tr>
                        <td>[$Location]</td>
                        <td><b><?= $business->location ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Address]</td>
                        <td><b><?= $business->address ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Phone]</td>
                        <td><b><?= $business->phone ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Mobile]</td>
                        <td><b><?= $business->mobile ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Email]</td>
                        <td><b><?= $business->email ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Website]</td>
                        <td><b><?= $business->website ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Featured]</td>
                        <td><?= ($business->featured == '1') ? '<i class="fa fa-check-square" style="color:green"></i>' : '<i class="fa fa-square-o" style="color:red"></i>' ?></td>
                    </tr>
                    <tr>
                        <td>[$Enabled]</td>
                        <td><?= ($business->enabled == '1') ? '<i class="fa fa-check-square" style="color:green"></i>' : '<i class="fa fa-square-o" style="color:red"></i>' ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <p>[$Description]</p>

            <div class="col-md-12">
                <?= $business->description ?>
            </div>
            <p>&nbsp;</p>

            <div class="col-md-12 text-right">
                <a class="btn btn-danger" href="<?= $url_business_delete."/{$business->id}" ?>"><i
                        class="fa fa-minus-circle"></i>&nbsp;&nbsp;[$Delete]</a>
            </div>
            <p>&nbsp;</p>
        </div>
    </div>
</div>