<?php
HeadHTML::AddStylesheet(substr(WEBROOT, 0, -6).COMMONS_PATH.'css/select2.min.css');
HeadHTML::AddStylesheet(substr(WEBROOT, 0, -6).COMMONS_PATH.'css/bootstrap-switch.min.css');
HeadHTML::AddJS(substr(WEBROOT, 0, -6).COMMONS_PATH.'js/select2.full.min.js');
HeadHTML::AddJS(substr(WEBROOT, 0, -6).COMMONS_PATH.'js/bootstrap-switch.min.js');
$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';
$company_name = isset($_POST['company_name']) ? $_POST['company_name'] : htmlentities($business->company_name);
$description = isset($_POST['description']) ? $_POST['description'] : $business->description;
$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $business->id_category;
$category = isset($_POST['category']) ? $_POST['category'] : $business->category;
$publish_date = isset($_POST['publish_date']) ? $_POST['publish_date'] : ((!is_null($business->publish_date) ? $business->publish_date : date('Y-m-d H:i')));
$expire_date = isset($_POST['expire_date']) ? $_POST['expire_date'] : ((!is_null($business->expire_date)) ? $business->expire_date : date('Y-m-d H:i', strtotime(date('Y-m-d  H:i', time()).' + 365 day')));
$location = isset($_POST['location']) ? $_POST['location'] : $business->location;
$address = isset($_POST['address']) ? $_POST['address'] : htmlentities($business->address);
$phone = isset($_POST['phone']) ? $_POST['phone'] : $business->phone;
$mobile = isset($_POST['mobile']) ? $_POST['mobile'] : $business->mobile;
$email = isset($_POST['email']) ? $_POST['email'] : $business->email;
$website = isset($_POST['website']) ? $_POST['website'] : $business->website;
$coordinates = isset($_POST['coordinates']) ? ltrim(rtrim($_POST['coordinates'], ')'), '(') : $business->coordinates;
$featured = isset($_POST['featured']) ? $_POST['featured'] : $business->featured;
$enabled = isset($_POST['enabled']) ? $_POST['enabled'] : $business->enabled;
$facebook = isset($_POST['facebook']) ? $_POST['facebook'] : $business->facebook;
$instagram = isset($_POST['instagram']) ? $_POST['instagram'] : $business->instagram;
$linkedin = isset($_POST['linkedin']) ? $_POST['linkedin'] : $business->linkedin;
$twitter = isset($_POST['twitter']) ? $_POST['twitter'] : $business->twitter;
$google = isset($_POST['google']) ? $_POST['google'] : $business->google;
$subdomain = isset($_POST['subdomain']) ? $_POST['subdomain'] : $business->subdomain;
$business_mail = isset($_POST['business_mail']) ? $_POST['business_mail'] : $business->business_mail;
$youtube_link = isset($_POST['youtube_link']) ? $_POST['youtube_link'] : $business->youtube_link;

$id_user = isset($_POST['id_user']) ? $_POST['id_user'] : $business->id_user;
$user = isset($_POST['user']) ? $_POST['user'] : $business->user_fullname;

$id_profile = isset($_POST['id_profile']) ? $_POST['id_profile'] : $business->id_profile;

$add_ids = array();

if (!is_null($business->id)) {
	foreach ($additionalCategories as $addCateg) {
		array_push($add_ids, $addCateg->id_category);
	}
}

if (isset($_POST['additional_categ'])) {
	if (is_array($_POST['additional_categ'])) {
		foreach ($_POST['additional_categ'] as $add_categ) {
			array_push($add_ids, $add_categ);
		}
	} else {
		array_push($add_ids, $_POST['additional_categ']);
	}
}
?>

<?php


if (isset($ComponentSettings['google_key']) && $ComponentSettings['google_key'] != '') {
	HeadHtml::AddJS('https://maps.googleapis.com/maps/api/js?key='.$ComponentSettings['google_key']);
} elseif (isset(CMSSettings::$google_maps_api) && CMSSettings::$google_maps_api != '') {
	HeadHtml::AddJS(Utils::googleMapsJsLink());
} else {
	HeadHtml::AddJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyBEiE-_79eSxQl4b1tdrEichm8vTLw4VC0');
}

//if (isset(CMSSettings::$google_maps_api) && CMSSettings::$google_maps_api != '') {
//    HeadHtml::linkJS(Utils::googleMapsJsLink());
//} else {
//    if (CMSSettings::$webdomain == 'nedurres.al') {
//        HeadHtml::linkJS("https://maps.googleapis.com/maps/api/js?key=AIzaSyB_Ruwh4QeCSuo2Ph1QgfWEXz9mnq1yu5Y");
//    } elseif (CMSSettings::$webdomain == 'neshkoder.al') {
//        HeadHtml::linkJS("https://maps.googleapis.com/maps/api/js?key=AIzaSyBjDARxGr0bq4V8G7qDTZRCZ2iv5yZa0ac");
//    } elseif (CMSSettings::$webdomain == 'nefier.al') {
//        HeadHtml::linkJS("https://maps.googleapis.com/maps/api/js?key=AIzaSyDLxL9Y1TonGXJ0jcsBSpIzZXQUt_-Zmnk");
//    } elseif (CMSSettings::$webdomain == 'nesarande.al') {
//        HeadHtml::linkJS("https://maps.googleapis.com/maps/api/js?key=AIzaSyC5qJvNoPN6ghwWq2BA3LQN4jeO1YpLEbE");
//    } elseif (CMSSettings::$webdomain == 'nevlore.al') {
//        HeadHtml::linkJS("https://maps.googleapis.com/maps/api/js?key=AIzaSyDjUC6pdw4b3tDf_wQIwRGmC1v9oJHQqow");
//    } elseif (CMSSettings::$webdomain == 'neelbasan.al') {
//        HeadHtml::linkJS("https://maps.googleapis.com/maps/api/js?key=AIzaSyCbrBjadcJFRjq4GyZFf67LxsJCa87bRw0");
//    } elseif (CMSSettings::$webdomain == 'nepogradec.al') {
//        HeadHtml::linkJS("https://maps.googleapis.com/maps/api/js?key=AIzaSyBEiE-_79eSxQl4b1tdrEichm8vTLw4VC0");
//    } elseif (CMSSettings::$webdomain == 'nelibrazhd.al') {
//        HeadHtml::linkJS("https://maps.googleapis.com/maps/api/js?key=AIzaSyAG1UVpwXrXECsvjEW8wmsPSjOQSR9kQMc");
//    } else {
//        HeadHtml::linkJS("https://maps.googleapis.com/maps/api/js?key=AIzaSyBEiE-_79eSxQl4b1tdrEichm8vTLw4VC0");
//    }
//}
?>

<div class="col-md-12">
    <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
          novalidate="" enctype="multipart/form-data" id="insert-business">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$GeneralInformation]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$CompanyName] *</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $company_name ?>"
                                       name="company_name" placeholder="[$CompanyName]" data-parsley-required="true"
                                       data-parsley-id="6524" required>
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Category]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_category" required>
                                    <option value=""></option>
<?php foreach ($categories as $categ) {
	?>
                                        <option
                                            value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
<?php
} ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$AdditionalCategories]</label>

                            <div class="col-md-8 col-sm-8">

                                <select multiple class="form-control" id="additional_categ" name="additional_categ[]">
<?php foreach ($categories as $categ) {
		?>
                                        <option
                                            value="<?= $categ->id ?>" <?= (in_array($categ->id, $add_ids)) ? 'selected' : '' ?>><?= $categ->category ?></option>
<?php
	} ?>
                                </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                            <div class="col-md-6 col-sm-6">
                                <input type="file" name="image">
<?php if ($business->image != '') {
		?>
                                    <p>
                                        <img
                                            src="<?= Utils::genThumbnailUrl('businesses/'.$business->image, 800, 500, array(), true, $image_baseurl) ?>"
                                            width="100px" alt="Image"/></p>
<?php
	} ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Profile]</label>

                            <div class="col-md-8 col-sm-8">
                                <select name="id_profile" class="form-control">
                                    <?php foreach ($profiles as $profile) {
		?>
                                        <option
                                            value="<?= $profile->id ?>" <?= ($profile->id == $id_profile) ? 'selected' : '' ?>><?= $profile->profile ?></option>
<?php
	} ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Featured]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="featured" <?= ($featured == '1') ? 'checked' : '' ?>
                                       id="featured-switch">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Enabled]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="enabled" <?= ($enabled == '1') ? 'checked' : '' ?>
                                       id="enabled-switch">
                            </div>
                        </div>
                    </div>
                </div>


                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$Content]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <textarea class="ckeditor" id="editor1" name="description"><?= $description ?></textarea>
                    </div>
                </div>
                <!-- end panel -->

            </div>

            <div class="col-md-6">


                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$AdditionalInformation]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$User]</label>

                            <div class="col-md-8 col-sm-8">
                                <div class="input-group">
                                    <input readonly id="user" name="user" type="text" class="form-control"
                                           placeholder="Select User" value="<?= $user ?>">
                                    <input type="hidden" id="id_user" name="id_user" value="<?= $id_user ?>"/>

                                    <div class="input-group-btn">
                                        <a title="[$Select]" class="btn btn-primary" data-toggle="modal"
                                           href="#myModal_Users">...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$PublishDate]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control" name="publish_date" id="datetimepicker1"
                                       placeholder="[$SelectDate]" value="<?= $publish_date ?>" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$EndDate]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control" name="expire_date" id="datetimepicker2"
                                       placeholder="[$SelectDate]" value="<?= $expire_date ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Phone]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="tel" value="<?= $phone ?>"
                                       name="phone" placeholder="[$Phone]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Mobile]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="tel" value="<?= $mobile ?>"
                                       name="mobile" placeholder="[$Mobile]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Email]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="email" value="<?= $email ?>"
                                       name="email" placeholder="[$Email]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Website]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $website ?>"
                                       name="website" placeholder="[$Website]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Facebook]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $facebook ?>"
                                       name="facebook" placeholder="[$Facebook]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Instagram]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $instagram ?>"
                                       name="instagram" placeholder="[$Instagram]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Linkedin]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $linkedin ?>"
                                       name="linkedin" placeholder="[$Linkedin]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Twitter]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $twitter ?>"
                                       name="twitter" placeholder="[$Twitter]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$google]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $google ?>"
                                       name="google" placeholder="[$Google]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$youtube_link]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $youtube_link ?>"
                                       name="youtube_link" placeholder="[$youtube_link]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Subdomain]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $subdomain ?>"
                                       name="subdomain" placeholder="[$Subdomain]">
                                <input type="hidden" name="domain" value="netirane.al">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$BusinessMail]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="email" value="<?= $business_mail ?>"
                                       name="business_mail" placeholder="[$BusinessMail]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Location]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $location ?>"
                                       name="location" placeholder="[$Location]" id="location"
                                       onblur="setMarker('Location');">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Address]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $address ?>"
                                       name="address" placeholder="[$Address]" id="address"
                                       onblur="setMarker('Address');">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel -->

                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$Map]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div id="map" style="width: 100%; height: 250px;"></div>
                        <input type="hidden" id="coordinates" name="coordinates"
                               value="<?= ($coordinates != '') ? "($coordinates)" : '' ?>">
                    </div>
                </div>
            </div>
        </div>

</div>
<div class="col-md-12">
    <p class="text-center">
        <button type="submit" name="save" class="btn btn-info">
            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
        </button>
    </p>
</div>
<p>&nbsp;</p>
</form>


<?php if (!is_null($business->id)) {
		?>

    <div class="col-md-8">

        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$AdditionalImages]</h4>
            </div>
            <div class="panel-body">

                <p>&nbsp;</p>
    <?php
	include(VIEWS_PATH.'TableList_class.php');
		$table = new TableListView('Articles', 'list');
		foreach ($additionalImages as &$row) {
			$row->rows_buttons = array(
			new Button('<i class="fa fa-eye"></i>', $url_addtImage_show.'/'.$row->id_category.'/additional/'.$row->image, 'xs', '', '[$Show]', '_blank'),
			new Button('<i class="fa fa-download"></i>', $url_addtImage_download.'/'.$row->id, 'xs', '', '[$Download]')
		);

			$row->row_imagePath = ($image_baseurl == '') ? substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.DS.'businesses'.DS.'additional' : $image_baseurl.'/businesses/additional';
		}

		$table->setElements($additionalImages);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
		new TableList_Field('id', '[$Id]', 'int', 'left'),
		new TableList_Field('image', '[$Filename]', 'string', 'left'),
		new TableList_Field('image', '[$Image]', 'image', 'left'),
	));
		$table->setUrl_delete($url_addtImage_delete);
		$table->setUrl_delete_params(array('id_business' => $business->id));
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->render(); ?>


                <div class="col-md-12">
                    <form id="fileupload" method="POST" enctype="multipart/form-data">
                        <div class="form-group col-md-9">
                            <label>[$SelectFile]</label>

                            <div class="box">
                                <input type="file" name="images[]" id="file-7" class="inputfile inputfile-6"
                                       data-multiple-caption="{count} files selected" multiple/>
                                <label for="file-7"><span></span><strong>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                             viewBox="0 0 20 17">
                                        <path
                                            d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                        </svg>
                                        [$Choose]&hellip;</strong></label>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <br>
                            <button class="btn btn-info" name="AddImage" type="submit"><i class="fa fa-plus"></i>&nbsp;&nbsp;[$AddFile]
                            </button>

                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>

    <div class="col-md-4">

        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$AddTags]</h4>
            </div>
            <div class="panel-body panel-form">

                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true"
                      name="demo-form">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="tag"/>

                                    <div class="input-group-btn">
                                        <button type="submit" name="AddTag" class="btn btn-success"><i
                                                class="fa fa-plus"></i>&nbsp;&nbsp;[$Add]
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
    <?php
	if ($business->tags != '') {
		$tags = explode(';', $business->tags);
		foreach ($tags as $tag) {
			?>
                                <span class="label label-primary" style="font-size: 12px"><?= $tag ?>
                                    <a class="btn btn-md btn-toolbar"
                                       href="<?= Utils::getComponentUrl("Businesses/deleteBusinessTag/$tag?id_business={$business->id}") ?>"><i
                                            class="fa fa-times"></i></a>
                                </span>&nbsp;
                            <?php
		}
	} ?>
                    </div>
                </form>

            </div>
        </div>
        <!-- end panel -->


    </div>

<?php
	} ?>

</div>


<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>

<script>

    CKEDITOR.replace('editor1', {
        imageBrowser_listUrl: "<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).LIBS_PATH ?>JsonDirImages.php?media_path=<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).MEDIA_ROOT ?>&docroot=<?= rtrim(DOCROOT, '/') ?>",
                height: 232
            });
</script>

<script>
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker();
    });
</script>


<?php
if ($coordinates != '' && strpos($coordinates, ',') !== false) {
	list($lat, $lng) = explode(',', $coordinates);
} else {
	$lat = (isset($ComponentSettings['map_lat'])) ? $ComponentSettings['map_lat'] : '38.900413';
	$lng = (isset($ComponentSettings['map_lng'])) ? $ComponentSettings['map_lng'] : '16.586658';
}
require_once DOCROOT.WEBROOT.VIEWS_PATH.'SelectionList_class.php';
$SL3 = new SelectionListView('Users');
$SL3->setTitle('Choose a user');
$SL3->setAjxUrl(Utils::getControllerUrl('Users/ajx_UsersSelectionList'));
$SL3->setDestinationField('fullname');
$SL3->setDestTextSelector('user');
$SL3->setDestIdSelector('id_user');
$SL3->render();
?>
<script type="text/javascript">

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: {lat: <?= $lat ?>, lng: <?= $lng ?>}
    });

    $('#coordinates').val("(<?= $lat ?>, <?= $lng ?>)");
    var geocoder = new google.maps.Geocoder();
    var marker;

    setMarker();
    function setMarker(destination) {

        if (marker != null)
            marker.setMap(null);

        var address = document.getElementById('location').value + ', ' + document.getElementById('address').value;
        geocodeAddress(address, geocoder, map, destination);
        if (document.getElementById('address').value != '')
            map.setZoom(15);
        else
            map.setZoom(8);

    }


    function geocodeAddress(address, geocoder, resultsMap, destination) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    draggable: true,
                    animation: google.maps.Animation.DROP,

                });

                google.maps.event.addListener(marker, 'dragend', function () {
                    geocodePosition(marker.getPosition());
                });

                $('#coordinates').val(results[0].geometry.location);
                console.log($('#coordinates').val());
            } else {
                $("#warning").modal();
            }
        });
    }

    function geocodePosition(pos) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode
                ({
                    latLng: pos
                },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                $("#address").val(results[0].formatted_address);
                                $('#coordinates').val(results[0].geometry.location.toString());
                            } else {
                                $("#warning").modal();
                                //$("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
                            }
                        }
                );
    }


    var checkbusinesscount = 1, prevTarget;
    $('.modal').on('show.bs.modal', function (e) {
        if (typeof prevTarget == 'undefined' || (checkbusinesscount == 1 && e.target != prevTarget)) {
            prevTarget = e.target;
            checkbusinesscount++;
            e.prbusinessDefault();
            $(e.target).appendTo('body').modal('show');
        } else if (e.target == prevTarget && checkbusinesscount == 2) {
            checkbusinesscount--;
        }
    });


</script>

<style>
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
    }

    .inputfile:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .box {
        padding: 0px;
    }
</style>
<script>

    $("#additional_categ").select2();
    $("#id_category").select2();

    $("#enabled-switch").bootstrapSwitch();
    $("#featured-switch").bootstrapSwitch();

    $('#insert-business').submit(function (evt) {

        var val = $('#category').val();
        var categ_id = $('#categories option').filter(function () {
            return this.value == val;
        }).data('id');

        if (categ_id !== null && categ_id != '') {
            $('#category-id').val(categ_id);
        } else {
            categ_id = $('#category-id').val();
        }

    });

    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
                labelVal = label.innerHTML;
        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();
            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });</script>