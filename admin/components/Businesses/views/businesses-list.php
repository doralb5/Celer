<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Businesses');

$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';
foreach ($businesses as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_business_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_business_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_business_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		//new Button('<i class="fa fa-eye"></i>', $url_business_details . '/' . $row->id, 'xs', '', '[$Details]'),
	);
	$imgtype = ($image_baseurl == '') ? 'image' : 'extimg';
	$row->image_base_url = $image_baseurl;
	$row->row_imagePath = 'businesses/';
	if ($row->featured) {
		$row_code = '<i style="color: #9FEE00" class="fa fa-star fa-2x"></i>';
	} else {
		$row_code = '<i class="fa fa-star-o fa-2x"></i>';
	}

	$row->additional_rows = array('<i class="fa fa-star-o"></i>' => $row_code);
}
$table->setElements($businesses);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('company_name', '[$Company]', 'text', 'left'),
	new TableList_Field('image', '[$Image]', $imgtype, 'left'),
	new TableList_Field('category', '[$Category]', 'string', 'left'),
	new TableList_Field('profile', '[$Profile]', 'string', 'left'),
	new TableList_Field('enabled', '<i class="fa fa-square-o"></i>', 'bool', 'left', 'no-sort'),
));
$table->setUrl_action($url_businesses_list);
$table->setUrl_delete($url_business_delete);
$table->addSelectFilter($enabledSelectFil);
$table->addSelectFilter($profileSelectFil);
$table->render();
