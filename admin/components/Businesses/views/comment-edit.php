<?php

HeadHTML::AddStylesheet(substr(WEBROOT, 0, -6).COMMONS_PATH.'css/bootstrap-tagsinput.css');
HeadHTML::AddJS(substr(WEBROOT, 0, -6).COMMONS_PATH.'js/bootstrap-tagsinput.min.js');
HeadHTML::AddJS(substr(WEBROOT, 0, -6).COMMONS_PATH.'js/bootstrap-tagsinput.min.js.map');

$comment_id = isset($_POST['id']) ? $_POST['id'] : $comment->id;
$comment_name = isset($_POST['name']) ? $_POST['name'] : $comment->name;
$comment_c = isset($_POST['comment']) ? $_POST['comment'] : $comment->comment;
$comment_email = isset($_POST['email']) ? $_POST['email'] : $comment->email;
?>
<div class="col-md-8">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$Comment]</h4>
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
                  id="add-category"
                  novalidate="" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Name] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $category_name ?>"
                               name="category" placeholder="[$Name]" data-parsley-required="true"
                               data-parsley-id="6524" required>
                        <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Description]</label>

                    <div class="col-md-8 col-sm-8">
                            <textarea name="description" class="form-control" rows="3"
                                      style="max-width: 100%; min-width: 100%"><?= $description ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                    <div class="col-md-6 col-sm-6">
                        <input type="file" name="image">
                        <?php if ($category->image != '') {
	?>
                            <p>
                                <img
                                    src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'events/categories/'.$category->image ?>"
                                    width="100px" alt="Image"/></p>
                        <?php
} ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$ParentCategory]</label>

                    <div class="col-md-8 col-sm-8">
                        <span class="label label-warning">Carefully!</span>
                        <select class="form-control" name="id_category">
                            <option>--[$NoParent]--</option>
                            <?php foreach ($categories as $categ) {
		if ($categ->id == $category->id) {
			continue;
		} ?>
                                <option
                                    value="<?= $categ->id ?>" <?= ($categ->id == $parent_id) ? 'selected' : '' ?>><?= $categ->category ?></option>
                            <?php
	} ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Sorting]</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="number" value="<?= $sorting ?>"
                               name="sorting" placeholder="[$Sorting]">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">Tags</label>
                    <div class="col-md-8 col-sm-8">
                        <input type="text" value="<?= $tags ?>" data-role="tagsinput" id="tags" name="tags"/>
                    </div>
                </div>

                <div class="form-group">
                    <p>&nbsp;</p>
                    <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                    <p>&nbsp;</p>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('#add-category').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
</script>
