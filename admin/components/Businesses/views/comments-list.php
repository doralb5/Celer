<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('BusinessRating');
foreach ($comments as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_comment_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_comment_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array($btn1);
}
$table->setElements($comments);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('id_biz', '[$Id_b]', 'int', 'left'),
	new TableList_Field('company_name', '[$Company_Name]', 'string', 'left'),
	new TableList_Field('rating', '[$Rating]', 'int', 'left'),
	new TableList_Field('comment', '[$Comment]', 'string', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('email', '[$Email]', 'string', 'left'),
	new TableList_Field('enabled', '<i class="fa fa-square-o"></i>', 'bool', 'left', 'no-sort'),
));

$table->deleteOption(false);
$table->multipleDeletion(false);
$table->setUrl_delete($url_comment_delete);
$table->render();
