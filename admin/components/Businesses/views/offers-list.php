<div id="owl-offers" class="owl-carousel">
<?php if (!is_null($offers)) {
	?>
    <?php $taglia_offer_content = (isset($parameters['taglia_offer_content'])) ? $parameters['taglia_offer_content'] : 125; ?>
    <!-- Ofertat -->
    <?php foreach ($offers as $offer) {
		?>

        <div id="carousel-example-generic-<?= $offer->id ?>" class="carousel slide offers-carousel" data-ride="carousel" data-interval="false">
            <ol class="carousel-indicators">
                <?php for ($i = 0; $i < 5 && $i < count($offer->images); $i++) {
			?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?= $i ?>" class="<?php if ($i == 0) {
				echo 'active';
			} ?>"></li>
                <?php
		} ?>
            </ol>
            <div class="carousel-inner">
                <?php for ($i = 0; $i < 5 && $i < count($offer->images); $i++) {
			?>
                    <div class="item <?php if ($i == 0) {
				echo 'active';
			} ?> srle">
                        <img src="<?= Utils::genThumbnailUrl('offers/'.$business->id.DS.$offer->images[$i]->image, 500, 300) ?>" alt="1.jpg" class="img-responsive">
                    </div>
                <?php
		} ?>
            </div>

            <a class="left carousel-control" href="#carousel-example-generic-<?= $offer->id ?>" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic-<?= $offer->id ?>" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>

            <ul class="thumbnails-carousel clearfix"></ul>

            <div class="alldiv-text">
                <p class="main-comune"></p>
                <h5 class="main-title1">
                    <a title=""><?= $offer->title ?></a>
                </h5>
                <p></p>
                <div class="location-height">
                    <p class="main-addres">
                        <?= StringUtils::CutString(strip_tags($offer->description), $taglia_offer_content) ?>
                    </p>
                </div>
                <div class="offer-price">
                    <?php if ($offer->final_price != '') {
			?>
                        <div class="price"><?= $offer->price ?>&nbsp;<?= $offer->currency ?></div>
                        <div class="final-price"><?= $offer->final_price ?>&nbsp;<?= $offer->currency ?></div>
                    <?php
		} else {
			?>
                        <span class="only-price"><?= $offer->price ?>&nbsp;<?= $offer->currency ?></span>                                                   
                    <?php
		} ?>
                </div>
                <span style=" float: right; ">
                    <a class="btn btn-xs btn-default bussiness-comments" data-toggle="modal" data-target="#BusinessOffer">Me Shume</a>
                </span>                                      
            </div>
        </div>
    <?php
	} ?>
<?php
} ?>