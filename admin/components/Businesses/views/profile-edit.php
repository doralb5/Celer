<?php
$profile_name = isset($_POST['profile']) ? $_POST['profile'] : $profile->profile;
$description = isset($_POST['description']) ? $_POST['description'] : $profile->description;
$relevance = isset($_POST['relevance']) ? $_POST['relevance'] : $profile->relevance;
$payment = isset($_POST['payment']) ? $_POST['payment'] : $profile->payment;
$max_photo = isset($_POST['max_photo']) ? $_POST['max_photo'] : $profile->max_photo;
$max_video = isset($_POST['max_video']) ? $_POST['max_video'] : $profile->max_video;
$max_offers = isset($_POST['max_offers']) ? $_POST['max_offers'] : $profile->max_offers;
$show_stats = isset($_POST['show_stats']) ? $_POST['show_stats'] : $profile->show_stats;

?>
<div class="col-md-8">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$Category]</h4>
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
                  novalidate="" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Name] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $profile_name ?>"
                               name="profile" placeholder="[$Name]" data-parsley-required="true"
                               data-parsley-id="6524" required>
                        <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Description]</label>

                    <div class="col-md-8 col-sm-8">
                            <textarea name="description" class="form-control" rows="3"
                                      style="max-width: 100%; min-width: 100%"><?= $description ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Relevance] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $relevance ?>"
                               name="relevance" placeholder="[$Relevance]">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Max_Photo] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $max_photo ?>"
                               name="max_photo" placeholder="[$Max_Photo]">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Max_Video] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $max_video ?>"
                               name="max_video" placeholder="[$Max_Video]">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Max_Offers] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $max_offers ?>"
                               name="max_offers" placeholder="[$Max_Offers]">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Show_Stats]</label>

                    <div class="col-md-8 col-sm-8">
                        <select name="payment" class="form-control">
                            <option value="1" <?= ($show_stats == '1') ? 'selected' : '' ?>>[$yes_option]</option>
                            <option value="0" <?= ($show_stats == '0') ? 'selected' : '' ?>>[$no_option]</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Payment]</label>

                    <div class="col-md-8 col-sm-8">
                        <select name="payment" class="form-control">
                            <option value="1" <?= ($payment == '1') ? 'selected' : '' ?>>[$yes_option]</option>
                            <option value="0" <?= ($payment == '0') ? 'selected' : '' ?>>[$no_option]</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <p>&nbsp;</p>
                    <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                    <p>&nbsp;</p>
                </div>
            </form>
        </div>
    </div>
</div>
