<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Businesses');

foreach ($profiles as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_profile_edit.'/'.$row->id, 'xs', '', '[$Edit]')
	);
}
$table->setElements($profiles);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('profile', '[$Name]', 'string', 'left'),
	new TableList_Field('relevance', '[$Relevance]', 'string', 'left'),
	new TableList_Field('description', '[$Description]', 'text', 'left'),
));
$table->setUrl_action($url_profiles_list);
$table->setUrl_delete($url_profile_delete);

$table->multipleDeletion(false);
$table->renderTopBar(false);
$table->render();
