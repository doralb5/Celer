<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Businesses');

foreach ($stickers as &$row) {
	if ($row->done == 0) {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_sticker_done.'/'.$row->id, 'xs', 'btn-success', '[$Done]');
	} else {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_sticker_undone.'/'.$row->id, 'xs', 'btn-warning', '[$Done]');
	}
	$row->rows_buttons = array($btn1);
}

$table->setElements($stickers);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('id_business', '[$Id_business]', 'int', 'left'),
	new TableList_Field('company_name', '[$Company_Name]', 'string', 'left'),
	new TableList_Field('done', '[$done]', 'int', 'left'),
));
$table->setUrl_action($url_stickers_list);
$table->setUrl_delete($url_sticker_done);
$table->setUrl_delete($url_sticker_undone);
$table->deleteOption(false);
$table->multipleDeletion(false);
$table->render();
