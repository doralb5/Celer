<?php

$subdomain_text = isset($_POST['subdomain']) ? $_POST['subdomain'] : $subdomain->subdomain;
$meta_keywords = isset($_POST['meta_keywords']) ? $_POST['meta_keywords'] : $subdomain->meta_keywords;
$meta_desc = isset($_POST['meta_desc']) ? $_POST['meta_desc'] : $subdomain->meta_description;
$type = isset($_POST['type']) ? $_POST['type'] : $subdomain->type;
$target_url = isset($_POST['target_url']) ? $_POST['target_url'] : $subdomain->target_url;
$target_id = isset($_POST['target_id']) ? $_POST['target_id'] : $subdomain->target_id;
$company_name = isset($_POST['company_name']) ? $_POST['company_name'] : $subdomain->company_name;
$category = isset($_POST['category']) ? $_POST['category'] : $subdomain->category;

$id_business = ($subdomain->type == 'B') ? (isset($_POST['id_business']) ? $_POST['id_business'] : $subdomain->target_id) : '';
$id_category = ($subdomain->type == 'C') ? (isset($_POST['id_category']) ? $_POST['id_category'] : $subdomain->target_id) : '';
?>

    <script>

        function handleType() {
            if ($('#type').val() == 'B') {
                $('#target-business').show();
                $('#target-category').hide();
                $('#target-url').hide();
            } else if ($('#type').val() == 'C') {
                $('#target-business').hide();
                $('#target-category').show();
                $('#target-url').show();
            } else if ($('#type').val() == 'R') {
                $('#target-business').hide();
                $('#target-category').hide();
                $('#target-url').show();
            }
        }


        $(document).ready(function () {
            $('#type').change(function () {
                handleType();
            });
            handleType();
        });

    </script>

    <div class="col-md-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$Category]</h4>
            </div>
            <div class="panel-body">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
                      novalidate="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="subdomain">[$Subdomain] *</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $subdomain_text ?>"
                                   name="subdomain" placeholder="[$Subdomain]" data-parsley-required="true"
                                   data-parsley-id="6524" required>
                            <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="meta_keywords">[$Meta_keywords] *</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $meta_keywords ?>"
                                   name="meta_keywords" placeholder="[$Meta_keywords]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="meta_desc">[$MetaDescription]</label>

                        <div class="col-md-8 col-sm-8">
                            <textarea name="meta_desc" class="form-control" rows="3"
                                      style="max-width: 100%; min-width: 100%"><?= $meta_desc ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="type">[$Type] *</label>

                        <div class="col-md-8 col-sm-8">
                            <select class="form-control"
                                    name="type" placeholder="[$type]" id="type" required>
                                <option value="B" <?= ($type == 'B') ? 'selected' : '' ?>>[$business]</option>
                                <option value="C" <?= ($type == 'C') ? 'selected' : '' ?>>[$category]</option>
                                <option value="R" <?= ($type == 'R') ? 'selected' : '' ?>>[$redirect]</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="target-business">
                        <label class="control-label col-md-4 col-sm-4">[$Business]</label>
                        <div class="col-md-8 col-sm-8">
                            <div class="input-group">
                                <input readonly id="company_name" name="company_name" type="text" class="form-control"
                                       placeholder="[$SelectCompany]" value="<?= $company_name ?>">
                                <input type="hidden" id="id_business" name="id_business" value="<?= $id_business ?>"/>
                                <div class="input-group-btn">
                                    <a title="[$Select]" class="btn btn-primary" data-toggle="modal"
                                       href="#myModal_Businesses">...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="target-category">
                        <label class="control-label col-md-4 col-sm-4">[$Category]</label>
                        <div class="col-md-8 col-sm-8">
                            <div class="input-group">
                                <input readonly id="category" name="category" type="text" class="form-control"
                                       placeholder="[$SelectCategory]" value="<?= $category ?>">
                                <input type="hidden" id="id_category" name="id_category" value="<?= $id_category ?>"/>
                                <div class="input-group-btn">
                                    <a title="[$Select]" class="btn btn-primary" data-toggle="modal"
                                       href="#myModal_BusinessCategory">...</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="target-url">
                        <label class="control-label col-md-4 col-sm-4" for="target_url">[$Target_url] *</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $target_url ?>"
                                   name="target_url" placeholder="[$Target_url]">
                        </div>
                    </div>

                    <div class="form-group">
                        <p>&nbsp;</p>
                        <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                        </button>
                        <p>&nbsp;</p>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php
require_once DOCROOT.WEBROOT.VIEWS_PATH.'SelectionList_class.php';
$SL1 = new SelectionListView('Businesses');
$SL1->setTitle('Choose a company');
$SL1->setAjxUrl(Utils::getComponentUrl('Businesses/ajx_BusinessesSelectionList'));
$SL1->setDestinationField('company_name');
$SL1->setDestTextSelector('company_name');
$SL1->setDestIdSelector('id_business');
$SL1->render();

$SL1 = new SelectionListView('BusinessCategory');
$SL1->setTitle('Choose a category');
$SL1->setAjxUrl(Utils::getComponentUrl('Businesses/ajx_CategoriesSelectionList'));
$SL1->setDestinationField('category');
$SL1->setDestTextSelector('category');
$SL1->setDestIdSelector('id_category');
$SL1->render();

?>