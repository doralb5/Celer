<?php
$profile_name = isset($_POST['profile']) ? $_POST['profile'] : $profile->profile;
$description = isset($_POST['description']) ? $_POST['description'] : $profile->description;
$relevance = isset($_POST['relevance']) ? $_POST['relevance'] : $profile->relevance;

$subdomain->subdomain = $_POST['subdomain'];
				$subdomain->target_id = $_POST['target_id'];
				$subdomain->meta_keywords = ($_POST['meta_keywords'] != '') ? $_POST['meta_keywords'] : '';
				$subdomain->meta_desc = ($_POST['meta_desc'] != '') ? $_POST['meta_desc'] : '';
				$subdomain->type = $_POST['type'];
				$subdomain->target_url = ($_POST['target_url'] != '') ? $_POST['target_url']:'';

?>
<div class="col-md-8">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$Category]</h4>
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
                  novalidate="" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Name] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $profile_name ?>"
                               name="profile" placeholder="[$Name]" data-parsley-required="true"
                               data-parsley-id="6524" required>
                        <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Description]</label>

                    <div class="col-md-8 col-sm-8">
                            <textarea name="description" class="form-control" rows="3"
                                      style="max-width: 100%; min-width: 100%"><?= $description ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Relevance] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $relevance ?>"
                               name="relevance" placeholder="[$Relevance]">
                    </div>
                </div>
                <div class="form-group">
                    <p>&nbsp;</p>
                    <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                    <p>&nbsp;</p>
                </div>
            </form>
        </div>
    </div>
</div>
