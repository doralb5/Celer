<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Businesses');

foreach ($subdomains as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_subdomain_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		//new Button('<i class="fa fa-eye"></i>', $url_business_details . '/' . $row->id, 'xs', '', '[$Details]'),
	);
}
$table->setElements($subdomains);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('subdomain', '[$Subdomain]', 'text', 'left'),
	new TableList_Field('target_url', '[$Target]', 'string', 'left'),
));
$table->setUrl_action($url_subdomains_list);
$table->setUrl_delete($url_subdomain_delete);
$table->render();
