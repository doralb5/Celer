<?php

class Contracts_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Contracts/contract_edit/');

		$this->view->set('url_contracts_list', $this->getActionUrl('contracts_list'));
		$this->view->set('url_contract_edit', $this->getActionUrl('contract_edit'));
		$this->view->set('url_contract_delete', $this->getActionUrl('contract_delete'));
		$this->view->set('url_contract_details', $this->getActionUrl('contract_details'));

		$this->view->set('url_ContractService_delete', $this->getActionUrl('ContractService_delete'));
		$this->view->set('url_ContractService_edit', $this->getActionUrl('ContractService_edit'));
		$this->view->set('url_ContractService_status', $this->getActionUrl('ContractService_status'));

		$this->view->set('url_ContractFile_delete', $this->getActionUrl('ContractFile_delete'));
		$this->view->set('url_ContractFile_download', $this->getActionUrl('ContractFile_download'));
		$this->view->set('url_ContractFile_show', substr(WEBROOT, 0, -6).MEDIA_ROOT.'contracts');
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function contracts_list()
	{
		HeadHTML::setTitleTag('Contracts'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'Contract.id', 'peso' => 100),
				array('field' => 'Contract.number', 'peso' => 100),
				array('field' => 'Company.name', 'peso' => 90),
			);
			$contracts = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$contracts = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($contracts as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddContract]', $this->getActionUrl('contract_edit')));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('contracts', $contracts);
		$this->view->setTitle('[$Contracts]');
		$this->view->render('contracts-list');
	}

	public function contract_edit($id = null)
	{
		if (!is_null($id)) {
			$contract = $this->model->getContract($id);
			HeadHTML::setTitleTag('Edit Contract - '.$contract->company_name.' | '.CMS_Settings::$website_title);
		} else {
			$contract = new Contract_Entity();
			HeadHTML::setTitleTag('New Contract'.' | '.CMS_Settings::$website_title);
		}

		if ($contract === false) {
			$this->AddError("Contract $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['number'] == '') ? $this->AddError('Contract Number is required!') : '';
			($_POST['date'] == '') ? $this->AddError('Contract Date is required!') : '';
			(!isset($_POST['clientType'])) ? $this->AddError('Client Type is required!') : '';

			if (is_null($id)) {
				($this->model->existNumber($_POST['number'])) ? $this->AddError('Contract Number exist!') : '';
			}

			if ($contract->status == 'active') {
				if (!in_array($_POST['status'], array('active'))) {
					$this->AddError('You can not do this change!');
				}
			} elseif ($contract->status == 'cancelled') {
				if (!in_array($_POST['status'], array('cancelled'))) {
					$this->AddError('You can not do this change!');
				}
			}

			if (count($this->view->getErrors()) == 0) {

				//Ne rast se duhet te krijojme nje kompani
				if ($_POST['clientType'] == 'business') {
					if ($_POST['id_company'] == '') {
						$company_md = Loader::getModel('Companies', 'Registries');

						($_POST['com_code'] == '') ? $this->AddError('Company Code is required!') : '';
						($_POST['com_name'] == '') ? $this->AddError('Company Name is required!') : '';
						($_POST['com_vat_number'] == '') ? $this->AddError('Company VAT is required!') : '';
						($_POST['com_id_type'] == '') ? $this->AddError('Company type is required!') : '';
						($_POST['com_id_category'] == '') ? $this->AddError('Company category is required!') : '';
						($company_md->existCode($_POST['com_code'])) ? $this->AddError('Company Code exist!') : '';

						if (count($this->view->getErrors()) == 0) {
							$company = new Company_Entity();

							$company->code = $_POST['com_code'];
							$company->name = $_POST['com_name'];
							$company->vat_number = $_POST['com_vat_number'];
							$company->id_type = $_POST['com_id_type'];
							$company->id_category = $_POST['com_id_category'];
							$inserted_id = $company_md->saveCompany($company);
							if (!is_array($inserted_id)) {
								$this->view->AddNotice('New company has been created successfully.');
								$this->LogsManager->registerLog('Companies', 'insert', 'Company inserted with id : '.$inserted_id, $inserted_id);
							} else {
								$this->view->AddError('Something went wrong during creation of the company!');
							}

							$_POST['id_company'] = $inserted_id;
						}
					}
					$_POST['id_contact'] = '';
				} elseif ($_POST['clientType'] == 'private') {
					if ($_POST['id_contact'] == '') {
						$contact_md = Loader::getModel('Contacts', 'Registries');
						($_POST['con_code'] == '') ? $this->AddError('Contact Code is required!') : '';
						($_POST['con_firstname'] == '') ? $this->AddError('Contact Firstname is required!') : '';
						($_POST['con_lastname'] == '') ? $this->AddError('Contact Lastname is required!') : '';
						($_POST['con_tax_code'] == '') ? $this->AddError('Contact Tax Code is required!') : '';
						($_POST['con_email'] == '') ? $this->AddError('Contact Email is required!') : '';
						($contact_md->existCode($_POST['con_code'])) ? $this->AddError('Code exist!') : '';

						if (count($this->view->getErrors()) == 0) {
							$contact = new Contact_Entity();

							$contact->code = $_POST['con_code'];
							$contact->firstname = $_POST['con_firstname'];
							$contact->lastname = $_POST['con_lastname'];
							$contact->tax_code = $_POST['con_tax_code'];
							$contact->email = $_POST['con_email'];
							$inserted_id = $contact_md->saveContact($contact);
							if (!is_array($inserted_id)) {
								$this->view->AddNotice('New contact has been created successfully.');
								$this->LogsManager->registerLog('Contacts', 'insert', 'Contact inserted with id : '.$inserted_id, $inserted_id);
							} else {
								$this->view->AddError('Something went wrong during creation of the contact!');
							}
							$_POST['id_contact'] = $inserted_id;
						}
					}
					$_POST['id_company'] = '';
				}

				if (count($this->view->getErrors()) == 0) {
					$contract->number = $_POST['number'];
					$contract->id_company = ($_POST['id_company'] != '') ? $_POST['id_company'] : null;
					$contract->id_contact = ($_POST['id_contact'] != '') ? $_POST['id_contact'] : null;
					$contract->date = date('Y-m-d H:i:s', strtotime($_POST['date']));
					$contract->expiration_date = date('Y-m-d H:i:s', strtotime($_POST['expiration_date']));
					$contract->description = $_POST['description'];
					$contract->id_user = ($_POST['id_user'] != '') ? $_POST['id_user'] : null;
					$contract->id_referred_by = ($_POST['id_referred_by'] != '') ? $_POST['id_referred_by'] : null;

					if (is_null($id)) {
						$contract->status = 'incompleted';
					} else {
						$contract->status = $_POST['status'];
					}

					$inserted_id = $this->model->saveContract($contract);
					if (!is_array($inserted_id)) {
						$this->AddNotice('Contract has been saved successfully.');

						if (!is_bool($inserted_id)) {
							$this->LogsManager->registerLog('Contract', 'insert', 'Contract inserted with id : '.$inserted_id, $inserted_id);
							Utils::RedirectTo($this->getActionUrl('contract_edit')."/$inserted_id");
						} else {
							$this->LogsManager->registerLog('Contract', 'update', 'Contract updated with id : '.$id, $id);
							Utils::RedirectTo($this->getActionUrl('contract_edit')."/$id");
						}
					} else {
						$this->AddError('Saving failed!');
					}
				}
			}
		} elseif (isset($_POST['AddCService'])) {
			($_POST['id_service'] == '') ? $this->AddError('Service is required!') : '';
			($_POST['cs_id_user'] == '') ? $this->AddError('User is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$cs = new ContractService_Entity();

				$cs->id_contract = $id;
				$cs->id_service = $_POST['id_service'];
				$cs->id_user = $_POST['cs_id_user'];
				$cs->status = 'working';
				$cs->description = $_POST['cs_description'];
				$cs->expiration_date = date('Y-m-d H:i:s', strtotime($_POST['cs_expiration_date']));
				$res = $this->model->saveContractService($cs);

				if (!is_array($res)) {
					$this->view->AddNotice('he service has been added successfully for thi contract.');
					$this->LogsManager->registerLog('ContractService', 'insert', "ContractService inserted with id: $res for the contract with id: $id", $res);
					Utils::RedirectTo($this->getActionUrl('contract_edit')."/$id");
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
		} elseif (isset($_POST['AddFile'])) {
			if ($_FILES['file']['tmp_name'][0] == '') {
				$this->view->AddError('Please upload a file...');
			}

			if (count($this->view->getErrors()) == 0) {
				$failures = 0;
				for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
					$tmp_name = time().'_'.$_FILES['file']['name'][$i];

					$conFile = new ContractFile_Entity();

					$conFile->id_contract = $id;
					$conFile->type = $_POST['type'];
					$conFile->filename = $tmp_name;
					$conFile->size = $_FILES['file']['size'][$i];
					$res = $this->model->saveContractFile($conFile);

					if (!is_array($res)) {
						$target = DOCROOT.MEDIA_ROOT.'contracts'.DS.$id;
						Utils::createDirectory($target);
						$filename = $target.DS.$tmp_name;
						move_uploaded_file($_FILES['file']['tmp_name'][$i], $filename);
					} else {
						$failures++;
					}
				}
				if ($failures > 0) {
					$this->AddError('There was '.$failures.' failures during file upload!');
				} else {
					$this->view->AddNotice('File has been uploaded successfully.');
				}
			}
			Utils::RedirectTo($this->getActionUrl('contract_edit')."/$id");
		}

		if (!is_null($id)) {
			$contractServices = $this->model->getContractServices($id);
			$contractFiles = $this->model->getContractFiles($id);
		} else {
			$contractServices = array();
			$contractFiles = array();
		}

		$service_md = Loader::getModel('Services', 'Contracts');
		$serCategories = $service_md->getServiceCategories();

		$company_md = Loader::getModel('Companies', 'Registries');
		$compCategories = $company_md->getCategories();
		$this->view->set('compCategories', $compCategories);
		$compTypes = $company_md->getCompanyTypes();
		$this->view->set('compTypes', $compTypes);

		$this->view->set('contractServices', $contractServices);
		$this->view->set('contractFiles', $contractFiles);
		$this->view->set('serCategories', $serCategories);
		$this->view->set('contract', $contract);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('contracts_list')));
		$title = (!is_null($id)) ? '[$EditContract]' : '[$NewContract]';
		$this->view->setTitle($title);
		$this->view->render('contract-edit');
	}

	public function contract_details($id = null)
	{
		if (!is_null($id)) {
			$contract = $this->model->getContract($id);
			HeadHTML::setTitleTag('Edit Contract - '.$contract->company_name.' | '.CMS_Settings::$website_title);
		} else {
			Utils::RedirectTo($this->getActionUrl('contracts_list'));
		}

		if ($contract === false) {
			$this->AddError("Contract $id not found");
		}

		$contractServices = $this->model->getContractServices($id);
		$contractFiles = $this->model->getContractFiles($id);

		$service_md = Loader::getModel('Services', 'Contracts');
		$serCategories = $service_md->getServiceCategories();

		$this->view->set('contractServices', $contractServices);
		$this->view->set('contractFiles', $contractFiles);
		$this->view->set('serCategories', $serCategories);
		$this->view->set('contract', $contract);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('contracts_list')));
		$this->view->setTitle('[$ContractDetails]');
		$this->view->render('contract-details');
	}

	public function contract_delete($id)
	{
		$contract = $this->model->getContract($id);
		$conFiles = $this->model->getContractFiles($id);

		$res = $this->model->deleteContract($id);
		if ($res) {
			$this->LogsManager->registerLog('Contract', 'delete', "Contract deleted with id : $id and company : ".$contract->company_name, $id);
			$this->AddNotice('Contract has been deleted successfully.');

			//Fshijme filet e kontrates
			foreach ($conFiles as $file) {
				$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'contracts'.DS.$file->id_contract.DS.$file->filename;
				unlink(DOCROOT.$filename);
			}
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('contracts_list'));
	}

	public function ContractService_edit($id)
	{
		$conService = $this->model->getContractService($id);

		$services_md = Loader::getModel('ServiceModels', 'Contracts');
		$serModel = $services_md->getServiceModel($conService->id_model);
		$modelFields = $services_md->getModelFields($conService->id_model);

		if (isset($_POST['save'])) {
			foreach ($modelFields as $field) {
				if ($field->required && $_POST['f_'.Utils::clean($field->name)] == '') {
					$this->AddError($field->name.' is required!');
				}
			}

			if (count($this->view->getErrors()) == 0) {

				//Indexes save
				foreach ($modelFields as $field) {
					$index = $this->model->getConSerData($id, $field->id);

					if (is_null($index)) {
						//E krijojme te ri nqs. nuk ekziston
						$index = new ContractServiceData_Entity();
						$index->id_contract_service = $id;
						$index->id_field = $field->id;
					}

					$index->value = $_POST['f_'.Utils::clean($field->name)];
					$result = $this->model->saveConSerData($index);
				}

				if (!is_array($result)) {
					$this->view->AddNotice('Contract of this service has been saved successfully.');
				} else {
					$this->view->AddError('Something went wrong!');
				}

				Utils::RedirectTo($this->getActionUrl('contract_edit')."/{$conService->id_contract}");
			}
		}

		$index = array();
		foreach ($modelFields as $field) {
			$ind = $this->model->getConSerData($id, $field->id);

			if (!is_null($ind)) {
				$index['f_'.Utils::clean($field->name)] = $ind->value;
			} else {
				$index['f_'.Utils::clean($field->name)] = '';
			}
		}

		if ($serModel->filename != '') {
			ob_start();
			include DOCROOT.DATA_DIR.'service_models'.DS.$serModel->filename;
			$serModel_content = ob_get_contents();
			ob_clean();
			$this->view->set('serModel_content', $serModel_content);
		}

		$this->view->set('serModel', $serModel);
		$this->view->set('modelFields', $modelFields);
		$this->view->set('conService', $conService);
		$this->view->set('index', $index);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('contract_edit')."/{$conService->id_contract}"));
		$this->view->setTitle('Edit Contract of Service');
		$this->view->render('edit-conService');
	}

	public function ContractService_status($id)
	{
		$conService = $this->model->getContractService($id);

		if (isset($_POST['save'])) {
			if ($conService->status = 'active' || $conService->status == 'suspended') {
				if (!in_array($_POST['status'], array('active', 'suspended', 'cancelled'))) {
					$this->AddError('You can not do this change!');
				}
			} elseif ($conService->status == 'cancelled') {
				if (!in_array($_POST['status'], array('cancelled'))) {
					$this->AddError('You can not do this change!');
				}
			}
			if (count($this->view->getErrors()) == 0) {
				$conService->status = $_POST['status'];
				$res = $this->model->saveContractService($conService);

				if (!is_array($res)) {
					$this->view->AddNotice('Service status has been saved successfully.');
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
		}

		$this->view->set('conService', $conService);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('contract_edit')."/{$conService->id_contract}"));
		$this->view->setTitle('Service Status');
		$this->view->render('service-status');
	}

	public function ContractService_delete($id)
	{
		$res = $this->model->deleteContractService($id);
		if ($res) {
			$this->LogsManager->registerLog('ContractService', 'delete', "ContractService deleted with id : $id", $id);
			$this->AddNotice('Service has been deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('contract_edit')."/{$_GET['id_con']}");
	}

	public function ContractFile_delete($id)
	{
		$file = $this->model->getContractFile($id);
		$res = $this->model->deleteContractFile($id);
		if ($res) {
			$this->LogsManager->registerLog('Contract File', 'delete', "Contract File deleted with id : $id AND type: {$file->type}", $id);
			$this->AddNotice('File has been deleted successfully.');
			$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'contracts'.DS.$file->id_contract.DS.$file->filename;
			unlink(DOCROOT.$filename);
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('contract_edit')."/{$_GET['id_con']}");
	}

	public function ContractFile_download($id)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('contracts_list'));
		}

		$file = $this->model->getContractFile($id);
		$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'contracts'.DS.$file->id_contract.DS.$file->filename;

		//Shkarkimi i Files
		header('Content-Type: '.mime_content_type(DOCROOT.$filename));
		header('Content-Disposition: attachment; filename="'.$file->filename.'"');
	}

	public function ajx_getAutogenerateNo()
	{
		$number = $this->model->getAutogenerateNo();
		echo json_encode(array('number' => $number));
	}
}
