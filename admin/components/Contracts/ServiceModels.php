<?php

class ServiceModels_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Contracts/ServiceModels/model_edit/');

		$this->view->set('url_models_list', $this->getActionUrl('models_list'));
		$this->view->set('url_model_edit', $this->getActionUrl('model_edit'));
		$this->view->set('url_model_delete', $this->getActionUrl('model_delete'));
		$this->view->set('url_model_details', $this->getActionUrl('model_details'));
		$this->view->set('url_modelField_delete', $this->getActionUrl('modelField_delete'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function models_list()
	{
		HeadHTML::setTitleTag('Models'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'ServiceModel.id', 'peso' => 100),
				array('field' => 'ServiceModel.name', 'peso' => 100),
			);
			$models = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$models = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($models as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddModel]', $this->getActionUrl('model_edit')));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('models', $models);
		$this->view->setTitle('[$Models]');
		$this->view->render('models-list');
	}

	public function model_edit($id = null)
	{
		if (!is_null($id)) {
			$serModel = $this->model->getServiceModel($id);
			$modelFields = $this->model->getModelFields($id);
			HeadHTML::setTitleTag('Edit Model - '.$serModel->name.' | '.CMS_Settings::$website_title);
		} else {
			$serModel = new ServiceModel_Entity();
			$modelFields = array();
			HeadHTML::setTitleTag('New Model'.' | '.CMS_Settings::$website_title);
		}

		if ($serModel === false) {
			$this->AddError("Model $id not found");
		} elseif (isset($_POST['change'])) {
			($_POST['modelname'] == '') ? $this->AddError('Model name must not be empty!') : '';
			if (count($this->view->getErrors()) == 0) {
				if (isset($_FILES['modelview']['tmp_name']) && strlen($_FILES['modelview']['tmp_name'])) {
					(!Utils::allowedFileType($_FILES['modelview']['name'], array('php', 'html'))) ? $this->view->AddError('Please upload a php or html file!') : '';
				}

				if (count($this->view->getErrors()) == 0) {
					if (strlen($_FILES['modelview']['tmp_name'])) {
						$_POST['modelview'] = time().'_'.$_FILES['modelview']['name'];
						$target = DOCROOT.DATA_DIR.DS.'service_models';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['modelview'];
						move_uploaded_file($_FILES['modelview']['tmp_name'], $filename);

						//Fshijme te vjetren
						if ($serModel->filename != '' && file_exists($target.DS.$serModel->filename)) {
							unlink($target.DS.$serModel->filename);
						}
					} else {
						$_POST['modelview'] = $serModel->filename;
					}

					$serModel->name = $_POST['modelname'];
					$serModel->filename = $_POST['modelview'];
					$inserted_id = $this->model->saveServiceModel($serModel);

					if (!is_array($inserted_id)) {
						$this->AddNotice('Model has been saved successfully.');
						if (!is_bool($inserted_id)) {
							$this->LogsManager->registerLog('ServiceModel', 'insert', 'Service Model inserted with id : '.$inserted_id, $inserted_id);
							Utils::RedirectTo($this->getActionUrl('model_edit')."/$inserted_id");
						} else {
							$this->LogsManager->registerLog('ServiceModel', 'update', 'Service Model updated with id : '.$id, $id);
						}
					} else {
						$this->AddError('Saving failed!');
					}
				}
			}
		}

		if (isset($_POST['save'])) {
			//Na sherben per te ruajtur fushat
			if (!isset($_POST['name'][0])) {
				$modelField = new ServiceModelField_Entity();
				if ($_POST['name'] != '') {
					$modelField->id_model = $id;
					$modelField->name = $_POST['name'];
					$modelField->type = $_POST['type'];
					$modelField->required = $_POST['required'];
					$result = $this->model->saveModelField($modelField);
				} else {
					$this->AddError('Input Name must not be empty!');
				}
			} else {
				for ($i = 0; $i < count($_POST['name']); $i++) {
					$modelField = new ServiceModelField_Entity();
					if ($_POST['name'][$i] != '') {
						$modelField->id_model = $id;
						$modelField->name = $_POST['name'][$i];
						$modelField->type = $_POST['type'][$i];
						$modelField->required = $_POST['required'][$i];
						$result = $this->model->saveModelField($modelField);
					}
				}
			}

			if (!is_array($result)) {
				$this->AddNotice('Model has been saved successfully.');
			} else {
				$this->AddError('Something went wrong!');
			}

			Utils::RedirectTo($this->getActionUrl('model_edit')."/$id");
		}

		$this->view->set('serModel', $serModel);
		$this->view->set('modelFields', $modelFields);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('models_list')));
		$title = (!is_null($id)) ? '[$EditModel]' : '[$NewModel]';
		$this->view->setTitle($title);
		$this->view->render('model-edit');
	}

	public function model_delete($id)
	{
		$serModel = $this->model->getServiceModel($id);
		$result = $this->model->deleteServiceModel($id);
		if ($result) {
			$this->LogsManager->registerLog('ServiceModel', 'delete', "Service Model deleted with id : $id and name : ".$serModel->name, $id);
			unlink(DOCROOT.DATA_DIR.DS.'docmodels'.DS.$serModel->filename);
			$this->AddNotice('Model has been deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('models_list'));
	}

	public function modelField_delete($id)
	{
		$result = $this->model->deleteModelField($id);
		if ($result) {
			$this->AddNotice('Field has been deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}

		if (isset($_GET['id_model'])) {
			Utils::RedirectTo($this->getActionUrl('model_edit').'/'.$_GET['id_model']);
		} else {
			Utils::RedirectTo($this->getActionUrl('models_list'));
		}
	}
}
