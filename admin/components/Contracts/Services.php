<?php

class Services_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Contracts/Service/service_edit/');

		$this->view->set('url_services_list', $this->getActionUrl('services_list'));
		$this->view->set('url_service_edit', $this->getActionUrl('service_edit'));
		$this->view->set('url_service_delete', $this->getActionUrl('service_delete'));

		$this->view->set('url_categories_list', $this->getActionUrl('categories_list'));
		$this->view->set('url_category_edit', $this->getActionUrl('category_edit'));
		$this->view->set('url_category_delete', $this->getActionUrl('category_delete'));

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function services_list()
	{
		HeadHTML::setTitleTag('Services'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'Service.id', 'peso' => 100),
				array('field' => 'Service.name', 'peso' => 100),
				array('field' => 'Service.description', 'peso' => 50),
			);
			$services = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$services = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($services as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddService]', $this->getActionUrl('service_edit')));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('services', $services);
		$this->view->setTitle('[$Services]');
		$this->view->render('services-list');
	}

	public function service_edit($id = null)
	{
		if (!is_null($id)) {
			$service = $this->model->getService($id);
			HeadHTML::setTitleTag('Edit Service - '.$service->name.' | '.CMS_Settings::$website_title);
		} else {
			$service = new Service_Entity();
			HeadHTML::setTitleTag('New Service'.' | '.CMS_Settings::$website_title);
		}

		if ($service === false) {
			$this->AddError("Service $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['name'] == '') ? $this->AddError('Name is required!') : '';
			($_POST['id_category'] == '') ? $this->AddError('Category is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$service->name = $_POST['name'];
				$service->description = $_POST['description'];
				$service->id_category = $_POST['id_category'];
				$service->id_model = $_POST['id_model'];
				$inserted_id = $this->model->saveService($service);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Service has been saved successfully.');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Service', 'insert', 'Service inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('service_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Service', 'update', 'Service updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('service_edit')."/$id");
					}
				} else {
					$this->AddError('Saving failed!');
				}
			}
		}

		$serCategories = $this->model->getServiceCategories(50);

		$serModels_md = Loader::getModel('ServiceModels', 'Contracts');
		$serModels = $serModels_md->getList(50);

		$this->view->set('serCategories', $serCategories);
		$this->view->set('serModels', $serModels);
		$this->view->set('service', $service);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('services_list')));
		$title = (!is_null($id)) ? '[$EditService]' : '[$NewService]';
		$this->view->setTitle($title);
		$this->view->render('service-edit');
	}

	public function service_delete($id)
	{
		$service = $this->model->getService($id);
		$result = $this->model->deleteService($id);
		if ($result !== false) {
			$this->AddNotice('Service has been deleted successfully.');
			$this->LogsManager->registerLog('Service', 'delete', "Service deleted with id : $id and name : ".$service->name, $id);
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('services_list'));
	}

	public function categories_list()
	{
		HeadHTML::setTitleTag('Service Categories'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		$categories = $this->model->getServiceCategories($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', $this->getActionUrl('category_edit')));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		if (!is_null($id)) {
			$category = $this->model->getServiceCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$category->category.' | '.CMS_Settings::$website_title);
		} else {
			$category = new ServiceCategory_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMS_Settings::$website_title);
		}

		if ($category === false) {
			$this->AddError("Category $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['category'] == '') ? $this->AddError('Name is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$category->category = $_POST['category'];
				$result = $this->model->saveServiceCategory($category);

				if (!is_array($result)) {
					$this->AddNotice('Category has been saved successfully.');

					if (is_null($id)) {
						$this->LogsManager->registerLog('DServiceCategory', 'insert', 'Service Category inserted with id : '.$id, $result);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$result");
					} else {
						$this->LogsManager->registerLog('ServiceCategory', 'update', 'Service Category updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}

		$this->view->set('category', $category);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('categories_list')));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id)
	{
		$category = $this->model->getServiceCategory($id);
		$result = $this->model->deleteServiceCategory($id);
		if ($result !== false) {
			$this->AddNotice('Category has been deleted successfully.');
			$this->LogsManager->registerLog('ServiceCategory', 'delete', "Service Category deleted with id : $id and name : ".$category->category, $id);
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('categories_list'));
	}

	public function ajx_getServices($id_categ)
	{
		$services = $this->model->getList(100, 0, "id_category = {$id_categ}");
		$options = array();
		foreach ($services as $ser) {
			array_push($options, array('text' => $ser->name, 'value' => $ser->id));
		}
		echo json_encode(array('options' => $options));
	}
}
