<?php
$number = $contract->number;
$id_company = $contract->id_company;
$company_name = $contract->company_name;
$contact_name = $contract->contact_fullname;
$date = isset($_POST['date']) ? $_POST['date'] : $contract->date;
$expiration_date = $contract->expiration_date;
$status = $contract->status;
$description = $contract->description;
$user_fullname = $contract->user_fullname;
$refby_fullname = $contract->refby_fullname;
?>


<div class="ui-sortable">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$ContractDetails]</h4>
            </div>
            <div class="panel-body panel-form">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form" novalidate="">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3" for="name">[$Number] * :</label>
                            <div class="col-md-9">
                                <input disabled="" class="form-control" type="text" value="<?= $number ?>" id="name" name="number" placeholder="[$Number]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">[$Company] :</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input disabled id="company" type="text" class="form-control" placeholder="Select Company" value="<?= $company_name ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">[$Contact] :</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input disabled id="contact" type="text" class="form-control" placeholder="Select Contact" value="<?= $contact_name ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">[$ContractDate]</label>
                            <div class="col-md-9">
                                <input disabled="" type="text" class="form-control" name="date" id="datepicker-default" placeholder="[$SelectDate]" value="<?= $date ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">[$ExpirationDate]</label>
                            <div class="col-md-9">
                                <input disabled="" type="text" class="form-control" name="expiration_date" id="datepicker-autoClose" placeholder="[$SelectDate]" value="<?= $expiration_date ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Status]</label>
                            <div class="col-md-9">
                                <select disabled="" class="form-control" name="status">
                                    <option value="incompleted" <?= ($status == 'incompleted') ? 'selected' : '' ?>>incompleted</option>
                                    <option value="completed" <?= ($status == 'completed') ? 'selected' : '' ?>>completed</option>
                                    <option value="canceled" <?= ($status == 'canceled') ? 'selected' : '' ?>>canceled</option>
                                    <option value="transferred" <?= ($status == 'transferred') ? 'selected' : '' ?>>transferred</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Description]</label>
                            <div class="col-md-9">
                                <textarea disabled="" name="description" class="form-control" placeholder="[$Description]" rows="4" style="max-width:100%"><?= $description ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Assignedto]</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input disabled id="user" type="text" class="form-control" placeholder="Select User" value="<?= $user_fullname ?>" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$ReferredBy]</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input disabled id="referred_by" type="text" class="form-control" placeholder="Select User" value="<?= $refby_fullname ?>" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end panel -->
    </div>

</div>

<div class="ui-sortable">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$Services]</h4>
            </div>
            <div class="panel-body panel-form">
                <p>&nbsp;</p>
                <?php
				include(VIEWS_PATH.'TableList_class.php');
				$table = new TableListView('Contracts', 'list');
				foreach ($contractServices as &$row) {
					$row->rows_buttons = array(
						new Button('<i class="fa fa-file"></i>', $url_ContractService_edit.'/'.$row->id, 'xs', '', '[$Contract]'),
						new Button('<i class="fa fa-random"></i>', $url_ContractService_status.'/'.$row->id, 'xs', '', '[$Status]'),
					);
				}
				$table->setElements($contractServices);
				$table->setTotalElements(100);
				$table->setElements_per_page(100);
				$table->setFields(array(
					new TableList_Field('id', '[$Id]', 'int', 'left'),
					new TableList_Field('service_name', '[$Service]', 'string', 'left'),
					new TableList_Field('expiration_date', '[$ExpirationDate]', 'date', 'left'),
				));
				$table->setUrl_delete($url_ContractService_delete);
				$table->setUrl_delete_params(array('id_con' => $contract->id));
				$table->multipleDeletion(false);
				$table->renderTopBar(false);
				$table->render();
				?>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>


<div class="ui-sortable">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$UploadFiles]</h4>
            </div>
            <div class="panel-body">

                <p>&nbsp;</p>
                <?php
				$table = new TableListView('ContractFiles', 'list');
				foreach ($contractFiles as &$row) {
					$row->rows_buttons = array(
						new Button('<i class="fa fa-eye"></i>', $url_ContractFile_show.'/'.$row->id_contract.'/'.$row->filename, 'xs', '', '[$Show]', '_blank'),
						new Button('<i class="fa fa-download"></i>', $url_ContractFile_download.'/'.$row->id, 'xs', '', '[$Download]')
					);
				}

				$table->setElements($contractFiles);
				$table->setTotalElements(100);
				$table->setElements_per_page(100);
				$table->setFields(array(
					new TableList_Field('id', '[$Id]', 'int', 'left'),
					new TableList_Field('filename', '[$Filename]', 'string', 'left'),
					new TableList_Field('type', '[$Type]', 'string', 'left'),
				));
				$table->setUrl_delete($url_ContractFile_delete);
				$table->setUrl_delete_params(array('id_con' => $contract->id));
				$table->multipleDeletion(false);
				$table->renderTopBar(false);
				$table->render();
				?>
            </div>
        </div>
    </div>
</div>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/js/form-plugins.demo.min.js'); ?>
