<?php
$number = isset($_POST['number']) ? $_POST['number'] : $contract->number;
$id_company = isset($_POST['id_company']) ? $_POST['id_company'] : $contract->id_company;
$company_name = isset($_POST['company']) ? $_POST['company'] : $contract->company_name;
$id_contact = isset($_POST['id_contact']) ? $_POST['id_contact'] : $contract->id_contact;
$contact_name = isset($_POST['contact']) ? $_POST['contact'] : $contract->contact_fullname;
$date = isset($_POST['date']) ? $_POST['date'] : $contract->date;
$expiration_date = isset($_POST['expiration_date']) ? $_POST['expiration_date'] : $contract->expiration_date;
$status = isset($_POST['status']) ? $_POST['status'] : $contract->status;
$description = isset($_POST['description']) ? $_POST['description'] : $contract->description;

$id_user = isset($_POST['id_user']) ? $_POST['id_user'] : $contract->id_user;
$user_fullname = isset($_POST['user']) ? $_POST['user'] : $contract->user_fullname;

$id_referred_by = isset($_POST['id_referred_by']) ? $_POST['id_referred_by'] : $contract->id_referred_by;
$refby_fullname = isset($_POST['referred_by']) ? $_POST['referred_by'] : $contract->refby_fullname;

if (!is_null($id_company)) {
	$client_type = 'business';
} elseif (!is_null($id_contact)) {
	$client_type = 'private';
}
$client_type = (isset($_POST['clientType'])) ? $_POST['clientType'] : '';
?>
<div class="col-md-12">
    <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form" novalidate="">
        <div class="col-md-6">
            <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$ContractData]</h4>
                </div>
                <div class="panel-body panel-form">
                    <div class="form-group">
                        <label class="control-label col-md-3" for="name">[$Number]</label>
                        <div class="col-md-6">
                            <input <?= (!is_null($contract->id) ? 'readonly' : '') ?> class="form-control" type="text" value="<?= $number ?>" id="ContractNr" name="number" placeholder="[$SetNumber]" required="">
                        </div>
                        <div class="col-md-3">
                            <div class="checkbox">
                                <label>
                                    <input <?= (!is_null($contract->id) ? 'disabled' : '') ?> type="checkbox" value="1" name="autogenerate" id="autogenerate">
                                    Autogenerate
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">[$ContractDate]</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="date" id="datepicker-default" placeholder="[$SelectDate]" value="<?= $date ?>" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">[$Status]</label>
                        <div class="col-md-9">
                            <select class="form-control" name="status" <?= (in_array($contract->status, array('completed', 'canceled')) || (is_null($contract->id))) ? 'readonly' : '' ?>>
                                <?php if (is_null($contract->id)) {
	?>
                                    <option value="incompleted" selected>incompleted</option>
                                <?php
} elseif ($contract->status == 'completed') {
		?>
                                    <option value="completed" <?= ($status == 'completed') ? 'selected' : '' ?>>completed</option>
                                <?php
	} elseif ($contract->status == 'canceled') {
		?>
                                    <option value="canceled" <?= ($status == 'canceled') ? 'selected' : '' ?>>canceled</option>
                                <?php
	} else {
		?>
                                    <option value="completed" <?= ($status == 'completed') ? 'selected' : '' ?>>completed</option>
                                    <option value="incompleted" <?= ($status == 'incompleted') ? 'selected' : '' ?>>incompleted</option>
                                    <option value="canceled" <?= ($status == 'canceled') ? 'selected' : '' ?>>canceled</option>
                                    <option value="transferred" <?= ($status == 'transferred') ? 'selected' : '' ?>>transferred</option>
                                <?php
	} ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-inverse" data-sortable-id="ui-widget-3" data-init="true">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$AdditionalData]</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">[$ExpirationDate]</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="expiration_date" id="datepicker-autoClose" placeholder="[$SelectDate]" value="<?= $expiration_date ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">[$Description]</label>
                        <div class="col-md-9">
                            <textarea name="description" class="form-control" placeholder="[$Description]" rows="3" style="max-width:100%"><?= $description ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">[$Assignedto]</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input readonly id="user" name="user" type="text" class="form-control" placeholder="Select User" value="<?= $user_fullname ?>">
                                <input type="hidden" id="id_user" name="id_user" value="<?= $id_user ?>"/>
                                <div class="input-group-btn">
                                    <a title="[$Select]" class="btn btn-primary" data-toggle="modal" href="#myModal_Users_1">...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">[$ReferredBy]</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input readonly id="referred_by" name="referred_by" type="text" class="form-control" placeholder="Select User" value="<?= $refby_fullname ?>">
                                <input type="hidden" id="id_referred_by" name="id_referred_by" value="<?= $id_referred_by ?>"/>
                                <div class="input-group-btn">
                                    <a title="[$Select]" class="btn btn-primary" data-toggle="modal" href="#myModal_Users_2">...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-inverse" data-sortable-id="ui-widget-2" data-init="true">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$CustomerDetails]</h4>
                </div>
                <div class="panel-body panel-form">
                    <div class="form-group">
                        <label class="col-md-3 control-label">[$ClientType]*</label>
                        <div class="col-md-9">
                            <label class="radio-inline" id="t_business">
                                <input type="radio" name="clientType" value="business" <?= ($client_type == 'business') ? 'checked' : '' ?> required="">
                                [$Business]
                            </label>
                            <label class="radio-inline" id="t_private">
                                <input type="radio" name="clientType" value="private" <?= ($client_type == 'private') ? 'checked' : '' ?> required="">
                                [$Private]
                            </label>
                        </div>
                    </div>
                    <div class="form-group" id="f_Company">
                        <label class="control-label col-md-3">[$Company]</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input readonly id="company" name="company" type="text" class="form-control" placeholder="Select Company" value="<?= $company_name ?>">
                                <input type="hidden" id="id_company" name="id_company" value="<?= $id_company ?>"/>
                                <div class="input-group-btn">
                                    <a title="[$Select]" class="btn btn-primary" data-toggle="modal" href="#myModal_Companies">...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="f_Contact">
                        <label class="control-label col-md-3">[$Contact]</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input readonly id="contact" type="text" name="contact" class="form-control" placeholder="Select Contact" value="<?= $contact_name ?>">
                                <input type="hidden" id="id_contact" name="id_contact" value="<?= $id_contact ?>"/>
                                <div class="input-group-btn">
                                    <a title="[$Select]" class="btn btn-primary" data-toggle="modal" href="#myModal_Contacts">...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="CompanyForm">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">[$Code] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="" id="com_code" name="com_code" placeholder="[$Code]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">[$Name] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="" id="com_name" name="com_name" placeholder="[$Name]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">[$VatNumber] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="" id="com_vat_number" name="com_vat_number" placeholder="[$VatNumber]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Type]</label>
                            <div class="col-md-9">
                                <select class="form-control" id="com_id_type" name="com_id_type">
                                    <?php foreach ($compTypes as $type) {
		?>
                                        <option value="<?= $type->id ?>"><?= $type->type ?></option>
                                    <?php
	} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Category]</label>
                            <div class="col-md-9">
                                <select class="form-control" id="com_id_category" name="com_id_category">
                                    <?php foreach ($compCategories as $categ) {
		?>
                                        <option value="<?= $categ->id ?>"><?= $categ->category ?></option>
                                    <?php
	} ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="ContactForm">
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Code] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="con_code" name="con_code" placeholder="[$Code]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Firstname] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="con_firstname" name="con_firstname" placeholder="[$Firstname]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Lastname] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="con_lastname" name="con_lastname" placeholder="[$Lastname]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$TaxCode] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="con_tax_code" name="con_tax_code" placeholder="[$TaxCode]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">[$Email] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="con_email" name="con_email" data-parsley-type="email" placeholder="[$Email]"><ul class="parsley-errors-list" id="parsley-id-0818"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <p class="text-center">
                <button type="submit" name="save" class="btn btn-info">
                    <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                </button>
            </p>
        </div>
        <p>&nbsp;</p>
    </form>
</div>

<?php if (!is_null($contract->id)) {
		?>
    <div class="ui-sortable">
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$Services]</h4>
                </div>
                <div class="panel-body panel-form">
                    <p>&nbsp;</p>
                    <?php
					include(VIEWS_PATH.'TableList_class.php');
		$table = new TableListView('Contracts', 'list');
		foreach ($contractServices as &$row) {
			$row->rows_buttons = array(
							new Button('<i class="fa fa-file"></i>', $url_ContractService_edit.'/'.$row->id, 'xs', '', '[$Contract]'),
							new Button('<i class="fa fa-random"></i>', $url_ContractService_status.'/'.$row->id, 'xs', '', '[$Status]'),
						);
		}

		$table->setElements($contractServices);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
						new TableList_Field('id', '[$Id]', 'int', 'left'),
						new TableList_Field('service_name', '[$Service]', 'string', 'left'),
						new TableList_Field('status', '[$Status]', 'string', 'left'),
						new TableList_Field('expiration_date', '[$ExpirationDate]', 'date', 'left'),
					));
		$table->setUrl_delete($url_ContractService_delete);
		$table->setUrl_delete_params(array('id_con' => $contract->id));
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->render(); ?>

                    <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form" novalidate="">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">[$Category]</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="ServiceCategory" name="id_category" onchange="refreshServicesList()">
                                        <?php foreach ($serCategories as $categ) {
			?>
                                            <option value="<?= $categ->id ?>"><?= $categ->category ?></option>
                                        <?php
		} ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">[$Service]</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="ServicesList" name="id_service">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">[$ExpirationDate]</label>
                                <div class="col-md-9">
                                    <div class="input-group date" id="datetimepicker1">
                                        <input type="text" class="form-control" name="cs_expiration_date" placeholder="[$SelectDate]">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">[$User]</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input disabled id="cs_user" value="" type="text" class="form-control" placeholder="Select User" required="">
                                        <input type="hidden" id="cs_id_user" name="cs_id_user"/>
                                        <div class="input-group-btn">
                                            <a title="[$Select]" class="btn btn-primary" data-toggle="modal" href="#myModal_Users_3">...</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">[$Description]</label>
                                <div class="col-md-9">
                                    <textarea name="cs_description" class="form-control" placeholder="[$Description]" rows="4" style="max-width:100%"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="pull-right">
                                <button type="submit" name="AddCService" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp;&nbsp;[$AddService]</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
    </div>


    <div class="ui-sortable">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$UploadFiles]</h4>
                </div>
                <div class="panel-body">

                    <p>&nbsp;</p>
                    <?php
					$table = new TableListView('ContractFiles', 'list');
		foreach ($contractFiles as &$row) {
			$row->rows_buttons = array(
							new Button('<i class="fa fa-eye"></i>', $url_ContractFile_show.'/'.$row->id_contract.'/'.$row->filename, 'xs', '', '[$Show]', '_blank'),
							new Button('<i class="fa fa-download"></i>', $url_ContractFile_download.'/'.$row->id, 'xs', '', '[$Download]')
						);
		}

		$table->setElements($contractFiles);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
						new TableList_Field('id', '[$Id]', 'int', 'left'),
						new TableList_Field('filename', '[$Filename]', 'string', 'left'),
						new TableList_Field('type', '[$Type]', 'string', 'left'),
					));
		$table->setUrl_delete($url_ContractFile_delete);
		$table->setUrl_delete_params(array('id_con' => $contract->id));
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->render(); ?>



                    <div class="col-md-12">
                        <form id="fileupload" method="POST" enctype="multipart/form-data">
                            <div class="form-group col-md-5">
                                <label>[$SelectFile]</label>
                                <div class="box">
                                    <input type="file" name="file[]" id="file-7" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple/>
                                    <label for="file-7"><span></span><strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> [$Choose]&hellip;</strong></label>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label>[$Type]</label>
                                <div class="input-group">
                                    <select class="form-control" name="type">
                                        <option value="identity document" >identity document</option>
                                        <option value="copy of the contract">copy of the contract</option>
                                        <option value="audio recording">audio recording</option>
                                        <option value="other">other</option>
                                    </select>
                                    <div class="input-group-btn">
                                        <button class="btn btn-info" name="AddFile" type="submit"><i class="fa fa-plus"></i>&nbsp;&nbsp;[$AddFile]</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12 note note-info">
                        <h4>Notes</h4>
                        <ul>
                            <li>The maximum file size for uploads is <strong>20 MB</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
	} ?>
<?php
//HeadHTML::AddStylesheet(WEBROOT . LIBS_PATH . 'file-input/css/normalize.css');
//HeadHTML::AddStylesheet(WEBROOT . LIBS_PATH . 'file-input/css/demo.css');
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/bootstrap-daterangepicker/moment.js'); ?>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/bootstrap-datepicker/css/datepicker.css'); ?>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/js/form-plugins.demo.min.js'); ?>

<?php HeadHTML::addScript('handleDatepicker();') ?>
<?php HeadHTML::addScript('handleDateTimePicker();') ?>

<?php
require_once DOCROOT.WEBROOT.VIEWS_PATH.'SelectionList_class.php';
$SL1 = new SelectionListView('Companies');
$SL1->setTitle('Choose a company');
$SL1->setAjxUrl(Utils::getComponentUrl('Registries/Companies/ajx_CompaniesSelectionList'));
$SL1->setDestinationField('name');
$SL1->setDestTextSelector('company');
$SL1->setDestIdSelector('id_company');
$SL1->addJSCode('getCompanyData();');
$SL1->render();

$SL2 = new SelectionListView('Contacts');
$SL2->setTitle('Choose a contact');
$SL2->setAjxUrl(Utils::getComponentUrl('Registries/Contacts/ajx_ContactsSelectionList'));
$SL2->setDestinationField('name');
$SL2->setDestTextSelector('contact');
$SL2->setDestIdSelector('id_contact');
$SL2->addJSCode('getContactData();');
$SL2->render();

$SL3 = new SelectionListView('Users', 1);
$SL3->setTitle('Choose a user');
$SL3->setAjxUrl(Utils::getControllerUrl('Users/ajx_UsersSelectionList').'?id='. 1);
$SL3->setDestinationField('fullname');
$SL3->setDestTextSelector('user');
$SL3->setDestIdSelector('id_user');
$SL3->render();

$SL4 = new SelectionListView('Users', 2);
$SL4->setTitle('Choose a user');
$SL4->setAjxUrl(Utils::getControllerUrl('Users/ajx_UsersSelectionList').'?id='. 2);
$SL4->setDestinationField('fullname');
$SL4->setDestTextSelector('referred_by');
$SL4->setDestIdSelector('id_referred_by');
$SL4->render();

$SL5 = new SelectionListView('Users', 3);
$SL5->setTitle('Choose a user');
$SL5->setAjxUrl(Utils::getControllerUrl('Users/ajx_UsersSelectionList').'?id='. 3);
$SL5->setDestinationField('fullname');
$SL5->setDestTextSelector('cs_user');
$SL5->setDestIdSelector('cs_id_user');
$SL5->render();
?>

<script>

    //Javascript Functions
    function refreshServicesList() {
        $.ajax({
            url: "<?= Utils::getComponentUrl('Contracts/Services/ajx_getServices/') ?>" + $('#ServiceCategory').val(),
            dataType: "json",
            success: function (data) {
                var options, index, select, option;
                select = document.getElementById('ServicesList');
                select.options.length = 0;
                options = data.options;
                for (index = 0; index < options.length; ++index) {
                    option = options[index];
                    select.options.add(new Option(option.text, option.value));
                }
                $('#ServicesList').val('');
            },
            error: function (e) {
                select = document.getElementById('ServicesList');
                select.options.length = 0;
                select.options.add(new Option('', ''));
                console.log('Error: ' + e);
            }
        });
    }

    function getCompanyData() {
        $.ajax({
            url: "<?= Utils::getComponentUrl('Registries/Companies/ajx_getCompany/') ?>" + $('#id_company').val(),
            dataType: "json",
            success: function (data) {
                $('#com_code').val(data.company.code);
                $('#com_name').val(data.company.name);
                $('#com_vat_number').val(data.company.vat_number);
                $('#com_id_type').val(data.company.id_type);
                $('#com_id_category').val(data.company.id_category);
            },
            error: function (e) {
                console.log('Error: ' + e);
            }
        });
    }
    function getContactData() {
        $.ajax({
            url: "<?= Utils::getComponentUrl('Registries/Contacts/ajx_getContact/') ?>" + $('#id_contact').val(),
            dataType: "json",
            success: function (data) {
                $('#con_code').val(data.contact.code);
                $('#con_firstname').val(data.contact.firstname);
                $('#con_lastname').val(data.contact.lastname);
                $('#con_tax_code').val(data.contact.tax_code);
                $('#con_email').val(data.contact.email);
            },
            error: function (e) {
                console.log('Error: ' + e);
            }
        });
    }

    function getAutogenerateNumber(handleData) {
        $.ajax({
            url: "<?= Utils::getComponentUrl('Contracts/ajx_getAutogenerateNo') ?>",
            dataType: "json",
            success: function (data) {
                handleData(data.number);
            },
            error: function (e) {
                console.log('Error: ' + e);
                handleData('');
            }
        });
    }
</script>


<script>
    $(document).ready(function () {
<?php if ($client_type != 'business') {
	?>
            $('#CompanyForm').hide();
            $('#f_Company').hide();
<?php
} else {
		?>
            getCompanyData();
<?php
	} ?>
<?php if ($client_type != 'private') {
		?>
            $('#ContactForm').hide();
            $('#f_Contact').hide();
<?php
	} else {
		?>
            getContactData();
<?php
	} ?>
        $(function () {
            $('input[type="radio"]').click(function () {
                if ($(this).is(':checked') && $(this).val() == 'business')
                {
                    $('#CompanyForm').show();
                    $('#f_Company').show();
                } else {
                    $('#CompanyForm').hide();
                    $('#f_Company').hide();
                }
            });
            $('input[type="radio"]').click(function () {
                if ($(this).is(':checked') && $(this).val() == 'private')
                {
                    $('#ContactForm').show();
                    $('#f_Contact').show();
                } else {
                    $('#ContactForm').hide();
                    $('#f_Contact').hide();
                }
            });

            $('#autogenerate').click(function () {
                if ($(this).is(':checked'))
                {
                    $('#ContractNr').attr('readonly', true);
                    $('#ContractNr').attr('placeholder', '[$AutoGenerate]');
                    getAutogenerateNumber(function (output) {
                        $('#ContractNr').val(++output);
                    });

                } else {
                    $('#ContractNr').attr('readonly', false);
                    $('#ContractNr').attr('placeholder', '[$SetNumber]');
                }
            });
        });

<?php if (!is_null($contract->id)) {
		?>
            if ($('#ServiceCategory').val() !== '') {
                refreshServicesList();
            }
<?php
	} ?>


    });</script>
<style>
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }



    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
    }

    .inputfile:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }
    .box {
        padding: 0px;
    }
</style>
<script>
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input)
    {
        var label = input.nextElementSibling,
                labelVal = label.innerHTML;
        input.addEventListener('change', function (e)
        {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();
            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });</script>