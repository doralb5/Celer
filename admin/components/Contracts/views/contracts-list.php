<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Contracts');

foreach ($contracts as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_contract_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		new Button('<i class="fa fa-eye"></i>', $url_contract_details.'/'.$row->id, 'xs', '', '[$Details]'),
		);
}
$table->setElements($contracts);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('number', '[$Number]', 'string', 'left'),
	new TableList_Field('company_name', '[$Company]', 'string', 'left'),
	new TableList_Field('contact_fullname', '[$Contact]', 'string', 'left'),
	new TableList_Field('date', '[$Date]', 'date', 'left'),
	new TableList_Field('status', '[$Status]', 'string', 'left'),
));
$table->setUrl_action($url_contracts_list);
$table->setUrl_delete($url_contract_delete);
$table->multipleDeletion(false);

$table->render();
