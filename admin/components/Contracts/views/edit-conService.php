<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--<script>(function (e, t, n) {
        var r = e.querySelectorAll("html")[0];
        r.className = r.className.replace(/(^|\s)no-js(\s|$)/, "$1js$2")
    })(document, window, 0);</script>
<hr>-->

<div class="ui-sortable">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$Details]</h4>
            </div>
            <div class="panel-body">
                <form role="form" method="post">
                    <?php
					if ($serModel->filename != '') {
						echo $serModel_content;
					} else {
						?>

                        <div class="row">
                            <div class="col-md-6">
                                <?php foreach ($modelFields as $field) {
							?>
                                    <div class="form-group">
                                        <label><?= $field->name ?></label>


                                        <!--TextBox-->
                                        <?php if ($field->type == 'text') {
								?>
                                            <input type="text" class="form-control" name="f_<?= Utils::clean($field->name) ?>" <?= ($field->required) ? 'required' : '' ?> value="<?= $index['f_'.Utils::clean($field->name)] ?>">
                                        <?php
							} ?>
                                        <!-------------->

                                        <!--Number-->
                                        <?php if ($field->type == 'number') {
								?>
                                            <input type="number" class="form-control" name="f_<?= Utils::clean($field->name) ?>" <?= ($field->required) ? 'required' : '' ?> value="<?= $index['f_'.Utils::clean($field->name)] ?>">
                                        <?php
							} ?>
                                        <!-------------->

                                        <!--TextArea-->
                                        <?php if ($field->type == 'textarea') {
								?>
                                            <textarea style="max-width: 100%" rows="4" class="form-control" name="f_<?= Utils::clean($field->name) ?>"   <?= ($field->required) ? 'required' : '' ?>><?= $index['f_'.Utils::clean($field->name)] ?></textarea>
                                        <?php
							} ?>
                                        <!-------------->
                                    </div>
                                <?php
						} ?>
                            </div>
                        </div>
                    <?php
					} ?>


                    <div class="form-group">
                        <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>