<div class="ui-sortable">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$MainData]</h4>
            </div>
            <div class="panel-body">
                <form class="" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-4">
                        <label>[$ModelName]<span class="required">*</span></label>
                        <input type="text" class="form-control" name="modelname"  value="<?= $serModel->name ?>" placeholder="[$ModelName]" required="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="modelView">[$LayoutFile]</label>
                        <input name="modelview" type="file" class="form-control-file" id="modelView">
                    </div>
                    <div class="col-md-4">
                        <br/>
                        <button type="submit" name="change" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button
                    </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end panel -->
</div>
</div>

<?php if (!is_null($serModel->id)) {
	?>

    <div class="ui-sortable">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$ModelItems]</h4>
                </div>
                <div class="panel-body">
                    <p>&nbsp;</p>
                    <?php
					include(VIEWS_PATH.'TableList_class.php');
	$table = new TableListView('ServiceModels', 'list');
	$table->setElements($modelFields);
	$table->setFields(array(
						new TableList_Field('id', '[$Id]', 'int', 'left'),
						new TableList_Field('name', '[$Name]', 'string', 'left'),
						new TableList_Field('type', '[$Type]', 'string', 'left'),
						new TableList_Field('required', '[$Required]', 'bool', 'left'),
					));
	$table->setUrl_delete($url_modelField_delete);
	$table->setUrl_delete_params(array('id_model' => $serModel->id));
	$table->renderTopBar(false);
	$table->multipleDeletion(false);
	$table->render(); ?>

                    <form role="form" method="post">
                        <div class="col-md-10" id="div-append">
                            <div class="row" id="content-append">
                                <div class="form-group col-sm-4">
                                    <label>[$InputName]<span class="required">*</span></label>
                                    <input type="text" class="form-control" name="name[]" placeholder="" required="">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>[$InputType]<span class="required">*</span></label>
                                    <select name='type[]' class="form-control">
                                        <option value="text">[$Text]</option>
                                        <option value="number">[$Number]</option>
                                        <option value="textarea">[$Textarea]</option>
                                        <!--                                    <option value="select">Selection List</option>
                                                                                <option value="radio">Radio Button</option>
                                                                                <option value="check">CheckBox</option>-->
                                    </select>
                                </div>

                                <div class="form-group col-sm-2">
                                    <label>[$Required]</label>
                                    <select name='required[]' class="form-control">
                                        <option value="0">[$no_option]</option>
                                        <option value="1">[$yes_option]</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <p>&nbsp;</p>
                            <button id="add-btn" type="button" name="add" class="btn btn-info pull-right"/><i class="fa fa-plus-circle"></i></button>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" name="save" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
} ?>

<script>
    $(document).ready(function () {

        $('#add-btn').click(function (event) {
            event.preventDefault();
            var content = $('#content-append').html();

            console.log(content);
            jQuery('#div-append').append('<div class="row">' + content + '</div>');
        });

    });
</script>