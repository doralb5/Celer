<?php
$name = isset($_POST['name']) ? $_POST['name'] : $service->name;
$description = isset($_POST['description']) ? $_POST['description'] : $service->description;
$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $service->id_category;
$id_model = isset($_POST['id_model']) ? $_POST['id_model'] : $service->id_model;
?>


<div class="ui-sortable">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$ContractData]</h4>
            </div>
            <div class="panel-body panel-form">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form" novalidate="">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3" for="name">[$Name] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="<?= $name ?>" id="name" name="name" placeholder="[$Name]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Description]</label>
                            <div class="col-md-9">
                                <textarea name="description" class="form-control" placeholder="[$Description]" rows="4" style="max-width:100%"><?= $description ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Category]</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_category">
                                    <?php foreach ($serCategories as $categ) {
	?>
                                        <option value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
                                    <?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Model]</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_model">
                                    <option value="0">--[$NoModel]--</option>
                                    <?php foreach ($serModels as $model) {
		?>
                                        <option value="<?= $model->id ?>" <?= ($model->id == $id_model) ? 'selected' : '' ?>><?= $model->name ?></option>
                                    <?php
	} ?>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <p class="text-center">
                            <button type="submit" name="save" class="btn btn-info">
                                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                            </button>
                        </p>
                    </div>
                    <p>&nbsp;</p>
                </form>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/js/form-plugins.demo.min.js'); ?>