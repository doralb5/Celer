<div class="ui-sortable">
    <div class="col-md-6">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$ServiceStatus]</h4>
            </div>
            <div class="panel-body">
                <form class="" method="post">
                    <div class="form-group">
                        <label>[$Status]</label>
                        <div class="input-group">
                            <select name="status" class="form-control">
                                <?php if ($conService->status == 'active' || $conService->status == 'suspended') {
	?>
                                    <option value="active" <?= ($conService->status == 'active') ? 'selected' : '' ?>>active</option>
                                    <option value="suspended" <?= ($conService->status == 'suspended') ? 'selected' : '' ?>>suspended</option>
                                    <option value="cancelled" <?= ($conService->status == 'cancelled') ? 'selected' : '' ?>>cancelled</option>
                                <?php
} elseif ($conService->status == 'cancelled') {
		?>
                                    <option value="cancelled" <?= ($conService->status == 'cancelled') ? 'selected' : '' ?>>cancelled</option>
                                <?php
	} else {
		?>
                                    <option value="working" <?= ($conService->status == 'working') ? 'selected' : '' ?>>working</option>
                                    <option value="active" <?= ($conService->status == 'active') ? 'selected' : '' ?>>active</option>
                                    <option value="suspended" <?= ($conService->status == 'suspended') ? 'selected' : '' ?>>suspended</option>
                                    <option value="cancelled" <?= ($conService->status == 'cancelled') ? 'selected' : '' ?>>cancelled</option>
                                <?php
	} ?>
                            </select>
                            <div class="input-group-btn">
                                <button class="btn btn-info" name="save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>