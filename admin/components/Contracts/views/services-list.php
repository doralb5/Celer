<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Services');

foreach ($services as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_service_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		);
}
$table->setElements($services);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('category', '[$Category]', 'date', 'left'),
));
$table->setUrl_action($url_services_list);
$table->setUrl_delete($url_service_delete);
$table->multipleDeletion(false);

$table->render();
