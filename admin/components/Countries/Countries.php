<?php

class Countries_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function ajx_getCities($id_country)
	{
		$cities = $this->model->getCities("id_country = {$id_country}");
		$options = array();
		foreach ($cities as $city) {
			array_push($options, array('text' => $city->city, 'value' => $city->id));
		}
		header('Content-Type: application/json');
		echo json_encode(array('options' => $options));
	}
}
