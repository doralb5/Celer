<?php

class Etick_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getControllerUrl('Users/login'));
		}

		$this->view->set('url_event_list', $this->getActionUrl('eventList'));
		$this->view->set('url_place_edit', $this->getActionUrl('place_edit'));
		$this->view->set('url_event_delete', $this->getActionUrl('eventDelete'));
		$this->view->set('url_event_disable', $this->getActionUrl('eventDisable'));
		$this->view->set('url_event_enable', $this->getActionUrl('eventEnable'));
		$this->view->set('url_event_edit', $this->getActionUrl('eventEdit'));
		$this->view->set('url_reservation_edit', $this->getActionUrl('reservationEdit'));
		$this->view->set('url_reservation_delete', $this->getActionUrl('reservationDelete'));
		$this->view->set('url_reservation_enable', $this->getActionUrl('reservationEnable'));
		$this->view->set('url_reservation_list', $this->getActionUrl('reservationList'));
	}

	public function eventList()
	{
		Utils::saveCurrentPageUrl();

		$this->view->setTitle('EventList');
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';
		$status = isset($_GET['status']) ? $_GET['status'] : '';
		$enabled = ($status == 'enabled') ? 1 : ($status == 'disabled' ? 0 : '');
		$EtickEvents_tbl = TABLE_PREFIX.'etick_event';
		if ($enabled !== '') {
			$filter .= "AND $EtickEvents_tbl.enabled = '$enabled'";
		}

		//$events = $this->model->getEvents($elements_per_page, $offset, $filter);
		$events = $this->model->getEvents();
		$totalElements = $this->model->getLastCounter();
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('events', $events);
		$this->view->render('etick-events-list');
	}

	public function places_list()
	{
		$this->view->setTitle('PlacesList');
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';
		//$places = $this->model->getPlaces($elements_per_page, $offset, $filter);
		$places = $this->model->getPlaces();
		$totalElements = $this->model->getLastCounter();
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('places', $places);
		$this->view->render('etick-places-list');
	}

	public function place_edit($id = '')
	{
		if ($id != '') {
			$place = $this->model->getPlace($id);
			if ($place == null) {
				$this->view->AddError('no_such_place');
				$this->view->render('empty');
				return;
			}
		} else {
			require DOCROOT.ENTITIES_PATH.'Etick/EtickPlace.php';
			$place = new EtickPlace_Entity();
		}
		HeadHTML::setTitleTag('New Place'.' | '.CMSSettings::$website_title);
		$this->view->BreadCrumb->addDir('[$NewPlace]', $this->getActionUrl('place_edit'));
		if (isset($_POST['save_place'])) {
			($_POST['name'] == '') ? $this->AddError('Name is required!') : '';
			($_POST['address'] == '') ? $this->AddError('Address is required!') : '';
			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$image = time().'_'.$_FILES['image']['name'];
					$old_image = $place->image_map;
				} else {
					$image = $place->image_map;
					$old_image = null;
				}
				$place->city = $_POST['city'];
				$place->zip_code = $_POST['zip_code'];
				$place->province = $_POST['province'];
				$place->address = $_POST['address'];
				$place->name = $_POST['name'];
				$place->svg_map = $_POST['svg_map'];
				$place->image_map = $image;
				$place->description = $_POST['description'];
				$inserted_id = $this->model->savePlace($place);
				if (!is_array($inserted_id)) {
					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'Etick/place';
						Utils::createDirectory($target);
						$filename = $target.DS.$image;
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}
					$this->AddNotice('Place has been saved successfully.');
					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Palce', 'insert', 'Place inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('place_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Place', 'update', 'Place updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('place_edit')."/$id");
					}
				} else {
					$this->AddError('Saving failed!');
					$this->model->rollback();
				}
			}
		}
		$this->view->BreadCrumb->addDir('[$AddPlace]', $this->getActionUrl('place_edit'));
		$this->view->set('place', $place);
		$this->view->render('etick-place-edit');
	}

	public function eventEdit($id = '')
	{
		Utils::saveCurrentPageUrl();

		$places = $this->model->getPlaces();

		if ($id != '') {
			$event = $this->model->getEvent($id);
			if (is_null($event)) {
				$this->AddError("Event $id not found");
				$this->view->render('empty');
				return;
			} else {
				$zoneFilter = ' id_place = '.$event->id_place;
				$zones = $this->model->getZones('', '', $zoneFilter);
				$this->view->set('zones', $zones);
				$timeRuleFilter = ' cms_etick_reservtime.id_event = '.$event->id;
				$reservation_time_rules = $this->model->getReserveTimeRules(-1, 0, $timeRuleFilter);
				$this->view->set('reservation_time_rules', $reservation_time_rules);
			}

			HeadHTML::setTitleTag('Edit Event - '.$event->name.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditEvent]', $this->getActionUrl('eventEdit')."/$id");
		} else {
			require DOCROOT.ENTITIES_PATH.'Etick/EtickEvent.php';
			$event = new EtickEvent_Entity();
			HeadHTML::setTitleTag('New Event'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$NewEvent]', $this->getActionUrl('eventEdit'));
		}

		if (isset($_POST['save-time-rule'])) {
			$this->saveTimeRule($event->id);
		}

		if (isset($_POST['save-pricelist'])) {
			$this->savePriceRule($event->id);
		}

		if (isset($_POST['save'])) {
			($_POST['name'] == '') ? $this->AddError('Name is required!') : '';
			($_POST['description'] == '') ? $this->AddError('Description is required!') : '';
			($_POST['id_place'] == '') ? $this->AddError('Place is required!') : '';
			($_POST['startDate'] == '') ? $this->AddError('Start Date is required!') : '';
			($_POST['endDate'] == '') ? $this->AddError('End Date is required!') : '';
			($_POST['endDate'] == '') ? $this->AddError('End Date is required!') : '';
			($_POST['maxReservations'] == '') ? $this->AddError('Max reservations is required!') : '';
			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}
			if (count($this->view->getErrors()) == 0) {
				$this->model->startTransaction();
				if (strlen($_FILES['image']['tmp_name'])) {
					$image = time().'_'.$_FILES['image']['name'];
					$old_image = $event->image;
				} else {
					$image = $event->image;
					$old_image = null;
				}
				if (!is_null($id) && $event->id_place != $_POST['id_place']) {
					$old_place = $event->id_place;
				} else {
					$old_category = null;
				}
				$event->id_place = $_POST['id_place'];
				$event->name = $_POST['name'];
				$event->image = $image;
				$event->description = $_POST['description'];
				$event->start_date = $_POST['startDate'];
				$event->end_date = $_POST['endDate'];
				$event->maxReservations = $_POST['maxReservations'];
				$event->website = $_POST['website'];
				$event->pending_time_limit = ($_POST['pending_time_limit'] != '') ? $_POST['pending_time_limit'] : null;
				$inserted_id = $this->model->saveEvent($event);
				if (!is_array($inserted_id)) {
					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'etick';
						Utils::createDirectory($target);
						$filename = $target.DS.$image;
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}
					$this->AddNotice('Event has been saved successfully.');
					$this->model->commit();
					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Event', 'insert', 'Event inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('eventEdit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Event', 'update', 'Event updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('eventEdit')."/$id");
					}
				} else {
					$this->AddError('Saving failed!');
					$this->model->rollback();
				}
			}
		}

		//$prices = $this->model->getPrices();
		$prices = $this->model->getPrices('', 0, " cms_etick_price.id_event = '".$id."'");
		$places = $this->model->getPlaces();
		$seats = $this->model->getReservations(-1, 0, " eve.id = '".$id."'", ' seat.name ASC ');
		$userCategories = $this->model->getUserCategories();
		$this->view->set('userCategories', $userCategories);
		$this->view->set('seats', $seats);
		$this->view->set('event', $event);
		$this->view->set('places', $places);
		$this->view->set('prices', $prices);
		$this->view->render('etick-event-edit');
	}

	public function deleteReserveTimeRule($id)
	{
		$this->model->deleteReserveTimeRule($id);
		$this->view->AddNotice('[$time_rule_deleted]');
		Utils::backRedirect();
	}

	public function deletePrice($id)
	{
		$this->model->deletePrice($id);
		$this->view->AddNotice('[$price_rule_deleted]');
		Utils::backRedirect();
	}

	public function eventDelete($id)
	{
		$event = $this->model->deleteEvent($id);
		Utils::backRedirect($this->getActionUrl('eventList'));
	}

	public function eventEnable($id)
	{
		if (!is_null($id)) {
			$event = $this->model->getEvent($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$event->enabled = '1';
		$res = $this->model->saveEvent($event);
		if (!is_array($res)) {
			$this->view->AddNotice('Event has been enabled successfully.');
			$this->LogsManager->registerLog('Event', 'update', 'Event enabled with id : '.$event->id, $event->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect();
	}

	public function eventDisable($id)
	{
		if (!is_null($id)) {
			$event = $this->model->getEvent($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$event->enabled = '0';
		$res = $this->model->saveEvent($event);
		if (!is_array($res)) {
			$this->view->AddNotice('Event has been enabled successfully.');
			$this->LogsManager->registerLog('Event', 'update', 'Event enabled with id : '.$event->id, $event->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect();
	}

	public function confirmReservation($id)
	{
		$res = $this->model->getReservation($id);
		$res->confirmed = 1;
		$this->model->saveReservation($res);
		Utils::backRedirect($this->getActionUrl('reservationList'));
	}

	public function unconfirmReservation($id)
	{
		$res = $this->model->getReservation($id);
		$res->confirmed = 0;
		$this->model->saveReservation($res);
		Utils::backRedirect($this->getActionUrl('reservationList'));
	}

	public function reservationList()
	{
		Utils::saveCurrentPageUrl();

		$this->view->setTitle('ReservationList');

		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = ' 1 and res.id is not null ';

		if (isset($_GET['search'])) {
			if (isset($_GET['id_event']) && $_GET['id_event'] != '') {
				$filter .= ' AND eve.id =  '.$_GET['id_event'];
			}

			if (isset($_GET['FLname']) && $_GET['FLname'] != '') {
				$filter .= " AND CONCAT(user.firstname , ' ' , user.lastname) LIKE  '%".$_GET['FLname']."%' ";
			}

			if (isset($_GET['from_date']) && $_GET['from_date'] != '') {
				$filter .= " AND res.creation_date > '".$_GET['from_date']."' ";
			}

			if (isset($_GET['to_date']) && $_GET['to_date'] != '') {
				$filter .= " AND res.creation_date <  '".$_GET['to_date']."' ";
			}
			if (isset($_GET['payment_state']) && $_GET['payment_state'] != '') {
				$filter .= " AND res.confirmed =  '".$_GET['payment_state']."' ";
			}

			if (isset($_GET['id_res']) && $_GET['id_res'] != '') {
				$filter = ' 1 AND res.id =  '.$_GET['id_res'];
			}
		}

		$reservations = $this->model->getReservations($elements_per_page, $offset, $filter);

		$total_price = 0;
		$total_price_paid = 0;
		foreach ($reservations as $reservation) {
			$total_price = $total_price + $reservation->price;
			if ($reservation->confirmed == '1') {
				$total_price_paid = $total_price_paid + $reservation->price;
			}
		}

		$events = $this->model->getEvents();
		$totalElements = $this->model->getLastCounter();
		$this->view->set('url_enable', $this->getActionUrl('confirmReservation/'));
		$this->view->set('url_disable', $this->getActionUrl('unconfirmReservation/'));
		$this->view->set('events', $events);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('total_price', $total_price);
		$this->view->set('total_price_paid', $total_price_paid);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('reservations', $reservations);
		$this->view->render('etick-reservations-list');
	}

	public function ListEtick()
	{
		$elements_per_page = 10;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$filter = '';

		if (isset($_POST['search'])) {
			if (isset($_GET['id_res'])) {
				$filter .= ' AND cms_etick_event.id =  '.$_GET['id_res'];
			}

			if (isset($_GET['FLname'])) {
				$filter .= " AND CONCAT(user.firstname , ' ' , user.lastname) LIKE  ".$_GET['FLname'];
			}

			if (isset($_GET['start_date'])) {
				$filter .= ' AND res.creation_date > '.$_GET['start_date'];
			}

			if (isset($_GET['end_date'])) {
				$filter .= ' AND res.creation_date < =  '.$_GET['start_date'];
			}
		}

		$etick = $this->model->getEvents($elements_per_page, $offset, $filter, $sorting);
		$totalElements = $this->model->getLastCounter();
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		HeadHTML::setTitleTag('Etick'.' | '.CMSSettings::$website_title);
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		$this->view->set('realestates', $realestates);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'etick-reservation-list';
		$this->view->render($view);
	}

	public function reservationEdit($id = '')
	{
		if ($id != '') {
			$reservation = $this->model->getReservation($id);
		}
		if (is_null($reservation)) {
			$this->AddError("Reservation $id not found");
			$this->view->render('empty');
			return;
		}
		if (isset($_POST['save'])) {
			if ($reservation->confirmed != '1') {
				$reservation->confirmed = $_POST['confirmed'];
			}

			$reservation->note = $_POST['note'] != '' ? $_POST['note'] : null;

			$inserted_id = $this->model->saveReservation($reservation);
			if (!is_array($inserted_id)) {
				$this->AddNotice('Reservation has been saved successfully');
			} else {
				$this->AddError('Saving failed!');
			}
		}
		HeadHTML::setTitleTag('Edit Reservation - '.$reservation->id.' | '.CMSSettings::$website_title);
		$seats = $this->model->getSeats();
		$this->view->set('seats', $seats);
		$this->view->set('reservation', $reservation);
		$this->view->render('reservation-edit');
	}

	public function reservationDelete($id)
	{
		$this->model->deleteReservation($id);
		$this->view->AddNotice('[$reservation_deleted]');
		Utils::backRedirect();
	}

	public function reservationEnable($id)
	{
		var_dump($_POST);
		exit;
		$reservation = $this->model->getReservation($id);
		$reservation->confirmed = '1';
		$this->model->saveReservation;
		$this->view->AddNotice('[$reservation_confirmed]');
		Utils::backRedirect();
	}

	//*************** PRIVATE FUNCTIONS *************//

	private function saveTimeRule($event_id)
	{
		$inserted_id_array = array();
		if (!isset($_POST['userCategory'])) {
			$this->view->addError('[$no_rule_selected]');
			return;
		}
		foreach ($_POST['userCategory'] as $key => $user_cat) {
			require_once DOCROOT.ENTITIES_PATH.'Etick/EtickReserveTime.php';
			$rule = new EtickReserveTime_entity();
			$rule->id_event = $event_id;
			if (isset($_POST['userCategory'][$key])) {
				$rule->id_user_category = $_POST['userCategory'][$key];
			}
			if (isset($_POST['startTime'][$key]) && $_POST['startTime'][$key] != '' && isset($_POST['endTime'][$key]) && $_POST['endTime'][$key] != '') {

//                var_dump(strtotime($_POST['startTime'][$key]));
				//                var_dump(strtotime($_POST['startTime'][$key]) < strtotime($_POST['endTime'][$key])) ;
				//                exit;

				if (strtotime($_POST['startTime'][$key]) < strtotime($_POST['endTime'][$key])) {
					$rule->start_date = $_POST['startTime'][$key];
					$rule->end_date = $_POST['endTime'][$key];
				} else {
					$this->view->addError('[$start_time_end_time_conflict]');
					return;
				}
			} else {
				$rule->start_date = null;
				$rule->end_date = null;
				$this->view->addError('[$start_end_time_required]');
			}
			$res = $this->model->saveReserveTime($rule);

			if (!is_numeric($res)) {
				$this->view->AddError('[$error_saving_time_rule] ! ');
			} else {
				array_push($inserted_id_array, $res);
			}
		}
		$ids = ' ';
		if (count($inserted_id_array)) {
			foreach ($inserted_id_array as $insert) {
				$ids .= $insert.' , ';
			}
			$this->view->AddNotice('[$time_rules_inserted] : '.$ids);
		}
	}

	private function savePriceRule($event_id)
	{
		$inserted_id_array = array();
		foreach ($_POST['zona'] as $key => $zone) {
			require_once DOCROOT.ENTITIES_PATH.'Etick/EtickPrice.php';
			$pricelist = new EtickPrice_entity();
			$pricelist->id_event = $event_id;
			if (isset($_POST['userCategory'][$key])) {
				$pricelist->id_user_category = $_POST['userCategory'][$key];
			} else {
				$pricelist->id_user_category = null;
			}
			if (isset($_POST['seat'][$key]) && $_POST['seat'][$key] != '') {
				$pricelist->id_seat = $_POST['seat'][$key];
			} else {
				$pricelist->id_seat = null;
			}
			if (isset($_POST['price'][$key]) && $_POST['price'][$key] != '') {
				$pricelist->price = $_POST['price'][$key];
			} else {
				$this->view->AddError('[$price_cannot_be_empty]');
				$this->view->render('empty');
				return;
			}
			if (isset($_POST['zona'][$key]) && $_POST['zona'][$key] != '') {
				$pricelist->id_zona = $_POST['zona'][$key];
			} else {
				$this->view->AddError('[$zone_cannot_be_empty]');
				$this->view->render('empty');
				return;
			}
			$res = $this->model->savePrice($pricelist);
			if (!is_numeric($res)) {
				$this->view->AddError(' [$error_saving_price_rule] !  ');
			} else {
				array_push($inserted_id_array, $res);
			}
		}
		$ids = ' ';
		if (count($inserted_id_array)) {
			foreach ($inserted_id_array as $insert) {
				$ids .= $insert.' , ';
			}

			$this->view->AddNotice('[$price_rules_inserted] : '.$ids);
		}
	}
}
