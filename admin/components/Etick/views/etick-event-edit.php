<?php
$name = isset($_POST['name']) ? $_POST['name'] : $event->name;
$description = isset($_POST['description']) ? $_POST['description'] : $event->description;
$startDate = isset($_POST['startDate']) ? $_POST['startDate'] : $event->start_date;
$endDate = isset($_POST['endDate']) ? $_POST['endDate'] : $event->end_date;
$enabled = isset($_POST['enabled']) ? $_POST['enabled'] : $event->enabled;
$id_place = isset($_POST['id_place']) ? $_POST['id_place'] : $event->id_place;
$placeName = isset($_POST['placeName']) ? $_POST['placeName'] : $event->placeName;
$website = isset($_POST['website']) ? $_POST['website'] : $event->website;
$maxReservations = isset($_POST['maxReservations']) ? $_POST['maxReservations'] : $event->maxReservations;
$pending_time_limit = isset($_POST['pending_time_limit']) ? $_POST['pending_time_limit'] : $event->pending_time_limit;
?>


<style>


    .table-fixed {

    }
    .table-fixed tbody {
        height: 800px;
        overflow-y: auto;
        width: 100%;
    }
    .table-fixed thead, .table-fixed tbody, .table-fixed tr {
        display: block;
    }


</style>






<form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
      novalidate="" enctype="multipart/form-data">
    <div class="col-md-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$event_settings]</h4>
            </div>

            <div class="panel-body panel-form">

                <ul class="nav nav-tabs">
                    <li class=""><a
                            href="#tab" data-toggle="tab"></a></li>
                </ul>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$event_name]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $name ?>"
                               name="name" placeholder="[$Name]">

                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$event_start_date]</label>

                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" name="startDate" id="datetimepicker1" placeholder="[$chose_start_date]" value="<?= $startDate ?>" required="" data-parsley-id="0051"><ul class="parsley-errors-list" id="parsley-id-0051"></ul>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$event_end_date]</label>

                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" name="endDate" id="datetimepicker2" placeholder="[$chose_end_date]" value="<?= $endDate ?>" required="" data-parsley-id="0051"><ul class="parsley-errors-list" id="parsley-id-0051"></ul>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$image]</label>
                    <div class="col-md-6 col-sm-6">
                        <input type="file"  name="image">
                        <?php if ($event->image != '') {
	?>
                            <p>
                                <img
                                    src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'etick/'.$event->image ?>"
                                    width="100px" alt="Image"/></p>
                            <?php
} ?>
                    </div>
                </div>
                <hr>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$place]</label>
                    <div class="col-md-8 col-sm-8">
                        <select class="form-control" name="id_place">
                            <?php foreach ($places as $place) {
		?>
                                <option
                                    value="<?= $place->id ?>" <?= ($place->id == $id_place) ? 'selected' : '' ?>><?= $place->name ?></option>
                                <?php
	} ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$max_reservations]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="number" value="<?= $maxReservations ?>"
                               name="maxReservations" placeholder="[$insert_max_reservations]">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$pending_time_limit]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="number" value="<?= $pending_time_limit ?>"
                               name="pending_time_limit" placeholder="[$insert_pending_time_limit]">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$website]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $website ?>"
                               name="website" placeholder="[$Website]">

                    </div>
                </div>

            </div>
        </div>






        <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$event_description]</h4>
            </div>
            <div class="panel-body panel-form">
                <textarea class="ckeditor" id="editor1" name="description"><?= $description ?></textarea>
            </div>
        </div> 






    </div>


    <?php if (count($prices)) {
		?>

        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$price_list]</h4>
                </div>
                <div class="form-group">

                    <table class="table table-bordered table-fixed">         <thead>
                            <tr>
                            <th>[$user_category]</th>
                            <th>[$zone]</th>
                            <th>[$seat]</th>
                            <th>[$price]</th>
                            <th>[$action]</th>
                            </tr>
                        </thead>

                        <?php foreach ($prices as $price) {
			?>
                            <tr>
                            <td><?= $price->userCategory ?></td>
                            <td><?= $price->zoneName ?></td>
                            <td><?= $price->seatName ?></td>
                            <td><?= $price->price ?></td>
                            <td><a title="[$Delete]" class="btn btn-danger btn-xs deleteRow" data-toggle="modal" href="<?= Utils::getComponentUrl('Etick/deletePrice/').$price->id ?>" data-id="<?= $price->id ?>">
                                    <i class="fa fa-times"></i></a></td>
                            </tr>
                        <?php
		} ?>


                    </table> 


                </div>

            </div>

        </div>

    <?php
	} ?>





    <div class="col-md-12">
        <p class="text-center">
        <button type="submit" name="save" class="btn btn-info">
            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$save]
        </button>
        </p>
    </div>



</form>

<script>
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker();
    });
</script>


<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>

<script>
<?php foreach (CMSSettings::$available_langs as $lang) {
	?>

        CKEDITOR.replace('editor_<?= $lang ?>', {
            imageBrowser_listUrl: "<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).LIBS_PATH ?>JsonDirImages.php?media_path=<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).MEDIA_ROOT ?>&docroot=<?= rtrim(DOCROOT, '/') ?>",
                    height: 232
                });

<?php
} ?>

</script>







<?php if (isset($zones)) {
		?>

    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab" id="tab_ContractServices">
                <div class="row">
                    <form method="POST">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-heading-btn">

                                    </div>
                                    <h4 class="panel-title">[$insert_price_rule]</h4>
                                </div>
                                <div class="panel-body">

                                    <table id="tblServicesList" class="table">
                                        <thead>
                                            <th>[$price_rule]</th>
                                            <th>&nbsp;</th>
                                        </thead>
                                    </table>
                                    <div id="new-service-div">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-1 control-label">[$zone]</label>
                                                <div class="col-md-2">
                                                    <select class="form-control" id="zone"
                                                            ">
                                                        <option></option>
                                                        <?php foreach ($zones as $id => $zone) {
			?>
                                                            <option value="<?= $zone->id ?>"><?= $zone->name ?></option>
                                                        <?php
		} ?>
                                                    </select>
                                                </div>
                                                <label class="col-md-1 control-label">[$user_category]</label>
                                                <div class="col-md-2">
                                                    <select class="form-control" id="userCategory"
                                                            ">
                                                        <option></option>
                                                        <?php foreach ($userCategories as $id => $category) {
			?>
                                                            <option value="<?= $category->id ?>"><?= $category->category ?></option>
                                                        <?php
		} ?>
                                                    </select>
                                                </div>
                                                <label class="col-md-1 control-label">[$seat]</label>
                                                <div class="col-md-2">
                                                    <select class="form-control" id="seat"
                                                            >
                                                        <option></option>
                                                        <?php foreach ($seats as $id => $seat) {
			?>
                                                            <option value="<?= $seat->seat_id ?>"><?= $seat->seat_name ?></option>
                                                        <?php
		} ?>
                                                    </select>
                                                </div>
                                                <label class="col-md-1 control-label">[$price]</label>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" value="" id="price">
                                                </div>
                                                <div class="col-md-1 text-right">
                                                    <button type="button" name="AddCService" id="btnAddService" class="btn btn-info"><i
                                                            class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p class="text-center">
                                <button type="submit" name="save-pricelist" class="btn btn-info">
                                    <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$save_price_rule]
                                </button>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




    <?php if (count($reservation_time_rules)) {
			?>

        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$reserv_time_rule_list]</h4>
                </div>
                <div class="form-group">

                    <table class="table table-bordered">         <thead>
                            <tr>
                            <th>[$user_category]</th>
                            <th>[$start_time]</th>
                            <th>[$end_time]</th>
                            </tr>
                        </thead>

                        <?php foreach ($reservation_time_rules as $rule) {
				?>
                            <tr>
                            <td><?= $rule->user_category ?></td>
                            <td><?= $rule->start_date ?></td>
                            <td><?= $rule->end_date ?></td>
                            <td><a title="[$Delete]" class="btn btn-danger btn-xs deleteRow" data-toggle="modal" href="<?= Utils::getComponentUrl('Etick/deleteReserveTimeRule/').$rule->id ?>" data-id="<?= $price->id ?>">
                                    <i class="fa fa-times"></i></a></td>
                            </tr>
                        <?php
			} ?>


                    </table> 


                </div>

            </div>

        </div>

    <?php
		} ?>





    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab" id="tab_ContractServices">
                <div class="row">
                    <form method="POST">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-heading-btn">

                                    </div>
                                    <h4 class="panel-title">[$insert_reservation_time_rules]</h4>
                                </div>
                                <div class="panel-body">

                                    <table id="tblTimeRule" class="table">
                                        <thead>
                                            <th>[$time_rule]</th>
                                            <th>&nbsp;</th>
                                        </thead>
                                    </table>
                                    <div id="new-service-div">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-1 control-label">[$user_category]</label>
                                                <div class="col-md-2">
                                                    <select class="form-control" id="userCategoryTimeRule"
                                                            ">
                                                        <option></option>
                                                        <?php foreach ($userCategories as $id => $category) {
			?>
                                                            <option value="<?= $category->id ?>"><?= $category->category ?></option>
                                                        <?php
		} ?>
                                                    </select>
                                                </div>



                                                <label class="control-label col-md-2 col-sm-2">[$reservation_start_date]</label>

                                                <div class="col-md-2 col-sm-2">
                                                    <input type="text" class="form-control" name="start_time" id="start_time" placeholder="[$chose_start_date]" value="" required="" data-parsley-id="0051"><ul class="parsley-errors-list" id="parsley-id-0051"></ul>
                                                </div>


                                                <label class="control-label col-md-2 col-sm-2">[$reservation_end_date]</label>

                                                <div class="col-md-2 col-sm-2">
                                                    <input type="text" class="form-control" name="end_time" id="end_time" placeholder="[$chose_end_date]" value=""  data-parsley-id="0051"><ul class="parsley-errors-list" id="parsley-id-0051"></ul>
                                                </div>


                                                <div class="col-md-1 text-right">
                                                    <button type="button" name="[$add_reservation_time_rule]" id="btnAddTimeRule" class="btn btn-info"><i
                                                            class="fa fa-plus"></i>
                                                    </button>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p class="text-center">
                                <button type="submit" name="save-time-rule" class="btn btn-info">
                                    <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$save_time_rule]
                                </button>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>








    <script>


        function addPriceRule(zone, servicefull, id_zone, userCategory, seat, price) {
            var count = $('#tblServicesList tr').length + 1;
            $('#tblServicesList').append('<tr data-id="' + count + '"><td>'
                    + '<input type="hidden" name="zona[' + count + ']" value="' + id_zone + '" />'
                    + '<input type="hidden" name="userCategory[' + count + ']" value="' + userCategory + '" />'
                    + '<input type="hidden" name="seat[' + count + ']" value="' + seat + '" />'
                    + '<input type="hidden" name="price[' + count + ']" value="' + price + '" />'
                    + servicefull
                    + '</td><td><button class="btn-small" onclick="deleteService(' + count + ')">[$remove]</button></td></tr>');

            if ($('#tblServicesList').is(':hidden'))
                $('#tblServicesList').show();
        }
        function deleteService(rowId) {
            $('#tblServicesList').find('[data-id="' + rowId + '"]').remove();
            if ($('#tblServicesList tr[data-id]').length == 0)
                $('#tblServicesList').hide();
        }
        $('#btnAddService').click(function (e) {
            var id_zone = $('#zone').val();
            var userCategory = $('#userCategory').val();
            var seat = $('#seat').val();
            var price = $('#price').val();
            if (id_zone !== '' && price !== '') {
                var zone = $('#zone option:selected').text();
                var servicefull = ' <b> [$zone] </b> : ' + zone;
                if (userCategory !== '') {
                    var userCategoryLabel = $('#userCategory option:selected').text();
                    servicefull = servicefull + ' » ' + ' <b> [$user_category] </b> : ' + userCategoryLabel;
                }
                if (seat !== '') {
                    var seatLabel = $('#seat option:selected').text();
                    servicefull = servicefull + ' » ' + ' <b> [$seat] </b> : ' + seatLabel;
                }
                servicefull = servicefull + ' » ' + ' <b> [$price] </b> : ' + price;
                addPriceRule(zone, servicefull, id_zone, userCategory, seat, price);
            } else {
                alert('[$zone_and_price_required]');
            }
        });




        function addTimeRule(start_time, servicefull_TimeRule, end_time, userCategoryTimeRule) {
            var count = $('#tblTimeRule tr').length + 1;
            $('#tblTimeRule').append('<tr data-id="' + count + '"><td>'
                    + '<input type="hidden" name="startTime[' + count + ']" value="' + start_time + '" />'
                    + '<input type="hidden" name="userCategory[' + count + ']" value="' + userCategoryTimeRule + '" />'
                    + '<input type="hidden" name="endTime[' + count + ']" value="' + end_time + '" />'
                    + servicefull_TimeRule
                    + '</td><td><button class="btn-small" onclick="deleteService(' + count + ')">[$remove]</button></td></tr>');

            if ($('#tblTimeRule').is(':hidden'))
                $('#tblTimeRule').show();
        }
        function deleteService(rowId) {
            $('#tblTimeRule').find('[data-id="' + rowId + '"]').remove();
            if ($('#tblTimeRule tr[data-id]').length == 0)
                $('#tblTimeRule').hide();
        }
        $('#btnAddTimeRule').click(function (e) {
            var start_time = $('#start_time').val();
            var end_time = $('#end_time').val();
            var userCategoryTimeRule = $('#userCategoryTimeRule').val();
            if (start_time !== '' && userCategoryTimeRule !== '' && end_time !== '') {
                var userCategory_text = $('#userCategoryTimeRule option:selected').text();
                var servicefull_TimeRule = ' <b> [$user_category] </b> : ' + userCategory_text;
                servicefull_TimeRule = servicefull_TimeRule + ' » ' + ' <b>[$reservation_start_time]</b> : ' + start_time;

                if (end_time !== '') {

                    servicefull_TimeRule = servicefull_TimeRule + ' » ' + ' <b>[$reserve_end_time]</b> : ' + end_time;
                }

                addTimeRule(start_time, servicefull_TimeRule, end_time, userCategoryTimeRule);
            } else {
                alert('[$all_the_fields_are_required]');
            }
        });


        $(document).ready(function () {
            $('#start_time').datetimepicker();
            $('#end_time').datetimepicker();
        });





    </script>
    <?php
	}?>