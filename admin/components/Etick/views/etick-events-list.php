<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('EtickEvents');

foreach ($events as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_event_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_event_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_event_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		//new Button('<i class="fa fa-eye"></i>', $url_business_details . '/' . $row->id, 'xs', '', '[$Details]'),
	);
	$row->row_imagePath = 'etick';
}
$table->setElements($events);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$id]', 'int', 'left'),
	new TableList_Field('name', '[$name]', 'text', 'left'),
	new TableList_Field('start_date', '[$start_date]', 'text', 'left'),
	new TableList_Field('end_date', '[$end_date]', 'text', 'left'),
	new TableList_Field('placeName', '[$place_name]', 'text', 'left'),
	new TableList_Field('image', '[$image]', 'image', 'left'),
	//new TableList_Field('category', '[$Category]', 'string', 'left'),
	new TableList_Field('maxReservations', '[$max_reservations]', 'integer', 'left'),
	new TableList_Field('enabled', '<i class="fa fa-square-o"></i>', 'bool', 'left', 'no-sort'),
));
$table->setUrl_action($url_event_list);
$table->setUrl_delete($url_event_delete);
//$table->addSelectFilter($enabledSelectFil);
//$table->addSelectFilter($profileSelectFil);
$table->render();
