<?php
$name = isset($_POST['name']) ? $_POST['name'] : $place->name;
$description = isset($_POST['description']) ? $_POST['description'] : $place->description;
$address = isset($_POST['address']) ? $_POST['address'] : $place->address;
$svg_map = isset($_POST['svg_map']) ? $_POST['svg_map'] : $place->svg_map;
$city = isset($_POST['city']) ? $_POST['city'] : $place->city;
$zip_code = isset($_POST['zip_code']) ? $_POST['zip_code'] : $place->zip_code;
$province = isset($_POST['province']) ? $_POST['province'] : $place->province;
$address = isset($_POST['address']) ? $_POST['address'] : $place->address;
?>


<form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
      novalidate="" enctype="multipart/form-data">
    <div class="col-md-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$place_settings]</h4>
            </div>

            <div class="panel-body panel-form">

                <ul class="nav nav-tabs">
                    <li class=""><a
                            href="#tab" data-toggle="tab"></a></li>
                </ul>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$place_name]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $name ?>"
                               name="name" placeholder="[$Name]">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$city]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $city ?>"
                               name="city" placeholder="[$city]">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$zip_code]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $zip_code ?>"
                               name="zip_code" placeholder="[$zip_code]">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$province]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $province ?>"
                               name="province" placeholder="[$province]">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$address]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $address ?>"
                               name="address" placeholder="[$address]">

                    </div>
                </div>


                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$place_description]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <textarea class="ckeditor" id="editor1" name="description"><?= $description ?></textarea>
                    </div>
                </div> 

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$svg_map]</label>
                    <div class="col-md-8 col-sm-8">
                        <textarea class="form-control"  
                                  name="svg_map" placeholder="[$svg_map]"> <?= $svg_map ?> </textarea>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$svg_map_preview]</label>
                    <div class="col-md-8 col-sm-8">
                       <?= $svg_map ?>

                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$image]</label>
                    <div class="col-md-6 col-sm-6">
                        <input type="file" name="image">
                        <?php if ($place->image_map != '') {
	?>
                            <p>
                                <img
                                    src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'Etick/place/'.$place->image_map ?>"
                                    width="100px" alt="Image"/></p>
                            <?php
} ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$place_address]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $address ?>"
                               name="address" placeholder="[$Address]">

                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>



    <div class="col-md-12">
        <p class="text-center">
            <button type="submit" name="save_place" class="btn btn-info">
                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$save]
            </button>
        </p>
    </div>



</form>

<script>
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker();
    });
</script>


<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>

<script>
<?php foreach (CMSSettings::$available_langs as $lang) {
	?>

        CKEDITOR.replace('editor_<?= $lang ?>', {
            imageBrowser_listUrl: "<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).LIBS_PATH ?>JsonDirImages.php?media_path=<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).MEDIA_ROOT ?>&docroot=<?= rtrim(DOCROOT, '/') ?>",
                    height: 232
                });

<?php
} ?>

</script>














<script>
    function addTmpService(zone, servicefull, id_zone, userCategory, seat, price) {
        var count = $('#tblServicesList tr').length + 1;
        $('#tblServicesList').append('<tr data-id="' + count + '"><td>'
                + '<input type="hidden" name="zona[' + count + ']" value="' + id_zone + '" />'
                + '<input type="hidden" name="userCategory[' + count + ']" value="' + userCategory + '" />'
                + '<input type="hidden" name="seat[' + count + ']" value="' + seat + '" />'
                + '<input type="hidden" name="price[' + count + ']" value="' + price + '" />'
                + servicefull
                + '</td><td><button class="btn-small" onclick="deleteService(' + count + ')">Rimuovi</button></td></tr>');

        if ($('#tblServicesList').is(':hidden'))
            $('#tblServicesList').show();
    }
    function deleteService(rowId) {
        $('#tblServicesList').find('[data-id="' + rowId + '"]').remove();
        if ($('#tblServicesList tr[data-id]').length == 0)
            $('#tblServicesList').hide();
    }
    $('#btnAddService').click(function (e) {
        var id_zone = $('#zone').val();
        var userCategory = $('#userCategory').val();
        var seat = $('#seat').val();
        var price = $('#price').val();
        if (id_zone !== '' && price !== '') {
            var zone = $('#zone option:selected').text();
            var servicefull = ' <b> Zona </b> : ' + zone;
            if (userCategory !== '') {
                var userCategoryLabel = $('#userCategory option:selected').text();
                servicefull = servicefull + ' » ' + ' <b> Categoria Utente </b> : ' + userCategoryLabel;
            }
            if (seat !== '') {
                var seatLabel = $('#seat option:selected').text();
                servicefull = servicefull + ' » ' + ' <b>Sedia</b> : ' + seatLabel;
            }
            servicefull = servicefull + ' » ' + ' <b>Prezzo</b> : ' + price;
            addTmpService(zone, servicefull, id_zone, userCategory, seat, price);
        } else {
            alert('Zone and price required');
        }
    });

<?if (isset($place->svg_map) && ($place->svg_map != null || $place->svg_map != '')) {
		?>
</script>
<script src="https://ariutta.github.io/svg-pan-zoom/dist/svg-pan-zoom.js"></script> 
<script>
    $(function () {
        panZoomInstance = svgPanZoom('#svg-id', {
            zoomEnabled: true,
            controlIconsEnabled: true,
            fit: true,
            center: true,
            minZoom: 1.0
        });

        // zoom out
        panZoomInstance.zoom(0.2)

        $("#move").on("click", function () {
            // Pan by any values from -80 to 80
            panZoomInstance.panBy({x: Math.round(Math.random() * 160 - 80), y: Math.round(Math.random() * 160 - 80)})
        });
    })
</script>
<?
	}?>