<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('EtickEvents');

foreach ($places as &$row) {
	$row->rows_buttons = array(
		$btn1 =
		new Button('<i class="fa fa-pencil"></i>', $url_place_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		//new Button('<i class="fa fa-eye"></i>', $url_business_details . '/' . $row->id, 'xs', '', '[$Details]'),
	);
	$row->row_imagePath = 'etick';
}
$table->setElements($places);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$id]', 'int', 'left'),
	new TableList_Field('name', '[$name]', 'text', 'left'),
	//new TableList_Field('image', '[$image]', 'image', 'left'),
	//new TableList_Field('category', '[$Category]', 'string', 'left'),
));
$table->setUrl_action($url_event_list);
$table->setUrl_delete($url_event_delete);
//$table->addSelectFilter($enabledSelectFil);
//$table->addSelectFilter($profileSelectFil);
$table->render();
