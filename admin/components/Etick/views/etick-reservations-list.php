<?php
$id_reservation = isset($_GET['id_res']) ? $_GET['id_res'] : '';
$fl_name = isset($_GET['FLname']) ? $_GET['FLname'] : '';
$from_date = isset($_GET['from_date']) ? $_GET['from_date'] : '';
$to_date = isset($_GET['to_date']) ? $_GET['to_date'] : '';
$id_event = isset($_GET['id_event']) ? $_GET['id_event'] : '';
$payment_state = isset($_GET['payment_state']) ? $_GET['payment_state'] : '';
?>
<form method="get" action="">
    <div class=" col-md-12">
        <div class="row">

            <div class="col-md-1">
                <input class="form-control" type="text" name="id_res" value="<?= $id_reservation ?>" placeholder="[$id_reservation]">
            </div>


            <div class="col-md-2">
                <input class="form-control" type="text" name="FLname" value="<?= $fl_name ?>" placeholder="[$f_l_name]">
            </div>


            <div class="col-md-2">
                <input type="text" class="form-control" name="from_date" id="datetimepicker4" placeholder="[$from_date]" value="<?= $from_date ?>" >
            </div>

            <div class="col-md-2">
                <input type="text" class="form-control" name="to_date" id="datetimepicker5" placeholder="[$to_date]" value="<?= $to_date ?>">
            </div>
            <div class="col-md-2">
                <select name="payment_state" class="form-control">
                    <option value="">[$stato_pagamento_tutti]
                    </option>
                    <option value="1" <?php if ($payment_state == '1') {
	echo ' selected ';
} ?>>[$paid]
                    </option>
                    <option value="0" <?php if ($payment_state == '0') {
	echo ' selected ';
} ?> >[$not_paid]
                    </option>
                </select>
            </div>

            <div class="col-md-2">
                <select class="form-control" name="id_event" >
                    <option value="">[$all_events]</option>
                    <?php foreach ($events as $event) {
	?>
                        <option value="<?= $event->id ?>" <?php if ($id_event == $event->id) {
		echo ' selected ';
	} ?> ><?= $event->name ?></option>
                    <?php
} ?>
                </select>
            </div>

            <div class="col-md-1">
                <span class="input-group-btn">
                    <button type="submit" name="search" class="btn btn-info btn-block"><i class="fa fa-filter"></i>&nbsp;&nbsp;Search
                    </button>
                </span>
            </div>
        </div>
        <br>
    </div>
</form>
<script>
    $(document).ready(function () {
        $('#datetimepicker4').datetimepicker();
        $('#datetimepicker5').datetimepicker();
    });
</script>
<?php
include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('EtickReserva');

foreach ($reservations as &$row) {
	if ($row->confirmed) {
		$btn_enable = new Button("<i class='fa fa-circle-o'></i>", $url_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn_enable = new Button("<i class='fa fa-check-circle-o'></i>", $url_enable.'/'.$row->id, 'xs', 'btn-success', '[$Confirm]');
	}
	$row->rows_buttons = array(
		$btn_enable,
		$btn1 = new Button('<i class="fa fa-pencil"></i>', $url_reservation_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
			//new Button('<i class="fa fa-eye"></i>', $url_business_details . '/' . $row->id, 'xs', '', '[$Details]'),
	);
	$row->row_imagePath = 'etick';
}
$table->setElements($reservations);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$ReservationId]', 'int', 'left'),
	new TableList_Field('fullname', '[$Name]', 'text', 'left'),
	new TableList_Field('seat_name', '[$seat]', 'text', 'left'),
	//new TableList_Field('creation_date', '[$DateOfReservation]', 'textdate', 'left'),
	new TableList_Field('price', '[$price]', 'currsm', 'left'),
	new TableList_Field('event_name', '[$event_name]', 'text', 'left'),
	//new TableList_Field('email', '[$email]', 'text', 'left'),
	//new TableList_Field('category', '[$Category]', 'string', 'left'),
	//new TableList_Field('maxReservations', '[$MaxReservations]', 'integer', 'left'),
	new TableList_Field('confirmed', '[$Confirmed]', 'bool', 'left', 'no-sort'),
));

$table->setUrl_action($url_reservation_list);
$table->setUrl_delete($url_reservation_delete);

$table->setUrl_enable($url_reservation_enable);
//$table->addSelectFilter($enabledSelectFil);
//$table->addSelectFilter($profileSelectFil);
$table->render();
?>

<div class="row">
    <div class="col-md-6 ui-sortable col-md-offset-3">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">[$accounting_state]</h4>
            </div>
            <div class="form-group">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th>[$total_price]</th><th>[$total_price_paid]</th><th>[$total_price_to_pay]</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="color: black;">&euro; <?= number_format($total_price, 2, ',', '.') ?></td><td style="color: green;">&euro; <?= number_format($total_price_paid, 2, ',', '.') ?></td><td style="color: red;">&euro; <?= number_format($total_price - $total_price_paid, 2, ',', '.') ?></td>
                        </tr>
                    <tbody>        
                </table>
            </div>
        </div>
    </div>
</div>
