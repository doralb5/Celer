<?php
$eventname = $reservation->event_name;
$name = $reservation->username;
$firstname = $reservation->firstname;
$lastname = $reservation->lastname;
$email = $reservation->email;
$creationDate = $reservation->creation_date;
$confirmed = $reservation->confirmed;
$SeatLabel = $reservation->seat_name;
$zoneLabel = $reservation->zone_name;
$note = $reservation->note;

?>


<form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
      novalidate="" enctype="multipart/form-data">
    <div class="col-md-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$UserInformation]</h4>
            </div>

            <div class="panel-body panel-form">

                <ul class="nav nav-tabs">
                    <li class=""><a
                            href="#tab" data-toggle="tab"></a></li>
                </ul>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$username]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $name ?>"
                               name="name" placeholder="[$Username]" disabled="true">

                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$name]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $firstname ?>"
                               name="firstname" placeholder="[$Name]" disabled="true">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$lastname]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $lastname ?>"
                               name="lastname" placeholder="[$Lastname]" disabled="true">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$email]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $email ?>"
                               name="email" placeholder="[$Email]" disabled="true">

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="col-md-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$ReservationInfo]</h4>
            </div>

            <div class="panel-body panel-form">

                <ul class="nav nav-tabs">
                    <li class=""><a
                            href="#tab" data-toggle="tab"></a></li>
                </ul>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$eventName]</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $eventname ?>"
                               name="eventname" placeholder="[$EventName]" disabled="true">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$seatName]</label>
                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" disabled="true" value="<?= $SeatLabel ?>"><ul class="parsley-errors-list" id="parsley-id-0051"></ul>

                    </div>
                </div>




                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$zoneName]</label>
                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" disabled="true" value="<?= $zoneLabel ?>"><ul class="parsley-errors-list" id="parsley-id-0051"></ul>
                    </div>
                </div>






                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$startDate]</label>

                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" disabled="true" name="creation_date" id="datetimepicker1" placeholder="[$ChoseStartDate]" value="2017-01-30 02:06:08" required="" data-parsley-id="0051"><ul class="parsley-errors-list" id="parsley-id-0051"></ul>
                    </div>
                </div>

                <div class="panel-body panel-form">
                    <div class="form-group">
                        <label class="col-md-4 control-label">[$Confirmed]</label>


                        <div class="col-md-8">
                            <?php if ($confirmed != '1') {
	?>
                                <select name="confirmed" class="form-control">
                                    <option value="1" <?= ($confirmed == '1') ? 'selected' : '' ?>>[$yes_option]
                                    </option>
                                    <option value="0" <?= ($confirmed == '0') ? 'selected' : '' ?>>[$no_option]
                                    </option>
                                </select>
                            <?php
} else {
		?>
                                <input  class="form-control" type="text" value="[$yes_option]" disabled="true">
                            <?php
	} ?>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">[$price]</label>

                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" disabled="true" name="price"  placeholder="00,00" value=" &euro; <?= number_format($reservation->price, 2, ',', $thousands_sep = '.') ?>" required="" data-parsley-id="0051"><ul class="parsley-errors-list"></ul>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">[$notes]</label>

                        <div class="col-md-8 col-sm-8">
                            <textarea type="text" rows="4" class="form-control"  name="note"  placeholder="[$insert_notes_here]" ><?=$note?></textarea><ul class="parsley-errors-list"></ul>
                        </div>
                    </div>



                </div>
            </div>
            <hr>
        </div>
    </div>



    <div class="col-md-12">
        <p class="text-center">
        <button type="submit" name="save" class="btn btn-info">
            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$save]
        </button>
        </p>
    </div>



</form>




<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>


