<?php

class Events_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Events/event_edit/');

		$this->view->set('url_events_list', $this->getActionUrl('events_list'));
		$this->view->set('url_event_edit', $this->getActionUrl('event_edit'));
		$this->view->set('url_event_delete', $this->getActionUrl('event_delete'));
		$this->view->set('url_event_enable', $this->getActionUrl('event_enable'));
		$this->view->set('url_event_disable', $this->getActionUrl('event_disable'));
		$this->view->set('url_event_details', $this->getActionUrl('event_details'));

		$this->view->set('url_categories_list', $this->getActionUrl('categories_list'));
		$this->view->set('url_category_edit', $this->getActionUrl('category_edit'));
		$this->view->set('url_category_delete', $this->getActionUrl('category_delete'));

		//Additional Images
		$this->view->set('url_addtImage_delete', $this->getActionUrl('addtImage_delete'));
		$this->view->set('url_addtImage_download', $this->getActionUrl('addtImage_download'));
		$this->view->set('url_addtImage_show', substr(WEBROOT, 0, -6).MEDIA_ROOT.'events');

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function events_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Events'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$event = $this->model->getEvent($id);
				$additionalImages = $this->model->getEventImages(100, 0, "id_event = $id");
				$res = $this->model->deleteEvent($id);
				if ($res !== false) {
					$target = DOCROOT.MEDIA_ROOT.'events'.DS.$event->id_category;
					if ($event->image != '' && file_exists($target.$event->image)) {
						unlink($target.$event->image);
					}

					foreach ($additionalImages as $image) {
						if (file_exists($target.DS.'additional'.DS.$image->image)) {
							unlink($target.DS.'additional'.DS.$image->image);
						}
					}
					$this->LogsManager->registerLog('Event', 'delete', 'Event deleted with id : '.$id, $id);
				}
			}
			if ($res !== false) {
				$this->view->AddNotice('Events has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('events_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'Event.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'Event.title', 'peso' => 90),
				array('field' => TABLE_PREFIX.'EventCategory.category', 'peso' => 80),
			);
			$events = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$events = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($events as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddEvent]', $this->getActionUrl('event_edit')));

		$this->view->BreadCrumb->addDir('[$Events]', $this->getActionUrl('events_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('events', $events);
		$this->view->setTitle('[$Events]');
		$this->view->render('events-list');
	}

	public function event_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Events]', $this->getActionUrl('events_list'));
		if (!is_null($id)) {
			$event = $this->model->getEvent($id);
			HeadHTML::setTitleTag('Edit Event - '.$event->title.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditEvent]', $this->getActionUrl('event_edit')."/$id");
		} else {
			$event = new Event_Entity();
			HeadHTML::setTitleTag('New Event'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$NewEvent]', $this->getActionUrl('event_edit'));
		}

		$logged_user = UserAuth::getLoginSession();

		if ($event === false) {
			$this->AddError("Event $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['title'] == '') ? $this->AddError('Title is required!') : '';
			($_POST['description'] == '') ? $this->AddError('Description is required!') : '';
			($_POST['id_category'] == '') ? $this->AddError('Category is required!') : '';
			($_POST['start_date'] == '') ? $this->AddError('Start Date is required!') : '';
			($_POST['pay'] == '') ? $this->AddError('Pay is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $event->image;
				} else {
					$_POST['image'] = $event->image;
					$old_image = null;
				}

				if (strlen($_FILES['file']['tmp_name'])) {
					$_POST['file'] = time().'_'.$_FILES['file']['name'];
					$old_file = $event->file;
				} else {
					$_POST['file'] = $event->file;
					$old_file = null;
				}

				if (!is_null($id) && $event->id_category != $_POST['id_category']) {
					$old_category = $event->id_category;
				} else {
					$old_category = null;
				}

				$event->title = $_POST['title'];
				$event->description = $_POST['description'];
				$event->image = $_POST['image'];
				$event->id_category = $_POST['id_category'];
				$event->pay = $_POST['pay'];
				$event->cost = $_POST['cost'];
				$event->currency = $_POST['currency'];
				$event->start_date = $_POST['start_date'];
				$event->end_date = ($_POST['end_date'] != '') ? $_POST['end_date'] : $event->end_date;
				$event->organizer = $_POST['organizer'];
				$event->location = $_POST['location'];
				$event->address = $_POST['address'];
				$event->phone = $_POST['phone'];
				$event->email = $_POST['email'];
				$event->website = $_POST['website'];
				$event->file = $_POST['file'];
				$event->coordinates = ltrim(rtrim($_POST['coordinates'], ')'), '(');
				$event->featured = $_POST['featured'];

				$inserted_id = $this->model->saveEvent($event);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Event has been saved successfully.');

					//Ne rastin kur ndryshojme kategorine duhet te zhvendosen imazhet
					if (!is_null($id) && !is_null($old_category)) {
						$target = DOCROOT.MEDIA_ROOT.DS.'events'.DS;
						Utils::createDirectory($target.$event->id_category);
						Utils::createDirectory($target.$event->id_category.DS.'files');
						$this->moveAdditionalImages($id, $old_category, $event->id_category);
						if (!is_null($old_image)) {
							rename($target.$old_category.DS.$old_image, $target.$event->id_category.DS.$old_image);
						} elseif ($event->image != '') {
							rename($target.$old_category.DS.$event->image, $target.$event->id_category.DS.$event->image);
						}
						if (!is_null($old_file)) {
							rename($target.$old_category.DS.'files'.DS.$old_file, $target.$event->id_category.DS.'files'.DS.$old_file);
						} elseif ($event->file != '') {
							rename($target.$old_category.DS.'files'.DS.$event->file, $target.$event->id_category.DS.'files'.DS.$event->file);
						}
					}

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'events'.DS.$event->id_category;
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}
					if (strlen($_FILES['file']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'events'.DS.$event->id_category.DS.'files';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['file'];
						move_uploaded_file($_FILES['file']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_file);
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Event', 'insert', 'Event inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('event_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Event', 'update', 'Event updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('event_edit')."/$id");
					}
				} else {
					$this->AddError('Saving failed!');
				}
			}
		} elseif (isset($_POST['AddImage'])) {
			if ($_FILES['images']['tmp_name'][0] == '') {
				$this->view->AddError('Please upload a file...');
			}

			if (count($this->view->getErrors()) == 0) {
				$failures = 0;
				for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
					$tmp_name = time().'_'.$_FILES['images']['name'][$i];

					$addImage = new EventImage_Entity();

					$addImage->id_event = $id;
					$addImage->image = $tmp_name;
					$addImage->size = $_FILES['images']['size'][$i];
					$res = $this->model->saveEventImage($addImage);

					if (!is_array($res)) {
						$target = DOCROOT.MEDIA_ROOT.'events'.DS.$event->id_category.DS.'additional';
						Utils::createDirectory($target);
						$filename = $target.DS.$tmp_name;
						move_uploaded_file($_FILES['images']['tmp_name'][$i], $filename);
					} else {
						$failures++;
					}
				}
				if ($failures > 0) {
					$this->AddError('There was '.$failures.' failures during image upload!');
				} else {
					$this->view->AddNotice('Image has been uploaded successfully.');
				}
			}
			Utils::RedirectTo($this->getActionUrl('event_edit')."/$id");
		}

		$categories = $this->model->getCategories(50);
		$this->view->set('categories', $categories);

		if (!is_null($id)) {
			$additionalImages = $this->model->getEventImages(50, 0, "id_event = $id");
			$this->view->set('additionalImages', $additionalImages);
		}

		$this->view->set('ComponentSettings', $this->ComponentSettings);

		if (isset($this->ComponentSettings['currency']) && $this->ComponentSettings['currency'] != '') {
			$this->view->set('Currencies', explode(';', $this->ComponentSettings['currency']));
		} else {
			$this->view->set('Currencies', array('EUR'));
		}
		$this->view->set('event', $event);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('events_list'))));
		$title = (!is_null($id)) ? '[$EditEvent]' : '[$NewEvent]';
		$this->view->setTitle($title);
		$this->view->render('event-edit');
	}

	//Private
	private function moveAdditionalImages($id_event, $old_category, $new_category)
	{
		$additionalImages = $this->model->getEventImages(50, 0, "id_event = $id_event");
		$target = DOCROOT.MEDIA_ROOT.DS.'events'.DS;
		Utils::createDirectory($target.$new_category.DS.'additional');
		foreach ($additionalImages as $img) {
			rename($target.$old_category.DS.'additional'.DS.$img->image, $target.$new_category.DS.'additional'.DS.$img->image);
		}
	}

	public function event_details($id)
	{
		Utils::saveCurrentPageUrl();
		$event = $this->model->getEvent($id);
		$this->view->set('event', $event);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('events_list'))));
		$this->view->BreadCrumb->addDir('[$Events]', $this->getActionUrl('events_list'));
		$this->view->BreadCrumb->addDir('[$EventDetails]', $this->getActionUrl('event_details')."/$id");
		$this->view->setTitle('[$Details]');
		$this->view->render('event-details');
	}

	public function event_delete($id)
	{
		$event = $this->model->getEvent($id);
		$additionalImages = $this->model->getEventImages(50, 0, "id_event = $id");
		$result = $this->model->deleteEvent($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'events'.DS.$event->id_category;
			if ($event->image != '' && file_exists($target.$event->image)) {
				unlink($target.$event->image);
			}

			foreach ($additionalImages as $image) {
				if (file_exists($target.DS.'additional'.DS.$image->image)) {
					unlink($target.DS.'additional'.DS.$image->image);
				}
			}

			$this->LogsManager->registerLog('Event', 'delete', "Event deleted with id : $id and title : ".$event->title, $id);
			$this->AddNotice('Event deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('events_list'));
	}

	public function event_enable($id)
	{
		if (!is_null($id)) {
			$event = $this->model->getEvent($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$event->enabled = 1;
		$res = $this->model->saveEvent($event);

		if (!is_array($res)) {
			$this->view->AddNotice('Event has been enabled successfully.');
			$this->LogsManager->registerLog('Event', 'update', 'Event enabled with id : '.$event->id, $event->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('events_list'));
	}

	public function event_disable($id)
	{
		if (!is_null($id)) {
			$event = $this->model->getEvent($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$event->enabled = 0;
		$res = $this->model->saveEvent($event);

		if (!is_array($res)) {
			$this->view->AddNotice('Event has been disabled successfully.');
			$this->LogsManager->registerLog('Event', 'update', 'Event disabled with id : '.$event->id, $event->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('events_list'));
	}

	public function categories_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Categories'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteCategory($id);
				$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Categories has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('categories_list'));
		}

		$categories = $this->model->getCategories($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', $this->getActionUrl('category_edit')));
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));
		if (!is_null($id)) {
			$category = $this->model->getCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$category->category.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditCategory]', $this->getActionUrl('category_edit')."/$id");
		} else {
			$category = new EventCategory_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddCategory]', $this->getActionUrl('category_edit'));
		}

		if ($category === false) {
			$this->AddError("Category $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['category'] == '') ? $this->AddError('Category Name is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $category->image;
				} else {
					$_POST['image'] = $category->image;
				}

				$category->category = $_POST['category'];
				$category->description = $_POST['description'];
				$category->image = $_POST['image'];
				$category->parent_id = $_POST['parent_id'];
				$category->sorting = $_POST['sorting'];

				$inserted_id = $this->model->saveCategory($category);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Category has been saved successfully!');

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'events'.DS.'categories';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Category', 'insert', 'Category inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Category', 'update', 'Category updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('category', $category);

		$categories = $this->model->getCategories(50);
		$this->view->set('categories', $categories);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('categories_list'))));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id)
	{
		$category = $this->model->getCategory($id);
		$result = $this->model->deleteCategory($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'events'.DS.'categories';
			if ($category->image != '' && file_exists($target.$category->image)) {
				unlink($target.$category->image);
			}
			$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id, name : {$category->category}", $id);
			$this->AddNotice('Category deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('categories_list'));
	}

	public function addtImage_delete($id)
	{
		if (!isset($_GET['id_event'])) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$image = $this->model->getEventImage($id);

		$res = $this->model->deleteEventImage($id);
		if ($res !== false) {
			$this->LogsManager->registerLog('EventImage', 'delete', "Image deleted with id : $id", $id);
			$this->AddNotice('Image has been deleted successfully.');
			$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'events'.DS.$image->id_category.DS.'additional'.DS.$image->image;
			unlink(DOCROOT.$filename);
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('event_edit')."/{$image->id_event}");
	}

	public function addtImage_download($id)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('events_list'));
		}

		$image = $this->model->getEventImage($id);
		$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'events'.DS.$image->id_category.DS.'additional'.DS.$image->image;

		//Shkarkimi i Files
		header('Content-Type: '.mime_content_type(DOCROOT.$filename));
		header('Content-Disposition: attachment; filename="'.$image->image.'""');
		readfile(DOCROOT.$filename);
	}
}
