<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Events');

foreach ($categories as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_category_edit.'/'.$row->id, 'xs', '', '[$Edit]')
	);
}
$table->setElements($categories);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('category', '[$Name]', 'string', 'left'),
	new TableList_Field('image', '[$Image]', 'image', 'left'),
	new TableList_Field('sorting', '[$Sorting]', 'int', 'left'),
	new TableList_Field('total_events', '[$TotalEvents]', 'int', 'left'),
));
$table->setUrl_action($url_categories_list);
$table->setUrl_delete($url_category_delete);

$table->setImagesPath('events'.DS.'categories');
$table->multipleDeletion(false);
$table->renderTopBar(false);
$table->render();
