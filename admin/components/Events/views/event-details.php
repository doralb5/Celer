<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-5">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$EventDetails] - <span class=""><?= $event->title ?></span></h4>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered personalinfo">
                    <tbody>
                    <tr>
                        <td>[$Id]</td>
                        <td><b><?= $event->id ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Title]</td>
                        <td><b><?= $event->title ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Image]</td>
                        <td><?php if ($event->image != '') {
	?>
                                <img
                                    src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'events'.DS.$event->id_category.DS.$event->image ?>"
                                    alt="Image" width="100px"/>
                            <?php
} else {
		?>
                                No Image
                            <?php
	} ?>
                        </td>
                    </tr>
                    <tr>
                        <td>[$Category]</td>
                        <td><b><?= $event->category ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Pay]</td>
                        <td>
                            <b><?= ($event->pay == '1') ? '<i class="fa fa-check-square" style="color:green"></i>' : '<i class="fa fa-square-o" style="color:red"></i>' ?></b>
                        </td>
                    </tr>
                    <tr>
                        <td>[$Cost]</td>
                        <td><b><?= $event->cost ?> <?= $event->currency ?></b></td>
                    </tr>
                    <tr>
                        <td>[$CreatedByUser]</td>
                        <td><b><?= $event->user_fullname ?></b></td>
                    </tr>
                    <tr>
                        <td>[$CreationDate]</td>
                        <td><?= date('d/m/Y H:i', strtotime($event->creation_date)) ?></td>
                    </tr>
                    <tr>
                        <td>[$StartDate]</td>
                        <td><?= date('d/m/Y H:i', strtotime($event->start_date)) ?></td>
                    </tr>
                    <tr>
                        <td>[$EndDate]</td>
                        <td><?= (is_null($event->end_date)) ? 'No End' : date('d/m/Y H:i', strtotime($event->end_date)) ?></td>
                    </tr>
                    <tr>
                        <td>[$Organizer]</td>
                        <td><b><?= $event->organizer ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Location]</td>
                        <td><b><?= $event->location ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Address]</td>
                        <td><b><?= $event->address ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Phone]</td>
                        <td><b><?= $event->phone ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Email]</td>
                        <td><b><?= $event->email ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Website]</td>
                        <td><b><?= $event->website ?></b></td>
                    </tr>
                    <tr>
                        <td>[$File]</td>
                        <td><?php if ($event->file != '') {
		?>
                                <a
                                    href="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'events'.DS.$event->id_category.DS.'files'.DS.$event->file ?>"
                                    download>Download</a>
                            <?php
	} else {
		?>
                                No File
                            <?php
	} ?>
                        </td>
                    </tr>
                    <tr>
                        <td>[$Featured]</td>
                        <td><?= ($event->featured == '1') ? '<i class="fa fa-check-square" style="color:green"></i>' : '<i class="fa fa-square-o" style="color:red"></i>' ?></td>
                    </tr>
                    <tr>
                        <td>[$Enabled]</td>
                        <td><?= ($event->enabled == '1') ? '<i class="fa fa-check-square" style="color:green"></i>' : '<i class="fa fa-square-o" style="color:red"></i>' ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <p>[$Description]</p>

            <div class="col-md-12">
                <?= $event->description ?>
            </div>
            <p>&nbsp;</p>

            <div class="col-md-12 text-right">
                <a class="btn btn-danger" href="<?= $url_event_delete."/{$event->id}" ?>"><i
                        class="fa fa-minus-circle"></i>&nbsp;&nbsp;[$Delete]</a>
            </div>
            <p>&nbsp;</p>
        </div>
    </div>
</div>
