<?php
$title = isset($_POST['title']) ? $_POST['title'] : $event->title;
$description = isset($_POST['description']) ? $_POST['description'] : $event->description;
$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $event->id_category;
$pay = isset($_POST['pay']) ? $_POST['pay'] : $event->pay;
$cost = isset($_POST['cost']) ? $_POST['cost'] : $event->cost;
$currency = isset($_POST['currency']) ? $_POST['currency'] : $event->currency;
$start_date = isset($_POST['start_date']) ? $_POST['start_date'] : $event->start_date;
$end_date = isset($_POST['end_date']) ? $_POST['end_date'] : $event->end_date;
$organizer = isset($_POST['organizer']) ? $_POST['organizer'] : $event->organizer;
$location = isset($_POST['location']) ? $_POST['location'] : $event->location;
$address = isset($_POST['address']) ? $_POST['address'] : $event->address;
$phone = isset($_POST['phone']) ? $_POST['phone'] : $event->phone;
$email = isset($_POST['email']) ? $_POST['email'] : $event->email;
$website = isset($_POST['website']) ? $_POST['website'] : $event->website;
$file = isset($_POST['file']) ? $_POST['file'] : $event->file;
$coordinates = isset($_POST['coordinates']) ? $_POST['coordinates'] : $event->coordinates;
$featured = isset($_POST['featured']) ? $_POST['featured'] : $event->featured;
?>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= CMSSettings::$google_key?>&callback=initMap"
  type="text/javascript"></script>
<div class="col-md-12">
    <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
          novalidate="" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">[$GeneralInformation]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$Title] *</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $title ?>"
                                       name="title" placeholder="[$Title]" data-parsley-required="true"
                                       data-parsley-id="6524" required>
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Category]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" name="id_category">
                                    <?php foreach ($categories as $categ) {
	?>
                                        <option
                                            value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
                                    <?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                            <div class="col-md-6 col-sm-6">
                                <input type="file" name="image">
                                <?php if ($event->image != '') {
		?>
                                    <p>
                                        <img
                                            src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT."events/{$event->id_category}/".$event->image ?>"
                                            width="100px" alt="Image"/></p>
                                <?php
	} ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Pay]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" name="pay" id="pay">
                                    <option value="0" <?= ($pay == '0') ? 'selected' : '' ?>>[$NoOption]</option>
                                    <option value="1" <?= ($pay == '1') ? 'selected' : '' ?>>[$YesOption]</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="cost">
                            <label class="control-label col-md-4 col-sm-4">[$Cost]</label>

                            <div class="col-md-8 col-sm-8">

                                <div class="input-group">
                                    <input class="form-control" type="number" step="any" value="<?= $cost ?>"
                                           name="cost" placeholder="[$Cost]">
                                    <span class="input-group-addon">
                                        <?php if (count($Currencies) > 1) {
		?>
                                            <select name="currency">
                                                <?php foreach ($Currencies as $curr) {
			if ($curr == $currency) {
				$selected = 'selected="selected"';
			} else {
				$selected = '';
			}

			echo "<option value=\"$curr\" $selected>$curr</option>\n";
		} ?>
                                            </select>
                                        <?php
	} else {
		echo $Currencies[0];
	} ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Featured]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" name="featured">
                                    <option value="0" <?= ($featured == '0') ? 'selected' : '' ?>>[$NoOption]
                                    </option>
                                    <option value="1" <?= ($featured == '1') ? 'selected' : '' ?>>[$YesOption]
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$File]</label>

                            <div class="col-md-6 col-sm-6">
                                <input type="file" name="file">
                                <?php if ($event->file != '') {
		?>
                                    <p>&nbsp;</p>
                                    <a
                                        href="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT."events/{$event->id_category}/files/".$event->file ?>"
                                        download>Download</a>
                                <?php
	} ?>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">[$Content]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <textarea class="ckeditor" id="editor1" name="description"><?= $description ?></textarea>
                    </div>
                </div>
                <!-- end panel -->
            </div>

            <div class="col-md-6">


                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">[$AdditionalInformation]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$StartDate]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control" name="start_date" id="datetimepicker1"
                                       placeholder="[$SelectDate]" value="<?= $start_date ?>" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$EndDate]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control" name="end_date" id="datetimepicker2"
                                       placeholder="[$SelectDate]" value="<?= $end_date ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Organizer]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $organizer ?>"
                                       name="organizer" placeholder="[$Organizer]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Phone]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="tel" value="<?= $phone ?>"
                                       name="phone" placeholder="[$Phone]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Email]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="email" value="<?= $email ?>"
                                       name="email" placeholder="[$Email]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Website]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $website ?>"
                                       name="website" placeholder="[$Website]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Location]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $location ?>"
                                       name="location" placeholder="[$Location]" id="location"
                                       onblur="setMarker('Location');">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Address]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $address ?>"
                                       name="address" placeholder="[$Address]" id="address"
                                       onblur="setMarker('Address');">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel -->

                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">[$Map]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div id="map" style="width: 100%; height: 250px;"></div>
                        <input type="hidden" id="coordinates" name="coordinates"
                               value="<?= ($coordinates != '') ? "$coordinates" : '' ?>">
                    </div>
                </div>
            </div>
        </div>

</div>
<div class="col-md-12">
    <p class="text-center">
        <button type="submit" name="save" class="btn btn-info">
            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
        </button>
    </p>
</div>
<p>&nbsp;</p>
</form>

<?php if (!is_null($event->id)) {
		?>

    <div class="col-md-8">

        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">[$AdditionalImages]</h4>
            </div>
            <div class="panel-body">

                <p>&nbsp;</p>
                <?php
				include(VIEWS_PATH.'TableList_class.php');
		$table = new TableListView('Articles', 'list');
		foreach ($additionalImages as &$row) {
			$row->rows_buttons = array(
						new Button('<i class="fa fa-eye"></i>', $url_addtImage_show.'/'.$row->id_category.'/additional/'.$row->image, 'xs', '', '[$Show]', '_blank'),
						new Button('<i class="fa fa-download"></i>', $url_addtImage_download.'/'.$row->id, 'xs', '', '[$Download]')
					);

			$row->row_imagePath = substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.DS.'events'.DS.$row->id_category.DS.'additional';
		}

		$table->setElements($additionalImages);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
					new TableList_Field('id', '[$Id]', 'int', 'left'),
					new TableList_Field('image', '[$Filename]', 'string', 'left'),
					new TableList_Field('image', '[$Image]', 'image', 'left'),
				));
		$table->setUrl_delete($url_addtImage_delete);
		$table->setUrl_delete_params(array('id_event' => $event->id));
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->render(); ?>


                <div class="col-md-12">
                    <form id="fileupload" method="POST" enctype="multipart/form-data">
                        <div class="form-group col-md-9">
                            <label>[$SelectFile]</label>

                            <div class="box">
                                <input type="file" name="images[]" id="file-7" class="inputfile inputfile-6"
                                       data-multiple-caption="{count} files selected" multiple/>
                                <label for="file-7"><span></span><strong>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                             viewBox="0 0 20 17">
                                            <path
                                                d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                        </svg>
                                        [$Choose]&hellip;</strong></label>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <br>
                            <button class="btn btn-info" name="AddImage" type="submit"><i class="fa fa-plus"></i>&nbsp;&nbsp;[$AddFile]
                            </button>

                        </div>
                    </form>
                </div>
                <div class="col-md-12 note note-info">
                    <h4>Notes</h4>
                    <ul>
                        <li>The maximum file size for uploads is <strong>20 MB</strong></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

<?php
	} ?>


<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>

<script>

    $(document).ready(function () {

        $('#cost').hide();
        <?php if ($pay == '1') {
	?>
        $('#cost').show();
        <?php
} ?>
        $('#pay').change(function () {
            if ($(this).val() == '0') {
                $('#cost').hide();
            } else {
                $('#cost').show();
            }
        });

    });


    CKEDITOR.replace('editor1', {
        imageBrowser_listUrl: "<?=substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).LIBS_PATH?>JsonDirImages.php?media_path=<?=substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).MEDIA_ROOT?>&docroot=<?=  rtrim(DOCROOT, '/')?>",
        height: 232
    });
</script>

<script>
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker();
    });
</script>


<?php
if ($coordinates != '') {
		list($lat, $lng) = explode(',', $coordinates);
	} else {
		$lat = (isset($ComponentSettings['map_lat'])) ? $ComponentSettings['map_lat'] : '38.900413';
		$lng = (isset($ComponentSettings['map_lng'])) ? $ComponentSettings['map_lng'] : '16.586658';
	}
?>
<script type="text/javascript">

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: {lat: <?= $lat ?>, lng: <?= $lng ?> }
    });
    $('#coordinates').val("(<?=$lat?>, <?=$lng?>)");
    var geocoder = new google.maps.Geocoder();
    var marker;
    function setMarker(destination) {

        if (marker != null)
            marker.setMap(null);

        var address = document.getElementById('location').value + ', ' + document.getElementById('address').value;
        geocodeAddress(address, geocoder, map, destination);
        if (document.getElementById('address').value != '')
            map.setZoom(15);
        else
            map.setZoom(8);

    }


    function geocodeAddress(address, geocoder, resultsMap, destination) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location
                });
                $('#coordinates').val(results[0].geometry.location);
                console.log($('#coordinates').val());
            } else {
                $("#warning").modal();
            }
        });
    }

    var checkeventcount = 1, prevTarget;
    $('.modal').on('show.bs.modal', function (e) {
        if (typeof prevTarget == 'undefined' || (checkeventcount == 1 && e.target != prevTarget)) {
            prevTarget = e.target;
            checkeventcount++;
            e.preventDefault();
            $(e.target).appendTo('body').modal('show');
        }
        else if (e.target == prevTarget && checkeventcount == 2) {
            checkeventcount--;
        }
    });

</script>

<style>
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
    }

    .inputfile:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .box {
        padding: 0px;
    }
</style>
<script>
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;
        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();
            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });</script>