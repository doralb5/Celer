<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Events');

foreach ($events as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_event_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_event_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_event_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		new Button('<i class="fa fa-eye"></i>', $url_event_details.'/'.$row->id, 'xs', '', '[$Details]'),
	);
	$row->row_imagePath = 'events'.DS.$row->id_category;
}
$table->setElements($events);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('title', '[$Title]', 'string', 'left'),
	new TableList_Field('image', '[$Image]', 'image', 'left'),
	new TableList_Field('category', '[$Category]', 'string', 'left'),
	new TableList_Field('start_date', '[$StartDate]', 'date', 'left'),
	new TableList_Field('end_date', '[$EndDate]', 'date', 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
));
$table->setUrl_action($url_events_list);
$table->setUrl_delete($url_event_delete);
$table->render();
