<?php

class Gallery_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Gallery/item_edit/');

		$this->view->set('url_images_list', $this->getActionUrl('images_list'));
		$this->view->set('url_videos_list', $this->getActionUrl('videos_list'));
		$this->view->set('url_item_edit', $this->getActionUrl('item_edit'));
		$this->view->set('url_item_delete', $this->getActionUrl('item_delete'));
		$this->view->set('url_item_enable', $this->getActionUrl('item_enable'));
		$this->view->set('url_item_disable', $this->getActionUrl('item_disable'));
		$this->view->set('url_item_show', substr(WEBROOT, 0, -6).MEDIA_ROOT.'gallery');
		$this->view->set('url_item_download', $this->getActionUrl('item_download'));

		$this->view->set('url_categories_list', $this->getActionUrl('categories_list'));
		$this->view->set('url_category_edit', $this->getActionUrl('category_edit'));
		$this->view->set('url_category_delete', $this->getActionUrl('category_delete'));

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function images_list($id_categ = null)
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Gallery'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = "type='image' ";

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$item = $this->model->getItem($id);
				$res = $this->model->deleteItem($id);

				if ($res !== false) {
					$target = DOCROOT.MEDIA_ROOT.'gallery'.DS.$item->id_category;
					if ($item->item_name != '' && file_exists($target.DS.$item->item_name)) {
						unlink($target.DS.$item->item_name);
					}

					$thumb_target = DOCROOT.MEDIA_ROOT.'gallery'.DS.'thumbs';
					if ($item->thumbnail != '' && file_exists($thumb_target.DS.$item->thumbnail)) {
						unlink($thumb_target.DS.$item->thumbnail);
					}

					$this->LogsManager->registerLog('Image', 'delete', 'Image deleted with id : '.$id, $id);
				}
			}
			if ($res !== false) {
				$this->view->AddNotice('Image has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('images_list'));
		}

		if (!is_null($id_categ)) {
			$filter = "AND id_category = $id_categ";
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'GalleryItem.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'GalleryItem.title', 'peso' => 90),
				array('field' => TABLE_PREFIX.'GalleryItem.description', 'peso' => 90),
			);
			$images = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$images = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddItem]', $this->getActionUrl('item_edit')));

		$this->view->BreadCrumb->addDir('[$Gallery]', $this->getActionUrl('images_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('images', $images);
		$this->view->setTitle('[$Gallery]');
		$this->view->render('images-list');
	}

	public function videos_list($id_categ = null)
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Gallery'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = "type='video' || type='youtube' ";

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$item = $this->model->getItem($id);
				$res = $this->model->deleteItem($id);
				if ($res !== false) {
					$target = DOCROOT.MEDIA_ROOT.'gallery'.DS.$item->id_category;
					if ($item->item_name != '' && file_exists($target.DS.$item->item_name)) {
						unlink($target.DS.$item->item_name);
					}

					$thumb_target = DOCROOT.MEDIA_ROOT.'gallery'.DS.'thumbs';
					if ($item->thumbnail != '' && file_exists($thumb_target.DS.$item->thumbnail)) {
						unlink($thumb_target.DS.$item->thumbnail);
					}

					$this->LogsManager->registerLog('Video', 'delete', 'Video deleted with id : '.$id, $id);
				}
			}
			if ($res !== false) {
				$this->view->AddNotice('Video has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('videos_list'));
		}

		if (!is_null($id_categ)) {
			$filter = "AND id_category = $id_categ";
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'GalleryItem.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'GalleryItem.title', 'peso' => 90),
				array('field' => TABLE_PREFIX.'GalleryItem.description', 'peso' => 90),
			);
			$videos = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$videos = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($videos as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddItem]', $this->getActionUrl('item_edit')));

		$this->view->BreadCrumb->addDir('[$Gallery]', $this->getActionUrl('videos_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('videos', $videos);
		$this->view->setTitle('[$Gallery]');
		$this->view->render('videos-list');
	}

	public function item_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Gallery]', $this->getActionUrl('images_list'));
		if (!is_null($id)) {
			$item = $this->model->getItem($id);
			HeadHTML::setTitleTag('Edit Item - '.$item->title.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditItem]', $this->getActionUrl('item_edit')."/$id");
		} else {
			$item = new GalleryItem_Entity();
			HeadHTML::setTitleTag('New Item'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$NewItem]', $this->getActionUrl('item_edit'));
		}

		$logged_user = UserAuth::getLoginSession();

		if ($item === false) {
			$this->AddError("Item $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['id_category'] == '') ? $this->AddError('Category is required!') : '';
			(is_null($id) && (!isset($_POST['type']) || $_POST['type'] == '')) ? $this->AddError('Type is required!') : '';

			if (is_null($id)) {
				if (isset($_FILES['file']['tmp_name']) && strlen($_FILES['file']['tmp_name']) == 0 && $_POST['remote_url'] == '') {
					$this->view->AddError('Please upload an image or set the remote url...');
				}
			}

			if (isset($_FILES['file']['tmp_name']) && strlen($_FILES['file']['tmp_name']) && $_POST['type'] == 'image') {
				(!Utils::allowedFileType($_FILES['file']['name'])) ? $this->view->AddError('Please upload a valid image!') : '';
			}

			if (isset($_FILES['file']['tmp_name']) && strlen($_FILES['file']['tmp_name']) && $_POST['type'] == 'video') {
				(!Utils::allowedFileType($_FILES['file']['name'], array('mp4', '3gp', 'flv', 'webm'))) ? $this->view->AddError('Please upload a valid video!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (($_POST['remote_url'] != '' || $_POST['type'] == 'youtube') && !strlen($_FILES['file']['tmp_name'])) {
					if (strpos($_POST['remote_url'], 'youtube.com') || strpos($_POST['remote_url'], 'youtu.be')) {
						$item->path_type = 'remote';
						$item->type = 'youtube';
					}
					$item->item_name = $_POST['remote_url'];
					$item->path_type = 'remote';
				}

				if (strlen($_FILES['file']['tmp_name'])) {
					$_POST['size'] = $_FILES['file']['size'];
					$old_file = $item->item_name;
					$_POST['file'] = time().'_'.$_FILES['file']['name'];
					$item->item_name = $_POST['file'];
					$item->path_type = 'local';
				} else {
					$_POST['file'] = $item->item_name;
					$_POST['size'] = $item->size;
					$old_file = null;
				}

				if (!is_null($id) && $item->id_category != $_POST['id_category']) {
					$old_category = $item->id_category;
				} else {
					$old_category = null;
				}

				$item->type = (isset($_POST['type']) && $_POST['type'] != '') ? $_POST['type'] : $item->type;
				$item->title = $_POST['title'];
				$item->description = $_POST['description'];
				$item->id_category = $_POST['id_category'];
				$item->size = $_POST['size'];
				$item->publish_date = ($_POST['publish_date'] != '') ? $_POST['publish_date'] : $item->publish_date;
			}

			$inserted_id = $this->model->saveItem($item);

			if (!is_array($inserted_id)) {
				$this->AddNotice('File has been saved successfully.');

				$target = DOCROOT.MEDIA_ROOT.DS.'gallery'.DS;
				Utils::createDirectory($target.$item->id_category);
				if (!is_null($id) && !is_null($old_category)) {
					if (!is_null($old_file)) {
						rename($target.$old_category.DS.$old_file, $target.$item->id_category.DS.$old_file);
					} else {
						rename($target.$old_category.DS.$item->item_name, $target.$item->id_category.DS.$item->item_name);
					}
				}

				if (strlen($_FILES['file']['tmp_name'])) {
					$target = DOCROOT.MEDIA_ROOT.DS.'gallery'.DS.$item->id_category;
					Utils::createDirectory($target);
					$filename = $target.DS.$_POST['file'];
					move_uploaded_file($_FILES['file']['tmp_name'], $filename);
					if (!is_null($id)) {
						unlink($target.DS.$old_file);
					}
				}

				if (!is_bool($inserted_id)) {
					$this->LogsManager->registerLog('GalleryItem', 'insert', 'Gallery item inserted with id : '.$inserted_id, $inserted_id);
					Utils::RedirectTo($this->getActionUrl('item_edit')."/$inserted_id");
				} else {
					$this->LogsManager->registerLog('GalleryItem', 'update', 'Gallery item updated with id : '.$id, $id);
					Utils::RedirectTo($this->getActionUrl('item_edit')."/$id");
				}
			} else {
				$this->AddError('Saving failed!');
			}
		}

		$categories = $this->model->getCategories(50);
		$this->view->set('categories', $categories);

		$this->view->set('item', $item);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('images_list'))));
		$title = (!is_null($id)) ? '[$EditItem]' : '[$NewItem]';
		$this->view->setTitle($title);
		$this->view->render('item-edit');
	}

	public function image_details($id)
	{
		Utils::saveCurrentPageUrl();
		$item = $this->model->getItem($id);
		$this->view->set('item', $item);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('images_list')."/{$item->id}")));
		$this->view->BreadCrumb->addDir('[$Gallery]', $this->getActionUrl('images_list'));
		$this->view->BreadCrumb->addDir('[$ImageDetails]', $this->getActionUrl('image_details')."/$id");
		$this->view->setTitle('[$Details]');
		$this->view->render('image-details');
	}

	public function item_delete($id)
	{
		$item = $this->model->getItem($id);
		$result = $this->model->deleteItem($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'gallery'.DS.$item->id_category;
			if ($item->item_name != '' && file_exists($target.DS.$item->item_name)) {
				unlink($target.DS.$item->item_name);
			}

			$thumb_target = DOCROOT.MEDIA_ROOT.'gallery'.DS.'thumbs';
			if ($item->thumbnail != '' && file_exists($thumb_target.DS.$item->thumbnail)) {
				unlink($thumb_target.DS.$item->thumbnail);
			}

			$this->LogsManager->registerLog('GalleryItem', 'delete', "Item deleted with id : $id and title : ".$item->title, $id);
			$this->AddNotice('Item deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		if ($item->type == 'image') {
			Utils::backRedirect($this->getActionUrl('images_list'));
		} else {
			Utils::backRedirect($this->getActionUrl('videos_list'));
		}
	}

	public function item_download($id)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('images_list'));
		}

		$item = $this->model->getItem($id);
		$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'gallery'.DS.$item->id_category.DS.$item->item_name;

		//Shkarkimi i Files
		header('Content-Type: '.mime_content_type(DOCROOT.$filename));
		header('Content-Disposition: attachment; filename="'.$item->item_name.'"');
		readfile(DOCROOT.$filename);
	}

	public function item_enable($id)
	{
		if (!is_null($id)) {
			$item = $this->model->getItem($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$item->enabled = 1;
		$res = $this->model->saveItem($item);

		if (!is_array($res)) {
			$this->view->AddNotice('Item has been enabled successfully.');
			$this->LogsManager->registerLog('GalleryItem', 'update', 'Gallery item enabled with id : '.$item->id, $item->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		if ($item->type == 'image') {
			Utils::backRedirect($this->getActionUrl('images_list'));
		} else {
			Utils::backRedirect($this->getActionUrl('videos_list'));
		}
	}

	public function item_disable($id)
	{
		if (!is_null($id)) {
			$item = $this->model->getItem($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$item->enabled = 0;
		$res = $this->model->saveItem($item);

		if (!is_array($res)) {
			$this->view->AddNotice('Item has been disabled successfully.');
			$this->LogsManager->registerLog('GalleryItem', 'update', 'Gallery item disabled with id : '.$item->id, $item->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		if ($item->type == 'image') {
			Utils::backRedirect($this->getActionUrl('images_list'));
		} else {
			Utils::backRedirect($this->getActionUrl('videos_list'));
		}
	}

	public function categories_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Categories'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteCategory($id);
				$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Categories has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('categories_list'));
		}

		$categories = $this->model->getCategories($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', $this->getActionUrl('category_edit')));
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));
		if (!is_null($id)) {
			$category = $this->model->getCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$category->category.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditCategory]', $this->getActionUrl('category_edit')."/$id");
		} else {
			$category = new GalleryCategory_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddCategory]', $this->getActionUrl('category_edit'));
		}

		if ($category === false) {
			$this->AddError("Category $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['category'] == '') ? $this->AddError('Category Name is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $category->image;
				} else {
					$_POST['image'] = $category->image;
				}

				$category->enabled = (isset($_POST['enabled']) && $_POST['enabled'] != '0') ? '1' : '0';
				$category->category = $_POST['category'];
				$category->description = $_POST['description'];
				$category->image = $_POST['image'];
				$inserted_id = $this->model->saveCategory($category);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Category has been saved successfully!');

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'gallery'.DS.'categories';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('GalleryCategory', 'insert', 'Gallery Category inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('GalleryCategory', 'update', 'Gallery Category updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('category', $category);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('categories_list'))));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id)
	{
		$category = $this->model->getCategory($id);
		$result = $this->model->deleteCategory($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('GalleryCategory', 'delete', "Gallery Category deleted with id : $id, name : {$category->category}", $id);
			$this->AddNotice('Category deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('categories_list'));
	}

	public function advanced_upload()
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Videos]', $this->getActionUrl('videos_list'));
		HeadHTML::setTitleTag('Advanced Upload | '.CMSSettings::$website_title);
		$category = $this->model->getCategories(-1, 0, '', $order = ' Category DESC ');
		$this->view->set('categories', $category);
		$this->view->BreadCrumb->addDir('[$AdvancedUpload]', $this->getActionUrl('advanced_upload'));
		$this->view->setTitle('[$AdvancedUpload]');
		$this->view->render('file-upload');
	}

	public function handleFileUpload()
	{
		//        print_r($_FILES);
		//        print_r($_POST['image_category']);
		//        exit;

		if (isset($_FILES['files']) && !empty($_FILES['files'])) {
			$no_files = count($_FILES['files']['name']);

			for ($i = 0; $i < $no_files; $i++) {
				if (!Utils::allowedFileType($_FILES['files']['name'][$i])) {
					$this->view->AddError('File type not allowed');
				}

				if (!count($this->view->getErrors())) {
					if ($_FILES['files']['error'][$i] > 0) {
						echo 'Error: '.$_FILES['files']['error'][$i].'<br>';
					} else {
						$part_name = Utils::url_slug(CMSSettings::$webdomain); // SOME SEO SHIT
						$filename = time().'_'.$part_name.'_'.$_FILES['files']['name'][$i];
						$id_category = $_POST['image_category'][$i];
						$item = new GalleryItem_Entity();
						$item->item_name = $filename;
						$item->size = $size;
						$item->path_type = 'local';
						$item->id_category = $id_category;
						$item->publish_date = date('Y-m-d H:i:s', strtotime('+5 min'));
						$item->title = $filename;
						$item->description = $filename;
						$item->enabled = 1;

						$result = $this->model->saveItem($item);
						if (is_array($result)) {
							echo 'Upload failed! Something went wrong.';
						} else {
							$target = DOCROOT.MEDIA_ROOT.'gallery'.DS.$id_category;
							Utils::createDirectory($target);
							$filepath = $target.DS.$filename;
							$uploaded = move_uploaded_file($_FILES['files']['tmp_name'][$i], $filepath);
							if ($uploaded) {
								echo "<img class='img-responsive col-sm-3 col-xs-4 col-md-2' src='".Utils::genThumbnailUrl('/gallery/'.$id_category.'/'.$filename, 100, 100)."'>";
							}
						}
					}
				}
			}
		} else {
			echo 'Please choose at least one file';
		}

		exit;

		$id_general_categ = (isset($this->ComponentSettings['id_category'])) ? $this->ComponentSettings['id_category'] : null;
		if (is_null($id_general_categ)) {
			$response['error'] = 'Upload failed! You have to set the general category setting first!';
			echo json_encode($response, true);
			return;
		}

		$category = $this->model->getCategory($id_general_categ);
		if (is_null($category)) {
			$response['error'] = 'Upload failed! General category is not valid. Please change it.';
			echo json_encode($response, true);
			return;
		}

		if (strlen($_FILES['file_data']['tmp_name'])) {
			$name = $_FILES['file_data']['name'];
			$arr_elemments = explode('.', $name);
			$extension = end($arr_elemments); // extra () to prevent notice

			$isVideo = in_array(strtolower($extension), array('mp4', '3gp', 'ogg', 'flv'));
			$isImage = in_array(strtolower($extension), array('gif', 'png', 'jpg', 'jpeg'));

			$target = DOCROOT.MEDIA_ROOT.DS.'gallery'.DS.$id_general_categ;
			Utils::createDirectory($target);

			$file = time().'_'.$_FILES['file_data']['name'];
			$size = $_FILES['file_data']['size'];

			$filename = $target.DS.$file;

			$item = new GalleryItem_Entity();

			if ($isImage) {
				$item->type = 'image';
			} elseif ($isVideo) {
				$item->type = 'video';
			}

			$item->item_name = $file;
			$item->size = $size;
			$item->path_type = 'local';
			$item->id_category = $id_general_categ;
			$item->publish_date = date('Y-m-d H:i:s', strtotime('+5 min'));
			$item->title = $_FILES['file_data']['name'];
			$item->description = $_FILES['file_data']['name'];
			$item->enabled = 0;

			$result = $this->model->saveItem($item);
			if (is_array($result)) {
				$response['error'] = 'Upload failed! Something went wrong.';
				echo json_encode($response, true);
				return;
			} else {
				move_uploaded_file($_FILES['file_data']['tmp_name'], $filename);
			}
		}

		$response['1'] = 'Upload succed!';
		echo json_encode($response, true);
	}

	public function genVideoThumb($id = null)
	{
		if (is_null($id)) {
			Messages::addError('A video id is required!');
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
			return;
		}

		$video = $this->model->getItem($id);
		if (is_null($video) || ($video->type != 'video' && $video->type != 'youtube')) {
			$this->view->AddError('Video does not exist!');
			Utils::backRedirect(Utils::getComponentUrl("Gallery/item_edit/$id"));
			return;
		}

		if ($video->type == 'youtube') {
			$query = parse_url($video->item_name, PHP_URL_QUERY);
			parse_str($query, $params);
			$video_id = $params['v'];

			$thumb = "http://img.youtube.com/vi/$video_id/0.jpg";
			$video->thumbnail = $thumb;
			$result = $this->model->saveItem($video);
			if (!is_array($result)) {
				$this->view->AddNotice('Thumbnail has been created successfully.');
			} else {
				$this->view->AddError('Problem while creating record in database.');
			}

			Utils::backRedirect(Utils::getComponentUrl("Gallery/item_edit/$id"));
			return;
		}

		$target = DOCROOT.MEDIA_ROOT.DS.'gallery'.DS.$video->id_category;
		if (!file_exists($target.DS.$video->item_name)) {
			$this->view->AddError('Video does not exist!');
			Utils::backRedirect(Utils::getComponentUrl("Gallery/item_edit/$id"));
			return;
		}

		$thumbs_target = DOCROOT.MEDIA_ROOT.DS.'gallery'.DS.'thumbs';
		Utils::createDirectory($thumbs_target);

		$old_thumbnail = null;
		if ($video->thumbnail != '') {
			$old_thumbnail = $video->thumbnail;
		}

		$new_filename = 'thmb_'.time().'_'.str_replace(' ', '_', $video->title).'.jpg';
		$output = $thumbs_target.DS.$new_filename;

		$thumbCreated = Utils::genVideoThumbnail($target.DS.$video->item_name, $output, 3);
		if ($thumbCreated) {
			$video->thumbnail = $new_filename;
			$result = $this->model->saveItem($video);
			if (!is_array($result)) {
				$this->view->AddNotice('Thumbnail has been created successfully.');
				if (!is_null($old_thumbnail) && file_exists($thumbs_target.DS.$old_thumbnail)) {
					unlink($thumbs_target.DS.$old_thumbnail);
				}
			} else {
				$this->view->AddError('Problem while creating record in database.');
				if (file_exists($output)) {
					unlink($output);
				}
			}

			Utils::backRedirect(Utils::getComponentUrl("Gallery/item_edit/$id"));
			return;
		} else {
			$this->view->AddError('Problem while creating thumbnail.');
		}
		Utils::backRedirect(Utils::getComponentUrl("Gallery/item_edit/$id"));
	}

	public function multiple_video_upload()
	{
		$view = (isset($parameter['view'])) ? $parameter['view'] : 'multiple_video_upload';

		$this->view->render($view);
	}

	public function upload_handle()
	{
		$udate = date('m-d-Y_h-i-s-a', time());
		$uploads_dir = DOCROOT.MEDIA_ROOT.DS.'gallery'; // vi
		$thumbs_dir = DOCROOT.MEDIA_ROOT.'gallery'.DS.'vid_thumbs'.DS.date('Y', time()).DS.date('m', time());
		if (isset($_FILES['file']['tmp_name'])) {
			$name = time().'_'.$_FILES['file']['name'];
			Utils::createDirectory($uploads_dir);
			Utils::createDirectory($thumbs_dir);
			$tmp_name = $_FILES['file']['tmp_name'];
			move_uploaded_file($tmp_name, "$uploads_dir/$name");
			$status = $name;
			$video_name = explode('.', $name);
			$new_filename = 'thmb_'.$video_name[0].'.jpg';
			$output = $thumbs_dir.DS.$new_filename;
			$thumbCreated = Utils::genVideoThumbnail($uploads_dir.DS.$name, $output, 3);
			if ($thumbCreated) {
				$status = $name;
			} else {
				$status = $name;
			}
		} else {
			$status = 'ko';
		}
		echo json_encode($status);
	}

	public function save_uploaded()
	{
		$item = new GalleryItem_Entity();
		$files = $_POST['ufile'];

		if (isset($_POST['avanti'])) {
		}

		if (isset($_POST['save'])) {
			$title = $_POST['title'];
			$description = $_POST['description'];
			$categ = $_POST['id_category'];
			$item_name = $_POST['ufile'];

			$uploads_dir = DOCROOT.MEDIA_ROOT.DS.'gallery'; // The folder that contains the uploaded files to be moved to categorie folder

			$fc = count($files);
			for ($i = 0; $i <= $fc - 1; $i++) {
				$item->title = $title[$i];
				$item->description = $description[$i];
				$item->id_category = $categ[$i];
				$item->item_name = $item_name[$i];
				$item->type = 'video';
				$video_name = explode('.', $item_name[$i]);
				$item->thumbnail = '/'.date('Y', time()).'/'.date('m', time()).'/thmb_'.$video_name[0].'.jpg';
				$tomove = $uploads_dir.DS.$item_name[$i];  // name and path of  uploaded file to be moved
				$newfile = $uploads_dir.DS.$categ[$i].DS.$item_name[$i];  // new name and path of the file
				rename($tomove, $newfile); // moves the file to the categorie id folder
				$item->path_type = 'local';
				$inserted_id = $this->model->saveItem($item);
				if (!is_array($inserted_id)) {
					$this->AddNotice('File has been saved successfully.');
				} else {
					$this->AddError('Saving failed!');
				}
			}
		}
		$categories = $this->model->getCategories(50);
		$this->view->set('categories', $categories);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'item_saving';
		$this->view->set('files', $files);
		$this->view->render($view);
	}

	//    public function genThumbonUpload($video = null) {
	//        if (is_null($video)) {
	//            Messages::addError("A video id is required!");
	//            Utils::RedirectTo(Utils::getComponentUrl("Dashboard"));
	//            return;
	//        }
//
//
//
	//        $target = DOCROOT . MEDIA_ROOT . DS . 'gallery';
	//        if (!file_exists($target . DS . $video)) {
	//            $this->view->AddError("Video does not exist!");
	//            return;
	//        }
//
	//        $thumbs_target = DOCROOT . MEDIA_ROOT . DS . 'gallery' . DS . 'thumbs';
	//        Utils::createDirectory($thumbs_target);
//
//
	//        $new_filename = 'thmb_' . time() . '_' . str_replace(' ', '_', $video) . '.jpg';
	//        $output = $thumbs_target . DS . $new_filename;
//
	//        $thumbCreated = Utils::genVideoThumbnail($target . DS . $video, $output, 3);
	//        if ($thumbCreated) {
//
	//            $video->thumbnail = $new_filename;
	//            $result = $this->model->saveItem($video);
	//            if (!is_array($result)) {
//
	//                $this->view->AddNotice("Thumbnail has been created successfully.");
	//            } else {
	//                $this->view->AddError("Problem while creating thumbnail.");
	//            }
	//            return;
	//        }
	//    }

	public function video_import()
	{
		//old Db
		$dbimport = new Database('mysql', 'vm-web0', 'bluehat_mondial', 'bluehat_mondial', 'kkDlPQws0rI');
		var_dump($dbimport);

		$sql = 'Select id , id_programma AS id_category, titolo AS title , descrizione AS description , data  AS publish_date, path AS path , path_img AS thumbnail ,  online AS enabled , visite AS counter_views from videos ';

		$result = $dbimport->select($sql);

		foreach ($result as $res) {
			$item = new GalleryItem_Entity();

			var_dump($res['path']);
			if ($res['path'] != '') {
				$item->title = $res['title'];
				echo 'title : '.$res['title'].'<br>';
				$item->description = $res['description'];
				echo 'description : '.$res['description'].'<br>';
				$item->id_category = $res['id_category'];
				echo 'id_category : '.$res['id_category'].'<br>';
				$item->item_name = $res['path'];
				echo 'item_name : '.$item->item_name.'<br>';
				$item->type = 'video';
				echo 'Item Type: '.$item->type.'<br>';
				$item->enabled = $res['enabled'];
				echo 'enabled : '.$res['enabled'].'<br>';
				$item->counter_views = $res['counter_views'];
				echo 'counter_views : '.$res['counter_views'].'<br>';
				$item->path_type = 'remote';
				echo 'remote  : '.$item->path_type.'<br>';
				$item->publish_date = $res['publish_date'];
				echo 'publish_date : '.$res['publish_date'].'<br>';
				$item->thumbnail = $res['thumbnail'];
				echo $item->thumbnail;
			} else {
				$item->title = $res['title'];
				echo 'title : '.$res['title'].'<br>';
				$item->description = $res['description'];
				echo 'description : '.$res['description'].'<br>';
				$item->id_category = $res['id_category'];
				echo 'id_category : '.$res['id_category'].'<br>';
				$item->item_name = $res['id'].'.flv';
				echo 'item_name : '.$item->item_name.'<br>';
				$item->type = 'video';
				echo 'Item Type: '.$item->type.'<br>';
				$item->enabled = $res['enabled'];
				echo 'enabled : '.$res['enabled'].'<br>';
				$item->counter_views = $res['counter_views'];
				echo 'counter_views : '.$res['counter_views'].'<br>';
				$item->path_type = 'local';
				echo 'local  : '.$item->path_type.'<br>';
				$item->publish_date = $res['publish_date'];
				echo 'publish_date : '.$res['publish_date'].'<br>';
				$item->thumbnail = $res['id'].'.jpg';
				echo $item->thumbnail;
			}

			$r = $this->model->saveItem($item);
			if (!is_array($r)) {
				echo '---STATUS OK---</br></br>';
			} else {
				echo '</br>---STATUS KO---</br>';
			}
		}
	}
}
