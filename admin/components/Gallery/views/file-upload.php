
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#upload').on('click', function () {
            $("#upload").prop("disabled", true);
            $('#msg').html("<div class='alert alert-success'>[$wait_images_are_uploading]</div>");
            var form_data = new FormData();
            var image_category = document.getElementById('image_category').value;
            var ins = document.getElementById('multiFiles').files.length;
            for (var x = 0; x < ins; x++) {
                form_data.append("files[]", document.getElementById('multiFiles').files[x]);
                form_data.append("image_category[]", document.getElementById('image_category').value);
            }
            $.ajax({
                url: '<?= Utils::getComponentUrl('Gallery/handleFileUpload'); ?>', // point to server-side PHP script 
                dataType: 'text', // what to expect back from the PHP script
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    document.getElementById("multiFiles").value = "";
                    $("#upload").prop("disabled", false);
                    $('#msg').html("");
                    $('#msg').append(response); // display success response from the PHP script
                },
                error: function (response) {
                    $('#msg').html("");
                    $('#msg').append(response); // display error response from the PHP script
                }
            });
        });
    });
</script>









<div class="col-md-12">
    <div class="form-horizontal form-bordered" >
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">

                    <div class="panel-body panel-form">
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$ChooseCategory]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="image_category" name="category">
                                    <?php foreach ($categories as $category) {
	?>

                                        <option value="<?= $category->id ?>"><?= $category->category ?></option>

                                    <?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$ChooseFiles]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="file" id="multiFiles" name="files[]" multiple="multiple"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6">
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-info"  id="upload">[$Caricare]</button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-md-12">
    <div class="form-horizontal form-bordered" >
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
                    <div class="panel-body panel-form">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12" id="msg">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




