<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Gallery');

foreach ($images as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_item_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_item_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_item_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		new Button('<i class="fa fa-eye"></i>', $url_item_show.'/'.$row->id_category.'/'.$row->item_name, 'xs', '', '[$Show]'),
		new Button('<i class="fa fa-download"></i>', $url_item_download.'/'.$row->id, 'xs', '', '[$Download]'),
	);
	$row->row_imagePath = 'gallery'.DS.$row->id_category;
}
$table->setElements($images);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('title', '[$Title]', 'string', 'left'),
	new TableList_Field('item_name', '[$Preview]', 'image', 'left'),
	new TableList_Field('category', '[$Category]', 'string', 'left'),
	new TableList_Field('publish_date', '[$PublishDate]', 'date', 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
));
$table->setUrl_action($url_images_list);
$table->setUrl_delete($url_item_delete);
$table->render();
