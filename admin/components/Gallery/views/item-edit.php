<?php
$title = isset($_POST['title']) ? $_POST['title'] : $item->title;
$description = isset($_POST['description']) ? $_POST['description'] : $item->description;
$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $item->id_category;
$publish_date = isset($_POST['publish_date']) ? $_POST['publish_date'] : $item->publish_date;
$type = isset($_POST['type']) ? $_POST['type'] : $item->type;

if (!is_null($item->id) && ($item->path_type == 'remote' || $item->type == 'youtube')) {
	$remote_url = $item->item_name;
} else {
	$remote_url = isset($_POST['remote_url']) ? $_POST['remote_url'] : '';
}

?>

<div class="col-md-12">
    <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
          novalidate="" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$GeneralInformation]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Type]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" name="type" <?= (!is_null($item->id) ? 'disabled' : '') ?>>
                                    <option value="image" <?= ($type == 'image') ? 'selected' : '' ?>>[$Image]</option>
                                    <option value="video" <?= ($type == 'video') ? 'selected' : '' ?>>[$Video]</option>
                                    <option value="youtube" <?= ($type == 'youtube') ? 'selected' : '' ?>>
                                        [$YoutubeVideo]
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$Title] *</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $title ?>"
                                       name="title" placeholder="[$Title]" data-parsley-required="true"
                                       data-parsley-id="6524" required>
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$Description]</label>

                            <div class="col-md-8 col-sm-8">
                            <textarea name="description" class="form-control" cols="3"
                                      style="max-width: 100%; min-width: 100%"><?= $description ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Category]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" name="id_category">
                                    <?php foreach ($categories as $categ) {
	?>
                                        <option
                                            value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
                                    <?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$File]</label>

                            <div class="col-md-6 col-sm-6">
                                <input type="file" name="file">
                                <?php if ($item->type == 'image' && $item->item_name != '') {
		?>
                                    <p>
                                        <img
                                            src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT."gallery/{$item->id_category}/".$item->item_name ?>"
                                            width="100px" alt="Image"/></p>
                                <?php
	} elseif ($item->type == 'video' && file_exists(DOCROOT.MEDIA_ROOT.'gallery'.DS.$item->id_category.DS.$item->item_name)) {
		$row_code = '<video width="320" height="240" controls>';

		$file_parts = pathinfo(DOCROOT.MEDIA_ROOT.'gallery'.DS.$item->id_category.DS.$item->item_name);

		$full_filename = substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'gallery'.DS.$item->id_category.DS.$item->item_name;
		switch ($file_parts['extension']) {
										case 'ogg':
											$row_code .= '<source src="'.$full_filename.'" type="audio/ogg">';
											break;

										case 'mp4':
											$row_code .= '<source src="'.$full_filename.'" type="audio/mp4">';
											break;

										case '3gp':
											$row_code .= '<source src="'.$full_filename.'" type="audio/3gp">';
											break;
									}
		$row_code .= 'Your browser does not support the video tag.';
		$row_code .= '</video>';
		echo $row_code;
	} ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$RemoteUrl]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control" name="remote_url"
                                       placeholder="[$RemoteUrl]" value="<?= $remote_url ?>">
                                <span>Complete only if you have not upload a file.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$PublishDate]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control" name="publish_date" id="datetimepicker1"
                                       placeholder="[$SelectDate]" value="<?= $publish_date ?>" required="">
                            </div>
                        </div>
                        <?php if (!is_null($item->id) && ($item->type == 'video' || $item->type == 'youtube')) {
		?>
                            <div class="col-md-12">
                                <label class="control-label col-md-4 col-sm-4">[$ChangeThumbnail]</label>
                                <div class="col-md-4 col-sm-4">
                                    <p>&nbsp;</p>
                                    <a class="btn btn-info"
                                       href="<?= Utils::getComponentUrl("Gallery/genVideoThumb/{$item->id}") ?>">Generate</a>
                                </div>
                                <div class="col-sm-4">
                                    <?php if (!is_null($item->thumbnail) && file_exists(DOCROOT.MEDIA_ROOT.'gallery'.DS.'thumbs'.DS.$item->thumbnail) && $item->type == 'video') {
			?>
                                        <img class="img-responsive" alt="Thumbnail"
                                             src="<?= Utils::genThumbnailUrl('gallery/thumbs/'.$item->thumbnail, 300, 0); ?>"
                                    <?php
		} elseif (!is_null($item->thumbnail) && $item->type == 'youtube') {
			?>
                                        <img class="img-responsive" alt="Thumbnail" src="<?= $item->thumbnail ?>"/>
                                    <?php
		} else {
			?>
                                        [$NoThumbnail]
                                    <?php
		} ?>
                                </div>
                            </div>
                        <?php
	} ?>
                        <div class="col-md-12 text-right">
                            <p>&nbsp;</p>
                            <button type="submit" name="save" class="btn btn-info">
                                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                            </button>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>

<script>
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>