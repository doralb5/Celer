<div class="col-md-12">
    <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
          novalidate="" enctype="multipart/form-data">

        <?php
		$countf = 0;
		foreach ($files as $file) {
			?>
            <div class="row">


                <div class="col-sm-4">
                    <img alt="Thumbnail" class="img-responsive" src="<?php $video_name = explode('.', $file);
			echo 'http://'.CMSSettings::$webdomain.DS.MEDIA_ROOT.'gallery/vid_thumbs/'.date('Y', time()).'/'.date('m', time()).'/'.'thmb_'.$video_name[0].'.jpg'; ?>" >
                </div>  

                <div class="col-md-8">


                    <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                                   data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                                   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                                   data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">[$GeneralInformation]</h4>
                        </div>


                        <div class="panel-body panel-form">

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4" for="name">[$Title] *</label>

                                <div class="col-md-8 col-sm-8">



                                    <input class="form-control" type="text" value=""
                                           name="title[<?php echo $countf; ?>]" placeholder="[$Title]" data-parsley-required="true"
                                           data-parsley-id="6524" required>
                                    <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4" for="name">[$Description]</label>

                                <div class="col-md-8 col-sm-8">
                                    <textarea name="description[<?php echo $countf; ?>]" class="form-control" cols="3"
                                              style="max-width: 100%; min-width: 100%"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 col-sm-4 control-label">[$Category]</label>

                                <div class="col-md-8 col-sm-8">
                                    <select class="form-control" name="id_category[<?php echo $countf; ?>]">
    <?php foreach ($categories as $categ) {
				?>
                                            <option
                                                value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
    <?php
			} ?>
                                    </select>
                                </div>
                            </div>



                        </div>
                    </div>

                    <input type="hidden" name="ufile[<?php echo $countf; ?>]" value="<?php echo $file; ?>">



                </div>
            </div>
            <?php
			$countf++;
		}
		?>

</div>


<div class="col-md-12 text-right">
    <p>&nbsp;</p>
    <button type="submit" name="save" class="btn btn-info">
        <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
    </button>
    <p>&nbsp;</p>
</div>


</form>
</div>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>

<script>
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>