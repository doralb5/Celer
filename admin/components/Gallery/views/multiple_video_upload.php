<style>
    .uploader
    {
        border: 2px dotted #A5A5C7;
        width: 100%;
        color: #92AAB0;
        text-align: center;
        vertical-align: middle;
        padding: 30px 0px;
        margin-bottom: 10px;
        font-size: 200%;

        cursor: default;

        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .uploader div.or {
        font-size: 50%;
        font-weight: bold;
        color: #C0C0C0;
        padding: 10px;
    }

    .uploader div.browser label {
        background-color: #5a7bc2;
        padding: 5px 15px;
        color: white;
        padding: 6px 0px;
        font-size: 40%;
        font-weight: bold;
        cursor: pointer;
        border-radius: 2px;
        position: relative;
        overflow: hidden;
        display: block;
        width: 300px;
        margin: 20px auto 0px auto;

        box-shadow: 2px 2px 2px #888888;
    }

    .uploader div.browser span {
        cursor: pointer;
    }


    .uploader div.browser input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        border: solid transparent;
        border-width: 0 0 100px 200px;
        opacity: .0;
        filter: alpha(opacity= 0);
        -o-transform: translate(250px,-50px) scale(1);
        -moz-transform: translate(-300px,0) scale(4);
        direction: ltr;
        cursor: pointer;
    }

    .uploader div.browser label:hover {
        background-color: #427fed;
    }



    /* Main Styles */
    #demo-nav {
        background: url(images/dark-bg.png);
        min-height: 40px;
    }

    #demo-nav a {
        /*color: #aaa;*/
        font-weight: bold;

        font-size: 12px;

        padding: 10px 15px;
    }

    #demo-nav a.home {
        color: #FFBF00;
        font-size: 16px;
        padding-left: 0px;
    }

    .demo-wrapper {
        margin-top:40px;
    }

    .demo-columns {
        margin-top: 35px;
        margin-bottom: 15px
    }

    .demo-note{
        color:gray;
        font-style: italic;
    }

    .demo-footer {
        padding-top: 19px;
        color: #777;
        border-top: 1px solid #e5e5e5;
    }

    /* Debug console */
    .demo-panel-debug {
        min-height: 90px;
        max-height: 90px;
        overflow: auto;
        cursor: default;
    }

    .demo-panel-debug ul {
        margin-bottom: 0px;
        padding-left: 0px;
        list-style-type:none;
    }

    .demo-panel-debug ul li,
    .demo-panel-debug ul li.demo-default {
        color: gray;
    }

    .demo-panel-debug ul li.demo-error {
        color: #990000;
    }

    .demo-panel-debug ul li.demo-success {
        color: #009900;
    }

    .demo-panel-debug ul li.demo-info {
        color: #000099;
    }

    /* D&D Demo */
    .demo-panel-files {
        max-height: 290px;
        min-height: 290px;
        overflow: auto;
        cursor: default;
    }

    .demo-file-id {
        font-weight: bold;
    }

    .demo-file-size{
        font-style: italic;
        color: gray;
        font-size: 90%;
    }

    .demo-file-status,
    .demo-file-status-default{
        color: gray;
    }

    .demo-file-status-error{
        color: #990000;
    }

    .demo-file-status-success{
        color: #009900;
    }

    .demo-image-preview {
        float:left;
        margin-right: 10px;
        margin-top: 4px;
        height: 56px;
        width: 56px;

        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .btn-glyphicon { 
        padding:8px; 
        background:#ffffff; 
        margin-right:4px; 
    }
    .icon-btn {
        padding: 1px 15px 3px 2px; 
        border-radius:50px;
        float: right;
    }
</style>

<div class="container demo-wrapper">

    <div class="row demo-columns">
        <div class="col-md-6">
            <!-- D&D Zone-->
            <div id="drag-and-drop-zone" class="uploader">
                <div>Drag &amp; Drop Videos Here</div>
                <div class="or">-or-</div>
                <div class="browser">
                    <label>
                        <span>Click to open the file Browser</span>
                        <input type="file" name="file" multiple="multiple" title="Click to add Files">
                    </label>
                </div>
            </div>
            <!-- /D&D Zone -->

            <!-- Debug box -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Debug</h3>
                </div>
                <div class="panel-body demo-panel-debug">
                    <ul id="demo-debug"><li class="demo-default"></li>
                    </ul>
                </div>
            </div>
            <!-- /Debug box -->
        </div>
        <!-- / Left column -->

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Uploads</h3>
                </div>
                <div class="panel-body demo-panel-files" id="demo-files">
                    <span class="demo-note">No Files have been selected/droped yet...</span>
                </div>
            </div>
        </div>
        <!-- / Right column -->
    </div>

    <form method="post"  id="items-form" action="<?=Utils::getComponentUrl('Gallery/save_uploaded')?>">
     <button class="btn icon-btn btn-success" ><span class="glyphicon btn-glyphicon glyphicon-plus img-circle text-info" name="avanti"></span>Avanti</button>   
    </form>

</div>

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<!-- Se non ci sono piu online i file fai inlude da folder js  -->

<script type="text/javascript" src="http://danielm.herokuapp.com/demos/dnd/js/demo.min.js"></script>
<script type="text/javascript" src="http://danielm.herokuapp.com/demos/dnd/js/dmuploader.min.js"></script>


<script type="text/javascript">
    
    $('#drag-and-drop-zone').dmUploader({
        url: '<?= Utils::getComponentUrl('Gallery/upload_handle') ?>',
        dataType: 'json',
        allowedTypes: 'video/*',
        onInit: function () {
            $.danidemo.addLog('#demo-debug', 'default', 'Plugin initialized correctly');
        },
        onBeforeUpload: function (id) {
            $.danidemo.addLog('#demo-debug', 'default', 'Starting the upload of #' + id);

            $.danidemo.updateFileStatus(id, 'default', 'Uploading...');
        },
        onNewFile: function (id, file) {
            $.danidemo.addFile('#demo-files', id, file);
        },
        onComplete: function () {
            $.danidemo.addLog('#demo-debug', 'default', 'All pending tranfers completed');
        },
        onUploadProgress: function (id, percent) {
            var percentStr = percent + '%';

            $.danidemo.updateFileProgress(id, percentStr);
        },
        onUploadSuccess: function (id, data) {
            $.danidemo.addLog('#demo-debug', 'success', 'Upload of file #' + id + ' completed');

            $.danidemo.addLog('#demo-debug', 'info', 'Server Response for file #' + id + ': ' + JSON.stringify(data));

            $.danidemo.updateFileStatus(id, 'success', 'Upload Complete');

            $.danidemo.updateFileProgress(id, '100%');
            

            $('#items-form').append('<input type="hidden" name="ufile[' + id + ']" value="' + data + '" />');
            
            },
        onUploadError: function (id, message) {
            $.danidemo.updateFileStatus(id, 'error', message);

            $.danidemo.addLog('#demo-debug', 'error', 'Failed to Upload file #' + id + ': ' + message);
        },
        onFileTypeError: function (file) {
            $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: must be a video');
        },
        onFileSizeError: function (file) {
            $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: size excess limit');
        },
        onFallbackMode: function (message) {
            $.danidemo.addLog('#demo-debug', 'info', 'Browser not supported(do something else here!): ' + message);
        }
    });







</script>