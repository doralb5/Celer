<?php

class HonestFeedback_Component extends BaseComponent
{
		public function __construct($name = '', $package = '')
		{
			if (!UserAuth::checkLoginSession()) {
				Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
			}
			
			parent::__construct($name, $package);
			
		}
	
	public function listMessages()
		{
		$this->view->render('list_messages');
		}
	
	public function editMessage($id = null)
	{
		if ($id == null){
			$this->Component->addError($this->view->getTerm('invalid_message_id'));
			Utils::BackRediret();
		}
		
		$this->view->render('edit_messages');
	}
	
	public function deleteMessage($id = null)
	{
		if ($id == null){
			$this->Component->addError($this->view->getTerm('invalid_message_id'));
			Utils::BackRediret();
		}
		
	}
	
	public function deleteMessages()
	{
		
	}
	
	
}