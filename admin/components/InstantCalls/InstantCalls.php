<?php

class InstantCalls_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Contracts/contract_edit/');

		$this->view->set('url_ins_calls_list', $this->getActionUrl('instant_calls_list'));
		$this->view->set('url_ins_call_status', $this->getActionUrl('instant_call_status'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function instant_calls_list()
	{
		HeadHTML::setTitleTag('Instant Calls'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'InstantCall.id', 'peso' => 100),
			);
			$ins_calls = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$ins_calls = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('ins_calls', $ins_calls);
		$this->view->setTitle('[$InstantCalls]');
		$this->view->render('instantcalls-list');
	}

	public function instant_call_status($id)
	{
		$instantCall = $this->model->getInstantCall($id);

		if (isset($_POST['save'])) {
			if ($instantCall->result == 'call ok') {
				if (!in_array($_POST['result'], array('call ok'))) {
					$this->AddError('You can not do this change!');
				}
			} elseif ($instantCall->result == 'call ko') {
				if (!in_array($_POST['result'], array('call ko'))) {
					$this->AddError('You can not do this change!');
				}
			} elseif ($instantCall->result == 'canceled') {
				if (!in_array($_POST['result'], array('canceled'))) {
					$this->AddError('You can not do this change!');
				}
			}

			if (count($this->view->getErrors()) == 0) {
				$instantCall->result = $_POST['result'];
				$res = $this->model->saveInstantCall($instantCall);

				if (!is_array($res)) {
					$this->view->AddNotice('Instant call has been saved successfully.');
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
		}

		$this->view->set('instantCall', $instantCall);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('instant_calls_list')));
		$this->view->setTitle('[$InstantCallStatus]');
		$this->view->render('instantcall-status');
	}

	public function ajx_InstantCalls()
	{
		if (!$this->checkRemotePermission('ajx_InstantCalls')) {
			$response['message'] = 'Permission denied!';
			$response['status'] = 0;
			echo json_encode($response);
			return;
		}
		$ins_calls = $this->model->getList(30, 0);

		foreach ($ins_calls as &$ic) {
			$ic = Utils::ObjectToArray($ic);
		}
		header('Content-Type: application/json');
		echo json_encode($ins_calls);
	}
}
