<div class="ui-sortable">
    <div class="col-md-6">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$InstantCallResult]</h4>
            </div>
            <div class="panel-body">
                <form class="" method="post">
                    <div class="form-group">
                        <label>[$Result]</label>
                        <div class="input-group">
                            <select name="result" class="form-control">
                                <?php if ($instantCall->result == 'call ok') {
	?>
                                    <option value="call ok">call ok</option>
                                <?php
} elseif ($instantCall->result == 'call ko') {
		?> 
                                    <option value="call ko">call ko</option>
                                <?php
	} elseif ($instantCall->result == 'canceled') {
		?>
                                    <option value="canceled">canceled</option>
                                <?php
	} else {
		?>
                                    <option  value="to call">to call</option>
                                    <option value="call ok">call ok</option>
                                    <option value="call ko">call ko</option>
                                    <option value="canceled">canceled</option>
                                <?php
	} ?>
                            </select>
                            <div class="input-group-btn">
                                <button class="btn btn-info" name="save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>