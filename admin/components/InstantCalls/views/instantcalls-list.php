<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('InstantCalls');
foreach ($ins_calls as &$row) {
	switch ($row->result) {
		case 'to call':
			$row->row_className = 'info';
			break;
		case 'canceled':
			$row->row_className = 'danger';
			break;
		default:
			break;
	}

	$row->rows_buttons = array(
		new Button('<i class="fa fa-random"></i>', $url_ins_call_status.'/'.$row->id, 'xs', '', '[$Status]')
		);
}
$table->setElements($ins_calls);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('lead_fullname', '[$Lead]', 'string', 'left'),
	new TableList_Field('creation_date', '[$CreationDate]', 'date', 'left'),
	new TableList_Field('result', '[$Result]', 'string', 'left'),
));
$table->setUrl_action($url_ins_calls_list);
$table->multipleDeletion(false);
$table->deleteOption(false);

$table->render();
