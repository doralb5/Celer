<?php

class Media_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/Login'));
		}
	}

	public function file_manager()
	{
		$this->view->setTitle('[$MediaManager]');
		$this->view->render('media-manager');
	}

	public function cacheManager()
	{
		if (isset($_POST['clean'])) {
			require DOCROOT.'/'.CONTROLLERS_PATH.'ImageManager/ImageManager.php';
			$ImageManager = new ImageManager();
			$ImageManager->cleanCache();
			$this->AddNotice('[$CacheCleaned]');
		}

		$this->view->setTitle('[$CacheManager]');
		$this->view->render('clean-cache');
	}
}
