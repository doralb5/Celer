<?php

// Open README file for descriptions and help.

$ftpHost = '';
$ftpPort = '21';
$ftpMode = '1';
$ftpSSL = '0';
$ftpDir = '';
$serverTmp = '';
$editableExts = 'asp,ashx,asmx,aspx,asx,axd,cfm,cgi,css,html,htm,jhtml,js,php,phtml,pl,txt,xhtml';
$sessionName = '';
$dateFormatUsa = 0;
$lockOutTime = 5;
$versionCheck = 1;
$showAdvOption = 1;
$showLockSess = 1;
$showHostInfo = 1;
$showAddons = 1;
$showDotFiles = 1;

require($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
$ftpDir = '/web/'.MEDIA_ROOT;
define('FTP_DIR', '/web/'.MEDIA_ROOT);
define('MFTP_DOCROOT', WEBROOT.COMPONENTS_PATH.'Media/views/libs/mftp/');
