<?php

class Menus_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Menus/menu_edit/');

		$this->view->set('url_menus_list', $this->getActionUrl('menus_list'));
		$this->view->set('url_menu_edit', $this->getActionUrl('menu_edit'));
		$this->view->set('url_menu_delete', $this->getActionUrl('menu_delete'));

		$this->view->set('url_detail_delete', $this->getActionUrl('MenuDetail_delete'));

		$this->view->set('url_MenuItem_edit', $this->getActionUrl('MenuItem_edit'));
		$this->view->set('url_MenuItem_delete', $this->getActionUrl('MenuItem_delete'));
		$this->view->set('url_MenuItem_enable', $this->getActionUrl('MenuItem_enable'));
		$this->view->set('url_MenuItem_disable', $this->getActionUrl('MenuItem_disable'));

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function menus_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Menus'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteMenu($id);
				$this->LogsManager->registerLog('Menu', 'delete', 'Menu deleted with id : '.$id, $id);
			}
			if ($res !== false) {
				$this->view->AddNotice('Menus has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('menus_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'Menu.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'Menu.name', 'peso' => 90),
			);
			$menus = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$menus = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($menus as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddMenu]', $this->getActionUrl('menu_edit')));

		$this->view->BreadCrumb->addDir('[$Menus]', $this->getActionUrl('menus_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('menus', $menus);
		$this->view->setTitle('[$Menus]');
		$this->view->render('menus-list');
	}

	public function menu_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Menus]', $this->getActionUrl('menus_list'));
		if (!is_null($id)) {
			$menu = $this->model->getMenu($id);
			HeadHTML::setTitleTag('Edit Menu - '.$menu->name.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditMenu]', $this->getActionUrl('menu_edit')."/$id");
		} else {
			$menu = new Menu_Entity();
			HeadHTML::setTitleTag('New Menu'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddMenu]', $this->getActionUrl('menu_edit'));
		}

		if ($menu === false) {
			$this->AddError("Menu $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['name'] == '') ? $this->AddError('Menu Name is required!') : '';
			($_POST['main'] == '') ? $this->AddError('Main is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$this->model->startTransaction();

				$menu->name = $_POST['name'];
				$menu->main = $_POST['main'];

				$inserted_id = $this->model->saveMenu($menu);

				if (!is_array($inserted_id)) {
					foreach (CMSSettings::$available_langs as $lang) {
						if (is_null($id)) {
							$detail = new MenuDetail_Entity();
							$detail->id_menu = $inserted_id;
						} else {
							$det = $this->model->getMenuDetails(1, 0, "id_menu = {$id} AND lang = '{$lang}'");
							if (count($det)) {
								$detail = $det[0];
							} else {
								$detail = new MenuDetail_Entity();
							}
							$detail->id_menu = $id;
						}

						$is_empty = ($_POST['title_'.$lang] == '') ? true : false;

						$detail->title = (!$is_empty) ? $_POST['title_'.$lang] : $_POST['title_'.CMSSettings::$default_lang];
						$detail->lang = $lang;

						$result = $this->model->saveMenuDetail($detail);

						if (is_array($result)) {
							$this->AddError('Problem creating titles of the menu!');
							$this->model->rollback();

							if (is_bool($inserted_id)) {
								Utils::RedirectTo($this->getActionUrl('menu_edit')."/$id");
							} else {
								Utils::RedirectTo($this->getActionUrl('menu_edit'));
							}
						}
					}

					$this->AddNotice('Menu has been saved successfully.');
					$this->model->commit();

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Menu', 'insert', 'Menu inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('menu_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Menu', 'update', 'Menu updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('menu_edit')."/$id");
					}
				} else {
					$this->AddError('Saving failed!');
					$this->model->rollback();
				}
			}
		} elseif (isset($_POST['save_item'])) {
			if (isset($_POST['parent_id']) && $_POST['parent_id'] != '' && $this->model->checkSubmenuLimit($_POST['parent_id'])) {
				$this->AddError('Your profile type is not permitted to add this tree of submenu!');
			}

			($_POST['type'] == '') ? $this->AddError('Type is required!') : '';
			($_POST['item'] == '') ? $this->AddError('Item name is required!') : '';
			($_POST['lang'] == '') ? $this->AddError('Language is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				$menuItem = new MenuItem_Entity();

				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
				} else {
					$_POST['image'] = $menuItem->image;
				}

				$menuItem->id_menu = $id;
				$menuItem->type = $_POST['type'];
				$menuItem->lang = $_POST['lang'];
				$menuItem->item = $_POST['item'];
				$menuItem->image = $_POST['image'];
				$menuItem->parent_id = ($_POST['parent_id'] != '') ? $_POST['parent_id'] : $menuItem->parent_id;
				$menuItem->sorting = $_POST['sorting'];

				$menuItem->target = null;
				$menuItem->parameters = null;

				switch ($_POST['type']) {
					case 'External Link':
						$menuItem->target = $_POST['target'];
						break;
					case 'Page':
						$menuItem->target = $_POST['page'];
						$menuItem->parameters = $_POST['parameters'];
						break;
				}

				$inserted_id = $this->model->saveMenuItem($menuItem);
				if (!is_array($inserted_id)) {
					$this->AddNotice('MenuItem has been saved successfully.');

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'menus';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('MenuItem', 'insert', 'MenuItem inserted with id : '.$inserted_id, $inserted_id);
					}
					Utils::RedirectTo($this->getActionUrl('menu_edit')."/$id");
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}

		if (!is_null($id)) {
			$menuDetails = $this->model->getMenuDetails("id_menu = {$id}");
			$this->view->set('menuDetails', $menuDetails);

			$menuItems = $this->model->getMenuItems("id_menu = {$id}");
			$this->view->set('menuItems', $menuItems);
			$this->view->set('items', $menuItems);

			$pages_md = Loader::getModel('Pages');
			$pages = $pages_md->getList(100, 0);
			$this->view->set('pages', $pages);
		}

		foreach (CMSSettings::$available_langs as $lang) {
			$mDetail = $this->model->getMenuDetails(1, 0, "id_menu = {$id} AND lang = '{$lang}'");
			$menu->{$lang} = count($mDetail) ? $mDetail[0] : null;
		}

		$this->view->set('menu', $menu);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('menus_list'))));
		$title = (!is_null($id)) ? '[$EditMenu]' : '[$NewMenu]';
		$this->view->setTitle($title);
		$this->view->render('menu-edit');
	}

	public function menu_delete($id)
	{
		$menu = $this->model->getMenu($id);
		$result = $this->model->deleteMenu($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Menu', 'delete', "Menu deleted with id : $id and name : ".$menu->name, $id);
			$this->AddNotice('Menu deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('menus_list'));
	}

	public function menu_enable($id)
	{
		if (!is_null($id)) {
			$menu = $this->model->getMenu($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$menu->enabled = 1;
		$res = $this->model->saveMenu($menu);

		if (!is_array($res)) {
			$this->view->AddNotice('Menu has been enabled successfully.');
			$this->LogsManager->registerLog('Menu', 'update', 'Menu enabled with id : '.$menu->id, $menu->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('menus_list'));
	}

	public function menu_disable($id)
	{
		if (!is_null($id)) {
			$menu = $this->model->getMenu($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$menu->enabled = 0;
		$res = $this->model->saveMenu($menu);

		if (!is_array($res)) {
			$this->view->AddNotice('Menu has been disabled successfully.');
			$this->LogsManager->registerLog('Menu', 'update', 'Menu disabled with id : '.$menu->id, $menu->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('menus_list'));
	}

	public function MenuDetail_delete($id)
	{
		$detail = $this->model->getMenuDetail($id);
		$result = $this->model->deleteMenuDetail($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('MenuDetail', 'delete', "MenuDetail deleted with id : $id, title : {$detail->title}", $id);
			$this->AddNotice('Detail deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('menu_edit')."/{$detail->id_menu}");
	}

	public function MenuItem_delete($id)
	{
		$menuitem = $this->model->getMenuItem($id);
		$result = $this->model->deleteMenuItem($id);
		if ($result !== false) {
			//Fshijme dhe te gjithe femijet e saj
			$this->recursiveDeleteMenuItem($id);

			$target = DOCROOT.MEDIA_ROOT.DS.'menus';
			if ($menuitem->image != '' && file_exists($target.DS.$menuitem->image)) {
				unlink($target.DS.$menuitem->image);
			}

			$this->LogsManager->registerLog('MenuItem', 'delete', "Menu item deleted with id : $id and name : ".$menuitem->item, $id);
			$this->AddNotice('Menu item deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('menu_edit')."/{$menuitem->id_menu}");
	}

	private function recursiveDeleteMenuItem($parent_id)
	{
		$menuitems = $this->model->getMenuItems("parent_id = {$parent_id}");
		foreach ($menuitems as $item) {
			$this->model->deleteMenuItem($item->id);
			$target = DOCROOT.MEDIA_ROOT.DS.'menus';
			if ($item->image != '' && file_exists($target.DS.$item->image)) {
				unlink($target.DS.$item->image);
			}
			$this->recursiveDeleteMenuItem($item->id);
		}
	}

	public function MenuItem_enable($id)
	{
		if (!is_null($id)) {
			$menuitem = $this->model->getMenuItem($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$menuitem->enabled = 1;
		$res = $this->model->saveMenuItem($menuitem);

		if (!is_array($res)) {
			$this->view->AddNotice('Menu item has been enabled successfully.');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('menu_edit')."/{$menuitem->id_menu}");
	}

	public function MenuItem_disable($id)
	{
		if (!is_null($id)) {
			$menuitem = $this->model->getMenuItem($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$menuitem->enabled = 0;
		$res = $this->model->saveMenuItem($menuitem);

		if (!is_array($res)) {
			$this->view->AddNotice('Menu item has been disabled successfully.');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('menu_edit')."/{$menuitem->id_menu}");
	}

	public function MenuItem_edit($id)
	{
		if (!is_null($id)) {
			$menuItem = $this->model->getMenuItem($id);
			$this->view->set('menuItem', $menuItem);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		if (isset($_POST['save'])) {
			($_POST['type'] == '') ? $this->AddError('Type is required!') : '';
			($_POST['item'] == '') ? $this->AddError('Item name is required!') : '';
			($_POST['lang'] == '') ? $this->AddError('Language is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $menuItem->image;
				} else {
					$_POST['image'] = $menuItem->image;
					$old_image = null;
				}
				if (isset($_POST['resetImage'])) {
					$_POST['image'] = null;
					$old_image = $menuItem->image;
				}

				$menuItem->type = $_POST['type'];
				$menuItem->lang = $_POST['lang'];
				$menuItem->item = $_POST['item'];
				$menuItem->parent_id = ($_POST['parent_id'] != '') ? $_POST['parent_id'] : $menuItem->parent_id;
				$menuItem->sorting = $_POST['sorting'];
				$menuItem->image = $_POST['image'];

				$menuItem->target = null;
				$menuItem->parameters = null;

				switch ($_POST['type']) {
					case 'External Link':
						$menuItem->target = $_POST['target'];
						break;
					case 'Page':
						$menuItem->target = $_POST['page'];
						$menuItem->parameters = $_POST['parameters'];
						break;
				}

				$res = $this->model->saveMenuItem($menuItem);
				if (!is_array($res)) {
					$this->AddNotice('Menu item has been saved successfully.');

					$target = DOCROOT.MEDIA_ROOT.DS.'menus';
					if (strlen($_FILES['image']['tmp_name'])) {
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id) && file_exists($target.DS.$old_image)) {
							unlink($target.DS.$old_image);
						}
					} elseif (isset($_POST['resetImage'])) {
						if (!is_null($id) && file_exists($target.DS.$old_image)) {
							unlink($target.DS.$old_image);
						}
					}

					$this->LogsManager->registerLog('MenuItem', 'update', 'Menu item updated with id : '.$id, $id);
				} else {
					$this->AddError('Something went wrong!');
				}
				Utils::RedirectTo($this->getActionUrl('menu_edit')."/{$menuItem->id_menu}");
			}
		}

		$menuItems = $this->model->getMenuItems("id_menu = {$menuItem->id_menu}");
		$this->view->set('items', $menuItems);

		$pages_md = Loader::getModel('Pages');
		$pages = $pages_md->getList(100, 0);
		$this->view->set('pages', $pages);

		$menu = $this->model->getMenu($menuItem->id_menu);
		$this->view->set('menu', $menu);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('menu_edit')."/{$menuItem->id_menu}")));
		$this->view->setTitle('[$EditItem]');
		$this->view->render('menu-item-edit');
	}
}
