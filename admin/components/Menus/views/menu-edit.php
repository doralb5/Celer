<?php
$name = isset($_POST['name']) ? $_POST['name'] : $menu->name;
$main = isset($_POST['main']) ? $_POST['main'] : $menu->main;

foreach (CMSSettings::$available_langs as $lang) {
	$title_{$lang} = isset($_POST['title_'.$lang]) ? $_POST['title_'.$lang] : ((!is_null($menu->{$lang})) ? $menu->{$lang}->title : '');
}

?>

<div class="col-md-6">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$Menu]</h4>
        </div>
        <div class="panel-body panel-form">

            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true"
                  name="demo-form"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Name] * :</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $name ?>"
                               name="name" placeholder="[$Name]" data-parsley-required="true"
                               data-parsley-id="6524" required>
                        <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$MainMenu]</label>

                    <div class="col-md-6 col-sm-6">
                        <select class="form-control" name="main">
                            <option value="0" <?= ($main == '0') ? 'selected' : '' ?>>[$NoOption]</option>
                            <option value="1" <?= ($main == '1') ? 'selected' : '' ?>>[$YesOption]</option>
                        </select>
                    </div>
                </div>


                <ul class="nav nav-tabs">
                    <?php foreach (CMSSettings::$available_langs as $lang) {
	?>
                        <li class="<?= ($lang == CMSSettings::$default_lang) ? 'active' : '' ?>"><a
                                href="#tab_<?= $lang ?>" data-toggle="tab"><?= strtoupper($lang) ?></a></li>
                    <?php
} ?>
                </ul>
                <div class="tab-content">
                    <?php foreach (CMSSettings::$available_langs as $lang) {
		?>

                        <div
                            class="tab-pane fade <?= ($lang == CMSSettings::$default_lang) ? 'active in' : '' ?>"
                            id="tab_<?= $lang ?>">

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4" for="name">[$Title]</label>

                                <div class="col-md-8 col-sm-8">
                                    <input class="form-control" type="text" value="<?= $title_{$lang} ?>"
                                           name="title_<?= $lang ?>" placeholder="[$Title]">
                                </div>
                            </div>

                        </div>
                    <?php
	} ?>
                </div>


                <p>&nbsp;</p>
                <div class="col-md-12 text-right">
                    <button type="submit" name="save" class="btn btn-info">
                        <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                </div>
                <p>&nbsp;</p>
            </form>
        </div>
    </div>
</div>

<?php if (!is_null($menu->id)) {
		?>

    <!--Menu Items-->
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$MenuItems]</h4>
            </div>
            <div class="panel-body panel-form">
                <p>&nbsp;</p>
                <?php
				include(VIEWS_PATH.'TableList_class.php');

		$table = new TableListView('MenuItems', 'list');
		foreach ($menuItems as &$row) {
			if ($row->enabled) {
				$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_MenuItem_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
			} else {
				$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_MenuItem_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
			}
			$row->rows_buttons = array(
						$btn1,
						new Button('<i class="fa fa-pencil"></i>', $url_MenuItem_edit.'/'.$row->id, 'xs', '', '[$Edit]')
					);
		}
		$table->setElements($menuItems);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
					new TableList_Field('type', '[$Type]', 'string', 'left'),
					new TableList_Field('item', '[$Item]', 'string', 'left'),
					new TableList_Field('lang', '[$Lang]', 'string', 'left'),
					new TableList_Field('target', '[$Target]', 'string', 'left'),
					new TableList_Field('parameters', '[$Parameters]', 'string', 'left'),
					new TableList_Field('sorting', '[$Sorting]', 'string', 'left'),
					new TableList_Field('image', '[$Image]', 'image', 'left'),
					new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
				));
		$table->setUrl_delete($url_MenuItem_delete);
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->setImagesPath(substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'menus');
		$table->render(); ?>

                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true"
                      name="demo-form"
                      enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 col-sm-4 control-label">[$Type]</label>

                                <div class="col-md-8 col-sm-8">
                                    <select class="form-control" name="type" id="item-type" required>
                                        <option value="Page">[$Page]</option>
                                        <option value="External Link">[$ExternalLink]</option>
                                        <option value="No Link">[$NoLink]</option>
                                        <option value="SubMenu">[$SubMenu]</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4" for="name">[$ItemName] * :</label>

                                <div class="col-md-8 col-sm-8">
                                    <input class="form-control" type="text"
                                           name="item" placeholder="[$Name]" data-parsley-required="true"
                                           data-parsley-id="6524" required>
                                    <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-sm-4 control-label">[$Laguage]</label>

                                <div class="col-md-8 col-sm-8">
                                    <select class="form-control" name="lang" required>
                                        <?php foreach (CMSSettings::$available_langs as $lang) {
			?>
                                            <option value="<?= $lang ?>"><?= strtoupper($lang) ?></option>
                                        <?php
		} ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-sm-4 control-label">[$ParentItem]</label>

                                <div class="col-md-8 col-sm-8">
                                    <select class="form-control" name="parent_id">
                                        <option value="">-- [$NoParent] --</option>
                                        <?php foreach ($items as $item) {
			?>
                                            <option
                                                value="<?= $item->id ?>"><?= strtoupper($item->lang).' &raquo '.$item->item ?></option>
                                        <?php
		} ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4">[$Sorting] * :</label>

                                <div class="col-md-8 col-sm-8">
                                    <input class="form-control" type="number"
                                           name="sorting" placeholder="[$Sorting]" data-parsley-required="true"
                                           data-parsley-id="6524" required value="10">
                                    <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                                <div class="col-md-6 col-sm-6">
                                    <input type="file" name="image">
                                </div>
                            </div>


                        </div>

                        <div class="col-md-6" id="external-link">
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4">[$Target] * :</label>
                                <div class="col-md-8 col-sm-8">
                                    <input class="form-control" type="text"
                                           name="target" placeholder="[$ExternalLink]">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6" id="page">
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4">[$Page] * :</label>
                                <div class="col-md-8 col-sm-8">
                                    <select class="form-control" name="page">
                                        <?php foreach ($pages as $page) {
			?>
                                            <option value="<?= $page->id ?>"><?= $page->name ?></option>
                                        <?php
		} ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4">[$Parameters]</label>
                                <div class="col-md-8 col-sm-8">
                                    <input class="form-control" type="text"
                                           name="parameters" placeholder="param1/param2?param4=val&param5=val">
                                </div>
                            </div>
                        </div>

                    </div>


                    <p>&nbsp;</p>
                    <div class="col-md-12 text-right">
                        <button type="submit" name="save_item" class="btn btn-info">
                            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                        </button>
                    </div>
                    <p>&nbsp;</p>
                </form>
            </div>
        </div>
    </div>

<?php
	} ?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/js/form-plugins.demo.min.js'); ?>

<script>
    $(document).ready(function () {
        $('#page').show();
        $('#external-link').hide();
        $('#item-type').change(function () {
            if ($('#item-type').val() == "External Link") {
                $('#external-link').show();
                $('#page').hide();
            } else if ($('#item-type').val() == "Page") {
                $('#page').show();
                $('#external-link').hide();
            } else {
                $('#external-link').hide();
                $('#page').hide();
            }
        });
    });
</script>