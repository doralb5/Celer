<?php
$type = isset($_POST['type']) ? $_POST['type'] : $menuItem->type;
$item = isset($_POST['item']) ? $_POST['item'] : $menuItem->item;
$item_lang = isset($_POST['lang']) ? $_POST['lang'] : $menuItem->lang;
$parent_id = isset($_POST['parent_id']) ? $_POST['parent_id'] : $menuItem->parent_id;
$sorting = isset($_POST['sorting']) ? $_POST['sorting'] : $menuItem->sorting;
$target = isset($_POST['target']) ? $_POST['target'] : $menuItem->target;
$parameters = isset($_POST['parameters']) ? $_POST['parameters'] : $menuItem->parameters;
?>

<!--Menu Items-->
<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">[$ItemOfMenu] - <?= $menu->name ?></h4>
        </div>
        <div class="panel-body panel-form">
            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true"
                  name="demo-form"
                  enctype="multipart/form-data">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Type]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" name="type" id="item-type" required>
                                    <option value="Page" <?= ($type == 'Page') ? 'selected' : '' ?>>[$Page]</option>
                                    <option
                                        value="External Link" <?= ($type == 'External Link') ? 'selected' : '' ?>>
                                        [$ExternalLink]
                                    </option>
                                    <option value="No Link" <?= ($type == 'No Link') ? 'selected' : '' ?>>[$NoLink]
                                    </option>
                                    <option value="SubMenu" <?= ($type == 'SubMenu') ? 'selected' : '' ?>>[$SubMenu]
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$ItemName] * :</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text"
                                       name="item" placeholder="[$Name]" data-parsley-required="true"
                                       data-parsley-id="6524" value="<?= $item ?>" required>
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Laguage]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" name="lang" required>
                                    <?php foreach (CMSSettings::$available_langs as $lang) {
	?>
                                        <option
                                            value="<?= $lang ?>" <?= ($item_lang == $lang) ? 'selected' : ''; ?>><?= strtoupper($lang) ?></option>
                                    <?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$ParentItem]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" name="parent_id">
                                    <option value="">-- [$NoParent] --</option>
                                    <?php foreach ($items as $item) {
		if ($item->id == $menuItem->id) {
			continue;
		} ?>
                                        <option
                                            value="<?= $item->id ?>" <?= ($item->id == $parent_id) ? 'selected' : ''; ?>><?= $item->item ?></option>
                                    <?php
	} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Sorting] * :</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="number"
                                       name="sorting" placeholder="[$Sorting]" data-parsley-required="true"
                                       data-parsley-id="6524" required value="<?= $sorting ?>">
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                            <div class="col-md-4 col-sm-4">
                                <input type="file" name="image">
                                <?php if ($menuItem->image != '') {
		?>
                                    <p>
                                        <img
                                            src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'menus/'.$menuItem->image ?>"
                                            width="100px" alt="Image"/></p>
                                <?php
	} ?>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="resetImage" value="1">
                                    [$Reset]
                                </label>
                            </div>
                        </div>


                    </div>

                    <div class="col-md-6" id="external-link">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Target] * :</label>
                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text"
                                       name="target" placeholder="[$ExternalLink]" value="<?= $target ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" id="page">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Page] * :</label>
                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" name="page">
                                    <?php foreach ($pages as $page) {
		?>
                                        <option
                                            value="<?= $page->id ?>"
                                            <?= ($page->id == $target) ? 'selected' : ''; ?>
                                            <?= ($page->enabled == '0') ? 'disabled' : ''; ?>><?= $page->name ?></option>
                                    <?php
	} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Parameters]</label>
                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text"
                                       name="parameters" value="<?= $parameters ?>"
                                       placeholder="param1/param2?param4=val&param5=val">
                            </div>
                        </div>
                    </div>

                </div>


                <p>&nbsp;</p>
                <div class="col-md-12 text-right">
                    <button type="submit" name="save" class="btn btn-info">
                        <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                </div>
                <p>&nbsp;</p>
            </form>
        </div>
    </div>
</div>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/js/form-plugins.demo.min.js'); ?>

<script>

    $(document).ready(function () {
        $('#page').hide();
        $('#external-link').hide();
        <?php
		switch ($menuItem->type) {
			case 'Page':
				echo "$('#page').show();";
				break;
			case 'External Link':
				echo "$('#external-link').show();";
				break;
		}
		?>
        $('#item-type').change(function () {
            if ($('#item-type').val() == "External Link") {
                $('#external-link').show();
                $('#page').hide();
            } else if ($('#item-type').val() == "Page") {
                $('#page').show();
                $('#external-link').hide();
            } else {
                $('#external-link').hide();
                $('#page').hide();
            }
        });
    });
</script>