<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Menus');

foreach ($menus as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_menu_edit.'/'.$row->id, 'xs', '', '[$Edit]')
	);
}
$table->setElements($menus);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('main', '[$Main]', 'bool', 'left'),
));
$table->setUrl_action($url_menus_list);
$table->setUrl_delete($url_menu_delete);
$table->render();
