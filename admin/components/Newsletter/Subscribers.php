<?php

class Subscribers_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_subscribers_list', $this->getActionUrl('subscribers_list'));
		$this->view->set('url_subscriber_delete', $this->getActionUrl('subscriber_delete'));
		$this->view->set('url_subscriber_enable', $this->getActionUrl('subscriber_enable'));
		$this->view->set('url_subscriber_disable', $this->getActionUrl('subscriber_disable'));

		$this->view->set('url_categories_list', $this->getActionUrl('categories_list'));
		$this->view->set('url_category_edit', $this->getActionUrl('category_edit'));
		$this->view->set('url_category_delete', $this->getActionUrl('category_delete'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function subscribers_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Subscribers'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = "deleted = '0'";

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$sbs = $this->model->getSubscriber($id);
				$sbs->deleted = 1;
				$res = $this->model->saveSubscriber($sbs);
				$this->LogsManager->registerLog('Subscriber', 'delete', 'Subscriber deleted with id : '.$id, $id);
			}
			if (!is_array($res)) {
				$this->view->AddNotice('Subscribers has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('subscribers_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'Subscriber.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'Subscriber.email', 'peso' => 90),
			);
			$subscribers = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$subscribers = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		$this->view->BreadCrumb->addDir('[$Subscribers]', $this->getActionUrl('subscribers_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('subscribers', $subscribers);
		$this->view->setTitle('[$Subscribers]');
		$this->view->render('subscribers-list');
	}

	public function subscriber_delete($id = null)
	{
		if (!is_null($id)) {
			$subscriber = $this->model->getSubscriber($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$subscriber->deleted = 1;
		$result = $this->model->saveSubscriber($subscriber);
		if (!is_array($result)) {
			$this->LogsManager->registerLog('Subscriber', 'delete', "Subscriber deleted with id : $id and email : ".$subscriber->email, $id);
			$this->AddNotice('Subscriber deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('subscribers_list'));
	}

	public function subscriber_enable($id = null)
	{
		if (!is_null($id)) {
			$subscriber = $this->model->getSubscriber($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$subscriber->enabled = 1;
		$res = $this->model->saveSubscriber($subscriber);

		if (!is_array($res)) {
			$this->view->AddNotice('Subscriber has been enabled successfully.');
			$this->LogsManager->registerLog('Subscriber', 'update', 'Subscriber enabled with id : '.$subscriber->id, $subscriber->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('subscribers_list'));
	}

	public function subscriber_disable($id = null)
	{
		if (!is_null($id)) {
			$subscriber = $this->model->getSubscriber($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$subscriber->enabled = 0;
		$res = $this->model->saveSubscriber($subscriber);

		if (!is_array($res)) {
			$this->view->AddNotice('Subscriber has been disabled successfully.');
			$this->LogsManager->registerLog('Subscriber', 'update', 'Subscriber disabled with id : '.$subscriber->id, $subscriber->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('subscribers_list'));
	}

	public function categories_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Categories'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteCategory($id);
				$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Categories has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('categories_list'));
		}

		$categories = $this->model->getCategories($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', $this->getActionUrl('category_edit')));
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));
		if (!is_null($id)) {
			$category = $this->model->getCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$category->category.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditCategory]', $this->getActionUrl('category_edit')."/$id");
		} else {
			$category = new SubscriberCategory_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddCategory]', $this->getActionUrl('category_edit'));
		}

		if ($category === false) {
			$this->AddError("Category $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['category'] == '') ? $this->AddError('Category Name is required!') : '';
			if (count($this->view->getErrors()) == 0) {
				$category->category = $_POST['category'];
				$category->description = $_POST['description'];
				$inserted_id = $this->model->saveCategory($category);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Category has been saved successfully!');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('SubscriberCategory', 'insert', 'Subscriber Category inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('SubscriberCategory', 'update', 'Subscriber Category updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('category', $category);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('categories_list'))));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id)
	{
		$category = $this->model->getCategory($id);
		$result = $this->model->deleteCategory($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id, name : {$category->category}", $id);
			$this->AddNotice('Category deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('categories_list'));
	}
}
