<?php
$category_name = isset($_POST['category']) ? $_POST['category'] : $category->category;
$description = isset($_POST['description']) ? $_POST['description'] : $category->description;
?>

    <div class="col-md-6">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$Category]</h4>
            </div>
            <div class="panel-body panel-form">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true"
                      name="demo-form" novalidate="">


                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="name">[$Name] *</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $category_name ?>"
                                   name="category" placeholder="[$Name]" data-parsley-required="true"
                                   data-parsley-id="6524" required>
                            <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="name">[$Description]</label>

                        <div class="col-md-8 col-sm-8">
                            <textarea name="description" class="form-control" rows="3"
                                      style="max-width: 100%; min-width: 100%"><?= $description ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <p>&nbsp;</p>
                        <button type="submit" name="save" class="btn btn-info pull-right"><i
                                class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                        </button>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>


                </form>
            </div>
        </div>
        <!-- end panel -->
    </div>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>