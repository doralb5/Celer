<?php

class Doc_Categories_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('NuvolaDoc/Doc_Categories/category_edit/');

		$this->view->set('url_category_edit', $this->getActionUrl('category_edit'));
		$this->view->set('url_category_search', $this->getActionUrl('categories_list'));
		$this->view->set('url_category_delete', $this->getActionUrl('category_delete'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Admins/login'));
		}
	}

	public function categories_list()
	{
		HeadHTML::setTitleTag('Categories'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['del_selected']) && isset($_POST['elements'])) {
			$deleted = 0;
			foreach ($_POST['elements'] as $id) {
				$res = $this->model->deleteCategory($id);
				if ($res !== false) {
					$category_path = DOCROOT.MEDIA_ROOT.'documents'.DS.$id;
					if (file_exists($category_path)) {
						Utils::deleteDirectory($category_path);
					}
					$deleted++;
				}
			}

			if ($deleted) {
				$this->AddNotice('Has been deleted '.$deleted.' categories.');
			} else {
				$this->AddError('Categories can not be deleted!');
			}

			Utils::RedirectTo($this->getActionUrl('categories_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'id', 'peso' => 100),
				array('field' => 'name', 'peso' => 90),
			);
			$categories = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$categories = $this->model->getList($elements_per_page, $offset, $filter);
		}

		//Emri i Kategorise Superiore
		foreach ($categories as &$categ) {
			$parent = $this->model->getDocCategory($categ->parent_id);
			if (!is_null($parent)) {
				$categ->parent_category = $parent->name;
			} else {
				$categ->parent_category = '-';
			}
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($categories as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', $this->getActionUrl('category_edit')));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		$client_info = UserAuth::getLoggedClientInfo();

		if (!is_null($id)) {
			$doccategory = $this->model->getDocCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$doccategory->name.' | '.CMS_Settings::$website_title);
		} else {
			$doccategory = new DocCategory_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMS_Settings::$website_title);
		}

		if ($doccategory === false) {
			$this->AddError("Model $id not found");
		} elseif (isset($_POST['save'])) {

			//Categories Limits
			$categ_number = $this->model->categoriesNumber();
			if (is_null($id) && ($categ_number >= $client_info['categories_limit'])) {
				$this->AddError('You have reached the maximum number of categories!<br/>You can not add another category!');
				$this->LogsManager->registerLog('DocCategory', 'insert', 'Maximum document categories limit reached.');
				Utils::RedirectTo($this->getActionUrl('categories_list'));
			}

			$doccategory->name = $_POST['name'];
			$doccategory->parent_id = $_POST['parent_id'];
			$result = $this->model->saveDocCategory($doccategory);

			if (!is_array($result)) {
				$this->AddNotice('[$categ_add_success]');

				if (is_null($id)) {
					$category_path = DOCROOT.MEDIA_ROOT.'documents'.DS.$result;
					if (!file_exists($category_path)) {
						Utils::createDirectory($category_path);
					}
					$this->LogsManager->registerLog('DocCategory', 'insert', 'Document Category inserted with id : '.$id, $result);
					Utils::RedirectTo($this->getActionUrl('category_edit')."/$result");
				} else {
					$this->LogsManager->registerLog('DocCategory', 'update', 'Document Category updated with id : '.$id, $id);
					Utils::RedirectTo($this->getActionUrl('category_edit')."/$id");
				}
			} else {
				$this->AddError('[$save_failed]');
			}
		}

		$doccategories = $this->model->getList();
		$this->view->set('doccategories', $doccategories);

		$this->view->set('doccategory', $doccategory);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('categories_list')));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id)
	{
		$doccategory = $this->model->getDocCategory($id);
		$result = $this->model->deleteCategory($id);
		if ($result !== false) {
			$this->AddNotice('[$categ_del_success]');
			$this->LogsManager->registerLog('DocCategory', 'delete', "Document Category deleted with id : $id and name : ".$doccategory->name, $id);

			//Fshijme directorine e kategorise
			$category_path = DOCROOT.MEDIA_ROOT.'documents'.DS.$id;
			if (file_exists($category_path)) {
				Utils::deleteDirectory($category_path);
			}
		} else {
			$this->AddError('[$delete_failed]');
		}

		Utils::RedirectTo($this->getActionUrl('categories_list'));
	}
}
