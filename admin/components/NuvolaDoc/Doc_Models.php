<?php

class Doc_Models_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('NuvolaDoc/Doc_Models/model_edit/');

		$this->view->set('url_model_edit', $this->getActionUrl('model_edit'));
		$this->view->set('url_model_search', $this->getActionUrl('models_list'));
		$this->view->set('url_model_delete', $this->getActionUrl('model_delete'));
		$this->view->set('url_modelField_delete', $this->getActionUrl('modelField_delete'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Admins/login'));
		}
	}

	public function models_list()
	{
		HeadHTML::setTitleTag('Models'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['del_selected']) && isset($_POST['elements'])) {
			foreach ($_POST['elements'] as $id) {
				$docmodel = $this->model->getDocModel($id);
				if ($docmodel->view != '') {
					unlink(DOCROOT.DATA_DIR.'docmodels'.DS.$docmodel->view);
				}

				$res = $this->model->deleteDocModel($id);
			}
			Utils::RedirectTo($this->getActionUrl('models_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'id', 'peso' => 100),
				array('field' => 'name', 'peso' => 90),
			);
			$models = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$models = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$this->view->setTitle('[$Models]');

		$totalElements = $this->model->getLastCounter();

		foreach ($models as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('models', $models);

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddModel]', $this->getActionUrl('model_edit')));
		$this->view->render('models-list');
	}

	public function model_edit($id = null)
	{
		$client_info = UserAuth::getLoggedClientInfo();

		if (!is_null($id)) {
			$docmodel = $this->model->getDocModel($id);
			$modelFields = $this->model->getDocModelFields($id);
			HeadHTML::setTitleTag('Edit Model - '.$docmodel->name.' | '.CMS_Settings::$website_title);
		} else {
			$docmodel = new DocModel_Entity();
			$modelFields = array();
			HeadHTML::setTitleTag('New Model'.' | '.CMS_Settings::$website_title);
		}

		//Na sherben per te ruajtur fushat
		$docmodelfield = new DocModelField_Entity();

		if ($docmodel === false) {
			$this->AddError("Model $id not found");
		} elseif (isset($_POST['change'])) {

			//Models Limits
			$models_number = $this->model->modelsNumber();
			if (is_null($id) && $models_number >= $client_info['models_limit']) {
				$this->AddError('You have reached the maximum number of document models!<br/>You can not add another model!');
				$this->LogsManager->registerLog('DocModel', 'insert', 'Maximum document models limit reached.');
				Utils::RedirectTo($this->getActionUrl('models_list'));
			}

			($_POST['modelname'] == '') ? $this->AddError('Model name must not be empty!') : '';
			if (count($this->view->getErrors()) == 0) {
				if (isset($_FILES['modelview']['tmp_name']) && strlen($_FILES['modelview']['tmp_name'])) {
					(!Utils::allowedFileType($_FILES['modelview']['name'], array('php', 'html'))) ? $this->view->AddError('Please upload a php or html file!') : '';
				}

				if (count($this->view->getErrors()) == 0) {
					if (strlen($_FILES['modelview']['tmp_name'])) {
						$_POST['modelview'] = time().'_'.$_FILES['modelview']['name'];

						$target = DOCROOT.DATA_DIR.DS.'docmodels'.DS.$_POST['modelview'];
						move_uploaded_file($_FILES['modelview']['tmp_name'], $target);
					} else {
						$_POST['modelview'] = $docmodel->view;
					}

					$docmodel->name = $_POST['modelname'];
					$docmodel->view = $_POST['modelview'];
					$docmodel->creation_date = date('Y-m-d H:i:s');
					$inserted_id = $this->model->saveDocModel($docmodel);

					if (!is_array($inserted_id)) {
						$this->AddNotice('[$model_save_success]');

						if (!is_bool($inserted_id)) {
							$this->LogsManager->registerLog('DocModel', 'insert', 'Document Model inserted with id : '.$inserted_id, $inserted_id);
							Utils::RedirectTo($this->getActionUrl('model_edit')."/$inserted_id");
						} else {
							$this->LogsManager->registerLog('DocModel', 'update', 'Document Model updated with id : '.$id, $id);
						}
					} else {
						$this->AddError('Saving failed!');
					}
				}
			}
		}

		if ($docmodelfield === false) {
			$this->AddError('Model DocModelField not found');
		} elseif (isset($_POST['save'])) {
			if (!isset($_POST['name'][0])) {
				if ($_POST['name'] != '') {
					$docmodelfield->id_model = $id;
					$docmodelfield->name = $_POST['name'];
					$docmodelfield->type = $_POST['type'];
					$docmodelfield->required = $_POST['required'];

					$result = $this->model->saveDocModelField($docmodelfield);
				} else {
					$this->AddError('Input Name must not be empty!');
				}
			} else {
				for ($i = 0; $i < count($_POST['name']); $i++) {
					if ($_POST['name'][$i] != '') {
						$docmodelfield->id_model = $id;
						$docmodelfield->name = $_POST['name'][$i];
						$docmodelfield->type = $_POST['type'][$i];
						$docmodelfield->required = $_POST['required'][$i];

						$result = $this->model->saveDocModelField($docmodelfield);
					} else {
						$this->AddError('[$input_name_empty]');
					}
				}
			}

			if (!is_array($result)) {
				$this->AddNotice('[$model_save_success]');
			} else {
				$this->AddError('[$save_failed]');
			}

			Utils::RedirectTo($this->getActionUrl('model_edit')."/$id");
		}

		$this->view->set('docmodel', $docmodel);
		$this->view->set('modelFields', $modelFields);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('models_list')));
		$title = (!is_null($id)) ? '[$EditModel]' : '[$NewModel]';
		$this->view->setTitle($title);
		$this->view->render('model-edit');
	}

	public function model_delete($id)
	{
		$docmodel = $this->model->getDocModel($id);
		$modelview = $docmodel->view;

		$result = $this->model->deleteDocModel($id);
		if ($result) {
			$this->LogsManager->registerLog('DocModel', 'delete', "Document Model deleted with id : $id and name : ".$docmodel->name, $id);
			unlink(DOCROOT.DATA_DIR.DS.'docmodels'.DS.$modelview);
			$this->AddNotice('[$model_del_success]');
		} else {
			$this->AddError('[$delete_failed]');
		}
		Utils::RedirectTo($this->getActionUrl('models_list'));
	}

	public function modelField_delete($id)
	{
		$result = $this->model->deleteDocModelField($id);
		if ($result) {
			$this->AddNotice('[$field_del_success]');
		} else {
			$this->AddError('[$delete_failed]');
		}

		if (isset($_GET['id_model'])) {
			Utils::RedirectTo($this->getActionUrl('model_edit').'/'.$_GET['id_model']);
		} else {
			Utils::RedirectTo($this->getActionUrl('models_list'));
		}
	}
}
