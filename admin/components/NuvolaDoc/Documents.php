<?php

class Documents_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('NuvolaDoc/Doc_Models/document_edit/');

		$this->view->set('url_doc_delete', $this->getActionUrl('document_delete'));
		$this->view->set('url_doc_edit', $this->getActionUrl('document_edit'));
		$this->view->set('url_doc_files', $this->getActionUrl('file_list'));
		$this->view->set('url_file_show', substr(WEBROOT, 0, -6).MEDIA_ROOT.'documents');
		$this->view->set('url_file_download', $this->getActionUrl('download_file'));
		$this->view->set('url_file_delete', $this->getActionUrl('delete_docfile'));
		$this->view->set('url_doc_search', $this->getActionUrl('documents_list'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Admins/login'));
		}
	}

	public function documents_list()
	{
		HeadHTML::setTitleTag('Documents'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['del_selected']) && isset($_POST['elements'])) {
			foreach ($_POST['elements'] as $id) {
				$document = $this->model->getDocument($id);
				$files = $this->model->getDocFiles($id);
				foreach ($files as $fl) {
					unlink(DOCROOT.MEDIA_ROOT.'documents'.DS.$document->id_category.DS.$fl->filename);
				}

				$res = $this->model->deleteDocument($id);
			}
			Utils::RedirectTo($this->getActionUrl('documents_list'));
		}

		if (isset($_GET['id_model']) && isset($_POST['search'])) {
			$documents = $this->model->search($_GET['id_model'], $_POST);
		} elseif (isset($_GET['query'])) {
			$searchFields = array(
				array('field' => 'id', 'peso' => 100),
				array('field' => 'title', 'peso' => 90),
				array('field' => 'reference_number', 'peso' => 80)
			);
			$documents = $this->model->FastSearch($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$documents = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddDocument]', $this->getActionUrl('select_model')));
		$this->view->addButton(new Button('<i class="fa fa-search"></i>&nbsp;&nbsp;[$Search]', $this->getActionUrl('select_model').'?back_url=search_document', '', '', '[$Search]'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('documents', $documents);
		$this->view->setTitle('[$Documents]');
		$this->view->render('documents-list');
	}

	public function select_model()
	{
		HeadHTML::setTitleTag('Select Model'.' | '.CMS_Settings::$website_title);
		if (isset($_GET['back_url'])) {
			$back_url = rtrim($this->getActionUrl($_GET['back_url']), '/').'/';
		} else {
			$back_url = $this->getActionUrl('add_document').'/';
		}

		if (isset($_POST['next'])) {
			Utils::RedirectTo($back_url.$_POST['id_model']);
		}

		$dm_model = Loader::getModel('Doc_Models', 'NuvolaDoc');
		$docmodels = $dm_model->getList();

		$this->view->set('docmodels', $docmodels);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('documents_list')));
		$this->view->setTitle('[$SelectModel]');
		$this->view->render('select-model');
	}

	public function add_document($id_model = null)
	{
		if (is_null($id_model)) {
			$this->AddError('[$select_model]');
			Utils::RedirectTo($this->getActionUrl('documents_list'));
		}

		HeadHTML::setTitleTag('Add Document'.' | '.CMS_Settings::$website_title);

		$dm_model = Loader::getModel('Doc_Models', 'NuvolaDoc');
		$docmodel = $dm_model->getDocModel($id_model);
		$docmodel_fields = $dm_model->getDocModelFields($id_model);

		if (isset($_POST['save'])) {
			//Entitetet
			$document = new Document_Entity();
			$docindex = new DocIndex_Entity();
			$docfile = new DocFile_Entity();

			$loggeduser = UserAuth::getLoginSession();

			if ($_FILES['file']['tmp_name'][0] == '') {
				$this->AddError('[$upload_file]');
			}

			($_POST['id_category'] == '') ? $this->AddError('Category is required!') : '';

			foreach ($docmodel_fields as $field) {
				if ($field->required && $_POST['f_'.Utils::clean($field->name)] == '') {
					$this->AddError($field->name.' is required!');
				}
			}

			if (count($this->view->getErrors()) == 0) {

				//Madhesia e fileve qe po uploadon
				$totalUploadedSize = 0;
				for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
					$totalUploadedSize += $_FILES['file']['size'][$i];
				}

				if (!$this->checkQuotaLimit($totalUploadedSize)) {
					$this->AddError('You have reached the maximum upload limit! <br/> You can not upload more files!');
					Utils::RedirectTo($this->getActionUrl('documents_list'));
					return;
				};

				//Document Save
				$document->id_user = $loggeduser['id'];
				$document->title = $_POST['title'];
				$document->reference_number = $_POST['reference_number'];
				$document->location = $_POST['location'];
				$document->creation_date = date('Y-m-d H:i:s');
				$document->id_model = $id_model;
				$document->id_category = $_POST['id_category'];

				$inserted_id = $this->model->saveDocument($document);

				if (!is_array($inserted_id)) {
					$this->AddNotice('[$document_add_success]');
					$this->LogsManager->registerLog('Document', 'insert', 'Document inserted with id : '.$inserted_id, $inserted_id);
				} else {
					$this->AddError('[$addition_failed]');
				}

				//File Uploads
				if (!is_array($inserted_id)) {

					//Indexes save
					foreach ($docmodel_fields as $field) {
						$docindex->id_doc = $inserted_id;
						$docindex->id_field = $field->id;
						$docindex->value = $_POST['f_'.Utils::clean($field->name)];
						$result = $this->model->saveDocIndex($docindex);
					}

					$failures = 0;
					for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
						$tmp_name = time().'_'.$_FILES['file']['name'][$i];

						$docfile->id_doc = $inserted_id;
						$docfile->filename = $tmp_name;
						$docfile->size = $_FILES['file']['size'][$i];
						$res = $this->model->saveDocFile($docfile);

						if (!is_array($res)) {
							$target = DOCROOT.MEDIA_ROOT.'documents'.DS.$document->id_category.DS.$tmp_name;
							move_uploaded_file($_FILES['file']['tmp_name'][$i], $target);
						} else {
							$failures++;
						}

						if ($failures > 0) {
							$this->AddError('There was '.$failures.' failures during file upload!');
						}
					}

					Utils::RedirectTo($this->getActionUrl('add_tags')."/$inserted_id");
				}
			}
		}

		if ($docmodel->view != '') {
			ob_start();
			include DOCROOT.DATA_DIR.'docmodels'.DS.$docmodel->view;
			$model_content = ob_get_contents();
			ob_clean();
			$this->view->set('model_content', $model_content);
		}

		$categ_model = Loader::getModel('Doc_Categories', 'NuvolaDoc');
		$doccategories = $categ_model->getList();

		$this->view->set('docmodel', $docmodel);
		$this->view->set('docmodel_fields', $docmodel_fields);
		$this->view->set('doccategories', $doccategories);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('select_model')));
		$this->view->setTitle('[$AddDocument]');
		$this->view->render('add-document');
	}

	public function add_tags($id_doc = null)
	{
		if (is_null($id_doc)) {
			$this->AddError('[$document_first]');
			Utils::RedirectTo($this->getActionUrl('documents_list'));
		}

		HeadHTML::setTitleTag('Add Tags'.' | '.CMS_Settings::$website_title);
		$doctag = new DocTag_Entity();

		if (isset($_POST['add_tag'])) {
			($_POST['tag'] == '') ? $this->AddError('[$tag_not_empty]') : '';

			if (count($this->view->getErrors()) == 0) {
				$doctag->id_doc = $id_doc;
				$doctag->tag = $_POST['tag'];

				$res = $this->model->saveDocTag($doctag);
				if (!is_array($res)) {
					$this->AddNotice('[$tag_add_success]');
				} else {
					$this->AddError('[$addition_failed]');
				}
				Utils::RedirectTo($this->getActionUrl('add_tags')."/$id_doc");
			}
		} elseif (isset($_POST['finish'])) {
			Utils::RedirectTo($this->getActionUrl('documents_list'));
			return;
		}

		$doctags = $this->model->getDocTags($id_doc);

		$document = $this->model->getDocument($id_doc);

		$this->view->set('document', $document);
		$this->view->set('doctags', $doctags);
		$this->view->setTitle('Add Tags');
		$this->view->render('add-tags');
	}

	public function document_edit($id = null)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('documents_list'));
		}

		$document = $this->model->getDocument($id);
		HeadHTML::setTitleTag('Edit Document - '.$document->title.' | '.CMS_Settings::$website_title);

		$doctag = new DocTag_Entity();
		if (isset($_POST['add_tag'])) {
			($_POST['tag'] == '') ? $this->AddError('[$tag_not_empty]') : '';

			if (count($this->view->getErrors()) == 0) {
				$doctag->id_doc = $id;
				$doctag->tag = $_POST['tag'];

				$res = $this->model->saveDocTag($doctag);
				if (!is_array($res)) {
					$this->AddNotice('[$tag_add_success]');
				} else {
					$this->AddError('[$addition_failed]');
				}
				Utils::RedirectTo($this->getActionUrl('document_edit')."/$id#addtag");
			}
		}

		$dm_model = Loader::getModel('Doc_Models', 'NuvolaDoc');
		$docmodel = $dm_model->getDocModel($document->id_model);
		$docmodel_fields = $dm_model->getDocModelFields($document->id_model);

		if (isset($_POST['save'])) {
			$docfile = new DocFile_Entity();

			$loggeduser = UserAuth::getLoginSession();

			foreach ($docmodel_fields as $field) {
				if ($field->required && $_POST['f_'.Utils::clean($field->name)] == '') {
					$this->AddError($field->name.' is required!');
				}
			}

			if (count($this->view->getErrors()) == 0) {
				if ($document->id_category != $_POST['id_category']) {
					$old_category = $document->id_category;
				} else {
					$old_category = 0;
				}

				//Document Save
				$document->title = $_POST['title'];
				$document->reference_number = $_POST['reference_number'];
				$document->location = $_POST['location'];
				$document->id_category = $_POST['id_category'];

				$result = $this->model->saveDocument($document);

				if (!is_array($result)) {
					$this->LogsManager->registerLog('Document', 'update', 'Document updated with id : '.$id, $id);
					$this->AddNotice('[$document_edit_success]');
				} else {
					$this->AddError('[$edition_failed]');
				}

				//File Upload Nqs. ekzistion
				if (!is_array($result)) {

					//Zhvendosim te gjitha filet nga folderi i kategorise se vjeter
					if ($old_category != 0) {
						$files = $this->model->getDocFiles($document->id);
						foreach ($files as $fil) {
							$old_name = DOCROOT.MEDIA_ROOT.'documents'.DS.$old_category.DS.$fil->filename;
							$new_name = DOCROOT.MEDIA_ROOT.'documents'.DS.$document->id_category.DS.$fil->filename;
							rename($old_name, $new_name);
						}
					}

					//Indexes save
					foreach ($docmodel_fields as $field) {
						$docindex = $this->model->getDocIndexByDoc($id, $field->id);

						$docindex->value = $_POST['f_'.Utils::clean($field->name)];
						$result = $this->model->saveDocIndex($docindex);
					}

					if ($_FILES['file']['tmp_name'][0] != '') {

						//Madhesia e fileve qe po uploadon
						$totalUploadedSize = 0;
						for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
							$totalUploadedSize += $_FILES['file']['size'][$i];
						}

						if (!$this->checkQuotaLimit($totalUploadedSize)) {
							$this->AddError('You have reached the maximum upload limit! <br/> You can not upload more files!');
							Utils::RedirectTo($this->getActionUrl('documents_list'));
							return;
						};

						$failures = 0;
						for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
							$tmp_name = time().'_'.$_FILES['file']['name'][$i];

							$docfile->id_doc = $id;
							$docfile->filename = $tmp_name;
							$docfile->size = $_FILES['file']['size'][$i];
							$res = $this->model->saveDocFile($docfile);

							if (!is_array($res)) {
								$target = DOCROOT.MEDIA_ROOT.'documents'.DS.$document->id_category.DS.$tmp_name;
								move_uploaded_file($_FILES['file']['tmp_name'][$i], $target);
							} else {
								$failures++;
							}

							if ($failures > 0) {
								$this->AddError('There was '.$failures.' failures during file upload!');
							}
						}
					}

					Utils::RedirectTo($this->getActionUrl('document_edit')."/$id");
				}
			}
		}

		$doctags = $this->model->getDocTags($id);

		$index = array();
		foreach ($docmodel_fields as $field) {
			$ind = $this->model->getDocIndexByDoc($id, $field->id);

			$index['f_'.Utils::clean($field->name)] = $ind->value;
		}

		if ($docmodel->view != '') {
			ob_start();
			include DOCROOT.DATA_DIR.'docmodels'.DS.$docmodel->view;
			$model_content = ob_get_contents();
			ob_clean();
			$this->view->set('model_content', $model_content);
		}

		$categ_model = Loader::getModel('Doc_Categories', 'NuvolaDoc');
		$doccategories = $categ_model->getList();

		$this->view->set('doctags', $doctags);
		$this->view->set('docmodel', $docmodel);
		$this->view->set('docmodel_fields', $docmodel_fields);
		$this->view->set('doccategories', $doccategories);
		$this->view->set('document', $document);
		$this->view->set('index', $index);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('documents_list')));
		$this->view->setTitle('[$EditDocument]');
		$this->view->render('edit-document');
	}

	public function document_delete($id)
	{
		$document = $this->model->getDocument($id);
		$result = $this->model->deleteDocument($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Document', 'delete', 'Document deleted with id : '.$id.' and title : '.$document->title, $id);
			$this->AddNotice('[$doc_delete_success]');
		} else {
			$this->AddError('[$delete_failed]');
		}

		Utils::RedirectTo($this->getActionUrl('documents_list'));
	}

	public function search_document($id_model)
	{
		if (is_null($id_model)) {
			$this->AddError('[$select_model]');
			Utils::RedirectTo($this->getActionUrl('documents_list'));
		}

		HeadHTML::setTitleTag('Search Document'.' | '.CMS_Settings::$website_title);

		$dm_model = Loader::getModel('Doc_Models', 'NuvolaDoc');
		$docmodel = $dm_model->getDocModel($id_model);
		$docmodel_fields = $dm_model->getDocModelFields($id_model);

		if (isset($_POST['search'])) {
			$this->model->search($id_model, $_POST);
		}

		$categ_model = Loader::getModel('Doc_Categories', 'NuvolaDoc');
		$doccategories = $categ_model->getList();

		if ($docmodel->view != '') {
			ob_start();
			include DOCROOT.DATA_DIR.'docmodels'.DS.$docmodel->view;
			$model_content = ob_get_contents();
			ob_clean();
			$this->view->set('model_content', $model_content);
		}

		$this->view->set('docmodel', $docmodel);
		$this->view->set('docmodel_fields', $docmodel_fields);
		$this->view->set('doccategories', $doccategories);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('select_model').'?back_url=search_document'));
		$this->view->setTitle('[$SearchDocuments]');
		$this->view->render('search-document');
	}

	public function file_list($id_doc = null)
	{
		if (is_null($id_doc)) {
			Utils::RedirectTo($this->getActionUrl('documents_list'));
		}

		$document = $this->model->getDocument($id_doc);
		$this->view->set('document', $document);
		HeadHTML::setTitleTag('Files of document - '.$document->title.' | '.CMS_Settings::$website_title);

		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = "id_doc = $id_doc";

		$files = $this->model->getFileList($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('documents_list')));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('files', $files);
		$this->view->setTitle('[$Files]');
		$this->view->render('files-list');
	}

	public function download_file($id)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('documents_list'));
		}

		$file = $this->model->getDocFile($id);
		$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'documents'.DS.$file->id_category.DS.$file->filename;

		//Shkarkimi i Files
		header('Content-Type: '.mime_content_type(DOCROOT.$filename));
		header('Content-Disposition: attachment; filename="'.$file->filename.'"');
		readfile(DOCROOT.$filename);
	}

	public function delete_docfile($id)
	{
		$result = $this->model->deleteDocFile($id);
		if ($result !== false) {
			$this->AddNotice('[$file_delete_success]');
		} else {
			$this->AddError('[$delete_failed]');
		}

		Utils::RedirectTo($this->getActionUrl('file_list').'/'.$_GET['id_doc']);
	}

	private function checkQuotaLimit($filesSizeToUpload)
	{

		//Quota limits
		$client_info = UserAuth::getLoggedClientInfo();
		$currentFilesSize = $this->model->filesTotalSize();

		if (($currentFilesSize + $filesSizeToUpload) >= $client_info['quota_limit']) {
			$this->LogsManager->registerLog('Document', 'insert', 'Maximum limit reached for space quota.');
			return false;
		}
		return true;
	}
}
