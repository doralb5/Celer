<hr>
<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">[$AddTags]</header>
            <div class="panel-body">
                <form role="form" method="post">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>[$DocumentTitle]</label>
                            <input type="text" value="<?= $document->title ?>"class="form-control" disabled/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>[$ReferenceNumber]</label>
                            <input type="text" value="<?= $document->reference_number ?>"class="form-control" disabled/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>[$DocumentTags]</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="tag"/>
                            <div class="input-group-btn">
                                <button type="submit" name="add_tag" class="btn btn-info"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$Add]</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <?php foreach ($doctags as $tag) {
	?>
                            <span class="label label-primary"><?= $tag->tag ?></span>
                        <?php
} ?>
                    </div>
                    <p>&nbsp;</p>
                    <div class="form-group">
                        <button type="submit" name="finish" class="btn btn-lg btn-success pull-right">[$Finish]&nbsp;&nbsp;<i class="fa fa-sign-in"></i></button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>