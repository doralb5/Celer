<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Doc_Categories');

foreach ($categories as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_category_edit.'/'.$row->id, 'xs', '', '[$Edit]')
		);
}
$table->setElements($categories);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('parent_category', '[$Parent]', 'int', 'left'),
	new TableList_Field('total_docs', '[$NrDocs]', 'int', 'left'),
));
$table->setUrl_action($url_category_search);
$table->setUrl_delete($url_category_delete);

$table->render();
