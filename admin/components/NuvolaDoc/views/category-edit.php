<hr>
<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading"><?= ($doccategory->name != '') ? '[$EditCategory]' : '[$NewCategory]' ?> </header>
            <div class="panel-body">
                <form role="form" method="post">
                    <div class="form-group">
                        <label>[$ParentCategory]<span class="required">*</span></label>
                        <select name='parent_id' class="form-control">
                            <option value="0">--[$NoParent]--</option>
                            <?php foreach ($doccategories as $categ) {
	?>
                                <option value="<?= $categ->id ?>" <?= ($doccategory->parent_id == $categ->id) ? 'selected' : '' ?>><?= $categ->name ?></option>
                            <?php
} ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>[$CategoryName]<span class="required">*</span></label>
                        <input type="text" class="form-control" name="name" placeholder="" required="" value="<?= $doccategory->name ?>">
                    </div>

                    <div class="form-group">
                        <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>