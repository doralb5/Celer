<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Documents');

foreach ($documents as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil-square-o"></i>', $url_doc_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		new Button('<i class="fa fa-files-o"></i>', $url_doc_files.'/'.$row->id, 'xs', '', '[$Files]')
	);
}

$table->setElements($documents);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('reference_number', '[$ReferenceNumber]', 'string', 'left'),
	new TableList_Field('title', '[$Title]', 'string', 'left'),
	new TableList_Field('creation_date', '[$Date]', 'int', 'left'),
	new TableList_Field('category_name', '[$Category]', 'string', 'left'),
	new TableList_Field('model_name', '[$Model]', 'string', 'left')
));
$table->setUrl_delete($url_doc_delete);
$table->setUrl_action($url_doc_search);
$table->renderTopBar(true);

$table->render();
