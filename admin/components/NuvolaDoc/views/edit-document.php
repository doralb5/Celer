<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/normalize.css');
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/demo.css');
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php
$reference_number = isset($_POST['reference_number']) ? $_POST['reference_number'] : $document->reference_number;
$document_title = isset($_POST['title']) ? $_POST['title'] : $document->title;
$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $document->id_category;
$location = isset($_POST['location']) ? $_POST['location'] : $document->location;
?>
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>(function (e, t, n) {
        var r = e.querySelectorAll("html")[0];
        r.className = r.className.replace(/(^|\s)no-js(\s|$)/, "$1js$2")
    })(document, window, 0);</script>
<hr>
<div class="row">

    <form role="form" method="post" enctype="multipart/form-data">

        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">[$GeneralData]</header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>[$Model]</label>
                                <input disabled="" type="text" class="form-control" value="<?= $document->model_name ?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>[$ReferenceNumber]</label>
                                <input name="reference_number" type="text" class="form-control" required="" value="<?= $reference_number ?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>[$DocumentTitle]</label>
                                <input name="title" type="text" class="form-control" required="" value="<?= $document_title ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>[$Category]</label>
                                <select name='id_category' class="form-control">
                                    <?php foreach ($doccategories as $dc) {
	?>
                                        <option value="<?= $dc->id ?>" <?= ($id_category == $dc->id) ? 'selected' : '' ?>><?= $dc->name ?></option>
                                    <?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>[$Location]</label>
                                <input name="location" type="text" class="form-control" value="<?=$location?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>[$SelectFiles]</label>
                            <div class="box">
                                <input type="file" name="file[]" id="file-7" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple/>
                                <label for="file-7"><span></span><strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> [$Choosefiles]&hellip;</strong></label>
                            </div>
                        </div>
                    </div>

                </div>
                </header>
            </section>
        </div>
        <hr>

        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">[$Details]</header>
                <div class="panel-body">

                    <?php
					if ($docmodel->view != '') {
						echo $model_content;
					} else {
						?>

                        <div class="row">
                            <div class="col-md-6">
                                <?php foreach ($docmodel_fields as $field) {
							?>
                                    <div class="form-group">
                                        <label><?= $field->name ?></label>


                                        <!--TextBox-->
                                        <?php if ($field->type == 'text') {
								?>
                                        <input type="text" class="form-control" name="f_<?= Utils::clean($field->name) ?>" <?= ($field->required) ? 'required' : '' ?> value="<?=$index['f_'.Utils::clean($field->name)]?>">
                                        <?php
							} ?>
                                        <!-------------->

                                        <!--Number-->
                                        <?php if ($field->type == 'number') {
								?>
                                            <input type="number" class="form-control" name="f_<?= Utils::clean($field->name) ?>" <?= ($field->required) ? 'required' : '' ?> value="<?=$index['f_'.Utils::clean($field->name)]?>">
                                        <?php
							} ?>
                                        <!-------------->

                                        <!--TextArea-->
                                        <?php if ($field->type == 'textarea') {
								?>
                                            <textarea style="max-width: 100%" rows="4" class="form-control" name="f_<?= Utils::clean($field->name) ?>"   <?= ($field->required) ? 'required' : '' ?>><?=$index['f_'.Utils::clean($field->name)]?></textarea>
                                        <?php
							} ?>
                                        <!-------------->
                                    </div>
                                <?php
						} ?>
                            </div>
                        </div>
                    <?php
					} ?>


                    <div class="form-group">
                        <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
                    </div>
                    </form>
                </div>
            </section>
        </div>

        <div class="col-md-6" id="addtag">
            <section class="panel">
                <header class="panel-heading">[$AddTags]</header>
                <div class="panel-body">
                    <form role="form" method="post">
                        <div class="form-group">
                            <label>[$DocumentTags]</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="tag"/>
                                <div class="input-group-btn">
                                    <buton type="submit" name="add_tag" class="btn btn-info"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$Add]</button>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php foreach ($doctags as $tag) {
						?>
                                <span class="label label-primary"><?= $tag->tag ?></span>
                            <?php
					} ?>
                        </div>
                        <p>&nbsp;</p>
                    </form>
                </div>
            </section>
        </div>
        <hr>
        </div>
        <style>
            .inputfile {
                width: 0.1px;
                height: 0.1px;
                opacity: 0;
                overflow: hidden;
                position: absolute;
                z-index: -1;
            }



            .inputfile + label {
                cursor: pointer; /* "hand" cursor */
            }

            .inputfile:focus + label {
                outline: 1px dotted #000;
                outline: -webkit-focus-ring-color auto 5px;
            }
            .box {
                padding: 0px;
            }
        </style>
        <script>
            var inputs = document.querySelectorAll('.inputfile');
            Array.prototype.forEach.call(inputs, function (input)
            {
                var label = input.nextElementSibling,
                        labelVal = label.innerHTML;

                input.addEventListener('change', function (e)
                {
                    var fileName = '';
                    if (this.files && this.files.length > 1)
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                    else
                        fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        label.querySelector('span').innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });
            });</script>