<div class="panel panel-default">
  <div class="panel-body">
      <div class="row">
          <div class="col-md-6"><h4>[$DocumentTitle] : <?=$document->title?></h4></div>
          <div class="col-md-6"><h4>[$ReferenceNumber] : <?=$document->reference_number?></h4></div>
      </div>
  </div>
</div>

<?php
include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Documents');

foreach ($files as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-eye"></i>', $url_file_show.'/'.$row->id_category.'/'.$row->filename, 'xs', '', '[$Show]', '_blank'),
		new Button('<i class="fa fa-download"></i>', $url_file_download.'/'.$row->id, 'xs', '', '[$Download]')
	);
}

$table->setElements($files);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('filename', '[$Filename]', 'string', 'left'),
	new TableList_Field('document_title', '[$Document]', 'string', 'left'),
	new TableList_Field('size', '[$Size]', 'byte', 'left')
));

$table->setUrl_delete($url_file_delete);
$table->setUrl_delete_params(array('id_doc' => $document->id));
$table->renderTopBar(false);
$table->multipleDeletion(false);
$table->render();
