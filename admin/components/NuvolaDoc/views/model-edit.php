<hr>
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">[$MainData]</header>
            <div class="panel-body">

                <form method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="form-group col-md-5">
                            <label>[$ModelName]<span class="required">*</span></label>
                            <input type="text" class="form-control" name="modelname"  value="<?= $docmodel->name ?>" placeholder="[$ModelName]" required="">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="modelView">[$LayoutFile]</label>
                            <input name="modelview" type="file" class="form-control-file" id="modelView">
                        </div>
                        <div class="form-group col-md-2">
                            <label>&nbsp;</label>
                            <button type="submit" name="change" class="form-control btn btn-info"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">[$ModelItems]</header>
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-12">
                        <?php
						include(VIEWS_PATH.'TableList_class.php');

						$table = new TableListView('Doc_Models');

						$table->setElements($modelFields);
						$table->setFields(array(
							new TableList_Field('id', '[$Id]', 'int', 'left'),
							new TableList_Field('name', '[$Name]', 'string', 'left'),
							new TableList_Field('type', '[$Type]', 'string', 'left'),
							new TableList_Field('required', '[$Required]', 'bool', 'left'),
						));
						$table->setUrl_delete($url_modelField_delete);
						$table->setUrl_delete_params(array('id_model' => $docmodel->id));
						$table->renderTopBar(false);
						$table->multipleDeletion(false);

						$table->render();
						?>
                    </div>
                </div>


                <form role="form" method="post">
                    <div class="row">
                        <div class="clearfix"></div>
                        <div class="col-md-10" id="div-append">
                            <div class="row" id="content-append">
                                <div class="form-group col-sm-4">
                                    <label>[$InputName]<span class="required">*</span></label>
                                    <input type="text" class="form-control" name="name[]" placeholder="" required="">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>[$InputType]<span class="required">*</span></label>
                                    <select name='type[]' class="form-control">
                                        <option value="text">[$Text]</option>
                                        <option value="number">[$Number]</option>
                                        <option value="textarea">[$Textarea]</option>
                                        <!--                                    <option value="select">Selection List</option>
                                                                                <option value="radio">Radio Button</option>
                                                                                <option value="check">CheckBox</option>-->
                                    </select>
                                </div>

                                <div class="form-group col-sm-2">
                                    <label>[$Required]</label>
                                    <select name='required[]' class="form-control">
                                        <option value="0">[$no_option]</option>
                                        <option value="1">[$yes_option]</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <p>&nbsp;</p>
                            <button id="add-btn" type="button" name="add" class="btn btn-info pull-right"/><i class="fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" name="save" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>

<script>

    $(document).ready(function () {

        $('#add-btn').click(function (event) {
            event.preventDefault();
            var content = $('#content-append').html();

            console.log(content);
            jQuery('#div-append').append('<div class="row">' + content + '</div>');
        });

    });


</script>