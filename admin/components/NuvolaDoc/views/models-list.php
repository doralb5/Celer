<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Doc_Models');

foreach ($models as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_model_edit.'/'.$row->id, 'xs', '', '[$Edit]')
		);
}
$table->setElements($models);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('creation_date', '[$CreationDate]', 'date', 'left'),
));
$table->setUrl_action($url_model_search);
$table->setUrl_delete($url_model_delete);

$table->render();
