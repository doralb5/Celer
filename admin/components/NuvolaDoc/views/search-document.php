<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/normalize.css');
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/demo.css');
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>(function (e, t, n) {
        var r = e.querySelectorAll("html")[0];
        r.className = r.className.replace(/(^|\s)no-js(\s|$)/, "$1js$2")
    })(document, window, 0);</script>
<hr>
<div class="row">

    <form role="form" method="post" action="<?= Utils::getComponentUrl('NuvolaDoc/Documents/documents_list?id_model='.$docmodel->id); ?>">

        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">[$SearchDocuments]</header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>[$Model]</label>
                                <input disabled="" type="text" class="form-control" value="<?= $docmodel->name ?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>[$ReferenceNumber]</label>
                                <input name="reference_number" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>[$DocumentTitle]</label>
                                <input name="title" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>[$Category]</label>
                                <select name='id_category' class="form-control">
                                    <option value="">[$All]</option>
                                    <?php foreach ($doccategories as $dc) {
	?>
                                        <option value="<?= $dc->id ?>"><?= $dc->name ?></option>
                                    <?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>[$Location]</label>
                                <input name="location" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>[$Tags]</label>
                                <textarea rows="1" class="form-control" style="max-width: 100%" name="tags" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                </header>
            </section>
        </div>
        <hr>

        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">[$SearchDocuments]</header>
                <div class="panel-body">

                    <?php
					if ($docmodel->view != '') {
						echo $model_content;
					} else {
						?>

                        <div class="row">
                            <div class="col-md-6">
                                <?php foreach ($docmodel_fields as $field) {
							?>
                                    <div class="form-group">
                                        <label><?= $field->name ?></label>


                                        <!--TextBox-->
                                        <?php if ($field->type == 'text') {
								?>
                                            <input type="text" class="form-control" name="f_<?= Utils::clean($field->name) ?>" <?= ($field->required) ? 'required' : '' ?>>
                                        <?php
							} ?>
                                        <!-------------->

                                        <!--Number-->
                                        <?php if ($field->type == 'number') {
								?>
                                            <input type="number" class="form-control" name="f_<?= Utils::clean($field->name) ?>" <?= ($field->required) ? 'required' : '' ?>>
                                        <?php
							} ?>
                                        <!-------------->

                                        <!--TextArea-->
                                        <?php if ($field->type == 'textarea') {
								?>
                                            <textarea style="max-width: 100%" rows="4" class="form-control" name="f_<?= Utils::clean($field->name) ?>"   <?= ($field->required) ? 'required' : '' ?>></textarea>
                                        <?php
							} ?>
                                        <!-------------->
                                    </div>
                                <?php
						} ?>
                            </div>
                        </div>
                    <?php
					} ?>


                    <div class="form-group">
                        <button type="submit" name="search" class="btn btn-info pull-right"><i class="fa fa-search"></i>&nbsp;&nbsp;[$Search]</button>
                    </div>
                    </form>
                </div>
            </section>
        </div>
</div>