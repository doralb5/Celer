<hr>
<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">[$SelectModel]</header>
            <div class="panel-body">
                <form role="form" method="post">
                    <div class="form-group">
                        <label>[$SelectModel]<span class="required">*</span></label>
                        <select name='id_model' class="form-control">
                            <?php foreach ($docmodels as $dm) {
	?>
                                <option value="<?= $dm->id ?>"><?= $dm->name ?></option>
                            <?php
} ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="next" class="btn btn-info pull-right">[$Next]&nbsp;&nbsp;<i class="fa fa-arrow-circle-right"></i></button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>