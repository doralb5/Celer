<?php

class Pages_Component extends BaseComponent
{
	public function __construct()
	{
		parent::__construct();
		$this->view->set('url_pages_list', Utils::getComponentUrl('Pages/pages_list'));
		$this->view->set('url_page_edit', Utils::getComponentUrl('Pages/page_edit'));
		$this->view->set('url_page_delete', Utils::getComponentUrl('Pages/delete_page'));
	}

	public function pages_list()
	{
		Utils::saveCurrentPageUrl();
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

		if (isset($_POST['delete_selected']) && isset($_POST['elements'])) {
			foreach ($_POST['elements'] as $id) {
				$res = $this->model->deletePage($id);
			}

			if ($res) {
				$this->view->AddNotice('Pages has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}

		$elements_per_page = 15;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = 'name';
		$filter = "lang = '".CMSSettings::$default_lang."'";

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'Page.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'Page.name', 'peso' => 90),
				array('field' => TABLE_PREFIX.'PageContent.alias', 'peso' => 80),
				array('field' => TABLE_PREFIX.'PageContent.meta_title', 'peso' => 70)
			);
			$pages = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$pages = $this->model->getList($elements_per_page, ($page - 1) * $elements_per_page, $filter, $sorting);
		}

		$totalElements = $this->model->getLastCounter();

		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);

		$this->view->set('pages', $pages);

		$this->view->setTitle('[$Pages]');
		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddPage]', $this->getActionUrl('page_edit')));
		$this->view->render('pages-list');
	}

	public function page_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		if (!is_null($id)) {
			$page = $this->model->getPage($id);
			HeadHTML::setTitleTag('Edit Page - '.$page->alias.' | '.CMSSettings::$website_title);
			$this->view->setTitle('Edit Page');

			$controllerParameters = $this->model->getParametersByXML($page->component_name, $page->action);
			$this->view->set('controllerParameters', $controllerParameters);
		} else {
			//Kontrollojme limitin e faqeve
			if ($this->model->checkPageLimit()) {
				$this->view->AddError('The page limit was passed. You can not create more pages!');
				Utils::RedirectTo($this->getActionUrl('pages_list'));
				return;
			}

			$page = new Page_Entity();
			$page->creation_date = date('Y-m-d H:i');
			HeadHTML::setTitleTag('New Page - '.' | '.CMSSettings::$website_title);
			$this->view->setTitle('New Page');
		}

		if ($page === false) {
			$this->AddError("Page $id not found");
		} elseif (isset($_POST['save'])) {
			($this->model->checkCompActionAvailability($id, $_POST)) ? $this->view->AddError('This component/action/parameter already exist!') : '';

			if (count($this->view->getErrors()) == 0) {
				$robots = '';
				if (isset($_POST['robots_index'])) {
					$robots = $_POST['robots_index'].',';
				}
				if (isset($_POST['robots_follow'])) {
					$robots .= "{$_POST['robots_follow']},";
				}
				$robots = rtrim($robots, ',');

				$this->model->startTransaction();

				$page->id_component = $_POST['id_component'];
				$page->action = isset($_POST['action']) ? $_POST['action'] : '';
				$page->action_params = ($_POST['action_params'] != '') ? $_POST['action_params'] : null;
				$page->name = $_POST['name'];
				$page->layout = $_POST['layout'];
				$page->home = (isset($_POST['home']) ? 1 : 0);
				$page->enabled = (isset($_POST['enabled']) ? 1 : 0);
				$page->robots = $robots;
				$page->update_date = date('Y-m-d H:i');
				$page->show_content_heading = (isset($_POST['show_content_heading']) ? 1 : 0);

				$inserted_id = $this->model->savePage($page);

				if (!is_array($inserted_id)) {
					if (isset($_POST['home'])) {
						$this->model->setHomePage($page->id);
					}

					$def_alias = ($_POST['alias_'.CMSSettings::$default_lang] != '') ? $_POST['alias_'.CMSSettings::$default_lang] : Utils::url_slug($page->name);

					foreach (CMSSettings::$available_langs as $lang) {
						if (is_null($id)) {
							$pContent = new PageContent_Entity();
							$pContent->id_page = $inserted_id;
						} else {
							$pcon = $this->model->getPageContents(1, 0, "id_page = {$id} AND lang = '{$lang}'");
							if (count($pcon)) {
								$pContent = $pcon[0];
							} else {
								$pContent = new PageContent_Entity();
							}
							$pContent->id_page = $id;
						}

						$pContent->lang = $lang;

						// TODO: Change var name to "ContentHeading"
						$pContent->title = ($_POST['title_'.$lang] != '') ? $_POST['title_'.$lang] : '';

						$pContent->alias = ($_POST['alias_'.$lang] != '') ? $_POST['alias_'.$lang] : $def_alias;
						$pContent->meta_title = ($_POST['meta_title_'.$lang] != '') ? $_POST['meta_title_'.$lang] : '';
						$pContent->meta_description = ($_POST['meta_description_'.$lang] != '') ? $_POST['meta_description_'.$lang] : '';
						$pContent->meta_keywords = ($_POST['meta_keywords_'.$lang] != '') ? $_POST['meta_keywords_'.$lang] : '';
						$pContent->content = ($_POST['content_'.$lang] != '') ? $_POST['content_'.$lang] : '';

						$result = $this->model->savePageContent($pContent);

						if (is_array($result)) {
							$this->AddError('Problem creating contents of the page!');
							$this->model->rollback();
							if (!is_bool($result)) {
								Utils::RedirectTo($this->getActionUrl('page_edit')."/$inserted_id");
							} else {
								Utils::RedirectTo($this->getActionUrl('page_edit')."/$id");
							}
						}
					}

					$this->view->AddNotice('Page has been saved successfully.');
					$this->model->commit();

					if (!is_bool($inserted_id)) {
						Utils::RedirectTo($this->getActionUrl('page_edit')."/$inserted_id");
						$this->LogsManager->registerLog('Page', 'insert', 'Page inserted with id : '.$inserted_id, $inserted_id);
					} else {
						Utils::RedirectTo($this->getActionUrl('page_edit')."/$id");
						$this->LogsManager->registerLog('Page', 'insert', 'Page inserted with id : '.$id, $id);
					}
				} else {
					$this->view->AddError('Something went wrong!');
					$this->model->rollback();
				}
			}
		} elseif (isset($_POST['save_parameters'])) {
			unset($_POST['save_parameters']);
			foreach ($_POST as $key => $value) {
				if ($value == '') {
					unset($_POST[$key]);
				}
			}
			$json_data = (count($_POST)) ? json_encode($_POST) : '';

			$page->parameters = $json_data;
			$result = $this->model->savePage($page);
			if (!is_array($result)) {
				$this->view->AddNotice('Page parameters has been saved successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}

		$temp_md = Loader::getModel('Templates');
		$template = $temp_md->getList(1, 0, "enabled = '1'");
		$layouts_md = Loader::getModel('Layouts');
		$layouts = array();
		$template_css = array();
		$positions = array();
		$blocks = array();
		if (count($template)) {
			$template_name = $template[0]->name;
			$layouts = $layouts_md->getLayoutsListByXml($template_name);
			$templcss = $temp_md->getStylesheetsByXML($template_name);
			foreach ($templcss as $css) {
				$filecss = Utils::genPath($css['file']);
				if ($filecss != null) {
					$template_css[] = $filecss;
				}
			}
			$positions = $layouts_md->getPositionsListByXml($template_name, $page->layout);
			$blocks_md = Loader::getModel('Blocks');
			foreach ($positions as $pos) {
				$blocks[$pos['name']] = $blocks_md->getList(30, 0, 'position = :posName AND id_page = :idPage', $order = '', array(':posName' => $pos['name'], ':idPage' => $page->id));
			}
		}
		$this->view->set('layouts', $layouts);
		$this->view->set('template_css', $template_css);
		$this->view->set('layout_positions', $positions);
		$this->view->set('position_blocks', $blocks);

		$comp_md = Loader::getModel('Components');
		$components = $comp_md->getList(30, 0, "enabled = '1' AND type = '0'");
		$this->view->set('components', $components);

		foreach (CMSSettings::$available_langs as $lang) {
			$pContent = $this->model->getPageContents(1, 0, "id_page = {$id} AND lang = '{$lang}'");
			$page->{$lang} = count($pContent) ? $pContent[0] : null;
		}
		$this->view->set('page', $page);

		if (is_null($page->id)) {
			$this->view->setTitle('[$NewPage]');
		} else {
			$this->view->setTitle('[$EditPage]');
		}

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('pages_list')));
		$this->view->ignoreWidgetPlaceHolder();
		$this->view->render('edit-page');
	}

	public function delete_page($id)
	{
		$res = $this->model->deletePage($id);
		if ($res !== false) {
			$this->view->AddNotice('Page has been deleted successfully.');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		//Fshijme njekohesisht edhe menu item nese ekzistion per kete faqe.
		//$menu_md = Loader::getModel('Menu');
		//$menu_md->deleteItemByTargetPage($id);

		Utils::backRedirect($this->getActionUrl('pages_list'));
	}

	public function generate_sitemap()
	{
		$output_file = DOCROOT.DATA_DIR.'sitemap.xml';
		$site = SITE_SCHEME.'://'.$_SERVER['HTTP_HOST'];
		$skip_url = array();
		require_once DOCROOT.CLASSES_PATH.'SiteMap_class.php';

		$SitemapGenerator = new SiteMap($output_file, $site, $skip_url, 'weekly');
		$generated = $SitemapGenerator->start();

		$this->view->AddNotice($SitemapGenerator->getDebug());
		$this->view->render('empty');
	}
}
