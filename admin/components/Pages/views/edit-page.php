<?php
$pg_name = isset($_POST['name']) ? $_POST['name'] : $page->name;
$pg_layout = isset($_POST['layout']) ? $_POST['layout'] : $page->layout;
$pg_idcomponent = isset($_POST['id_component']) ? $_POST['id_component'] : $page->id_component;
$pg_action = isset($_POST['action']) ? $_POST['action'] : $page->action;
$pg_action_params = isset($_POST['action_params']) ? $_POST['action_params'] : $page->action_params;
$pg_enabled = isset($_POST['enabled']) ? $_POST['enabled'] : $page->enabled;
$pg_home = isset($_POST['home']) ? $_POST['home'] : $page->home;
$pg_show_content_heading = isset($_POST['show_content_heading']) ? $_POST['show_content_heading'] : $page->show_content_heading;

$robots = explode(',', $page->robots);
if (count($robots) == 2) {
	$rindex = 1;
	$rfollow = 1;
} elseif (count($robots) == 1) {
	$rindex = ($robots[0] == 'INDEX') ? 1 : 0;
	$rfollow = ($robots[0] == 'FOLLOW') ? 1 : 0;
} else {
	$rindex = 0;
	$rfollow = 0;
}
$pg_robots_index = isset($_POST['robots_index']) ? $_POST['robots_index'] : $rindex;
$pg_robots_follow = isset($_POST['robots_follow']) ? $_POST['robots_follow'] : $rfollow;

if (!is_null($page->parameters)) {
	$page_parameters = json_decode($page->parameters, true);
}

foreach (CMSSettings::$available_langs as $lang) {
	$alias_{$lang} = isset($_POST['alias_'.$lang]) ? $_POST['alias_'.$lang] : ((!is_null($page->{$lang})) ? $page->{$lang}->alias : '');
	$title_{$lang} = isset($_POST['title_'.$lang]) ? $_POST['title_'.$lang] : ((!is_null($page->{$lang})) ? $page->{$lang}->title : '');
	$meta_title_{$lang} = isset($_POST['meta_title_'.$lang]) ? $_POST['meta_title_'.$lang] : ((!is_null($page->{$lang})) ? $page->{$lang}->meta_title : '');
	$meta_description_{$lang} = isset($_POST['meta_description_'.$lang]) ? $_POST['meta_description_'.$lang] : ((!is_null($page->{$lang})) ? $page->{$lang}->meta_description : '');
	$meta_keywords_{$lang} = isset($_POST['meta_keywords_'.$lang]) ? $_POST['meta_keywords_'.$lang] : ((!is_null($page->{$lang})) ? $page->{$lang}->meta_keywords : '');
	$content_{$lang} = isset($_POST['content_'.$lang]) ? $_POST['content_'.$lang] : ((!is_null($page->{$lang})) ? $page->{$lang}->content : '');
}
?>

<script>
    function refreshActionList() {
        //console.log("<?= Utils::getControllerUrl('Components/ajx_getActions') ?>/" + $('#component').val());
        $.ajax({
            url: "<?= Utils::getControllerUrl('Components/ajx_getActions') ?>/" + $('#component').val(),
            dataType: "json",
            success: function (data) {
                console.log(data);
                var options, index, select, option;
                // Get the raw DOM object for the select box
                select = document.getElementById('actionsList');
                // Clear the old options
                select.options.length = 0;
                // Load the new options
                options = data.options; // Or whatever source information you're working with
                for (index = 0; index < options.length; ++index) {
                    option = options[index];
                    select.options.add(new Option(option.text, option.value));
                }
                $('#actionsList').val('<?= $pg_action ?>');
            },
            error: function (e) {
                select = document.getElementById('actionsList');
                // Clear the old options
                select.options.length = 0;
                select.options.add(new Option('', ''));
                console.log('Error: ' + e);
            }
        });
    }
    $(document).ready(function () {

        if ($('#component').val() !== '') {
            refreshActionList();
        }
    });

    function generatedAlias(id_src, id_dest) {
        $('#' + id_dest).val(replaceAll($('#' + id_src).val(), " ", "-").toLowerCase());
    }
//
//    function replaceAll(str, find, replace) {
//        return str.replace(new RegExp(find, 'g'), replace);
//    }
</script>












<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>


<form method="post" action="" class="form-horizontal">

    <div class="col-md-12">
        
        
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Enable Page</label>
                        <div class="col-sm-8">
                            <input <?= ($pg_enabled) ? 'checked' : '' ?> data-toggle="toggle" data-on="Enabled" data-off="Disabled" type="checkbox" name="enabled">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 required">Page Title<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="name" placeholder="Page Title" value="<?= $pg_name ?>">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        

            <p class="text-center">
                <button type="submit" name="save" class="btn btn-info">
                    <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                </button>
            </p>
            <p>&nbsp;</p>

        
        <ul  class="nav nav-tabs">
            <li class="active"><a  href="#tab-information" data-toggle="tab">[$PageInformation]</a></li>
            <li><a href="#tab-page-content" data-toggle="tab">[$Content]</a></li>
            <li><a href="#tab-design" data-toggle="tab">[$Design]</a></li>
            <li><a href="#tab-meta" data-toggle="tab">[$SearchEngineOptimization]</a></li>
        </ul>

        <div class="tab-content clearfix">

            <div class="tab-pane active" id="tab-information">

                <div class="tab-content clearfix">
                
                    <div class="form-group">
                        <label class="control-label col-sm-2">Component</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="component" name="id_component"
                                    onchange="refreshActionList()">
                                <option></option>
                                <?php foreach ($components as $component) {
	?>
                                    <option
                                        value="<?= $component->id ?>" <?= ($component->id == $pg_idcomponent) ? 'selected' : '' ?>><?= $component->name ?></option>
                                    <?php
} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Action</label>
                        <div class="col-sm-4">
                            <select id="actionsList" class="form-control" name="action">
                                <option value="">--</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="action_params"
                               placeholder="Action Parameters" value="<?= $pg_action_params ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Show Content Heading</label>
                        <div class="col-sm-4">
                            <input <?= ($pg_show_content_heading) ? 'checked' : '' ?> data-width="90" data-toggle="toggle" data-on="Visible" data-off="Hidden" type="checkbox" name="show_content_heading">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Is Home Page</label>
                        <div class="col-sm-4">
                            <input <?= ($pg_home) ? 'checked' : '' ?> data-toggle="toggle" data-on="[$Yes]" data-off="[$No]" type="checkbox" name="home">
                        </div>
                    </div>
                    
                </div>
                
            </div>



            <div class="tab-pane" id="tab-page-content">

                <ul  class="nav nav-tabs">
                    <?php foreach (CMSSettings::$available_langs as $lang) {
		?>
                        <li class="<?= ($lang == CMSSettings::$default_lang) ? 'active' : '' ?>"><a
                                href="#tabcontent_<?= $lang ?>" data-toggle="tab"><?= strtoupper($lang) ?></a></li>
                        <?php
	} ?>
                </ul>

                <div class="tab-content clearfix">
                    <?php foreach (CMSSettings::$available_langs as $lang) {
		?>

                        <div
                            class="tab-pane fade <?= ($lang == CMSSettings::$default_lang) ? 'active in' : '' ?>"
                            id="tabcontent_<?= $lang ?>">

                            
                            <div class="form-group">
                                <label class="control-label col-sm-2">[$ContentHeading]</label>
                                <div class="col-sm-8">
                                    <input type="text"
                                           class="form-control" id="title_<?= $lang ?>"
                                           name="title_<?= $lang ?>" placeholder="Title" value="<?= $title_{$lang} ?>">
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <textarea id="content_<?= $lang ?>" name="content_<?= $lang ?>"><?= $content_{$lang} ?></textarea>
                                    <script>
                                        CKEDITOR.replace('content_<?= $lang ?>', {
                                                filebrowserBrowseUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                                                filebrowserImageBrowseUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
                                                allowedContent : true,
                                                height : 400,
                                                contentsCss : [<?= "'".implode("','", $template_css)."'"; ?>]
                                            });
                                    </script>
                                </div>
                            </div>
                            

                        </div>
                    <?php
	} ?>
                </div>

            </div>



            <div class="tab-pane" id="tab-design">
                <div class="tab-content clearfix">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Layout</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="layout">
                                <?php foreach ($layouts as $layout) {
		?>
                                    <option value="<?= $layout ?>" <?= ($layout == $pg_layout) ? 'selected' : '' ?>><?= $layout ?></option>
                                <?php
	} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Layout Positions</label>
                        <div class="col-sm-8">
                            <?php
								foreach ($layout_positions as $pos) {
									?>
                                    <div class="position_container">
                                    <h4><?=$pos['name']?></h4>
                                    <p><?=$pos['description']; ?></p>
                                    <div class="panel panel-group">
                                      <?foreach ($position_blocks[$pos['name']] as $pb) {
										?>
                                        <div class="panel panel-inverse">
                                            <div class="panel-heading clearfix">
                                                <h5 class="panel-title pull-left">
                                                    <a data-toggle="collapse" href="#posb_<?=$pb->id?>"><i class="fa fa-chevron-down"></i> &nbsp; <?=$pb->module_name?> [<?=$pb->id?>]</a>
                                                </h5>
                                                <div class="btn-group pull-right">
                                                    <?if ($pb->enabled) {
											?>
                                                        <a href="#" class="btn btn-info btn-sm"><i class="fa fa-check"></i></a>
                                                    <?
										} else {
											?>
                                                        <a href="#" class="btn btn-default btn-sm"><i class="fa fa-check"></i></a>
                                                    <?
										} ?>
                                                    <a href="#" class="btn btn-warning btn-sm"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </div>
                                            <div id="posb_<?=$pb->id?>" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <?$params = json_decode($pb->parameters, true);
										foreach ($params as $par => $val) {
											?>
                                                            <div class="col-sm-2"><?=$par?>:</div>
                                                            <div class="col-sm-10"><?=$val?></div>
                                                    <?php
										} ?>
                                                </div>
                                            </div>
                                        </div>
                                      <?
									} ?>
                                  </div>
                                <div class="text-center">
                                    <a href="" class="btn btn-info">Add Element</a>
                                </div>
                                </div>
                            <?php
								}
							?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="tab-meta">
                
                <ul  class="nav nav-tabs">
                    <?php foreach (CMSSettings::$available_langs as $lang) {
								?>
                        <li class="<?= ($lang == CMSSettings::$default_lang) ? 'active' : '' ?>"><a
                                href="#tabmeta_<?= $lang ?>" data-toggle="tab"><?= strtoupper($lang) ?></a></li>
                        <?php
							} ?>
                </ul>

                <div class="tab-content clearfix">
                    <?php foreach (CMSSettings::$available_langs as $lang) {
								?>

                        <div class="tab-pane fade <?= ($lang == CMSSettings::$default_lang) ? 'active in' : '' ?>"
                            id="tabmeta_<?= $lang ?>">
                            
                                <div class="form-group">
                                    <label class="control-label col-sm-2">URL Key</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="alias_<?= $lang ?>"
                                           name="alias_<?= $lang ?>" placeholder="Alias"
                                           value="<?= $alias_{$lang} ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Meta Title</label>
                                    <div class="col-sm-8">
                                        <input type="text"
                                           class="form-control" id="meta_title_<?= $lang ?>"
                                           name="meta_title_<?= $lang ?>" placeholder="Meta Title" value="<?= $meta_title_{$lang} ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Meta Description</label>
                                    <div class="col-sm-8">
                                        <textarea type="text" class="form-control" name="meta_description_<?= $lang ?>"
                                              placeholder="Meta Description" rows="4"
                                              style="max-width: 100%"><?= $meta_description_{$lang} ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Meta Keywords</label>
                                    <div class="col-sm-8">
                                        <textarea type="text" class="form-control" name="meta_keywords_<?= $lang ?>"
                                              placeholder="Meta Keywords" rows="4"
                                              style="max-width: 100%"><?= $meta_keywords_{$lang} ?></textarea>
                                    </div>
                                </div>

                        </div>
                    <?php
							} ?>
                </div>
                
                <label class="control-label col-sm-2">Robots</label>
                <div class="col-sm-8">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="FOLLOW"
                                   name="robots_follow" <?= ($pg_robots_follow) ? 'checked' : '' ?>>FOLLOW
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="INDEX"
                                   name="robots_index" <?= ($pg_robots_index) ? 'checked' : '' ?>>INDEX
                        </label>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-12">
        <p class="text-center">
        <button type="submit" name="save" class="btn btn-info">
            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
        </button>
        </p>
    </div>
    <p>&nbsp;</p>
</form>








<?php if (!is_null($page->id)) {
								?>
    <?php if (isset($controllerParameters) && count($controllerParameters)) {
									?>
        <div class="col-md-6">
            <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$GeneralInformation]</h4>
                </div>
                <div class="panel-body panel-form" style="padding:15px !important">
                    <form method="post">
                        <div
                            style="border: 1px solid lightgray; padding: 15px; border-radius: 2px; box-shadow: 0px 0px 1px lightgray">
                            <h4>Page Parameters</h4>
                            <hr>
                            <div class="form-group">
                                <?php
								foreach ($controllerParameters as $field) {
									?>
                                    <?php if ($field['type'] == 'text') {
										?>
                                        <label><?= $field['label'] ?></label>
                                        <input type="text" class="form-control" name="<?= $field['name'] ?>"
                                               placeholder="<?= $field['description'] ?>"
                                               value="<?= (isset($page_parameters[$field['name']])) ? $page_parameters[$field['name']] : '' ?>">
                                           <?php
									} ?>

                                    <?php if ($field['type'] == 'number') {
										?>
                                        <label><?= $field['label'] ?></label>
                                        <input type="number" class="form-control"
                                               name="<?= $field['name'] ?>"
                                               placeholder="<?= $field['description'] ?>"
                                               value="<?= (isset($page_parameters[$field['name']])) ? $page_parameters[$field['name']] : '' ?>">
                                           <?php
									} ?>


                                    <?php if ($field['type'] == 'radio') {
										?>
                                        <label><?= $field['label'] ?></label>
                                        <?php foreach ($field['fields'] as $option) {
											?>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="<?= $field['name'] ?>"
                                                           id="optionsRadios1"
                                                           value="<?= $option['value'] ?>" <?= (isset($page_parameters[$field['name']]) && ($option['value'] == $page_parameters[$field['name']])) ? 'checked' : '' ?>>
                                                           <?= $option['label'] ?>
                                                </label>
                                            </div>
                                        <?php
										} ?>
                                    <?php
									} ?>


                                    <?php if ($field['type'] == 'select') {
										?>
                                        <label><?= $field['label'] ?></label>
                                        <select class="form-control" name="<?= $field['name'] ?>">
                                            <?php foreach ($field['fields'] as $option) {
											?>
                                                <option
                                                    value="<?= $option['value'] ?>" <?= (isset($page_parameters[$field['name']]) && ($option['value'] == $page_parameters[$field['name']])) ? 'selected' : '' ?>><?= $option['label'] ?></option>
                                                <?php
										} ?>
                                        </select>
                                    <?php
									} ?>


                                    <!--SQL List Type-->
                                    <?php if ($field['type'] == 'sqllist') {
										?>
                                        <label><?= $field['label'] ?></label>
                                        <select class="form-control" name="<?= $field['name'] ?>">
                                            <!--Static Options-->
                                            <?php foreach ($field['fields'] as $option) {
											?>
                                                <option
                                                    value="<?= $option['value'] ?>" <?= (isset($page_parameters[$field['name']])) ? (($page_parameters[$field['name']] == $option['value']) ? 'selected' : '') : '' ?>><?= $option['label'] ?></option>
                                                <?php
										} ?>
                                            <!--SQL Options-->
                                            <?php foreach ($field['source']['elements'] as $option) {
											?>
                                                <option
                                                    value="<?= $option[$field['source']['value']] ?>" <?= (isset($page_parameters[$field['name']])) ? (($page_parameters[$field['name']] == $option[$field['source']['value']]) ? 'selected' : '') : '' ?>><?= $option[$field['source']['label']] ?></option>
                                                <?php
										} ?>
                                        </select>

                                    <?php
									} ?>


                                    <!--SQL List Multi Selection Type-->
                                    <?php if ($field['type'] == 'sqllist_multi') {
										?>
                                        <label><?= $field['label'] ?></label>
                                        <select class="form-control" multiple name="<?= $field['name'] ?>[]" size="8">
                                            <!--Static Options-->
                                            <?php foreach ($field['fields'] as $option) {
											?>
                                                <option
                                                    value="<?= $option['value'] ?>" <?= (count($page_parameters[$field['name']])) ? (in_array($option['value'], $page_parameters[$field['name']]) ? 'selected' : '') : '' ?>><?= $option['label'] ?></option>
                                                <?php
										} ?>
                                            <!--SQL Options-->
                                            <?php foreach ($field['source']['elements'] as $option) {
											?>
                                                <option
                                                    value="<?= $option[$field['source']['value']] ?>" <?= (count($page_parameters[$field['name']])) ? ((in_array($option[$field['source']['value']], $page_parameters[$field['name']])) ? 'selected' : '') : '' ?>><?= $option[$field['source']['label']] ?></option>
                                                <?php
										} ?>
                                        </select>

                                    <?php
									} ?>


                                    <!--  tipe te tjerash inputesh-->

                                <?php
								} ?>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-info" name="save_parameters"><i
                                        class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
								}
							}
?>


