<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Pages');

foreach ($pages as &$row) {
	$row->rows_buttons = array(
		new Button("<i class='fa fa-pencil'></i>", $url_page_edit.'/'.$row->id, 'xs', 'btn-xs btn-info', '[$Edit]'),
	);
}

$table->setElements($pages);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', 'ID', 'string', 'left'),
	new TableList_Field('name', '[$Title]', 'string', 'left'),
	new TableList_Field('alias', '[$URLKey]', 'string', 'left'),
	new TableList_Field('layout', '[$Layout]', 'string', 'left'),
	new TableList_Field('creation_date', '[$CreationDate]', 'datetime', 'left'),
	new TableList_Field('update_date', '[$UpdateDate]', 'datetime', 'left'),
	new TableList_Field('meta_title', '[$MetaTitle]', 'string', 'left'),
	new TableList_Field('enabled', '[$Status]', 'bool', 'left'),
));
$table->multipleDeletion(false);
$table->setUrl_action($url_pages_list);
$table->setUrl_delete($url_page_delete);

$table->render();
