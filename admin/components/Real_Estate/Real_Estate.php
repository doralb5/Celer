<?php

require_once DOCROOT.LIBS_PATH.'Email.php';

class Real_Estate_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_realestates_list', $this->getActionUrl('re_list'));
		$this->view->set('url_request_list', $this->getActionUrl('RequestList'));
		$this->view->set('url_realestate_edit', $this->getActionUrl('realestate_edit'));
		$this->view->set('url_request_edit', $this->getActionUrl('request_edit'));
		$this->view->set('url_request_delete', $this->getActionUrl('request_delete'));
		$this->view->set('url_realestate_delete', $this->getActionUrl('realestate_delete'));
		$this->view->set('url_realestate_enable', $this->getActionUrl('realestate_enable'));
		$this->view->set('url_realestate_disable', $this->getActionUrl('realestate_disable'));
		$this->view->set('url_realestate_details', $this->getActionUrl('realestates-details'));
		$this->view->set('url_typologies_list', $this->getActionUrl('typologies_list'));
		$this->view->set('url_typologies_edit', $this->getActionUrl('typology_edit'));
		$this->view->set('url_typology_delete', $this->getActionUrl('typology_delete'));
		$this->view->set('url_typology_enable', $this->getActionUrl('typology_enable'));
		$this->view->set('url_typology_disable', $this->getActionUrl('typology_disable'));
		$this->view->set('url_addtImage_delete', $this->getActionUrl('addtImage_delete'));
		$this->view->set('url_addtImage_download', $this->getActionUrl('addtImage_download'));
		$this->view->set('url_flyer_edit', $this->getActionUrl('editFlyer'));
		$this->view->set('url_flyer_pdf', $this->getActionUrl('printPdf'));

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function RequestList()
	{
		$elements_per_page = 20;

		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$RealEstateRequest_tbl = TABLE_PREFIX.'RealEstate_Request';
		$filter = '';
		$sorting = "  $RealEstateRequest_tbl.id";
		$requests = $this->model->getListRequest($elements_per_page, $offset, $filter, $sorting);
		$totalElements = $this->model->getLastCounter();
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('requests', $requests);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'request_list';
		$this->view->render($view);
	}

	public function request_edit($id)
	{
		if (!is_null($id)) {
			$request = $this->model->getRequest($id);
			if ($request == null) {
				$this->view->addError('[$request_not_exist]');
				$this->view->render('empty');
				return;
			}
			$typologies = $this->model->getTypologies(-1);
			$this->view->set('typologies', $typologies);

			$contract = $this->model->getContract();
			$this->view->set('contract', $contract);

			$floors = $this->model->getFloors();
			$this->view->set('floors', $floors);

			$buildings = $this->model->getBuildings();
			$this->view->set('buildings', $buildings);

			$heating = $this->model->getHeating();
			$this->view->set('heating', $heating);

			if ($request === false) {
				$this->AddError("Request $id not found");
			} elseif (isset($_POST['save'])) {
				$this->model->startTransaction();
				$request->description = $_POST['description'];
				$request->id_contract = $_POST['id_contract'];
				$request->id_typology = $_POST['id_typology'];
				$request->address = $_POST['address'];
				$request->price = str_replace('.', '', $_POST['price']);
				$request->rooms = $_POST['rooms'];
				$request->surface = $_POST['surface'];
				$request->id_heating = $_POST['id_heating'];
				$request->id_building_state = $_POST['id_building'];
				$request->bathrooms = $_POST['bathrooms'];
				$request->id_floor = $_POST['id_floor'];
				$request->zip_code = $_POST['zip_code'];
				$request->street = $_POST['street'];
				(isset($_POST['garden'])) ? $request->garden = $_POST['garden'] : $request->garden = '0';
				(isset($_POST['terrace'])) ? $request->terrace = $_POST['terrace'] : $request->terrace = '0';
				(isset($_POST['furnished'])) ? $request->furnished = $_POST['furnished'] : $request->furnished = '0';
				(isset($_POST['balcony'])) ? $request->balcony = $_POST['balcony'] : $request->balcony = '0';
				(isset($_POST['energetic_class'])) ? $request->energetic_class = $_POST['energetic_class'] : $request->energetic_class = '';

				$inserted_id = $this->model->saveRequest($request);
				if (!is_array($inserted_id)) {
					if (is_null($id)) {
						$realestate->id = $inserted_id;
					}
					if ($errors == 0) {
						$this->model->commit();
						$this->AddNotice('Request has been saved successfully.');

						if (!is_bool($inserted_id)) {
							$this->LogsManager->registerLog('Request', 'insert', 'Request inserted with id : '.$inserted_id, $inserted_id);
							Utils::RedirectTo($this->getActionUrl('realestate_edit')."/$inserted_id");
						}
						Utils::RedirectTo($this->getActionUrl('request_edit/'."$inserted_id"));
					}
				} else {
					$this->AddError('Saving failed!');
					$this->model->rollback();
				}
			}
		}
		$this->view->set('request', $request);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'request_edit';
		$this->view->render($view);
	}

	public function request_delete($id)
	{
		$request = $this->model->getRequest($id);
		$result = $this->model->deleteRequest($id);
		Utils::backRedirect($this->getActionUrl('RequestList'));
	}

	public function re_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('RealEstate'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '1 ';
		$status = isset($_GET['status']) ? $_GET['status'] : '';
		$enabled = ($status == 'enabled') ? 1 : ($status == 'disabled' ? 0 : '');
		$RealEstate_tbl = TABLE_PREFIX.'RealEstate';
		if ($enabled !== '') {
			$filter .= "AND $RealEstate_tbl.enabled = '$enabled'";
		}
		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$realestates = $this->model->getRealEstate($id);
				$additionalImages = $this->model->getRealEstateImage(100, 0, "id_realestate = $id");
				$res = $this->model->deleteRealEstate($id);

				if ($res !== false) {
					$target = DOCROOT.MEDIA_ROOT.'realestates';
					if ($realestate->image != '' && file_exists($target.$realestate->image)) {
						unlink($target.$realestate->image);
					}

					foreach ($additionalImages as $image) {
						if (file_exists($target.DS.'additional'.DS.$image->image)) {
							unlink($target.DS.'additional'.DS.$image->image);
						}
					}
					$this->LogsManager->registerLog('RealEstate', 'delete', 'RealEstate deleted with id : '.$id, $id);
				}
			}
			if ($res !== false) {
				$this->view->AddNotice('RealEstate has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('re_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'RealEstate.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'RealEstate.title', 'peso' => 90),
				array('field' => TABLE_PREFIX.'RealEstateContract.type', 'peso' => 80),
				array('field' => TABLE_PREFIX.'RealEstateTypology.type', 'peso' => 80),
			);
			$realestates = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$realestates = $this->model->getList($elements_per_page, $offset, $filter);
		}
		$totalElements = $this->model->getLastCounter();
		$this->view->set('totalElements', $totalElements);
		$enabledSelectFil = new SelectList('status');
		$enabledSelectFil->setSelectLabel('label');
		$enabledSelectFil->setSelectValue('value');
		$enabs = array('enabled' => '[$Enabled]', 'disabled' => '[$Disabled]');
		foreach ($enabs as $key => $type) {
			$element = new stdClass();
			$element->label = $type;
			$element->value = $key;
			$enabledSelectFil->addElement($element);
		}
		$enabledSelectFil->setDefault('[$Status]');
		$this->view->set('enabledSelectFil', $enabledSelectFil);
		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddRealEstate]', $this->getActionUrl('realestate_edit')));
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('realestates', $realestates);
		$this->view->setTitle('[$RealEstates]');
		$this->view->render('realestate_list');
	}

	public function realestate_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$RealEstate]', $this->getActionUrl('re_list'));
		if (!is_null($id)) {
			$realestate = $this->model->getRealEstate($id);
			if ($realestate == null) {
				$this->view->addError('[$real_estate_not_exist]');
				$this->view->render('empty');
				return;
			}
			HeadHTML::setTitleTag('Edit RealEstate - '.$realestate->title.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir($this->view->getTerm('EditRealEstate'), $this->getActionUrl('realestate_edit')."/$id");
		} else {
			$realestate = new RealEstate_Entity();
			HeadHTML::setTitleTag($this->view->getTerm('NewRealEstate').' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$NewRealEstate]', $this->getActionUrl('realestate_edit'));
		}
		$logged_user = UserAuth::getLoginSession();
		if ($realestate === false) {
			$this->AddError("RealEstate $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['description'] == '') ? $this->AddError('Description is required!') : '';
			($_POST['id_contract'] == '') ? $this->AddError('Contract is required!') : '';
			($_POST['id_typology'] == '') ? $this->AddError('Typology is required!') : '';
			($_POST['date'] == '') ? $this->AddError('Publish Date is required!') : '';
			($_POST['street'] == '') ? $this->AddError('Street  is required!') : '';
			($_POST['locality'] == '') ? $this->AddError('Locality  is required!') : '';
			($_POST['price'] == '') ? $this->AddError('Price  is required!') : '';
			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}
			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $realestate->image;
				} else {
					$_POST['image'] = $realestate->image;
					$old_image = null;
				}
				if (!is_null($id) && $realestate->id_typology != $_POST['id_typology']) {
					$old_category = $realestate->id_typology;
				} else {
					$old_category = null;
				}
				$this->model->startTransaction();
				$realestate->description = $_POST['description'];
				$realestate->ref_code = $_POST['ref_code'];
				$realestate->image = $_POST['image'];
				$realestate->id_contract = $_POST['id_contract'];
				$realestate->id_typology = $_POST['id_typology'];
				$realestate->publish_date = $_POST['date'];
				$realestate->address = $_POST['address'];
				$realestate->title = $_POST['title'];
				$realestate->price = str_replace('.', '', $_POST['price']);
				$realestate->rooms = $_POST['rooms'];
				$realestate->square_meter = $_POST['square_meter'];
				$realestate->id_heating = $_POST['id_heating'];
				$realestate->id_building_state = $_POST['id_building'];
				$realestate->bathrooms = $_POST['bathrooms'];
				$realestate->id_floor = $_POST['id_floor'];
				$realestate->locality = $_POST['locality'];
				$realestate->zip_code = $_POST['zip_code'];
				$realestate->street = $_POST['street'];
				(isset($_POST['id_currency'])) ? $realestate->id_currency = $_POST['id_currency'] : $realestate->id_currency = 2;
				(isset($_POST['garden'])) ? $realestate->garden = $_POST['garden'] : $realestate->garden = '0';
				(isset($_POST['terrace'])) ? $realestate->terrace = $_POST['terrace'] : $realestate->terrace = '0';
				(isset($_POST['furnished'])) ? $realestate->furnished = $_POST['furnished'] : $realestate->furnished = '0';
				(isset($_POST['balcony'])) ? $realestate->balcony = $_POST['balcony'] : $realestate->balcony = '0';
				(isset($_POST['hide_price'])) ? $realestate->hide_price = $_POST['hide_price'] : $realestate->hide_price = '0';
				(isset($_POST['energetic_class'])) ? $realestate->energetic_class = $_POST['energetic_class'] : $realestate->energetic_class = '';
				$realestate->location = $_POST['location'];
				$realestate->coordinates = ltrim(rtrim($_POST['coordinates'], ')'), '(');
				$realestate->featured = isset($_POST['featured']) ? $_POST['featured'] : 0;
				$realestate->enabled = isset($_POST['enabled']) ? $_POST['enabled'] : 0;
				$realestate->id_user = (isset($_POST['id_user']) && $_POST['id_user'] != '') ? $_POST['id_user'] : $logged_user['id'];
				$inserted_id = $this->model->saveRealEstate($realestate);
				if (!is_array($inserted_id)) {
					if (is_null($id)) {
						$realestate->id = $inserted_id;
					}
					if ($errors == 0) {
						$this->model->commit();
						$this->AddNotice('RealEstate has been saved successfully.');
						if (strlen($_FILES['image']['tmp_name'])) {
							$target = DOCROOT.MEDIA_ROOT.DS.'realestates';
							Utils::createDirectory($target);
							$filename = $target.DS.$_POST['image'];
							move_uploaded_file($_FILES['image']['tmp_name'], $filename);
							if (!is_null($id)) {
								unlink($target.DS.$old_image);
							}
						}
						if (!is_bool($inserted_id)) {
							$this->LogsManager->registerLog('RealEstate', 'insert', 'RealEstate inserted with id : '.$inserted_id, $inserted_id);
							Utils::RedirectTo($this->getActionUrl('realestate_edit')."/$inserted_id");
						} else {
							$this->LogsManager->registerLog('RealEstate', 'update', 'RealEstate updated with id : '.$id, $id);
							Utils::RedirectTo($this->getActionUrl('realestate_edit')."/$id");
						}
						Utils::RedirectTo($this->getActionUrl('realestate_edit/'."$inserted_id"));
					} else {
						$this->model->rollback();
						$this->view->AddError('Problem while adding additional categories.');
					}
				} else {
					$this->AddError('Saving failed!');
					$this->model->rollback();
				}
			}
		} elseif (isset($_POST['AddImage'])) {
			if ($_FILES['images']['tmp_name'][0] == '') {
				$this->view->AddError('Please upload a file...');
			}
			$file_names = $_FILES['images']['name'];
			foreach ($file_names as $file_name) {
				(!Utils::allowedFileType($file_name)) ? $this->view->AddError('Please upload an image!') : '';
			}
			if (count($this->view->getErrors()) == 0) {
				$failures = 0;
				for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
					$tmp_name = time().'_'.$_FILES['images']['name'][$i];
					$addImage = new RealEstateImage_Entity();
					$addImage->id_realestate = $id;
					$addImage->image = $tmp_name;
					$addImage->size = $_FILES['images']['size'][$i];
					$res = $this->model->saveRealEstateImage($addImage);
					if (!is_array($res)) {
						$target = DOCROOT.MEDIA_ROOT.'realestates'.DS.'additional';
						if (!is_dir($target)) {
							Utils::createDirectory($target);
						}
						$filename = $target.DS.$tmp_name;
						move_uploaded_file($_FILES['images']['tmp_name'][$i], $filename);
					} else {
						$failures++;
					}
				}
				if ($failures > 0) {
					$this->AddError('There was '.$failures.' failures during image upload!');
				} else {
					$this->view->AddNotice('Image has been uploaded successfully.');
				}
			}
			Utils::RedirectTo($this->getActionUrl('realestate_edit')."/$id");
		}
		$contract = $this->model->getContract(-1);
		$this->view->set('contract', $contract);
		$typology = $this->model->getTypologies(-1);
		$floor = $this->model->getFloors(-1);
		$heating = $this->model->getHeating(-1);
		$buildings = $this->model->getBuildings(-1);
		$currency = $this->model->getCurrency(-1);
		$this->view->set('currency', $currency);
		if (!is_null($id)) {
			$additionalImages = $this->model->getRealEstateImages(50, 0, "id_realestate = $id");
			$this->view->set('additionalImages', $additionalImages);
		}
		$this->view->set('typology', $typology);
		$this->view->set('floor', $floor);
		$this->view->set('heating', $heating);
		$this->view->set('buildings', $buildings);
		$this->view->set('realestate', $realestate);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('realestates_list'))));
		$title = (!is_null($id)) ? '[$EditRealEstate]' : '[$NewRealestate]';
		$this->view->setTitle($title);
		$this->view->render('realestate-edit');
	}

	public function newRealEstateBodyMessageUser($data)
	{
		$logged_user = UserAuth::getLoginSession();
		$message = 'I nderuar klient , prona juaj eshte shtuar me sukses ne faqen '.CMSSettings::$webdomain.' me te dhenat e meposhteme : <br/>';
		$message .= 'Prona '.' : '.$data['title'].'<br/>';
		$message .= 'Address '.' : '.$data['address'].'<br/>';
		//$message .= 'User ' . ' : ' . $logged_user['email'] . '<br/>';
		$message .= 'Price'.' : '.$data['price'].'<br/>';
		return $message;
	}

	public function realestate_details($id)
	{
		Utils::saveCurrentPageUrl();
		$realestate = $this->model->getRealEstate($id);
		$this->view->set('realestate', $realestate);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('realestates_list'))));
		$this->view->BreadCrumb->addDir('[$RealEstates]', $this->getActionUrl('realestates_list'));
		$this->view->BreadCrumb->addDir('[$RealEstateDetails]', $this->getActionUrl('realestates_details')."/$id");
		$this->view->setTitle('[$Details]');
		$this->view->render('realestates-details');
	}

	public function realestate_delete($id)
	{
		$realestate = $this->model->getRealEstate($id);
		$additionalImages = $this->model->getRealEstateImages(50, 0, "id_realestate = $id");
		$result = $this->model->deleteRealEstate($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'realestates';
			if ($realestate->image != '' && file_exists($target.$realestate->image)) {
				unlink($target.$realestate->image);
			}

			foreach ($additionalImages as $image) {
				if (file_exists($target.DS.'additional'.DS.$image->image)) {
					unlink($target.DS.'additional'.DS.$image->image);
				}
			}

			$this->LogsManager->registerLog('RealEstate', 'delete', "RealEstate deleted with id : $id and title : ".$realestate->title, $id);
			$this->AddNotice('RealEstate deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('realestate_list'));
	}

	public function realestate_enable($id)
	{
		if (!is_null($id)) {
			$realestate = $this->model->getRealEstate($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$realestate->enabled = 1;

		if ($realestate->notified == 0) {
			//Njoftojme userin
			$sended = $this->notifyRealEstateEnabled($realestate);
			if ($sended) {
				$realestate->notified = 1;
			}
		}
		$res = $this->model->saveRealEstate($realestate);

		if (!is_array($res)) {
			$this->view->AddNotice('RealEstate has been enabled successfully.');
			$this->LogsManager->registerLog('RealEstate', 'update', 'RealEstate enabled with id : '.$realestate->id, $realestate->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('realestate_list'));
	}

	public function realestate_disable($id)
	{
		if (!is_null($id)) {
			$realestate = $this->model->getRealEstate($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$realestate->enabled = 0;
		$res = $this->model->saveRealEstate($realestate);

		if (!is_array($res)) {
			$this->view->AddNotice('RealEstate has been disabled successfully.');
			$this->LogsManager->registerLog('RealEstate', 'update', 'RealEstate disabled with id : '.$realestate->id, $realestate->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('realestate_list'));
	}

	public function typologies_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Typology'.' | '.CMSSettings::$website_title);
		$elements_per_page = 50;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		$totalElements = 0;
		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteTypology($id);
				$this->LogsManager->registerLog('Typology', 'delete', "Typology deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Typologies has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('typologies_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'RealEstateTypology.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'RealEstateTypology.type', 'peso' => 90),
			);
			$typologies = $this->model->searchTypology($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
			$totalElements = $this->model->getLastCounter();
		} else {
			$typologies = $this->model->getTypologies($elements_per_page, $offset, $filter);
		}
		$totalElements = $this->model->getLastCounter();
		//Krijimi i aliaseve
		$pages_md = Loader::getModel('Pages');
		foreach ($typologies as $tp) {
			$alias = $pages_md->getPageAliases(1, 0, "object='RealEstateTypology' AND id_object={$tp->id}");
			if (count($alias) == 0) {
				$alias = new PageAlias_Entity();
				$alias->object = 'RealEstateTypology';
				$alias->id_object = $tp->id;

				$alias->alias = Utils::url_slug($tp->type);

				$alias->id_component = $this->ComponentID;
				$alias->action = 're_list';
				$alias->parameters = Utils::url_slug($tp->type)."-{$tp->id}";
				$res = $pages_md->savePageAlias($alias);
			}
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddTypology]', $this->getActionUrl('typology_edit')));
		$this->view->BreadCrumb->addDir('[$Typology]', $this->getActionUrl('typologies_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('typologies', $typologies);
		$this->view->setTitle('[$typologies]');
		$this->view->render('typologies_list');
	}

	public function typology_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$typologies]', $this->getActionUrl('typologies_list'));
		if (!is_null($id)) {
			$typology = $this->model->getTypology($id);
			HeadHTML::setTitleTag('Edit Typology - '.$typology->type.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditTypology]', $this->getActionUrl('typology_edit')."/$id");
		} else {
			$typology = new RealEstateTypology_Entity();
			HeadHTML::setTitleTag('New Typology'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddTypology]', $this->getActionUrl('typology_edit'));
		}

		if ($typology === false) {
			$this->AddError("Typology $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['type'] == '') ? $this->AddError('Typology Name is required!') : '';
			if (count($this->view->getErrors()) == 0) {
				$typology->type = $_POST['type'];
				$typology->enabled = 1;
				$inserted_id = $this->model->saveTypology($typology);
				if (!is_array($inserted_id)) {
					$this->AddNotice('Typology has been saved successfully!');
					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Typology', 'insert', 'Typology inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('typology_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Typology', 'update', 'Typology updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('typology_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong! - '.$inserted_id[2]);
				}
			}
		}
		$this->view->set('typology', $typology);
		$categories = $this->model->getTypologies(-1);
		$this->view->set('categories', $categories);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('typologies_list'))));
		$title = (!is_null($id)) ? '[$EditTypology]' : '[$Newtypology]';
		$this->view->setTitle($title);
		$this->view->render('typology_edit');
	}

	public function typology_delete($id)
	{
		$typology = $this->model->getTypology($id);
		$result = $this->model->deleteTypology($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Typology', 'delete', "Typology deleted with id : $id, name : {$typology->type}", $id);
			$this->AddNotice('Typology deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('typologies_list'));
	}

	public function newRealEstateBodyMessage($data)
	{
		$logged_user = UserAuth::getLoginSession();
		$message = 'At '.date('d/m/Y H:i').' has been inserted a new realestate. <br/>';
		$message .= 'RealEstate '.' : '.$data['title'].'<br/>';
		$message .= 'Address '.' : '.$data['address'].'<br/>';
		//$message .= 'User ' . ' : ' . $logged_user['email'] . '<br/>';
		$message .= 'Price'.' : '.$data['price'].'<br/>';
		return $message;
	}

	public function notifyRealEstateEnabled($realestate)
	{
		$user_md = Loader::getModel('Users');
		$user = $user_md->getUser($realestate->id_user);
		//I Dergojme email userit me passwordin e resetuar.
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH.'Real_Estate'.DS.'views'.DS);
		$message->setLangPath(COMPONENTS_PATH.'Real_Estate'.DS.'lang'.DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->set('realestate', $realestate);
		$message->set('user', $user);
		$message->renderTemplate(false);
		$txt = $message->render('mails/email_realestate_online', true);
		$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, 'noreply@'.CMSSettings::$webdomain, $realestate->title.$this->view->getTerm('is_online'), $txt);
		if ($sended) {
			$this->view->AddNotice('Notification Mail Sended');
			return true;
		} else {
			$this->view->AddError('There was a problem sending email');
			return false;
		}
	}

	public function typology_enable($id)
	{
		if (!is_null($id)) {
			$typology = $this->model->getTypology($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$typology->enabled = 1;
		$res = $this->model->saveTypology($typology);
		if (!is_array($res)) {
			$this->view->AddNotice('Typology has been enabled successfully.');
			$this->LogsManager->registerLog('Typology', 'update', 'Typology enabled with id : '.$typology->id, $typology->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('typologies_list'));
	}

	public function typology_disable($id)
	{
		if (!is_null($id)) {
			$typology = $this->model->getTypology($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$typology->enabled = 0;
		$res = $this->model->saveTypology($typology);
		if (!is_array($res)) {
			$this->view->AddNotice('Typology has been disabled successfully.');
			$this->LogsManager->registerLog('Typology', 'update', 'Typology disabled with id : '.$typology->id, $typology->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('typologies_list'));
	}

	public function addtImage_download($id)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('re_list'));
		}
		$image = $this->model->getRealEstateImage($id);
		$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'realestates'.DS.'additional'.DS.$image->image;
		//Shkarkimi i Files
		header('Content-Type: '.mime_content_type(DOCROOT.$filename));
		header('Content-Disposition: attachment; filename="'.$image->image.'""');
		readfile(DOCROOT.$filename);
	}

	public function addtImage_delete($id)
	{
		if (!isset($_GET['id_realestate'])) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$image = $this->model->getRealEstateImage($id);

		$res = $this->model->deleteRealEstateImage($id);
		if ($res) {
			$this->LogsManager->registerLog('RealEstateImage', 'delete', "Image deleted with id : $id", $id);
			$this->AddNotice('Image has been deleted successfully.');
			$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'realestates'.DS.'additional'.DS.$image->image;
			unlink(DOCROOT.$filename);
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('re_list')."/{$image->id_realestate}");
	}

	public function listFlyers()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag($this->view->getTerm('flyers_list').' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteFlyer($id);
				$this->LogsManager->registerLog('Flyer', 'delete', "Flyer deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice($this->view->getTerm('[flyers_deleted]'));
			} else {
				$this->view->AddError($this->view->getTerm('[something_went_wrong]'));
			}
			Utils::RedirectTo($this->getActionUrl('listFlyers'));
		}

		$flyers = $this->model->getFlyers($elements_per_page, $offset, $filter, $sorting);
		$totalElements = $this->model->getLastCounter();

		$this->view->set('flyers', $flyers);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);
		$this->view->addButton(new Button('[$New]', $this->getActionUrl('editFlyer')));
		$this->view->render('flyers_list');
	}

	public function editFlyer($id = null)
	{
		if (!is_null($id)) {
			$id = preg_replace('/[^0-9]/', '', $id);
			$flyer = $this->model->getFlyer($id);
			if ($flyer === null) {
				$this->view->AddError('[$flyer] : '.$id.' [$not_found]');
				$this->view->render('empty');
				return;
			} else {
				if (isset($_POST['add-re'])) {
					foreach ($_POST['re'] as $re) {
						$flyer_item = new RealEstatePdfFlyerItems_Entity();
						$flyer_item->id_real_estate = $re;
						$flyer_item->id_flyer = $id;
						$inserted_flyer_item = $this->model->saveFlyerItem($flyer_item);
						// var_dump($inserted_flyer_item);exit;
					}
				}
				$flyer_items = $this->model->getFlyerItems(555, 0, ' id_flyer = '.$flyer->id);
				$this->view->set('flyer_items', $flyer_items);
				HeadHTML::setTitleTag($this->view->getTerm('edit_flyer').$flyer->name.' | '.CMSSettings::$website_title);
				$this->view->BreadCrumb->addDir($this->view->getTerm('edit_flyer'), $this->getActionUrl('editFlyer')."/$id");
			}
		} else {
			$flyer = new RealEstatePdfFlyer_Entity;
			HeadHTML::setTitleTag($this->view->getTerm('new_flyer').' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir($this->view->getTerm('new_flyer'), $this->getActionUrl('editFlyer'));
		}

		if (isset($_POST['save-flyer'])) {
			if ($flyer->id == null || $flyer->id == '') {
				$flyer->creation_date = date('Y-m-d H:i:s', time());
			} else {
				$flyer->last_modification = date('Y-m-d H:i:s', time());
			}
			$flyer->name = $_POST['name'];
			$inserted_flyer = $this->model->saveFlyer($flyer);
			if (!is_array($inserted_flyer)) {
				if (is_bool($inserted_flyer)) {
					$this->view->AddNotice('[$flyer_modified]');
					Utils::RedirectTo($this->getActionUrl('editFlyer').'/'.$id);
				} else {
					$this->view->AddNotice('[$flyer_inserted]');
					Utils::RedirectTo($this->getActionUrl('editFlyer').'/'.$inserted_flyer);
				}
			} else {
				$this->view->AddError('[$flyer_insertion_failed]');
			}
		}

		$filter = '';
		$real_estates = $this->model->getList(-1, 0, $filter);
		$contracts = $this->model->getContract(-1);
		$typologies = $this->model->getTypologies(-1);
		$this->view->set('contracts', $contracts);
		$this->view->set('typologies', $typologies);
		$this->view->set('id', $id);

		$this->view->set('real_estates', $real_estates);
		$this->view->set('flyer', $flyer);
		$this->view->render('flyer-edit');
	}

	public function deleteFlyer($id)
	{
		$this->model->deleteFlyer($id);
		$this->view->AddError('[$flyer_deleted]');
		Utils::backRedirect();
	}

	public function deleteFlyerItem($id)
	{
		$this->model->deleteFlyerItem($id);
		$this->view->AddError('[$flyer_deleted]');
		Utils::backRedirect();
	}

	public function ajx_loadRealEstates()
	{
		$filter = ' 1 ';
		$sorting = ' publish_date DESC ';
		$elements_per_page = 50;
		if (isset($_POST['contract']) && $_POST['contract'] != '') {
			$filter = ' id_contract = '.$_POST['contract'];
		}

		if (isset($_POST['query']) && $_POST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'RealEstate.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'RealEstate.title', 'peso' => 90),
				array('field' => TABLE_PREFIX.'RealEstate.location', 'peso' => 90),
				array('field' => TABLE_PREFIX.'RealEstate.locality', 'peso' => 90),
				array('field' => TABLE_PREFIX.'RealEstateContract.type', 'peso' => 80),
				array('field' => TABLE_PREFIX.'RealEstateTypology.type', 'peso' => 80),
			);
			$real_estates = $this->model->search($_POST['query'], $searchFields, $filter, $sorting, $elements_per_page, 0);
		} else {
			$real_estates = $this->model->getList($elements_per_page, 0, $filter, $sorting);
		}

		foreach ($real_estates as $realestate) {
			echo '<tr class = "re_element">
                    <td>'.$realestate->id.'</td>
                    <td> <img src="http://'.CMSSettings::$webdomain.Utils::genThumbnailUrl('realestates/'.urlencode($realestate->image), 50, 50, array('zc' => 1)).'"> </td>
                    <td> '.$realestate->title.'</td>
                    <td> '.$realestate->publish_date.'</td>
                    <input type="hidden" name="re['.$realestate->id.']" value="'.$realestate->id.'">
                    <input id="re_desc_'.$realestate->id.'" type="hidden" name="re_desc['.$realestate->id.']" value="'.strip_tags($realestate->description).'">
                    <td><input type="button" class="add-button" value="+" data-id = "'.$realestate->id.'"></td>
                    </tr>';
		}
	}

	public function ajx_deleteFlyerItem()
	{
		if (isset($_POST['item_id'])) {
			$this->model->deleteFlyerItem($_POST['item_id']);
		}
	}

	public function printPdf($id)
	{
		$htmlview = new BaseView();
		$htmlview->renderTemplate(false);
		$htmlview->setViewPath(dirname(__FILE__).'/views/');

		$flyer = $this->model->getFlyer($id);
		if ($flyer != null) {
			$flyer_items = $this->model->getFlyerItems(-1, 0, ' id_flyer = '.$id);
			$htmlview->set('flyer', $flyer);
			$htmlview->set('flyer_items', $flyer_items);
			$htmlview->render('htmlpdf_flyer');
		} else {
			$this->AddError('Flyer not exists!');
			$this->view->render('empty');
		}
	}

	public function printPdfFlyer($id = '')
	{
		if (!is_null($id)) {
			$id = preg_replace('/[^0-9]/', '', $id);
			$flyer = $this->model->getFlyer($id);
			if ($flyer === null) {
				$this->view->AddError('[$flyer] : '.$id.' [$not_found]');
				$this->view->render('empty');
				return;
			}
		} else {
			$this->view->AddError('[$please_choose_a_flyer]');
			Utils::RedirectTo(Utils::getComponentUrl($this->getActionUrl('listFlyers')));
			return;
		}

		if (isset($_GET['template'])) {
			$template = $_GET['template'];
		} else {
			$template = 'multiple';
		}

		$flyer_items = $this->model->getFlyerItems(555, 0, ' id_flyer = '.$flyer->id);

		$ids_of_realestates = '';

		foreach ($flyer_items as $flyer_item) {
			if ($flyer_item->id_real_estate != end($flyer_items)->id_real_estate) {
				$ids_of_realestates .= $flyer_item->id_real_estate.' , ';
			} else {
				$ids_of_realestates .= $flyer_item->id_real_estate;
			}
		}
		$filter = ' cms_RealEstate.id in ( '.$ids_of_realestates.' ) ';
		$real_estates = $this->model->getList($limit = 500, $offset = 0, $filter);

		require_once DOCROOT.LIBS_PATH.'tcpdf/tcpdf.php';
		if ($template == 'single') {
			$pdf = new TCPDF();
			foreach ($real_estates as $real_estate) {
				$htmlview = new BaseView();
				$htmlview->renderTemplate(false);
				$htmlview->setViewPath(dirname(__FILE__).'/views/');
				$htmlview->set('real_estate', $real_estate);
				$content = $htmlview->render('htmlpdf_flyer_single', true);
				$pdf->AddPage();
				$pdf->writeHTML($content);
			}
			$pdf->Output(DOCROOT.MEDIA_ROOT.' pdf/'.'example_006.pdf', 'I');
		} else {
			$htmlview = new BaseView();
			$htmlview->renderTemplate(false);
			$htmlview->setViewPath(dirname(__FILE__).'/views/');
			$htmlview->set('real_estates', $real_estates);
			$content = $htmlview->render('htmlpdf_flyer_multiple', true);
			$pdf = new TCPDF();
			$pdf->AddPage();
			$pdf->writeHTML($content);
			$pdf->Output(DOCROOT.MEDIA_ROOT.' pdf/'.'example_006.pdf', 'I');
		}
	}

	public function ajx_updateShortDescription()
	{
		$id = $_POST['item_id'];
		$short_description = strip_tags($_POST['short_description']);
		if (strlen($short_description) < 250 && strlen($short_description) > 10 || $short_description == '') {
			$item = $this->model->getFlyerItem($id);
			$item->short_description = $short_description;
			$result = $this->model->saveFlyerItem($item);
			if (!is_array($result)) {
				echo 'ItemSaved';
			} else {
				echo 'ItemSavingFailed';
			}
		} else {
			echo 'Short description must be longer than 10 and shorter than 250 characters or empty.';
		}
	}
}
