public function realestate_edit($id = null) {
Utils::saveCurrentPageUrl();
$this->view->BreadCrumb->addDir('[$RealEstate]', $this->getActionUrl('realestates_list'));
if (!is_null($id)) {
$realestate = $this->model->getRealEstate($id);
HeadHTML::setTitleTag('Edit RealEstate - ' . $realestate->title . ' | ' . CMSSettings::$website_title);
$this->view->BreadCrumb->addDir('[$EditRealEstate]', $this->getActionUrl('realestate_edit') . "/$id");
} else {
$realestate = new RealEstate_Entity();
HeadHTML::setTitleTag('New RealEstate' . ' | ' . CMSSettings::$website_title);
$this->view->BreadCrumb->addDir('[$NewRealEstate]', $this->getActionUrl('realestate_edit'));
}
$logged_user = UserAuth::getLoginSession();
if ($realestate === false) {
$this->AddError("RealEstate $id not found");
} elseif (isset($_POST['save'])) {
($_POST['description'] == '') ? $this->AddError("Description is required!") : '';
($_POST['id_contract'] == '') ? $this->AddError("Contract is required!") : '';
($_POST['id_typology'] == '') ? $this->AddError("Typology is required!") : '';
($_POST['date'] == '') ? $this->AddError("Publish Date is required!") : '';
if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name']))
(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError("Please upload an image!") : '';
if (count($this->view->getErrors()) == 0) {
if (strlen($_FILES['image']['tmp_name'])) {
$_POST['image'] = time() . '_' . $_FILES['image']['name'];
$old_image = $article->image;
} else {
$_POST['image'] = $article->image;
$old_image = null;
}
if (!is_null($id) && $realestate->id_typology != $_POST['id_typology']) {
$old_category = $realestate->id_typology;
} else {
$old_category = null;
}
$this->model->startTransaction();
$realestate->description = $_POST['description'];
$realestate->image = $_POST['image'];
$realestate->id_contract = $_POST['id_contract'];
$realestate->id_typology = $_POST['id_typology'];
$realestate->id_currency = $_POST['id_currency'];
$realestate->date = $_POST['date'];
$realestate->address = $_POST['address'];
$realestate->title = $_POST['title'];
$realestate->price = $_POST['price'];
$realestate->email = $_POST['email'];
$realestate->location = $_POST['location'];
$realestate->coordinates = ltrim(rtrim($_POST['coordinates'], ')'), '(');
$realestate->featured = isset($_POST['featured']) ? $_POST['featured'] : 0;
$realestate->enabled = isset($_POST['enabled']) ? $_POST['enabled'] : 0;
$realestate->id_user = (isset($_POST['id_user']) && $_POST['id_user'] != '') ? $_POST['id_user'] : $logged_user['id'];
$inserted_id = $this->model->saveRealEstate($realestate);
if (!is_array($inserted_id)) {
if (is_null($id)) {
$realestate->id = $inserted_id;
}
if ($errors == 0) {
$this->model->commit();
$this->AddNotice('RealEstate has been saved successfully.');
if (strlen($_FILES['image']['tmp_name'])) {
$target = DOCROOT . MEDIA_ROOT . DS . 'realestates';
Utils::createDirectory($target);
$filename = $target . DS . $_POST['image'];
move_uploaded_file($_FILES['image']['tmp_name'], $filename);
if (!is_null($id)) {
unlink($target . DS . $old_image);
}
}
//I dergojme email
$message = $this->newRealEstateBodyMessage($_POST);
$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, "New realestate has been inserted", $message);
$msg = "I nderuar klient, objekti juaj" . $_POST['title'] . "eshte shtuar me sukses ne faqen " . CMSSettings::$webdomain;
Email::sendMail($_POST['email'], CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, "Objekti juaj ne faqen " . CMSSettings::$webdomain, $msg);
$this->LogsManager->registerLog('RealEstate', 'insert', 'RealEstate inserted with id : ' . $inserted_id, $inserted_id);
Utils::RedirectTo($this->getActionUrl('realestate_edit/' . "$id"));
} else {
$this->model->rollback();
$this->view->AddError('Problem while adding additional categories.');
}
} else {
$this->AddError("Saving failed!");
$this->model->rollback();
}
}
} elseif (isset($_POST['AddImage'])) {

if ($_FILES['images']['tmp_name'][0] == '') {
$this->view->AddError('Please upload a file...');
}
if (count($this->view->getErrors()) == 0) {
$failures = 0;
for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
$tmp_name = time() . '_' . $_FILES['images']['name'][$i];
$addImage = new RealEstateImage_Entity();
$addImage->id_realestate = $id;
$addImage->image = $tmp_name;
$addImage->size = $_FILES['images']['size'][$i];
$res = $this->model->saveRealEstateImage($addImage);
if (!is_array($res)) {
$target = DOCROOT . MEDIA_ROOT . 'realestate' . DS . $realestate->id_typology . DS . "additional";
Utils::createDirectory($target);
$filename = $target . DS . $tmp_name;
move_uploaded_file($_FILES['images']['tmp_name'][$i], $filename);
} else {
$failures++;
}
}
if ($failures > 0) {
$this->AddError("There was " . $failures . " failures during image upload!");
} else {
$this->view->AddNotice("Image has been uploaded successfully.");
}
}
Utils::RedirectTo($this->getActionUrl('realestate_edit') . "/$id");
}
$contract = $this->model->getContract(10);
$this->view->set('contract', $contract);
$typology = $this->model->getTypologies(10);
$currency = $this->model->getCurrency(10);
$this->view->set('currency', $currency);
if (!is_null($id)) {
$additionalImages = $this->model->getRealEstateImages(50, 0, "id_realestate = $id");
$this->view->set('additionalImages', $additionalImages);
}
$this->view->set('typology', $typology);
$this->view->set('realestate', $realestate);
$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('realestates_list'))));
$title = (!is_null($id)) ? '[$EditRealEstate]' : '[$NewRealestate]';
$this->view->setTitle($title);
$this->view->render('realestate-edit');
}