<?php foreach ($real_estates as $realestate) {
	?>
    <tr class="re_element">
        <td> 
            <?= $realestate->id ?>
        </td>
        <td> 
            <img src="http://<?= CMSSettings::$webdomain.Utils::genThumbnailUrl('realestates/'.urlencode($realestate->image), 50, 50, array('zc' => 1)) ?>"> 
        </td>
        <td> 
            <?= $realestate->title ?>
        </td>
        <td> 
            <?= $realestate->publish_date ?>
        </td>
    <input type="hidden" name="re[<?= $realestate->id ?>]" value="<?= $realestate->id ?>">
    <td><input type="button" class="add-button" value="+" data-id = "<?= $realestate->id ?>"></td>
    </tr>
<?php
} ?>
