
<script async src="http://pioul.fr/ext/jquery-maxlength/jquery.maxlength.min.js"></script>

<?php
$name = isset($_POST['name']) ? $_POST['name'] : $flyer->name;
?>


<div class="row">

    <div class="col-md-<?= ($id != null) ? '4' : '12'; ?>" style="min-height: 350px!important;">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>

                </div>
                <h4 class="panel-title">[$Flyer]</h4>
            </div>
            <div class="panel-body">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
                      novalidate="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="name">[$Title] *</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $name ?>"
                                   name="name" placeholder="[$Title]" data-parsley-required="true"
                                   data-parsley-id="6524" required>
                            <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>


                    <div class="form-group">
                        <p>&nbsp;</p>
                        <?php if ($id) {
	?>
                            <a href="<?= $url_flyer_pdf ?>/<?= $flyer->id ?>" name="download-flyer" class="btn btn-info pull-left"><i class="fa fa-download"></i>&nbsp;&nbsp;[$Download]</a>
                            &nbsp;&nbsp;
                        <?php
} ?>
                        <button type="submit" name="save-flyer" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
                        <p>&nbsp;</p>
                    </div>
                </form>
            </div>
        </div>

    </div>


    <?php if ($id == null) {
		?>
    </div>
<?php
	} ?>





<?php if ($id != null) {
		?>

    <div class="col-md-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">[$available_real_estates]</h4>
            </div>

        </div>
        <div>
            <div class="row">
                <div class="col-md-6">
                    <select class="form-control" onchange="loadRealEstates(this.value)" class="form-control" id="id_category" name="id_contract" required>
                        <option value="" selected="">[$select_contract]</option>
                        <?php foreach ($contracts as $con) {
			?>
                            <option
                                value="<?= $con->id ?>"><?= $con->type ?></option>
                            <?php
		} ?>
                    </select>
                </div>

                <div class="col-md-6">
                    <input class="form-control" type="text" value="" onkeyup="loadRealEstatesBySearch(this.value)" name="search-input">
                </div>
            </div>



            <table id="available-re" class="table available-real-estate">
                <tr class="table-header-available stay">
                    <th class="stay">Id</th> <th class="stay">[$image]</th><th class="stay" >[$title]</th><th class="stay">[$publish_date]</th><th class="stay" >[$action]</th>    
                </tr>


            </table>


        </div>


    </div>


    <div class="col-md-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">[$selected_real_estates]</h4>
            </div>

        </div>
        <form action="" method="POST">

            <div>
                <button id="add-selected" type="submit" name="add-re" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$add_real_estate]
                </button>

                <table class="table selected-real-estate">
                    <tr id="add-selected-tr" class="table-header-selected">
                        <th>Id</th> <th>[$image]</th><th>[$title]</th><th>[$publish_date]</th><th>[$action]</th>    
                    </tr> 
                </table>



            </div>

        </form>
    </div>


    </div>


    <div class="row">

        <?php if (isset($flyer_items)) {
			?>

            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                                    class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                                    class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>

                        </div>
                        <h4 class="panel-title">[$Flyer_items]</h4>
                    </div>
                    <div class="panel-body">
                        <style>
                            table.tableSection {
                                display: table;
                                width: 100%;
                            }
                            table.tableSection thead, table.tableSection tbody {
                                float: left;
                                width: 100%;
                            }
                            table.tableSection tbody {
                                overflow: auto;
                                height: 350px;
                            }
                            table.tableSection tr {
                                width: 100%;
                                display: table;
                                text-align: left;
                            }
                            table.tableSection th, table.tableSection td {
                                width: 33%;
                            }

                        </style>
                        <table class="table tableSection items-table">
                            <thead>
                                <tr>
                                    <th>Id</th> <th>[$image]</th><th>[$title]</th>
        <!--                                    <th>[$description]</th>-->
                                    <th>[$description]</th><th>[$action]</th>    
                                </tr>
                            </thead>
                            <?php
							foreach ($flyer_items as $item) {
								$short_description = '';
								$description = strip_tags($item->description);
								if (strlen($description) < 250) {
									$short_description = $description;
								} else {
									$short_description = Utils::trim_text($description, 250);
								}
								if (strlen(strip_tags($item->short_description)) > 10) {
									$short_description = strip_tags($item->short_description);
								} ?>

                                <tr class="re_element">
                                    <td> 
                                        <?= $item->id ?>
                                    </td>
                                    <td> 
                                        <img src="http://<?= CMSSettings::$webdomain.Utils::genThumbnailUrl('realestates/'.urlencode($item->image), 50, 50, array('zc' => 1)) ?>"> 
                                    </td>
                                    <td> 
                                        <?= $item->title ?>
                                    </td>
            <!--                                    <td> 
                                    <?= strip_tags($item->description) ?>
                                    </td>-->

                                    <?php if (trim($short_description) == '' || strlen(trim($short_description)) < 3) {
									?>
                                        <td> 
                                            <a class="btn btn-sm btn-success pull-right" id='addDescButton-<?= $item->id ?>' data-id ='<?= $item->id ?>' data-toggle="modal" data-target="#FlyerDescription-<?= $item->id ?>">Aggiungi</a>
                                        </td>
                                    <?php
								} else {
									?>
                                        <td> 
                                            <a class="btn btn-sm btn-success pull-right" id='editDescButton-<?= $item->id ?>' data-id ='<?= $item->id ?>' data-toggle="modal" data-target="#FlyerDescription-<?= $item->id ?>">Edit</a>
                                        </td>  
                                    <?php
								} ?>

                                    <!-- Modal Offers Preview -->

                                <style>
                                    .modal-footer {
                                        border-top-color: rgb(226, 231, 235);
                                        padding: 14px 15px 15px;
                                        margin-top: 20px;
                                    }
                                </style>


                                <div class="modal fade" id="FlyerDescription-<?= $item->id ?>" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="lineModalLabel">Descrizione Annuncio</h3>
                                            </div>
                                            <div class="modal-body">
                                                <!-- content goes here -->

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5 class="titlecatege">Descrizione Originale (Questo testo non apparirà nel volantino)</h5>
                                                        <textarea readonly="true" rows="5" class=" form-control" data-id="<?= $item->id ?>" name="description"><?= $description ?></textarea>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <h5 class="titlecatege">Descrizione Volantino</h5>
                                                        <textarea id="input_short_description" maxlength="250" rows="5" class="short_description form-control" data-id="<?= $item->id ?>" name="short_description"><?= $short_description ?></textarea>
                                                    </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                                        <div class="btn-group" role="group">
                                                            <button type="button" id  class="btn btn-danger" data-dismiss="modal"  role="button">Chiudi</button>
                                                        </div>
                                                        <div class="btn-group" role="group">
                                                            <button type="button" data-id ='<?= $item->id ?>'  data-dismiss="modal" class="btn btn-success btn-hover-green savedata-close " data-action="save" name="save" role="button">Salva</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <td><input type="button"  class="delete_item" value="x" data-id = "<?= $item->id ?>"></td>
                                    </tr>
                                <?php
							} ?>
                        </table>



                    </div>
                </div>

            </div>

        <?php
		} ?>



    </div>



    <script>
        // basic usage
        $("#input_short_description, .short_description").maxlength();

        // disable enter button
        $('textarea').keypress(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });

    </script>


    <script>


        //        $('.savedata-close').on('click' , function(){
        //            var id = $(this).data('id');
        //            $('#FlyerDescription-'+id).hide();
        //        });


        function selectRE(id, row) {
            var content = row.prop('outerHTML');
            var btn = row.find('.add-button');
            if ($('.selected-real-estate').find('[data-id="' + id + '"]').length < 1) {
                btn.removeClass('add-button');
                btn.addClass('del-button');
                //                var count = $("#re_desc_"+id).val().length;
                //                alert(count);


                $('.table-header-selected').after(content);
                $('[data-id="' + id + '"]').prop('value', '-');
                row.remove();
                buttonEventdeselect(id);
                showHideButton();
            } else {
                row.remove();
            }

        }

        function deselectRE(id, row) {
            var content = row.prop('outerHTML');
            var btn = row.find('.del-button');
            if ($('.available-real-estate').find('[data-id="' + id + '"]').length < 1) {
                btn.removeClass('del-button');
                btn.addClass('add-button');
                $('.table-header-available').after(content);
                $('[data-id="' + id + '"]').prop('value', '+');
                row.remove();
                buttonEventselect(id);
                showHideButton();
            } else {
                row.remove();
            }
        }
        function buttonsEvent() {
            $('.add-button').click(function () {
                var selector = $(this).parents('tr');
                selectRE($(this).data('id'), selector);
            });
            $('.del-button').click(function () {
                var selector = $(this).parents('tr');
                deselectRE($(this).data('id'), selector);
            });
        }
        function buttonsEventAfterLoad() {
            $('#available-re').find('.add-button').click(function () {
                var selector = $(this).parents('tr');
                selectRE($(this).data('id'), selector);
            });
        }

        function buttonEventselect(id) {
            $(' [data-id="' + id + '"]').click(function () {
                var selector = $(this).parents('tr');
                selectRE($(this).data('id'), selector);
            });

        }
        function buttonEventdeselect(id) {
            $(' [data-id="' + id + '"]').click(function () {
                var selector = $(this).parents('tr');
                deselectRE($(this).data('id'), selector);
            });
        }

        function showHideButton() {
            var rowCount = $('.selected-real-estate tr').length;
            if (rowCount < 2) {
                $("button#add-selected").hide();
                $("tr#add-selected-tr").hide();
            } else {
                $("button#add-selected").show();
                $("tr#add-selected-tr").show();
            }
        }

        function loadRealEstates(select_value) {

            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Real_Estate/ajx_loadRealEstates') ?>",
                data: {'contract': select_value},
                success: function (data) {
                    $('#available-re').find('.re_element').remove();
                    $('.table-header-available').after(data);
                    buttonsEventAfterLoad();
                },
                error: function (e) {
                    console.log(e);
                }
            });

        }


        $('.delete_item').click(function () {
            var row = $(this).parents('tr');
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Real_Estate/ajx_deleteFlyerItem') ?>",
                data: {'item_id': $(this).data('id')},
                success: function () {
                    row.remove();
                },
                error: function (e) {
                    console.log(e);
                }
            });

        });


        $('.short_description').on('blur', function () {
            var id = $(this).data('id');
            if ($(this).val().length > 10 || $(this).val().length == '') {
                $(this).css({"border-color": "#00ff00",
                    "border-weight": "1px",
                    "border-style": "solid"});
                $.ajax({
                    type: "POST",
                    url: "<?= Utils::getComponentUrl('Real_Estate/ajx_updateShortDescription') ?>",
                    data: {'item_id': id, 'short_description': $(this).val()},
                    success: function (data) {
                        $('#addDescButton-' + id).removeClass('btn-success');
                        $('#addDescButton-' + id).addClass('btn-danger');
                        $('#addDescButton-' + id).text('Edit');
                        console.log(data);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });

            } else {
                $(this).css({"border-color": "red",
                    "border-weight": "1px",
                    "border-style": "solid"});

            }

        });




        $('.short_description').on('keyup', function () {

            var count = $(this).val().length;
            if (count < 10 || count > 250) {
                $(this).css({"border-color": "#ff0000",
                    "border-weight": "1px",
                    "border-style": "solid"});
            } else {
                $(this).css({"border-color": "#0000ff",
                    "border-weight": "1px",
                    "border-style": "solid"});
            }
        });



        function loadRealEstatesBySearch(search_value) {
            console.log(search_value);
            var select_value = $('#id_category').val();
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Real_Estate/ajx_loadRealEstates') ?>",
                data: {'query': search_value, 'contract': select_value},
                success: function (data) {
                    $('#available-re').find('.re_element').remove();
                    $('.table-header-available').after(data);
                    buttonsEventAfterLoad();
                },
                error: function (e) {
                    console.log(e);
                }
            });

        }

        buttonsEvent();
        showHideButton();

    </script>

    <?php
	}?>