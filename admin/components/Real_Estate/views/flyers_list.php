<?php

 
include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('RealEstateFlyers');

$url_flyer_edit = Utils::getComponentUrl('Real_Estate/editFlyer');
$url_flyers_list = Utils::getComponentUrl('Real_Estate/listFlyers');
$url_flyer_delete = Utils::getComponentUrl('Real_Estate/deleteFlyer');

foreach ($flyers as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-download"></i>', $url_flyer_pdf.'/'.$row->id, 'xs', '', '[$Edit]'),
		new Button('<i class="fa fa-pencil"></i>', $url_flyer_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
	);
}
$table->setElements($flyers);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Title]', 'text', 'left'),
	new TableList_Field('creation_date', '[$creation_date]', 'text', 'left'),
	new TableList_Field('last_modification', '[$last_modification]', 'text', 'left')
));
$table->setUrl_action($url_flyers_list);
$table->setUrl_delete($url_flyer_delete);
$table->render();
