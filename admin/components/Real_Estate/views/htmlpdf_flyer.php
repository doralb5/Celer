<?php

/*
 *
 * $flyer       =>>>>> Flyer generic informations
 * $flyer_items =>>>>> Real Estate of this Flyer
 *
 *  */
//var_dump($flyer_items);

define('RE_IMAGES_PATH', MEDIA_ROOT.'realestates/');

define('K_PATH_IMAGES', '');
define('PDF_HEADER_LOGO', Utils::genThumbnailUrl(CMSSettings::$logo, 400, 0, array(), true));
define('PDF_HEADER_LOGO_WIDTH', 50);
define('PDF_HEADER_TITLE', CMSSettings::$company_name);

define('PDF_HEADER_STRING', CMSSettings::$company_mail."\n".CMSSettings::$company_tel);

//define('PDF_MARGIN_LEFT', '3');
//define('PDF_MARGIN_RIGHT', '3');

require_once DOCROOT.LIBS_PATH.'tcpdf/tcpdf.php';
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('BlueHat CMS');
$pdf->SetAuthor('Blue Hat sh.p.k.');
$pdf->SetTitle(CMSSettings::$webdomain.' Flyer');
$pdf->SetSubject('RealEstate Flyer');
$pdf->SetKeywords('RealEstate, Flyer');
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

$pdf->SetPrintFooter(false);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(3, PDF_MARGIN_TOP, 3);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 2);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
// set font
$pdf->SetFont('helvetica', '', 8);

// add a page
$pdf->AddPage();

for ($i = 0; $i < count($flyer_items); $i++) {
	if ($i % 2 == 0) {
		$nextCol = 0;
	} else {
		$nextCol = 1;
	}
	
	// @var $item RealEstatePdfFlyerItems_Entity
	$item = $flyer_items[$i];
	
	// IMAGE BOX
	$img = Utils::thumb(DOCROOT.MEDIA_ROOT.'realestates/'.$item->image, array('w' => 120, 'h' => 90, 'zc' => 1));
	$pdf->image('@'.$img, '', '', 35);
	
	// PRICE BOX
	$pdf->SetFillColor(7, 93, 163);
	if ($item->energetic_class != '') {
		switch ($item->energetic_class) {
			case 'Y': $enclass = 'In Fase di Richiesta'; break;
			case 'X': $enclass = 'Esente'; break;
			default: $enclass = $item->energetic_class;
		}
		$energetic_class = "<span style=\"font-size: 10px; color:white; text-align: center;\">C.E.: {$enclass}</span>";
	} else {
		$energetic_class = '';
	}
	
	if ($item->ref_code != '') {
		$ref_code = "<span style=\"font-size: 10px; color:white; text-align: center; padding-top:15px;\">RIF. {$item->ref_code}</span>";
	} else {
		$ref_code = '<br>';
	}
	
	if ($item->hide_price == '1') {
		$price_value = 'Tratt. Ris.';
	} else {
		$price_value = '€ '.number_format($item->price, 0, ',', '.');
	}
	$price = '<br/><span style="text-align:center; font-size:18px; color: rgb(255, 255, 255); font-weight:bold;">'.$price_value.'</span><br>';
	$pdf->setCellPaddings(0, 0, 0, 0);
	$pdf->setCellMargins(0, 26, 0, 0);
	$pdf->writeHTMLCell(35, 14, '', '', $ref_code.$price.$energetic_class, 0, 0, 1, true, 'C');
	
	// DESCRIPTION BOX
	$pdf->SetFillColor(250, 250, 250);
	ob_start();
	include dirname(__FILE__).'/includes/pdf_item.php';
	$txt = ob_get_contents();
	ob_end_clean();
	if ($nextCol) {
		$pdf->setCellMargins(0, 0, 0, 2);
	} else {
		$pdf->setCellMargins(0, 0, 4, 2);
	}
	$pdf->writeHTMLCell(65, 40, '', '', $txt, 0, $nextCol, 1, true, 'L');
	
	if ((($i + 1) % 12 == 0 && $i != 0) || ($i == count($flyer_items) - 1)) {
		// Position at 25 mm from bottom
		$pdf->SetY(-12);
		// Set font
		$pdf->SetFont('helvetica', 'I', 8);
		$pdf->SetFillColor(7, 93, 163);
		$pdf->Cell(0, 0, CMSSettings::$webdomain.' - '.CMSSettings::$EMAIL_ADMIN.' - '.CMSSettings::$company_tel, 0, false, 'C', 0, '', 0, false, 'T', 'M');
		$pdf->Ln();
		$pdf->Cell(0, 0, CMSSettings::$company_address, 0, false, 'C', 0, '', 0, false, 'T', 'M');
		
		if ($i != count($flyer_items) - 1) {
			$pdf->addPage();
		}
	}
}

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

$pdf->Output(DOCROOT.MEDIA_ROOT.'pdf/'.$flyer->name.'_'.date('d-m-Y_H-i').'.pdf', 'I');
