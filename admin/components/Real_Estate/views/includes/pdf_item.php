<?php
$short_description = Utils::trim_text(strip_tags(str_replace('<br/>', "\n\r", $item->short_description)), 250, false);

if ($short_description == '') {
	$short_description = Utils::trim_text(strip_tags(str_replace('<br/>', "\n\r", $item->description)), 250);
}

?>



<table width="100%" border="0" margin="0" style="padding-left: 5px;">
    <tr>
        <td height="42" ><span><?= strtoupper($item->typology) ?> <br>IN <?= strtoupper($item->contract) ?></span>
            <br/><span style="color:rgb(100,100,100);"><?= ucwords($item->location) ?>, <?= ucwords($item->locality) ?></span>
        </td>
    </tr>

    <tr>
        <td height="87"><b><span style="color:rgb(7, 93, 163)"><?= $item->title ?></span></b>
            <br/>
            <span style="color:rgb(50,50,50); font-size: 7px;"><?= strtoupper($short_description); ?></span>
        </td>
    </tr>

    <tr bgcolor="rgb(7, 93, 163)" style="color:white;">
        <td height="5" >
            <?php if ($item->square_meter != '') {
	?> <span style=""><?= $item->square_meter ?>m²</span>  <?php
} ?>&nbsp; - &nbsp;<?php if ($item->rooms != '') {
		?>  <span> Locali: <?= $item->rooms ?></span> <?php
	} ?>

        </td>
    </tr>
</table>