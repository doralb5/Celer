<title>{SITENAME}</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<style>
    .body-main{
        margin: 0 auto; 

    }
    .body-info{
        background-color : #2e2e2e;
        color: #9A9A9A;
        padding: 5%;
    }

    button.btn {
        color: white;
        background: #c90000;
        display: inline-block;
        padding: 8px 14px;
        outline: none;
        cursor: pointer;
        text-decoration: none;
        text-align: center;
        white-space: nowrap;
        font-weight: bold;
        border-radius: 3px;
        border: 1px solid #820000;
        vertical-align: middle;
        box-shadow: 0px 1px 5px rgba(0,0,0,0.05);
        -webkit-transition: all 0.2s;
        -moz-transition: all 0.2s;
        transition: all 0.2s;
    }
    hr.mail-hr {
        color: #eaeaea;
        margin-bottom: 10px;
        border: 1px solid #424242;
    }
    a {
    color: white;
    text-decoration: none;
}
</style>
<!-- begin page body -->
<table class="body-main" style="background: white;">
    <tr>
        <td>
    <center>
        <!-- begin page header -->
        <table>
            <tr>
                <td>
            <center>
                <!-- begin container -->
                <table >
                    <tr>
                        <td>
                            <!-- begin six columns -->
                            <table>
                                <tr>
                                    <td>
                                        <a href="http://{URL_WEBSITE}"><img src="{URL_LOGO}"/></a>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                            <!-- end six columns -->
                        </td>

                    </tr>
                </table>
                <!-- end container -->
            </center>
            </td>
            </tr>
        </table>
        <!-- end page header -->

        <!-- begin page container -->
        <table>
            <tr>
                <td>
                    <!-- begin row -->
                    <table>
                        <tr>
                            <td>
                                <!-- begin twelve columns -->
                                <table>
                                    <tr>
                                        <td class="body-info">
                                            <h2>[$congratulations]<br/>[$your_realestate][$is_online]</h2>
                                            <table>
                                                <tr>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                           

                                            <table class="body-info">
                                                
                                                <tr>
                                                    <td>
                                                        [$username]:
                                                    </td>
                                                    <td>
                                                        <?= $user->username ?>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        [$password]:
                                                    </td>
                                                    <td>
                                                        <?= $user->plain_password ?>
                                                    </td>
                                                </tr>

                                                <br>
                                                <tr>
                                                    <td>
                                                        <a href="http://netirane.al/login" target="_blank"><button class="btn btn-md btn-default" >[$login]</button></a>
                                                    </td>
                                                    <td>
                                                        <a href="http://netirane.al/kontakt" target="_blank"><button class="btn btn-md btn-default">[$contact_us]</button></a>
                                                    </td>
                                                </tr>


                                            </table>


                                            <div>[$call_us] <span style="color: white; font-size: 18px; padding-left: 15px;"> 04 450 0371</span></div>
                                            
                                            <hr class="mail-hr">

                                            <a href="http://{URL_WEBSITE}">neTirane.al</a><br/>
                                            [$search_engine]

                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                                <!-- end twelve columns -->
                            </td>
                        </tr>
                    </table>
                    <!-- end row -->
                    <!-- begin divider -->
                    <table></table>
                    <!-- end divider -->
                </td>
            </tr>
        </table>
        <!-- end page container -->

        <!-- begin page footer -->
        <table>
            <tr>
                <td>
            <center>
                <!-- begin container -->
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="color:#000000;">
                                        &copy; {SITENAME} <?= date('Y') ?>.
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <!-- end container -->
            </center>
            </td>
            </tr>
        </table>
        <!-- end page footer -->
    </center>
</td>
</tr>
</table>

