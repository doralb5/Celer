
<?php
HeadHTML::AddStylesheet(substr(WEBROOT, 0, -6).COMMONS_PATH.'css/select2.min.css');
HeadHTML::AddStylesheet(substr(WEBROOT, 0, -6).COMMONS_PATH.'css/bootstrap-switch.min.css');
HeadHTML::AddJS(substr(WEBROOT, 0, -6).COMMONS_PATH.'js/select2.full.min.js');
HeadHTML::AddJS(substr(WEBROOT, 0, -6).COMMONS_PATH.'js/bootstrap-switch.min.js');

$title = isset($_POST['title']) ? $_POST['title'] : $realestate->title;
$ref_code = isset($_POST['ref_code']) ? $_POST['ref_code'] : $realestate->ref_code;
$rooms = isset($_POST['rooms']) ? $_POST['rooms'] : $realestate->rooms;
$square_meter = isset($_POST['square_meter']) ? $_POST['square_meter'] : $realestate->square_meter;
$bathrooms = isset($_POST['bathrooms']) ? $_POST['bathrooms'] : $realestate->bathrooms;
$garden = isset($_POST['garden']) ? $_POST['garden'] : $realestate->garden;
$balcony = isset($_POST['balcony']) ? $_POST['balcony'] : $realestate->balcony;
$furnished = isset($_POST['furnished']) ? $_POST['furnished'] : $realestate->furnished;
$terrace = isset($_POST['terrace']) ? $_POST['terrace'] : $realestate->terrace;
$description = isset($_POST['description']) ? $_POST['description'] : $realestate->description;
$id_contract = isset($_POST['id_contract']) ? $_POST['id_contract'] : $realestate->id_contract;
$id_typology = isset($_POST['id_typology']) ? $_POST['id_typology'] : $realestate->id_typology;
$id_floor = isset($_POST['id_floor']) ? $_POST['id_floor'] : $realestate->id_floor;
$id_heating = isset($_POST['id_heating']) ? $_POST['id_heating'] : $realestate->id_heating;
$id_building_state = isset($_POST['id_building']) ? $_POST['id_building'] : $realestate->id_building_state;
$id_currency = isset($_POST['id_currency']) ? $_POST['id_currency'] : $realestate->id_currency;
$publish_date = isset($_POST['date']) ? $_POST['date'] : ((!is_null($realestate->publish_date) ? $realestate->publish_date : date('Y-m-d H:i', time())));
//$expire_date = isset($_POST['expire_date']) ? $_POST['expire_date'] : ((!is_null($realestate->expire_date)) ? $realestate->expire_date : date('Y-m-d H:i', strtotime(date("Y-m-d  H:i", time()) . " + 365 day")));
$address = isset($_POST['address']) ? $_POST['address'] : $realestate->address;
$price = isset($_POST['price']) ? $_POST['price'] : $realestate->price;
$hide_price = isset($_POST['hide_price']) ? $_POST['hide_price'] : $realestate->hide_price;
$email = isset($_POST['email']) ? $_POST['email'] : $realestate->email;
($email == '') ? ((isset($_SESSION['user_auth']['email']) ? $email = $_SESSION['user_auth']['email'] : $email = $_SESSION['auth']['email'])) : $email;
$featured = isset($_POST['featured']) ? $_POST['featured'] : $realestate->featured;
$enabled = isset($_POST['enabled']) ? $_POST['enabled'] : $realestate->enabled;
$id_user = isset($_POST['id_user']) ? $_POST['id_user'] : $realestate->id_user;
$location = isset($_POST['location']) ? $_POST['location'] : $realestate->location;
$locality = isset($_POST['locality']) ? $_POST['locality'] : $realestate->locality;
$zip_code = isset($_POST['zip_code']) ? $_POST['zip_code'] : $realestate->zip_code;
$street = isset($_POST['street']) ? $_POST['street'] : $realestate->street;
$energetic_class = (isset($_POST['energetic_class'])) ? $energetic_class = $_POST['energetic_class'] : $realestate->energetic_class;

$coordinates = isset($_POST['coordinates']) ? ltrim(rtrim($_POST['coordinates'], ')'), '(') : $realestate->coordinates;
$currency = isset($realestate->currency) ? $realestate->currency : 'EUR';
?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= CMSSettings::$google_maps_api ?>&libraries=places&language=<?= CMSSettings::$default_lang ?>" ></script>
<div class="col-md-12">
    <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
          novalidate="" enctype="multipart/form-data" id="insert-business">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$GeneralInformation]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$Real_Estate_Title] *</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $title ?>"
                                       name="title" placeholder="[$Title]" data-parsley-required="true"
                                       data-parsley-id="6524" required>
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="ref_code">[$ref_code]</label>
                            <div class="col-md-8 col-sm-8">
                                <input id ="ref_code" class="form-control" type="text" value="<?= $ref_code ?>"
                                       name="ref_code" placeholder="[$ref_code]" 
                                       data-parsley-id="6524">
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Contract]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_contract" required>
                                    <?php foreach ($contract as $con) {
	?>
                                        <option
                                            value="<?= $con->id ?>" <?= ($con->id == $id_contract) ? 'selected' : '' ?>><?= $con->type ?></option>
                                        <?php
} ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Typology]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_typology" required>
                                    <?php foreach ($typology as $tp) {
		?>
                                        <option
                                            value="<?= $tp->id ?>" <?= ($tp->id == $id_typology) ? 'selected' : '' ?>><?= $tp->type ?></option>
                                        <?php
	} ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Price]</label>

                            <div class="col-md-8 col-sm-8">
                                <div class="input-group">
                                <input id="price_input" class="form-control" style="text-align:right;" type="text" value="<?= $price ?>"
                                       name="price" placeholder="[$Price]" onkeyup="this.value=addThousandsSeparator(this.value);">
                                 <span class="input-group-addon">€</span>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Hide_Price]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="hide_price" <?= ($hide_price == '1') ? 'checked' : '' ?>
                                       id="hprice-switch">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                            <div class="col-md-6 col-sm-6">
                                <input type="file" name="image">
                                <?php if ($realestate->image != '') {
		?>
                                    <p>
                                        <img
                                            src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'realestates/'.$realestate->image ?>"
                                            width="100px" alt="Image"/></p>
                                    <?php
	} ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Featured]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="featured" <?= ($featured == '1') ? 'checked' : '' ?>
                                       id="featured-switch">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$PublishDate]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control" name="date" id="datetimepicker1"
                                       placeholder="[$SelectDate]" value="<?= $publish_date ?>" required="">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Enabled]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="enabled" <?= ($enabled == '1') ? 'checked' : '' ?>
                                       id="enabled-switch">
                            </div>
                        </div>
                    </div>
                </div>


                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$Content]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <textarea class="ckeditor" id="editor1" name="description"><?= $description ?></textarea>
                    </div>
                </div>
                <!-- end panel -->

            </div>

            <div class="col-md-6">


                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$AdditionalInformation]</h4>
                    </div>
                    <div class="panel-body panel-form">

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Rooms]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="number"  min="0" max="20" step="1" value="<?= $rooms ?>"
                                       name="rooms" placeholder="[$Rooms]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$square_meter]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="number"  min="0" max="1000" step="1" value="<?= $square_meter ?>"
                                       name="square_meter" placeholder="[$square_meter]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Bathrooms]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="number"  min="0" step="1" value="<?= $bathrooms ?>"
                                       name="bathrooms" placeholder="[$bathrooms]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Garden]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="garden" <?= ($garden == '1') ? 'checked' : '' ?>
                                       id="garden-switch">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Furnished]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="furnished" <?= ($furnished == '1') ? 'checked' : '' ?>
                                       id="furnished-switch">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Balcony]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="balcony" <?= ($balcony == '1') ? 'checked' : '' ?>
                                       id="balcony-switch">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Terrace]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="terrace" <?= ($terrace == '1') ? 'checked' : '' ?>
                                       id="tarrace-switch">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Floor]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_floor" >
                                    <option value="<?= $id_floor ?>"></option>
                                    <?php foreach ($floor as $tp) {
		?>
                                        <option
                                            value="<?= $tp->id ?>" <?= ($tp->id == $id_floor) ? 'selected' : '' ?>><?= $tp->name ?></option>
                                        <?php
	} ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Building]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_building" >
                                    <option value="<?= $id_building_state ?>"></option>
                                    <?php foreach ($buildings as $tp) {
		?>
                                        <option
                                            value="<?= $tp->id ?>" <?= ($tp->id == $id_building_state) ? 'selected' : '' ?>><?= $tp->type ?></option>
                                        <?php
	} ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Heating]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_heating" >
                                    <option value="<?= $id_heating ?>"></option>
                                    <?php foreach ($heating as $tp) {
		?>
                                        <option
                                            value="<?= $tp->id ?>" <?= ($tp->id == $id_heating) ? 'selected' : '' ?>><?= $tp->name ?></option>
                                        <?php
	} ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$energetic_class]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="" name="energetic_class">
                                    <option <?php if ($energetic_class == 'Y') {
		echo 'selected';
	} ?> value="Y">[$requesting]</option>
                                    <option <?php if ($energetic_class == 'X') {
		echo 'selected';
	} ?> value="X">[$missing]</option>
                                    <option <?php if ($energetic_class == 'A') {
		echo 'selected';
	} ?> value="A">A</option>
                                    <option <?php if ($energetic_class == 'B') {
		echo 'selected';
	} ?> value="B">B</option>
                                    <option <?php if ($energetic_class == 'C') {
		echo 'selected';
	} ?> value="C">C</option>
                                    <option <?php if ($energetic_class == 'D') {
		echo 'selected';
	} ?> value="D">D</option>
                                    <option <?php if ($energetic_class == 'E') {
		echo 'selected';
	} ?> value="E">E</option>
                                    <option <?php if ($energetic_class == 'F') {
		echo 'selected';
	} ?> value="F">F</option>
                                    <option <?php if ($energetic_class == 'G') {
		echo 'selected';
	} ?> value="G">G</option>

                                </select>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- end panel -->

            </div>

        </div>


        <div class="row">

            <div class="col-md-6">

                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$address_information]</h4>
                        <h5 style="color:grey"><em>[$how_to_use_map]</em></h5>
                    </div>

                    <div class="form-group">

                        <label class="control-label col-md-4 col-sm-4">[$city]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $location ?>"
                                   name="location" placeholder="[$start_typing_city]" id="location"
                                   onblur="setMarker('Location');">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label col-md-4 col-sm-4">[$Locality]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $locality ?>"
                                   name="locality" placeholder="[$start_typing_locality]" id="locality"
                                   onblur="setMarker('Locality');">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label col-md-4 col-sm-4">[$Zip_Code]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $zip_code ?>"
                                   name="zip_code" placeholder="[$start_typing_zip_code]" >
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">[$street]</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $street ?>"
                                   name="street" placeholder="[$insert_street]" id="street"
                                   onblur="setMarker('Address');">
                        </div>
<!--                         <p class="control-label col-md-8 col-sm-8">[$coplete_street_and_click_search]</p>
                        <div class="form-group col-md-4">
                            <br>
                            <div class="btn btn-info" name="fake_button"=><i class="fa fa-plus"></i>&nbsp;&nbsp;[$search_on_map]</div>

                        </div>-->
                    </div>



                    <input class="form-control" type="hidden" value="<?= $address ?>" name="address" placeholder="[$Address]" id="address" >


                </div>
                <!-- end panel -->





            </div>





            <div class="col-md-6">




                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$Map]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div id="map" style="width: 100%; height: 350px;"></div>
                        <input type="hidden" id="coordinates" name="coordinates"
                               value="<?= ($coordinates != '') ? "($coordinates)" : '' ?>">
                    </div>
                </div>
                <!-- end panel -->
            </div>


        </div>
</div>






<div class="col-md-12">
    <p class="text-center">
        <button type="submit" name="save" class="btn btn-info">
            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
        </button>
    </p>
</div>
<p>&nbsp;</p>
</form>


<?php if (!is_null($realestate->id)) {
		?>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i
                                class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i
                                class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$AdditionalImages]</h4>
                </div>
                <div class="panel-body">

                    <p>&nbsp;</p>
                    <?php
					include(VIEWS_PATH.'TableList_class.php');
		$table = new TableListView('RealEstate', 'list');
		foreach ($additionalImages as &$row) {
			$row->rows_buttons = array(
							new Button('<i class="fa fa-download"></i>', $url_addtImage_download.'/'.$row->id, 'xs', '', '[$Download]')
						);

			$row->row_imagePath = substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'realestates'.DS.'additional';
		}

		$table->setElements($additionalImages);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
						new TableList_Field('id', '[$Id]', 'int', 'left'),
						new TableList_Field('image', '[$Filename]', 'string', 'left'),
						new TableList_Field('image', '[$Image]', 'image', 'left'),
					));
		$table->setUrl_delete($url_addtImage_delete);
		$table->setUrl_delete_params(array('id_realestate' => $realestate->id));
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->render(); ?>


                    <div class="col-md-12">
                        <form id="fileupload" method="POST" enctype="multipart/form-data">
                            <div class="form-group col-md-9">
                                <label>[$SelectFile]</label>

                                <div class="box">
                                    <input type="file" name="images[]" id="file-7" class="inputfile inputfile-6"
                                           data-multiple-caption="{count} files selected" multiple/>
                                    <label for="file-7"><span></span><strong>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                                 viewBox="0 0 20 17">
                                            <path
                                                d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                            </svg>
                                            [$Choose]&hellip;</strong></label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <br>
                                <button class="btn btn-info" name="AddImage" type="submit"><i class="fa fa-plus"></i>&nbsp;&nbsp;[$AddFile]
                                </button>

                            </div>
                        </form>
                    </div>
                    <!--<div class="col-md-12 note note-info">
                        <h4>Notes</h4>
                        <ul>
                            <li>The maximum file size for uploads is <strong>20 MB</strong></li>
                        </ul>
                    </div>-->
                </div>
            </div>

        </div>
    </div>

<?php
	} ?>


<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>



<?php
if ($coordinates != '' && strpos($coordinates, ',') !== false) {
	list($lat, $lng) = explode(',', $coordinates);
} else {
	$lat = (isset($ComponentSettings['map_lat'])) ? $ComponentSettings['map_lat'] : '38.900413';
	$lng = (isset($ComponentSettings['map_lng'])) ? $ComponentSettings['map_lng'] : '16.586658';
}
?>


<style>
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
    }

    .inputfile:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .box {
        padding: 0px;
    }
</style>


<script>

    $('#datetimepicker1').datetimepicker({
                    locale: '<?=CMSSettings::$default_lang?>'
                });



    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: {lat: <?= $lat ?>, lng: <?= $lng ?>}
    });

    $('#coordinates').val("(<?= $lat ?>, <?= $lng ?>)");
    var geocoder = new google.maps.Geocoder();
    var marker;

    function setMarker(destination) {

        if (marker != null)
            marker.setMap(null);

        var address = document.getElementById('location').value + ', ' + document.getElementById('street').value;
        geocodeAddress(address, geocoder, map, destination);
        if (document.getElementById('street').value != '')
            map.setZoom(17);
        else
            map.setZoom(8);

    }

    function geocodeAddress(address, geocoder, resultsMap, destination) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                });

                google.maps.event.addListener(marker, 'dragend', function () {
                    geocodePosition(marker.getPosition());
                });

                $('#coordinates').val(results[0].geometry.location);
                console.log($('#coordinates').val());
            } else {
                $("#warning").modal();
            }
        });
    }

    function geocodePosition(pos) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode
                ({
                    latLng: pos
                },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                $("#address").val(results[0].formatted_address);
                                $('#coordinates').val(results[0].geometry.location.toString());
                            } else {
                                $("#warning").modal();
                                //$("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
                            }
                        }
                );
    }

<?php if (isset($realestate->coordinates)) {
	?>
        geocodePosition(<?= $realestate->coordinates ?>);
<?php
} ?>
    var checkbusinesscount = 1, prevTarget;
    $('.modal').on('show.bs.modal', function (e) {
        if (typeof prevTarget == 'undefined' || (checkbusinesscount == 1 && e.target != prevTarget)) {
            prevTarget = e.target;
            checkbusinesscount++;
            e.prbusinessDefault();
            $(e.target).appendTo('body').modal('show');
        } else if (e.target == prevTarget && checkbusinesscount == 2) {
            checkbusinesscount--;
        }
    });




    $("#enabled-switch").bootstrapSwitch();
    $("#garden-switch").bootstrapSwitch();
    $("#furnished-switch").bootstrapSwitch();
    $("#balcony-switch").bootstrapSwitch();
    $("#tarrace-switch").bootstrapSwitch();
    $("#featured-switch").bootstrapSwitch();
    $("#hprice-switch").bootstrapSwitch();
    $('#insert-business').submit(function (evt) {
        var val = $('#category').val();
        var categ_id = $('#categories option').filter(function () {
            return this.value == val;
        }).data('id');

        if (categ_id !== null && categ_id != '') {
            $('#category-id').val(categ_id);
        } else {
            categ_id = $('#category-id').val();
        }

    });

    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
                labelVal = label.innerHTML;
        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();
            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });

    var input = document.getElementById('location');

<?php if (CMSSettings::$country != '') {
		?>
        var options = {
            types: ['(cities)'],
            componentRestrictions: {
                country: '<?= CMSSettings::$country ?>'
            }
        };
        var autocomplete = new google.maps.places.Autocomplete(input, options);
<?php
	} else {
		?>
        var options = {
            types: ['(cities)']
        };
        var autocomplete = new google.maps.places.Autocomplete(input, options);

<?php
	} ?>





function addThousandsSeparator(nStr) {
    nStr += '';
    var x = nStr.split(',');
    var x1 = x[0];
    x1 = x1.replace(/\./gi,"");
    var x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

$('#price_input').val(addThousandsSeparator($('#price_input').val()));


</script>

