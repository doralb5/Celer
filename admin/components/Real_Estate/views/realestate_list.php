<?php

 
include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('realestates');

foreach ($realestates as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_realestate_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_realestate_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_realestate_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
			//new Button('<i class="fa fa-eye"></i>', $url_business_details . '/' . $row->id, 'xs', '', '[$Details]'),
	);
	$row->row_imagePath = 'realestates';
	if ($row->featured) {
		$row_code = '<i style="color: #9FEE00" class="fa fa-star fa-2x"></i>';
	} else {
		$row_code = '<i class="fa fa-star-o fa-2x"></i>';
	}

	$row->additional_rows = array('<i class="fa fa-star-o"></i>' => $row_code);
}
$table->setElements($realestates);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);
$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('title', '[$Title]', 'text', 'left'),
	new TableList_Field('fullAddress', '[$Address]', 'text', 'left'),
	new TableList_Field('contract_type', '[$contract_type]', 'text', 'left'),
	new TableList_Field('square_meter', '(m²)', 'int', 'left'),
	new TableList_Field('price', '[$price]', 'curr', 'right'),
	new TableList_Field('enabled', '<i class="fa fa-square-o"></i>', 'bool', 'left', 'no-sort'),
	new TableList_Field('image', '[$Image]', 'image', 'left'),
));
$table->setUrl_action($url_realestates_list);
$table->setUrl_delete($url_realestate_delete);

$table->addSelectFilter($enabledSelectFil);
$table->render();
