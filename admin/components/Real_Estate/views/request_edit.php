
<?php
HeadHTML::AddStylesheet(substr(WEBROOT, 0, -6).COMMONS_PATH.'css/select2.min.css');
HeadHTML::AddStylesheet(substr(WEBROOT, 0, -6).COMMONS_PATH.'css/bootstrap-switch.min.css');
HeadHTML::AddJS(substr(WEBROOT, 0, -6).COMMONS_PATH.'js/select2.full.min.js');
HeadHTML::AddJS(substr(WEBROOT, 0, -6).COMMONS_PATH.'js/bootstrap-switch.min.js');

$firstname = isset($_POST['firstname']) ? $_POST['firstname'] : $request->firstname;
$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : $request->lastname;
$tel = isset($_POST['tel']) ? $_POST['tel'] : $request->tel;
$cel = isset($_POST['cel']) ? $_POST['cel'] : $request->cel;
$email = isset($_POST['email']) ? $_POST['email'] : $request->email;
$rooms = isset($_POST['rooms']) ? $_POST['rooms'] : $request->rooms;
$surface = isset($_POST['surface']) ? $_POST['surface'] : $request->surface;
$bathrooms = isset($_POST['bathrooms']) ? $_POST['bathrooms'] : $request->bathrooms;
$garden = isset($_POST['garden']) ? $_POST['garden'] : $request->garden;
$balcony = isset($_POST['balcony']) ? $_POST['balcony'] : $request->balcony;
$furnished = isset($_POST['furnished']) ? $_POST['furnished'] : $request->furnished;
$terrace = isset($_POST['terrace']) ? $_POST['terrace'] : $request->terrace;
$description = isset($_POST['description']) ? $_POST['description'] : $request->description;
$id_contract = isset($_POST['id_contract']) ? $_POST['id_contract'] : $request->id_contract;
$id_typology = isset($_POST['id_typology']) ? $_POST['id_typology'] : $request->id_typology;
$id_floor = isset($_POST['id_floor']) ? $_POST['id_floor'] : $request->id_floor;
$city = isset($_POST['city']) ? $_POST['city'] : $request->city;
$id_heating = isset($_POST['id_heating']) ? $_POST['id_heating'] : $request->id_heating;
$id_building = isset($_POST['id_building']) ? $_POST['id_building'] : $request->id_building;
$zip_code = isset($_POST['zip_code']) ? $_POST['zip_code'] : $request->zip_code;
$address = isset($_POST['address']) ? $_POST['address'] : $request->address;
$street = isset($_POST['street']) ? $_POST['street'] : $request->street;
$price = isset($_POST['price']) ? $_POST['price'] : $request->price;
$energetic_class = (isset($_POST['energetic_class'])) ? $energetic_class = $_POST['energetic_class'] : $request->energetic_class;
   
 ?>

<div class="col-md-12">
    <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
          novalidate="" enctype="multipart/form-data" id="insert-business">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$GeneralInformationUser]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$Firstname] *</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $firstname ?>"
                                       name="firstname" placeholder="[$Firstname]" data-parsley-required="true"
                                       data-parsley-id="6524" readonly="true">
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$Lastname] *</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $lastname ?>"
                                       name="lastname" placeholder="[$Lastname]" data-parsley-required="true"
                                       data-parsley-id="6524" readonly="true" >
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <?php  if ($request->tel != '') {
 	?>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$Telephono] *</label>
                                   <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $tel ?>"
                                       name="tel" placeholder="[$Tel]" data-parsley-required="true"
                                       data-parsley-id="6524" readonly="true" >
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <?
 }?>
                         <?php  if ($request->cel != '') {
 	?>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$Phone] *</label>
                                   <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text" value="<?= $cel ?>"
                                       name="cel" placeholder="[$Tel]" data-parsley-required="true"
                                       data-parsley-id="6524" readonly="true" >
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <?
 }?>
                        
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="name">[$Email] *</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="email" value="<?= $email ?>"
                                       name="email" placeholder="[$Email]" data-parsley-required="true"
                                       data-parsley-id="6524" readonly="true" >
                                <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>



                    </div>
                </div>


                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$Content]</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <textarea class="ckeditor" id="editor1" name="description"><?= $description ?></textarea>
                    </div>
                </div>
                <!-- end panel -->

            </div>

            <div class="col-md-6">


                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                               data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                               data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">[$AdditionalInformationReq]</h4>
                    </div>
                    <div class="panel-body panel-form">

                        
                        

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Contract]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_contract" required>
                                    <?php foreach ($contract as $con) {
 	?>
                                        <option
                                            value="<?= $con->id ?>" <?= ($con->id == $id_contract) ? 'selected' : '' ?>><?= $con->type ?></option>
                                        <?php
 } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Typology]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_typology" required>
                                    <?php foreach ($typologies as $tp) {
 	?>
                                        <option
                                            value="<?= $tp->id ?>" <?= ($tp->id == $id_typology) ? 'selected' : '' ?>><?= $tp->type ?></option>
                                        <?php
 } ?>
                                </select>
                            </div>
                        </div>




                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Price_Offer]</label>

                            <div class="col-md-8 col-sm-8">
                                <div class="input-group">
                                    <input id="price_input" class="form-control" style="text-align:right;" type="text" value="<?= $price ?>"
                                           name="price" placeholder="[$Price]" onkeyup="this.value = addThousandsSeparator(this.value);">
                                    <span class="input-group-addon">€</span>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Rooms]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="number"  min="0" max="20" step="1" value="<?= $rooms ?>"
                                       name="rooms" placeholder="[$Rooms]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$square_meter]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="number"  min="0" max="1000" step="1" value="<?= $surface ?>"
                                       name="surface" placeholder="[$surface]">
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$City]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text"  value="<?= $city ?>"
                                       name="city" placeholder="[$City]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Street]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text"   value="<?= $street ?>"
                                       name="street" placeholder="[$Street]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Address]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="text"   value="<?= $address ?>"
                                       name="address" placeholder="[$Address]">
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Zip_Code]</label>

                            <div class="col-md-8 col-sm-8">
                                <input class="form-control" type="number" value="<?= $zip_code ?>"
                                       name="zip_code" placeholder="[$Zip_Code]">
                            </div>
                        </div>




                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Furnished]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="furnished" <?= ($furnished == '1') ? 'checked' : '' ?>
                                       id="furnished-switch">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Balcony]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="balcony" <?= ($balcony == '1') ? 'checked' : '' ?>
                                       id="balcony-switch">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Terrace]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="checkbox" data-size="normal"
                                       data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                       data-label-width="30px"
                                       value="1" name="terrace" <?= ($terrace == '1') ? 'checked' : '' ?>
                                       id="tarrace-switch">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Floor]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_floor" >
                                    <option value="<?= $id_floor ?>"></option>
                                    <?php foreach ($floor as $tp) {
 	?>
                                        <option
                                            value="<?= $tp->id ?>" <?= ($tp->id == $id_floor) ? 'selected' : '' ?>><?= $tp->name ?></option>
                                        <?php
 } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Building]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_building" >
                                    <option value="<?= $id_building_state ?>"></option>
                                    <?php foreach ($buildings as $tp) {
 	?>
                                        <option
                                            value="<?= $tp->id ?>" <?= ($tp->id == $id_building_state) ? 'selected' : '' ?>><?= $tp->type ?></option>
                                        <?php
 } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$Heating]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="id_category" name="id_heating" >
                                    <option value="<?= $id_heating ?>"></option>
                                    <?php foreach ($heating as $tp) {
 	?>
                                        <option
                                            value="<?= $tp->id ?>" <?= ($tp->id == $id_heating) ? 'selected' : '' ?>><?= $tp->name ?></option>
                                        <?php
 } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 control-label">[$energetic_class]</label>

                            <div class="col-md-8 col-sm-8">
                                <select class="form-control" id="" name="energetic_class">
                                    <option <?php if ($energetic_class == 'Y') {
 	echo 'selected';
 } ?> value="Y">[$requesting]</option>
                                    <option <?php if ($energetic_class == 'X') {
 	echo 'selected';
 } ?> value="X">[$missing]</option>
                                    <option <?php if ($energetic_class == 'A') {
 	echo 'selected';
 } ?> value="A">A</option>
                                    <option <?php if ($energetic_class == 'B') {
 	echo 'selected';
 } ?> value="B">B</option>
                                    <option <?php if ($energetic_class == 'C') {
 	echo 'selected';
 } ?> value="C">C</option>
                                    <option <?php if ($energetic_class == 'D') {
 	echo 'selected';
 } ?> value="D">D</option>
                                    <option <?php if ($energetic_class == 'E') {
 	echo 'selected';
 } ?> value="E">E</option>
                                    <option <?php if ($energetic_class == 'F') {
 	echo 'selected';
 } ?> value="F">F</option>
                                    <option <?php if ($energetic_class == 'G') {
 	echo 'selected';
 } ?> value="G">G</option>

                                </select>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- end panel -->

            </div>

        </div>


      




        <div class="col-md-12">
            <p class="text-center">
                <button type="submit" name="save" class="btn btn-info">
                    <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                </button>
            </p>
        </div>
        <p>&nbsp;</p>
    </form>


   

    <?php
	HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
	HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
	?>

    <?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
    <?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
    <?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>



    <style>
        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .inputfile + label {
            cursor: pointer; /* "hand" cursor */
        }

        .inputfile:focus + label {
            outline: 1px dotted #000;
            outline: -webkit-focus-ring-color auto 5px;
        }

        .box {
            padding: 0px;
        }
    </style>


    <script>

        $('#datetimepicker1').datetimepicker({
            locale: '<?= CMSSettings::$default_lang ?>'
        });



        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: {lat: <?= $lat ?>, lng: <?= $lng ?>}
        });

        $('#coordinates').val("(<?= $lat ?>, <?= $lng ?>)");
        var geocoder = new google.maps.Geocoder();
        var marker;

        function setMarker(destination) {

            if (marker != null)
                marker.setMap(null);

            var address = document.getElementById('location').value + ', ' + document.getElementById('street').value;
            geocodeAddress(address, geocoder, map, destination);
            if (document.getElementById('street').value != '')
                map.setZoom(17);
            else
                map.setZoom(8);

        }

        function geocodeAddress(address, geocoder, resultsMap, destination) {
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    resultsMap.setCenter(results[0].geometry.location);
                    marker = new google.maps.Marker({
                        map: resultsMap,
                        position: results[0].geometry.location,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        geocodePosition(marker.getPosition());
                    });

                    $('#coordinates').val(results[0].geometry.location);
                    console.log($('#coordinates').val());
                } else {
                    $("#warning").modal();
                }
            });
        }

        function geocodePosition(pos) {
            geocoder = new google.maps.Geocoder();
            geocoder.geocode
                    ({
                        latLng: pos
                    },
                            function (results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    $("#address").val(results[0].formatted_address);
                                    $('#coordinates').val(results[0].geometry.location.toString());
                                } else {
                                    $("#warning").modal();
                                    //$("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
                                }
                            }
                    );
        }

<?php if (isset($request->coordinates)) {
		?>
            geocodePosition(<?= $request->coordinates ?>);
<?php
	} ?>
        var checkbusinesscount = 1, prevTarget;
        $('.modal').on('show.bs.modal', function (e) {
            if (typeof prevTarget == 'undefined' || (checkbusinesscount == 1 && e.target != prevTarget)) {
                prevTarget = e.target;
                checkbusinesscount++;
                e.prbusinessDefault();
                $(e.target).appendTo('body').modal('show');
            } else if (e.target == prevTarget && checkbusinesscount == 2) {
                checkbusinesscount--;
            }
        });




        $("#enabled-switch").bootstrapSwitch();
        $("#garden-switch").bootstrapSwitch();
        $("#furnished-switch").bootstrapSwitch();
        $("#balcony-switch").bootstrapSwitch();
        $("#tarrace-switch").bootstrapSwitch();
        $("#featured-switch").bootstrapSwitch();
        $("#hprice-switch").bootstrapSwitch();
        $('#insert-business').submit(function (evt) {
            var val = $('#category').val();
            var categ_id = $('#categories option').filter(function () {
                return this.value == val;
            }).data('id');

            if (categ_id !== null && categ_id != '') {
                $('#category-id').val(categ_id);
            } else {
                categ_id = $('#category-id').val();
            }

        });

        var inputs = document.querySelectorAll('.inputfile');
        Array.prototype.forEach.call(inputs, function (input) {
            var label = input.nextElementSibling,
                    labelVal = label.innerHTML;
            input.addEventListener('change', function (e) {
                var fileName = '';
                if (this.files && this.files.length > 1)
                    fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                else
                    fileName = e.target.value.split('\\').pop();
                if (fileName)
                    label.querySelector('span').innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });
        });

        var input = document.getElementById('location');

<?php if (CMSSettings::$country != '') {
		?>
            var options = {
                types: ['(cities)'],
                componentRestrictions: {
                    country: '<?= CMSSettings::$country ?>'
                }
            };
            var autocomplete = new google.maps.places.Autocomplete(input, options);
<?php
	} else {
		?>
            var options = {
                types: ['(cities)']
            };
            var autocomplete = new google.maps.places.Autocomplete(input, options);

<?php
	} ?>





        function addThousandsSeparator(nStr) {
            nStr += '';
            var x = nStr.split(',');
            var x1 = x[0];
            x1 = x1.replace(/\./gi, "");
            var x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }

        $('#price_input').val(addThousandsSeparator($('#price_input').val()));


    </script>

