<?php

 
include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('requests');

foreach ($requests as &$row) {
	$row->rows_buttons = array(
		$btn1 =
		new Button('<i class="fa fa-pencil"></i>', $url_request_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
			//new Button('<i class="fa fa-eye"></i>', $url_business_details . '/' . $row->id, 'xs', '', '[$Details]'),
	);
}
$table->setElements($requests);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);
$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('firstname', '[$Firstname]', 'text', 'left'),
	new TableList_Field('lastname', '[$Lastname]', 'text', 'left'),
   // new TableList_Field('id_contract', '[$contract_type]', 'text', 'left'),
	new TableList_Field('surface', '(m²)', 'int', 'left'),
	new TableList_Field('price', '[$price]', 'curr', 'right'),
));
$table->setUrl_action($url_request_list);
$table->setUrl_delete($url_request_delete);

//$table->addSelectFilter($enabledSelectFil);
$table->render();
