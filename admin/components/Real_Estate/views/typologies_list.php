<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('RealEstate');

foreach ($typologies as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_typology_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_typology_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_typologies_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
			//new Button('<i class="fa fa-eye"></i>', $url_business_details . '/' . $row->id, 'xs', '', '[$Details]'),
	);
}
$table->setElements($typologies);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('type', '[$Type]', 'text', 'left'),
	new TableList_Field('enabled', '<i class="fa fa-square-o"></i>', 'bool', 'left', 'no-sort'),
));
$table->setUrl_action($url_typologies_list);
$table->setUrl_delete($url_typology_delete);
//$table->addSelectFilter($enabledSelectFil);
$table->render();
