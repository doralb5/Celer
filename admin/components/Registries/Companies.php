<?php

class Companies_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Registries/Companies/company_edit/');

		$this->view->set('url_companies_list', $this->getActionUrl('companies_list'));
		$this->view->set('url_company_edit', $this->getActionUrl('company_edit'));
		$this->view->set('url_company_delete', $this->getActionUrl('company_delete'));
		$this->view->set('url_company_details', $this->getActionUrl('company_details'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function companies_list()
	{
		HeadHTML::setTitleTag('Companies'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$company = $this->model->getCompany($id);
				$company->deleted = 1;
				$res = $this->model->saveCompany($company);
				$this->LogsManager->registerLog('Company', 'delete', 'Company deleted with id : '.$company->id, $company->id);
			}

			if (!is_array($res)) {
				$this->view->AddNotice('Companies has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo(Utils::getComponentUrl('Registries/Companies/companies_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'Company.id', 'peso' => 100),
				array('field' => 'Company.name', 'peso' => 90),
				array('field' => 'Company.email', 'peso' => 80),
			);
			$companies = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$companies = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($companies as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCompany]', $this->getActionUrl('company_edit')));

		$this->view->BreadCrumb->addDir('[$Companies]', $this->getActionUrl('companies_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('companies', $companies);
		$this->view->setTitle('[$Companies]');
		$this->view->render('companies-list');
	}

	public function company_edit($id = null)
	{
		$this->view->BreadCrumb->addDir('[$Companies]', $this->getActionUrl('companies_list'));
		if (!is_null($id)) {
			$company = $this->model->getCompany($id);
			HeadHTML::setTitleTag('Edit Company - '.$company->name.' | '.CMS_Settings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditCompany]', $this->getActionUrl('company_edit')."/$id");
		} else {
			$company = new Company_Entity();
			HeadHTML::setTitleTag('New Company'.' | '.CMS_Settings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddCompany]', $this->getActionUrl('company_edit'));
		}

		if ($company === false) {
			$this->AddError("Company $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['code'] == '') ? $this->AddError('Code is required!') : '';
			($_POST['name'] == '') ? $this->AddError('Name is required!') : '';
			($_POST['email'] == '') ? $this->AddError('Email is required!') : '';
			($_POST['address'] == '') ? $this->AddError('Address is required!') : '';
			($_POST['city'] == '') ? $this->AddError('City is required!') : '';
			($_POST['state'] == '') ? $this->AddError('State is required!') : '';
			($_POST['zip'] == '') ? $this->AddError('ZIP is required!') : '';
			($_POST['country'] == '') ? $this->AddError('Country is required!') : '';
			($_POST['vat_number'] == '') ? $this->AddError('VAT number is required!') : '';
			($this->model->existCode($_POST['code'])) ? $this->AddError('Code exist!') : '';

			if (count($this->view->getErrors()) == 0) {
				$company->code = $_POST['code'];
				$company->name = $_POST['name'];
				$company->vat_number = $_POST['vat_number'];
				$company->email = $_POST['email'];
				$company->state = $_POST['state'];
				$company->country = $_POST['country'];
				$company->city = $_POST['city'];
				$company->zip = $_POST['zip'];
				$company->address = $_POST['address'];
				$company->phone = $_POST['phone'];
				$company->fax = $_POST['fax'];
				$company->website = $_POST['website'];
				$company->description = $_POST['description'];
				$company->annual_revenue = $_POST['annual_revenue'];
				$company->employees = $_POST['employees'];
				$company->id_user_assigned = $_POST['id_user_assigned'];
				$company->id_campaign = 0;
				$company->id_type = $_POST['id_type'];
				$company->id_category = $_POST['id_category'];
				$inserted_id = $this->model->saveCompany($company);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Company has been saved successfully.');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Company', 'insert', 'Company inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('company_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Company', 'update', 'Company updated with id : '.$id, $id);
					}
				} else {
					$this->AddError('Saving failed!');
				}
			}
		}

		$compTypes = $this->model->getCompanyTypes(50);
		$this->view->set('compTypes', $compTypes);

		$compCategories = $this->model->getCategories(50);
		$this->view->set('compCategories', $compCategories);

		$this->view->set('company', $company);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('companies_list')));
		$title = (!is_null($id)) ? '[$EditCompany]' : '[$NewCompany]';
		$this->view->setTitle($title);
		$this->view->render('company-edit');
	}

	public function company_details($id)
	{
		$company = $this->model->getCompany($id);
		$this->view->set('company', $company);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('companies_list')));
		$this->view->setTitle('[$CompanyDetails]');
		$this->view->render('company-details');
	}

	public function company_delete($id)
	{
		$company = $this->model->getCompany($id);
		$company->deleted = 1;
		$result = $this->model->saveCompany($company);
		if (!is_array($result)) {
			$this->LogsManager->registerLog('Company', 'delete', "Company deleted with id : $id and name : ".$company->name, $id);
			$this->AddNotice('Company deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('companies_list'));
	}

	public function ajx_CompaniesSelectionList()
	{
		if (isset($_POST['query'])) {
			$searchFields = array(
				array('field' => 'Company.id', 'peso' => 100),
				array('field' => 'Company.vat_number', 'peso' => 100),
				array('field' => 'Company.name', 'peso' => 90),
				array('field' => 'Company.email', 'peso' => 80),
			);
			$companies = $this->model->search($_POST['query'], $searchFields, "deleted = '0'", '', 100, 0);
		} else {
			$companies = $this->model->getList(20, 0, "deleted = '0'");
		}

		if (count($companies)) {
			require_once DOCROOT.WEBROOT.VIEWS_PATH.'SelectionList_class.php';
			$table = new SelectionList_Table('Companies');

			$table->setElements($companies);
			$table->setFields(array(
				new SelectionList_Field('id', $this->view->getTerm('Id'), 'int', 'left'),
				new SelectionList_Field('name', $this->view->getTerm('Name'), 'string', 'left'),
				new SelectionList_Field('email', $this->view->getTerm('Email'), 'string', 'left'),
			));
			$table->renderTable();
		} else {
			echo '<p class="text-center">No results</p>';
		}
	}

	public function ajx_getCompany($id)
	{
		if (!$this->checkRemotePermission('ajx_getCompany')) {
			$response['message'] = 'Permission denied!';
			$response['status'] = 0;
			echo json_encode($response);
			return;
		}
		$company = $this->model->getCompany($id);
		$company = Utils::ObjectToArray($company);
		header('Content-Type: application/json');
		echo json_encode(array('company' => $company));
	}
}
