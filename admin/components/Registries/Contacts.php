<?php

class Contacts_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Registries/Contacts/contact_edit/');

		$this->view->set('url_contacts_list', $this->getActionUrl('contacts_list'));
		$this->view->set('url_contact_edit', $this->getActionUrl('contact_edit'));
		$this->view->set('url_contact_details', $this->getActionUrl('contact_details'));
		$this->view->set('url_contact_delete', $this->getActionUrl('contact_delete'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function contacts_list()
	{
		HeadHTML::setTitleTag('Contacts'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $contact = $this->model->deleteContact($id);
				$this->LogsManager->registerLog('Contact', 'delete', 'Contact deleted with id : '.$contact->id, $contact->id);
			}

			if ($res) {
				$this->view->AddNotice('Contacts has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo(Utils::getComponentUrl('Registries/Contacts/contacts_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'Contact.id', 'peso' => 100),
				array('field' => 'Contact.code', 'peso' => 100),
				array('field' => 'Contact.firstname', 'peso' => 90),
				array('field' => 'Contact.lastname', 'peso' => 90),
			);
			$contacts = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$contacts = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($contacts as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddContact]', $this->getActionUrl('contact_edit')));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('contacts', $contacts);
		$this->view->setTitle('[$Contacts]');
		$this->view->render('contacts-list');
	}

	public function contact_edit($id = null)
	{
		if (!is_null($id)) {
			$contact = $this->model->getContact($id);
			HeadHTML::setTitleTag('Edit Contact - '.$contact->name.' | '.CMS_Settings::$website_title);
		} else {
			$contact = new Contact_Entity();
			HeadHTML::setTitleTag('New Contact'.' | '.CMS_Settings::$website_title);
		}

		if ($contact === false) {
			$this->AddError("Contact $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['code'] == '') ? $this->AddError('Code is required!') : '';
			($_POST['title'] == '') ? $this->AddError('Title is required!') : '';
			($_POST['firstname'] == '') ? $this->AddError('Firstname is required!') : '';
			($_POST['lastname'] == '') ? $this->AddError('Lastname is required!') : '';
			($_POST['email'] == '') ? $this->AddError('Email is required!') : '';
			($_POST['address'] == '') ? $this->AddError('Address is required!') : '';
			($_POST['city'] == '') ? $this->AddError('City is required!') : '';
			($_POST['state'] == '') ? $this->AddError('State is required!') : '';
			($_POST['zip'] == '') ? $this->AddError('ZIP is required!') : '';
			($_POST['country'] == '') ? $this->AddError('Country is required!') : '';
			($_POST['tax_code'] == '') ? $this->AddError('Tax Code is required!') : '';
			($this->model->existCode($_POST['code'])) ? $this->AddError('Code exist!') : '';

			if (count($this->view->getErrors()) == 0) {
				$contact->code = $_POST['code'];
				$contact->id_company = $_POST['id_company'];
				$contact->department = $_POST['department'];
				$contact->title = $_POST['title'];
				$contact->firstname = $_POST['firstname'];
				$contact->lastname = $_POST['lastname'];
				$contact->birthday = date('Y-m-d', strtotime($_POST['birthday']));
				$contact->tax_code = $_POST['tax_code'];
				$contact->state = $_POST['state'];
				$contact->country = $_POST['country'];
				$contact->city = $_POST['city'];
				$contact->zip = $_POST['zip'];
				$contact->address = $_POST['address'];
				$contact->phone = $_POST['phone'];
				$contact->mobile = $_POST['mobile'];
				$contact->fax = $_POST['fax'];
				$contact->website = $_POST['website'];
				$contact->email = $_POST['email'];
				$contact->description = $_POST['description'];
				$contact->id_user_assigned = $_POST['id_user_assigned'];
				$contact->id_status = $_POST['id_status'];
				$contact->status_description = $_POST['status_description'];
				$contact->opportunity_amount = $_POST['opportunity_amount'];
				$contact->id_source = $_POST['id_source'];
				$contact->source_description = $_POST['source_description'];
				$contact->referred_by = $_POST['referred_by'];
				$contact->id_campaign = 0;
				$inserted_id = $this->model->saveContact($contact);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Contact has been saved successfully.');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Contact', 'insert', 'Contact inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('contact_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Contact', 'update', 'Contact updated with id : '.$id, $id);
					}
				} else {
					$this->AddError('Saving failed!');
				}
			}
		}

		$leads_md = Loader::getModel('Leads', 'Registries');
		$leadSources = $leads_md->getLeadSources();
		$this->view->set('leadSources', $leadSources);

		$this->view->set('contact', $contact);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('contacts_list')));
		$title = (!is_null($id)) ? '[$EditContact]' : '[$NewContact]';
		$this->view->setTitle($title);
		$this->view->render('contact-edit');
	}

	public function contact_details($id)
	{
		$contact = $this->model->getContact($id);
		$this->view->set('contact', $contact);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('contacts_list')));
		$this->view->setTitle('[$ContactDetails]');
		$this->view->render('contact-details');
	}

	public function contact_delete($id)
	{
		$result = $this->model->deleteContact($id);
		if ($result) {
			$this->LogsManager->registerLog('Contact', 'delete', "Contact deleted with id : $id and name : ".$contact->name, $id);
			$this->AddNotice('Contact deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('contacts_list'));
	}

	public function ajx_ContactsSelectionList()
	{
		if (isset($_POST['query'])) {
			$searchFields = array(
				array('field' => 'Contact.id', 'peso' => 100),
				array('field' => 'Contact.tax_code', 'peso' => 100),
				array('field' => 'Contact.firstname', 'peso' => 90),
				array('field' => 'Contact.lastname', 'peso' => 90),
				array('field' => 'Contact.title', 'peso' => 70),
			);
			$contacts = $this->model->search($_POST['query'], $searchFields, '', '', 100, 0);
		} else {
			$contacts = $this->model->getList(20, 0, '');
		}

		if (count($contacts)) {
			require_once DOCROOT.WEBROOT.VIEWS_PATH.'SelectionList_class.php';
			$table = new SelectionList_Table('Contacts');

			$table->setElements($contacts);
			$table->setFields(array(
				new SelectionList_Field('id', $this->view->getTerm('Id'), 'int', 'left'),
				new SelectionList_Field('name', $this->view->getTerm('Name'), 'string', 'left'),
				new SelectionList_Field('email', $this->view->getTerm('Email'), 'string', 'left'),
				new SelectionList_Field('company_name', $this->view->getTerm('Company'), 'string', 'left'),
			));
			$table->renderTable();
		} else {
			echo '<p class="text-center">No results</p>';
		}
	}

	public function ajx_getContact($id)
	{
		$contact = $this->model->getContact($id);
		$contact = Utils::ObjectToArray($contact);
		echo json_encode(array('contact' => $contact));
	}
}
