<?php

class Leads_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('Registries/Leads/lead_edit/');

		$this->view->set('url_leads_list', $this->getActionUrl('leads_list'));
		$this->view->set('url_lead_edit', $this->getActionUrl('lead_edit'));
		$this->view->set('url_lead_details', $this->getActionUrl('lead_details'));
		$this->view->set('url_lead_delete', $this->getActionUrl('lead_delete'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function leads_list()
	{
		HeadHTML::setTitleTag('Leads'.' | '.CMS_Settings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $lead = $this->model->deleteLead($id);
				$this->LogsManager->registerLog('Lead', 'delete', 'Lead deleted with id : '.$lead->id, $lead->id);
			}

			if ($res) {
				$this->view->AddNotice('Leads has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo(Utils::getComponentUrl('Registries/Leads/leads_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'Lead.id', 'peso' => 100),
				array('field' => 'Lead.code', 'peso' => 100),
				array('field' => 'Lead.firstname', 'peso' => 90),
				array('field' => 'Lead.lastname', 'peso' => 90),
				array('field' => 'Lead.department', 'peso' => 80),
			);
			$leads = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$leads = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($leads as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddLead]', $this->getActionUrl('lead_edit')));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('leads', $leads);
		$this->view->setTitle('[$Leads]');
		$this->view->render('leads-list');
	}

	public function lead_edit($id = null)
	{
		if (!is_null($id)) {
			$lead = $this->model->getLead($id);
			HeadHTML::setTitleTag('Edit Lead - '.$lead->name.' | '.CMS_Settings::$website_title);
		} else {

			//Per rastin e duplikimit
			if (isset($_GET['id_duplicate'])) {
				$id_dup = $_GET['id_duplicate'];
				$lead = $this->model->getLead($id_dup);
			} else {
				$lead = new Lead_Entity();
			}

			HeadHTML::setTitleTag('New Lead'.' | '.CMS_Settings::$website_title);
		}

		if ($lead === false) {
			$this->AddError("Lead $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['code'] == '') ? $this->AddError('Code is required!') : '';
			($_POST['firstname'] == '') ? $this->AddError('Firstname is required!') : '';
			($_POST['lastname'] == '') ? $this->AddError('Lastname is required!') : '';
			($_POST['tax_code'] == '') ? $this->AddError('Tax Code is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				if (isset($_GET['id_duplicate'])) {
					$lead = new Lead_Entity();
				}

				$lead->code = $_POST['code'];
				$lead->id_company = $_POST['id_company'];
				$lead->department = $_POST['department'];
				$lead->title = $_POST['title'];
				$lead->firstname = $_POST['firstname'];
				$lead->lastname = $_POST['lastname'];
				$lead->birthday = date('Y-m-d', strtotime($_POST['birthday']));
				$lead->tax_code = $_POST['tax_code'];
				$lead->state = $_POST['state'];
				$lead->country = $_POST['country'];
				$lead->city = $_POST['city'];
				$lead->zip = $_POST['zip'];
				$lead->address = $_POST['address'];
				$lead->phone = $_POST['phone'];
				$lead->mobile = $_POST['mobile'];
				$lead->fax = $_POST['fax'];
				$lead->website = $_POST['website'];
				$lead->email = $_POST['email'];
				$lead->description = $_POST['description'];
				$lead->id_user_assigned = $_POST['id_user_assigned'];
				$lead->id_status = $_POST['id_status'];
				$lead->status_description = $_POST['status_description'];
				$lead->opportunity_amount = $_POST['opportunity_amount'];
				$lead->id_source = $_POST['id_source'];
				$lead->source_description = $_POST['source_description'];
				$lead->id_referred_by = $_POST['id_referred_by'];
				$lead->id_campaign = 0;
				$inserted_id = $this->model->saveLead($lead);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Lead has been saved successfully.');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Lead', 'insert', 'Lead inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('lead_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Lead', 'update', 'Lead updated with id : '.$id, $id);
					}
				} else {
					$this->AddError('Saving failed!');
				}
			}
		}

		$leadStatuses = $this->model->getLeadStatuses();
		$this->view->set('leadStatuses', $leadStatuses);

		$leadSources = $this->model->getLeadSources();
		$this->view->set('leadSources', $leadSources);

		$this->view->set('lead', $lead);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('leads_list')));
		$title = (!is_null($id)) ? '[$EditLead]' : '[$NewLead]';
		$this->view->setTitle($title);
		$this->view->render('lead-edit');
	}

	public function lead_details($id)
	{
		$lead = $this->model->getLead($id);
		$this->view->set('lead', $lead);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', $this->getActionUrl('leads_list')));
		$this->view->setTitle('[$LeadDetails]');
		$this->view->render('lead-details');
	}

	public function lead_delete($id)
	{
		$result = $this->model->deleteLead($id);
		if ($result) {
			$this->LogsManager->registerLog('Lead', 'delete', "Lead deleted with id : $id and name : ".$lead->name, $id);
			$this->AddNotice('Lead deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('leads_list'));
	}
}
