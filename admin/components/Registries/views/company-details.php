<div class="col-md-12 ui-sortable">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-5">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$CompanyDetails] - <span class=""><?= $company->name ?></span></h4>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered personalinfo">
                    <tbody>
                        <tr>
                            <td>[$Id]</td>
                            <td><b><?= $company->id ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Code]</td>
                            <td><b><?= $company->code ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Name]</td>
                            <td><b><?= $company->name ?></b></td>
                        </tr>
                        <tr>
                            <td>[$VatNumber]</td>
                            <td><b><?= $company->vat_number ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Country]</td>
                            <td><b><?= $company->country ?></b></td>
                        </tr>
                        <tr>
                            <td>[$State]</td>
                            <td><b><?= $company->state ?></b></td>
                        </tr>
                        <tr>
                            <td>[$City]</td>
                            <td><b><?= $company->city ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Zip]</td>
                            <td><b><?= $company->zip ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Address]</td>
                            <td><b><?= $company->address ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Phone]</td>
                            <td><b><?= $company->phone ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Fax]</td>
                            <td><b><?= $company->fax ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Website]</td>
                            <td><b><?= $company->website ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Email]</td>
                            <td><b><?= $company->email ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Description]</td>
                            <td><b><?= $company->description ?></b></td>
                        </tr>

                        <tr>
                            <td>[$AnnualRevenue]</td>
                            <td><b><?= $company->annual_revenue ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Employees]</td>
                            <td><b><?= $company->employees ?></b></td>
                        </tr>
                        <tr>
                            <td>[$CreationDate]</td>
                            <td><b><?= date('d/m/Y H:i', strtotime($company->creation_date)); ?></b></td>
                        </tr>
                        <tr>
                            <td>[$UserAssigned]</td>
                            <td><b><?= $company->user_fullname; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Type]</td>
                            <td><b><?= $company->type; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Category]</td>
                            <td><b><?= $company->category; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Campaign]</td>
                            <td><b><?= $company->id_campaign; ?></b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>