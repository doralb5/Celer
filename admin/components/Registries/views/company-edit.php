<?php
$code = isset($_POST['code']) ? $_POST['code'] : $company->code;
$name = isset($_POST['name']) ? $_POST['name'] : $company->name;
$vat_number = isset($_POST['vat_number']) ? $_POST['vat_number'] : $company->vat_number;
$website = isset($_POST['website']) ? $_POST['website'] : $company->website;
$email = isset($_POST['email']) ? $_POST['email'] : $company->email;
$phone = isset($_POST['phone']) ? $_POST['phone'] : $company->phone;
$fax = isset($_POST['fax']) ? $_POST['fax'] : $company->fax;
$address = isset($_POST['address']) ? $_POST['address'] : $company->address;
$city = isset($_POST['city']) ? $_POST['city'] : $company->city;
$state = isset($_POST['state']) ? $_POST['state'] : $company->state;
$zip = isset($_POST['zip']) ? $_POST['zip'] : $company->zip;
$country = isset($_POST['country']) ? $_POST['country'] : $company->country;
$description = isset($_POST['description']) ? $_POST['description'] : $company->description;
$id_type = isset($_POST['id_type']) ? $_POST['id_type'] : $company->id_type;
$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $company->id_category;
$id_campaign = isset($_POST['id_campaign']) ? $_POST['id_campaign'] : $company->id_campaign;
$annual_revenue = isset($_POST['annual_revenue']) ? $_POST['annual_revenue'] : $company->annual_revenue;
$employees = isset($_POST['employees']) ? $_POST['employees'] : $company->employees;

$id_user_assigned = isset($_POST['id_user_assigned']) ? $_POST['id_user_assigned'] : $company->id_user_assigned;
$user_fullname = isset($_POST['user_assigned']) ? $_POST['user_assigned'] : $company->user_fullname;
?>


<form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form" novalidate="">
    <div class="ui-sortable">
        <div class="col-md-6">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Overview</h4>
                </div>
                <div class="panel-body panel-form">

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="name">[$Code] * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" value="<?= $code ?>" id="name" name="code" placeholder="[$Code]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="name">[$Name] * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" value="<?= $name ?>" id="name" name="name" placeholder="[$Name]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="name">[$VatNumber] * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" value="<?= $vat_number ?>" id="name" name="vat_number" placeholder="[$VatNumber]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="website">[$Website] :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="url" value="<?= $website ?>" id="website" name="website" data-parsley-type="url" placeholder="http://" data-parsley-id="3806"><ul class="parsley-errors-list" id="parsley-id-3806"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="email">[$Email] * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" value="<?= $email ?>" id="email" name="email" data-parsley-type="email" placeholder="[$Email]" data-parsley-required="true" data-parsley-id="0818"><ul class="parsley-errors-list" id="parsley-id-0818"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="data-phone">[$Phone] :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" value="<?= $phone ?>" name="phone" id="data-phone" data-parsley-type="number" placeholder="(XXX) XXXX XXX" data-parsley-id="7580"><ul class="parsley-errors-list" id="parsley-id-7580"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">[$Fax]</label>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" name="fax" value="<?= $fax ?>" class="form-control" placeholder="[$Fax]">
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>

        <div class="col-md-6">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-2">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Billing Address</h4>
                </div>
                <div class="panel-body panel-form">

                    <div class="form-group">
                        <label class="col-md-3 control-label">[$Address]</label>
                        <div class="col-md-9">
                            <textarea name="address" class="form-control" placeholder="[$Address]" rows="4" required="" style="max-width:100%"><?= $address ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">[$City] * :</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" value="<?= $city ?>" name="city" placeholder="[$City]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">[$State] * :</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" value="<?= $state ?>" name="state" placeholder="[$State]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">[$ZIPCode] * :</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" value="<?= $zip ?>" name="zip" placeholder="[$ZIPCode]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">[$Country] * :</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" value="<?= $country ?>" name="country" placeholder="[$Country]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>

        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-validation-1"  data-sortable-id="ui-widget-3">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">More Information</h4>
                </div>
                <div class="panel-body panel-form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Description]</label>
                            <div class="col-md-9">
                                <textarea name="description" class="form-control" placeholder="[$Description]" rows="5" style="max-width:100%"><?= $description ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Type]</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_type">
                                    <?php foreach ($compTypes as $type) {
	?>
                                        <option value="<?= $type->id ?>" <?= ($type->id == $id_type) ? 'selected' : '' ?>><?= $type->type ?></option>
                                    <?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Category]</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_category">
                                    <?php foreach ($compCategories as $categ) {
		?>
                                        <option value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
                                    <?php
	} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$AnnualRevenue]</label>
                            <div class="col-md-9">
                                <input type="text" name="annual_revenue" value="<?= $annual_revenue ?>" class="form-control" placeholder="[$AnnualRevenue]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Employees]</label>
                            <div class="col-md-9">
                                <input type="text" name="employees" value="<?= $employees ?>" class="form-control" placeholder="[$Employees]">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Assignedto]</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input readonly name="user_assigned" id="user_assigned" type="text" class="form-control" placeholder="Select User" value="<?= $user_fullname ?>" required="">
                                    <input type="hidden" id="id_user_assigned" name="id_user_assigned" value="<?= $id_user_assigned ?>"/>
                                    <div class="input-group-btn">
                                        <a title="[$Select]" class="btn btn-primary" data-toggle="modal" href="#myModal_Users">...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Campaign]</label>
                            <div class="col-md-9">
                                <input readonly type="text" name="id_campaign" class="form-control" placeholder="Selection of the campaign with popup">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>

        <div class="col-md-12 text-center">
            <button type="submit" name="save" class="btn btn-info"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
        </div>
    </div>
</form>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php
require_once DOCROOT.WEBROOT.VIEWS_PATH.'SelectionList_class.php';
$selectList = new SelectionListView('Users');
$selectList->setTitle('Choose a user');
$selectList->setAjxUrl(Utils::getControllerUrl('Users/ajx_UsersSelectionList'));
$selectList->setDestinationField('fullname');
$selectList->setDestTextSelector('user_assigned');
$selectList->setDestIdSelector('id_user_assigned');
$selectList->render();
?>