<div class="col-md-12 ui-sortable">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-5">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$ContactDetails] - <span class=""><?= $contact->firstname ?> <?= $contact->lastname ?></span></h4>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered personalinfo">
                    <tbody>
                        <tr>
                            <td>[$Id]</td>
                            <td><b><?= $contact->id ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Code]</td>
                            <td><b><?= $contact->code ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Company]</td>
                            <td><b><?= $contact->company_name ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Department]</td>
                            <td><b><?= $contact->department ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Title]</td>
                            <td><b><?= $contact->title ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Firstname]</td>
                            <td><b><?= $contact->firstname ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Lastname]</td>
                            <td><b><?= $contact->lastname ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Birthday]</td>
                            <td><b><?= date('d/m/Y', strtotime($contact->birthday)) ?></b></td>
                        </tr>
                        <tr>
                            <td>[$TaxCode]</td>
                            <td><b><?= $contact->tax_code ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Country]</td>
                            <td><b><?= $contact->country ?></b></td>
                        </tr>
                        <tr>
                            <td>[$State]</td>
                            <td><b><?= $contact->state ?></b></td>
                        </tr>
                        <tr>
                            <td>[$City]</td>
                            <td><b><?= $contact->city ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Zip]</td>
                            <td><b><?= $contact->zip ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Address]</td>
                            <td><b><?= $contact->address ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Phone]</td>
                            <td><b><?= $contact->phone ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Mobile]</td>
                            <td><b><?= $contact->mobile ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Fax]</td>
                            <td><b><?= $contact->fax ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Website]</td>
                            <td><b><?= $contact->website ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Email]</td>
                            <td><b><?= $contact->email ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Description]</td>
                            <td><b><?= $contact->description ?></b></td>
                        </tr>
                        <tr>
                            <td>[$CreationDate]</td>
                            <td><b><?= date('d/m/Y H:i', strtotime($contact->creation_date)); ?></b></td>
                        </tr>
                        <tr>
                            <td>[$UserAssigned]</td>
                            <td><b><?= $contact->user_fullname; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Source]</td>
                            <td><b><?= $contact->source; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Campaign]</td>
                            <td><b><?= $contact->id_campaign; ?></b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>