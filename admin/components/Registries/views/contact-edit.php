<?php
$code = isset($_POST['code']) ? $_POST['code'] : $contact->code;
$id_company = isset($_POST['id_company']) ? $_POST['id_company'] : $contact->id_company;
$company_name = isset($_POST['company']) ? $_POST['company'] : $contact->company_name;
$department = isset($_POST['department']) ? $_POST['department'] : $contact->department;
$title = isset($_POST['title']) ? $_POST['title'] : $contact->title;
$firstname = isset($_POST['firstname']) ? $_POST['firstname'] : $contact->firstname;
$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : $contact->lastname;
$birthday = isset($_POST['birthday']) ? $_POST['birthday'] : $contact->birthday;
$tax_code = isset($_POST['tax_code']) ? $_POST['tax_code'] : $contact->tax_code;
$state = isset($_POST['state']) ? $_POST['state'] : $contact->state;
$country = isset($_POST['country']) ? $_POST['country'] : $contact->country;
$city = isset($_POST['city']) ? $_POST['city'] : $contact->city;
$zip = isset($_POST['zip']) ? $_POST['zip'] : $contact->zip;
$address = isset($_POST['address']) ? $_POST['address'] : $contact->address;
$phone = isset($_POST['phone']) ? $_POST['phone'] : $contact->phone;
$mobile = isset($_POST['mobile']) ? $_POST['mobile'] : $contact->mobile;
$fax = isset($_POST['fax']) ? $_POST['fax'] : $contact->fax;
$website = isset($_POST['website']) ? $_POST['website'] : $contact->website;
$email = isset($_POST['email']) ? $_POST['email'] : $contact->email;
$description = isset($_POST['description']) ? $_POST['description'] : $contact->description;
$id_source = isset($_POST['id_source']) ? $_POST['id_source'] : $contact->id_source;
$id_campaign = isset($_POST['id_campaign']) ? $_POST['id_campaign'] : $contact->id_campaign;

$id_user_assigned = isset($_POST['id_user_assigned']) ? $_POST['id_user_assigned'] : $contact->id_user_assigned;
$user_fullname = isset($_POST['user_assigned']) ? $_POST['user_assigned'] : $contact->user_fullname;
?>

<form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form" novalidate="">
    <div class="ui-sortable">
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Overview</h4>
                </div>
                <div class="panel-body panel-form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Code] * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="text" value="<?= $code ?>" name="code" placeholder="[$Code]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Firstname] * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="text" value="<?= $firstname ?>" name="firstname" placeholder="[$Firstname]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Lastname] * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="text" value="<?= $lastname ?>" name="lastname" placeholder="[$Lastname]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Birthday]</label>
                            <div class="col-md-6 col-sm-6">
                                <input type="text" class="form-control" name="birthday" id="datepicker-default" placeholder="[$SelectDate]" value="<?= $birthday ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$TaxCode] * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="text" value="<?= $tax_code ?>" name="tax_code" placeholder="[$TaxCode]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Title] :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="text" value="<?= $title ?>" name="title" placeholder="[$Title]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Department] * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="text" value="<?= $department ?>" name="department" placeholder="[$Department]">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 no-padding">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Company]</label>
                            <div class="col-md-6 col-sm-6">
                                <div class="input-group">
                                    <input readonly name="company" id="company" type="text" class="form-control" placeholder="Select Company" value="<?= $company_name ?>" required="">
                                    <input type="hidden" id="id_company" name="id_company" value="<?= $id_company ?>"/>
                                    <div class="input-group-btn">
                                        <a title="[$Select]" class="btn btn-primary" data-toggle="modal" href="#myModal_Companies">...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="email">[$Email] * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="text" value="<?= $email ?>" id="email" name="email" data-parsley-type="email" placeholder="[$Email]" data-parsley-required="true" data-parsley-id="0818"><ul class="parsley-errors-list" id="parsley-id-0818"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="data-phone">[$Phone] :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="text" value="<?= $phone ?>" name="phone" id="data-phone" data-parsley-type="number" placeholder="(XXX) XXXX XXX" data-parsley-id="7580"><ul class="parsley-errors-list" id="parsley-id-7580"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="data-phone">[$Mobile] :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="text" value="<?= $mobile ?>" name="mobile" id="data-phone" data-parsley-type="number" placeholder="(XXX) XXXX XXX" data-parsley-id="7580"><ul class="parsley-errors-list" id="parsley-id-7580"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$Fax]</label>
                            <div class="col-md-6 col-sm-6">
                                <input type="text" name="fax" value="<?= $fax ?>" class="form-control" placeholder="[$Fax]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="website">[$Website] :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" type="url" value="<?= $website ?>" id="website" name="website" data-parsley-type="url" placeholder="http://" data-parsley-id="3806"><ul class="parsley-errors-list" id="parsley-id-3806"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>

        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-5">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Primary Address</h4>
                </div>
                <div class="panel-body panel-form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Address]</label>
                            <div class="col-md-9">
                                <textarea name="address" class="form-control" placeholder="[$Address]" rows="4" required="" style="max-width:100%"><?= $address ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">[$City] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="<?= $city ?>" name="city" placeholder="[$City]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">[$State] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="<?= $state ?>" name="state" placeholder="[$State]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">[$ZIPCode] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="<?= $zip ?>" name="zip" placeholder="[$ZIPCode]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">[$Country] * :</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="<?= $country ?>" name="country" placeholder="[$Country]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>


        <div class="col-md-12">
            <div class="panel panel-inverse" data-sortable-id="form-validation-1"  data-sortable-id="ui-widget-3">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">More Information</h4>
                </div>
                <div class="panel-body panel-form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Description]</label>
                            <div class="col-md-9">
                                <textarea name="description" class="form-control" placeholder="[$Description]" rows="5" style="max-width:100%"><?= $description ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Assignedto]</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input readonly id="user_assigned" name="user_assigned" type="text" class="form-control" placeholder="Select User" value="<?= $user_fullname ?>" required="">
                                    <input type="hidden" id="id_user_assigned" name="id_user_assigned" value="<?= $id_user_assigned ?>"/>
                                    <div class="input-group-btn">
                                        <a title="[$Select]" class="btn btn-primary" data-toggle="modal" href="#myModal_Users">...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-3 control-label">[$LeadSource]</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_source">
                                    <?php foreach ($leadSources as $source) {
	?>
                                        <option value="<?= $source->id ?>" <?= ($source->id == $id_source) ? 'selected' : '' ?>><?= $source->source ?></option>
                                    <?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Campaign]</label>
                            <div class="col-md-9">
                                <input disabled type="text" name="id_campaign" class="form-control" placeholder="Selection of the campaign with popup">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 text-center">
            <button type="submit" name="save" class="btn btn-info"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
        </div>
    </div>
</form>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/bootstrap-datepicker/css/datepicker.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/js/form-plugins.demo.min.js'); ?>
<?php HeadHTML::addScript('handleDatepicker();') ?>

<?php
require_once DOCROOT.WEBROOT.VIEWS_PATH.'SelectionList_class.php';
$selectList1 = new SelectionListView('Companies');
$selectList1->setTitle('Choose a company');
$selectList1->setAjxUrl(Utils::getComponentUrl('Registries/Companies/ajx_CompaniesSelectionList'));
$selectList1->setDestinationField('name');
$selectList1->setDestTextSelector('company');
$selectList1->setDestIdSelector('id_company');
$selectList1->render();

$selectList2 = new SelectionListView('Users');
$selectList2->setTitle('Choose a user');
$selectList2->setAjxUrl(Utils::getControllerUrl('Users/ajx_UsersSelectionList'));
$selectList2->setDestinationField('fullname');
$selectList2->setDestTextSelector('user_assigned');
$selectList2->setDestIdSelector('id_user_assigned');
$selectList2->render();
?>