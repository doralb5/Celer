<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Contacts');

foreach ($contacts as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_contact_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		new Button('<i class="fa fa-eye"></i>', $url_contact_details.'/'.$row->id, 'xs', '', '[$Details]'),
	);
}
$table->setElements($contacts);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('email', '[$Email]', 'string', 'left'),
	new TableList_Field('company_name', '[$Company]', 'string', 'left'),
	new TableList_Field('creation_date', '[$CreationDate]', 'date', 'left'),
));
$table->setUrl_action($url_contacts_list);
$table->setUrl_delete($url_contact_delete);

$table->render();
