<div class="col-md-12 ui-sortable">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-5">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$LeadDetails] - <span class=""><?= $lead->firstname ?> <?= $lead->lastname ?></span></h4>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered personalinfo">
                    <tbody>
                        <tr>
                            <td>[$Id]</td>
                            <td><b><?= $lead->id ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Code]</td>
                            <td><b><?= $lead->code ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Company]</td>
                            <td><b><?= $lead->company_name ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Department]</td>
                            <td><b><?= $lead->department ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Title]</td>
                            <td><b><?= $lead->title ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Firstname]</td>
                            <td><b><?= $lead->firstname ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Lastname]</td>
                            <td><b><?= $lead->lastname ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Birthday]</td>
                            <td><b><?= date('d/m/Y', strtotime($lead->birthday)) ?></b></td>
                        </tr>
                        <tr>
                            <td>[$TaxCode]</td>
                            <td><b><?= $lead->tax_code ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Country]</td>
                            <td><b><?= $lead->country ?></b></td>
                        </tr>
                        <tr>
                            <td>[$State]</td>
                            <td><b><?= $lead->state ?></b></td>
                        </tr>
                        <tr>
                            <td>[$City]</td>
                            <td><b><?= $lead->city ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Zip]</td>
                            <td><b><?= $lead->zip ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Address]</td>
                            <td><b><?= $lead->address ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Phone]</td>
                            <td><b><?= $lead->phone ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Mobile]</td>
                            <td><b><?= $lead->mobile ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Fax]</td>
                            <td><b><?= $lead->fax ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Website]</td>
                            <td><b><?= $lead->website ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Email]</td>
                            <td><b><?= $lead->email ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Description]</td>
                            <td><b><?= $lead->description ?></b></td>
                        </tr>
                        <tr>
                            <td>[$CreationDate]</td>
                            <td><b><?= date('d/m/Y H:i', strtotime($lead->creation_date)); ?></b></td>
                        </tr>
                        <tr>
                            <td>[$UserAssigned]</td>
                            <td><b><?= $lead->user_fullname; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Status]</td>
                            <td><b><?= $lead->status; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$StatusDescription]</td>
                            <td><b><?= $lead->status_description; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$OpportunityAmount]</td>
                            <td><b><?= $lead->opportunity_amount; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Source]</td>
                            <td><b><?= $lead->source; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$SourceDescription]</td>
                            <td><b><?= $lead->source_description; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$ReferredBy]</td>
                            <td><b><?= $lead->referred_by; ?></b></td>
                        </tr>
                        <tr>
                            <td>[$Campaign]</td>
                            <td><b><?= $lead->id_campaign; ?></b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>