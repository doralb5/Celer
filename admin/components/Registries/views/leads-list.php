<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Leads');

foreach ($leads as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_lead_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		new Button('<i class="fa fa-clone"></i>', $url_lead_edit.'?id_duplicate='.$row->id, 'xs', '', '[$Duplicate]'),
		new Button('<i class="fa fa-eye"></i>', $url_lead_details.'/'.$row->id, 'xs', '', '[$Details]'),
	);
}
$table->setElements($leads);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('email', '[$Email]', 'string', 'left'),
	new TableList_Field('company_name', '[$Company]', 'string', 'left'),
	new TableList_Field('creation_date', '[$CreationDate]', 'date', 'left'),
));
$table->setUrl_action($url_leads_list);
$table->setUrl_delete($url_lead_delete);

$table->render();
