<?php

class RemoteUsers_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('NuvolaDoc/Doc_Categories/category_edit/');

		$this->view->set('url_rusers_list', Utils::getComponentUrl('RemoteUsers/users_list'));
		$this->view->set('url_ruser_edit', Utils::getComponentUrl('RemoteUsers/edit_user'));
		$this->view->set('url_ruser_delete', Utils::getComponentUrl('RemoteUsers/delete_user'));
		$this->view->set('url_ruser_enable', Utils::getComponentUrl('RemoteUsers/enable_user'));
		$this->view->set('url_ruser_disable', Utils::getComponentUrl('RemoteUsers/disable_user'));

		$this->view->set('url_permission_delete', Utils::getComponentUrl('RemoteUsers/delete_permission'));
	}

	public function users_list()
	{
		$this->checkPermission();
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$elements_per_page = 100;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'id', 'peso' => 100),
				array('field' => 'username', 'peso' => 100),
			);
			$users = $this->model->search($_REQUEST['query'], $searchFields, "deleted = '0'", $sorting, $elements_per_page, $offset);
		} else {
			$users = $this->model->getList($elements_per_page, ($page - 1) * $elements_per_page);
		}

		$totalElements = $this->model->getLastCounter();

		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);
		$this->view->addButton(new Button('<i class="fa fa-plus"></i>&nbsp;&nbsp;[$AddRemoteUser]', Utils::getComponentUrl('RemoteUsers/edit_user')));
		$this->view->setTitle('[$RemoteUsers]');
		$this->view->set('users', $users);
		$this->view->render('rusers-list');
	}

	public function edit_user($id = null)
	{
		$this->checkPermission();
		if (!is_null($id)) {
			$user = $this->model->getRemoteUser($id);
			HeadHTML::setTitleTag('Edit Remote User'.' | '.CMS_Settings::$website_title);
			$this->view->setTitle('[$EditRemoteUser]');
		} else {
			$user = new RemoteUser_Entity();
			HeadHTML::setTitleTag('Add Remote User'.' | '.CMS_Settings::$website_title);
			$this->view->setTitle('[$AddRemoteUser]');
		}

		if (isset($_POST['save'])) {
			($_POST['username'] == '') ? $this->view->AddError('Username is required!') : '';
			(strlen($_POST['username']) > 20) ? $this->view->AddError('Username too long') : '';
			($this->model->existUsername($_POST['username'])) ? $this->view->AddError('Username exist!') : '';
			if (is_null($id)) {
				($_POST['password'] == '') ? $this->view->AddError('Password is required!') : '';
				($_POST['password'] != $_POST['rpassword']) ? $this->view->AddError("Passwords don't match!") : '';
				(strlen($_POST['password']) < 6 || strlen($_POST['password']) > 20) ? $this->view->AddError('Password must be between 6 and 20 characters!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				$user->username = $_POST['username'];
				if (is_null($id)) {
					$user->password = $_POST['password'];
				}
				$res = $this->model->saveRemoteUser($user);
				if (!is_array($res)) {
					$this->view->AddNotice('Remote user has been saved successfully!');

					if (!is_null($id)) {
						Utils::RedirectTo($this->getActionUrl('edit_user')."/$id");
					} else {
						Utils::RedirectTo($this->getActionUrl('edit_user')."/$res");
					}
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
		}

		if (isset($_POST['add'])) {
			($_POST['id_component'] == '') ? $this->view->AddError('Component is required!') : '';
			(!isset($_POST['action']) || $_POST['action'] == '') ? $this->view->AddError('Action is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$uPerm = new RemoteUserPermission_Entity();

				$uPerm->id_user = $id;
				$uPerm->id_component = $_POST['id_component'];
				$uPerm->action = $_POST['action'];
				$res = $this->model->saveUserPermission($uPerm);

				if (!is_array($res)) {
					$this->view->AddNotice('Permission has been added successfully!');
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
			Utils::RedirectTo($this->getActionUrl('edit_user')."/$id");
		}

		if (!is_null($id)) {
			$permissions = $this->model->getRemoteUserPerms($id);
			$this->view->set('permissions', $permissions);
		}

		$comp_md = Loader::getModel('Components');
		$components = $comp_md->getList(50, 0, "enabled = '1'");

		$this->view->set('components', $components);
		$this->view->set('user', $user);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$back]', Utils::getComponentUrl('RemoteUsers/users_list'), '', '', '[$back]'));
		$this->view->set('user', $user);
		$this->view->render('user-edit');
	}

	public function delete_user($id)
	{
		$this->checkPermission();
		if (!is_null($id)) {
			$user = $this->model->getRemoteUser($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
		}

		$res = $this->model->deleteRemoteUser($id);

		if ($res !== false) {
			$this->view->AddNotice('Remote User has been deleted successfully.');
			$this->LogsManager->registerLog('RemoteUser', 'delete', 'Remote User deleted with id : '.$user->id, $user->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::RedirectTo(Utils::getComponentUrl('RemoteUsers/users_list'));
	}

	public function enable_user($id)
	{
		$this->checkPermission();
		if (!is_null($id)) {
			$user = $this->model->getRemoteUser($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$user->enabled = 1;
		$res = $this->model->saveRemoteUser($user);

		if (!is_array($res)) {
			$this->view->AddNotice('Remote User has been enabled successfully.');
			$this->LogsManager->registerLog('RemoteUser', 'update', 'Remote User enabled with id : '.$user->id, $user->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::RedirectTo(Utils::getComponentUrl('RemoteUsers/users_list'));
	}

	public function disable_user($id)
	{
		$this->checkPermission();
		if (!is_null($id)) {
			$user = $this->model->getRemoteUser($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$user->enabled = 0;
		$res = $this->model->saveRemoteUser($user);

		if (!is_array($res)) {
			$this->view->AddNotice('Remote User has been disabled successfully.');
			$this->LogsManager->registerLog('RemoteUser', 'update', 'Remote User disabled with id : '.$user->id, $user->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::RedirectTo(Utils::getComponentUrl('RemoteUsers/users_list'));
	}

	public function delete_permission($id)
	{
		$this->checkPermission();
		$res = $this->model->deleteUserPermission($id);

		if ($res !== false) {
			$this->view->AddNotice('Permission has been deleted successfully.');
			$this->LogsManager->registerLog('RemoteUserPermission', 'delete', 'Remote User Permission deleted with id : '.$id, $id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::RedirectTo(Utils::getComponentUrl('RemoteUsers/edit_user/'.$_GET['id_user']));
	}

	private function checkPermission()
	{
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
		if (!UserAuth::checkComponentPerms('RemoteUsers')) {
			Utils::RedirectTo(WEBROOT.'Dashboard');
		}
	}

	public function ajx_getCompActions($comp_id)
	{
		$comp_md = Loader::getModel('Components');
		$component = $comp_md->getComponent($comp_id);
		$comp_name = $component->name;
		if ((strpos($comp_name, '/') !== false)) {
			list($package, $cName) = explode('/', $comp_name);
		} else {
			$package = $comp_name;
			$cName = $comp_name;
		}

		if ($component->type == 0) {
			require_once COMPONENTS_PATH.$package.'/'.$cName.'.php';
			$cName = $cName.'_Component';
		} else {
			require_once CONTROLLERS_PATH.$package.'/'.$cName.'.php';
		}
		$Class = new $cName();

		$refl = new ReflectionClass($cName);
		$methods = array();
		foreach ($refl->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
			if ($method->class == $refl->getName() && $method->name != '__construct' && Utils::StartsWith($method->name, 'ajx_')) {
				array_push($methods, $method->name);
			}
		}

		$options = array();
		foreach ($methods as $meth) {
			array_push($options, array('text' => $meth, 'value' => $meth));
		}
		echo json_encode(array('options' => $options));
	}
}
