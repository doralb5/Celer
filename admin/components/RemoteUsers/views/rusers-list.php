<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('RemoteUsers');

foreach ($users as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_ruser_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_ruser_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button("<i class='fa fa-pencil'></i>", $url_ruser_edit.'/'.$row->id, 'xs', 'btn-xs btn-info', '[$Edit]'),
	);
}

$table->setElements($users);
$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('username', '[$Username]', 'string', 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
));
$table->setUrl_action($url_rusers_list);
$table->setUrl_delete($url_ruser_delete);
$table->multipleDeletion(false);
$table->render();
