<?php
$username = (isset($_POST['username'])) ? $_POST['username'] : $user->username;
?>

<div class="ui-sortable">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$RemoteUser]</h4>
            </div>
            <div class="panel-body panel-form">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form" novalidate="">
                    <div class="col-md-offset-6 col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3" for="name">[$Username]*</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="<?= $username ?>" name="username" placeholder="[$Username]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            </div>
                        </div>
                        <?php if (is_null($user->id)) {
	?>
                            <div class="form-group">
                                <label class="control-label col-md-3">[$Password]*</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="password" name="password" placeholder="[$Password]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">[$RepeatPassword]*</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="password" name="rpassword" placeholder="[$RepeatPassword]" data-parsley-required="true" data-parsley-id="6524"><ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                                </div>
                            </div>
                        <?php
} ?>
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <button type="submit" name="save" class="btn btn-info">
                                    <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>

<?php if (!is_null($user->id)) {
		?>

    <div class="ui-sortable">
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-validation-1" data-sortable-id="ui-widget-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$Permissions]</h4>
                </div>
                <div class="panel-body">
                    <?php
					include(VIEWS_PATH.'TableList_class.php');
		$table = new TableListView('RemoteUsers', 'list');
		$table->setElements($permissions);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
						new TableList_Field('id', '[$Id]', 'int', 'left'),
						new TableList_Field('component_name', '[$Component]', 'string', 'left'),
						new TableList_Field('action', '[$Action]', 'string', 'left'),
					));
		$table->setUrl_delete($url_permission_delete);
		$table->setUrl_delete_params(array('id_user' => $user->id));
		$table->renderTopBar(false);
		$table->multipleDeletion(false);
		$table->render(); ?>
                    <div class="col-md-offset-6 col-md-6">
                        <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form" novalidate="">
                            <div class="form-group">
                                <label class="control-label col-md-3">[$Component]*</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="id_component" id="component" onchange="refreshActionsList()" required="">
                                        <?php foreach ($components as $comp) {
			?>
                                            <option value="<?= $comp->id ?>"><?= $comp->name ?></option>
                                        <?php
		} ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">[$Action]*</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="ActionsList" name="action" required="">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <div class="col-md-12">
                                    <button type="submit" name="add" class="btn btn-info">
                                        <i class="fa fa-plus"></i>&nbsp;&nbsp;[$Add]
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
    </div>
<?php
	} ?>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>

<script>
    function refreshActionsList() {
        console.log("<?= Utils::getComponentUrl('RemoteUsers/ajx_getCompActions/') ?>" + $('#component').val());
        $.ajax({
            url: "<?= Utils::getComponentUrl('RemoteUsers/ajx_getCompActions/') ?>" + $('#component').val(),
            dataType: "json",
            success: function (data) {
                var options, index, select, option;
                // Get the raw DOM object for the select box
                select = document.getElementById('ActionsList');
                // Clear the old options
                select.options.length = 0;
                // Load the new options
                options = data.options; // Or whatever source information you're working with
                for (index = 0; index < options.length; ++index) {
                    option = options[index];
                    select.options.add(new Option(option.text, option.value));
                }
                $('#ActionsList').val('');
            },
            error: function (e) {
                select = document.getElementById('ActionsList');
                // Clear the old options
                select.options.length = 0;
                select.options.add(new Option('', ''));
                console.log('Error: ' + e);
            }
        });
    }

    $(document).ready(function () {

        if ($('#component').val() !== '') {
            refreshActionsList();
        }
    });
</script>