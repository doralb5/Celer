<?php

class Shop_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_products_list', $this->getActionUrl('products_list'));
		$this->view->set('url_product_edit', $this->getActionUrl('product_edit'));
		$this->view->set('url_product_delete', $this->getActionUrl('product_delete'));
		$this->view->set('url_product_enable', $this->getActionUrl('product_enable'));
		$this->view->set('url_product_disable', $this->getActionUrl('product_disable'));

		$this->view->set('url_categories_list', $this->getActionUrl('categories_list'));
		$this->view->set('url_category_edit', $this->getActionUrl('category_edit'));
		$this->view->set('url_category_delete', $this->getActionUrl('category_delete'));

		$this->view->set('url_brands_list', $this->getActionUrl('brands_list'));
		$this->view->set('url_brand_edit', $this->getActionUrl('brand_edit'));
		$this->view->set('url_brand_delete', $this->getActionUrl('brand_delete'));

		$this->view->set('url_model_delete', $this->getActionUrl('model_delete'));

		$this->view->set('url_types_list', $this->getActionUrl('types_list'));
		$this->view->set('url_type_edit', $this->getActionUrl('type_edit'));
		$this->view->set('url_type_delete', $this->getActionUrl('type_delete'));

		$this->view->set('url_features_list', $this->getActionUrl('features_list'));
		$this->view->set('url_feature_edit', $this->getActionUrl('feature_edit'));
		$this->view->set('url_feature_delete', $this->getActionUrl('feature_delete'));

		//Additional Images
		$this->view->set('url_addtImage_delete', $this->getActionUrl('addtImage_delete'));
		$this->view->set('url_addtImage_download', $this->getActionUrl('addtImage_download'));
		$this->view->set('url_addtImage_show', substr(WEBROOT, 0, -6).MEDIA_ROOT.'products');

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function products_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Products'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '1 ';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteProduct($id);
				$this->LogsManager->registerLog('shop_Product', 'delete', 'Shop product deleted with id : '.$id, $id);
			}
			if ($res !== false) {
				$this->view->AddNotice('Products has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('products_list'));
		}

		$prod_Tbl = TABLE_PREFIX.'shop_Product';
		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => "$prod_Tbl.id", 'peso' => 100),
				array('field' => "$prod_Tbl.name", 'peso' => 90),
				array('field' => "$prod_Tbl.description", 'peso' => 90),
			);
			$products = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$products = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($products) == 0) {
			Utils::RedirectTo($this->getActionUrl('products_list'));
			return;
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddProduct]', $this->getActionUrl('product_edit')));

		$this->view->BreadCrumb->addDir('[$Products]', $this->getActionUrl('products_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('products', $products);
		$this->view->setTitle('[$Products]');
		$this->view->render('products-list');
	}

	public function product_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Products]', $this->getActionUrl('products_list'));
		if (!is_null($id)) {
			$product = $this->model->getProduct($id);
			HeadHTML::setTitleTag('Edit Product - '.$product->name.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditProduct]', $this->getActionUrl('product_edit')."/$id");
		} else {
			$product = new shop_Product_Entity();
			HeadHTML::setTitleTag('New Product'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$NewProduct]', $this->getActionUrl('product_edit'));
		}

		$logged_user = UserAuth::getLoginSession();

		if (!is_null($id)) {
			$features = $this->model->getFeatures(50, 0, "id_type = {$product->id_type}");
			foreach ($features as $feat) {
				$prodFeat = $this->model->getProdFeatures(20, 0, "id_product = $id AND id_feature = {$feat->id}");
				if (count($prodFeat)) {
					$feat->value = $prodFeat[0]->value;
				} else {
					$feat->value = null;
				}
			}
			$this->view->set('features', $features);
		}

		if ($product === false) {
			$this->AddError("Product $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['name'] == '') ? $this->AddError('Name is required!') : '';
			//   ($_POST['id_type'] == '') ? $this->AddError("Type is required!") : '';
			//($_POST['id_model'] == '') ? $this->AddError("Model is required!") : '';
			//($_POST['id_category'] == '') ? $this->AddError("Category is required!") : '';
			($_POST['publish_date'] == '') ? $this->AddError('Publish Date is required!') : '';
			(!isset($_POST['status']) || $_POST['status'] == '') ? $this->AddError('Status is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $product->image;
				} else {
					$_POST['image'] = $product->image;
					$old_image = null;
				}

				$this->model->startTransaction();

				$product->id_category = $_POST['id_category'];
				$product->id_model = $_POST['id_model'];
				$product->id_type = $_POST['id_type'];
				$product->id_brand = $_POST['id_brand'];
				$product->name = $_POST['name'];
				$product->description = $_POST['description'];
				$product->enabled = isset($_POST['enabled']) ? $_POST['enabled'] : 0;
				$product->featured = isset($_POST['featured']) ? $_POST['featured'] : 0;
				$product->publish_date = $_POST['publish_date'];
				$product->status = $_POST['status'];
				$product->price = $_POST['price'];
				$product->final_price = $_POST['final_price'];
				$product->image = $_POST['image'];

				$inserted_id = $this->model->saveProduct($product);

				if (!is_array($inserted_id)) {
					$errors = 0;
					foreach ($features as $feature) {
						if (isset($_POST["ft_{$feature->feature}"])) {
							if ($_POST["ft_{$feature->feature}"] == '') {
								continue;
							}

							$prodFeature = $this->model->getProdFeatures(1, 0, "id_feature = {$feature->id} AND id_product = $id");
							if (count($prodFeature)) {
								$prodFeature = $prodFeature[0];
							} else {
								$prodFeature = new shop_ProductFeature_Entity();
								$prodFeature->id_feature = $feature->id;
								$prodFeature->id_product = $id;
							}
							$prodFeature->value = $_POST["ft_{$feature->feature}"];
							$res = $this->model->saveProdFeature($prodFeature);
							if (is_array($res)) {
								$errors++;
							}
						}
					}

					if ($errors == 0) {
						$this->AddNotice('Product has been saved successfully.');
						$this->model->commit();

						if (strlen($_FILES['image']['tmp_name'])) {
							$target = DOCROOT.MEDIA_ROOT.DS.'products';
							Utils::createDirectory($target);
							$filename = $target.DS.$_POST['image'];
							move_uploaded_file($_FILES['image']['tmp_name'], $filename);

							if (!is_null($id)) {
								unlink($target.DS.$old_image);
							}
						}
					} else {
						$this->AddError('Problem while saving product features!');
						$this->model->rollback();
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('shop_Product', 'insert', 'Shop product inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('product_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('shop_Product', 'update', 'Shop product updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('product_edit')."/$id");
					}
				} else {
					$this->AddError('Saving failed!');
					$this->AddError($inserted_id[2]);
					$this->model->rollback();
				}
			}
		} elseif (isset($_POST['AddImage'])) {
			if ($_FILES['images']['tmp_name'][0] == '') {
				$this->view->AddError('Please upload a file...');
			}

			if (count($this->view->getErrors()) == 0) {
				$failures = 0;
				for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
					$tmp_name = time().'_'.$_FILES['images']['name'][$i];

					$addImage = new shop_ProductImage_Entity();

					$addImage->id_product = $id;
					$addImage->image = $tmp_name;
					$addImage->size = $_FILES['images']['size'][$i];
					$res = $this->model->saveProductImage($addImage);

					if (!is_array($res)) {
						$target = DOCROOT.MEDIA_ROOT.'products'.DS.'additional';
						Utils::createDirectory($target);
						$filename = $target.DS.$tmp_name;
						move_uploaded_file($_FILES['images']['tmp_name'][$i], $filename);
					} else {
						$failures++;
					}
				}
				if ($failures > 0) {
					$this->AddError('There was '.$failures.' failures during image upload!');
				} else {
					$this->view->AddNotice('Image has been uploaded successfully.');
				}
			}
			Utils::RedirectTo($this->getActionUrl('product_edit')."/$id");
		}

		$categories = $this->model->getCategories(-1);
		$this->view->set('categories', $categories);

		$types = $this->model->getTypes(-1);
		$this->view->set('types', $types);

		$brands = $this->model->getBrands(-1);
		$this->view->set('brands', $brands);

		if (!is_null($id)) {
			$additionalImages = $this->model->getProductImages(50, 0, "id_product = $id");
			$this->view->set('additionalImages', $additionalImages);
		}

		$this->view->set('product', $product);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('products_list'))));
		$title = (!is_null($id)) ? '[$EditProduct]' : '[$NewProduct]';
		$this->view->setTitle($title);
		$this->view->render('product-edit');
	}

	public function product_details($id)
	{
		Utils::saveCurrentPageUrl();
		$product = $this->model->getArticle($id);
		$this->view->set('product', $product);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('products_list'))));
		$this->view->BreadCrumb->addDir('[$Products]', $this->getActionUrl('products_list'));
		$this->view->BreadCrumb->addDir('[$ArticleDetails]', $this->getActionUrl('product_details')."/$id");
		$this->view->setTitle('[$Details]');
		$this->view->render('product-details');
	}

	public function product_delete($id)
	{
		$product = $this->model->getProduct($id);
		$additionalImages = $this->model->getProductImages(50, 0, "id_product = $id");
		$result = $this->model->deleteProduct($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'products';
			if ($product->image != '' && file_exists($target.DS.$product->image)) {
				unlink($target.DS.$product->image);
			}

			foreach ($additionalImages as $image) {
				if (file_exists($target.DS.'additional'.DS.$image->image)) {
					unlink($target.DS.'additional'.DS.$image->image);
				}
			}

			$this->LogsManager->registerLog('shop_Product', 'delete', "Shop product deleted with id : $id and name : ".$product->name, $id);
			$this->AddNotice('Product deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('products_list'));
	}

	public function product_enable($id)
	{
		if (!is_null($id)) {
			$product = $this->model->getProduct($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$product->enabled = 1;
		$res = $this->model->saveProduct($product);

		if (!is_array($res)) {
			$this->view->AddNotice('Product has been enabled successfully.');
			$this->LogsManager->registerLog('shop_Product', 'update', 'Shop product enabled with id : '.$product->id, $product->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('products_list'));
	}

	public function product_disable($id)
	{
		if (!is_null($id)) {
			$product = $this->model->getProduct($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$product->enabled = 0;
		$res = $this->model->saveProduct($product);

		if (!is_array($res)) {
			$this->view->AddNotice('Product has been disabled successfully.');
			$this->LogsManager->registerLog('shop_Product', 'update', 'Shop product disabled with id : '.$product->id, $product->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('products_list'));
	}

	public function categories_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Categories'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteCategory($id);
				$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Categories has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::backRedirect($this->getActionUrl('categories_list'));
		}

		$categories = $this->model->getCategories($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($categories) == 0) {
			Utils::RedirectTo($this->getActionUrl('categories_list'));
			return;
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', $this->getActionUrl('category_edit')));
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Categories]', $this->getActionUrl('categories_list'));
		if (!is_null($id)) {
			$category = $this->model->getCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$category->category.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditCategory]', $this->getActionUrl('category_edit')."/$id");
		} else {
			$category = new shop_Category_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddCategory]', $this->getActionUrl('category_edit'));
		}

		if ($category === false) {
			$this->AddError("Category $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['category'] == '') ? $this->AddError('Category Name is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $category->image;
				} else {
					$_POST['image'] = $category->image;
				}
				$category->category = $_POST['category'];
				$category->description = $_POST['description'];
				$category->image = $_POST['image'];
				$category->parent_id = ($_POST['parent_id'] != '') ? $_POST['parent_id'] : null;
				$category->sorting = $_POST['sorting'];

				$inserted_id = $this->model->saveCategory($category);
				if (!is_array($inserted_id)) {
					$this->AddNotice('Category has been saved successfully!');

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'products'.DS.'categories';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Category', 'insert', 'Category inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('Category', 'update', 'Category updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('category_edit')."/$id");
					}
				} else {
					$this->AddError($inserted_id[2]);
				}
			}
		}
		$this->view->set('category', $category);
		$this->view->set('categories', $this->model->getCategories());

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('categories_list'))));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id)
	{
		$category = $this->model->getCategory($id);
		$result = $this->model->deleteCategory($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'products'.DS.'categories';
			if ($category->image != '' && file_exists($target.$category->image)) {
				unlink($target.$category->image);
			}
			$this->LogsManager->registerLog('Category', 'delete', "Category deleted with id : $id, name : {$category->category}", $id);
			$this->AddNotice('Category deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('categories_list'));
	}

	public function brands_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Brands'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteBrand($id);
				$this->LogsManager->registerLog('shop_Brand', 'delete', "Shop brand deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Brands has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::backRedirect($this->getActionUrl('brands_list'));
		}

		$brands = $this->model->getBrands($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($brands) == 0) {
			Utils::RedirectTo($this->getActionUrl('brands_list'));
			return;
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddBrand]', $this->getActionUrl('brand_edit')));
		$this->view->BreadCrumb->addDir('[$Brands]', $this->getActionUrl('brands_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('brands', $brands);
		$this->view->setTitle('[$Brands]');
		$this->view->render('brands-list');
	}

	public function brand_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Brands]', $this->getActionUrl('brands_list'));
		if (!is_null($id)) {
			$brand = $this->model->getBrand($id);
			HeadHTML::setTitleTag('Edit Brand - '.$brand->brand.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditBrand]', $this->getActionUrl('brand_edit')."/$id");
		} else {
			$brand = new shop_Brand_Entity();
			HeadHTML::setTitleTag('New Brand'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddBrand]', $this->getActionUrl('brand_edit'));
		}

		if ($brand === false) {
			$this->AddError("Brand $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['brand'] == '') ? $this->AddError('Brand Name is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $brand->image;
				} else {
					$_POST['image'] = $brand->image;
				}

				$brand->brand = $_POST['brand'];
				$brand->description = $_POST['description'];
				$brand->image = $_POST['image'];
				$inserted_id = $this->model->saveBrand($brand);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Brand has been saved successfully!');

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'products'.DS.'brands';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('shop_Brand', 'insert', 'Shop brand inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('brand_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('shop_Brand', 'update', 'Shop brand updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('brand_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		} elseif (isset($_POST['save_model'])) {
			($_POST['model'] == '') ? $this->AddError('Model Name is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				if (isset($_GET['id_model'])) {
					$id_model = $_GET['id_model'];
					$model = $this->model->getModel($id_model);
				} else {
					$model = new shop_ProductModel_Entity();
					$model->id_brand = $id;
				}

				$model->model = $_POST['model'];

				$result = $this->model->saveModel($model);
				if (!is_array($result)) {
					$this->view->AddNotice('Model has been saved successfully.');

					if (!is_bool($result)) {
						$this->LogsManager->registerLog('shop_ProductModel', 'insert', 'Shop product model inserted with id : '.$result, $result);
					} else {
						$this->LogsManager->registerLog('shop_ProductModel', 'update', 'Shop product model updated with id : '.$model->id, $model->id);
					}
					Utils::RedirectTo($this->getActionUrl('brand_edit')."/$id");
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
		}

		if (!is_null($id)) {
			$models = $this->model->getModels(50, 0, "id_brand = $id");
			$this->view->set('models', $models);

			$id_model = isset($_GET['id_model']) ? $_GET['id_model'] : null;
			if (!is_null($id_model)) {
				$model = $this->model->getModel($id_model);
				$this->view->set('model', $model);
			}
		}

		$this->view->set('brand', $brand);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('brands_list'))));
		$title = (!is_null($id)) ? '[$EditBrand]' : '[$NewBrand]';
		$this->view->setTitle($title);
		$this->view->render('brand-edit');
	}

	public function brand_delete($id)
	{
		$brand = $this->model->getBrand($id);
		$result = $this->model->deleteBrand($id);
		if ($result !== false) {
			$target = DOCROOT.MEDIA_ROOT.'products'.DS.'brands';
			if ($brand->image != '' && file_exists($target.$brand->image)) {
				unlink($target.$brand->image);
			}
			$this->LogsManager->registerLog('shop_Brand', 'delete', "Shop brand deleted with id : $id, name : {$brand->brand}", $id);
			$this->AddNotice('Brand deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('brands_list'));
	}

	public function model_delete($id)
	{
		$model = $this->model->getModel($id);

		if (is_null($model)) {
			$this->view->AddError('Model not found!');
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
			return;
		}

		$result = $this->model->deleteModel($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('shop_ProductModel', 'delete', "Shop product model deleted with id : $id, name : {$model->model}", $id);
			$this->AddNotice('Model deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('brand_edit')."/{$model->id_brand}");
	}

	public function types_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Product Types'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteType($id);
				$this->LogsManager->registerLog('shop_ProductType', 'delete', "Shop product type deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Types has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::backRedirect($this->getActionUrl('types_list'));
		}

		$types = $this->model->getTypes($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($types) == 0) {
			Utils::RedirectTo($this->getActionUrl('types_list'));
			return;
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddType]', $this->getActionUrl('type_edit')));
		$this->view->BreadCrumb->addDir('[$Types]', $this->getActionUrl('types_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('types', $types);
		$this->view->setTitle('[$Types]');
		$this->view->render('types-list');
	}

	public function type_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Types]', $this->getActionUrl('types_list'));
		if (!is_null($id)) {
			$type = $this->model->getType($id);
			HeadHTML::setTitleTag('Edit Type - '.$type->type.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditType]', $this->getActionUrl('type_edit')."/$id");
		} else {
			$type = new shop_ProductType_Entity();
			HeadHTML::setTitleTag('New Type'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddType]', $this->getActionUrl('type_edit'));
		}

		if ($type === false) {
			$this->AddError("Type $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['type'] == '') ? $this->AddError('Type Name is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$type->type = $_POST['type'];
				$inserted_id = $this->model->saveType($type);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Type has been saved successfully!');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('shop_Type', 'insert', 'Shop type inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('type_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('shop_Type', 'update', 'Shop type updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('type_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('type', $type);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('types_list'))));
		$title = (!is_null($id)) ? '[$EditType]' : '[$NewType]';
		$this->view->setTitle($title);
		$this->view->render('type-edit');
	}

	public function type_delete($id)
	{
		$type = $this->model->getType($id);
		$result = $this->model->deleteType($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('shop_Type', 'delete', "Shop type deleted with id : $id, name : {$type->type}", $id);
			$this->AddNotice('Type deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('types_list'));
	}

	public function features_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Features'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteFeature($id);
				$this->LogsManager->registerLog('shop_Feature', 'delete', "Shop feature deleted with id : $id", $id);
			}

			if ($res !== false) {
				$this->view->AddNotice('Features has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::backRedirect($this->getActionUrl('features_list'));
		}

		$features = $this->model->getFeatures($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($features) == 0) {
			Utils::RedirectTo($this->getActionUrl('features_list'));
			return;
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddFeature]', $this->getActionUrl('feature_edit')));
		$this->view->BreadCrumb->addDir('[$Features]', $this->getActionUrl('features_list'));

		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('features', $features);
		$this->view->setTitle('[$Features]');
		$this->view->render('features-list');
	}

	public function feature_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Features]', $this->getActionUrl('features_list'));
		if (!is_null($id)) {
			$feature = $this->model->getFeature($id);
			HeadHTML::setTitleTag('Edit Feature - '.$feature->feature.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditFeature]', $this->getActionUrl('feature_edit')."/$id");
		} else {
			$feature = new shop_Feature_Entity();
			HeadHTML::setTitleTag('New Feature'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddFeature]', $this->getActionUrl('feature_edit'));
		}

		if ($feature === false) {
			$this->AddError("Feature $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['feature'] == '') ? $this->AddError('Feature is required!') : '';
			($_POST['id_type'] == '') ? $this->AddError('Type is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$feature->feature = $_POST['feature'];
				$feature->id_type = $_POST['id_type'];
				$inserted_id = $this->model->saveFeature($feature);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Feature has been saved successfully!');

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('shop_Feature', 'insert', 'Shop feature inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('feature_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('shop_Feature', 'update', 'Shop feature updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('feature_edit')."/$id");
					}
				} else {
					$this->AddError('Something went wrong!');
				}
			}
		}
		$this->view->set('feature', $feature);

		$types = $this->model->getTypes(50);
		$this->view->set('types', $types);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('features_list'))));
		$title = (!is_null($id)) ? '[$EditFeature]' : '[$NewFeature]';
		$this->view->setTitle($title);
		$this->view->render('feature-edit');
	}

	public function feature_delete($id)
	{
		$feature = $this->model->getFeature($id);
		$result = $this->model->deleteFeature($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('shop_Feature', 'delete', "Shop feature deleted with id : $id, name : {$feature->feature}", $id);
			$this->AddNotice('Feature deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('features_list'));
	}

	public function addtImage_delete($id)
	{
		$image = $this->model->getProductImage($id);

		$res = $this->model->deleteProductImage($id);
		if ($res) {
			$this->LogsManager->registerLog('shop_ProductImage', 'delete', "Product image deleted with id : $id", $id);
			$this->AddNotice('Image has been deleted successfully.');
			$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'products'.DS.'additional'.DS.$image->image;
			unlink(DOCROOT.$filename);
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('product_edit')."/{$image->id_product}");
	}

	public function addtImage_download($id)
	{
		if (is_null($id)) {
			Utils::RedirectTo($this->getActionUrl('products_list'));
		}

		$image = $this->model->getProductImage($id);
		$filename = substr(WEBROOT, 0, -6).MEDIA_ROOT.'products'.DS.'additional'.DS.$image->image;

		//Shkarkimi i Files
		header('Content-Type: '.mime_content_type(DOCROOT.$filename));
		header('Content-Disposition: attachment; filename="'.$image->image.'""');
		readfile(DOCROOT.$filename);
	}

	public function ajx_getModels($id_brand)
	{
		$models = $this->model->getModels(50, 0, "id_brand = $id_brand");

		$options = array();
		foreach ($models as $model) {
			array_push($options, array('text' => $model->model, 'value' => $model->id));
		}
		echo json_encode(array('options' => $options));
	}
}
