<?php
$brand_name = isset($_POST['brand']) ? $_POST['brand'] : $brand->brand;
$description = isset($_POST['description']) ? $_POST['description'] : $brand->description;
?>
    <div class="col-md-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$Category]</h4>
            </div>
            <div class="panel-body">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
                      novalidate="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="name">[$Name] *</label>

                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" value="<?= $brand_name ?>"
                                   name="brand" placeholder="[$Name]" data-parsley-required="true"
                                   data-parsley-id="6524" required>
                            <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="name">[$Description]</label>

                        <div class="col-md-8 col-sm-8">
                            <textarea name="description" class="form-control" rows="3"
                                      style="max-width: 100%; min-width: 100%"><?= $description ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                        <div class="col-md-6 col-sm-6">
                            <input type="file" name="image">
                            <?php if ($brand->image != '') {
	?>
                                <p>
                                    <img
                                        src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'products/brands/'.$brand->image ?>"
                                        width="100px" alt="Image"/></p>
                            <?php
} ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <p>&nbsp;</p>
                        <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                        </button>
                        <p>&nbsp;</p>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php if (!is_null($brand->id)) {
		?>
    <div class="col-md-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$Models]</h4>
            </div>
            <div class="panel-body">
                <p>&nbsp;</p>
                <?php
				include(VIEWS_PATH.'TableList_class.php');
		$table = new TableListView('ProductModels');
		foreach ($models as &$row) {
			$row->rows_buttons = array(
						new Button('<i class="fa fa-edit"></i>', "$url_brand_edit/{$row->id_brand}?id_model={$row->id}", 'xs', '', '[$Edit]'),
					);
		}

		$table->setElements($models);
		$total = count($models);
		$table->setTotalElements(count($total));
		$table->setElements_per_page(count($total));
		$table->setFields(array(
					new TableList_Field('id', '[$Id]', 'int', 'left'),
					new TableList_Field('model', '[$Model]', 'string', 'left'),
				));
		$table->setUrl_delete($url_model_delete);
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->renderPanel(false);
		$table->showResultInfo(false);
		$table->render();

		$model_name = isset($_POST['model']) ? $_GET['model'] : (isset($model) ? $model->model : '');

		$id_model = isset($_GET['id_model']) ? $_GET['id_model'] : null; ?>

                <form method="POST" class="form-horizontal form-bordered">
                    <div class="form-group">
                        <label class="control-label col-sm-4">[$ModelName]</label>
                        <div class="col-sm-8">
                            <?php if (!is_null($id_model)) {
			?>
                                <p>[$EditingModel] : <?= $model->model ?></p>
                            <?php
		} ?>
                            <div class="input-group">
                                <?php if (!is_null($id_model)) {
			?>
                                    <div class="input-group-btn">
                                        <a href="<?= Utils::getComponentUrl('Shop/brand_edit')."/$brand->id" ?>"
                                           class="btn btn-danger"><span class="fa fa-times"></span> </a>
                                    </div>
                                    <div class="input-group-addon">
                                        <span class="fa fa-pencil"></span>
                                    </div>
                                <?php
		} ?>
                                <input name="model" class="form-control" type="text" value="<?= $model_name ?>"
                                       required style="border: 1px solid #e2e7eb"/>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-info" name="save_model"><span
                                            class="fa fa-floppy-o"></span>&nbsp;&nbsp;[$Save]
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
	} ?>