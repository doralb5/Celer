<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Shop');

foreach ($brands as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_brand_edit.'/'.$row->id, 'xs', '', '[$Edit]')
	);
}
$table->setElements($brands);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('brand', '[$Name]', 'string', 'left'),
	new TableList_Field('image', '[$Image]', 'image', 'left')
));
$table->setUrl_action($url_brands_list);
$table->setUrl_delete($url_brand_delete);

$table->setImagesPath('products'.DS.'brands');
$table->multipleDeletion(false);
$table->renderTopBar(false);
$table->render();
