<?php
$idcat = $category->id;
$category_name = isset($_POST['category']) ? $_POST['category'] : $category->category;
$description = isset($_POST['description']) ? $_POST['description'] : $category->description;
$parent_id = isset($_POST['parent_id']) ? $_POST['parent_id'] : $category->parent_id;
$category_sorting = isset($_POST['sorting']) ? $_POST['sorting'] : $category->sorting;
?>
<div class="col-md-8">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">[$Category]</h4>
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form" novalidate="" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Name] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $category_name ?>"
                               name="category" placeholder="[$Name]" data-parsley-required="true"
                               data-parsley-id="6524" required>
                        <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="parent_id">[$ParentCategory]</label>

                    <div class="col-md-8 col-sm-8">
                        <select class="form-control" name="parent_id">
                            <option value="">-- [$None] --</option>
                            <?foreach ($categories as $cat) {
	if ($cat->id != $idcat) {
		?>
                                <option value="<?=$cat->id?>" <?=($cat->id == $parent_id)?'selected':''?>><?=$cat->category?></option>
                            <?
	}
}?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="txt_description">[$Description]</label>

                    <div class="col-md-8 col-sm-8">
                            <textarea name="description" class="form-control" rows="3" id="txt_description" style="max-width: 100%; min-width: 100%"><?= $description ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                    <div class="col-md-6 col-sm-6">
                        <input type="file" name="image">
                        <?php if ($category->image != '') {
	?>
                            <p>
                                <img
                                    src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'products/categories/'.$category->image ?>"
                                    width="100px" alt="Image"/></p>
                        <?php
} ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$sorting] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $category_sorting ?>"
                               name="sorting" placeholder="[$Name]" data-parsley-required="true"
                               data-parsley-id="6524" required>
                        <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                    </div>
                </div>
                <div class="form-group">
                    <p>&nbsp;</p>
                    <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                    <p>&nbsp;</p>
                </div>
            </form>
        </div>
    </div>
</div>