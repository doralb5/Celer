<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Shop');

foreach ($features as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_feature_edit.'/'.$row->id, 'xs', '', '[$Edit]')
	);
}
$table->setElements($features);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('feature', '[$Name]', 'string', 'left'),
	new TableList_Field('type', '[$Type]', 'string', 'left')
));
$table->setUrl_action($url_features_list);
$table->setUrl_delete($url_feature_delete);

$table->multipleDeletion(false);
$table->renderTopBar(false);
$table->render();
