<?php
HeadHTML::AddStylesheet(substr(WEBROOT, 0, -6).COMMONS_PATH.'css/bootstrap-switch.min.css');
HeadHTML::AddJS(substr(WEBROOT, 0, -6).COMMONS_PATH.'js/bootstrap-switch.min.js');

$id_category = isset($_POST['id_category']) ? $_POST['id_category'] : $product->id_category;
$id_type = isset($_POST['id_type']) ? $_POST['id_type'] : $product->id_type;
$id_brand = isset($_POST['id_brand']) ? $_POST['id_brand'] : $product->id_brand;
$id_model = isset($_POST['id_model']) ? $_POST['id_model'] : $product->id_model;
$name = isset($_POST['name']) ? $_POST['name'] : $product->name;
$description = isset($_POST['description']) ? $_POST['description'] : $product->description;
$price = isset($_POST['price']) ? $_POST['price'] : $product->price;
$final_price = isset($_POST['final_price']) ? $_POST['final_price'] : $product->final_price;
$status = isset($_POST['status']) ? $_POST['status'] : (!is_null($product->id) ? $product->status : 'new');
$publish_date = isset($_POST['publish_date']) ? $_POST['publish_date'] : (!is_null($product->id) ? $product->publish_date : date('Y-m-d H:i:s'));
$enabled = isset($_POST['enabled']) ? $_POST['enabled'] : $product->enabled;
$featured = isset($_POST['featured']) ? $_POST['featured'] : $product->featured;
?>

<form method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
    <div class="col-md-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">[$GeneralInformation]</h4>
            </div>
            <div class="panel-body panel-form">

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Type]</label>

                    <div class="col-md-8 col-sm-8">
                        <select class="form-control" name="id_type">
                            <?php foreach ($types as $type) {
	?>
                                <option
                                    value="<?= $type->id ?>" <?= ($type->id == $id_type) ? 'selected' : '' ?>><?= $type->type ?></option>
                                <?php
} ?>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$ProductName] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $name ?>"
                               name="name" placeholder="[$ProductName]" data-parsley-required="true"
                               data-parsley-id="6524" required>
                        <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Category]</label>

                    <div class="col-md-8 col-sm-8">
                        <select class="form-control" name="id_category">
                            <option>-- [$SelectOption] --</option>
                            <?php foreach ($categories as $categ) {
		?>
                                <option
                                    value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
                                <?php
	} ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Brand]</label>

                    <div class="col-md-8 col-sm-8">
                        <!--<select class="form-control" name="id_brand" id="id_brand" onchange="refreshModelsList();">-->
                        <select class="form-control" name="id_brand" id="id_brand">
                            <option>-- [$SelectOption] --</option>
                            <?php foreach ($brands as $brand) {
		?>
                                <option
                                    value="<?= $brand->id ?>" <?= ($brand->id == $id_brand) ? 'selected' : '' ?>><?= $brand->brand ?></option>
                                <?php
	} ?>
                        </select>
                    </div>
                </div>
<!--                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Model]</label>

                    <div class="col-md-8 col-sm-8">
                        <select class="form-control" name="id_model" id="id_model">
                        </select>
                    </div>
                </div>-->

                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Status]</label>

                    <div class="col-md-8 col-sm-8">
                        <label class="radio-inline">
                            <input type="radio" name="status" value="new" <?= ($status == 'new') ? 'checked' : '' ?>
                                   required>
                            [$New]
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="used" <?= ($status == 'used') ? 'checked' : '' ?>
                                   required>
                            [$Used]
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                    <div class="col-md-6 col-sm-6">
                        <input type="file" name="image">
                        <?php if ($product->image != '') {
		?>
                            <p>
                                <img
                                    src="<?= Utils::genThumbnailUrl('products/'.$product->image, 100, 100) ?>"
                                    width="100px" alt="Image"/></p>
                            <?php
	} ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Price] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text"  value="<?= $price ?>"
                               name="price" placeholder="[$Price]" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$FinalPrice]</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="number" step="any" value="<?= $final_price ?>"
                               name="final_price" placeholder="[$Finalprice]">
                    </div>
                </div>
            </div>
        </div>

    </div>



    <?php if (!is_null($product->id) && count($features)) {
		?>
        <!-- begin panel -->
        <div class="col-md-6 ">
            <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">[$Features]</h4>
                </div>
                <div class="panel-body panel-form">

                    <?php
					foreach ($features as $feature) {
						$feature_val = isset($_POST["ft_{$feature->feature}"]) ? $_POST["ft_{$feature->feature}"] : $feature->value; ?>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">[$<?= $feature->feature ?>]</label>

                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control" name="ft_<?= $feature->feature ?>"
                                       placeholder="<?= ucfirst($feature->feature) ?>" value="<?= $feature_val ?>">
                            </div>
                        </div>
                    <?php
					} ?>

                </div>
            </div>
        </div>
        <!-- end panel -->
    <?php
	} ?>



    




    <!-- begin panel -->
    <div class="col-md-6">
        <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$PublishSettings]</h4>
            </div>
            <div class="panel-body panel-form">
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Featured]</label>

                    <div class="col-md-8 col-sm-8">
                        <input type="checkbox" data-size="normal"
                               data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                               data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                               data-label-width="30px"
                               value="1" name="featured" <?= ($featured == '1') ? 'checked' : '' ?>
                               id="featured-switch">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Enabled]</label>

                    <div class="col-md-8 col-sm-8">
                        <input type="checkbox" data-size="normal"
                               data-on-text="[$yes_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                               data-off-text="[$no_option]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                               data-label-width="30px"
                               value="1" name="enabled" <?= ($enabled == '1') ? 'checked' : '' ?>
                               id="enabled-switch">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$PublishDate]</label>

                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" name="publish_date" id="datetimepicker1"
                               placeholder="[$SelectDate]" value="<?= $publish_date ?>" required="">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end panel -->







<!-- begin panel -->
    <div class="col-md-6">
        <div class="panel panel-inverse" data-sortable-id="form-wysiwyg-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$Description]</h4>
            </div>
            <div class="panel-body panel-form">

                <textarea class="editor" id="editor"
                          name="description"><?= $description ?></textarea>

            </div>

        </div>
    </div>
    <!-- end panel -->











    <div class="col-md-12">
        <p class="text-center">
            <button type="submit" name="save" class="btn btn-info">
                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
            </button>
        </p>
    </div>
    <p>&nbsp;</p>
</form>


<?php if (!is_null($product->id)) {
		?>

    <div class="row">
        <div class="col-md-8">

            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i
                                class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i
                                class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                           data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">[$AdditionalImages]</h4>
                </div>
                <div class="panel-body">

                    <p>&nbsp;</p>
                    <?php
					include(VIEWS_PATH.'TableList_class.php');
		$table = new TableListView('Shop');
		foreach ($additionalImages as &$row) {
			$row->rows_buttons = array(
							new Button('<i class="fa fa-eye"></i>', $url_addtImage_show.'/additional/'.$row->image, 'xs', '', '[$Show]', '_blank'),
							new Button('<i class="fa fa-download"></i>', $url_addtImage_download.'/'.$row->id, 'xs', '', '[$Download]')
						);

			$row->row_imagePath = 'products'.DS.'additional';
		}

		$table->setElements($additionalImages);
		$table->setTotalElements(100);
		$table->setElements_per_page(100);
		$table->setFields(array(
						new TableList_Field('id', '[$Id]', 'int', 'left'),
						new TableList_Field('image', '[$Filename]', 'string', 'left'),
						new TableList_Field('image', '[$Image]', 'image', 'left'),
					));
		$table->setUrl_delete($url_addtImage_delete);
		$table->multipleDeletion(false);
		$table->renderTopBar(false);
		$table->renderPanel(false);
		$table->render(); ?>


                    <div class="col-md-12">
                        <form id="fileupload" method="POST" enctype="multipart/form-data">
                            <div class="form-group col-md-9">
                                <label>[$SelectFile]</label>

                                <div class="box">
                                    <input type="file" name="images[]" id="file-7" class="inputfile inputfile-6"
                                           data-multiple-caption="{count} files selected" multiple/>
                                    <label for="file-7"><span></span><strong>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                                 viewBox="0 0 20 17">
                                            <path
                                                d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                            </svg>
                                            [$Choose]&hellip;</strong></label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <br>
                                <button class="btn btn-info" name="AddImage" type="submit"><i class="fa fa-plus"></i>&nbsp;&nbsp;[$AddFile]
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php
	} ?>



<?php
HeadHTML::AddStylesheet(WEBROOT.LIBS_PATH.'file-input/css/component.css');
HeadHTML::AddJS(LIBS_PATH.'file-input/js/custom-file-input.js');
?>

<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>

<script>



    $("#enabled-switch").bootstrapSwitch();
    $("#featured-switch").bootstrapSwitch();


    setTimeout(function () {

    CKEDITOR.replace('editor', {
        imageBrowser_listUrl: "<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).LIBS_PATH ?>JsonDirImages.php?media_path=<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin')).MEDIA_ROOT ?>&docroot=<?= rtrim(DOCROOT, '/') ?>",
                    height: 232
                });


            }, 500);


</script>

<script>

    function refreshModelsList() {
        $.ajax({
            url: "<?= Utils::getComponentUrl('Shop/ajx_getModels') ?>/" + $('#id_brand').val(),
            dataType: "json",
            success: function (data) {
                var options, index, select, option;
                select = document.getElementById('id_model');
                select.options.length = 0;
                options = data.options;
                select.options.add(new Option("", ""));
                for (index = 0; index < options.length; ++index) {
                    option = options[index];
                    select.options.add(new Option(option.text, option.value));
                }
                $('#id_model').val('<?= $id_model ?>');
            },
            error: function (e) {
                select = document.getElementById('id_model');
                select.options.length = 0;
                select.options.add(new Option('', ''));
                console.log('Error: ' + e);
            }
        });
    }

    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
        if ($('#id_brand').val() !== '') {
            refreshModelsList();
        }
    });

</script>
<style>
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
    }

    .inputfile:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .box {
        padding: 0px;
    }
</style>
<script>
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
                labelVal = label.innerHTML;
        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();
            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });
</script>