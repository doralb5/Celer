<?php
$type_name = isset($_POST['type']) ? $_POST['type'] : $type->type;
?>
<div class="col-md-8">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">[$Typology]</h4>
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
                  novalidate="" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Name] *</label>

                    <div class="col-md-8 col-sm-8">

                        <div class="input-group">
                            <input class="form-control" type="text" value="<?= $type_name ?>"
                                   name="type" placeholder="[$Name]" data-parsley-required="true"
                                   data-parsley-id="6524" required>
                            <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                            <div class="input-group-btn">
                                <button type="submit" name="save" class="btn btn-info pull-right"><i
                                        class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>