<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Shop');

foreach ($types as &$row) {
	$row->rows_buttons = array(
		new Button('<i class="fa fa-pencil"></i>', $url_type_edit.'/'.$row->id, 'xs', '', '[$Edit]')
	);
}
$table->setElements($types);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('type', '[$Name]', 'string', 'left'),
));
$table->setUrl_action($url_types_list);
$table->setUrl_delete($url_type_delete);

$table->multipleDeletion(false);
$table->renderTopBar(false);
$table->render();
