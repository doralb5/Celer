<?php

class StaticBlocks_Component extends BaseComponent
{
	private $ActionEditUrl = '';

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->ActionEditUrl = Utils::getComponentUrl('StaticBlocks/staticBlock_edit/');

		$this->view->set('url_staticBlocks_list', $this->getActionUrl('staticBlocks_list'));
		$this->view->set('url_staticBlock_edit', $this->getActionUrl('staticBlock_edit'));
		$this->view->set('url_staticBlock_delete', $this->getActionUrl('staticBlock_delete'));
		$this->view->set('url_staticBlock_enable', $this->getActionUrl('staticBlock_enable'));
		$this->view->set('url_staticBlock_disable', $this->getActionUrl('staticBlock_disable'));

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function staticBlocks_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('StaticBlocks'.' | '.CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Delete selected
		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$res = $this->model->deleteStaticBlock($id);
				$this->LogsManager->registerLog('StaticBlock', 'delete', 'StaticBlock deleted with id : '.$id, $id);
			}
			if ($res !== false) {
				$this->view->AddNotice('Static Blocks has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo($this->getActionUrl('staticBlocks_list'));
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'StaticBlock.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'StaticBlock.name', 'peso' => 90),
			);
			$staticBlocks = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$staticBlocks = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		foreach ($staticBlocks as &$row) {
			$row->action_buttons = array(
				array('name' => '<i class="fa fa-edit"></i>', 'link' => $this->ActionEditUrl.$row->id, 'class' => 'btn-info'));
		}

		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddStaticBlock]', $this->getActionUrl('staticBlock_edit')));

		$this->view->BreadCrumb->addDir('[$StaticBlocks]', $this->getActionUrl('staticBlocks_list'));
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('staticBlocks', $staticBlocks);
		$this->view->setTitle('[$StaticBlocks]');
		$this->view->render('staticBlocks-list');
	}

	public function staticBlock_edit($id = null)
	{
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$StaticBlocks]', $this->getActionUrl('staticBlocks_list'));
		if (!is_null($id)) {
			$staticBlock = $this->model->getStaticBlock($id);
			HeadHTML::setTitleTag('Edit StaticBlock - '.$staticBlock->name.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditStaticBlock]', $this->getActionUrl('staticBlock_edit')."/$id");
		} else {
			$staticBlock = new StaticBlock_Entity();
			HeadHTML::setTitleTag('New StaticBlock'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddStaticBlock]', $this->getActionUrl('staticBlock_edit'));
		}

		if (is_null($staticBlock)) {
			$this->AddError("StaticBlock $id not found");
			$this->view->render('empty');
			return;
		}
		if (isset($_POST['save'])) {
			($_POST['name'] == '') ? $this->AddError('Static Block Name is required!') : '';
			($_POST['content_'.CMSSettings::$default_lang] == '') ? $this->AddError('Content is required!') : '';
			if (count($this->view->getErrors()) == 0) {
				$this->model->startTransaction();

				$staticBlock->name = $_POST['name'];
				$inserted_id = $this->model->saveStaticBlock($staticBlock);

				if (!is_array($inserted_id)) {
					foreach (CMSSettings::$available_langs as $lang) {
						if (is_null($id)) {
							$Content = new StaticBlockContent_Entity();
							$Content->id_static_block = $inserted_id;
						} else {
							$con = $this->model->getStaticBlockContents(1, 0, "id_static_block = {$id} AND lang = '{$lang}'");
							if (count($con)) {
								$Content = $con[0];
							} else {
								$Content = new StaticBlockContent_Entity();
							}
							$Content->id_static_block = $id;
						}

						$is_empty = ($_POST['title_'.$lang] == '' && $_POST['content_'.$lang] == '') ? true : false;

						$Content->title = (!$is_empty) ? $_POST['title_'.$lang] : $_POST['title_'.CMSSettings::$default_lang];
						$Content->content = (!$is_empty) ? $_POST['content_'.$lang] : $_POST['content_'.CMSSettings::$default_lang];
						$Content->lang = $lang;

						$result = $this->model->saveStaticBlockContent($Content);

						if (is_array($result)) {
							$this->AddError('Problem creating static block contents!');
							$this->model->rollback();

							if (is_bool($inserted_id)) {
								Utils::RedirectTo($this->getActionUrl('staticBlock_edit')."/$id");
							} else {
								Utils::RedirectTo($this->getActionUrl('staticBlock_edit'));
							}
						}
					}

					$this->AddNotice('StaticBlock has been saved successfully.');
					$this->model->commit();

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('StaticBlock', 'insert', 'Static Block inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('staticBlock_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('StaticBlock', 'update', 'Static Block updated with id : '.$id, $id);
						Utils::RedirectTo($this->getActionUrl('staticBlock_edit')."/$id");
					}
				} else {
					$this->AddError('Saving failed!');
					$this->model->rollback();
				}
			}
		}

		foreach (CMSSettings::$available_langs as $lang) {
			$content = $this->model->getStaticBlockContents(1, 0, "id_static_block = {$id} AND lang = '{$lang}'");
			$staticBlock->{$lang} = count($content) ? $content[0] : null;
		}

		$this->view->set('staticBlock', $staticBlock);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory($this->getActionUrl('staticBlocks_list'))));
		$title = (!is_null($id)) ? '[$EditStaticBlock]' : '[$NewStaticBlock]';
		$this->view->setTitle($title);
		$this->view->ignoreWidgetPlaceHolder();
		$this->view->render('staticBlock-edit');
	}

	public function staticBlock_delete($id)
	{
		$staticBlock = $this->model->getStaticBlock($id);
		$result = $this->model->deleteStaticBlock($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('StaticBlock', 'delete', "Static Block deleted with id : $id and name : ".$staticBlock->name, $id);
			$this->AddNotice('Static Block deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('staticBlocks_list'));
	}

	public function staticBlock_enable($id)
	{
		if (!is_null($id)) {
			$staticBlock = $this->model->getStaticBlock($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$staticBlock->enabled = 1;
		$res = $this->model->saveStaticBlock($staticBlock);

		if (!is_array($res)) {
			$this->view->AddNotice('Static Block has been enabled successfully.');
			$this->LogsManager->registerLog('StaticBlock', 'update', 'Static Block enabled with id : '.$staticBlock->id, $staticBlock->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('staticBlocks_list'));
	}

	public function staticBlock_disable($id)
	{
		if (!is_null($id)) {
			$staticBlock = $this->model->getStaticBlock($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$staticBlock->enabled = 0;
		$res = $this->model->saveStaticBlock($staticBlock);

		if (!is_array($res)) {
			$this->view->AddNotice('Static Block has been disabled successfully.');
			$this->LogsManager->registerLog('StaticBlock', 'update', 'Static Block disabled with id : '.$staticBlock->id, $staticBlock->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect($this->getActionUrl('staticBlocks_list'));
	}
}
