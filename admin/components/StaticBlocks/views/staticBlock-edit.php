<?php
$name = isset($_POST['name']) ? $_POST['name'] : $staticBlock->name;

foreach (CMSSettings::$available_langs as $lang) {
	$title_{$lang} = isset($_POST['title_'.$lang]) ? $_POST['title_'.$lang] : ((!is_null($staticBlock->{$lang})) ? $staticBlock->{$lang}->title : '');
	$content_{$lang} = isset($_POST['content_'.$lang]) ? $_POST['content_'.$lang] : ((!is_null($staticBlock->{$lang})) ? $staticBlock->{$lang}->content : '');
}

?>
<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$StaticBlock]</h4>
        </div>
        <div class="panel-body panel-form">

            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true"
                  name="demo-form"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4" for="name">[$Name] *</label>

                    <div class="col-md-8 col-sm-8">

                        <input class="form-control" type="text" value="<?= $name ?>"
                               name="name" placeholder="[$Name]" data-parsley-required="true"
                               data-parsley-id="6524" required>

                    </div>
                </div>
                <br>

                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <?php foreach (CMSSettings::$available_langs as $lang) {
	?>
                            <li class="<?= ($lang == CMSSettings::$default_lang) ? 'active' : '' ?>"><a
                                    href="#tab_<?= $lang ?>" data-toggle="tab"><?= strtoupper($lang) ?></a></li>
                        <?php
} ?>
                    </ul>
                    <div class="tab-content">
                        <?php foreach (CMSSettings::$available_langs as $lang) {
		?>

                            <div
                                class="tab-pane fade <?= ($lang == CMSSettings::$default_lang) ? 'active in' : '' ?>"
                                id="tab_<?= $lang ?>">

                                <div class="form-group">
                                    <label class="control-label col-md-4 col-sm-4" for="name">[$Title]</label>

                                    <div class="col-md-8 col-sm-8">
                                        <input class="form-control" type="text" value="<?= $title_{$lang} ?>"
                                               name="title_<?= $lang ?>" placeholder="[$Title]">
                                    </div>
                                </div>

                                <textarea class="ckeditor" id="editor_<?= $lang ?>" name="content_<?= $lang ?>" rows="50"><?= $content_{$lang} ?></textarea>
                            </div>
                        <?php
	} ?>
                    </div>
                </div>
                <p class="text-center">
                    <button type="submit" name="save" class="btn btn-info">
                        <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                </p>
            </form>
        </div>
    </div>
</div>
<p>&nbsp;</p>


<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/parsley/src/parsley.css'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/parsley/dist/parsley.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/ckeditor/ckeditor.js'); ?>
<script>
    <?php foreach (CMSSettings::$available_langs as $lang) {
		?>
        CKEDITOR.replace('editor_<?= $lang ?>', {
                filebrowserBrowseUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                filebrowserImageBrowseUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
//                filebrowserUploadUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
//                filebrowserImageUploadUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
//                filebrowserBrowseLinkUrl : '../../../../components/Media/views/libs/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                allowedContent : true
            });
    <?php
	} ?>
</script>

