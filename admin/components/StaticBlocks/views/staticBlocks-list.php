<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('StaticBlocks');

foreach ($staticBlocks as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_staticBlock_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_staticBlock_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil"></i>', $url_staticBlock_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
	);
}
$table->setElements($staticBlocks);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
));
$table->setUrl_action($url_staticBlocks_list);
$table->setUrl_delete($url_staticBlock_delete);
$table->render();
