<?php

require_once DOCROOT.LIBS_PATH.'Email.php';

class Users_Component extends BaseComponent
{
	public function __construct()
	{
		parent::__construct();
		$this->view->set('url_users_list', Utils::getComponentUrl('Users/users_list'));
		$this->view->set('url_user_permissions', Utils::getComponentUrl('Users/manage_user'));
		$this->view->set('url_user_edit', Utils::getComponentUrl('Users/user_edit'));

		$this->view->set('url_user_details', Utils::getComponentUrl('Users/user_details'));
		$this->view->set('url_user_delete', Utils::getComponentUrl('Users/delete_user'));
		$this->view->set('url_user_enable', Utils::getComponentUrl('Users/enable_user'));
		$this->view->set('url_user_disable', Utils::getComponentUrl('Users/disable_user'));
		$this->view->set('url_permission_delete', Utils::getComponentUrl('Users/delete_permission'));
		$this->view->set('url_user_loginas', Utils::getComponentUrl('Users/login_as'));
		$this->view->set('url_user_loginasFrontend', Utils::getComponentUrl('Users/login_asFrontend'));
		$this->view->set('url_categories_list', Utils::getComponentUrl('Users/categories_list'));
		$this->view->set('url_category_edit', Utils::getComponentUrl('Users/category_edit'));
		$this->view->set('url_category_delete', Utils::getComponentUrl('Users/category_delete'));

		$this->view->set('url_models_list', Utils::getComponentUrl('Users/perm_models_list'));
		$this->view->set('url_model_edit', Utils::getComponentUrl('Users/perm_model_edit'));
		$this->view->set('url_model_delete', Utils::getComponentUrl('Users/perm_model_delete'));

		$this->view->set('url_permComponent_delete', Utils::getComponentUrl('Users/permComponent_delete'));
	}

	//    public function register() {
	//        HeadHTML::setTitleTag('Register' . ' | ' . DOMAIN);
	//        $this->view->setComponentLayout('empty');
	//        UserAuth::unsetLoginSession();
	//        if (UserAuth::checkLoginSession())
	//            Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
//
	//        if (isset($_POST['register'])) {
//
	//            (!isset($_POST['agree'])) ? Messages::addError("You must accept our terms and data policy!") : "";
	//            (!isset($_POST['firstname'])) ? Messages::addError("Firstname code is required!") : "";
	//            (!isset($_POST['lastname'])) ? Messages::addError("Lastname code is required!") : "";
	//            (!isset($_POST['username'])) ? Messages::addError("Username is required!") : "";
	//            (!isset($_POST['email'])) ? Messages::addError("Email code is required!") : "";
	//            (!isset($_POST['password'])) ? Messages::addError("Password code is required!") : "";
	//            ($_POST['password'] != $_POST['rpassword']) ? Messages::addError("Passwords don't match!") : "";
	//            (strlen($_POST['password']) > 20 || strlen($_POST['password']) < 6) ? Messages::addError("Password must be between 6 and 20 characters!") : "";
	//            (strlen($_POST['firstname']) > 25) ? Messages::addError("Firstname is too long!") : "";
	//            (strlen($_POST['lastname']) > 25) ? Messages::addError("Lastname is too long!") : "";
	//            (strlen($_POST['username']) > 25) ? Messages::addError("Username is too long!") : "";
	//            (strlen($_POST['email']) > 25) ? Messages::addError("Email is too long!") : "";
	//            ($this->model->existEmail($_POST['email'])) ? Messages::addError("Email already exist!") : '';
	//            ($this->model->existUsername($_POST['username'])) ? Messages::addError("Username already exist!") : '';
//
	//            if (!Messages::existErrors()) {
//
	//                $user = new User_Entity();
	//                $user->username = $_POST['username'];
	//                $user->email = $_POST['email'];
	//                $user->firstname = $_POST['firstname'];
	//                $user->lastname = $_POST['lastname'];
	//                $user->password = md5($_POST['password']);
	//                //$user->admin = 1;
	//                $res = $this->model->saveUser($user);
	//                if (!is_array($res)) {
	//                    Messages::addInfoMsg("You have been registered successfully. <br/>Contact your administrator to enable your login!");
	//                    $this->LogsManager->registerLog('User', 'register', "User registered with id : " . $res, $res);
	//                } else {
	//                    Messages::addError("Something went wrong!");
	//                    $this->LogsManager->registerLog('User', 'register', "User tried to register with firstname : {$_POST['firstname']} and lastname {$_POST['lastname']}");
	//                }
	//            }
	//        }
//
	//        $this->view->setLayout('essential');
	//        $this->view->render('register');
	//    }

	public function login()
	{
		HeadHTML::setTitleTag('Login'.' | '.DOMAIN);
		$this->view->setComponentLayout('empty');

		if (UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
		}

		$db = new Database(MAIN_DB_TYPE, MAIN_DB_HOST, MAIN_DB_NAME, MAIN_DB_USER, MAIN_DB_PASS);
		$this->model->setDatabase($db);
		$brand_info = $this->model->getBrandInfo();
		UserAuth::setBrandInfoSession($brand_info);

		$this->model->setDatabase(Database::getInstance());
		if (isset($_POST['login'])) {
			if (!Messages::existErrors()) {
				$user = array();
				$logged = UserAuth::checkAuthentication($_POST['username'], $_POST['password'], $user);
				if ($logged) {
					UserAuth::setLoginSession($user);

					//Vendosim last_access
					$usr = $this->model->getUser($user['id']);
					$usr->last_access = date('Y-m-d H:i:s');
					$this->model->saveUser($usr);

					$this->afterLogin($usr);

					$this->LogsManager->registerLog('User', 'login', 'User logged in with id : '.$user['id'], $user['id']);
					Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
				} else {
					$this->LogsManager->registerLog('User', 'login', 'User tried to log in with username : '.$_POST['username'].' and password : '.$_POST['password']);
					Messages::addError('Login failed !');
				}
			}
		}
		$this->view->setLayout('essential');
		$this->view->render('login_v2');
	}

	public function logout()
	{
		$loggeduser = UserAuth::getLoginSession();
		$this->LogsManager->registerLog('User', 'logout', 'User logged out with id : '.$loggeduser['id'], $loggeduser['id']);
		UserAuth::unsetLoginSession();
		Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
	}

	public function users_list()
	{
		$this->checkPermission();
		Utils::saveCurrentPageUrl();
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

		if (isset($_POST['delete_selected']) && $_POST['elements'] != '') {
			$elements = explode(',', $_POST['elements']);
			foreach ($elements as $id) {
				$user = $this->model->getUser($id);
				$res = $this->model->deleteUser($user);
				$this->LogsManager->registerLog('User', 'delete', 'User deleted with id : '.$user->id, $user->id);
			}

			if (!is_array($res)) {
				$this->view->AddNotice('Users has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo(Utils::getComponentUrl('Users/users_list'));
		}

		$logged_user = UserAuth::getLoginSession();

		$elements_per_page = 100;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$User_Table = TABLE_PREFIX.'User';
		$filter = "{$User_Table}.id != {$logged_user['id']} ";

		if ($logged_user['super'] == 0) {
			$filter .= "AND {$User_Table}.super = '0'";
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'User.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'User.firstname', 'peso' => 90),
				array('field' => TABLE_PREFIX.'User.lastname', 'peso' => 90),
				array('field' => TABLE_PREFIX.'User.email', 'peso' => 80),
			);
			$users = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$users = $this->model->getList($elements_per_page, ($page - 1) * $elements_per_page, $filter);
		}

		$totalElements = $this->model->getLastCounter();

		$this->view->setTitle('[$Users_List]');

		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);

		$this->view->set('users', $users);
		$this->view->addButton(new Button('[$NewUser]', $this->getActionUrl('user_edit')));
		$this->view->BreadCrumb->addDir('[$Users]', Utils::getComponentUrl('Users/users_list'));
		$this->view->render('users-list');
	}

	public function manage_user($id)
	{
		$this->checkPermission();
		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Users]', Utils::getComponentUrl('Users/users_list'));
		if (!is_null($id)) {
			$user = $this->model->getUser($id);
			$this->view->BreadCrumb->addDir('[$ManageUser]', Utils::getComponentUrl('Users/manage_user')."/$id");
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
		}

		HeadHTML::setTitleTag('Edit User'.' | '.CMSSettings::$website_title);

		$loggeduser = UserAuth::getLoginSession();

		if ($loggeduser['super'] == 0 && $loggeduser['id'] != $user->id) {
			$this->view->AddError("You don't have permission to do this action!");
			Utils::RedirectTo(Utils::getComponentUrl('Users/users_list'));
		}

		if (isset($_POST['save'])) {
			($_POST['password'] != $_POST['rpassword']) ? $this->view->AddError("Passwords don't match!") : '';
			(strlen($_POST['password']) < 6 || strlen($_POST['password']) > 50) ? $this->view->AddError('Password must be between 6 and 50 characters!') : '';

			if (count($this->view->getErrors()) == 0) {
				$user->password = md5($_POST['password']);

				if (isset($_POST['sendmail'])) {
					//I Dergojme email userit me passwordin e resetuar.
					$message = new BaseView();
					$message->setViewPath(VIEWS_PATH.'Users'.DS);
					$message->setLangPath(LANGS_PATH.'Users'.DS);
					$message->setLang($this->getLang());
					$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
					$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
					$message->placeholder('URL_LOGO')->setVal('http://'.$_SERVER['HTTP_HOST'].substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.CMSSettings::$logo);
					$message->placeholder('NEW_PASSWORD')->setVal($_POST['password']);

					$subject = $message->getTerm('PasswordReset').' - '.CMSSettings::$website_title;
					$message->renderTemplate(false);
					$message->set('user', $user);
					$txt = $message->render('mails/email_reset_pass', true);
					Email::sendMail($user->email, CMSSettings::$website_title, CMSSettings::$sender_mail, $subject, $txt);
				}

				$res = $this->model->saveUser($user);

				if (!is_array($res)) {
					$this->view->AddNotice('Password has been changed successfully.');
					$this->LogsManager->registerLog('User', 'update', 'Password changed for user with id : '.$user->id, $user->id);
				} else {
					$this->view->AddError('Something went wrong!');
				}
				Utils::RedirectTo(Utils::getComponentUrl('Users/manage_user/'.$id));
			}
		} elseif (isset($_POST['change'])) {
			$_POST['super'] = isset($_POST['super']) ? $_POST['super'] : '0';

			$user->super = $_POST['super'];
			$user->admin = $_POST['admin'];
			$res = $this->model->saveUser($user);
			if (!is_array($res)) {
				$this->view->AddNotice('User has been saved successfully.');
				$this->LogsManager->registerLog('User', 'update', 'User updated with id : '.$user->id, $user->id);
			} else {
				$this->view->AddError('Something went wrong!');
			}
			Utils::RedirectTo(Utils::getComponentUrl('Users/manage_user/'.$id));
		}

		if (isset($_POST['add']) && isset($_POST['comp_id'])) {
			foreach ($_POST['comp_id'] as $comp_id) {
				$up = new UserPermission_Entity();
				$up->id_component = $comp_id;
				$up->id_user = $id;

				$this->model->saveUserPermission($up);
			}
		}

		$this->view->set('user', $user);

		$comp_unpermitted = $this->model->getUnpermittedComponents($id);
		$comp_permitted = $this->model->getPermittedComponents($id);

		$this->view->set('comp_unpermitted', $comp_unpermitted);
		$this->view->set('comp_permitted', $comp_permitted);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getComponentUrl('Users/users_list')), '', '', '[$back]'));
		$this->view->setTitle('[$EditUser]');
		$this->view->set('user', $user);
		$this->view->render('user-edit');
	}

	public function user_edit($id = null)
	{
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
		$logged_user = UserAuth::getLoginSession();

		//        if ($logged_user['super'] == '0' && $logged_user['id'] != $id) {
		//            Messages::addError("Permission denied!");
		//            Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		//            return;
		//        }

		Utils::saveCurrentPageUrl();
		$this->view->BreadCrumb->addDir('[$Users]', Utils::getComponentUrl('Users/users_list'));
		$user_categories = $this->model->getUserCategList();
		if (!is_null($id)) {
			$user = $this->model->getUser($id);

			if (is_null($user)) {
				Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
			}
			HeadHTML::setTitleTag('Edit User'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$EditUser]', Utils::getComponentUrl('Users/user_edit'."/$id"));
			$this->view->setTitle('[$EditUser]');
		} else {
			$user = new User_Entity();
			$user->enabled = 1;
			HeadHTML::setTitleTag('New User'.' | '.CMSSettings::$website_title);
			$this->view->BreadCrumb->addDir('[$AddUser]', Utils::getComponentUrl('Users/user_edit'));
			$this->view->setTitle('[$AddUser]');
		}

		$comp_md = Loader::getModel('Components');
		$comp_available = $comp_md->getList();
		$comp_permitted = $this->model->getPermittedComponents($id);

		if (isset($_POST['save'])) {
			$_POST['username'] = trim($_POST['username']);
			$_POST['email'] = trim($_POST['email']);
			$_POST['firstname'] = trim($_POST['firstname']);
			$_POST['lastname'] = trim($_POST['lastname']);
			($_POST['username'] == '') ? $this->view->AddError('Username is required!') : '';
			($_POST['user_category'] == '') ? $this->view->AddError('User Category is required!') : '';
			($_POST['email'] == '') ? $this->view->AddError('Email is required!') : '';
			($_POST['firstname'] == '') ? $this->view->AddError('Firstname is required!') : '';
			($_POST['lastname'] == '') ? $this->view->AddError('Lastname is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('File must be an image!') : '';
			}

			if (is_null($id) || $_POST['username'] != $user->username) {
				($this->model->existUsername($_POST['username'])) ? $this->view->AddError('Username exist!') : '';
			}
			if (is_null($id) || $_POST['email'] != $user->email) {
				($this->model->existEmail($_POST['email'])) ? $this->view->AddError('Email exist!') : '';
			}

			if ($_POST['password'] == '' && $_POST['rpassword'] == '') {
				if ($user->id == '') {
					$new_password = Utils::GenerateRandomPassword(8);
					$user->password = md5($new_password);
					$user->plain_password = $new_password;
				}
			} elseif ($_POST['password'] != $_POST['rpassword']) {
				$this->view->AddError("Password don't match!");
			} else {
				$user->plain_password = $_POST['password'];
				$user->password = md5($_POST['password']);
			}

			if (count($this->view->getErrors()) == 0) {
				$user->enabled = (isset($_POST['enabled'])) ? '1' : '0';

				if (isset($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time().'_'.$_FILES['image']['name'];
					$old_image = $user->image;
				} else {
					$_POST['image'] = $user->image;
				}

				$user->username = $_POST['username'];
				if ($user->email != $_POST['email']) {
					$user->email = $_POST['email'];
					$user->verified = '0';
				}

				$user->firstname = ucwords($_POST['firstname']);
				$user->lastname = ucwords($_POST['lastname']);
				$user->image = $_POST['image'];
				$user->lang = (isset($_POST['lang']) ? $_POST['lang'] : CMSSettings::$default_lang);
				$user->admin = (isset($_POST['admin'])) ? '1' : '0';
				$user->super = ($user->admin) ? ((isset($_POST['super'])) ? '1' : '0') : '0';
				if (isset($_POST['user_category'])) {
					$user->id_category = $_POST['user_category'];
				}
				$inserted_id = $this->model->saveUser($user);

				if (!is_array($inserted_id)) {
					$this->view->AddNotice('User has been saved successfully.');

					$newComps = (isset($_POST['userPermissions'])) ? $_POST['userPermissions'] : array();

					foreach ($comp_available as $comp) {
						if (!in_array($comp->id, $newComps) && Utils::in_ObjectArray($comp_permitted, 'id_component', $comp->id)) {
							$this->deleteCompPerm($user->id, $comp->id);
						} elseif (in_array($comp->id, $newComps) && !Utils::in_ObjectArray($comp_permitted, 'id_component', $comp->id)) {
							$this->addCompPerm($user->id, $comp->id);
						}
					}

					if (isset($_FILES['image']['tmp_name'])) {
						$target = DOCROOT.MEDIA_ROOT.DS.'users';
						Utils::createDirectory($target);
						$filename = $target.DS.$_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
						if (!is_null($id)) {
							unlink($target.DS.$old_image);
						}
					}

					if (is_null($id)) {

						//I Dergojme email clientit me passwordin e resetuar.
						$message = new BaseView();
						$message->setViewPath(VIEWS_PATH.'Users'.DS);
						$message->setLangPath(LANGS_PATH.'Users'.DS);
						$message->setLang($this->getLang());
						$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
						$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
						$message->placeholder('URL_LOGO')->setVal('http://'.$_SERVER['HTTP_HOST'].substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.CMSSettings::$logo);
						$message->placeholder('NEW_PASSWORD')->setVal($new_password);

						$subject = 'New Account - '.CMSSettings::$website_title;
						$message->renderTemplate(false);
						$message->set('user', $user);
						$txt = $message->render('mails/email_new_account', true);
						Email::sendMail($user->email, CMSSettings::$website_title, CMSSettings::$sender_mail, $subject, $txt);
					} else {
						if (isset($_POST['sendmail']) && $_POST['password'] != '') {
							//I Dergojme email clientit me passwordin e resetuar.
							$message = new BaseView();
							$message->setViewPath(VIEWS_PATH.'Users'.DS);
							$message->setLangPath(LANGS_PATH.'Users'.DS);
							$message->setLang($this->getLang());
							$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
							$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
							$message->placeholder('URL_LOGO')->setVal('http://'.$_SERVER['HTTP_HOST'].substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.CMSSettings::$logo);
							$message->placeholder('NEW_PASSWORD')->setVal($_POST['password']);

							$subject = $message->getTerm('PasswordReset').' - '.CMSSettings::$website_title;
							$message->renderTemplate(false);
							$txt = $message->render('mails/email_reset_pass', true);
							Email::sendMail($user->email, CMSSettings::$website_title, CMSSettings::$sender_mail, $subject, $txt);
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('User', 'insert', 'User inserted with id : '.$inserted_id, $inserted_id);
						Utils::RedirectTo(Utils::getComponentUrl('Users/user_edit'."/$inserted_id"));
					} else {
						$this->LogsManager->registerLog('User', 'update', 'User updated with id : '.$id, $id);
						Utils::RedirectTo(Utils::getComponentUrl('Users/user_edit'."/$id"));
					}
				} else {
					$this->view->AddError('Problem while saving the user!');
					print_r($inserted_id);
				}
			}
		}
		//        elseif (isset($_POST['save_password']) && false) {
//
		//            ($_POST['password'] != $_POST['rpassword']) ? $this->view->AddError("Passwords don't match!") : '';
		//            (strlen($_POST['password']) < 6 || strlen($_POST['password']) > 50) ? $this->view->AddError("Password must be between 6 and 50 characters!") : '';
//
		//            if (count($this->view->getErrors()) == 0) {
//
		//                $user->password = md5($_POST['password']);
//
		//                if (isset($_POST['sendmail'])) {
		//                    //I Dergojme email clientit me passwordin e resetuar.
		//                    $message = new BaseView();
		//                    $message->setViewPath(VIEWS_PATH . 'Users' . DS);
		//                    $message->setLangPath(LANGS_PATH . 'Users' . DS);
		//                    $message->setLang($this->getLang());
		//                    $message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		//                    $message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		//                    $message->placeholder('URL_LOGO')->setVal('http://' . $_SERVER['HTTP_HOST'] . substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')) . MEDIA_ROOT . CMSSettings::$logo);
		//                    $message->placeholder('NEW_PASSWORD')->setVal($_POST['password']);
//
		//                    $subject = $message->getTerm('PasswordReset') . " - " . CMSSettings::$website_title;
		//                    $message->renderTemplate(false);
		//                    $txt = $message->render('mails/email_reset_pass', true);
		//                    Email::sendMail($user->email, CMSSettings::$website_title, CMSSettings::$sender_mail, $subject, $txt);
		//                }
//
		//                $res = $this->model->saveUser($user);
//
		//                if (!is_array($res)) {
		//                    $this->view->AddNotice("Password has been changed successfully.");
		//                    $this->LogsManager->registerLog('User', 'update', "Password changed for client with id : " . $user->id, $user->id);
		//                } else {
		//                    $this->view->AddError("Something went wrong!");
		//                }
		//                Utils::RedirectTo(Utils::getComponentUrl('Users/user_edit/' . $id));
		//            }
		//        }
		$this->view->set('user_categories', $user_categories);
		$this->view->set('user', $user);

		$comp_permitted = $this->model->getPermittedComponents($id);
		$this->view->set('comp_available', $comp_available);
		$this->view->set('comp_permitted', $comp_permitted);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getComponentUrl('Users/users_list')), '', '', '[$back]'));
		$this->view->render('user-edit');
	}

	public function user_details($id)
	{
		$this->checkPermission();
		Utils::saveCurrentPageUrl();

		if (!is_null($id)) {
			$user = $this->model->getUser($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
		}

		$logged_user = UserAuth::getLoginSession();
		if ($user->super == 1 && $logged_user['super'] != 1) {
			Messages::addError('Permission denied!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
			return;
		}

		$this->view->setTitle('[$UserDetails]');
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getComponentUrl('Users/users_list')), '', '', '[$back]'));
		$this->view->set('user', $user);
		$this->view->render('user-details');
	}

	public function delete_user($id = null)
	{
		$this->checkPermission();

		$user = $this->model->getUser($id);

		$logged_user = UserAuth::getLoginSession();
		if ($user->super == 1 && $logged_user['super'] != 1) {
			Messages::addError('Permission denied!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
			return;
		}

		$logged_user = UserAuth::getLoginSession();
		if ($id == $logged_user['id']) {
			$this->view->AddError('You can not delete yourself!');
			Utils::backRedirect(Utils::getComponentUrl('Dashboard'));
			return;
		}

		if ($this->model->deleteUser($user)) {
			$this->view->AddNotice('User has been deleted successfully.');
			$this->LogsManager->registerLog('User', 'delete', 'User deleted with id : '.$user->id, $user->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getComponentUrl('Users/users_list'));

		if (!is_null($id)) {
			$user = $this->model->deleteUser($user);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
		}
	}

	public function enable_user($id = null, $sendNotify = false)
	{
		$this->checkPermission();
		if (!is_null($id)) {
			$user = $this->model->getUser($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$logged_user = UserAuth::getLoginSession();
		if ($id == $logged_user['id']) {
			$this->view->AddError('You can not enable yourself!');
			Utils::backRedirect(Utils::getComponentUrl('Dashboard'));
			return;
		}

		if ($user->super == 1 && $logged_user['super'] != 1) {
			Messages::addError('Permission denied!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
			return;
		}

		$user->enabled = 1;
		$res = $this->model->saveUser($user);

		if (!is_array($res)) {
			$this->view->AddNotice('User has been enabled successfully.');
			$this->LogsManager->registerLog('User', 'update', 'User enabled with id : '.$user->id, $user->id);
			if ($sendNotify) {
				$notified = $this->sendEmailToUser($user, 'your_account_was_enabled');
			}
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getComponentUrl('Users/users_list'));
	}

	private function sendEmailToUser($user, $message_type)
	{
		$to = $user->email;
		$from_name = CMSSettings::$webdomain;
		$from = CMSSettings::$sender_mail;

		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH.'Users'.DS.VIEWS_PATH.DS);
		$message->setLangPath(COMPONENTS_PATH.'Users'.DS.LANGS_PATH.DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal('http://'.$_SERVER['HTTP_HOST'].substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.CMSSettings::$logo);
		$message->placeholder('NEW_PASSWORD')->setVal($new_password);
		$message->renderTemplate(false);
		$message->set('user', $user);

		switch ($message_type) {
			case 'your_account_was_enabled':
				$subject = $this->view->getTerm('registration_completed');
				$content = $message->render('mails/email_account_enabled', true);
				break;
			default:
				return false;
		}
		$sended = Email::sendMail($to, $from_name, $from, $subject, $content);

		return $sended;
	}

	public function disable_user($id = null)
	{
		$this->checkPermission();
		if (!is_null($id)) {
			$user = $this->model->getUser($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}

		$logged_user = UserAuth::getLoginSession();
		if ($id == $logged_user['id']) {
			$this->view->AddError('You can not disable yourself!');
			Utils::backRedirect(Utils::getComponentUrl('Dashboard'));
			return;
		}

		if ($user->super == 1 && $logged_user['super'] != 1) {
			Messages::addError('Permission denied!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
			return;
		}

		$user->enabled = 0;
		$res = $this->model->saveUser($user);

		if (!is_array($res)) {
			$this->view->AddNotice('User has been disabled successfully.');
			$this->LogsManager->registerLog('User', 'update', 'User disabled with id : '.$user->id, $user->id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getComponentUrl('Users/users_list'));
	}

	private function addCompPerm($user_id, $comp_id)
	{
		$logged_user = UserAuth::getLoginSession();
		if ($logged_user['super'] == 0) {
			Messages::addError("addCompPerm: You can't complete this action!");
			return false;
		}
		$res = $this->model->addUserPerm($user_id, $comp_id);
		//        if ($res !== false) {
		//            $this->view->AddNotice("Permission has been added successfully.");
		//        } else {
		//            $this->view->AddError("Something went wrong!");
		//        }
		return $res;
	}

	private function deleteCompPerm($user_id, $comp_id)
	{
		$logged_user = UserAuth::getLoginSession();
		if ($logged_user['super'] == 0) {
			Messages::addError("deleteCompPerm: You can't complete this action!");
			return false;
		}
		$res = $this->model->deleteUserPerm($user_id, $comp_id);
		//        if ($res !== false) {
		//            $this->view->AddNotice("Permission has been deleted successfully.");
		//        } else {
		//            $this->view->AddError("Something went wrong!");
		//        }
		return $res;
	}

	public function delete_permission($id)
	{
		$logged_user = UserAuth::getLoginSession();
		if ($logged_user['super'] == 0) {
			Messages::addError('Permission denied!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
			return;
		}

		$res = $this->model->deleteUserPermission($id);
		if ($res !== false) {
			$this->view->AddNotice('Permission has been deleted successfully.');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getComponentUrl("Users/manage_user/{$_GET['id_user']}"));
	}

	public function categories_list()
	{
		$this->checkPermission();
		Utils::saveCurrentPageUrl();
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

		$logged_user = UserAuth::getLoginSession();

		$elements_per_page = 15;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		if ($logged_user['super'] != 1) {
			Messages::addError('Permission denied!');
			Utils::backRedirect(Utils::getControllerUrl('Dashboard/index'));
			return;
		}

		$categories = $this->model->getUserCategList($elements_per_page, ($page - 1) * $elements_per_page, $filter);

		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($categories) == 0) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/categories_list'));
			return;
		}

		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);

		if ($logged_user['super'] == 1 || $logged_user['is_agency'] == 1) {
			$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddCategory]', Utils::getComponentUrl('Users/category_edit'), '', '', '[$AddCategory]'));
		}

		$this->view->set('categories', $categories);
		$this->view->BreadCrumb->addDir('[$Categories]', Utils::getComponentUrl('Users/categories_list'));
		$this->view->setTitle('[$Categories]');
		$this->view->render('categories-list');
	}

	public function category_edit($id = null)
	{
		$this->checkPermission();
		$logged_user = UserAuth::getLoginSession();
		Utils::saveCurrentPageUrl();
		if (!is_null($id)) {
			$category = $this->model->getUserCategory($id);
			HeadHTML::setTitleTag('Edit Category - '.$category->category.' | '.CMSSettings::$website_title);
		} else {
			$category = new UserCategory_Entity();
			HeadHTML::setTitleTag('New Category'.' | '.CMSSettings::$website_title);
		}

		if ($category === false) {
			$this->AddError("Category $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['category'] == '') ? $this->view->AddError('Category is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$category->category = $_POST['category'];
				$category->id_permission_model = ($_POST['id_permission_model'] != '') ? $_POST['id_permission_model'] : null;

				$inserted_id = $this->model->saveUserCategory($category);

				if (!is_array($inserted_id)) {
					$this->view->AddNotice('Category has been saved successfully.');

					if (is_null($id)) {
						$this->LogsManager->registerLog('UserCategory', 'insert', "User Category inserted with id : $inserted_id", $inserted_id);
						Utils::RedirectTo(Utils::getComponentUrl('Users/category_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('UserCategory', 'update', "User Category updated with id : $id", $id);
						Utils::RedirectTo(Utils::getComponentUrl('Users/category_edit')."/$id");
					}
				} else {
					$this->view->AddError('Something went wrong!');
					print_r($inserted_id);
				}
			}
		}

		$models = $this->model->getPermModelList();
		$this->view->set('models', $models);

		$this->view->set('category', $category);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getComponentUrl('Users/categories_list'))));
		$title = (!is_null($id)) ? '[$EditCategory]' : '[$NewCategory]';
		$this->view->setTitle($title);
		$this->view->render('category-edit');
	}

	public function category_delete($id = null)
	{
		$this->checkPermission();
		if (is_null($id)) {
			Messages::addError('Please select a category to delete!');
			Utils::backRedirect(Utils::getControllerUrl('Dashboard/index'));
			return;
		}
		$category = $this->model->getUserCategory($id);

		$res = $this->model->deleteUserCategory($id);
		if ($res !== false) {
			$this->LogsManager->registerLog('UserCategory', 'delete', "User category deleted with id : $id AND name: {$category->category}", $id);
			$this->view->AddNotice('Category has been deleted successfully.');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getComponentUrl('Users/categories_list'));
	}

	public function perm_models_list()
	{
		$this->checkPermission();
		Utils::saveCurrentPageUrl();
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

		$logged_user = UserAuth::getLoginSession();

		$elements_per_page = 15;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		if ($logged_user['super'] != 1) {
			Messages::addError('Permission denied!');
			Utils::backRedirect(Utils::getControllerUrl('Dashboard/index'));
			return;
		}

		$models = $this->model->getPermModelList($elements_per_page, ($page - 1) * $elements_per_page, $filter);

		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($models) == 0) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/perm_models_list'));
			return;
		}

		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);

		if ($logged_user['super'] == 1 || $logged_user['is_agency'] == 1) {
			$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddModel]', Utils::getComponentUrl('Users/perm_model_edit'), '', '', '[$AddModel]'));
		}

		$this->view->set('models', $models);
		$this->view->BreadCrumb->addDir('[$Models]', Utils::getComponentUrl('Users/perm_models_list'));
		$this->view->setTitle('[$Models]');
		$this->view->render('perm-models-list');
	}

	public function perm_model_edit($id = null)
	{
		$this->checkPermission();
		$logged_user = UserAuth::getLoginSession();
		Utils::saveCurrentPageUrl();
		if (!is_null($id)) {
			$model = $this->model->getUserPermModel($id);
			HeadHTML::setTitleTag('Edit Model - '.$model->name.' | '.CMSSettings::$website_title);
		} else {
			$model = new UserPermModel_Entity();
			HeadHTML::setTitleTag('New Model'.' | '.CMSSettings::$website_title);
		}

		if ($model === false) {
			$this->AddError("Model $id not found");
		} elseif (isset($_POST['save'])) {
			($_POST['name'] == '') ? $this->view->AddError('Model name is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$model->name = $_POST['name'];
				$model->description = $_POST['description'];

				$inserted_id = $this->model->saveUserPermModel($model);

				if (!is_array($inserted_id)) {
					$this->view->AddNotice('Permission Model has been saved successfully.');

					if (is_null($id)) {
						$this->LogsManager->registerLog('UserPermModel', 'insert', "Permission model inserted with id : $inserted_id", $inserted_id);
						Utils::RedirectTo(Utils::getComponentUrl('Users/perm_model_edit')."/$inserted_id");
					} else {
						$this->LogsManager->registerLog('UserPermModel', 'update', "Permission model updated with id : $id", $id);
						Utils::RedirectTo(Utils::getComponentUrl('Users/perm_model_edit')."/$id");
					}
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
		} elseif (isset($_POST['add'])) {
			(!isset($_POST['id_component']) || count($_POST['id_component']) == 0) ? $this->view->AddError('Component is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				foreach ($_POST['id_component'] as $id_comp) {
					$permModel = new PermissionModel_Entity();
					$permModel->id_component = $id_comp;
					$permModel->id_model = $id;

					$inserted_id = $this->model->savePermissionModel($permModel);
				}

				if (!is_array($inserted_id)) {
					$this->view->AddNotice('Permission has been saved successfully.');
					$this->LogsManager->registerLog('PermissionModel', 'insert', "Permission inserted with id : $inserted_id", $inserted_id);
				} else {
					$this->view->AddError('Something went wrong!');
				}
				Utils::RedirectTo(Utils::getComponentUrl('Users/perm_model_edit')."/$id");
			}
		}

		if (!is_null($id)) {
			$permittedComponents = $this->model->getPermissionModelList("id_model = $id");
			$this->view->set('permittedComponents', $permittedComponents);
			$ids = '';
			foreach ($permittedComponents as $pcomp) {
				$ids .= "{$pcomp->id_component},";
			}
			$ids = rtrim($ids, ',');

			$components_md = Loader::getModel('Components');
			$filter = '1 ';
			if ($ids != '') {
				$filter .= "AND id NOT IN ($ids) ";
			}
			$components = $components_md->getList(100, 0, $filter);
			$this->view->set('components', $components);
		}

		$this->view->set('model', $model);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getComponentUrl('Users/perm_models_list'))));
		$title = (!is_null($id)) ? '[$EditModel]' : '[$NewModel]';
		$this->view->setTitle($title);
		$this->view->render('perm-model-edit');
	}

	public function perm_model_delete($id = null)
	{
		$this->checkPermission();
		if (is_null($id)) {
			Messages::addError('Please select a model to delete!');
			Utils::backRedirect(Utils::getControllerUrl('Dashboard/index'));
			return;
		}
		$model = $this->model->getUserPermModel($id);

		$res = $this->model->deleteUserPermModel($id);
		if ($res !== false) {
			$this->LogsManager->registerLog('UserPermModel', 'delete', "User permission model deleted with id : $id AND name: {$model->name}", $id);
			$this->view->AddNotice('Model has been deleted successfully.');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getComponentUrl('Users/models_list'));
	}

	public function permComponent_delete($id = null)
	{
		if (is_null($id)) {
			Messages::addError('Please select an item to delete!');
			Utils::backRedirect(Utils::getControllerUrl('Dashboard/index'));
			return;
		}
		$permModel = $this->model->getPermissionModel($id);

		$res = $this->model->deletePermissionModel($id);
		if ($res !== false) {
			$this->LogsManager->registerLog('PermissionModel', 'delete', "Permission model deleted with id : $id AND id_component: {$permModel->id_component}", $id);
			$this->view->AddNotice('Permission has been deleted successfully.');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getComponentUrl('Users/perm_model_edit'."/{$permModel->id_model}"));
	}

	public function login_as($id = null)
	{
		$logged_user = UserAuth::getLoginSession();

		if ($logged_user['super'] == 0) {
			Messages::addError('Permission denied!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
			return;
		}

		if (is_null($id)) {
			$this->view->AddError('You have to select a user to login as!');
			Utils::backRedirect(Utils::getControllerUrl('Dashboard'));
			return;
		}

		$UserLoginAs = $this->model->getUser($id);
		$UserLoginAs = Utils::ObjectToArray($UserLoginAs);

		UserAuth::setPreviousLogin($logged_user);
		UserAuth::unsetLoginSession();

		UserAuth::setLoginSession($UserLoginAs);
		Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
	}

	public function login_asFrontend($id = null)
	{
		$logged_user = UserAuth::getLoginSession();

		if ($logged_user['admin'] == 0) {
			Messages::addError('Permission denied!');
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
			return;
		}

		if (is_null($id)) {
			$this->view->AddError('You have to select a user to login as!');
			Utils::backRedirect(Utils::getControllerUrl('Dashboard'));
			return;
		}

		$UserLoginAs = $this->model->getUser($id);
		$UserLoginAs = Utils::ObjectToArray($UserLoginAs);

		UserAuth::setLoginSessionFrontend($UserLoginAs);
		Utils::RedirectTo('http://'.CMSSettings::$webdomain);
	}

	public function previousLogin()
	{
		if (!UserAuth::checkPreviousLogin()) {
			$this->view->AddError('There is no a previous login!');
			Utils::backRedirect(Utils::getControllerUrl('Dashboard'));
			return;
		}

		$UserPrevious = UserAuth::getPreviousLogin();

		UserAuth::unsetPreviousLogin();
		UserAuth::unsetLoginSession();
		UserAuth::setLoginSession($UserPrevious);
		Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
	}

	private function checkPermission()
	{
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
		if (!UserAuth::checkComponentPerms('Users')) {
			Utils::RedirectTo(WEBROOT.'Dashboard');
		}
	}

	public function afterLogin($user)
	{
	}

	public function ajx_UsersSelectionList()
	{
		$id = (isset($_GET['id'])) ? $_GET['id'] : null;

		if (isset($_POST['query']) && $_POST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'User.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'User.firstname', 'peso' => 90),
				array('field' => TABLE_PREFIX.'User.lastname', 'peso' => 90),
			);
			$users = $this->model->search($_POST['query'], $searchFields, "AND enabled = '1'", '', 100, 0);
		} else {
			$users = $this->model->getList(20, 0, " AND enabled = '1'");
		}

		if (count($users)) {
			require_once DOCROOT.WEBROOT.VIEWS_PATH.'SelectionList_class.php';

			$table = new SelectionList_Table('Users', $id);

			$table->setElements($users);
			$table->setFields(array(
				new SelectionList_Field('id', $this->view->getTerm('Id'), 'int', 'left'),
				new SelectionList_Field('fullname', $this->view->getTerm('Name'), 'string', 'left'),
				new SelectionList_Field('enabled', $this->view->getTerm('Enabled'), 'bool', 'left'),
			));
			$table->renderTable();
		} else {
			echo '<p class="text-center">No results</p>';
		}
	}
}
