<?php
$category_name = isset($_POST['category']) ? $_POST['category'] : $category->category;
$id_permission_model = isset($_POST['id_permission_model']) ? $_POST['id_permission_model'] : $category->id_permission_model;
?>

<div class="ui-sortable">
    <div class="col-md-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$Category]</h4>
            </div>
            <div class="panel-body">
                <form role="form" method="post" class="form-bordered form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">[$CategoryName]<span
                                class="required">*</span></label>
                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" name="category" placeholder="[$Name]" required=""
                                   value="<?= $category_name ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">[$Model]<span
                                class="required">*</span></label>
                        <div class="col-md-8 col-sm-8">
                            <select class="form-control" name="id_permission_model">
                                <option value="">[$NoModel]</option>
                                <?php foreach ($models as $model) {
	?>
                                    <option
                                        value="<?= $model->id ?>" <?= ($id_permission_model == $model->id) ? 'selected' : '' ?>><?= $model->name ?></option>
                                <?php
} ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12 form-group">
                        <p>&nbsp;</p>
                        <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                        </button>
                        <p>&nbsp;</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>