<?php
$model_name = isset($_POST['name']) ? $_POST['name'] : $model->name;
$description = isset($_POST['description']) ? $_POST['description'] : $model->description;
?>


<div class="col-md-6">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$Model]</h4>
        </div>
        <div class="panel-body">
            <form role="form" method="post" class="form-bordered form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Name]<span
                            class="required">*</span></label>
                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control" name="name" placeholder="[$Name]" required=""
                               value="<?= $model_name ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Description]</label>
                    <div class="col-md-8">
                            <textarea name="description" class="form-control" placeholder="Indirizzo" rows="4"
                                      style="max-width:100%"><?= $description ?></textarea>
                    </div>
                </div>

                <div class="col-md-12 form-group">
                    <p>&nbsp;</p>
                    <button type="submit" name="save" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                    <p>&nbsp;</p>
                </div>
            </form>
        </div>
    </div>
</div>

<?php if (!is_null($model->id)) {
	?>
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">[$AddPermission]</h4>
            </div>
            <div class="panel-body panel-form">
                <p>&nbsp;</p>
                <?php
				include(VIEWS_PATH.'TableList_class.php');
	$table = new TableListView('Users');

	$table->setElements($permittedComponents);
	$table->setTotalElements(count($permittedComponents));
	$table->setElements_per_page(count($permittedComponents));
	$table->setFields(array(
					new TableList_Field('id_component', '[$Id]', 'int', 'left'),
					new TableList_Field('component_name', '[$Component]', 'string', 'left'),
				));
	$table->setUrl_delete($url_permComponent_delete);
	$table->multipleDeletion(false);
	$table->renderTopBar(false);
	$table->renderPanel(false);
	$table->showResultInfo(false);
	$table->render(); ?>
                <hr>

                <div id="new-comp-div">
                    <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true"
                          name="demo-form" novalidate="">
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$Component]</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_component[]" multiple style="height: 150px">
                                    <?php foreach ($components as $component) {
		?>
                                        <option value="<?= $component->id ?>"><?= $component->name ?></option>
                                    <?php
	} ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 text-right">
                            <p>&nbsp;</p>
                            <button type="submit" name="add" class="btn btn-info"><i
                                    class="fa fa-plus"></i>&nbsp;&nbsp;[$Add]
                            </button>
                            <p>&nbsp;</p>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- end panel -->
    </div>

<?php
} ?>
