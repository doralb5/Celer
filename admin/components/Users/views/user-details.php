<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-5">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$UserDetails] - <span
                    class=""><?= $user->firstname ?> <?= $user->lastname ?></span></h4>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered personalinfo">
                    <tbody>
                    <tr>
                        <td>[$Id]</td>
                        <td><b><?= $user->id ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Firstname]</td>
                        <td><b><?= $user->firstname ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Lastname]</td>
                        <td><b><?= $user->lastname ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Image]</td>
                        <?php
						if ($user->image != '') {
							$path = substr(WEBROOT, 0, -6).MEDIA_ROOT.'users/'.$user->image;
							if (file_exists(DOCROOT.$path)) {
								$image = "<img width='100px' src='{$path}' alt='Profile Image'/>";
							} else {
								$image = 'No Image';
							}
						} else {
							$image = 'No Image';
						}
						?>
                        <td><?= $image ?></td>
                    </tr>
                    <tr>
                        <td>[$Username]</td>
                        <td><b><?= $user->username ?></b></td>
                    </tr>
                    <tr>
                        <td>[$Email]</td>
                        <td><b><?= $user->email ?></b></td>
                    </tr>
                    <tr>
                        <td>[$CreationDate]</td>
                        <td><?= date('d/m/Y H:i', strtotime($user->creation_date)) ?></td>
                    </tr>
                    <tr>
                        <td>[$LastAccess]</td>
                        <td><?= date('d/m/Y H:i', strtotime($user->last_access)) ?></td>
                    </tr>
                    <tr>
                        <td>[$Super]</td>
                        <td>
                            <b><?= ($user->super) ? '<i class="fa fa-check-square-o"></i>' : '<i class="fa fa-square-o"></i>' ?></b>
                        </td>
                    </tr>
                    <tr>
                        <td>[$Enabled]</td>
                        <td>
                            <b><?= ($user->enabled) ? '<i class="fa fa-check-square-o"></i>' : '<i class="fa fa-square-o"></i>' ?></b>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
