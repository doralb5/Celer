<?php $loggeduser = UserAuth::getLoginSession(); ?>

<?php
// @var $user User_Entity
$username = isset($_POST['username']) ? $_POST['username'] : $user->username;
$user_enabled = isset($_POST['enabled']) ? $_POST['enabled'] : $user->enabled;
$email = isset($_POST['email']) ? $_POST['email'] : $user->email;
$email_verified = isset($_POST['email_verified']) ? $_POST['email_verified'] : $user->verified;
$user_category = isset($_POST['user_category']) ? $_POST['user_category'] : $user->id_category;

$firstname = isset($_POST['firstname']) ? $_POST['firstname'] : $user->firstname;
$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : $user->lastname;
$lang = isset($_POST['lang']) ? $_POST['lang'] : $user->lang;

$user_admin = isset($_POST['admin']) ? $_POST['admin'] : $user->admin;
$user_super = isset($_POST['super']) ? $_POST['super'] : $user->super;
?>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>



<style>
    label.required > span { color: red; font-size: 1.3em;}
</style>



<?php
//include(VIEWS_PATH . 'TableList_class.php');
//
//$table = new TableListView('Users');
//
//$table->setElements($comp_permitted);
//
//$table->setTotalElements(count($comp_permitted));
//$table->setElements_per_page(100);
//
//$table->setFields(array(
//    new TableList_Field('id', '[$Id]', 'int', 'left'),
//    new TableList_Field('comp_name', '[$ComponentName]', 'string', 'left'),
//));
//$table->setUrl_delete($url_permission_delete);
//$table->setUrl_delete_params(array("id_user" => $user->id));
//$table->multipleDeletion(false);
//$table->renderTopBar(false);
//$table->setTitle('[$UserPermissions]');
?>


<form method="post" action="" class="form-horizontal">

    <div class="col-md-12">
        
        
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-2">[$Enable_Account]</label>
                        <div class="col-sm-8">
                            <input <?= ($user_enabled) ? 'checked' : '' ?> data-toggle="toggle" data-on="Enabled" data-off="Disabled" type="checkbox" name="enabled">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 required">[$Username]<span>*</span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="username" placeholder="[$Username]" value="<?= $username ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 required">[$UserCategory]<span>*</span></label>
                        <div class="col-sm-4">
                            <select name="user_category" class="form-control">
                                <?php foreach ($user_categories as $c) {
	$select = ($c->id == $user_category) ? 'selected' : '';
	echo "<option value=\"$c->id\" $select>$c->category</option>";
}?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 required">[$Password]</label>
                        <div class="col-sm-2">
                            <input type="password" class="form-control" name="password" placeholder="[$NewPassword]" value="">
                        </div>
                        <div class="col-sm-2">
                            <input type="password" class="form-control" name="rpassword" placeholder="[$RepeatPassword]" value="">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">
                            <input type="checkbox" name="sendmail" value="1"> [$SendNotify]
                            </label>
                        </div>
                        
                    </div>
                </div>

                <div class="panel-footer">
                    <div class="text-right">
                        <button type="submit" name="delete" tabindex="-1" class="btn btn-danger">
                            <i class="fa fa-minus"></i>&nbsp;&nbsp;[$Delete]
                        </button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="submit" name="save" class="btn btn-info">
                            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                        </button>
                    </div>
                </div>
            </div>
        </div>


        
        <ul  class="nav nav-tabs">
            <li class="active"><a  href="#tab-information" data-toggle="tab">[$UserInformation]</a></li>
            <li><a href="#tab-authorizations" data-toggle="tab">[$Authorizations]</a></li>
            <li><a href="#tab-logs" data-toggle="tab">[$Logs]</a></li>
        </ul>

        <div class="tab-content clearfix">

            <div class="tab-pane active" id="tab-information">

                <div class="tab-content clearfix">
                
                    <div class="form-group">
                        <label class="control-label col-sm-2 required">[$Firstname]<span>*</span></label>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" value="<?= $firstname ?>"
                               name="firstname" placeholder="[$Firstname]" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 required">[$Lastname]<span>*</span></label>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" value="<?= $lastname ?>"
                               name="lastname" placeholder="[$Lastname]" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 required">[$Email]<span>*</span></label>
                        <div class="col-sm-4">
                            <input class="form-control" type="email" value="<?= $email ?>"
                               name="email" placeholder="[$Email]" required>
                        </div>
                        <div class="col-sm-1">
                            <i class="fa fa-check" style="color:<?= ($email_verified) ? 'green' : 'grey' ?>"></i>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">[$Language]</label>
                        <div class="col-sm-2">
                            <select name="lang" class="form-control">
                                <?php foreach (CMSSettings::$available_langs as $l) {
	$select = ($l == $userlang) ? 'selected' : '';
	echo "<option value=\"$l\" $select>$l</option>";
}?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">[$Image]</label>
                        <div class="col-sm-8">
                            <input type="file" name="image">
                        <?php if ($user->image != '') {
	?>
                                <img src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'users/'.$user->image ?>"
                                    width="100px" alt="Image"/>
                        <?php
} ?>
                        </div>
                    </div>
                    
                </div>
                
                <div class="panel-footer">
                    <div class="text-right">
                        <button type="submit" name="save" class="btn btn-info">
                            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                        </button>
                    </div>
                </div>
                
            </div>



            <div class="tab-pane" id="tab-authorizations">

                <div class="tab-content clearfix">
                    <div class="form-group">
                        <label class="control-label col-sm-2">[$BackEnd_Access]</label>
                        <div class="col-sm-1">
                            <input <?= ($user_admin) ? 'checked' : '' ?> data-toggle="toggle" data-on="[$Yes]" data-off="[$No]" type="checkbox" name="admin" value="1">
                        </div>
                        <label class="control-label col-sm-2">[$isSuper]</label>
                        <div class="col-sm-1">
                            <input <?= ($user_super) ? 'checked' : '' ?> data-toggle="toggle" data-on="[$Yes]" data-off="[$No]" type="checkbox" name="super" value="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">[$Permissions]</label>
                        <div class="col-sm-8">
                            
                            
                            <select id="perm-select" name="userPermissions[]" multiple="multiple">
                            <?php
								$comp_options = array();
								foreach ($comp_permitted as $comp) {
									array_push($comp_options, $comp->id_component);
								}

								foreach ($comp_available as $comp) {
									if (Utils::in_ObjectArray($comp_permitted, 'id_component', $comp->id)) {
										$selected = ' data-index="'.$comp->id.'" ';
									} else {
										$selected = '';
									}
									echo "<option value=\"$comp->id\" data-section=\"\" $selected>$comp->name</option>\n";
								}
							?>
                          </select>
                            <?php HeadHTML::AddJS(WEBROOT.'helpers/tree-multiselect/jquery.tree-multiselect.min.js'); ?>
                            <?php HeadHTML::AddStylesheet(WEBROOT.'helpers/tree-multiselect/jquery.tree-multiselect.min.css'); ?>
                            <script type="text/javascript">
                                $("#perm-select").treeMultiselect({searchable: true, searchParams: ['section', 'text']});
                            </script>
                            
                            
                        </div>
                    </div>

                </div>

                <div class="panel-footer">
                    <div class="text-right">
                        <button type="submit" name="save" class="btn btn-info">
                            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                        </button>
                    </div>
                </div>
            </div>


            <div class="tab-pane" id="tab-logs">

                <div class="tab-content clearfix">
                    <label class="control-label col-sm-2">Logs</label>
                    <div class="col-sm-8">
                        .....
                    </div>
                </div>
            </div>
        </div>

    </div>

</form>