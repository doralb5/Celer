<?php

class Audits extends BaseController
{
	public function __construct()
	{
		parent::__construct();

		$this->view->set('url_logs_list', Utils::getComponentUrl('Audits/logs_list'));

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
		if (!UserAuth::checkComponentPerms('Audits')) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard/index'));
		}
		$this->view->setLayout('default');
	}

	public function logs_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Components | '.CMSSettings::$website_title);
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$filter = '';
		$sorting = '';

		$elements_per_page = 30;
		$offset = ($page - 1) * $elements_per_page;

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'id', 'peso' => 100),
				array('field' => 'username', 'peso' => 90),
				array('field' => 'object', 'peso' => 80),
				array('field' => 'action', 'peso' => 80),
				array('field' => 'ip', 'peso' => 50),
			);
			$logs = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$logs = $this->model->getList($elements_per_page, ($page - 1) * $elements_per_page);
		}

		$totalElements = $this->model->getLastCounter();

		if ($page != 1 && count($logs) == 0) {
			Utils::RedirectTo(Utils::getControllerUrl('Audits/logs_list'));
			return;
		}

		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);

		$this->view->set('logs', $logs);

		$this->view->setTitle('[$Audits]');
		$this->view->BreadCrumb->addDir('[$Audits]', Utils::getControllerUrl('Audits/logs_list'));
		$this->view->render('logs-list');
	}
}
