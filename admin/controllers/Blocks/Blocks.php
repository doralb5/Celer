<?php

class Blocks extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->view->set('url_blockslist', Utils::getComponentUrl('Blocks/always_blocks'));
		$this->view->set('url_edit_block', Utils::getComponentUrl('Blocks/edit_block'));
		$this->view->set('url_delete_block', Utils::getComponentUrl('Blocks/delete_block'));
		$this->view->set('url_search_block', Utils::getComponentUrl('Blocks/blocks_list'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/Login'));
		}
		if (!UserAuth::checkComponentPerms('Blocks')) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$this->view->setLayout('default');
	}

	public function blocks_list()
	{
		Utils::saveCurrentPageUrl();
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		if (isset($_POST['save'])) {
			$block = new Block_Entity();

			$block->always = '1';
			$block->id_module = $_POST['id_module'];
			$block->position = $_POST['position'];
			$block->sorting = $_POST['sorting'];
			$block->enabled = (isset($_POST['enabled']) ? 1 : 0);
			$this->model->saveBlock($block);
		}

		if (isset($_POST['filter'])) {
			$_SESSION['layout'] = $_POST['layout'];
		} else {
			$_SESSION['layout'] = 1;
		}

		$template_name = CMSSettings::$template;
		$config_file = $template_name.DS.'config.xml';

		$layouts_md = Loader::getModel('Layouts');
		$layouts = $layouts_md->getLayoutsListByXml($template_name);
		$positions = $layouts_md->getPositionsListByXml($template_name, 'default');

		$this->view->set('layouts', $layouts);
		$this->view->set('positions', $positions);

		//Manaxhimi i moduleve
		$modules_md = Loader::getModel('Modules');
		$modules = $modules_md->getList(30, 0, "enabled = '1'");

		foreach ($modules as &$mod) {
			$mod->settings = $this->model->getModuleByXML($mod->name);
		}
		$this->view->set('modules', $modules);

		$elements_per_page = 15;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = "always = '1'";

		$blocks = $this->model->getList($elements_per_page, ($page - 1) * $elements_per_page);

		$totalElements = $this->model->getLastCounter();

		$this->view->set('always', true);
		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('blocks', $blocks);
		$this->view->setTitle('Always Blocks');
		$this->view->render('add-page-blocks');
	}

	public function edit_block($id)
	{
		Utils::saveCurrentPageUrl();
		$block = $this->model->getBlock($id);
		$this->view->set('block', $block);

		if (isset($_POST['change'])) {
			$block->position = $_POST['position'];
			$block->sorting = $_POST['sorting'];
			$block->enabled = (isset($_POST['enabled'])) ? 1 : 0;
			$res = $this->model->saveBlock($block);
			if (!is_array($res)) {
				$this->view->AddNotice('The block has been saved successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}

		if (isset($_POST['save_params'])) {
			unset($_POST['save_params']);

			foreach ($_POST as $key => $value) {
				if ($value == '') {
					unset($_POST[$key]);
				}
			}
			$json_data = (count($_POST)) ? json_encode($_POST) : '';

			$block->parameters = $json_data;
			$res = $this->model->saveBlock($block);

			if (!is_array($res)) {
				$this->view->AddNotice('The parameters has been saved successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}

		$module = $this->model->getModuleByXML($block->module_name);
		$module['values'] = json_decode($block->parameters, true);
		$this->view->set('module', $module);

		$template_name = CMSSettings::$template;
		$layouts_md = Loader::getModel('Layouts');
		$positions = $layouts_md->getPositionsListByXml($template_name, 'default');
		$this->view->set('positions', $positions);

		$this->view->set('positions', $positions);
		$this->view->set('block', $block);
		$this->view->set('always', true);

		$this->view->setTitle('Edit Block');
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getControllerUrl('Blocks/blocks_list/'))));
		$this->view->render('edit-block');
	}

	public function delete_block($id)
	{
		$res = $this->model->deleteBlock($id);
		if ($res !== false) {
			$this->view->AddNotice('Block has been deleted successfully!');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getControllerUrl('Blocks/blocks_list'));
	}
}
