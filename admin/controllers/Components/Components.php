<?php

class Components extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->view->set('url_components_list', Utils::getComponentUrl('Components/components_list'));
		$this->view->set('url_component_search', Utils::getComponentUrl('Components/components_list'));
		$this->view->set('url_component_enable', Utils::getComponentUrl('Components/enableComponent'));
		$this->view->set('url_component_disable', Utils::getComponentUrl('Components/disableComponent'));
		$this->view->set('url_component_delete', Utils::getComponentUrl('Components/deleteComponent'));
		$this->view->set('url_component_manage', Utils::getComponentUrl('Components/manage_component'));
		$this->view->set('url_component_permissions', Utils::getComponentUrl('Components/manage_permissions'));
		$this->view->set('url_permission_delete', Utils::getComponentUrl('Components/delete_permission'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
		if (!UserAuth::checkComponentPerms('Components')) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$this->view->setLayout('default');
	}

	public function components_list()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('Components | '.CMSSettings::$website_title);
		Utils::saveCurrentPageUrl();
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

		$elements_per_page = 15;
		$offset = ($page - 1) * $elements_per_page;
		$filter = '';
		$sorting = '';

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'Component.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'Component.name', 'peso' => 90),
			);
			$components = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$components = $this->model->getList($elements_per_page, ($page - 1) * $elements_per_page);
		}

		$totalElements = $this->model->getLastCounter();

		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);

		$this->view->set('components', $components);

		$this->view->setTitle('[$components]');
		$this->view->addButton(new Button('<i class="fa fa-puzzle-piece"></i>&nbsp;&nbsp;[$install_component]', Utils::getControllerUrl('Components/install_component')));
		$this->view->render('components-list');
	}

	public function manage_component($id)
	{
		Utils::saveCurrentPageUrl();
		$component = $this->model->getComponent($id);

		if (isset($_POST['change'])) {
			$component->featured = $_POST['featured'];
			$res = $this->model->saveComponent($component);
			if (!is_array($res)) {
				$this->view->AddNotice('Component has been saved successfully!');
				$this->LogsManager->registerLog('Component', 'update', 'Component updated with id : '.$component->id, $component->id);
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}

		if (isset($_POST['save'])) {
			unset($_POST['save']);

			foreach ($_POST as $key => $value) {
				if ($value == '') {
					unset($_POST[$key]);
				}
			}
			$json_data = (count($_POST)) ? json_encode($_POST) : '';

			$component->settings = $json_data;
			$res = $this->model->saveComponent($component);

			if (!is_array($res)) {
				$this->view->AddNotice('The parameters has been updated successfully.');
				$this->LogsManager->registerLog('Component', 'update', 'Settings of component updated with id : '.$component->id, $component->id);
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}
		$settings = $this->model->getComponentSettingsByXml($component);

		$this->view->set('component', $component);
		$this->view->set('settings', $settings);

		$this->view->setTitle('[$ManageComponent] - '.$component->name);
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getControllerUrl('Components/components_list')), '', '', '[$Back]'));
		$this->view->render('component-manage');
	}

	public function manage_permissions($id)
	{
		Utils::saveCurrentPageUrl();
		$UserPermission = new UserPermission_Entity();
		$user_md = Loader::getModel('Users');

		if (isset($_POST['add'])) {
			foreach ($_POST['upuser_id'] as $user_id) {
				$UserPermission->id_component = $id;
				$UserPermission->id_user = $user_id;
				$user_md->saveUserPermission($UserPermission);
				$this->LogsManager->registerLog('UserPermission', 'insert', 'User permission inserted with id of user : '.$UserPermission->id_user.' and id of component : '.$UserPermission->id_component, $UserPermission->id);
			}
		}

		if (isset($_POST['remove'])) {
			foreach ($_POST['puser_id'] as $user_id) {
				$user_md->deleteUserPermByComp($id, $user_id);
				$this->LogsManager->registerLog('UserPermission', 'delete', 'User permission deleted with id of user : '.$user_id.' and id of component : '.$id);
			}
		}

		$component = $this->model->getComponent($id);

		$users_unpermitted = $this->model->getUnpermittedUsers($id);
		$users_permitted = $this->model->getPermittedUsers($id);

		$this->view->set('component', $component);
		$this->view->set('users_unpermitted', $users_unpermitted);
		$this->view->set('users_permitted', $users_permitted);

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getControllerUrl('Components/components_list')), '', '', '[$Back]'));
		$this->view->setTitle('[$components]');
		$this->view->render('component-permissions');
	}

	public function delete_permission($id)
	{
		$user_md = Loader::getModel('Users');
		$res = $user_md->deleteUserPermission($id);

		if ($res) {
			$this->view->AddNotice('Permission has been deleted successfully.');
			$this->LogsManager->registerLog('UserPermission', 'delete', 'User permission deleted with id : '.$id, $id);
		} else {
			$this->view->AddError('Something went wrong!');
		}

		Utils::backRedirect(Utils::getControllerUrl('Components/manage_permissions')."/{$_GET['id_comp']}");
	}

	public function install_component()
	{
		Utils::saveCurrentPageUrl();
		$Component = new Component_Entity();
		if (isset($_POST['install'])) {
			(!is_null($this->model->getComponent($_POST['name']))) ? $this->view->AddError('This component already exists!') : '';
			(empty($_POST['name'])) ? $this->view->AddError('Name is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				if ($_POST['package'] != '' && $_POST['package'] != $_POST['name']) {
					$cname = $_POST['package'].'/'.$_POST['name'];
				} else {
					$cname = $_POST['name'];
				}

				$Component->name = $cname;
				$Component->type = $_POST['type'];
				$Component->featured = $_POST['featured'];
				$Component->sorting = $_POST['sorting'];

				$result = $this->model->saveComponent($Component);

				if (!is_array($result)) {
					$this->view->AddNotice('The component has been installed succesfully.');
					$this->LogsManager->registerLog('Component', 'insert', 'Component installed with id : '.$result, $result);
					Utils::RedirectTo(Utils::getComponentUrl('Components/components_list'));
				} else {
					$this->view->AddError('There was an error during installation!');
				}
			}
		}

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getControllerUrl('Components/components_list')), '', '', '[$Back]'));
		$this->view->setTitle('[$install_component]');
		$this->view->render('install-component');
	}

	public function deleteComponent($id = null)
	{
		if (!is_null($id)) {
			$component = $this->model->getComponent($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
		}
		$res = $this->model->deleteComponent($id);

		if ($res !== false) {
			$this->view->AddNotice('Component has been deleted successfully.');
			$this->LogsManager->registerLog('Component', 'delete', 'Component deleted with id : '.$id, $id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getControllerUrl('Components/components_list'));
	}

	public function enableComponent($id = null)
	{
		if (!is_null($id)) {
			$component = $this->model->getComponent($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
		}
		$component->enabled = '1';
		$res = $this->model->saveComponent($component);
		if (!is_array($res)) {
			$this->view->AddNotice('Component has been enabled successfully.');
			$this->LogsManager->registerLog('Component', 'update', 'Component enabled with id : '.$id, $id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getControllerUrl('Components/components_list'));
	}

	public function disableComponent($id = null)
	{
		if (!is_null($id)) {
			$component = $this->model->getComponent($id);
		} else {
			$this->view->AddError('Something went wrong!');
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
		}
		$component->enabled = '0';
		$res = $this->model->saveComponent($component);
		if (!is_array($res)) {
			$this->view->AddNotice('Component has been disabled successfully.');
			$this->LogsManager->registerLog('Component', 'update', 'Component disabled with id : '.$id, $id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getControllerUrl('Components/components_list'));
	}

	public function ajx_getActions($component_id)
	{
		$component = $this->model->getComponent($component_id);

		if ((strpos($component->name, '/') !== false)) {
			list($package, $compName) = explode('/', $component->name);
		} else {
			$package = $component->name;
			$compName = $component->name;
		}

		$filename = DOCROOT.DATA_DIR.COMPONENTS_PATH.$package.DS.'config.xml';

		if (!file_exists($filename)) {
			$filename = DOCROOT.WEBROOT.COMPONENTS_PATH.$package.DS.'config.xml';
		}

		if (file_exists($filename)) {
			$xml = new SimpleXMLElement(file_get_contents($filename));
			$xmlController = $xml->xpath('//controller');

			foreach ($xmlController as $contr) {
				if ((string) $contr->attributes()->name == $compName) {
					$Actions = array();

					foreach ($contr->children()->ControllerActions->children() as $act) {
						if ($act->getName() == 'action') {
							array_push($Actions, array('text' => (string) $act->attributes()->title, 'value' => (string) $act->attributes()->name));
						}
					}
					header('Content-Type: application/json');
					echo json_encode(array('options' => $Actions));
				}
			}
		} else {
			header('Content-Type: application/json');
			echo json_encode(array('options' => array()));
		}
	}
}
