<?php

class Dashboard extends BaseController
{
	public function __construct()
	{
		parent::__construct();

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getControllerUrl('Users/Login'));
		}

		$this->view->setLayout('default');
	}

	public function index()
	{
		Utils::saveCurrentPageUrl();
		$this->view->setTitle('Dashboard');
		HeadHTML::setTitleTag('Dashboard'.' | '.CMSSettings::$website_title);

		$dashboard_modules = array();

		$components_md = Loader::getModel('Components');

		$components = $components_md->getList(100, 0, "enabled = '1'", 'sorting');

		$Dashboard_Widgets = array();

		//array_push($Dashboard_Widgets, array("Generic/Dashboard_Logo" => array()));

		$arr_widgets = array();
		foreach ($components as $comp) {
			if (!UserAuth::checkComponentPerms($comp->name)) {
				continue;
			}

			if ($components_md->isEnabled($comp->name)) {

				//Load widgets by XML
				$widgets = $this->model->getWidgetsByXML($comp);

				if (count($widgets)) {
					foreach ($widgets as $wdg) {
						if (!isset($Dashboard_Widgets[$wdg['name']])) {
							$Dashboard_Widgets[$wdg['name']] = array($wdg['name'] => $wdg['parameters']);
						}
					}
				}
			}
		}

		foreach ($Dashboard_Widgets as $widget) {
			foreach ($widget as $wid => $params) {
				$cWid = Loader::loadModule($wid);
				array_push($arr_widgets, $cWid->execute($params));
			}
		}

		$this->view->set('widgets', $arr_widgets);
		$this->view->render('dashboard');
	}

	public function change_lang($lang)
	{
		Utils::setLang($lang);
		Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
	}
}
