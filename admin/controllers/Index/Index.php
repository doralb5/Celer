<?php

class Index extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->view->setLayout('default');
	}

	public function index()
	{
		Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
	}
}
