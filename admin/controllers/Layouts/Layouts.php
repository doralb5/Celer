<?php

class Layouts extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->view->set('url_layoutslist', Utils::getComponentUrl('Layouts/layouts_list'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
		if (!UserAuth::checkComponentPerms('Layouts')) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$this->view->setLayout('default');
	}

	public function layouts_list()
	{
		Utils::saveCurrentPageUrl();
		if (isset($_POST['manage'])) {
			Utils::RedirectTo(Utils::getControllerUrl("Layouts/manage_layout/{$_POST['managed_item']}"));
		}

		$temp_md = Loader::getModel('Templates');
		$template = $temp_md->getList(1, 0, "enabled = '1'");

		$layouts = $this->model->getLayoutsListByXml($template[0]->name);

		$this->view->set('layouts', $layouts);
		$this->view->setTitle('[$Layouts]');
		$this->view->render('layouts-list');
	}

	public function manage_layout($name)
	{
		Utils::saveCurrentPageUrl();
		$blocks_md = Loader::getModel('Blocks');

		if (isset($_POST['save'])) {
			$block = new Block_Entity();
			$block->position = $_POST['position'];
			$block->id_module = $_POST['id_module'];
			$block->sorting = $_POST['sorting'];
			$block->enabled = $_POST['enabled'];

			if (in_array('0', $_POST['id_page'])) {
				$block->always = 1;
			} else {
				$block->always = 0;
			}
			$ins_id = $blocks_md->saveBlock($block);
			if (!is_array($ins_id)) {
				$this->view->AddNotice('The block has been saved successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}

			if ($block->always == 0) {
				foreach ($_POST['id_page'] as $page_id) {
					$PageBlock = new PageBlock_Entity();
					$PageBlock->id_page = $page_id;
					$PageBlock->id_block = $ins_id;
					$blocks_md->savePageBlock($PageBlock);
				}
			}
		}

		//Manaxhimi i blocks dhe positions
		$positions = array();
		$blocks = array();
		$template_name = CMSSettings::$template;
		$positions = $this->model->getPositionsListByXml($template_name, $name);
		foreach ($positions as $pos) {
			$blocks[$pos['name']] = $blocks_md->getList(30, 0, 'position = :posName AND (cms_Page.layout = :layout OR cms_Page.layout IS NULL)', $order = '', array(':posName' => $pos['name'], ':layout' => $name));
		}
		$this->view->set('blocks', $blocks);
		$this->view->set('positions', $positions);

		//Manaxhimi i moduleve
		$modules_md = Loader::getModel('Modules');
		$modules = $modules_md->getList(30, 0, "enabled = '1'");

		foreach ($modules as &$mod) {
			$mod->settings = $blocks_md->getModuleByXML($mod->name);
		}
		$this->view->set('modules', $modules);

		$pages_md = Loader::getModel('Pages');
		$pages = $pages_md->getList(50);
		$this->view->set('pages', $pages);

		$this->view->set('LayoutName', $name);
		$this->view->set('url_block_delete', Utils::getComponentUrl('Layouts/delete_block/'));
		$this->view->setTitle('Layout Positions');
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getControllerUrl('Layouts/layouts_list'))));
		$this->view->render('layout-positions');
	}

	public function edit_block($id)
	{
		//Utils::saveCurrentPageUrl();
		$layout_name = (isset($_GET['layout'])) ? $_GET['layout'] : 'default';

		$blocks_md = Loader::getModel('Blocks');
		$block = $blocks_md->getBlock($id);
		if (isset($_POST['save'])) {
			$block->position = $_POST['position'];
			$block->sorting = $_POST['sorting'];
			$block->enabled = isset($_POST['enabled']) ? 1 : 0;

			$_POST['id_page'] = isset($_POST['id_page']) ? $_POST['id_page'] : array();

			if (in_array(0, $_POST['id_page'])) {
				$block->always = 1;
				$blocks_md->deletePageBlocks($id);
			} else {
				$blocks_md->deletePageBlocks($id);
				$block->always = 0;
				foreach ($_POST['id_page'] as $page_id) {
					$pb = $blocks_md->getPageBlock($id, $page_id);
					if (count($pb) == 0) {
						$PageBlock = new PageBlock_Entity();
						$PageBlock->id_page = $page_id;
						$PageBlock->id_block = $id;
						$blocks_md->savePageBlock($PageBlock);
					}
				}
			}

			$res = $blocks_md->saveBlock($block);

			if (!is_array($res)) {
				$this->view->AddNotice('The block has been saved successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}

		$pageBlocks = $blocks_md->getPagesOfBlock($id);

		$pageIds = array();
		foreach ($pageBlocks as $pb) {
			array_push($pageIds, $pb['id_page']);
		}
		$this->view->set('pageIds', $pageIds);

		if (isset($_POST['save_params'])) {
			unset($_POST['save_params']);

			$block->class = @$_POST['block_class'];
			unset($_POST['block_class']);

			foreach ($_POST as $key => $value) {
				if ($value == '') {
					unset($_POST[$key]);
				}
			}
			$json_data = (count($_POST)) ? json_encode($_POST) : '';

			$block->parameters = $json_data;
			$res = $blocks_md->saveBlock($block);

			if (!is_array($res)) {
				$this->view->AddNotice('The parameters has been saved successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}

		$block = $blocks_md->getBlock($id);
		$this->view->set('block', $block);

		$module = $blocks_md->getModuleByXML($block->module_name);
		$module['values'] = json_decode($block->parameters, true);
		$this->view->set('module', $module);

		$template_name = CMSSettings::$template;
		$positions = $this->model->getPositionsListByXml($template_name, $layout_name);
		$this->view->set('positions', $positions);

		$pages_md = Loader::getModel('Pages');
		$pages = $pages_md->getList();
		$this->view->set('pages', $pages);

		$this->view->setTitle('Edit Block');
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getControllerUrl("Layouts/manage_layout/$layout_name"))));
		$this->view->render('edit-block');
	}

	public function delete_block($id)
	{
		$layout_name = (isset($_GET['layout'])) ? $_GET['layout'] : 'default';
		$block_md = Loader::getModel('Blocks');
		$res = $block_md->deleteBlock($id);

		if ($res !== false) {
			$this->view->AddNotice('Block has been deleted successfully.');
			$this->LogsManager->registerLog('Block', 'delete', "Block deleted with id : $id", $id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getControllerUrl("Layouts/manage_layout/$layout_name"));
	}

	public function delete_page_block($id)
	{
		$layout_name = (isset($_GET['layout'])) ? $_GET['layout'] : 'default';
		$block_md = Loader::getModel('Blocks');
		$res = $block_md->deletePageBlock($id);
		if ($res !== false) {
			$this->view->AddNotice('Block has been deleted successfully.');
			$this->LogsManager->registerLog('Block', 'delete', "Block deleted with id : $id", $id);
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getControllerUrl("Layouts/manage_layout/$layout_name"));
	}
}
