<?php

class Modules extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->view->set('url_modules_list', Utils::getComponentUrl('Modules/modules_list'));
		$this->view->set('url_module_delete', Utils::getComponentUrl('Modules/delete_module'));
		$this->view->set('url_module_enable', Utils::getComponentUrl('Modules/enable_module'));
		$this->view->set('url_module_disable', Utils::getComponentUrl('Modules/disable_module'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
		if (!UserAuth::checkComponentPerms('Modules')) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$this->view->setLayout('default');
	}

	public function modules_list()
	{
		Utils::saveCurrentPageUrl();
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

		$elements_per_page = 20;
		$totalElements = $this->model->getLastCounter();
		$modules = $this->model->getList($elements_per_page, ($page - 1) * $elements_per_page);

		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('modules', $modules);

		$this->view->setTitle('Modules');
		$this->view->addButton(new Button('<i class="fa fa-puzzle-piece"></i>&nbsp;&nbsp;[$InstallModule]', Utils::getControllerUrl('Modules/install_module')));
		$this->view->render('modules-list');
	}

	public function install_module()
	{
		Utils::saveCurrentPageUrl();

		if (isset($_POST['install'])) {
			($this->model->existModule($_POST['name'])) ? $this->view->AddError('This module already exists!') : '';
			(empty($_POST['name'])) ? $this->view->AddError('Name is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				if ($_POST['package'] != '' && $_POST['package'] != $_POST['name']) {
					$Modname = $_POST['package'].'/'.$_POST['name'];
				} else {
					$Modname = $_POST['name'];
				}

				$module = new Module_Entity();

				$module->name = $Modname;
				$result = $this->model->saveModule($module);

				if (!is_array($result)) {
					$this->view->AddNotice('The module has been installed succesfully.');
					Utils::RedirectTo(Utils::getComponentUrl('Modules/modules_list'));
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
		}

		$this->view->setTitle('Install Module');
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getControllerUrl('Modules/modules_list'))));
		$this->view->render('install-module');
	}

	public function delete_module($id)
	{
		$res = $this->model->deleteModule($id);
		if ($res !== false) {
			$this->view->AddNotice('Module has been deleted successfully!');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getControllerUrl('Modules/modules_list'));
	}

	public function enable_module($id)
	{
		$module = $this->model->getModule($id);
		$module->enabled = 1;
		$res = $this->model->saveModule($module);
		if (!is_array($res)) {
			$this->view->AddNotice('Module has been enabled successfully!');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getControllerUrl('Modules/modules_list'));
	}

	public function disable_module($id)
	{
		$module = $this->model->getModule($id);
		$module->enabled = 0;
		$res = $this->model->saveModule($module);
		if (!is_array($res)) {
			$this->view->AddNotice('Module has been disabled successfully!');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getControllerUrl('Modules/modules_list'));
	}
}
