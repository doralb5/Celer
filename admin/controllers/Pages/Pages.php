<?php

class Pages extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->view->set('url_pages_list', Utils::getControllerUrl('Pages/pages_list'));
		$this->view->set('url_page_edit', Utils::getControllerUrl('Pages/edit_page'));
		$this->view->set('url_page_delete', Utils::getControllerUrl('Pages/delete_page'));

		$this->view->set('url_pcontents_list', Utils::getControllerUrl('Pages/PageContents_list'));
		$this->view->set('url_pagecontent_edit', Utils::getControllerUrl('Pages/edit_PageContent'));
		$this->view->set('url_pagecontent_delete', Utils::getControllerUrl('Pages/delete_PageContent'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/Login'));
		}
		if (!UserAuth::checkComponentPerms('Pages')) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$this->view->setLayout('default');
	}

	public function pages_list()
	{
		Utils::saveCurrentPageUrl();
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

		if (isset($_POST['delete_selected']) && isset($_POST['elements'])) {
			foreach ($_POST['elements'] as $id) {
				$res = $this->model->deletePage($id);
			}

			if ($res) {
				$this->view->AddNotice('Pages has been deleted successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}

		$elements_per_page = 15;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = "lang = '".CMSSettings::$default_lang."'";

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => TABLE_PREFIX.'Page.id', 'peso' => 100),
				array('field' => TABLE_PREFIX.'Component.name', 'peso' => 90),
				array('field' => TABLE_PREFIX.'Page.action', 'peso' => 90),
			);
			$pages = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$pages = $this->model->getList($elements_per_page, ($page - 1) * $elements_per_page, $filter, $sorting);
		}

		$totalElements = $this->model->getLastCounter();

		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('totalElements', $totalElements);

		$this->view->set('pages', $pages);

		$this->view->setTitle('[$Pages]');
		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$AddPage]', Utils::getControllerUrl('Pages/edit_page')));
		$this->view->render('pages-list');
	}

	public function edit_page($id = null)
	{
		Utils::saveCurrentPageUrl();
		if (!is_null($id)) {
			$page = $this->model->getPage($id);
			HeadHTML::setTitleTag('Edit Page - '.$page->alias.' | '.CMSSettings::$website_title);
			$this->view->setTitle('Edit Page');

			$controllerParameters = $this->model->getParametersByXML($page->component_name, $page->action);
			$this->view->set('controllerParameters', $controllerParameters);
		} else {
			//Kontrollojme limitin e faqeve
			if ($this->model->checkPageLimit()) {
				$this->view->AddError('The page limit was passed. You can not create more pages!');
				Utils::RedirectTo(Utils::getControllerUrl('Pages/pages_list'));
				return;
			}

			$page = new Page_Entity();
			HeadHTML::setTitleTag('New Page - '.' | '.CMSSettings::$website_title);
			$this->view->setTitle('New Page');
		}

		if ($page === false) {
			$this->AddError("Page $id not found");
		} elseif (isset($_POST['save'])) {
			($this->model->checkCompActionAvailability($id, $_POST)) ? $this->view->AddError('This component/action/parameter already exist!') : '';
			($_POST['alias_'.CMSSettings::$default_lang] == '') ? $this->view->AddError('Default Alias is required!') : '';

			if (count($this->view->getErrors()) == 0) {
				$robots = '';
				if (isset($_POST['robots_index'])) {
					$robots = $_POST['robots_index'].',';
				}
				if (isset($_POST['robots_follow'])) {
					$robots .= "{$_POST['robots_follow']},";
				}
				$robots = rtrim($robots, ',');

				$this->model->startTransaction();

				$page->id_component = $_POST['id_component'];
				$page->action = isset($_POST['action']) ? $_POST['action'] : '';
				$page->action_params = ($_POST['action_params'] != '') ? $_POST['action_params'] : null;
				$page->name = $_POST['name'];
				$page->layout = $_POST['layout'];
				$page->enabled = (isset($_POST['enabled']) ? 1 : 0);
				$page->robots = $robots;
				$page->home = isset($_POST['home']) ? 1 : 0;

				$inserted_id = $this->model->savePage($page);

				if (!is_array($inserted_id)) {
					foreach (CMSSettings::$available_langs as $lang) {
						if (is_null($id)) {
							$pContent = new PageContent_Entity();
							$pContent->id_page = $inserted_id;
						} else {
							$pcon = $this->model->getPageContents(1, 0, "id_page = {$id} AND lang = '{$lang}'");
							if (count($pcon)) {
								$pContent = $pcon[0];
							} else {
								$pContent = new PageContent_Entity();
							}
							$pContent->id_page = $id;
						}

						$is_empty = ($_POST['title_'.$lang] == '' && $_POST['alias_'.$lang] == '' && $_POST['meta_description_'.$lang] == '' && $_POST['meta_keywords_'.$lang] == '') ? true : false;

						$pContent->title = (!$is_empty) ? $_POST['title_'.$lang] : $_POST['title_'.CMSSettings::$default_lang];
						$pContent->alias = (!$is_empty) ? $_POST['alias_'.$lang] : $_POST['alias_'.CMSSettings::$default_lang];
						$pContent->meta_description = (!$is_empty) ? $_POST['meta_description_'.$lang] : $_POST['meta_description_'.CMSSettings::$default_lang];
						$pContent->meta_keywords = (!$is_empty) ? $_POST['meta_keywords_'.$lang] : $_POST['meta_keywords_'.CMSSettings::$default_lang];
						$pContent->lang = $lang;

						$result = $this->model->savePageContent($pContent);

						if (is_array($result)) {
							$this->AddError('Problem creating contents of the page!');
							$this->model->rollback();
							if (!is_bool($result)) {
								Utils::RedirectTo(Utils::getControllerUrl('Pages/edit_page')."/$inserted_id");
							} else {
								Utils::RedirectTo(Utils::getControllerUrl('Pages/edit_page')."/$id");
							}
						}
					}

					$this->view->AddNotice('Page has been saved successfully.');
					$this->model->commit();

					if (!is_bool($inserted_id)) {
						Utils::RedirectTo(Utils::getControllerUrl('Pages/edit_page')."/$inserted_id");
						$this->LogsManager->registerLog('Page', 'insert', 'Page inserted with id : '.$inserted_id, $inserted_id);
					} else {
						Utils::RedirectTo(Utils::getControllerUrl('Pages/edit_page')."/$id");
						$this->LogsManager->registerLog('Page', 'insert', 'Page inserted with id : '.$id, $id);
					}
				} else {
					$this->view->AddError('Something went wrong!');
					$this->model->rollback();
				}
			}
		} elseif (isset($_POST['save_parameters'])) {
			unset($_POST['save_parameters']);
			foreach ($_POST as $key => $value) {
				if (is_array($value)) {
					//$_POST[$key] = json_encode($value);
				}

				if ($value == '') {
					unset($_POST[$key]);
				}
			}
			$json_data = (count($_POST)) ? json_encode($_POST) : '';

			$page->parameters = $json_data;
			$result = $this->model->savePage($page);
			if (!is_array($result)) {
				$this->view->AddNotice('Page parameters has been saved successfully.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}

		$temp_md = Loader::getModel('Templates');
		$template = $temp_md->getList(1, 0, "enabled = '1'");

		$layouts_md = Loader::getModel('Layouts');
		$layouts = $layouts_md->getLayoutsListByXml((count($template) ? $template[0]->name : ''));
		$this->view->set('layouts', $layouts);

		$comp_md = Loader::getModel('Components');
		$components = $comp_md->getList(30, 0, "enabled = '1' AND type = '0'");
		$this->view->set('components', $components);

		foreach (CMSSettings::$available_langs as $lang) {
			$pContent = $this->model->getPageContents(1, 0, "id_page = {$id} AND lang = '{$lang}'");
			$page->{$lang} = count($pContent) ? $pContent[0] : null;
		}
		$this->view->set('page', $page);

		$this->view->setTitle('New Page');
		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getControllerUrl('Pages/pages_list'))));
		$this->view->render('edit-page');
	}

	public function delete_page($id)
	{
		$res = $this->model->deletePage($id);
		if ($res !== false) {
			$this->view->AddNotice('Page has been deleted successfully.');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		//Fshijme njekohesisht edhe menu item nese ekzistion per kete faqe.
		//$menu_md = Loader::getModel('Menu');
		//$menu_md->deleteItemByTargetPage($id);

		Utils::backRedirect(Utils::getControllerUrl('Pages/pages_list'));
	}
}
