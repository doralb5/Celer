<?php

class RemoteApi extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
	}

	public function authenticate()
	{
		$data = $_POST;
		$code = isset($data['code']) ? $data['code'] : '';
		$username = isset($data['username']) ? $data['username'] : '';
		$password = isset($data['password']) ? md5($data['password']) : '';
		$user = array();

		$user_md = Loader::getModel('Users');
		$client_info = $user_md->getClientDbInformation($code);

		if (!is_null($client_info)) {
			$db = new Database(DB_TYPE, $client_info->db_host, $client_info->db_name, $client_info->db_user, $client_info->db_pass);
			Database::setInstance($db);
			$this->LogsManager->setDatabase($db);
			$this->model->setDatabase($db);

			$logged = $this->model->authenticate($username, $password, $user);

			$response = array();
			if ($logged) {
				$session = $this->model->getRemoteUserSession($user->id);
				if (is_null($session)) {
					$session = new RemoteUserSession_Entity();
				}

				$session->id_user = $user->id;
				$session->start_time = date('Y-m-d H:i:s');
				$session->token = md5($user->username.$session->start_time.$user->password);
				$res = $this->model->saveRemoteUserSession($session);

				if (!is_array($res)) {
					UserAuth::setLoginSession($user);
					$client_info = Utils::ObjectToArray($client_info);
					UserAuth::setClientInfo($client_info);
					UserAuth::setRemoteSession();
					$response['message'] = 'Authentication succeed.';
					$response['status'] = 1;
					$response['token'] = $session->token;
				} else {
					$response['message'] = 'Something went wrong!';
					$response['status'] = 0;
				}
			} else {
				$response['message'] = 'Authentication Failed!';
				$response['status'] = 0;
			}
		} else {
			$response['message'] = 'Authentication Failed!';
			$response['status'] = 0;
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function logout()
	{
		UserAuth::unsetClientInfo();
		UserAuth::unsetLoginSession();
		UserAuth::unsetRemoteSession();
		$response['message'] = 'You have been logged out!';
		$response['status'] = 1;
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function getAccessPermission()
	{
		$data = $_POST;
		$code = isset($data['code']) ? $data['code'] : '';
		$token = isset($data['token']) ? $data['token'] : '';

		if (!$this->CheckTokenValidity($code, $token)) {
			$response['message'] = 'Permission denied!';
			$response['status'] = 0;
			$response['auth'] = -1;
			echo json_encode($response);
			return;
		}

		$redirect_url = isset($data['url']) ? $data['url'] : '';
		if ($redirect_url == '') {
			$response['message'] = 'You need to set the url!';
			$response['status'] = 0;
			echo json_encode($response);
			return;
		}

		session_write_close();
		$strCookie = 'PHPSESSID='.session_id().'; path=/';

		$defaults = array(
			CURLOPT_URL => $redirect_url,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $_POST,
			CURLOPT_COOKIE => $strCookie,
		);

		$ch = curl_init();
		curl_setopt_array($ch, $defaults);
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}

	public function CheckTokenValidity($code, $token)
	{
		$users_md = Loader::getModel('Users');
		$client_info = $users_md->getClientDbInformation($code);

		if (!is_null($client_info)) {
			$db = new Database(DB_TYPE, $client_info->db_host, $client_info->db_name, $client_info->db_user, $client_info->db_pass);
			Database::setInstance($db);
			$this->LogsManager->setDatabase($db);
			$this->model->setDatabase($db);

			$user = array();
			if ($this->model->validToken($token, $user)) {
				$user = Utils::ObjectToArray($user);
				UserAuth::setLoginSession($user);
				$client_info = Utils::ObjectToArray($client_info);
				UserAuth::setClientInfo($client_info);
				UserAuth::setRemoteSession();
				return true;
			}
		}
		return false;
	}

	public function helloWorld()
	{
		echo 'Hello Worlddddddddddd';
	}
}
