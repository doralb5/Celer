<?php

class Settings extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getControllerUrl('Users/Login'));
		}
		if (!UserAuth::checkComponentPerms('Settings')) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$this->view->setLayout('default');
	}

	public function general_settings()
	{
		Utils::saveCurrentPageUrl();
		HeadHTML::setTitleTag('General Settings'.' | '.CMSSettings::$website_title);

		if (isset($_POST['save'])) {
			unset($_POST['save']);
			$output = array();
			foreach ($_FILES as $attrName => $valuesArray) {
				foreach ($valuesArray as $key => $value) {
					$output[$key][$attrName] = $value;
				}
			}
			foreach ($output['name'] as $key => $name) {
				if ($key == 'logo') {
					$cstt = CMSSettings::$logo;
				} elseif ($key == 'marker_icon') {
					$cstt = CMSSettings::$marker_icon;
				}
				if (strlen($_FILES[$key]['tmp_name'])) {
					if (!Utils::allowedFileType($_FILES[$key]['name'])) {
						$this->view->AddError($key.' is not valid!');
						$_POST[$key] = $cstt;
					} else {
						if (!$cstt == 'logo.png' && file_exists(DOCROOT.MEDIA_ROOT.DS.$cstt)) {
							unlink(DOCROOT.MEDIA_ROOT.DS.$cstt);
						}

						$_POST[$key] = time().$_FILES[$key]['name'];
						$target = DOCROOT.MEDIA_ROOT.DS.$_POST[$key];
						move_uploaded_file($_FILES[$key]['tmp_name'], $target);
					}
				} else {
					$_POST[$key] = $cstt;
				}
			}
			// add new fields to settings
			$addfieldname = $_POST['addfieldname'];
			$addfieldvalue = $_POST['addfieldvalue'];
			$addfield = array_combine($addfieldname, $addfieldvalue);
			foreach ($addfield as $f => $v) {
				$setting = new Setting_Entity();
				$setting->field = $f;
				$setting->value = $v;
				$res = $this->model->saveSetting($setting);
			}
			foreach ($_POST as $field => $value) {
				if (!is_array($value)) {
					$setting = $this->model->getSettingByField($field);
					;
					$setting->field = $field;
					$setting->value = $value;
					$res = $this->model->saveSetting($setting);
				}
			}

			if (!is_array($res)) {
				$this->view->AddNotice('Settings has been updated successfully!');
				$this->LogsManager->registerLog('Setting', 'update', 'Settings updated.');
			} else {
				$this->view->AddError('Something went wrong!');
			}
		}

		$settings = $this->model->getList();
		$this->view->set('settings', $settings);
		$this->view->setTitle('[$Settings]');
		$this->view->render('general-settings');
	}
}
