<?php

class Templates extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->view->set('url_templates_list', Utils::getComponentUrl('Templates/templates_list'));
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getControllerUrl('Users/login'));
		}
		if (!UserAuth::checkComponentPerms('Templates')) {
			Utils::RedirectTo(Utils::getControllerUrl('Dashboard'));
		}
		$this->view->setLayout('default');
	}

	public function templates_list()
	{
		Utils::saveCurrentPageUrl();
		if (isset($_POST['enable'])) {
			$res = $this->model->enableTemplate($_POST['enabled_item']);
			if (!is_array($res)) {
				$this->view->AddNotice('The template has been enabled successfully.');
			} else {
				$this->view->AddNotice('Something went wrong!');
			}
		}

		$templates = $this->model->getList();

		$this->view->set('templates', $templates);
		$this->view->set('PAGE_TITLE', 'Templates');
		$this->view->addButton(new Button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$InstallTemplate]', Utils::getControllerUrl('Templates/install_template')));
		$this->view->render('templates-list');
	}

	public function delete_template($id)
	{
		$res = $this->model->deleteTemplate($id);
		if ($res !== false) {
			$this->view->AddNotice('Template has been deleted successfully.');
		} else {
			$this->view->AddError('Something went wrong!');
		}
		Utils::backRedirect(Utils::getControllerUrl('Templates/templates_list'));
	}

	public function manage_template($id)
	{
		Utils::saveCurrentPageUrl();
		// @var $template Template_Entity
		$template = $this->model->getTemplate($id);

		if ($template === false) {
			$this->view->AddError('Template not found!');
		}

		if (isset($_POST['create_override'])) {
			$this->createOverride($template);
		}

		if (isset($_POST['save_general'])) {
			$template->head_extra = $_POST['head-extra'];
			$template->footer_extra = $_POST['footer-extra'];
			if ($this->model->saveTemplate($template) === true) {
				$this->view->AddNotice('Template Generic Data saved successfully!');
			} else {
				$this->view->AddError('Something wrong during update of Generic Data!');
			}
		}

		if (isset($_POST['save_override'])) {
			$filepath = (isset($_GET['filepath']) ? $_GET['filepath'] : '');

			if ($filepath != '') {
				$directory_path = DOCROOT.DATA_DIR.TEMPLATES_PATH.$template->name;

				$output = $_POST['editorCodeOverride'];

				$result = file_put_contents($directory_path.DS.trim($filepath, DS), $output);
				if ($result) {
					$this->view->AddNotice('The file has been saved successfully.');
				} else {
					$this->view->AddError('There was an error during save!');
				}
			}
		}

		if (isset($_POST['save'])) {
			$this->view->AddError('This function is disabled temporary!');
		}

		//Ruajme opsionet e Templatit
		if (isset($_POST['save_options'])) {
			$this->saveTemplateOptions($template->id);
			Utils::RedirectTo(Utils::getControllerUrl("Templates/manage_template/$id/?active=tab_options"));
		}

		$xml = null;

		//Kontrollojme per config.xml te personalizuar
		$config_path = DOCROOT.DATA_DIR.TEMPLATES_PATH.$template->name;
		if (file_exists($config_path.DS.'config.xml')) {
			$xml = new SimpleXMLElement(file_get_contents($config_path.DS.'config.xml'));
		} else {
			$config_path = DOCROOT.TEMPLATES_PATH.$template->name;
			if (file_exists($config_path.DS.'config.xml')) {
				$xml = new SimpleXMLElement(file_get_contents($config_path.DS.'config.xml'));
			} else {
				$this->view->AddError('Config File '.$config_path.DS.'config.xml'.' not found!');
			}
		}

		//Gjenerimi i kodit ne editore
		if (isset($_GET['filepath']) && isset($_GET['active'])) {
			if ($_GET['active'] == 'tab_editor') {
				$filepath = $config_path.$_GET['filepath'];
			} elseif ($_GET['active'] == 'tab_overrides') {
				$filepath = DOCROOT.DATA_DIR.TEMPLATES_PATH.$template->name.$_GET['filepath'];
			} else {
				$filepath = '';
			}

			if (file_exists($filepath)) {
				$sourceCode = file_get_contents($filepath);
				$this->view->set('sourceCode', $sourceCode);
			}
		}

		$this->view->set('template', $xml);
		$this->view->set('head_extra', $template->head_extra);
		$this->view->set('footer_extra', $template->footer_extra);

		$options = $this->model->getOptionsByXml($template->name);
		$this->view->set('options', $options);

		$options_values = json_decode($template->options, true);
		$this->view->set('options_values', $options_values);

		$this->view->set('template_path', $config_path);
		$this->view->set('template_data_path', DOCROOT.DATA_DIR.TEMPLATES_PATH.$template->name);
		$this->view->set('temp_path', substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).DATA_DIR.TEMPLATES_PATH.$template->name);
		$this->view->set('manage_url', Utils::getControllerUrl('Templates/manage_template/'.$id));
		$this->view->setTitle('Templates');
		$this->view->render('template-manage');
	}

	public function install_template()
	{
		Utils::saveCurrentPageUrl();
		if (isset($_POST['install'])) {
			($this->model->existTemplate($_POST['name'])) ? $this->view->AddError('This template already exists!') : '';
			(empty($_POST['name'])) ? $this->view->AddError('Name is required!') : '';
			if (!is_dir(DOCROOT.DATA_DIR.TEMPLATES_PATH.$_POST['name']) && !is_dir(DOCROOT.TEMPLATES_PATH.$_POST['name'])) {
				$this->view->AddError("Folder {$_POST['name']} doesn't exist!");
			}

			if (count($this->view->getErrors()) == 0) {
				$template = new Template_Entity();
				$template->name = $_POST['name'];

				$result = $this->model->saveTemplate($template);

				if (!is_array($result)) {
					$this->view->AddNotice('The template has been installed succesfully.');
					Utils::RedirectTo(Utils::getComponentUrl('Templates/templates_list'));
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
		}

		$this->view->addButton(new Button('<i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;[$Back]', Utils::getLastPageHistory(Utils::getControllerUrl('Templates/templates_list'))));
		$this->view->setTitle('Templates');
		$this->view->render('install-template');
	}

	private function saveTemplateOptions($id)
	{
		unset($_POST['save_options']);
		$template = $this->model->getTemplate($id);
		$options_values = json_decode($template->options, true);

		if (strlen($_FILES['background_image']['tmp_name'])) {
			if (!Utils::allowedFileType($_FILES['background_image']['name'])) {
				$this->view->AddError('Background Image is not valid!');
			} else {
				if (isset($options_values['background_image'])) {
					unlink(DOCROOT.DATA_DIR.MEDIA_ROOT.DS.$options_values['background_image']);
				}

				$_POST['background_image'] = time().$_FILES['background_image']['name'];

				//Krijojme folderin imagses nqs. nuk ekziston
				$directory_path = DOCROOT.DATA_DIR.TEMPLATES_PATH.$template->name.DS.'images';
				Utils::createDirectory($directory_path);

				$target = $directory_path.DS.$_POST['background_image'];
				move_uploaded_file($_FILES['background_image']['tmp_name'], $target);
			}
		} else {
			if (isset($_POST['reset_background_image'])) {
				$_POST['background_image'] = '';
				unset($_POST['reset_background_image']);
			} elseif (isset($options_values['background_image'])) {
				$_POST['background_image'] = $options_values['background_image'];
			}
		}
		foreach ($_POST as $key => $value) {
			if ($value == '') {
				unset($data[$key]);
			}
		}

		$json_data = (count($_POST)) ? json_encode($_POST) : '';
		$template->options = $json_data;
		$res = $this->model->saveTemplate($template);

		if (!is_array($res)) {
			$this->view->AddNotice('The options has been updated successfully.');
		} else {
			$this->view->AddError('There was an error during update.');
		}
	}

	private function createOverride($template)
	{
		$filepath = (isset($_GET['filepath']) ? $_GET['filepath'] : '');

		if ($filepath != '') {
			$array_path = explode('/', $filepath);
			$filename = array_pop($array_path);

			if (count($array_path)) {
				$dir_path = implode(DS, $array_path);
				//Krijojme folderin images nqs. nuk ekziston
				$directory_path = DOCROOT.DATA_DIR.TEMPLATES_PATH.$template->name.$dir_path;

				Utils::createDirectory($directory_path);

				if (file_exists($directory_path.DS.$filename)) {
					$this->view->AddError('The override of this file already exists!');
				} else {
					$output = $_POST['editorCode'];
					$result = file_put_contents($directory_path.DS.$filename, $output);
					if ($result) {
						$this->view->AddNotice('The override file has been created successfully.');
					} else {
						$this->view->AddError('The creation of override file failed!');
					}
				}
			}
		}
	}
}
