<?php
// Front Controller

define('WEBROOT_ADMIN', 'admin/');
define('WEBROOT_FRONT', '/');

define('WEBROOT', WEBROOT_FRONT.WEBROOT_ADMIN);
define('ADMIN', true);

include '../config/definitions.php';
include '../config/config.php';

if (SHOW_ERRORS) {
	ini_set('display_errors', '1');
}

ini_set('post_max_size', POST_MAX_SIZE);
ini_set('memory_limit', MEMORY_LIMIT);

function __autoload($class)
{
	if (is_file(DOCROOT.CLASSES_PATH.$class.'_class.php')) {
		require_once DOCROOT.CLASSES_PATH.$class.'_class.php';
	}
}

spl_autoload_register('__autoload');

include DOCROOT.LIBS_PATH.'Loader.php';
include DOCROOT.LIBS_PATH.'FrontController.php';
include DOCROOT.LIBS_PATH.'AdminFrontController.php';
$fc = new AdminFrontController();

$fc->bootstrap();

