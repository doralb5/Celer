<?php

require_once DOCROOT.ENTITIES_PATH.'Article/Article.php';

class Articles_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('Article');

		if ($order == '') {
			$order = 'publish_date DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Article_Table = TABLE_PREFIX.Article_Entity::TABLE_NAME;
		$ArticleDetail_Table = TABLE_PREFIX.ArticleDetail_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Article_Table 
                                        LEFT JOIN {$ArticleDetail_Table} ON ($ArticleDetail_Table.id_article = $Article_Table.id)
                                        WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getArticle($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('Article');
		$this->em->loadById($id);
		$Article = $this->em->getEntities();
		return (count($Article)) ? $Article[0] : null;
	}

	public function saveArticle($Article)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('Article');
		$res = $this->em->persist($Article);
		if ($res > 0) {
			$details = new ArticleDetail_Entity();
			$details->id_article = $res;
			$details->lang = $Article->lang;
			$details->title = $Article->title;
			$details->subtitle = $Article->subtitle;
			$details->content = $Article->content;
			$this->saveArticleDetail($details);
		}
		return $res;
	}

	public function deleteArticle($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('Article');
		$res = $this->em->delete('Article', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('Article');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Article_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);
		echo '<!--'.$query->getQuery().'-->';
		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getCategories($limit = -1, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleCategory');

		if ($order == '') {
			$order = 'parent_id DESC, id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$ArticleCategory_Table = TABLE_PREFIX.ArticleCategory_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $ArticleCategory_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];

		//Marrim dhe numrin e kategorive
		$Article_Table = TABLE_PREFIX.Article_Entity::TABLE_NAME;
		foreach ($result as &$categ) {
			$tot_ser = $this->db->select("SELECT COUNT(*) AS total FROM $Article_Table WHERE id_category = :categ", array(':categ' => $categ->id));
			$categ->total_articles = $tot_ser[0]['total'];
			$this->hierarchyCategory($categ, $categ->category);
		}

		return $result;
	}

	public function getCategory($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleCategory');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveCategory($category)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleCategory');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleCategory');
		$res = $this->em->delete('ArticleCategory', $id);
		return $res;
	}

	public function getSections($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleSection');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$ArticleSection_Table = TABLE_PREFIX.ArticleSection_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $ArticleSection_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getSection($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleSection');
		$this->em->loadById($id);
		$section = $this->em->getEntities();
		return (count($section)) ? $section[0] : null;
	}

	public function saveSection($section)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleSection');
		$res = $this->em->persist($section);
		return $res;
	}

	public function deleteSection($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleSection');
		$res = $this->em->delete('ArticleSection', $id);
		return $res;
	}

	public function getRoles($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleRole');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$ArticleRole_Table = TABLE_PREFIX.ArticleRole_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $ArticleRole_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];

		return $result;
	}

	public function getRole($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleRole');
		$this->em->loadById($id);
		$role = $this->em->getEntities();
		return (count($role)) ? $role[0] : null;
	}

	public function saveRole($role)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleRole');
		$res = $this->em->persist($role);
		return $res;
	}

	public function deleteRole($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleRole');
		$res = $this->em->delete('ArticleRole', $id);
		return $res;
	}

	public function getArticleImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleImage');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$ArticleImage_Table = TABLE_PREFIX.ArticleImage_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $ArticleImage_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getArticleImage($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleImage');
		$this->em->loadById($id);
		$arImage = $this->em->getEntities();
		return (count($arImage)) ? $arImage[0] : null;
	}

	public function saveArticleImage($arImage)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleImage');
		$res = $this->em->persist($arImage);
		return $res;
	}

	public function deleteArticleImage($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleImage');
		$res = $this->em->delete('ArticleImage', $id);
		return $res;
	}

	public function getArticleDetails($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleDetail');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getArticleDetail($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleDetail');
		$this->em->loadById($id);
		$detail = $this->em->getEntities();
		return (count($detail)) ? $detail[0] : null;
	}

	public function saveArticleDetail($detail)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleDetail');
		$res = $this->em->persist($detail);
		return $res;
	}

	public function deleteArticleDetail($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleDetail');
		$res = $this->em->delete('ArticleDetail', $id);
		return $res;
	}

	private function hierarchyCategory($category, &$category_name)
	{
		if (!is_null($category->parent_id)) {
			$categ = $this->getCategory($category->parent_id);
			$category_name = $categ->category.' <b>&raquo</b> '.$category_name;
			$this->hierarchyCategory($categ, $category_name);
		} else {
			return;
		}
	}
}
