<?php

require_once DOCROOT.ENTITIES_PATH.'Generic/Log.php';

class Audits_model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Log');

		if ($order == '') {
			$order = 'date DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Log_Table = TABLE_PREFIX.Log_Entity::TABLE_NAME;

		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Log_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Log');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Log_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}
}
