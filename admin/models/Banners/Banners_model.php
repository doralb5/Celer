<?php

require_once DOCROOT.ENTITIES_PATH.'Banner/Banner.php';

class Banners_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Banner');
		$this->em->setEntity('Banner');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Banner_Table = TABLE_PREFIX.Banner_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Banner_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getBanner($id)
	{
		$this->em->setRepository('Banner');
		$this->em->setEntity('Banner');
		$this->em->loadById($id);
		$Banner = $this->em->getEntities();
		return (count($Banner)) ? $Banner[0] : null;
	}

	public function saveBanner($Banner)
	{
		$this->em->setRepository('Banner');
		$this->em->setEntity('Banner');
		$res = $this->em->persist($Banner);
		return $res;
	}

	public function deleteBanner($id)
	{
		$this->em->setRepository('Banner');
		$this->em->setEntity('Banner');
		$res = $this->em->delete('Banner', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Banner');
		$this->em->setEntity('Banner');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Banner_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Banner');
		$this->em->setEntity('BannerCategory');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$BannerCategory_Table = TABLE_PREFIX.BannerCategory_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $BannerCategory_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getCategory($id)
	{
		$this->em->setRepository('Banner');
		$this->em->setEntity('BannerCategory');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveCategory($category)
	{
		$this->em->setRepository('Banner');
		$this->em->setEntity('BannerCategory');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('Banner');
		$this->em->setEntity('BannerCategory');
		$res = $this->em->delete('BannerCategory', $id);
		return $res;
	}
}
