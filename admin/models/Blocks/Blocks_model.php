<?php

require_once DOCROOT.ENTITIES_PATH.'Page/Block.php';

class Blocks_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '', $bindArray = array())
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Block');

		if ($order == '') {
			$order = 'sorting';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order, $bindArray);
		$result = $this->em->getEntities();

		$this->lastCounter = $this->em->countByFilter($filter, $bindArray);

		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getBlocksOfPage($id)
	{
		$Block_Table = TABLE_PREFIX.Block_Entity::TABLE_NAME;
		$PageBlock_Table = TABLE_PREFIX.PageBlock_Entity::TABLE_NAME;

		$sql = "SELECT blc.* FROM $Block_Table AS blc "
				."LEFT JOIN $PageBlock_Table AS pb ON(pb.id_block=blc.id) "
				."WHERE pb.id_page = :id AND blc.always='0'";
		$result = $this->db->select($sql, array(':id' => $id));
		return $result;
	}

	public function getPagesOfBlock($id)
	{
		$Block_Table = TABLE_PREFIX.Block_Entity::TABLE_NAME;
		$PageBlock_Table = TABLE_PREFIX.PageBlock_Entity::TABLE_NAME;

		$sql = "SELECT pb.* FROM $PageBlock_Table AS pb "
				."LEFT JOIN $Block_Table AS blc ON(pb.id_block=blc.id) "
				."WHERE pb.id_block = :id AND blc.always='0'";

		$result = $this->db->select($sql, array(':id' => $id));
		return $result;
	}

	public function getBlock($id)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Block');
		$this->em->loadById($id);
		$block = $this->em->getEntities();
		return (count($block)) ? $block[0] : null;
	}

	public function deleteBlock($id)
	{
		$this->em->setRepository('Page');
		$res = $this->em->delete('Block', $id);
		return $res;
	}

	public function deletePageBlock($id)
	{
		$this->em->setRepository('Page');
		$res = $this->em->delete('PageBlock', $id);
		return $res;
	}

	//Updated
	public function getModuleByXML($mod_name)
	{
		if ((strpos($mod_name, '/') !== false)) {
			list($package, $moduleName) = explode('/', $mod_name);
		} else {
			$package = $mod_name;
			$moduleName = $mod_name;
		}

		$filename = MODULES_PATH.$package.DS.'config.xml';

		//Kontrollojme per te personalizuar
		if (file_exists(DOCROOT.DATA_DIR.$filename)) {
			$filename = DOCROOT.DATA_DIR.$filename;
		} else {
			$filename = DOCROOT.$filename;
		}

		if (file_exists($filename)) {
			$module = array();

			$xml = new SimpleXMLElement(file_get_contents($filename));
			$xmlmodules = $xml->xpath('//Modules/module');

			foreach ($xmlmodules as $mod) {
				if ($mod->attributes()->name == $moduleName) {
					$module['label'] = (string) $mod->attributes()->label;

					if (isset($mod->children()->Parameters)) {
						$module['params'] = array();
						foreach ($mod->children()->Parameters->children() as $param) {
							if ($param->getName() == 'param') {
								$module['params'][(string) $param->attributes()->name] = array('name' => (string) $param->attributes()->name, 'type' => (string) $param->attributes()->type, 'label' => (string) $param->attributes()->label, 'description' => (string) $param->attributes()->description, 'default' => (string) $param->attributes()->default);

								//SQLList Type
								if ((string) $param->attributes()->type == 'sqllist' || (string) $param->attributes()->type == 'sqllist-multiple') {
									$module['params'][(string) $param->attributes()->name]['options'] = array();
									foreach ($param->children() as $option) {
										if ($option->getName() == 'option') {
											array_push($module['params'][(string) $param->attributes()->name]['options'], array('value' => (string) $option->attributes()->value, 'label' => (string) $option->__toString()));
										}
									}

									$source = $param->children()->source;
									$sql = (string) $source->__toString();
									$result = $this->db->select($sql);
									$module['params'][(string) $param->attributes()->name]['source'] = array('value' => (string) $source->attributes()->value, 'label' => (string) $source->attributes()->label, 'elements' => $result);
								}

								//Radio /Select /Checkbox
								if ((string) $param->attributes()->type == 'radio' || (string) $param->attributes()->type == 'select' || (string) $param->attributes()->type == 'select-multiple' || (string) $param->attributes()->type == 'checkbox') {
									$module['params'][(string) $param->attributes()->name]['options'] = array();
									foreach ($param->children() as $option) {
										array_push($module['params'][(string) $param->attributes()->name]['options'], array('value' => (string) $option->attributes()->value, 'label' => (string) $option->__toString()));
									}
								}
							}
						}
					}
				}
			}
			return $module;
		}
	}

	public function saveBlock($block)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Block');
		$res = $this->em->persist($block);
		return $res;
	}

	public function getPageBlock($id_block, $id_page)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageBlock');
		$this->em->loadByFilter("id_block = $id_block AND id_page = $id_page");
		$result = $this->em->getEntities();
		return $result;
	}

	public function savePageBlock($pageblock)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageBlock');
		$res = $this->em->persist($pageblock);
		return $res;
	}

	public function deletePageBlocks($id_block, $limit = 30)
	{
		$PageBlock_tbl = TABLE_PREFIX.'PageBlock';
		$res = $this->db->delete($PageBlock_tbl, "id_block = $id_block", $limit);
		return $res;
	}
}
