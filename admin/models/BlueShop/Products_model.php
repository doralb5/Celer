<?php

class Products_Model extends BaseModel
{
	private $lastCounter = 0;

	public function __construct()
	{
		parent::__construct();
	}

	public function initialization($settings = array())
	{
		$db = new Database(DB_TYPE, $settings['DB_HOST'], $settings['DB_NAME'], $settings['DB_USER'], $settings['DB_PASS']);
		$this->setDatabase($db);
	}

	public function getProducts($filter = '', $limit = 30, $offset = 0, $sorting = '')
	{
		$this->em->setRepository('BlueShop');
		$this->em->setEntity('Product');

		if ($sorting == '') {
			$sorting = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $sorting);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);

		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}
}
