<?php

require_once DOCROOT.ENTITIES_PATH.'Booking/Booking.php';

class Booking_Model extends BaseModel
{
	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('Booking');

		if ($order == '') {
			$order = 'reservation_end DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$res = $this->em->getEntities();
		return $res;
	}

	public function getRoomTypeList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomType');
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$res = $this->em->getEntities();
		return $res;
	}

	public function getRoomType($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomType');
		$this->em->loadById($id);
		$res = $this->em->getEntities();
		return (count($res)) ? $res[0] : null;
	}

	public function getRoomsByType($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoom');
		$filter = 'room_type_id='.$id;
		$this->em->loadByFilter($filter);
		$res = $this->em->getEntities();
		return $res;
	}

	public function saveRoomType($room_type)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomType');
		$res = $this->em->persist($room_type);
		return $res;
	}

	public function saveRoom($room = array())
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoom');
		$res = $this->em->persist($room);
		return $res;
	}

	public function deleteRoom($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoom');
		$res = $this->em->delete('BookingRoom', $id);
		return $res;
	}

	public function deleteRoomType($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomType');
		$res = $this->em->delete('BookingRoomType', $id);
		return $res;
	}

	public function getRoomImagesByType($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomTypeImage');
		$filter = 'room_type_id='.$id;
		$this->em->loadByFilter($filter);
		$res = $this->em->getEntities();
		return $res;
	}

	public function getRoomImagesById($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomTypeImage');
		$this->em->loadById($id);
		$res = $this->em->getEntities();
		return (count($res)) ? $res[0] : null;
	}

	public function saveRoomTypeImage($roomtypeimage)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomTypeImage');
		$res = $this->em->persist($roomtypeimage);
		return $res;
	}

	public function deleteRoomTypeImage($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomTypeImage');
		$res = $this->em->delete('BookingRoomTypeImage', $id);
		return $res;
	}

	public function deleteBooking($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('Booking');
		$res = $this->em->delete('Booking', $id);
		return $res;
	}

	public function getBooking($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('Booking');
		$this->em->loadById($id);
		$res = $this->em->getEntities();

		return (count($res)) ? $res[0] : null;
	}

	public function saveBooking($booking)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('Booking');
		if ($booking->id == '') {
			$booking->created_time = date('Y-m-d H:i');
		}
		$res = $this->em->persist($booking);
		return $res;
	}
}
