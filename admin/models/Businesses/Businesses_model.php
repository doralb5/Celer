<?php

require_once DOCROOT.ENTITIES_PATH.'Business/Business.php';

class Businesses_Model extends BaseModel
{
	protected $lastCounter;
	public $ComponentSettings;

	public function __construct()
	{
		parent::__construct();

		$this->ComponentSettings = BaseComponent::getComponentSettings('Businesses', 'Businesses');
		if (isset($this->ComponentSettings['db_host']) &&
				isset($this->ComponentSettings['db_name']) &&
				isset($this->ComponentSettings['db_user']) &&
				isset($this->ComponentSettings['db_pass'])) {
			$this->setDatabase(new Database('mysql', $this->ComponentSettings['db_host'], $this->ComponentSettings['db_name'], $this->ComponentSettings['db_user'], $this->ComponentSettings['db_pass']));
		}
	}

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');

		$Business_Table = TABLE_PREFIX.Business_Entity::TABLE_NAME;
		$BC_Table = TABLE_PREFIX.BusinessCateg_Entity::TABLE_NAME;
		//        if (isset($this->ComponentSettings['filter_city'])) {
		//            if ($filter != '') {
		//                $filter .= ' AND ';
		//            }
		//            $filter .= " " . $Business_Table . ".location = '" . $this->ComponentSettings['filter_city'] . "' ";
		//        }

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Business_Table = TABLE_PREFIX.Business_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Business_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getBusiness($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');
		$this->em->loadById($id);
		$Business = $this->em->getEntities();
		return (count($Business)) ? $Business[0] : null;
	}

	public function saveBusiness($Business)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');
		$res = $this->em->persist($Business);
		return $res;
	}

	public function deleteBusiness($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');
		$res = $this->em->delete('Business', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Business_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCategory');

		if ($order == '') {
			$order = 'parent_id DESC, id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$BusinessCategory_Table = TABLE_PREFIX.BusinessCategory_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $BusinessCategory_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];

		$Business_Table = TABLE_PREFIX.Business_Entity::TABLE_NAME;
		foreach ($result as &$categ) {
			$tot_ser = $this->db->select("SELECT COUNT(*) AS total FROM $Business_Table WHERE id_category = :categ", array(':categ' => $categ->id));
			$categ->total_businesses = $tot_ser[0]['total'];
			$this->hierarchyCategory($categ, $categ->category);
		}

		return $result;
	}

	public function getCategory($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCategory');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveCategory($category)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCategory');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCategory');
		$res = $this->em->delete('BusinessCategory', $id);
		return $res;
	}

	public function searchCategory(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCategory');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = BusinessCategory_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getBusinessImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessImage');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		//$BusinessImage_Table = TABLE_PREFIX . BusinessImage_Entity::TABLE_NAME;
		//$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $BusinessImage_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ""));
		//$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getBusinessImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessImage');
		$this->em->loadById($id);
		$evImage = $this->em->getEntities();
		return (count($evImage)) ? $evImage[0] : null;
	}

	public function saveBusinessImage($evImage)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessImage');
		$res = $this->em->persist($evImage);
		return $res;
	}

	public function deleteBusinessImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessImage');
		$res = $this->em->delete('BusinessImage', $id);
		return $res;
	}

	public function getBusinessCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCateg');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getBusinessCategory($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCateg');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveBusinessCategory($category)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCateg');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteBusinessCategory($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCateg');
		$res = $this->em->delete('BusinessCateg', $id);
		return $res;
	}

	public function getProfiles($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessProfile');

		if ($order == '') {
			$order = 'id';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$BusinessProf_Table = TABLE_PREFIX.BusinessProfile_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $BusinessProf_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];

		return $result;
	}

	public function getProfile($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessProfile');
		$this->em->loadById($id);
		$profile = $this->em->getEntities();
		return (count($profile)) ? $profile[0] : null;
	}

	public function saveProfile($profile)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessProfile');
		$res = $this->em->persist($profile);
		return $res;
	}

	public function deleteProfile($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessProfile');
		$res = $this->em->delete('BusinessProfile', $id);
		return $res;
	}

	private function hierarchyCategory($category, &$category_name)
	{
		if (!is_null($category->parent_id)) {
			$categ = $this->getCategory($category->parent_id);
			$category_name = $categ->category.' <b>&raquo</b> '.$category_name;
			$this->hierarchyCategory($categ, $category_name);
		} else {
			return;
		}
	}

	public function saveTempImage($temp_image)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		$res = $this->em->persist($temp_image);
		return $res;
	}

	public function getTempImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		$this->em->loadById($id);
		$Image = $this->em->getEntities();
		return (count($Image)) ? $Image[0] : null;
	}

	public function deleteTempImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		$res = $this->em->delete('TempImage', $id);
		return $res;
	}

	public function getTempImages($limit = 999, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		if ($order == '') {
			$order = 'id';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}
}
