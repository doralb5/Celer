<?php

require_once DOCROOT.ENTITIES_PATH.'Business/Subdomain.php';

class Businesses_sub_Model extends Businesses_Model
{
	public $ComponentSettings;

	public function __construct()
	{
		parent::__construct();

		$this->ComponentSettings = BaseComponent::getComponentSettings('Businesses', 'Businesses');
		if (isset($this->ComponentSettings['db_host']) &&
				isset($this->ComponentSettings['db_name']) &&
				isset($this->ComponentSettings['db_user']) &&
				isset($this->ComponentSettings['db_pass'])) {
			$this->setDatabase(new Database('mysql', $this->ComponentSettings['db_host'], $this->ComponentSettings['db_name'], $this->ComponentSettings['db_user'], $this->ComponentSettings['db_pass']));
		}
	}

	public function getSubdomains($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		if ($order == '') {
			$order = 'id';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$subdom_Table = TABLE_PREFIX.Subdomain_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $subdom_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getSubdomain($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		$this->em->loadById($id);
		$subdom = $this->em->getEntities();
		return (count($subdom)) ? $subdom[0] : null;
	}

	public function saveSubdomain($subdom)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		$res = $this->em->persist($subdom);
		return $res;
	}

	public function deleteSubdomain($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		$res = $this->em->delete('Subdomain', $id);
		return $res;
	}

	public function searchSubdomain(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Subdomain_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getSticker($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessSticker');
		$this->em->loadById($id);
		$stickers = $this->em->getEntities();
		return (count($stickers)) ? $stickers[0] : null;
	}

	public function doneSticker($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessSticker');
		$sticker_entity = $this->getSticker($id);
		$sticker_entity->done = 1;
		$res = $this->em->persist($sticker_entity);
		return $res;
	}

	public function undoneSticker($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessSticker');
		$sticker_entity = $this->getSticker($id);
		$sticker_entity->done = 0;
		$res = $this->em->persist($sticker_entity);
		return $res;
	}

	public function getStickers($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessSticker');

		if ($order == '') {
			$order = 'id';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$stickers_Table = TABLE_PREFIX.BusinessSticker_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $stickers_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];

		return $result;
	}

	public function saveStickerRecord($business_sticker)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessSticker');
		return $this->em->persist($business_sticker);
	}

	public function checkTokenSticker($token)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessSticker');
		$filter = 'token='.$token;
		$this->em->loadByFilter($filter, 1, 0);
		$result = $this->em->getEntities();

		return $result;
	}

	public function getComments()
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessRating');

		$busRat_tbl = TABLE_PREFIX.'BusinessRating';
		$sql = "SELECT * FROM $busRat_tbl WHERE comment IS NOT NULL";
		$result = $this->db->select($sql);
		return $result;
	}

	public function getReview($limit = 20, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessRating');

		if ($order == '') {
			$order = 'id';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$busRat_tbl = TABLE_PREFIX.'BusinessRating';
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $busRat_tbl WHERE 1 ".(($filter != '') ? "AND $filter" : ''));

		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getComment($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessRating');
		$this->em->loadById($id);
		$comment = $this->em->getEntities();
		return (count($comment)) ? $comment[0] : null;
	}

	public function saveComment($comment)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessRating');
		return $this->em->persist($comment);
	}

	public function deleteComment($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessRating');
		$res = $this->em->delete('BusinessRating', $id);
		return $res;
	}

	public function getSubdomainByBizId($biz = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		$filter = " cms_Subdomain.target_id='".$biz."'";
		$this->em->loadByFilter($filter);
		$result = $this->em->getEntities();

		return count($result) ? $result[0] : false;
	}
}
