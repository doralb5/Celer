<?php

require_once DOCROOT.ENTITIES_PATH.'Component/Component.php';
require_once DOCROOT.ENTITIES_PATH.'User/User.php';
require_once DOCROOT.ENTITIES_PATH.'User/UserPermission.php';

class Components_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');

		if ($order == '') {
			$order = 'sorting,id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Component_Table = TABLE_PREFIX.Component_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Component_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getComponentByName($name)
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');
		$this->em->loadByFilter("name = '$name'");
		$component = $this->em->getEntities();
		return (count($component)) ? $component[0] : null;
	}

	public function getComponent($id)
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');
		$this->em->loadById($id);
		$component = $this->em->getEntities();
		return (count($component)) ? $component[0] : null;
	}

	public function getComponentActions($id)
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Action');
		$this->em->loadByFilter("id_component = $id");
		$component = $this->em->getEntities();
		return $component;
	}

	public function getUnpermittedUsers($id_comp)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');

		$User_Table = TABLE_PREFIX.User_Entity::TABLE_NAME;
		$UserPermission_Table = TABLE_PREFIX.UserPermission_Entity::TABLE_NAME;

		$sql = "SELECT * FROM $User_Table "
				.'WHERE NOT EXISTS '
				."(SELECT * FROM $UserPermission_Table AS up WHERE $User_Table.id = up.id_user AND up.id_component = {$id_comp}) "
				."AND $User_Table.super != '1' AND $User_Table.enabled = '1' AND $User_Table.deleted='0'";
		$this->em->loadByQuery($sql);
		$res = $this->em->getEntities();
		return $res;
	}

	public function getPermittedUsers($id_comp)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserPermission');

		$User_Table = TABLE_PREFIX.User_Entity::TABLE_NAME;
		$UserPermission_Table = TABLE_PREFIX.UserPermission_Entity::TABLE_NAME;
		//$filter
		//$this->em->loadByFilter($filter, $limit, $offset);

		$sql = "SELECT up.*,CONCAT($User_Table.firstname, ' ', $User_Table.lastname) AS name, $User_Table.email FROM $User_Table "
				."LEFT JOIN $UserPermission_Table AS up  ON($User_Table.id = up.id_user) "
				."WHERE id_component = {$id_comp} AND $User_Table.enabled = '1' AND $User_Table.deleted='0'";

		$this->em->loadByQuery($sql);
		$res = $this->em->getEntities();
		return $res;
	}

	public function isEnabled($comp_name)
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');
		$this->em->loadByFilter("name = '$comp_name' AND enabled = '1'");
		$component = $this->em->getEntities();
		return (count($component)) ? true : false;
	}

	public function saveComponent($component)
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');
		$res = $this->em->persist($component);
		return $res;
	}

	public function deleteComponent($id)
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');
		$res = $this->em->delete('Component', $id);
		return $res;
	}

	public function getComponentSettingsByXml($component)
	{
		$comp_name = $component->name;
		if ((strpos($comp_name, '/') !== false)) {
			list($package, $compName) = explode('/', $comp_name);
		} else {
			$package = $comp_name;
			$compName = $comp_name;
		}

		if ($component->type == 0) {
			$filename = DOCROOT.WEBROOT.DATA_DIR.COMPONENTS_PATH.$package.DS.'config.xml';
		} else {
			$filename = DOCROOT.WEBROOT.DATA_DIR.CONTROLLERS_PATH.$package.DS.'config.xml';
		}

		if (!file_exists($filename)) {
			if ($component->type == 0) {
				$filename = DOCROOT.WEBROOT.COMPONENTS_PATH.$package.DS.'config.xml';
			} else {
				$filename = DOCROOT.WEBROOT.CONTROLLERS_PATH.$package.DS.'config.xml';
			}
		}

		if (file_exists($filename)) {
			$xml = new SimpleXMLElement(file_get_contents($filename));
			$xmlController = $xml->xpath('//controller');

			$settings = array();
			foreach ($xmlController as $contr) {
				if ((string) $contr->attributes()->name == $compName) {
					foreach ($contr->children()->ControllerSettings->children() as $setting) {

						//Marrim vetem elementet setting
						if ($setting->getName() == 'setting') {
							//Shtojme settingun tek $settings
							$settings[(string) $setting->attributes()->name] = array('name' => (string) $setting->attributes()->name, 'type' => (string) $setting->attributes()->type, 'label' => (string) $setting->attributes()->label, 'description' => (string) $setting->attributes()->description, 'default' => (string) $setting->attributes()->default);

							//SQLList Type
							if ((string) $setting->attributes()->type == 'sqllist') {

								//$module['params'][(string)$setting->attributes()->name]['options'] = array();
								$settings[(string) $setting->attributes()->name]['fields'] = array();
								foreach ($setting->children() as $field) {
									if ($field->getName() == 'option') {
										array_push($settings[(string) $setting->attributes()->name]['fields'], array('value' => (string) $field->attributes()->value, 'label' => (string) $field->__toString()));
									}
								}

								$source = $setting->children()->source;
								$sql = (string) $source->__toString();
								$result = $this->db->select($sql);

								$settings[(string) $setting->attributes()->name]['source'] = array('value' => (string) $source->attributes()->value, 'label' => (string) $source->attributes()->label, 'elements' => $result);
							}

							//Shohim nqs eshte ne forme liste dhe marrim listen
							if (in_array((string) $setting->attributes()->type, array('radio', 'checkbox', 'select'))) {
								$settings[(string) $setting->attributes()->name]['fields'] = array();
								foreach ($setting->children() as $field) {
									array_push($settings[(string) $setting->attributes()->name]['fields'], array('value' => (string) $field->attributes()->value, 'label' => (string) $field->__toString()));
								}
							}
						}
					}
				}
			}
			return $settings;
		} else {
			return array();
		}
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Component_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}
}
