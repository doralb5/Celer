<?php

require_once DOCROOT.ENTITIES_PATH.'Contract/Contract.php';

class Contracts_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Contract');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.Contract_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getContract($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Contract');
		$this->em->loadById($id);
		$contract = $this->em->getEntities();
		return (count($contract)) ? $contract[0] : null;
	}

	public function saveContract($contract)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Contract');
		$res = $this->em->persist($contract);
		return $res;
	}

	public function deleteContract($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Contract');
		$res = $this->em->delete('Contract', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Contract');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Contract_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getContractServices($id_contract)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ContractService');
		$this->em->loadByFilter("id_contract = {$id_contract}");
		$result = $this->em->getEntities();
		return $result;
	}

	public function getContractService($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ContractService');
		$this->em->loadById($id);
		$cs = $this->em->getEntities();
		return (count($cs)) ? $cs[0] : null;
	}

	public function saveContractService($cs)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ContractService');
		$res = $this->em->persist($cs);
		return $res;
	}

	public function deleteContractService($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ContractService');
		$res = $this->em->delete('ContractService', $id);
		return $res;
	}

	public function getContractFiles($id_contract)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ContractFile');
		$this->em->loadByFilter("id_contract = {$id_contract}");
		$result = $this->em->getEntities();
		return $result;
	}

	public function getContractFile($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ContractFile');
		$this->em->loadById($id);
		$cf = $this->em->getEntities();
		return (count($cf)) ? $cf[0] : null;
	}

	public function saveContractFile($cf)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ContractFile');
		$res = $this->em->persist($cf);
		return $res;
	}

	public function deleteContractFile($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ContractFile');
		$res = $this->em->delete('ContractFile', $id);
		return $res;
	}

	public function getConSerData($id_conser, $id_field)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ContractServiceData');
		$this->em->loadByFilter("id_contract_service = $id_conser AND id_field = $id_field");
		$index = $this->em->getEntities();
		if (count($index)) {
			return $index[0];
		} else {
			return null;
		}
	}

	public function saveConSerData($csdata)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ContractServiceData');
		$res = $this->em->persist($csdata);
		return $res;
	}

	public function getAutogenerateNo()
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Contract');
		$sql = "SELECT MAX(number) AS max_num FROM Contract WHERE number REGEXP '^[0-9]+$';";
		$this->em->loadByQuery($sql);
		$cf = $this->em->getEntities();
		if (count($cf) && !is_null($cf[0]->max_num)) {
			$number = $cf[0]->max_num;
		} else {
			$number = rand(1000, 10000);
		}
		return $number;
	}

	public function existNumber($number)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Contract');
		$this->em->loadByFilter('number = :num', 1, 0, '', array(':num' => $number));
		$con = $this->em->getEntities();
		return (count($con)) ? true : false;
	}
}
