<?php

require_once DOCROOT.ENTITIES_PATH.'Contract/ServiceModel.php';
require_once DOCROOT.ENTITIES_PATH.'Contract/ServiceModelField.php';

class ServiceModels_Model extends BaseModel
{
	private $lastCounter = 0;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceModel');
		$this->em->loadByFilter($filter, $limit, $offset);
		$res = $this->em->getEntities();
		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.ServiceModel_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceModel');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = ServiceModel_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getServiceModel($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceModel');
		$this->em->loadById($id);
		$serModel = $this->em->getEntities();
		return $serModel[0];
	}

	public function getModelFields($id_model)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceModelField');
		$this->em->loadByFilter("id_model = $id_model", 50, 0);
		$serModel_fields = $this->em->getEntities();
		return $serModel_fields;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function saveServiceModel($serModel)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceModel');
		$res = $this->em->persist($serModel);
		return $res;
	}

	public function saveModelField($ModelField)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceModelField');
		$res = $this->em->persist($ModelField);
		return $res;
	}

	public function deleteServiceModel($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceModel');
		$res = $this->em->delete('ServiceModel', $id);
		return $res;
	}

	public function deleteModelField($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceModelField');
		$res = $this->em->delete('ServiceModelField', $id);
		return $res;
	}
}
