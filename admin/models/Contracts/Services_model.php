<?php

require_once DOCROOT.ENTITIES_PATH.'Contract/Service.php';

class Services_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Service');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.Service_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getService($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Service');
		$this->em->loadById($id);
		$service = $this->em->getEntities();
		return (count($service)) ? $service[0] : null;
	}

	public function saveService($service)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Service');
		$res = $this->em->persist($service);
		return $res;
	}

	public function deleteService($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Service');
		$res = $this->em->delete('Service', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('Service');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Service_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getServiceCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceCategory');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		//Marrim dhe numrin e dokumenteve
		foreach ($result as &$categ) {
			$tot_ser = $this->db->select('SELECT COUNT(*) AS total FROM Service WHERE id_category = :categ', array(':categ' => $categ->id));
			$categ->total_services = $tot_ser[0]['total'];
		}
		return $result;
	}

	public function getServiceCategory($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceCategory');
		$this->em->loadById($id);
		$sc = $this->em->getEntities();
		return (count($sc)) ? $sc[0] : null;
	}

	public function saveServiceCategory($sc)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceCategory');
		$res = $this->em->persist($sc);
		return $res;
	}

	public function deleteServiceCategory($id)
	{
		$this->em->setRepository('Contract');
		$this->em->setEntity('ServiceCategory');
		$res = $this->em->delete('ServiceCategory', $id);
		return $res;
	}
}
