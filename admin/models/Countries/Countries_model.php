<?php

require_once DOCROOT.ENTITIES_PATH.'Generic/Country.php';

class Countries_Model extends BaseModel
{
	private $lastCounter;

	public function getCountriesList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Country');

		if ($order == '') {
			$order = 'id ASC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Country_Table = TABLE_PREFIX.'Country';
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Country_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getCities($filter = '', $order = '')
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('City');

		if ($order == '') {
			$order = 'id ASC';
		}
		$this->em->loadByFilter($filter);
		$result = $this->em->getEntities();
		return $result;
	}
}
