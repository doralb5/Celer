<?php

class Dashboard_Model extends BaseModel
{
	public function getWidgetsByXML($comp)
	{
		if ((strpos($comp->name, '/') !== false)) {
			list($package, $moduleName) = explode('/', $comp->name);
		} else {
			$package = $comp->name;
			$moduleName = $comp->name;
		}

		$filename = DOCROOT.WEBROOT.COMPONENTS_PATH.$package.DS.'config.xml';
		if (!file_exists($filename)) {
			$filename = DOCROOT.WEBROOT.CONTROLLERS_PATH.$package.DS.'config.xml';
		}

//
//
		//        if ($comp->type == 0) {
		//            $filename = DOCROOT . WEBROOT . COMPONENTS_PATH . $package . DS . 'config.xml';
//
		//        } else {
		//            $filename = DOCROOT . WEBROOT . CONTROLLERS_PATH . $package . DS . 'config.xml';
		//        }

		if (file_exists($filename)) {
			$widgets = array();

			$xml = new SimpleXMLElement(file_get_contents($filename));
			$dashboardWidgets = $xml->xpath('//DashboardWidgets/widget');

			foreach ($dashboardWidgets as $widget) {
				$parameters = array();
				if (count($widget->children())) {
					foreach ($widget->children() as $wdg) {
						if ($wdg->getName() == 'parameter') {
							//key => value
							$parameters[(string) $wdg->attributes()->name] = (string) $wdg->attributes()->value;
						}
					}
				}

				array_push($widgets, array('name' => $xml->attributes()->package.'/'.$widget->attributes()->name, 'parameters' => $parameters));
			}

			return $widgets;
		}
	}
}
