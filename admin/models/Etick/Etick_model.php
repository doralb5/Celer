<?php

class Etick_Model extends BaseModel
{
	private $lastCounter;

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getEvents($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickEvent');
		if ($order == '') {
			$order = ' start_date DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getEvent($id = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickEvent');
		$this->em->loadById($id);
		$event = $this->em->getEntities();
		return (count($event)) ? $event[0] : null;
	}

	public function saveEvent($event)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickEvent');
		$res = $this->em->persist($event);
		return $res;
	}

	public function deleteEvent($id)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickEvent');
		$res = $this->em->delete('EtickEvent', $id);
		return $res;
	}

	public function getPlaces($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPlace');
		if ($order == '') {
			$order = ' name ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getPlace($id)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPlace');
		$this->em->loadById($id);
		$place = $this->em->getEntities();
		return (count($place)) ? $place[0] : null;
	}

	public function savePlace($place)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPlace');
		$res = $this->em->persist($place);
		//print_r($res);exit;
		return $res;
	}

	public function getPrices($limit = 300, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPrice');
		$order = ' price ASC';
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getPrice($id)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPrice');
		$this->em->loadById($id);
		$result = $this->em->getEntities();
		return $result;
	}

	public function savePrice($price)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPrice');
		$res = $this->em->persist($price);
		return $res;
	}

	public function deletePrice($id)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPrice');
		$res = $this->em->delete('EtickPrice', $id);
		return $res;
	}

	public function getZones($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickZona');
		if ($order == '') {
			$order = ' name ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getReservations($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReservation');
		if ($order == '') {
			$order = ' creation_date DESC';
		}
		if ($filter == '') {
			$filter = ' res.id IS NOT NULL';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getSeats($limit = 9999, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickSeat');
		if ($order == '') {
			$order = ' name ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getUserCategories($limit = 50, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserCategory');
		if ($order == '') {
			$order = ' category ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getReservation($id = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReservation');
		$this->em->loadById($id);
		$reservation = $this->em->getEntities();
		return (count($reservation)) ? $reservation[0] : null;
	}

	public function saveReservation($reservation)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReservation');
		$res = $this->em->persist($reservation);
		return $res;
	}

	public function saveReserveTime($rule)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReserveTime');
		$res = $this->em->persist($rule);
		return $res;
	}

	public function getReserveTimeRules($limit = -1, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReserveTime');
		if ($order == '') {
			$order = ' id ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getReserveTimeRule($id = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReserveTime');
		$this->em->loadById($id);
		$reservation = $this->em->getEntities();
		return (count($reservation)) ? $reservation[0] : null;
	}

	public function deleteReserveTimeRule($id)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReserveTime');
		$res = $this->em->delete('EtickReserveTime', $id);
		return $res;
	}

	public function deleteReservation($id)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReservation');
		$res = $this->em->delete('EtickReservation', $id);
		return $res;
	}
}
