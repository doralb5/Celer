<?php

require_once DOCROOT.ENTITIES_PATH.'Gallery/GalleryItem.php';

class Gallery_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Gallery');
		$this->em->setEntity('GalleryItem');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$GalleryItem_Table = TABLE_PREFIX.GalleryItem_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $GalleryItem_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getItem($id)
	{
		$this->em->setRepository('Gallery');
		$this->em->setEntity('GalleryItem');
		$this->em->loadById($id);
		$item = $this->em->getEntities();
		return (count($item)) ? $item[0] : null;
	}

	public function saveItem($item)
	{
		$this->em->setRepository('Gallery');
		$this->em->setEntity('GalleryItem');
		$res = $this->em->persist($item);
		return $res;
	}

	public function deleteItem($id)
	{
		$this->em->setRepository('Gallery');
		$this->em->setEntity('GalleryItem');
		$res = $this->em->delete('GalleryItem', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Gallery');
		$this->em->setEntity('GalleryItem');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = GalleryItem_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Gallery');
		$this->em->setEntity('GalleryCategory');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$GalleryCategory_Table = TABLE_PREFIX.GalleryCategory_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $GalleryCategory_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];

		//Marrim dhe numrin e dokumenteve
		$GalleryItem_Table = TABLE_PREFIX.GalleryItem_Entity::TABLE_NAME;
		foreach ($result as &$categ) {
			$tot_img = $this->db->select("SELECT COUNT(*) AS total FROM $GalleryItem_Table WHERE id_category = :categ", array(':categ' => $categ->id));
			$categ->total_images = $tot_img[0]['total'];
		}

		return $result;
	}

	public function getCategory($id)
	{
		$this->em->setRepository('Gallery');
		$this->em->setEntity('GalleryCategory');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveCategory($category)
	{
		$this->em->setRepository('Gallery');
		$this->em->setEntity('GalleryCategory');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('Gallery');
		$this->em->setEntity('GalleryCategory');
		$res = $this->em->delete('GalleryCategory', $id);
		return $res;
	}
}
