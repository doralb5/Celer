<?php

class SideBar_Model extends BaseModel
{
	public function getMenuByXml($package)
	{
		$filename = DOCROOT.WEBROOT.COMPONENTS_PATH.$package.DS.'config.xml';

		if (!file_exists($filename)) {
			$filename = DOCROOT.WEBROOT.CONTROLLERS_PATH.$package.DS.'config.xml';
		}

		$menu = array();
		if (file_exists($filename)) {
			$xml = new SimpleXMLElement(file_get_contents($filename));
			$xmlsidemenu = $xml->xpath('//sidemenu');

			if (count($xmlsidemenu)) {
				for ($i = 0; $i < count($xmlsidemenu); $i++) {
					$sidemenu = $xmlsidemenu[$i];

					$menu[$i]['label'] = (string) $sidemenu->attributes()->label;
					$menu[$i]['action'] = (isset($sidemenu->attributes()->action)) ? (string) $sidemenu->attributes()->action : '';
					$menu[$i]['icon'] = (isset($sidemenu->attributes()->icon)) ? (string) $sidemenu->attributes()->icon : '';
					$menu[$i]['controller'] = (isset($sidemenu->attributes()->controller)) ? (string) $sidemenu->attributes()->controller : '';

					$menu[$i]['items'] = array();
					foreach ($sidemenu->children() as $item) {

						//Ne rastet kur kemi me submenu

						if ((string) $item->getName() == 'itemgroup') {
							$groupitems = array();
							foreach ($item->children() as $subitem) {
								array_push($groupitems, array('action' => (string) $subitem->attributes()->action, 'controller' => (string) $subitem->attributes()->controller, 'icon' => (string) $item->attributes()->icon, 'label' => $subitem->__toString()));
							}
							array_push($menu[$i]['items'], array('label' => (string) $item->attributes()->label, 'icon' => (string) $item->attributes()->icon, 'groupitems' => $groupitems));

							continue;
						}

						array_push($menu[$i]['items'], array('action' => (string) $item->attributes()->action, 'controller' => (string) $item->attributes()->controller, 'icon' => (string) $item->attributes()->icon, 'label' => $item->__toString()));
					}
				}
			}
		}
		return $menu;
	}
}
