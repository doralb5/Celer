<?php

require_once DOCROOT.ENTITIES_PATH.'InstantCall/InstantCall.php';

class InstantCalls_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('InstantCall');
		$this->em->setEntity('InstantCall');

		if ($order == '') {
			$order = 'creation_date DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.InstantCall_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('InstantCall');
		$this->em->setEntity('InstantCall');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = InstantCall_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getInstantCall($id)
	{
		$this->em->setRepository('InstantCall');
		$this->em->setEntity('InstantCall');
		$this->em->loadById($id);
		$insCall = $this->em->getEntities();
		return (count($insCall)) ? $insCall[0] : null;
	}

	public function saveInstantCall($insCall)
	{
		$this->em->setRepository('InstantCall');
		$this->em->setEntity('InstantCall');
		$res = $this->em->persist($insCall);
		return $res;
	}
}
