<?php

class Layouts_Model extends BaseModel
{
	public function getLayoutsListByXml($template_name)
	{
		$filename = $template_name.DS.'config.xml';
		//Kontrollojme per template te personalizuar
		if (file_exists(DOCROOT.DATA_DIR.TEMPLATES_PATH.$filename)) {
			$filename = DOCROOT.DATA_DIR.TEMPLATES_PATH.$filename;
		} else {
			$filename = DOCROOT.TEMPLATES_PATH.$filename;
		}

		if (file_exists($filename)) {
			$layouts = array();

			$xml = new SimpleXMLElement(file_get_contents($filename));
			$xmllayouts = $xml->xpath('//Layouts/layout');

			foreach ($xmllayouts as $l) {
				$name = (string) $l->attributes()->name;
				array_push($layouts, $name);
			}
			return $layouts;
		}
	}

	public function getPositionsListByXml($template_name, $layout)
	{
		$filename = $template_name.DS.'config.xml';
		//Kontrollojme per template te personalizuar
		if (file_exists(DOCROOT.DATA_DIR.TEMPLATES_PATH.$filename)) {
			$filename = DOCROOT.DATA_DIR.TEMPLATES_PATH.$filename;
		} else {
			$filename = DOCROOT.TEMPLATES_PATH.$filename;
		}

		if (file_exists($filename)) {
			$positions = array();
			$xml = new SimpleXMLElement(file_get_contents($filename));
			$xmllayouts = $xml->xpath('//Layouts/layout');

			foreach ($xmllayouts as $l) {
				if ((string) $l->attributes()->name == $layout) {
					foreach ($l->children() as $position) {
						array_push($positions, array('name' => (string) $position->attributes()->name, 'label' => (string) $position->attributes()->label, 'description' => (string) $position->attributes()->description));
					}
				}
			}
			return $positions;
		} else {
			return array();
		}
	}
}
