<?php

require_once DOCROOT.ENTITIES_PATH.'Menu/Menu.php';

class Menus_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('Menu');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Menu_Table = TABLE_PREFIX.Menu_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Menu_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getMenu($id)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('Menu');
		$this->em->loadById($id);
		$Menu = $this->em->getEntities();
		return (count($Menu)) ? $Menu[0] : null;
	}

	public function saveMenu($Menu)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('Menu');
		$res = $this->em->persist($Menu);
		return $res;
	}

	public function deleteMenu($id)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('Menu');
		$res = $this->em->delete('Menu', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('Menu');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Menu_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getMenuDetails($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuDetail');
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function existMenuDetail($filter = '')
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuDetail');
		$this->em->loadByFilter($filter);
		$result = $this->em->getEntities();
		return (count($result) ? true : false);
	}

	public function getMenuDetail($id)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuDetail');
		$this->em->loadById($id);
		$detail = $this->em->getEntities();
		return (count($detail)) ? $detail[0] : null;
	}

	public function saveMenuDetail($detail)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuDetail');
		$res = $this->em->persist($detail);
		return $res;
	}

	public function deleteMenuDetail($id)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuDetail');
		$res = $this->em->delete('MenuDetail', $id);
		return $res;
	}

	public function getMenuItems($filter = '')
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuItem');
		$this->em->loadByFilter($filter, 100, 0, 'parent_id ASC, sorting');
		$result = $this->em->getEntities();

		$pages_md = Loader::getModel('Pages');
		foreach ($result as &$item) {
			if (is_numeric($item->target)) {
				$page = $pages_md->getPage($item->target);
				if (!is_null($page)) {
					$item->target = $page->name;
				}
			}

			$this->hierarchyItems($item, $item->item);
		}
		return $result;
	}

	public function getMenuItem($id)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuItem');
		$this->em->loadById($id);
		$item = $this->em->getEntities();
		return (count($item)) ? $item[0] : null;
	}

	public function saveMenuItem($item)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuItem');
		$res = $this->em->persist($item);
		return $res;
	}

	public function deleteMenuItem($id)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuItem');
		$res = $this->em->delete('MenuItem', $id);
		return $res;
	}

	private function hierarchyItems($item, &$item_name)
	{
		if (!is_null($item->parent_id)) {
			$itm = $this->getMenuItem($item->parent_id);
			$item_name = $itm->item.' <b>&raquo</b> '.$item_name;
			$this->hierarchyItems($itm, $item_name);
		} else {
			return;
		}
	}

	public function checkSubmenuLimit($id_item)
	{
		$nivel = 0;
		$this->submenuLevel($id_item, $nivel);

		if ($nivel >= MAX_SUBMENUS) {
			return true;
		} else {
			return false;
		}
	}

	private function submenuLevel($id_item, &$level)
	{
		$item = $this->getMenuItem($id_item);
		if (!is_null($item->parent_id)) {
			$level++;
			$this->submenuLevel($item->parent_id, $level);
		} else {
			return $level;
		}
	}
}
