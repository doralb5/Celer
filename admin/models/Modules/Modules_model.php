<?php

require_once DOCROOT.ENTITIES_PATH.'Page/Module.php';

class Modules_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Module');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Module_Table = TABLE_PREFIX.Module_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Module_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getModule($id)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Module');
		$this->em->loadById($id);
		$module = $this->em->getEntities();
		return (count($module)) ? $module[0] : null;
	}

	public function saveModule($module)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Module');
		$res = $this->em->persist($module);
		return $res;
	}

	public function deleteModule($id)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Module');
		$res = $this->em->delete('Module', $id);
		return $res;
	}

	public function existModule($name)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Module');
		$this->em->loadByFilter("name = '$name'");
		$result = $this->em->getEntities();
		if (count($result)) {
			return true;
		} else {
			return false;
		}
	}
}
