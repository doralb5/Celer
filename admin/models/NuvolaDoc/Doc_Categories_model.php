<?php

require_once DOCROOT.CLASSES_PATH.'NuvolaDoc_entities/DocCategory.php';

class Doc_Categories_Model extends BaseModel
{
	private $sql_select;
	private $sql_from;
	private $lastCounter = 0;

	public function __construct()
	{
		parent::__construct();

		$this->sql_select = 'SELECT * ';

		$this->sql_from = ' FROM doc_categories ';
	}

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocCategory');

		if ($order == '') {
			$order = 'name ASC,id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$res = $this->em->getEntities();

		//Marrim dhe numrin e dokumenteve
		foreach ($res as &$categ) {
			$tot_docs = $this->db->select('SELECT COUNT(*) AS total FROM docs WHERE id_category = :categ', array(':categ' => $categ->id));
			$categ->total_docs = $tot_docs[0]['total'];
		}

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.DocCategory_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		$se->select($this->sql_select);
		$se->from($this->sql_from);
		$se->where('WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$se->orderby(($sorting != '') ? $sorting : 'name ASC,id desc');
		$se->limit($limit, $offset);
		$res = $se->search($keywords, $searchFields, $this->lastCounter);
		$res = Utils::ArrayToEntities($res, 'DocCategory_Entity');
		return $res;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getDocCategory($id)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocCategory');
		$this->em->loadById($id);
		$doccategory = $this->em->getEntities();
		if (count($doccategory)) {
			return $doccategory[0];
		} else {
			return null;
		}
	}

	public function saveDocCategory($docCategory)
	{
		$res = $this->em->persist($docCategory);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->delete('DocCategory', $id);
	}

	public function categoriesNumber()
	{
		$tot_count = $this->db->select('SELECT COUNT(*) AS total FROM '.DocCategory_Entity::TABLE_NAME);
		return $tot_count[0]['total'];
	}
}
