<?php

require_once DOCROOT.CLASSES_PATH.'NuvolaDoc_entities/DocModel.php';
require_once DOCROOT.CLASSES_PATH.'NuvolaDoc_entities/DocModelField.php';

class Doc_Models_Model extends BaseModel
{
	private $sql_select;
	private $sql_from;
	private $lastCounter = 0;

	public function __construct()
	{
		parent::__construct();

		$this->sql_select = 'SELECT * ';

		$this->sql_from = ' FROM doc_models ';
	}

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocModel');
		$this->em->loadByFilter($filter, $limit, $offset);

		$res = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.DocModel_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];

		//$this->addCalculatedFields($res);

		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		$se->select($this->sql_select);
		$se->from($this->sql_from);
		$se->where('WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$se->orderby(($sorting != '') ? $sorting : 'name ASC,id desc');
		$se->limit($limit, $offset);
		$res = $se->search($keywords, $searchFields, $this->lastCounter);
		$res = Utils::ArrayToEntities($res, 'DocModel_Entity');
		return $res;
	}

	public function getDocModel($id)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocModel');
		$this->em->loadById($id);
		$docmodel = $this->em->getEntities();
		return $docmodel[0];
	}

	public function getDocModelFields($id_model)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocModelField');
		$this->em->loadByFilter("id_model = $id_model", 50, 0);
		$docmodel_fields = $this->em->getEntities();
		return $docmodel_fields;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function saveDocModel($docModel)
	{
		$res = $this->em->persist($docModel);
		return $res;
	}

	public function saveDocModelField($docModelField)
	{
		$res = $this->em->persist($docModelField);
		return $res;
	}

	public function deleteDocModel($id)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$res = $this->em->delete('DocModel', $id);
		return $res;
	}

	public function deleteDocModelField($id)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$res = $this->em->delete('DocModelField', $id);
		return $res;
	}

	public function modelsNumber()
	{
		$tot_count = $this->db->select('SELECT COUNT(*) AS total FROM '.DocModel_Entity::TABLE_NAME);
		return $tot_count[0]['total'];
	}
}
