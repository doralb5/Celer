<?php

require_once DOCROOT.CLASSES_PATH.'NuvolaDoc_entities/Document.php';
require_once DOCROOT.CLASSES_PATH.'NuvolaDoc_entities/DocIndex.php';
require_once DOCROOT.CLASSES_PATH.'NuvolaDoc_entities/DocFile.php';
require_once DOCROOT.CLASSES_PATH.'NuvolaDoc_entities/DocTag.php';
require_once DOCROOT.CLASSES_PATH.'NuvolaDoc_entities/DocModel.php';
require_once DOCROOT.CLASSES_PATH.'NuvolaDoc_entities/DocModelField.php';
require_once DOCROOT.CLASSES_PATH.'NuvolaDoc_entities/DocCategory.php';

class Documents_Model extends BaseModel
{
	private $lastCounter = 0;
	private $sql_select;
	private $sql_from;

	public function __construct()
	{
		parent::__construct();
		$this->sql_select = 'SELECT * ';

		$this->sql_from = ' FROM docs  ';
	}

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('Document');

		if ($order == '') {
			$order = 'docs.creation_date ASC, docs.id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$res = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.Document_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $res;
	}

	public function getFileList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocFile');

		if ($order == '') {
			$order = 'docs_files.id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$res = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.DocFile_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $res;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getDocument($id)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('Document');
		$this->em->loadById($id);
		$document = $this->em->getEntities();
		if (count($document)) {
			return $document[0];
		} else {
			return null;
		}
	}

	public function saveDocument($document)
	{
		$res = $this->em->persist($document);
		return $res;
	}

	public function getDocIndexByDoc($id_doc, $id_field)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocIndex');
		$this->em->loadByFilter("id_doc = $id_doc AND id_field = $id_field");
		$index = $this->em->getEntities();
		if (count($index)) {
			return $index[0];
		} else {
			return null;
		}
	}

	public function saveDocIndex($docindex)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocIndex');
		$res = $this->em->persist($docindex);
		return $res;
	}

	public function saveDocFile($docfile)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocFile');
		$res = $this->em->persist($docfile);
		return $res;
	}

	public function getDocTags($id_doc)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocTag');
		$this->em->loadByFilter("id_doc = $id_doc", 100, 0, 'id');
		$doctags = $this->em->getEntities();
		return $doctags;
	}

	public function saveDocTag($doctag)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocTag');
		$res = $this->em->persist($doctag);
		return $res;
	}

	public function deleteDocument($id)
	{
		$doc_files = $this->getDocFiles($id);

		foreach ($doc_files as $file) {
			unlink(DOCROOT.MEDIA_ROOT.'documents'.DS.$file->id_category.DS.$file->filename);
		}

		$this->em->setRepository('NuvolaDoc_entities');
		$res = $this->em->delete('Document', $id);
		return $res;
	}

	public function deleteDocFile($id)
	{
		$doc_file = $this->getDocFile($id);

		unlink(DOCROOT.MEDIA_ROOT.'documents'.DS.$doc_file->id_category.DS.$doc_file->filename);

		$this->em->setRepository('NuvolaDoc_entities');
		$res = $this->em->delete('DocFile', $id);
		return $res;
	}

	public function getDocFiles($id_doc)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocFile');
		$this->em->loadByFilter("id_doc = $id_doc", 100, 0, 'id');
		$doc_files = $this->em->getEntities();
		return $doc_files;
	}

	public function getDocFile($id)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocFile');
		$this->em->loadById($id);
		$docfile = $this->em->getEntities();
		if (count($docfile)) {
			return $docfile[0];
		} else {
			return null;
		}
	}

	public function filesTotalSize()
	{
		$tot_size = $this->db->select('SELECT SUM(size) AS totalSize FROM '.DocFile_Entity::TABLE_NAME);
		return $tot_size[0]['totalSize'];
	}

	public function FastSearch(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		$se->select($this->sql_select);
		$se->from($this->sql_from);
		$se->where('WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$se->orderby(($sorting != '') ? $sorting : 'title ASC,id desc');
		$se->limit($limit, $offset);
		$res = $se->search($keywords, $searchFields, $this->lastCounter);
		$res = Utils::ArrayToEntities($res, 'Document_Entity');
		return $res;
	}

	public function search($id_model, $data)
	{
		$this->em->setRepository('NuvolaDoc_entities');
		$this->em->setEntity('DocModel');
		$this->em->loadById($id_model);
		$docmodel = $this->em->getEntities();

		$this->em->setEntity('DocModelField');
		$this->em->loadByFilter("id_model = $id_model", 50, 0);
		$docmodel_fields = $this->em->getEntities();

		$this->em->setEntity('Document');
		$query = Document_Entity::getSelectQueryObj($this->db);
		$join_tags = DocTag_Entity::TABLE_NAME.' ON '.DocTag_Entity::FK_Document.' = '.Document_Entity::TABLE_NAME.'.'.Document_Entity::INDEX;
		$join_fields = DocModelField_Entity::TABLE_NAME.' ON '.DocModelField_Entity::FK_DocModel.' = '.DocModel_Entity::TABLE_NAME.'.'.DocModel_Entity::INDEX;
		$join_indexes = DocIndex_Entity::TABLE_NAME.' ON '.DocIndex_Entity::FK_Document.' = '.Document_Entity::TABLE_NAME.'.'.Document_Entity::INDEX;
		$query = $query->leftJoin($join_tags)->innerJoin($join_fields)->innerJoin($join_indexes);

		$ms = new QueryBuilder($this->db, $query);

		if ($_POST['title'] != '') {
			$ms->addAndCondition('docs.title LIKE "%'.$_POST['title'].'%"');
		}

		if ($_POST['reference_number'] != '') {
			$ms->addOrCondition('docs.reference_number LIKE "%'.$_POST['reference_number'].'%"');
		}

		if ($_POST['location'] != '') {
			$ms->addOrCondition('docs.location LIKE "%'.$_POST['location'].'%"');
		}

		if ($_POST['tags'] != '') {
			$ms->addOrCondition('docs_tags.tag  LIKE "%'.$_POST['tags'].'%"');
		}

		foreach ($docmodel_fields as $field) {
			if (isset($_POST['f_'.$field->name]) && $_POST['f_'.$field->name] != '') {
				$ms->addOrCondition('docs_indexes.value  LIKE "%'.$_POST['f_'.$field->name].'%"');
			}
		}

		if ($_POST['id_category'] != '') {
			$ms->addAndCondition('docs.id_category = '.$_POST['id_category']);
		}

		$ms->groupBy('docs.id');
		$this->em->loadByQuery($ms->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}
}

class QueryBuilder
{
	public $select_query = '';
	public $where = ' WHERE ';
	public $groupBy = ' GROUP BY ';

	public function __construct($db, $select_from)
	{
		if (is_object($select_from)) {
			$this->select_query = $select_from->getQuery();
		} elseif (is_string($select_from)) {
			$this->select_query = $select_from;
		}

		$this->select_query = trim($this->select_query);
	}

	public function addAndCondition($cond)
	{
		if ($this->where == ' WHERE ') {
			$this->where .= " $cond ";
		} else {
			$this->where .= " AND $cond ";
		}
	}

	public function addOrCondition($cond)
	{
		if ($this->where == ' WHERE ') {
			$this->where .= " $cond ";
		} else {
			$this->where .= " OR $cond ";
		}
	}

	public function addOperator($elem)
	{
		$this->where .= " $elem ";
	}

	public function groupBy($gb)
	{
		$this->groupBy .= " $gb ";
	}

	public function getQuery()
	{
		return $this->select_query.$this->where.$this->groupBy;
	}
}
