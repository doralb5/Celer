<?php

require_once DOCROOT.ENTITIES_PATH.'Page/Page.php';
require_once DOCROOT.ENTITIES_PATH.'Page/PageAlias.php';

class Pages_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 80, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Page');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Page_Table = TABLE_PREFIX.Page_Entity::TABLE_NAME;
		$PageContent_Table = TABLE_PREFIX.PageContent_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Page_Table 
                                        LEFT JOIN $PageContent_Table ON ($PageContent_Table.id_page = $Page_Table.id)
                                        WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getPage($id)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Page');
		$this->em->loadById($id);
		$page = $this->em->getEntities();
		return (count($page)) ? $page[0] : null;
	}

	public function savePage($page)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Page');
		$res = $this->em->persist($page);
		return $res;
	}

	public function deletePage($id)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Page');
		$res = $this->em->delete('Page', $id);
		return $res;
	}

	public function getPageContents($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageContent');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getPageContent($id)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageContent');
		$this->em->loadById($id);
		$pagecon = $this->em->getEntities();
		return (count($pagecon)) ? $pagecon[0] : null;
	}

	public function savePageContent($pagecontent)
	{
		$this->em->setRepository('PageContent');
		$this->em->setEntity('PageContent');
		$res = $this->em->persist($pagecontent);
		return $res;
	}

	public function deletePageContent($id)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageContent');
		$res = $this->em->delete('PageContent', $id);
		return $res;
	}

	public function checkPCAvailability($id_page, $lang)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageContent');
		$this->em->loadByFilter("id_page = $id_page AND lang = '{$lang}'");
		$result = $this->em->getEntities();
		if (count($result)) {
			return true;
		} else {
			return false;
		}
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Page');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Page_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);
		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function setHomePage($idPage)
	{
		$this->db->update(Entity::$table_prefix.Page_Entity::TABLE_NAME, array('home' => 0), "id != '$idPage'");
		$this->db->update(Entity::$table_prefix.Page_Entity::TABLE_NAME, array('home' => 1), "id = '$idPage'");
	}

	public function getPageAliases($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageAlias');
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getPageAlias($id)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageAlias');
		$this->em->loadById($id);
		$alias = $this->em->getEntities();
		return (count($alias)) ? $alias[0] : null;
	}

	public function savePageAlias($alias)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageAlias');
		$res = $this->em->persist($alias);
		return $res;
	}

	public function deletePageAlias($id)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageAlias');
		$res = $this->em->delete('PageAlias', $id);
		return $res;
	}

	public function checkCompActionAvailability($id, $data)
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Page');

		if ($data['id_component'] !== '') {
			$filter = "id_component = {$data['id_component']} AND action = '{$data['action']}' AND action_params = '{$data['action_params']}'";
			if (!is_null($id)) {
				$filter .= " AND id != $id";
			}
			$this->em->loadByFilter($filter);
			$result = $this->em->getEntities();
			if (count($result)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	public function checkPageLimit()
	{
		$Page_Table = TABLE_PREFIX.Page_Entity::TABLE_NAME;
		$sql = "SELECT COUNT(*) AS total FROM $Page_Table";
		$result = $this->db->select($sql);

		if ($result[0]['total'] > MAX_PAGES) {
			return true;
		} else {
			return false;
		}
	}

	public function getParametersByXML($comp_name, $actionName)
	{
		if ((strpos($comp_name, '/') !== false)) {
			list($package, $compName) = explode('/', $comp_name);
		} else {
			$package = $comp_name;
			$compName = $comp_name;
		}

		$filename = DOCROOT.DATA_DIR.COMPONENTS_PATH.$package.DS.'config.xml';

		if (!file_exists($filename)) {
			$filename = DOCROOT.WEBROOT.COMPONENTS_PATH.$package.DS.'config.xml';
		}

		if (file_exists($filename)) {
			$xml = new SimpleXMLElement(file_get_contents($filename));
			$xmlController = $xml->xpath('//controller');

			$parameters = array();
			foreach ($xmlController as $contr) {
				if ((string) $contr->attributes()->name == $compName) {
					foreach ($contr->children()->ControllerActions->children() as $act) {
						if ((string) $act->attributes()->name == $actionName) {
							if (isset($act->children()->Parameters)) {
								foreach ($act->children()->Parameters->children() as $parameter) {
									if ($parameter->getName() == 'parameter') {
										$parameters[(string) $parameter->attributes()->name] = array('name' => (string) $parameter->attributes()->name, 'type' => (string) $parameter->attributes()->type, 'label' => (string) $parameter->attributes()->label, 'description' => (string) $parameter->attributes()->description, 'default' => (string) $parameter->attributes()->default);

										//SQLList Type
										$type = (string) $parameter->attributes()->type;
										if ($type == 'sqllist' || $type == 'sqllist_multi') {
											$parameters[(string) $parameter->attributes()->name]['fields'] = array();

											foreach ($parameter->children() as $field) {
												if ($field->getName() == 'field') {
													array_push($parameters[(string) $parameter->attributes()->name]['fields'], array('value' => (string) $field->attributes()->value, 'label' => (string) $field->__toString()));
												}
											}

											$source = $parameter->children()->source;
											$sql = (string) $source->__toString();
											$result = $this->db->select($sql);
											$parameters[(string) $parameter->attributes()->name]['source'] = array('value' => (string) $source->attributes()->value, 'label' => (string) $source->attributes()->label, 'elements' => $result);
										}

										if (in_array((string) $parameter->attributes()->type, array('radio', 'checkbox', 'select'))) {
											$parameters[(string) $parameter->attributes()->name]['fields'] = array();
											foreach ($parameter->children() as $field) {
												array_push($parameters[(string) $parameter->attributes()->name]['fields'], array('value' => (string) $field->attributes()->value, 'label' => (string) $field->__toString()));
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return $parameters;
		}
	}
}
