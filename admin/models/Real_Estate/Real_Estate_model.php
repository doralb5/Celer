<?php

require_once DOCROOT.ENTITIES_PATH.'Real_Estate/RealEstate.php';

class Real_Estate_Model extends BaseModel
{
	protected $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstate');
		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$RealEstate_Table = TABLE_PREFIX.RealEstate_Entity::TABLE_NAME;
		$this->lastCounter = $this->em->countByFilter($filter);
		foreach ($result as &$res) {
			$res->images = $this->getRealEstateImages(100, 0, " id_realestate = {$res->id} ");
		}
		return $result;
	}

	public function getListRequest($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateRequest');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getRealEstate($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstate');
		$this->em->loadById($id);
		$RealEstate = $this->em->getEntities();
		return (count($RealEstate)) ? $RealEstate[0] : null;
	}

	public function getRequest($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateRequest');
		$this->em->loadById($id);
		$RealEstateRequest = $this->em->getEntities();
		return (count($RealEstateRequest)) ? $RealEstateRequest[0] : null;
	}

	public function saveRealEstate($realestate)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstate');
		$res = $this->em->persist($realestate);
		return $res;
	}

	public function saveRequest($request)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateRequest');
		$res = $this->em->persist($request);
		return $res;
	}

	public function deleteRealEstate($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstate');
		$res = $this->em->delete('RealEstate', $id);
		return $res;
	}

	public function deleteRequest($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateRequest');
		$res = $this->em->delete('RealEstateRequest', $id);
		return $res;
	}

	public function search($keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstate');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = RealEstate_Entity::getSelectQueryObj($this->db);
		if ($sorting != '') {
			$q->orderBy($sorting);
		}
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		$query->limit($limit);
		$query->offset($offset);
		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		//self::setBusinessArray($res);
		return $res;
	}

	public function getTypologies($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateTypology');
		if ($order == '') {
			$order = ' type ASC ';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getFloors($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateFloor');
		if ($order == '') {
			$order = ' name ASC ';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getHeating($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateHeating');
		if ($order == '') {
			$order = ' name ASC ';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getBuildings($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateBuildingState');
		if ($order == '') {
			$order = ' type ASC  ';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getContract($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateContract');
		if ($order == '') {
			$order = ' type ASC ';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getCurrency($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateCurrency');

		if ($order == '') {
			$order = ' type ASC ';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function searchCurrency($keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateTypology');
		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = RealEstateTypology_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);
		if ($sorting != '') {
			$query->orderBy($sorting);
		} else {
			$query->orderBy(' type ASC ');
		}
		$query->limit($limit);
		$query->offset($offset);
		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getTypology($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateTypology');
		$this->em->loadById($id);
		$typology = $this->em->getEntities();
		return (count($typology)) ? $typology[0] : null;
	}

	public function saveTypology($typology)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateTypology');
		$res = $this->em->persist($typology);
		return $res;
	}

	public function deleteTypology($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateTypology');
		$res = $this->em->delete('RealEstateTypology', $id);
		return $res;
	}

	public function getRealEstateImage($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateImage');
		$this->em->loadById($id);
		$evImage = $this->em->getEntities();
		return (count($evImage)) ? $evImage[0] : null;
	}

	public function saveRealEstateImage($evImage)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateImage');
		$res = $this->em->persist($evImage);
		return $res;
	}

	public function deleteRealEstateImage($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateImage');
		$res = $this->em->delete('RealEstateImage', $id);
		return $res;
	}

	public function getRealEstateImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateImage');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	//**************** PDF FLYER *************//

	public function saveFlyer($flyer)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstatePdfFlyer');
		$res = $this->em->persist($flyer);
		return $res;
	}

	public function getFlyers($limit = -1, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstatePdfFlyer');
		if ($order == '') {
			$order = ' creation_date DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getFlyer($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstatePdfFlyer');
		$this->em->loadById($id);
		$evImage = $this->em->getEntities();
		return (count($evImage)) ? $evImage[0] : null;
	}

	public function deleteFlyer($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstatePdfFlyer');
		$res = $this->em->delete('RealEstatePdfFlyer', $id);
		return $res;
	}

	public function saveFlyerItem($item)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstatePdfFlyerItems');
		$res = $this->em->persist($item);
		return $res;
	}

	public function getFlyerItems($limit = -1, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstatePdfFlyerItems');
		if ($order == '') {
			$order = ' cms_RealEstate.title ASC ';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getFlyerItem($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstatePdfFlyerItems');
		$this->em->loadById($id);
		$evImage = $this->em->getEntities();
		return (count($evImage)) ? $evImage[0] : null;
	}

	public function deleteFlyerItem($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstatePdfFlyer');
		$res = $this->em->delete('RealEstatePdfFlyerItems', $id);
		return $res;
	}
}
