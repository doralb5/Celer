<?php

require_once DOCROOT.ENTITIES_PATH.'Registries/Company.php';

class Companies_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Company');

		if ($order == '') {
			$order = 'id DESC';
		}
		$filter = "Company.deleted = '0'";

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.Company_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getCompany($id)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Company');
		$this->em->loadById($id);
		$company = $this->em->getEntities();
		return (count($company)) ? $company[0] : null;
	}

	public function saveCompany($company)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Company');
		$res = $this->em->persist($company);
		return $res;
	}

	public function deleteCompany($id)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Company');
		$res = $this->em->delete('Company', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Company');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Company_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('CompanyCategory');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.CompanyCategory_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getCategory($id)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('CompanyCategory');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveCategory($category)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('CompanyCategory');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('CompanyCategory');
		$res = $this->em->delete('CompanyCategory', $id);
		return $res;
	}

	public function getCompanyTypes($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('CompanyType');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.CompanyType_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getCompanyType($id)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('CompanyType');
		$this->em->loadById($id);
		$type = $this->em->getEntities();
		return (count($type)) ? $type[0] : null;
	}

	public function saveCompanyType($type)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('CompanyType');
		$res = $this->em->persist($type);
		return $res;
	}

	public function deleteCompanyType($id)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('CompanyType');
		$res = $this->em->delete('CompanyType', $id);
		return $res;
	}

	public function existCode($code)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Company');
		$this->em->loadByFilter('code = :code', 1, 0, '', array(':code' => $code));
		$comp = $this->em->getEntities();
		return (count($comp)) ? true : false;
	}
}
