<?php

require_once DOCROOT.ENTITIES_PATH.'Registries/Contact.php';

class Contacts_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Contact');

		if ($order == '') {
			$order = 'Contact.id DESC';
		}
		$filter = '';

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.Contact_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getContact($id)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Contact');
		$this->em->loadById($id);
		$contact = $this->em->getEntities();
		return (count($contact)) ? $contact[0] : null;
	}

	public function saveContact($contact)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Contact');
		$res = $this->em->persist($contact);
		return $res;
	}

	public function deleteContact($id)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Contact');
		$res = $this->em->delete('Contact', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Contact');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Contact_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function existCode($code)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Contact');
		$this->em->loadByFilter('code = :code', 1, 0, '', array(':code' => $code));
		$cont = $this->em->getEntities();
		return (count($cont)) ? true : false;
	}
}
