<?php

require_once DOCROOT.ENTITIES_PATH.'Registries/Lead.php';

class Leads_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Lead');

		if ($order == '') {
			$order = 'Lead.id DESC';
		}
		$filter = '';

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.Lead_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getLead($id)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Lead');
		$this->em->loadById($id);
		$lead = $this->em->getEntities();
		return (count($lead)) ? $lead[0] : null;
	}

	public function saveLead($lead)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Lead');
		$res = $this->em->persist($lead);
		return $res;
	}

	public function deleteLead($id)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Lead');
		$res = $this->em->delete('Lead', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('Lead');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Lead_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getLeadSources()
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('LeadSource');
		$this->em->loadByFilter();
		$result = $this->em->getEntities();
		return $result;
	}

	public function getLeadStatuses()
	{
		$this->em->setRepository('Registries');
		$this->em->setEntity('LeadStatus');
		$this->em->loadByFilter();
		$result = $this->em->getEntities();
		return $result;
	}
}
