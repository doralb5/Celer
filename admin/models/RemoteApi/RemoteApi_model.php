<?php

require_once DOCROOT.ENTITIES_PATH.'User/RemoteUser.php';
require_once DOCROOT.ENTITIES_PATH.'User/RemoteUserSession.php';

class RemoteApi_Model extends BaseModel
{
	public function authenticate($username, $password, &$user)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUser');
		$this->em->loadByFilter("username = :uname AND password = :pass AND enabled = '1'", 1, 0, '', array(':uname' => $username, ':pass' => $password));
		$us = $this->em->getEntities();
		if (count($us)) {
			$user = $us[0];
			return true;
		}
		return false;
	}

	public function saveRemoteUserSession($session)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUserSession');
		$res = $this->em->persist($session);
		return $res;
	}

	public function getRemoteUserSession($id_user)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUserSession');
		$this->em->loadByFilter("id_user = {$id_user}");
		$session = $this->em->getEntities();
		return (count($session)) ? $session[0] : null;
	}

	public function validToken($token, &$user, $time = 24)
	{ //time in hours
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUserSession');
		$this->em->loadByFilter("token = '$token'");
		$session = $this->em->getEntities();

		if (count($session)) {
			$usr_md = Loader::getModel('RemoteUsers');
			$user = $usr_md->getRemoteUser($session[0]->id_user);

			$currentTime = strtotime(date('Y-m-d H:i:s'));
			$tokenTime = strtotime($session[0]->start_time);

			$limit = $time * 60 * 60; //time in seconds

			if ($currentTime - $tokenTime < $limit) {
				return true;
			}
		}
		return false;
	}
}
