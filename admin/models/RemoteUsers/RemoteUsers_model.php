<?php

require_once DOCROOT.ENTITIES_PATH.'User/RemoteUser.php';
require_once DOCROOT.ENTITIES_PATH.'Component/Component.php';

class RemoteUsers_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUser');
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.RemoteUser_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUser');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = RemoteUser_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getRemoteUser($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUser');
		$this->em->loadById($id);
		$user = $this->em->getEntities();
		return (count($user)) ? $user[0] : null;
	}

	public function saveRemoteUser($user)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUser');
		$res = $this->em->persist($user);
		return $res;
	}

	public function getRemoteUserPerms($id_user)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUserPermission');
		$this->em->loadByFilter("id_user = {$id_user}");
		$result = $this->em->getEntities();
		return $result;
	}

	public function deleteRemoteUser($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUser');
		$res = $this->em->delete('RemoteUser', $id);
		return $res;
	}

	public function saveUserPermission($UserPermission)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUserPermission');
		$res = $this->em->persist($UserPermission);
		return $res;
	}

	public function deleteUserPermission($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUserPermission');
		$res = $this->em->delete('RemoteUserPermission', $id);
		return $res;
	}

	public function existUsername($username)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUserPermission');
		$this->em->loadByFilter('username = :username', 1, 0, '', array(':username' => $username));
		$user = $this->em->getEntities();
		return (count($user)) ? true : false;
	}

	public function existPermission($id_user, $id_cmp, $action)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('RemoteUserPermission');
		$this->em->loadByFilter("id_user = {$id_user} AND id_component = {$id_cmp} AND action = '{$action}'");
		$result = $this->em->getEntities();
		if (count($result)) {
			return true;
		} else {
			return false;
		}
	}
}
