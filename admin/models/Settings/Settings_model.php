<?php

require_once DOCROOT.ENTITIES_PATH.'Generic/Setting.php';

class Settings_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 50, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Setting');

		if ($order == '') {
			$order = 'id ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Setting_Table = TABLE_PREFIX.Setting_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Setting_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function saveSetting($setting)
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Setting');
		$res = $this->em->persist($setting);
		return $res;
	}

	public function getSettingByField($field)
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Setting');
		$this->em->loadByFilter("field = '$field'");
		$setting = $this->em->getEntities();
		return (count($setting)) ? $setting[0] : null;
	}
}
