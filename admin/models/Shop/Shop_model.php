<?php

require_once DOCROOT.ENTITIES_PATH.'Shop/shop_Product.php';

class Shop_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Product');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Product_Table = TABLE_PREFIX.shop_Product_Entity::TABLE_NAME;
		$Categ_Table = TABLE_PREFIX.shop_Category_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Product_Table
                                        LEFT JOIN {$Categ_Table} ON ($Categ_Table.id = $Product_Table.id_category)
                                        WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getProduct($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Product');
		$this->em->loadById($id);
		$product = $this->em->getEntities();
		return (count($product)) ? $product[0] : null;
	}

	public function saveProduct($product)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Product');
		$res = $this->em->persist($product);
		return $res;
	}

	public function deleteProduct($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Product');
		$res = $this->em->delete('shop_Product', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Product');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = shop_Product_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Category');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Category_Table = TABLE_PREFIX.shop_Category_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Category_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getCategory($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Category');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveCategory($category)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Category');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Category');
		$res = $this->em->delete('shop_Category', $id);
		return $res;
	}

	public function getBrands($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Brand');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Brand_Table = TABLE_PREFIX.shop_Brand_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Brand_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getBrand($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Brand');
		$this->em->loadById($id);
		$brand = $this->em->getEntities();
		return (count($brand)) ? $brand[0] : null;
	}

	public function saveBrand($brand)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Brand');
		$res = $this->em->persist($brand);
		return $res;
	}

	public function deleteBrand($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Brand');
		$res = $this->em->delete('shop_Brand', $id);
		return $res;
	}

	public function getModels($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductModel');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getModel($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductModel');
		$this->em->loadById($id);
		$model = $this->em->getEntities();
		return (count($model)) ? $model[0] : null;
	}

	public function saveModel($model)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductModel');
		$res = $this->em->persist($model);
		return $res;
	}

	public function deleteModel($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductModel');
		$res = $this->em->delete('shop_ProductModel', $id);
		return $res;
	}

	public function getTypes($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductType');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		//$Type_Table = TABLE_PREFIX . shop_ProductType_Entity::TABLE_NAME;
		//$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Type_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ""));
		//$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getType($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductType');
		$this->em->loadById($id);
		$type = $this->em->getEntities();
		return (count($type)) ? $type[0] : null;
	}

	public function saveType($type)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductType');
		$res = $this->em->persist($type);
		return $res;
	}

	public function deleteType($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductType');
		$res = $this->em->delete('shop_ProductType', $id);
		return $res;
	}

	public function getFeatures($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Feature');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getFeature($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Feature');
		$this->em->loadById($id);
		$feature = $this->em->getEntities();
		return (count($feature)) ? $feature[0] : null;
	}

	public function saveFeature($feature)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Feature');
		$res = $this->em->persist($feature);
		return $res;
	}

	public function deleteFeature($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_Feature');
		$res = $this->em->delete('shop_Feature', $id);
		return $res;
	}

	public function getProductImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductImage');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getProductImage($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductImage');
		$this->em->loadById($id);
		$image = $this->em->getEntities();
		return (count($image)) ? $image[0] : null;
	}

	public function saveProductImage($image)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductImage');
		$res = $this->em->persist($image);
		return $res;
	}

	public function deleteProductImage($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductImage');
		$res = $this->em->delete('shop_ProductImage', $id);
		return $res;
	}

	public function getProdFeatures($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductFeature');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getProdFeature($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductFeature');
		$this->em->loadById($id);
		$feature = $this->em->getEntities();
		return (count($feature)) ? $feature[0] : null;
	}

	public function saveProdFeature($feature)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductFeature');
		$res = $this->em->persist($feature);
		return $res;
	}

	public function deleteProdFeature($id)
	{
		$this->em->setRepository('Shop');
		$this->em->setEntity('shop_ProductFeature');
		$res = $this->em->delete('shop_ProductFeature', $id);
		return $res;
	}
}
