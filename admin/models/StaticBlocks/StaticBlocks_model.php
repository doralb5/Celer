<?php

require_once DOCROOT.ENTITIES_PATH.'StaticBlock/StaticBlock.php';

class StaticBlocks_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlock');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$StaticBlock_Table = TABLE_PREFIX.StaticBlock_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $StaticBlock_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getStaticBlock($id)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlock');
		$this->em->loadById($id);
		$StaticBlock = $this->em->getEntities();
		return ((count($StaticBlock)) ? $StaticBlock[0] : null);
	}

	public function saveStaticBlock($StaticBlock)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlock');
		$res = $this->em->persist($StaticBlock);
		return $res;
	}

	public function deleteStaticBlock($id)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlock');
		$res = $this->em->delete('StaticBlock', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlock');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = StaticBlock_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getStaticBlockContents($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlockContent');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getStaticBlockContent($id)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlockContent');
		$this->em->loadById($id);
		$staticBlockContent = $this->em->getEntities();
		return (count($staticBlockContent)) ? $staticBlockContent[0] : null;
	}

	public function saveStaticBlockContent($staticBlockContent)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlockContent');
		$res = $this->em->persist($staticBlockContent);
		return $res;
	}

	public function deleteStaticBlockContent($id)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlockContent');
		$res = $this->em->delete('StaticBlockContent', $id);
		return $res;
	}

	public function existContent($id_block, $lang)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlockContent');
		$this->em->loadByFilter("id_static_block = $id_block AND lang = '$lang'");
		$result = $this->em->getEntities();
		return (count($result)) ? true : false;
	}
}
