<?php

require_once DOCROOT.ENTITIES_PATH.'Generic/Template.php';

class Templates_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Template');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Template_Table = TABLE_PREFIX.Template_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Template_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getTemplate($id)
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Template');
		$this->em->loadById($id);
		$template = $this->em->getEntities();
		return (count($template)) ? $template[0] : null;
	}

	public function saveTemplate($template)
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Template');
		$res = $this->em->persist($template);
		return $res;
	}

	public function deleteTemplate($id)
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Template');
		$res = $this->em->delete('Template', $id);
		return $res;
	}

	public function existTemplate($name)
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Template');
		$this->em->loadByFilter("name = '$name'");
		$result = $this->em->getEntities();
		if (count($result)) {
			return true;
		} else {
			return false;
		}
	}

	public function enableTemplate($id)
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Template');
		$this->em->loadByFilter("enabled = '1'");
		$result = $this->em->getEntities();
		foreach ($result as &$res) {
			$res->enabled = 0;
			$this->saveTemplate($res);
		}

		$template = $this->getTemplate($id);
		$template->enabled = 1;
		$res = $this->saveTemplate($template);
		return $res;
	}

	public function getOptionsByXml($temp_name)
	{
		$filename = DOCROOT.TEMPLATES_PATH.$temp_name.DS.'config.xml';

		if (file_exists($filename)) {
			$options = array();

			$xml = new SimpleXMLElement(file_get_contents($filename));
			$xmloptions = $xml->xpath('//option');

			foreach ($xmloptions as $opt) {
				array_push($options, array('name' => (string) $opt->attributes()->name, 'type' => (string) $opt->attributes()->type, 'label' => (string) $opt->attributes()->label, 'description' => (string) $opt->attributes()->description, 'default' => (string) $opt->attributes()->default));
			}
			return $options;
		}
	}

	public function getStylesheetsByXml($temp_name)
	{
		$filename = DOCROOT.TEMPLATES_PATH.$temp_name.DS.'config.xml';

		if (file_exists($filename)) {
			$options = array();

			$xml = new SimpleXMLElement(file_get_contents($filename));
			$xmloptions = $xml->xpath('//style');

			foreach ($xmloptions as $opt) {
				array_push($options, array('name' => (string) $opt->attributes()->name, 'file' => (string) $opt->attributes()->file));
			}
			return $options;
		}
	}
}
