<?php

require_once DOCROOT.ENTITIES_PATH.'User/User.php';
require_once DOCROOT.ENTITIES_PATH.'User/UserPermission.php';
require_once DOCROOT.ENTITIES_PATH.'Component/Component.php';

class Users_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$User_Table = TABLE_PREFIX.User_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $User_Table WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');

		require_once DOCROOT.LIBS_PATH.'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = User_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getUser($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$this->em->loadById($id);
		$user = $this->em->getEntities();
		return (count($user)) ? $user[0] : null;
	}

	public function saveUser($user)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$res = $this->em->persist($user);
		return $res;
	}

	public function getUnpermittedComponents($id_user)
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');

		$Component_Table = TABLE_PREFIX.Component_Entity::TABLE_NAME;
		$UserPermission_Table = TABLE_PREFIX.UserPermission_Entity::TABLE_NAME;

		$sql = "SELECT * FROM $Component_Table AS cmp "
				.'WHERE NOT EXISTS '
				."(SELECT * FROM $UserPermission_Table AS up WHERE cmp.id = up.id_component AND id_user = {$id_user}) ";
		$this->em->loadByQuery($sql);
		$res = $this->em->getEntities();
		return $res;
	}

	public function getPermittedComponents($id_user)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserPermission');

		$Component_Table = TABLE_PREFIX.Component_Entity::TABLE_NAME;
		$UserPermission_Table = TABLE_PREFIX.UserPermission_Entity::TABLE_NAME;

		$sql = "SELECT up.*, cmp.name AS comp_name FROM $UserPermission_Table AS up "
				."LEFT JOIN $Component_Table AS cmp ON(cmp.id = up.id_component) "
				."WHERE id_user = {$id_user}";

		$this->em->loadByQuery($sql);
		$res = $this->em->getEntities();
		return $res;
	}

	public function saveUserPermission($UserPermission)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserPermission');
		$res = $this->em->persist($UserPermission);
		return $res;
	}

	public function addUserPerm($userid, $compid)
	{
		$perm = new UserPermission_Entity();
		$perm->id_user = $userid;
		$perm->id_component = $compid;
		return $this->em->persist($perm);
	}

	public function deleteUserPerm($id_user, $id_comp)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserPermission');
		$res = $this->em->delete('UserPermission', array('id_component' => $id_comp, 'id_user' => $id_user));
		return $res;
	}

	public function deleteUserPermByComp($id_comp, $id_user)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserPermission');
		$res = $this->em->delete('UserPermission', array('id_component' => $id_comp, 'id_user' => $id_user));
		return $res;
	}

	public function deleteUserPermission($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserPermission');
		$res = $this->em->delete('UserPermission', $id);
		return $res;
	}

	public function deleteUser($user)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$res = $this->em->delete('User', $user->id);
		return $res;
	}

	public function getBrandInfo()
	{

		//Query per tek databaza e serv.bluehat.al
		$sql = "SELECT Brand.* FROM Brand
                LEFT JOIN Client ON(Client.id_brand = Brand.id)
                LEFT JOIN Website ON(Website.id_client = Client.id)
                WHERE Website.domain = '".DOMAIN."'";
		$res = $this->db->select($sql);
		if (count($res)) {
			return $res[0];
		} else {
			return null;
		}
	}

	public function existEmail($email)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$this->em->loadByFilter('email = :email', 1, 0, '', array(':email' => $email));
		$user = $this->em->getEntities();
		return (count($user)) ? true : false;
	}

	public function existUsername($username)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$this->em->loadByFilter('username = :username', 1, 0, '', array(':username' => $username));
		$user = $this->em->getEntities();
		return (count($user)) ? true : false;
	}

	public function getUserCategList($limit = 30, $offset = 0, $filter = '', $order = '', $set_total = true)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserCategory');
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$UserCategory_Table = TABLE_PREFIX.'UserCategory';

		if ($set_total) {
			$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $UserCategory_Table
                                            WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
			$this->lastCounter = $tot_count[0]['tot'];
		}

		return $result;
	}

	public function getUserCategory($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserCategory');
		$this->em->loadById($id);
		$categ = $this->em->getEntities();
		return (count($categ)) ? $categ[0] : null;
	}

	public function saveUserCategory($categ)
	{
		if ($categ->default == null) {
			$categ->default = '0';
		}
		if ($categ->enabled_on_register == null) {
			$categ->enabled_on_register = '0';
		}
		if ($categ->required_verify == null) {
			$categ->required_verify = '1';
		}
		$this->em->setRepository('User');
		$this->em->setEntity('UserCategory');
		$res = $this->em->persist($categ);
		return $res;
	}

	public function deleteUserCategory($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserCategory');
		$res = $this->em->delete('UserCategory', $id);
		return $res;
	}

	public function getPermModelList($limit = 30, $offset = 0, $filter = '', $order = '', $set_total = true)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserPermModel');
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$UserPermModel_Table = TABLE_PREFIX.'UserPermModel';
		if ($set_total) {
			$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $UserPermModel_Table
                                            WHERE 1 ".(($filter != '') ? "AND $filter" : ''));
			$this->lastCounter = $tot_count[0]['tot'];
		}

		return $result;
	}

	public function getUserPermModel($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserPermModel');
		$this->em->loadById($id);
		$permModel = $this->em->getEntities();
		return (count($permModel)) ? $permModel[0] : null;
	}

	public function saveUserPermModel($permModel)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserPermModel');
		$res = $this->em->persist($permModel);
		return $res;
	}

	public function deleteUserPermModel($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserPermModel');
		$res = $this->em->delete('UserPermModel', $id);
		return $res;
	}

	public function getPermissionModelList($filter = '')
	{
		$this->em->setRepository('User');
		$this->em->setEntity('PermissionModel');
		$this->em->loadByFilter($filter);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getPermissionModel($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('PermissionModel');
		$this->em->loadById($id);
		$permModel = $this->em->getEntities();
		return (count($permModel)) ? $permModel[0] : null;
	}

	public function savePermissionModel($permModel)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('PermissionModel');
		$res = $this->em->persist($permModel);
		return $res;
	}

	public function deletePermissionModel($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('PermissionModel');
		$res = $this->em->delete('PermissionModel', $id);
		return $res;
	}
}
