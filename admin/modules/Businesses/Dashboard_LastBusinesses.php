<?php

class Dashboard_LastBusinesses extends BaseModule
{
	public function execute($parameters = array())
	{
		$model = Loader::getModel('Businesses');
		$logged_business = UserAuth::getLoginSession();
		$Bussines_Table = TABLE_PREFIX.'Business';
		$filter = '1';

		$businesses = $model->getList(10, 0, $filter, 'creation_date DESC');

		$this->view->set('businesses', $businesses);
		$this->view->set('url_business_edit', Utils::getComponentUrl('Businesses/business_edit'));
		$this->view->set('url_business_delete', Utils::getComponentUrl('Businesses/business_delete'));
		$this->view->set('url_business_enable', Utils::getComponentUrl('Businesses/business_enable'));
		$this->view->set('url_business_disable', Utils::getComponentUrl('Businesses/business_disable'));
		return $this->view->render('last-businesses', true);
	}
}
