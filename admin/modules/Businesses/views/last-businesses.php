<?php
require_once(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Businesses');

foreach ($businesses as &$row) {
	if ($row->enabled) {
		$btn1 = new Button('<i class="fa fa-square-o"></i>', $url_business_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button('<i class="fa fa-check-square-o"></i>', $url_business_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}

	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil-square-o"></i>', $url_business_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
	);
}

$table->setElements($businesses);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('company_name', '[$CompanyName]', 'string', 'left'),
	//new TableList_Field('creation_date', '[$CreatedAt]', 'date', 'left'),
));

$table->setUrl_delete($url_business_delete);
$table->renderTopBar(false);
$table->multipleDeletion(false);
$table->setTitle('[$LastBusinesses]');
$table->showResultInfo(false);
?>
<div class="row">
    <?php
	$table->render();
	?>
</div>
