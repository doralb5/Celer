<?php

class Dashboard_Logo extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('logo', CMSSettings::$logo);
		return $this->view->render('dashboard-logo', true);
	}
}
