<div class="panel panel-default" data-sortable-id="ui-widget-1">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                    class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i
                    class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">[$WebsiteLogo]</h4>
    </div>
    <div class="panel-body">
        <?php if (file_exists(DOCROOT.MEDIA_ROOT.$logo) && Utils::allowedFileType(DOCROOT.MEDIA_ROOT.$logo)) {
	?>
            <img src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.$logo ?>" alt="Website Logo"
                 width="100%"/>
        <?php
} else {
		?>
        <p><?= CMSSettings::$website_title?></p>
        <?php
	}?>
    </div>
</div>

