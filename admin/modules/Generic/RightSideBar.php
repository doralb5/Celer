<?php

class RightSideBar extends BaseModule
{
	public function execute($parameters = array())
	{
		$menus = array();
		$comp_md = Loader::getModel('Components');
		$components = $comp_md->getList(999, 0, "enabled = '1' AND type = '1'");

		$login = UserAuth::getLoginSession();
		foreach ($components as $comp) {
			if ($login['super'] != '1' && !UserAuth::checkComponentPerms($comp->name)) {
				continue;
			}

			if ((strpos($comp->name, '/') !== false)) {
				list($package, $compName) = explode('/', $comp->name);
			} else {
				$package = $comp->name;
			}

			if (array_key_exists($package, $menus)) {
				continue;
			}

			$sidebar_md = Loader::getModel('SideBar', 'Generic');
			$mens = $sidebar_md->getMenuByXml($package);

			if (count($mens)) {
				foreach ($mens as $me) {
					if ($me['action'] != '') {
						$me['link'] = $comp->name.'/'.$me['action'];
					}

					foreach ($me['items'] as &$m) {
						if (isset($m['groupitems'])) {
							foreach ($m['groupitems'] as &$gitem) {
								$gitem['link'] = (($gitem['controller'] != '') ? $gitem['controller'] : $comp->name).'/'.$gitem['action'];
							}
							continue;
						} else {
							$m['link'] = (($m['controller'] != '') ? $m['controller'] : $comp->name).'/'.$m['action'];
						}
					}
					$menus[$me['label']] = $me;
				}
			}
		}

		$this->view->set('menus', $menus);

		$this->view->render('sidebar-right');
	}
}
