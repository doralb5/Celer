<?php

class SideBar extends BaseModule
{
	private $menu = array();
	private $items = array();

	public function addMenu($name)
	{
		$menu = $this->getMenu($name);
		if (is_null($menu)) {
			$this->menu[$name] = new SideBarMenu($name);
			$menu = $this->menu[$name];
		}

		return $menu;
	}

	public function getMenu($name)
	{
		if (isset($this->menu[$name])) {
			return $this->menu[$name];
		} else {
			return null;
		}
	}

	public function addItem($menuitem, $menu = null)
	{
		if (is_null($menu)) {
			$this->items[] = $menuitem;
		} else {
			$this->getMenu($name)->addItem($menuitem);
		}
	}

	public function execute($parameters = array())
	{
		$menus = array();
		$comp_md = Loader::getModel('Components');
		$components = $comp_md->getList(999, 0, "enabled = '1' AND (type = '0' OR featured='1') ");

		foreach ($components as $comp) {
			if (!UserAuth::checkComponentPerms($comp->name)) {
				continue;
			}

			if ((strpos($comp->name, '/') !== false)) {
				list($package, $compName) = explode('/', $comp->name);
			} else {
				$package = $comp->name;
			}

			if (array_key_exists($package, $menus)) {
				continue;
			}

			$mens = $this->model->getMenuByXml($package);

			if (count($mens)) {
				foreach ($mens as $me) {
					if ($me['action'] != '') {
						$me['link'] = $comp->name.'/'.$me['action'];
					}

					foreach ($me['items'] as &$m) {
						if (isset($m['groupitems'])) {
							foreach ($m['groupitems'] as &$gitem) {
								$gitem['link'] = (($gitem['controller'] != '') ? $gitem['controller'] : $comp->name).'/'.$gitem['action'];
							}
							continue;
						} else {
							$m['link'] = (($m['controller'] != '') ? $m['controller'] : $comp->name).'/'.$m['action'];
						}
					}
					$menus[$me['label']] = $me;
				}
			}
		}

		$this->view->set('menu', $this->menu);
		$this->view->set('menus', $menus);
		$this->view->render('sidebar');
	}
}

class SideBarMenu
{
	private $name;
	private $items;

	public function __construct($name)
	{
		$this->name = $name;
	}

	public function addItem($name)
	{
		$item = new SideBarMenuItem($name);
		$this->items[] = $item;
	}
}

class SideBarMenuItem
{
	private $name;

	public function __construct($name)
	{
		$this->name = $name;
	}
}
