
<?php
$loggeduser = UserAuth::getLoginSession();
?>	
<!-- begin #sidebar-right -->
<div id="sidebar-right" class="sidebar sidebar-right sidebar-grid">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">

        <?php
		$controller = (FCRequest::getPackage() != '') ? (FCRequest::getPackage().'/'.FCRequest::getController()) : FCRequest::getController();
		$action = FCRequest::getAction();

		//Marrim parasysh edhe argumentet ne url
		if ((strpos($_SERVER['REQUEST_URI'], '?') !== false)) {
			$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
			$args_part = '?'.$uri_parts[1];
		} else {
			$args_part = '';
		}
		?>


        <!-- begin sidebar nav -->
        <ul class="nav m-t-10">
            <li class="nav-header">Super Navigation</li>

            <?php foreach ($menus as $menu) {
			?>

                <?php if ($menu['action'] == '') {
				?>

                    <?php
					if (!UserAuth::checkComponentPerms($menu['controller'])) {
						continue;
					} ?>

                    <?php $active = ($controller == $menu['controller']) ? 'active' : '' ?>
                    <li class="has-sub <?= $active ?>">
                        <a href="javascript:;" class="">
                            <b class="caret pull-right"></b>
                            <i class="<?= ($menu['icon'] != '') ? $menu['icon'] : 'fa fa-book' ?>"></i>
                            <span>[$<?= str_replace(' ', '', $menu['label']); ?>]</span> 
                        </a>


                        <ul class="sub-menu">

                            <?php foreach ($menu['items'] as $item) {
						?>

                                <?php if (isset($item['groupitems'])) {
							?>
                                    <li class="has-sub">
                                        <a href="javascript:;">
                                            <b class="caret pull-right"></b>
                                            <i class="<?= ($item['icon'] != '') ? $item['icon'] : '' ?>"></i>
                                            [$<?= str_replace(' ', '', $item['label']); ?>]
                                        </a>
                                        <ul class="sub-menu">
                                            <?php foreach ($item['groupitems'] as $gitem) {
								?>
                                                <li>
                                                    
                                                    <a href="<?= Utils::getComponentUrl($gitem['link']) ?>">
                                                        <i class="<?= ($gitem['icon'] != '') ? $gitem['icon'] : '' ?>"></i>&nbsp;&nbsp;
                                                        [$<?= str_replace(' ', '', $gitem['label']); ?>]
                                                    </a>
                                                </li>
                                            <?php
							} ?>
                                        </ul>
                                    </li>
                                    <?php
						} else {
							$subactive = ($controller.'/'.$action.$args_part == $item['link']) ? 'active' : ''; ?>

                                    <li class="<?= $subactive ?>">
                                        <a href="<?= Utils::getComponentUrl($item['link']) ?>">
                                            <i class="<?= ($item['icon'] != '') ? $item['icon'] : '' ?>"></i>&nbsp;&nbsp;
                                            [$<?= str_replace(' ', '', $item['label']); ?>]
                                        </a>
                                    </li>
                                <?php
						} ?>
                            <?php
					} ?>
                        </ul>
                    </li>
                    <?php
			} else {
				$active = ($controller.'/'.$action.$args_part == $menu['link']) ? 'single-active' : ''; ?>

                    <li class="<?= $active ?>">
                        <a href="<?= Utils::getComponentUrl($menu['link']) ?>">
                            <i class="<?= ($menu['icon'] != '') ? $menu['icon'] : 'fa fa-book' ?>"></i>
                            <span>[$<?= str_replace(' ', '', $menu['label']); ?>]</span>
                        </a>
                    </li>

                <?php
			} ?>
            <?php
		} ?>
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg sidebar-right"></div>
<!-- end #sidebar -->