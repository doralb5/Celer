<?php
$loggeduser = UserAuth::getLoginSession();
?>	
<!-- begin #sidebar -->
<div id="sidebar" class="sidebar sidebar-left sidebar-grid">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <?php
				$profile_img = ($loggeduser['image'] != '') ? Utils::genThumbnailUrl('users/'.$loggeduser['image'], 100, 100, array('zc' => 1)) : $this->template_path.'assets/img/img_profile.png';
				?>
                <div class="image">
                    <a href="javascript:;"><img src="<?= $profile_img ?>" alt="" /></a>
                </div>
                <div class="info">
                    <?= $loggeduser['firstname'] ?> <?= $loggeduser['lastname'] ?>
                    <small></small>
                </div>
            </li>
        </ul>
        <!-- end sidebar user -->


        <?php
		$controller = (FCRequest::getPackage() != '') ? (FCRequest::getPackage().'/'.FCRequest::getController()) : FCRequest::getController();
		$action = FCRequest::getAction();

		//Marrim parasysh edhe argumentet ne url
		if ((strpos($_SERVER['REQUEST_URI'], '?') !== false)) {
			$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
			$args_part = '?'.$uri_parts[1];
		} else {
			$args_part = '';
		}
		?>


        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">Navigation</li>
            <li class="<?= ($controller == 'Dashboard') ? 'active' : '' ?>">
                <a href="<?= Utils::getComponentUrl('Dashboard') ?>">
                    <i class="fa fa-laptop"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <?php foreach ($menus as $menu) {
			?>

                <?php if ($menu['action'] == '') {
				?>

                    <?php
					if (!UserAuth::checkComponentPerms($menu['controller'])) {
						continue;
					} ?>

                    <?php $active = ($controller == $menu['controller']) ? 'active' : '' ?>
                    <li class="has-sub <?= $active ?>">
                        <a href="javascript:;" class="">
                            <b class="caret pull-right"></b>
                            <i class="<?= ($menu['icon'] != '') ? $menu['icon'] : 'fa fa-book' ?>"></i>
                            <span>[$<?= str_replace(' ', '', $menu['label']); ?>]</span> 
                        </a>


                        <ul class="sub-menu">

                            <?php foreach ($menu['items'] as $item) {
						?>

                                <?php if (isset($item['groupitems'])) {
							?>
                                    <li class="has-sub">
                                        <a href="javascript:;">
                                            <b class="caret pull-right"></b>
                                            <i class="<?= ($item['icon'] != '') ? $item['icon'] : '' ?>"></i>
                                            [$<?= str_replace(' ', '', $item['label']); ?>]
                                        </a>
                                        <ul class="sub-menu">
                                            <?php foreach ($item['groupitems'] as $gitem) {
								?>
                                                <li>
                                                    
                                                    <a href="<?= Utils::getComponentUrl($gitem['link']) ?>">
                                                        
                                                        <i class="<?= ($gitem['icon'] != '') ? $gitem['icon'] : '' ?>"></i>&nbsp;&nbsp;
                                                        [$<?= str_replace(' ', '', $gitem['label']); ?>]
                                                    </a>
                                                </li>
                                            <?php
							} ?>
                                        </ul>
                                    </li>
                                    <?php
						} else {
							$subactive = ($controller.'/'.$action.$args_part == $item['link']) ? 'active' : ''; ?>

                                    <li class="<?= $subactive ?>">
                                        <a href="<?= Utils::getComponentUrl($item['link']) ?>">
                                            <i class="<?= ($item['icon'] != '') ? $item['icon'] : '' ?>"></i>&nbsp;&nbsp;
                                            [$<?= str_replace(' ', '', $item['label']); ?>]
                                        </a>
                                    </li>
                                <?php
						} ?>
                            <?php
					} ?>
                        </ul>
                    </li>
                    <?php
			} else {
				$active = ($controller.'/'.$action.$args_part == $menu['link']) ? 'single-active' : ''; ?>

                    <li class="<?= $active ?>">
                        <a href="<?= Utils::getComponentUrl($menu['link']) ?>">
                            <i class="<?= ($menu['icon'] != '') ? $menu['icon'] : 'fa fa-book' ?>"></i>
                            <span>[$<?= str_replace(' ', '', $menu['label']); ?>]</span>
                        </a>
                    </li>

                <?php
			} ?>
            <?php
		} ?>
            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->