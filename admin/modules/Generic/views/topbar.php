<?php
$loggeduser = UserAuth::getLoginSession();
$brand_info = UserAuth::getBrandInfoSession();
?>

<!-- begin #header -->
<div id="header" class="header navbar navbar-inverse navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle pull-left" data-click="sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle pull-right" data-click="right-sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <?php if (!is_null($brand_info) && $brand_info['logo'] != '') {
	$logo = '';
	if (@get_headers($logo)[0] != 'HTTP/1.1 404 Not Found') {
		?>
                    <a href="<?= Utils::getComponentUrl('Dashboard/index') ?>" class="navbar-brand">
                        <img src="<?= $logo ?>" alt="BRAND LOGO" width="120px"/>
                    </a>
                <?php
	}
} elseif (!is_null($brand_info)) {
	?>
                <a href="<?= Utils::getControllerUrl('Dashboard/index') ?>" class="navbar-brand">
                    <span class="navbar-logo"></span>
                    <?= $brand_info['name'] ?></a>
            <?php
} else {
		?>
                <a href="<?= Utils::getControllerUrl('Dashboard/index') ?>" class="navbar-brand">
                    <span class="navbar-logo"></span>
                    WeWeb</a>
            <?php
	} ?>


        </div>

        <ul class="nav navbar-nav navbar-right">
 
            <li>
                <a href="<?= 'http://'.$_SERVER['HTTP_HOST'] ?>">
                    <span ><i class="fa fa-eye"></i>&nbsp;&nbsp;[$view_site]</span>
                </a>
            </li>

            <li class="dropdown navbar-user">
                <?php
				$profile_img = ($loggeduser['image'] != '') ? Utils::genThumbnailUrl('users/'.$loggeduser['image'], 100, 100, array('zc' => 1)) : $this->template_path.'assets/img/img_profile.png';
				?>
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?= $profile_img ?>" alt=""/>
                    <span class="hidden-xs"><?= $loggeduser['firstname'] ?> <?= $loggeduser['lastname'] ?></span> <b
                        class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInLeft">
                    <li class="arrow"></li>
                    <li><a href="<?= Utils::getControllerUrl("Users/edit_profile/{$loggeduser['id']}") ?>"><i
                                class="fa fa-pencil-square-o" style="line-height: 18px"></i>&nbsp;&nbsp;&nbsp;[$EditProfile]</a>
                    </li>
                    <?php if ($loggeduser['super'] == 1) {
					?>
                        <li><a href="<?= Utils::getControllerUrl('Users/edit_profile') ?>">
                                <i class="fa fa-plus-square" style="line-height: 18px"></i>&nbsp;&nbsp;&nbsp;[$AddUser]</a>
                        </li>
                    <?php
				} ?>
                    <?php if (UserAuth::checkPreviousLogin()) {
					?>
                        <li><a href="<?= Utils::getControllerUrl('Users/previousLogin') ?>">
                                <i class="fa fa-caret-square-o-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;[$PrevousLogin]</a>
                        </li>
                    <?php
				} ?>
                    <li class="divider"></li>
                    <li><a href="<?= Utils::getControllerUrl('Users/logout') ?>"><i
                                class="fa fa-sign-out pull-right" style="line-height: 18px"></i>Log Out</a></li>
                </ul>
            </li>
            <li class="dropdown navbar-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <span>
                        <?= strtoupper(Utils::getLang()) ?>
                    </span> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInRight bg-grey">
                    <li class="arrow"></li>

                    <?php
					foreach (CMSSettings::$available_langs as $lang) {
						switch ($lang) {
							case 'it':
								$label = 'Italian';
								break;
							case 'en':
								$label = 'English';
								break;
							case 'al':
								$label = 'Shqip';
								break;
							default:
								$label = '';
								break;
						} ?>
                        <li>
                            <a href="<?= Utils::getControllerUrl('Dashboard/change_lang/'.$lang) ?>">
                                <img alt="<?= strtoupper($lang) ?>" style="margin:0px; width: 18px; height: 18px"
                                     src="/admin/templates/admin_t2/assets/img/langs/<?= $lang ?>.png">&nbsp;&nbsp;
                                <?= (($label != '') ? $label : strtoupper($lang)) ?>
                            </a>
                        </li>
                    <?php
					} ?>
                </ul>
            </li>


            <li class="divider hidden-xs"></li>
            <li class="hidden-xs">
                <a href="javascript:;" data-click="right-sidebar-toggled" class="f-s-14">
                    <i class="fa fa-th"></i>
                </a>
            </li>
        </ul>
        <!-- end header navigation right -->
    </div>
    <!-- end container-fluid -->
</div>
<!-- end #header -->