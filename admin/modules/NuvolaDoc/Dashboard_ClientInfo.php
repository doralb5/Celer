<?php

class Dashboard_ClientInfo extends BaseModule
{
	public function execute($parameters = array())
	{
		$ClientInfo = UserAuth::getLoggedClientInfo();
		$this->view->set('ClientInfo', $ClientInfo);

		$CurrentState = array();

		$categ_md = Loader::getModel('Doc_Categories', 'NuvolaDoc');
		$CurrentState['categories_nr'] = $categ_md->categoriesNumber();

		$docmodel_md = Loader::getModel('Doc_Models', 'NuvolaDoc');
		$CurrentState['models_nr'] = $docmodel_md->modelsNumber();

		$documents_md = Loader::getModel('Documents', 'NuvolaDoc');
		$CurrentState['files_size'] = $documents_md->filesTotalSize();

		$this->view->set('CurrentState', $CurrentState);
		return $this->view->render('client-info', true);
	}
}
