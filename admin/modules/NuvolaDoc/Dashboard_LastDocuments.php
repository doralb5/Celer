<?php

class Dashboard_LastDocuments extends BaseModule
{
	public function execute($parameters = array())
	{
		$model = Loader::getModel('Documents', 'NuvolaDoc');
		$documents = $model->getList(10, 0);

		$this->view->set('documents', $documents);

		$this->view->set('url_doc_files', Utils::getComponentUrl('NuvolaDoc/Documents/file_list'));
		$this->view->set('url_doc_delete', Utils::getComponentUrl('NuvolaDoc/Documents/document_delete'));
		return $this->view->render('last-documents', true);
	}
}
