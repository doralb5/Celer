<?php

class Dashboard_SearchDocuments extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('url_docs_list', Utils::getComponentUrl('NuvolaDoc/Documents/documents_list'));
		return $this->view->render('search-documents', true);
	}
}
