<div class="col-md-12">
    <section class="panel panel-info">
        <header class="panel-heading">
            <i class="fa fa-info-circle"></i>&nbsp;&nbsp;[$client_info]
        </header>

        <div class="panel-body">
            <div class="col-md-12">
                <ul class="list-group text-left">
                    <li class="list-group-item">
                        <label class="pull-right"><?= $ClientInfo['code'] ?></label>
                        <i class="fa fa-building"></i>&nbsp;&nbsp;[$ClientCode]
                    </li>
                    <li class="list-group-item">
                        <label class="pull-right"><?= $ClientInfo['profile_name'] ?></label>
                        <i class="fa fa-tasks"></i>&nbsp;&nbsp;[$Profile]
                    </li>
                    <div>&nbsp;</div>
                    <?php
					$c_res = ($CurrentState['categories_nr'] / $ClientInfo['categories_limit'] * 100);
					$state = ($c_res > 70 && $c_res < 85) ? 'progress-bar-warning' : (($c_res >= 85) ? 'progress-bar-danger' : '');
					?>
                    <li class="list-group-item">
                        <label class="pull-right"><?= $CurrentState['categories_nr'] ?> of <?= $ClientInfo['categories_limit'] ?></label>
                        <i class="fa fa-folder-open"></i>&nbsp;&nbsp;[$CategoriesLimit]
                    </li>
                    <div class="progress progress-striped active progress-xs">
                        <div class="progress-bar <?= $state ?>" role="progressbar" aria-valuenow="<?= $c_res ?>"
                             aria-valuemin="0" aria-valuemax="100" style="width:<?= $c_res ?>%">
<!--                            <b><?= $c_res ?> %</b>-->
                        </div>
                    </div>

                    <li class="list-group-item">
                        <label class="pull-right"><?= $CurrentState['models_nr'] ?> of <?= $ClientInfo['models_limit'] ?></label>
                        <i class="fa fa-modx"></i>&nbsp;&nbsp;[$ModelsLimit]
                    </li>
                    <?php
					$m_res = ($CurrentState['models_nr'] / $ClientInfo['models_limit'] * 100);
					$state = ($m_res > 70 && $m_res < 85) ? 'progress-bar-warning' : (($m_res >= 85) ? 'progress-bar-danger' : '');
					?>
                    <div class="progress progress-striped active progress-xs">
                        <div class="progress-bar <?= $state ?>" role="progressbar" aria-valuenow="<?= $m_res ?>"
                             aria-valuemin="0" aria-valuemax="100" style="width:<?= $m_res ?>%">
<!--                            <b><?= $m_res ?> %</b>-->
                        </div>
                    </div>
                    <?php
					$q_res = ($CurrentState['files_size'] / $ClientInfo['quota_limit']) * 100;
					$q_res = ($q_res > 100) ? 100 : $q_res;
					$state = ($q_res > 70 && $q_res < 85) ? 'progress-bar-warning' : (($q_res >= 85) ? 'progress-bar-danger' : '');
					?>

                    <li class="list-group-item">
                        <label class="pull-right"><?= Utils::formatSizeUnits($CurrentState['files_size'], 3) ?>&nbsp;&nbsp;of&nbsp;&nbsp;<?= Utils::formatSizeUnits($ClientInfo['quota_limit']) ?></label>
                        <i class="fa fa-cubes"></i>&nbsp;&nbsp;[$QuotaLimit]
                    </li>
                    <div class="progress progress-striped active progress-xs">
                        <div class="progress-bar <?= $state ?>" role="progressbar" aria-valuenow="<?= $q_res ?>"
                             aria-valuemin="0" aria-valuemax="100" style="width:<?= $q_res ?>%">
<!--                            <b><?= $q_res ?> %</b>-->
                        </div>
                    </div>

                    <?php
					//Sa dite kane mbetur nga skadenca
					$timestamp = strtotime($ClientInfo['expiration_date']) - time();
					$days = $timestamp / (24 * 60 * 60);
					?>
                    
                    <li class="list-group-item">
                        <label class="pull-right"><?= date('d/m/Y', strtotime($ClientInfo['activation_date'])) ?></label>
                        <i class="fa fa-calendar-check-o"></i>&nbsp;&nbsp;[$ActivationDate]
                    </li>
                    
                    <li class="list-group-item <?= ($days < 7) ? 'red-bg' : ''?> <?= ($days > 7 && $days < 30) ? 'orange-bg' : ''?>">
                        <label class="pull-right"><?= date('d/m/Y', strtotime($ClientInfo['expiration_date'])) ?></label>
                        <i class="fa fa-calendar-times-o"></i>&nbsp;&nbsp;[$ExpirationDate]
                    </li>

                    <li class="list-group-item">
                        <label class="pull-right"><?= $ClientInfo['brand_name'] ?></label>
                        <i class="fa fa-puzzle-piece "></i>&nbsp;&nbsp;[$Service]
                    </li>
                    <li class="list-group-item">
                        <label class="pull-right"><?= $ClientInfo['company_name'] ?></label>
                        <i class="fa fa-building-o"></i>&nbsp;&nbsp;[$Company]
                    </li>
                    <li class="list-group-item">
                        <label class="pull-right"><a href="mailto:<?= $ClientInfo['support_email'] ?>"><?= $ClientInfo['support_email'] ?></a></label>
                        <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;[$SupportEmail]
                    </li>
                    <li class="list-group-item">
                        <label class="pull-right"><a href="http://<?= $ClientInfo['support_url'] ?>/" target="_blank"><?= $ClientInfo['support_url'] ?></a></label>
                        <i class="fa fa-link"></i>&nbsp;&nbsp;[$SupportUrl]
                    </li>
                </ul>
            </div>
        </div>
    </section>
</div>