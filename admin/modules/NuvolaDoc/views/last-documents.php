<div class="col-md-12">
    <section class="panel panel-default">
        <header class="panel-heading">
            <i class="fa fa-file-text"></i>&nbsp;&nbsp;[$last_documents]
        </header>
        <div class="panel-body">
            <?php
			require_once(VIEWS_PATH.'TableList_class.php');

			$table = new TableListView('Documents');

			foreach ($documents as &$row) {
				$row->rows_buttons = array(
					new Button('<i class="fa fa-files-o"></i>', $url_doc_files.'/'.$row->id, 'xs', '', '[$Files]')
				);
			}

			$table->setElements($documents);

			$table->setFields(array(
				new TableList_Field('id', '[$Id]', 'int', 'left'),
				new TableList_Field('title', '[$Title]', 'string', 'left'),
				new TableList_Field('category_name', '[$Category]', 'string', 'left')
			));

			$table->setUrl_delete($url_doc_delete);
			$table->renderTopBar(false);
			$table->multipleDeletion(false);

			$table->render();
			?>
        </div>
    </section>
</div>