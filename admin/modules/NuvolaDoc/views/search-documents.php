<div class="col-md-12">
    <section class="panel panel-info">
        <header class="panel-heading">
            <i class="fa fa-files-o"></i>&nbsp;&nbsp;[$search_documents]
        </header>
        <div class="panel-body">
            <div class="col-md-12">
                <form class="form" action="<?= $url_docs_list ?>">
                    <div class="form-group">
                        <div class="input-group">
                            <input name="query" class="form-control input-lg" placeholder="[$Search]" type="text">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-info input-lg"><i class="fa fa-search"></i>&nbsp;&nbsp;[$Search]</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>