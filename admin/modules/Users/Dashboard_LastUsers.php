<?php

class Dashboard_LastUsers extends BaseModule
{
	public function execute($parameters = array())
	{
		$model = Loader::getModel('Users');
		$logged_user = UserAuth::getLoginSession();
		$User_Table = TABLE_PREFIX.'User';
		$filter = "{$User_Table}.deleted = '0' AND {$User_Table}.id != {$logged_user['id']} ";

		if ($logged_user['super'] == 0) {
			$filter .= "AND {$User_Table}.super = '0'";
		}

		$users = $model->getList(15, 0, $filter, 'creation_date DESC');

		$this->view->set('users', $users);
		$this->view->set('url_user_edit', Utils::getControllerUrl('Users/edit_profile'));
		$this->view->set('url_user_delete', Utils::getControllerUrl('Users/delete_user'));
		$this->view->set('url_user_enable', Utils::getControllerUrl('Users/enable_user'));
		$this->view->set('url_user_disable', Utils::getControllerUrl('Users/disable_user'));
		$this->view->set('url_user_details', Utils::getControllerUrl('Users/user_details'));
		return $this->view->render('last-users', true);
	}
}
