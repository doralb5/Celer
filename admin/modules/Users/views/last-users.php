<?php
require_once(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Users');

foreach ($users as &$row) {
	if ($row->enabled) {
		$btn1 = new Button('<i class="fa fa-square-o"></i>', $url_user_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button('<i class="fa fa-check-square-o"></i>', $url_user_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}

	$row->rows_buttons = array(
		$btn1,
		new Button('<i class="fa fa-pencil-square-o"></i>', $url_user_edit.'/'.$row->id, 'xs', '', '[$Edit]'),
		//new Button('<i class="fa fa-eye"></i>', $url_user_details . '/' . $row->id, 'xs', '', '[$Details]')
	);
}

$table->setElements($users);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('fullname', '[$Name]', 'string', 'left'),
));

$table->setUrl_delete($url_user_delete);
$table->renderTopBar(false);
$table->multipleDeletion(false);
$table->setTitle('[$LastUsers]');
$table->showResultInfo(false);
?>
<div class="row">
    <?php
	$table->render();
	?>
</div>
