<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
        <meta content="" name="description"/>
		<?= HeadHTML::printFavicon('/favicon.ico') ?>
		<?= HeadHTML::printTitle() ?>


        <link href="<?= WEBROOT.LIBS_PATH ?>bootstrap-colorpicker-master/dist/css/bootstrap-colorpicker.min.css"
              rel="stylesheet">
        <link rel="stylesheet" type="text/css"
              href="<?= $this->template_path; ?>assets/plugins/datetimepicker-master/jquery.datetimepicker.css"/>


        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="<?= $this->template_path; ?>assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css"
              rel="stylesheet"/>
        <link href="<?= $this->template_path; ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="<?= $this->template_path; ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="<?= $this->template_path; ?>assets/css/animate.min.css" rel="stylesheet"/>
        <link href="<?= $this->template_path; ?>assets/css/style.css" rel="stylesheet"/>
        <link href="<?= $this->template_path; ?>assets/css/style-responsive.min.css" rel="stylesheet"/>

		<?php
		$brand_info = UserAuth::getBrandInfoSession();
		?>

		<?php
		if (!is_null($brand_info)) {
			if ($brand_info['id'] == 3) {
				?>
				<link href="<?= $this->template_path; ?>assets/css/theme/bluehat.css" rel="stylesheet" id="theme"/>
			<?php
			} elseif ($brand_info['id'] == 1) {
				?>
				<link href="<?= $this->template_path; ?>assets/css/theme/orange.css" rel="stylesheet" id="theme"/>
			<?php
			} else {
				?>
				<link href="<?= $this->template_path; ?>assets/css/theme/black.css" rel="stylesheet" id="theme"/>
			<?php
			}
		}
		?>
        <!-- ================== END BASE CSS STYLE ================== -->

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="<?= $this->template_path; ?>assets/plugins/pace/pace.min.js"></script>
        <!-- ================== END BASE JS ================== -->

<?php HeadHTML::printStylesheetLinks(); ?>
        <script src="<?= $this->template_path; ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/plugins/datetimepicker-master/jquery.datetimepicker.js"></script>

        <script src="<?= WEBROOT.LIBS_PATH ?>bootstrap-colorpicker-master/dist/js/bootstrap-colorpicker.min.js"></script>
        <link rel="stylesheet" href="<?= WEBROOT.LIBS_PATH ?>codemirror-5.10/lib/codemirror.css">
        <link rel="stylesheet" href="<?= WEBROOT.LIBS_PATH ?>codemirror-5.10/theme/cobalt.css">
        <script src="<?= WEBROOT.LIBS_PATH ?>codemirror-5.10/lib/codemirror.js"></script>
        <script src="<?= WEBROOT.LIBS_PATH ?>codemirror-5.10/lib/util/formatting.js"></script>
        <script src="<?= WEBROOT.LIBS_PATH ?>codemirror-5.10/mode/css/css.js"></script>
        <script src="<?= WEBROOT.LIBS_PATH ?>codemirror-5.10/mode/javascript/javascript.js"></script>
        <script src="<?= WEBROOT.LIBS_PATH ?>codemirror-5.10/mode/php/php.js"></script>
        <script src="<?= WEBROOT.LIBS_PATH ?>codemirror-5.10/mode/cypher/cypher.js"></script>
        <script src="<?= WEBROOT.LIBS_PATH ?>codemirror-5.10/mode/htmlmixed/htmlmixed.js"></script>
        <script src="<?= WEBROOT.LIBS_PATH ?>codemirror-5.10/mode/xml/xml.js"></script>

<?php HeadHTML::printJSLinks(); ?>
    </head>
    <body class="pace-done flat-black" ng-app="MyCMS">
        <!-- begin #page-loader -->
        <div id="page-loader" class="fade in"><span class="spinner"></span></div>
        <!-- end #page-loader -->

        <!-- begin #page-container -->
        <div id="page-container"
             class="fade page-header-fixed page-sidebar-fixed page-with-two-sidebar page-right-sidebar-collapsed gradient-enabled">

			<?php
			//$module = Loader::loadModule('Generic/TopMenu');
			//$module->execute();
			//Sho klasen page-with-top-menu tek div me lart
			?>

			<?php
			$module = Loader::loadModule('Generic/TopBar');
			$module->execute();
			?>

			<?php
			$module = Loader::loadModule('Generic/SideBar');
			$module->execute();
			?>

			<?php
			$module = Loader::loadModule('Generic/RightSideBar');
			$module->execute();
			?>

            <!-- begin #content -->


            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->

				<?php
				$this->BreadCrumb->setCssClass('breadcrumb pull-right');
				$this->BreadCrumb->setHomepage('Home');
				$this->BreadCrumb->setRootIndexPage(Utils::getControllerUrl('Dashboard/index'));
				$this->BreadCrumb->render();
				?>

                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header"><?= $this->getTitle(); ?>
                    <small><?= $this->getSubTitle(); ?></small>
                </h1>
                <!-- end page-header -->

                <div class="row">
                    <div class="col-md-12" style="text-align: right;">
                        <div class="action_filters" style="display:inline-block;">
								<?php $current_url = WEBROOT.FCRequest::getUrl(); ?>
                            <form method='get' action='<?= $current_url ?>'>
								<?php
								foreach ($this->action_filters as $filter) {
									echo '<div style="float:left;">';
									$filter->render();
									echo '</div>';
									echo "\n";
								}
								?>
                            </form>
                        </div>
                        <div class="action_buttons" style="display:inline-block;">
							<?php
							foreach ($this->action_buttons as $button) {
								echo '<div style="float:left;">';
								$button->render();
								echo '</div>';
								echo "\n";
							}
							?>
                        </div>
                    </div>
                </div>

<?php Messages::showErrors(); ?>
				<?php Messages::showInfoMsg(); ?>


						<?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
								?>
					<div class="row">
						<div class="col-xs-12">
	<?php $this->ComponentErrors->renderHTML(); ?>
					<?php $this->ComponentMessages->renderHTML(); ?>
						</div>
					</div>
<?php
							} ?>


                <div class="row">

<?= $content; ?>

                </div>
            </div>


            <!-- end #content -->

            <!-- begin scroll to top btn -->
            <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                    class="fa fa-angle-up"></i></a>
            <!-- end scroll to top btn -->
        </div>
        <!-- end page container -->

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="<?= $this->template_path; ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <!--[if lt IE 9]>
        <script src="<?= $this->template_path; ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?= $this->template_path; ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/crossbrowserjs/excanvas.min.js"></script>
        <![endif]-->
        <script src="<?= $this->template_path; ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
        <!-- ================== END BASE JS ================== -->

        <!-- ================== BEGIN PAGE LEVEL JS ================== -->
        <script src="<?= $this->template_path; ?>assets/plugins/sparkline/jquery.sparkline.js"></script>
        <script src="<?= $this->template_path; ?>assets/plugins/jquery-knob/js/jquery.knob.js"></script>
        <script src="<?= $this->template_path; ?>assets/js/page-with-two-sidebar.demo.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/js/apps.min.js"></script>
        <!-- ================== END PAGE LEVEL JS ================== -->


        <script>
            $(document).ready(function () {
                App.init();
                PageWithTwoSidebar.init();
		<?php Messages::showAlerts(); ?>
            });
        </script>
<?php HeadHTML::printScripts(); ?>

    </body>
</html>
