<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="<?= $this->template_path; ?>assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
        <link href="<?= $this->template_path; ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?= $this->template_path; ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="<?= $this->template_path; ?>assets/css/animate.min.css" rel="stylesheet" />
        <link href="<?= $this->template_path; ?>assets/css/style.css" rel="stylesheet" />
        <link href="<?= $this->template_path; ?>assets/css/style-responsive.min.css" rel="stylesheet" />
        <link href="<?= $this->template_path; ?>assets/css/theme/orange.css" rel="stylesheet" id="theme" />
        <!-- ================== END BASE CSS STYLE ================== -->

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="<?= $this->template_path; ?>assets/plugins/pace/pace.min.js"></script>
        <!-- ================== END BASE JS ================== -->

		<?= HeadHTML::printFavicon('/favicon.ico') ?>
		<?= HeadHTML::printTitle() ?>
    </head>
    <body class="pace-top">

		<?= $content; ?>

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="<?= $this->template_path; ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <!--[if lt IE 9]>
                <script src="<?= $this->template_path; ?>assets/crossbrowserjs/html5shiv.js"></script>
                <script src="<?= $this->template_path; ?>assets/crossbrowserjs/respond.min.js"></script>
                <script src="<?= $this->template_path; ?>assets/crossbrowserjs/excanvas.min.js"></script>
        <![endif]-->
        <script src="<?= $this->template_path; ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
        <!-- ================== END BASE JS ================== -->

        <!-- ================== BEGIN PAGE LEVEL JS ================== -->
        <script src="<?= $this->template_path; ?>assets/js/login-v2.demo.min.js"></script>
        <script src="<?= $this->template_path; ?>assets/js/apps.min.js"></script>
        <!-- ================== END PAGE LEVEL JS ================== -->

        <script>
            $(document).ready(function () {
                App.init();
                LoginV2.init();
            });
        </script>
    </body>
</html><?php exit; ?>