<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Audits');

$table->setElements($logs);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('username', '[$User]', 'string', 'left'),
	new TableList_Field('date', '[$Date]', 'date', 'left'),
	new TableList_Field('object', '[$Object]', 'string', 'left'),
	new TableList_Field('action', '[$Action]', 'string', 'left'),
	new TableList_Field('description', '[$Description]', 'string', 'left'),
	new TableList_Field('ip', '[$IP]', 'string', 'left'),
));
$table->setUrl_action($url_logs_list);
$table->multipleDeletion(false);
$table->deleteOption(false);

$table->render();
