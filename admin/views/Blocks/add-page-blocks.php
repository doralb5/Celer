<?php
include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Blocks');

foreach ($blocks as &$row) {
	$row->rows_buttons = array(
		new Button("<i class='fa fa-pencil'></i>", $url_edit_block.'/'.$row->id, 'xs', 'btn-xs btn-info', '[$Edit]'),
	);
}

$table->setElements($blocks);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('position', '[$Position]', 'string', 'left'),
	new TableList_Field('module_name', '[$Module]', 'string', 'left'),
	new TableList_Field('sorting', '[$Sorting]', 'int', 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
));
$table->setUrl_action($url_search_block);
$table->setUrl_delete($url_delete_block);
$table->renderTopBar(false);
$table->multipleDeletion(false);

$table->render();
?>

<hr>

<div class="col-md-4">
    <h2 class="sub-header">Add Block</h2>
    <hr>
    <form method="post" action="">
        <div class="form-group">
            <label>Position</label>
            <select class="form-control" name="position">
				<?php foreach ($positions as $pos) {
	?>
					<option value="<?= $pos['name'] ?>"><?= $pos['label'] ?> ( <?= $pos['name'] ?> )</option>
				<?php
} ?>
            </select>
        </div>
        <div class="form-group">
            <label>Module</label>
            <select class="form-control" name="id_module">
				<?php foreach ($modules as $module) {
		?>
					<option
						value="<?= $module->id ?>" <?= ($block_module == $module->name) ? 'selected' : ''; ?>><?= (isset($module->settings['label'])) ? $module->settings['label'] : $module->name ?></option>
					<?php
	} ?>
            </select>
        </div>
        <div class="form-group">
            <label>Sorting</label>
            <input type="text" class="form-control" name="sorting" placeholder="Sorting" required="">
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="enabled"><strong>ENABLED</strong>
            </label>
        </div>
        <button type="submit" class="btn btn-info btn-lg pull-right" name="save">SAVE</button>
    </form>
</div>

