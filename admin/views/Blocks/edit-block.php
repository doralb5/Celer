<?php
$block_position = isset($_POST['position']) ? $_POST['position'] : $block->position;
$block_parameters = isset($_POST['parameters']) ? $_POST['parameters'] : $block->parameters;
$block_sorting = isset($_POST['sorting']) ? $_POST['sorting'] : $block->sorting;
$block_class = isset($_POST['block_class']) ? $_POST['block_class'] : $block->class;
?>

<hr>

<div class="col-md-4">
    <form method="post" action="">
        <div class="form-group">
            <label>Position</label>
            <select class="form-control" name="position">
				<?php foreach ($positions as $pos) {
	?>
					<option
						value="<?= $pos['name'] ?>" <?= ($pos['name'] == $block_position) ? 'selected' : '' ?>><?= $pos['label'] ?>
						( <?= $pos['name'] ?> )
					</option>
				<?php
} ?>
            </select>
        </div>
        <div class="form-group">
            <label>Sorting</label>
            <input type="text" class="form-control" name="sorting" placeholder="Sorting"
                   value="<?= $block_sorting ?>">
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="enabled" <?= ($block->enabled) ? 'checked' : ''; ?>><strong>ENABLED</strong>
            </label>
        </div>
        <button type="submit" class="btn btn-info pull-right" name="change">Change</button>
        <p>&nbsp</p>
    </form>
</div>


<div class="col-md-3 col-md-offset-3">
	<?php if (isset($module['params'])) {
		?>  

		<form method="post" action="">
			<h4>Design</h4>
			<div class="form-group">
				<label>Block Class</label>
				<input type="text" class="form-control" name="block_class" value="<?= $block_class ?>" />
			</div>

			<h4>Parameters</h4>
			<div class="form-group">
				<?php
				if (isset($module['params'])) {
					foreach ($module['params'] as $param) {
						?>
						<label><?= $param['label'] ?></label>

						<!--Number type-->
						<?php if ($param['type'] == 'number') {
							?>
							<input type="number" class="form-control" name="<?= $param['name'] ?>"
								   value="<?= (isset($module['values'][$param['name']])) ? $module['values'][$param['name']] : '' ?>">
							   <?php
						} ?>

						<!--TextBox-->
						<?php if ($param['type'] == 'text') {
							?>
							<input type="text" class="form-control" name="<?= $param['name'] ?>"
								   placeholder="<?= $param['label'] ?>"
								   value="<?= (isset($module['values'][$param['name']])) ? $module['values'][$param['name']] : '' ?>">
							   <?php
						} ?>

						<!--SQL List Type-->
						<?php if ($param['type'] == 'sqllist') {
							?>
							<select class="form-control" name="<?= $param['name'] ?>">
								<!--Static Options-->
								<?php foreach ($param['options'] as $option) {
								?>
									<option
										value="<?= $option['value'] ?>" <?= (isset($module['values'][$param['name']])) ? (($module['values'][$param['name']] == $option['value']) ? 'selected' : '') : '' ?>><?= $option['label'] ?></option>
									<?php
							} ?>
								<!--SQL Options-->
								<?php foreach ($param['source']['elements'] as $option) {
								?>
									<option
										value="<?= $option[$param['source']['value']] ?>" <?= (isset($module['values'][$param['name']])) ? (($module['values'][$param['name']] == $option[$param['source']['value']]) ? 'selected' : '') : '' ?>><?= $option[$param['source']['label']] ?></option>
									<?php
							} ?>
							</select>

						<?php
						} ?>


						<?php if ($param['type'] == 'radio') {
							?>
							<?php foreach ($param['options'] as $option) {
								?>
								<div class="radio">
									<label>
										<input type="radio" name="<?= $param['name'] ?>" id="optionsRadios1"
											   value="<?= $option['value'] ?>" <?= (isset($module['values'][$param['name']])) ? (($module['values'][$param['name']] == $option['value']) ? 'checked' : '') : '' ?>>
											   <?= $option['label'] ?>
									</label>
								</div>
							<?php
							} ?>
						<?php
						} ?>

						<?php if ($param['type'] == 'select') {
							?>
							<select class="form-control" name="<?= $param['name'] ?>">
								<?php foreach ($param['options'] as $option) {
								?>
									<option
										value="<?= $option['value'] ?>" <?= (isset($module['values'][$param['name']])) ? (($module['values'][$param['name']] == $option['value']) ? 'selected' : '') : '' ?>><?= $option['label'] ?></option>
									<?php
							} ?>
							</select>
						<?php
						} ?>

						<!--                    tipe te tjerash inputesh-->

						<?php
					}
				} ?>
			</div>
			<button type="submit" class="btn btn-info pull-right" name="save_params">Save</button>
		</form>
	<?php
	} ?>
</div>
