<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->

	<?php
	$this->BreadCrumb->setCssClass('breadcrumb pull-right');
	$this->BreadCrumb->setHomepage('Home');
	$this->BreadCrumb->setRootIndexPage(Utils::getControllerUrl('Dashboard/index'));
	$this->BreadCrumb->render();
	?>

    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header"><?= $this->getTitle(); ?>
        <small><?= $this->getSubTitle(); ?></small>
    </h1>
    <!-- end page-header -->

    <div class="row">
        <div class="col-md-12" style="text-align: right;">
            <div class="action_filters" style="display:inline-block;">
				<?php $current_url = WEBROOT.FCRequest::getUrl(); ?>
                <form method='get' action='<?= $current_url ?>'>
					<?php
					foreach ($this->action_filters as $filter) {
						echo '<div style="float:left;">';
						$filter->render();
						echo '</div>';
						echo "\n";
					}
					?>
                </form>
            </div>
            <div class="action_buttons" style="display:inline-block;">
				<?php
				foreach ($this->action_buttons as $button) {
					echo '<div style="float:left;">';
					$button->render();
					echo '</div>';
					echo "\n";
				}
				?>
            </div>
        </div>
    </div>

	<?php Messages::showErrors(); ?>
	<?php Messages::showInfoMsg(); ?>


	<?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
					?>
		<div class="row">
			<div class="col-xs-12">
				<?php $this->ComponentErrors->renderHTML(); ?>
				<?php $this->ComponentMessages->renderHTML(); ?>
			</div>
		</div>
	<?php
				} ?>


    <div class="row">

		<?= $content; ?>

    </div>
</div>