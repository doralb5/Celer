<style>
    .component-settings .action {
        width: 110px;
        vertical-align: middle;
    }

    .component-settings .inner-settings .action {
        width: 30px;
    }

    .component-settings .inner-settings {
        margin-bottom: 0px;
    }
</style>
<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$EditComponent]</h4>
        </div>
        <div class="panel-body">
            <form role="form" method="post">
                <div class="row">
                    <div class="form-group col-md-5">
                        <label>[$featured]</label>

                        <div class="input-group">
                            <select name="featured" class="form-control">
                                <option value="1" <?= ($component->featured) ? 'selected' : '' ?>>[$yes_option]
                                </option>
                                <option value="0" <?= (!$component->featured) ? 'selected' : '' ?>>[$no_option]
                                </option>
                            </select>
                            <span class="input-group-btn">
                                <button type="submit" name="change" class="btn btn-info"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-2">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$ComponentSettings]</h4>
        </div>
        <div class="panel-body bg-cover">


			<?php
			$current_settings = $component->settings;
			$current_settings = json_decode($current_settings, true);
			?>
			<?php if (isset($settings) && count($settings)) {
				?>

				<div class="col-md-12">
					<form method="post" class="form-horizontal form-bordered" data-parsley-validate="true"
						  name="demo-form" novalidate="">

						<?php foreach ($settings as $sett) {
					?>
							<?php if ($sett['type'] == 'text') {
						?>
								<div class="form-group col-md-6">
									<label class="col-md-3 control-label"><?= $sett['label'] ?></label>

									<div class="col-md-9">
										<input type="text" class="form-control" name="<?= $sett['name'] ?>"
											   placeholder="<?= $sett['description'] ?>"
											   value="<?= (isset($current_settings[$sett['name']])) ? $current_settings[$sett['name']] : '' ?>">
									</div>
								</div>
							<?php
					} ?>
							<?php if ($sett['type'] == 'number') {
						?>
								<div class="form-group col-md-6">
									<label class="col-md-3 control-label"><?= $sett['label'] ?></label>

									<div class="col-md-9">
										<input type="number" class="form-control" name="<?= $sett['name'] ?>"
											   placeholder="<?= $sett['description'] ?>"
											   value="<?= (isset($current_settings[$sett['name']])) ? $current_settings[$sett['name']] : '' ?>">
									</div>
								</div>
							<?php
					} ?>

							<!--SQL List Type-->
							<?php if ($sett['type'] == 'sqllist') {
						?>
								<div class="form-group col-md-6">
									<label class="col-md-3 control-label"><?= $sett['label'] ?></label>

									<div class="col-md-9">
										<select class="form-control" name="<?= $sett['name'] ?>">
											<!--Static Options-->
											<?php foreach ($sett['fields'] as $option) {
							?>
												<option
													value="<?= $option['value'] ?>" <?= (isset($current_settings[$sett['name']])) ? (($current_settings[$sett['name']] == $option['value']) ? 'selected' : '') : '' ?>><?= $option['label'] ?></option>
												<?php
						} ?>
											<!--SQL Options-->
											<?php foreach ($sett['source']['elements'] as $option) {
							?>
												<option
													value="<?= $option[$sett['source']['value']] ?>" <?= (isset($current_settings[$sett['name']])) ? (($current_settings[$sett['name']] == $option[$sett['source']['value']]) ? 'selected' : '') : '' ?>><?= $option[$sett['source']['label']] ?></option>
												<?php
						} ?>
										</select>
									</div>
								</div>

							<?php
					} ?>

							<?php if ($sett['type'] == 'radio') {
						?>
								<div class="form-group col-md-6">
									<label class="col-md-3 control-label"><?= $sett['label'] ?></label>

									<div class="col-md-9">
										<?php foreach ($sett['fields'] as $option) {
							?>
											<div class="radio">
												<label>
													<input type="radio" name="<?= $sett['name'] ?>"
														   value="<?= $option['value'] ?>" <?= (isset($current_settings[$sett['name']])) ? (($current_settings[$sett['name']] == $option['value']) ? 'checked' : '') : '' ?>>
														   <?= $option['label'] ?>
												</label>
											</div>
										<?php
						} ?>
									</div>
								</div>
							<?php
					} ?>
							<?php if ($sett['type'] == 'checkbox') {
						?>
								<div class="form-group col-md-6">
									<label class="col-md-3 control-label"><?= $sett['label'] ?></label>

									<div class="col-md-9">
										<?php foreach ($sett['fields'] as $option) {
							?>
											<div class="checkbox">
												<label>
													<input type="checkbox" name="<?= $sett['name'] ?>[]"
														   value="<?= $option['value'] ?>" <?= (isset($current_settings[$sett['name']])) ? (($current_settings[$sett['name']] == $option['value']) ? 'checked' : '') : '' ?>>
														   <?= $option['label'] ?>
												</label>
											</div>
										<?php
						} ?>
									</div>
								</div>
							<?php
					} ?>

							<?php if ($sett['type'] == 'select') {
						?>
								<div class="form-group col-md-6">
									<label class="col-md-3 control-label"><?= $sett['label'] ?></label>

									<div class="col-md-9">
										<select class="form-control" name="<?= $sett['name'] ?>">
											<?php foreach ($sett['fields'] as $option) {
							?>
												<option
													value="<?= $option['value'] ?>" <?= (isset($current_settings[$sett['name']])) ? (($current_settings[$sett['name']] == $option['value']) ? 'selected' : '') : '' ?>><?= $option['label'] ?></option>
												<?php
						} ?>
										</select>
									</div>
								</div>
							<?php
					} ?>
							<!--  tipe te tjerash inputesh-->
						<?php
				} ?>
						<div class="clearfix"></div>
						<div class="col-md-12 text-center">
							<p>&nbsp;</p>
							<button type="submit" class="btn btn-info" name="save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
							</button>
						</div>
					</form>
				</div>
			<?php
			} else {
				?>
				<div class="alert alert-warning fade in">
					<button data-dismiss="alert" class="close close-sm" type="button">
						<i class="icon_error-circle"></i>
					</button>
					There are no settings !
				</div>
			<?php
			} ?>


        </div>
    </div>
</div>
