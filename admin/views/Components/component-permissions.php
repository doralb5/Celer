<?php
include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Users');

$table->setElements($users_permitted);

$table->setTotalElements(count($users_permitted));
$table->setElements_per_page(100);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('email', '[$Email]', 'string', 'left'),
));
$table->setUrl_delete($url_permission_delete);
$table->setUrl_delete_params(array('id_comp' => $component->id));
$table->multipleDeletion(false);
$table->renderTopBar(false);
$table->setTitle('[$ComponentPermissions]');
$table->render();
?>


<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-6" data-init="true">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$AddPermission]</h4>
        </div>
        <div class="panel-body">

            <div class="form">
                <form role="form" method="post">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>[$unpermitted]</label>
                                <select id="unpermitted" class="form-control" name="upuser_id[]" multiple="multiple"
                                        size="6">
											<?php foreach ($users_unpermitted as $user) {
	?>
										<option
											value="<?= $user->id ?>"><?= $user->firstname.' '.$user->lastname ?></option>
										<?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <br/><br/>
                            <p class="text-center">
                            <button disabled="" id="add_btn" type="submit" name="add" class="btn btn-info"><i
                                    class="fa fa-angle-double-right"></i></button>
                            </p>
                            <p class="text-center">
                            <button disabled="" id="remove_btn" type="submit" name="remove"
                                    class="btn btn-info"><i class="fa fa-angle-double-left"></i></button>
                            </p>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>[$permitted]</label>
                                <select id="permitted" class="form-control" name="puser_id[]" multiple="multiple"
                                        size="6">
											<?php foreach ($users_permitted as $user) {
		?>
										<option value="<?= $user->id_user ?>"><?= $user->name ?></option>
									<?php
	} ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<script>
<?php
$code = "$(document).ready(function () {
        $('#permitted').change(function () {
            $('#remove_btn').removeAttr('disabled');
            $('#add_btn').prop('disabled', true);
            ;
            $('#unpermitted').val([]);
        });

        $('#unpermitted').change(function () {
            $('#add_btn').removeAttr('disabled');
            $('#remove_btn').prop('disabled', true);
            ;
            $('#permitted').val([]);
        });
    });";
HeadHTML::addScript($code);
?>
</script>