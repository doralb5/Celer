<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Components');

foreach ($components as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_component_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_component_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button("<i class='fa fa-wrench'></i>", $url_component_manage.'/'.$row->id, 'xs', 'btn-xs btn-default', '[$Manage]'),
		new Button('Permissions', $url_component_permissions.'/'.$row->id, 'xs', 'btn-xs btn-info'),
	);
}

$table->setElements($components);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
	new TableList_Field('sorting', '[$Sorting]', 'int', 'left'),
));
$table->setUrl_action($url_component_search);
$table->setUrl_delete($url_component_delete);
$table->multipleDeletion(false);
$table->render();
