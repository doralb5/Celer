<div class="col-md-12 ui-sortable">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$NewComponent]</h4>
        </div>
        <div class="panel-body">
            <div class="form">
                <form role="form" method="post">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>[$type]</label>
                            <select name="type" class="form-control">
                                <option value="0">[$Component]</option>
                                <option value="1">[$Controller]</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>[$featured]</label>
                            <select name="featured" class="form-control">
                                <option value="1">[$yes_option]</option>
                                <option value="0">[$no_option]</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>[$sorting]</label>
                            <input type="text" name="sorting" class="form-control" placeholder="[$sorting]">
                        </div>
                        <div class="form-group col-md-3">
                            <label>[$package]</label>
                            <input type="text" name="package" class="form-control" placeholder="[$package]">
                        </div>
                        <div class="form-group col-md-4 col-md-offset-4">
                            <label for="inputTitle">[$name]<span class="required">*</span></label>
                            <div class="input-group">
                                <input type="text" name="name" class="form-control" id="inputTitle"
                                       placeholder="[$name]" required="">
                                <span class="input-group-btn">
                                    <button type="submit" name="install" class="btn btn-info"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$Install]</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
