<hr>
<div class="container-fluid text-center">
    <div class="row">
        <div class="col-md-6">
			<?php
			if (count($widgets)) {
				for ($i = 0; $i < count($widgets); $i = $i + 2) {
					echo $widgets[$i];
				}
			}
			?>
        </div>
        <div class="col-md-6">
			<?php
			if (count($widgets)) {
				for ($i = 1; $i < count($widgets); $i = $i + 2) {
					echo $widgets[$i];
				}
			}
			?>
        </div>
    </div>
</div>
<style>
    .dashboard_logo {
        box-shadow: 2px 2px 1px lightgray;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
    }
</style>