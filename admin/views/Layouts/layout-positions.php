<hr>
<?php if (count($positions) > 0) {
	?>
	<div class="col-md-12">
		<?php foreach ($positions as $pos) {
		?>

			<form method="post" id="form-one">
				<div class="panel panel-primary">
					<div class="panel-heading text-right">
						<div class="pull-left">
							<span class="text-uppercase"><?= $pos['label'] ?> ( <?= $pos['name'] ?> )</span>
						</div>
						<p>&nbsp;</p>
						<input type="hidden" name="hidden_position" value="<?= $pos['name'] ?>">
					</div>
					<div class="panel-body">

						<?php foreach ($blocks[$pos['name']] as $block) {
			?>
							<div class="panel <?= ($block->always == '1') ? 'panel-success' : 'panel-info' ?>">
								<div class="panel-heading text-left">
									<table class="table table-striped layout_positions">
										<tbody>
											<tr>
											<td class="id"><?= $block->id ?></td>
											<td class="module"><?= $block->module_name ?></td>
											<?php if ($block->always == '1') {
				?>
												<td class="always">
													<strong><?= $block->always == '1' ? 'Always' : '' ?></strong></td><?php
			} ?>
											<td class="pages">
												<?php
												if (isset($block->pages)) {
													$k = 0;
													foreach ($block['pages'] as $page_block) {
														?>
														<a class="btn btn-toolbar btn-xs" data-toggle="modal"
														   href="#myModal<?= $i.'_'.$j.'_'.$k ?>"><?= $page_block['alias'] ?>
															<i class="fa fa-times"></i></a>
														<?php
														$k++;
													}
												} ?>

											</td>
											<td class="params"><?= $block->parameters ?></td>
											<td class="sorting"><?= $block->sorting ?></td>
											<td class="action">
												<a class="btn btn-xs btn-info"
												   href='<?= Utils::getControllerUrl('Layouts/edit_block/'.$block->id."?layout=$LayoutName") ?>'><i
														class="fa fa-pencil-square-o"></i></a>&nbsp
											</td>
											<td class="action">
												<a title="[$Delete]" class="btn btn-warning btn-xs deleteRow"
												   data-toggle="modal" data-id="<?= $block->id ?>"
												   href="#myModal_<?= $LayoutName ?>"><i class="fa fa-times"></i></a>
											</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

						<?php
		} ?>
					</div>
				</div>
			</form>

		<?php
	} ?>
	<?php
} else {
		?>
		<div class="alert alert-warning fade in">
			<button data-dismiss="alert" class="close close-sm" type="button">
				<i class="icon_error-circle"></i>
			</button>
			There are not positions !
		</div>
	<?php
	} ?>
</div>
<div class="col-md-4" id="addblock">
    <h2 class="sub-header">Add Block</h2>
    <hr>
    <form method="post" action="">
        <div class="form-group">
            <label>Position</label>
            <select class="form-control" name="position">
				<?php foreach ($positions as $pos) {
		?>
					<option value="<?= $pos['name'] ?>"><?= $pos['label'] ?> ( <?= $pos['name'] ?> )</option>
				<?php
	} ?>
            </select>
        </div>
        <div class="form-group">
            <label>Module</label>
            <select class="form-control" name="id_module">
				<?php foreach ($modules as $module) {
		?>
					<option
						value="<?= $module->id ?>" <?= ($block_module == $module->name) ? 'selected' : ''; ?>><?= (isset($module->settings['label'])) ? $module->settings['label'] : $module->name ?></option>
					<?php
	} ?>
            </select>
        </div>
        <div class="form-group">
            <label>Page</label>
            <select class="form-control" name="id_page[]" multiple="multiple" required="" style="height: 150px">
                <option value="0">All Pages</option>
				<?php foreach ($pages as $page) {
		?>
					<option
						value="<?= $page->id ?>"><?= $page->name ?> <?= ($page->component_name != '') ? ' - '.$page->component_name.'/'.$page->action : '' ?></option>
					<?php
	} ?>
            </select>
        </div>
        <div class="form-group">
            <label>Sorting</label>
            <input type="text" class="form-control" name="sorting" placeholder="Sorting" required="">
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="enabled"><strong>ENABLED</strong>
            </label>
        </div>
        <button type="submit" class="btn btn-info pull-right" name="save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]</button>
    </form>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal_<?= $LayoutName ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">[$delete_record]<strong></strong></h4>
            </div>
            <div class="modal-body">
                [$delete_record_question]
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-danger deleteButton" type="submit"> [$Delete]</a>
                <button data-dismiss="modal" class="btn btn-default" type="button">[$Close]</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->

<script>

    $(document).ready(function () {

        var modal = $('#myModal_<?= $LayoutName ?>');
        var modal_title = $('#myModal_<?= $LayoutName ?> .modal-title');
        var modal_delete_btn = $('#myModal_<?= $LayoutName ?> .deleteButton');

        $('.deleteRow').click(function () {

            var id = $(this).data('id');


            modal_title.html("[$delete_record] " + id);
            modal_delete_btn.attr('href', "<?= $url_block_delete ?>" + id + "?layout=<?= $LayoutName ?>");
        });

    });

</script>