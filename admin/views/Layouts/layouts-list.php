<hr>
<?php if (count($layouts) > 0) {
	?>
	<?php
	$i = 0;
	foreach ($layouts as $layout) {
		?>
		<form method="post">
			<div class="panel panel-info">
				<div class="panel-heading text-right">
					<div class="pull-left">
						<span class="text-uppercase"><?= $layout ?></span>
					</div>
					<input type="hidden" name="managed_item" value="<?= $layout ?>">

					<button type="submit" name="manage" class="btn btn-xs btn-primary">MANAGE</button>
				</div>
				<div class="panel-body">
					<!--Layout Description-->
				</div>
			</div>
		</form>
		<?php
		$i++;
	} ?>
<?php
} else {
		?>
	<div class="alert alert-warning fade in">
		<button data-dismiss="alert" class="close close-sm" type="button">
			<i class="icon_error-circle"></i>
		</button>
		There are not layouts !
	</div>
<?php
	} ?>