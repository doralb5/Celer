<div class="col-md-12">
    <section class="panel">
        <header class="panel-heading">
            New Module
        </header>
        <div class="panel-body">
            <div class="form">
                <form role="form" method="post">
                    <div class="row">

                        <div class="form-group col-md-3">
                            <label>[$Package]</label>
                            <input type="text" name="package" class="form-control" placeholder="[$package]">
                        </div>
                        <div class="form-group col-md-5">
                            <label for="inputTitle">[$Name]<span class="required">*</span></label>
                            <div class="input-group">
                                <input type="text" name="name" class="form-control" id="inputTitle"
                                       placeholder="Name" required="">
                                <span class="input-group-btn">
                                    <button type="submit" name="install" class="btn btn-info"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$Install]</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
