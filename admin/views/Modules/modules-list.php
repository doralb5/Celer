<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Modules');

foreach ($modules as &$row) {
	if ($row->enabled) {
		$btn1 = new Button('<i class="fa fa-square-o"></i>', $url_module_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button('<i class="fa fa-check-square-o"></i>', $url_module_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1
	);
}

$table->setElements($modules);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
));
$table->setUrl_action($url_modules_list);
$table->setUrl_delete($url_module_delete);

$table->render();
