<?php
$pg_name = isset($_POST['name']) ? $_POST['name'] : $page->name;
$pg_layout = isset($_POST['layout']) ? $_POST['layout'] : $page->layout;
$pg_idcomponent = isset($_POST['id_component']) ? $_POST['id_component'] : $page->id_component;
$pg_action = isset($_POST['action']) ? $_POST['action'] : $page->action;
$pg_action_params = isset($_POST['action_params']) ? $_POST['action_params'] : $page->action_params;
$pg_enabled = isset($_POST['enabled']) ? $_POST['enabled'] : $page->enabled;
$pg_home = isset($_POST['home']) ? $_POST['home'] : $page->home;

$robots = explode(',', $page->robots);
if (count($robots) == 2) {
	$rindex = 1;
	$rfollow = 1;
} elseif (count($robots) == 1) {
	$rindex = ($robots[0] == 'INDEX') ? 1 : 0;
	$rfollow = ($robots[0] == 'FOLLOW') ? 1 : 0;
} else {
	$rindex = 0;
	$rfollow = 0;
}
$pg_robots_index = isset($_POST['robots_index']) ? $_POST['robots_index'] : $rindex;
$pg_robots_follow = isset($_POST['robots_follow']) ? $_POST['robots_follow'] : $rfollow;

if (!is_null($page->parameters)) {
	$page_parameters = json_decode($page->parameters, true);
}

foreach (CMSSettings::$available_langs as $lang) {
	$alias_{$lang} = isset($_POST['alias_'.$lang]) ? $_POST['alias_'.$lang] : ((!is_null($page->{$lang})) ? $page->{$lang}->alias : '');
	$title_{$lang} = isset($_POST['title_'.$lang]) ? $_POST['title_'.$lang] : ((!is_null($page->{$lang})) ? $page->{$lang}->title : '');
	$meta_description_{$lang} = isset($_POST['meta_description_'.$lang]) ? $_POST['meta_description_'.$lang] : ((!is_null($page->{$lang})) ? $page->{$lang}->meta_description : '');
	$meta_keywords_{$lang} = isset($_POST['meta_keywords_'.$lang]) ? $_POST['meta_keywords_'.$lang] : ((!is_null($page->{$lang})) ? $page->{$lang}->meta_keywords : '');
}
?>

<script>
    function refreshActionList() {
        console.log("<?= Utils::getControllerUrl('Components/ajx_getActions') ?>/" + $('#component').val());
        $.ajax({
            url: "<?= Utils::getControllerUrl('Components/ajx_getActions') ?>/" + $('#component').val(),
            dataType: "json",
            success: function (data) {
                console.log(data);
                var options, index, select, option;
                // Get the raw DOM object for the select box
                select = document.getElementById('actionsList');
                // Clear the old options
                select.options.length = 0;
                // Load the new options
                options = data.options; // Or whatever source information you're working with
                for (index = 0; index < options.length; ++index) {
                    option = options[index];
                    select.options.add(new Option(option.text, option.value));
                }
                $('#actionsList').val('<?= $pg_action ?>');
            },
            error: function (e) {
                select = document.getElementById('actionsList');
                // Clear the old options
                select.options.length = 0;
                select.options.add(new Option('', ''));
                console.log('Error: ' + e);
            }
        });
    }
    $(document).ready(function () {

        if ($('#component').val() !== '') {
            refreshActionList();
        }
    });

    function generatedAlias(id_src, id_dest) {
        $('#' + id_dest).val(replaceAll($('#' + id_src).val(), " ", "-").toLowerCase());
    }

    function replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }
</script>
<hr>
<form method="post" action="">
    <div class="col-md-6">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$GeneralInformation]</h4>
            </div>
            <div class="panel-body panel-form" style="padding:15px !important">
                <div class="form-group">
                    <label>Page Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Page name"
                           value="<?= $pg_name ?>">
                </div>
                <div class="form-group">
                    <label>Layout</label>
                    <select class="form-control" name="layout">
						<?php foreach ($layouts as $layout) {
	?>
							<option
								value="<?= $layout ?>" <?= ($layout == $pg_layout) ? 'selected' : '' ?>><?= $layout ?></option>
							<?php
} ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Component</label>
                    <select class="form-control" id="component" name="id_component"
                            onchange="refreshActionList()">
                        <option></option>
						<?php foreach ($components as $component) {
		?>
							<option
								value="<?= $component->id ?>" <?= ($component->id == $pg_idcomponent) ? 'selected' : '' ?>><?= $component->name ?></option>
							<?php
	} ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Action</label>
                    <select id="actionsList" class="form-control" name="action">
                        <option value="">--</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Action Parameters</label>
                    <input type="text" class="form-control" name="action_params"
                           placeholder="Action Parameters" value="<?= $pg_action_params ?>">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="home" <?= ($pg_home) ? 'checked' : '' ?>> Home Page
                    </label>
                </div>

                <label>Robots</label>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="FOLLOW"
                               name="robots_follow" <?= ($pg_robots_follow) ? 'checked' : '' ?>>FOLLOW
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="INDEX"
                               name="robots_index" <?= ($pg_robots_index) ? 'checked' : '' ?>>INDEX
                    </label>
                </div>
                <hr>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="1"
                               name="enabled" <?= ($pg_enabled) ? 'checked' : '' ?>><strong>ENABLED</strong>
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                       data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                       data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">[$Contents]</h4>
            </div>
            <div class="panel-body panel-form" style="padding:15px !important">


                <ul class="nav nav-tabs">
					<?php foreach (CMSSettings::$available_langs as $lang) {
		?>
						<li class="<?= ($lang == CMSSettings::$default_lang) ? 'active' : '' ?>"><a
								href="#tab_<?= $lang ?>" data-toggle="tab"><?= strtoupper($lang) ?></a></li>
						<?php
	} ?>
                </ul>
                <div class="tab-content">

					<?php foreach (CMSSettings::$available_langs as $lang) {
		?>

						<div
							class="tab-pane fade <?= ($lang == CMSSettings::$default_lang) ? 'active in' : '' ?>"
							id="tab_<?= $lang ?>">


							<div class="form-group">
								<label>Title</label>
								<input onkeyup="generatedAlias('title_<?= $lang ?>', 'alias_<?= $lang ?>')"
									   type="text"
									   class="form-control" id="title_<?= $lang ?>"
									   name="title_<?= $lang ?>" placeholder="Title" value="<?= $title_{$lang} ?>">
							</div>
							<div class="form-group">
								<label>Alias</label>
								<input type="text" class="form-control" id="alias_<?= $lang ?>"
									   name="alias_<?= $lang ?>" placeholder="Alias"
									   value="<?= $alias_{$lang} ?>">
							</div>
							<br>
							<br>
							<div class="form-group">
								<label>Meta Description</label>
								<textarea type="text" class="form-control" name="meta_description_<?= $lang ?>"
										  placeholder="Meta Description" rows="4"
										  style="max-width: 100%"><?= $meta_description_{$lang} ?></textarea>
							</div>
							<div class="form-group">
								<label>Meta Keywords</label>
								<textarea type="text" class="form-control" name="meta_keywords_<?= $lang ?>"
										  placeholder="Meta Keywords" rows="4"
										  style="max-width: 100%"><?= $meta_keywords_{$lang} ?></textarea>
							</div>


						</div>
					<?php
	} ?>

                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <p class="text-center">
        <button type="submit" name="save" class="btn btn-info">
            <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
        </button>
        </p>
    </div>
    <p>&nbsp;</p>
</form>

<?php if (!is_null($page->id)) {
		?>
	<?php if (isset($controllerParameters) && count($controllerParameters)) {
			?>
		<div class="col-md-6">
			<div class="panel panel-inverse" data-sortable-id="ui-widget-1" data-init="true">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
						   data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
						   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
						   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
						   data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">[$GeneralInformation]</h4>
				</div>
				<div class="panel-body panel-form" style="padding:15px !important">
					<form method="post">
						<div
							style="border: 1px solid lightgray; padding: 15px; border-radius: 2px; box-shadow: 0px 0px 1px lightgray">
							<h4>Page Parameters</h4>
							<hr>
							<div class="form-group">
								<?php
								foreach ($controllerParameters as $field) {
									?>
									<?php if ($field['type'] == 'text') {
										?>
										<label><?= $field['label'] ?></label>
										<input type="text" class="form-control" name="<?= $field['name'] ?>"
											   placeholder="<?= $field['description'] ?>"
											   value="<?= (isset($page_parameters[$field['name']])) ? $page_parameters[$field['name']] : '' ?>">
										   <?php
									} ?>

									<?php if ($field['type'] == 'number') {
										?>
										<label><?= $field['label'] ?></label>
										<input type="number" class="form-control"
											   name="<?= $field['name'] ?>"
											   placeholder="<?= $field['description'] ?>"
											   value="<?= (isset($page_parameters[$field['name']])) ? $page_parameters[$field['name']] : '' ?>">
										   <?php
									} ?>


									<?php if ($field['type'] == 'radio') {
										?>
										<label><?= $field['label'] ?></label>
										<?php foreach ($field['fields'] as $option) {
											?>
											<div class="radio">
												<label>
													<input type="radio" name="<?= $field['name'] ?>"
														   id="optionsRadios1"
														   value="<?= $option['value'] ?>" <?= (isset($page_parameters[$field['name']]) && ($option['value'] == $page_parameters[$field['name']])) ? 'checked' : '' ?>>
														   <?= $option['label'] ?>
												</label>
											</div>
										<?php
										} ?>
									<?php
									} ?>


									<?php if ($field['type'] == 'select') {
										?>
										<label><?= $field['label'] ?></label>
										<select class="form-control" name="<?= $field['name'] ?>">
											<?php foreach ($field['fields'] as $option) {
											?>
												<option
													value="<?= $option['value'] ?>" <?= (isset($page_parameters[$field['name']]) && ($option['value'] == $page_parameters[$field['name']])) ? 'selected' : '' ?>><?= $option['label'] ?></option>
												<?php
										} ?>
										</select>
									<?php
									} ?>


									<!--SQL List Type-->
									<?php if ($field['type'] == 'sqllist') {
										?>
										<label><?= $field['label'] ?></label>
										<select class="form-control" name="<?= $field['name'] ?>">
											<!--Static Options-->
											<?php foreach ($field['fields'] as $option) {
											?>
												<option
													value="<?= $option['value'] ?>" <?= (isset($page_parameters[$field['name']])) ? (($page_parameters[$field['name']] == $option['value']) ? 'selected' : '') : '' ?>><?= $option['label'] ?></option>
												<?php
										} ?>
											<!--SQL Options-->
											<?php foreach ($field['source']['elements'] as $option) {
											?>
												<option
													value="<?= $option[$field['source']['value']] ?>" <?= (isset($page_parameters[$field['name']])) ? (($page_parameters[$field['name']] == $option[$field['source']['value']]) ? 'selected' : '') : '' ?>><?= $option[$field['source']['label']] ?></option>
												<?php
										} ?>
										</select>

									<?php
									} ?>


									<!--SQL List Multi Selection Type-->
									<?php if ($field['type'] == 'sqllist_multi') {
										?>
										<label><?= $field['label'] ?></label>
										<select class="form-control" multiple name="<?= $field['name'] ?>[]" size="8">
											<!--Static Options-->
											<?php foreach ($field['fields'] as $option) {
											?>
												<option
													value="<?= $option['value'] ?>" <?= (count($page_parameters[$field['name']])) ? (in_array($option['value'], $page_parameters[$field['name']]) ? 'selected' : '') : '' ?>><?= $option['label'] ?></option>
												<?php
										} ?>
											<!--SQL Options-->
											<?php foreach ($field['source']['elements'] as $option) {
											?>
												<option
													value="<?= $option[$field['source']['value']] ?>" <?= (count($page_parameters[$field['name']])) ? ((in_array($option[$field['source']['value']], $page_parameters[$field['name']])) ? 'selected' : '') : '' ?>><?= $option[$field['source']['label']] ?></option>
												<?php
										} ?>
										</select>

									<?php
									} ?>


									<!--  tipe te tjerash inputesh-->

								<?php
								} ?>
							</div>
							<div class="text-right">
								<button type="submit" class="btn btn-info" name="save_parameters"><i
										class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
		}
	}
?>


