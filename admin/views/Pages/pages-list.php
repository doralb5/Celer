<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Pages');

foreach ($pages as &$row) {
	$row->rows_buttons = array(
		new Button("<i class='fa fa-pencil'></i>", $url_page_edit.'/'.$row->id, 'xs', 'btn-xs btn-info', '[$Edit]'),
	);
}

$table->setElements($pages);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('name', '[$Name]', 'string', 'left'),
	new TableList_Field('component_name', '[$Component]', 'string', 'left'),
	new TableList_Field('action', '[$Action]', 'string', 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
));
$table->setUrl_action($url_pages_list);
$table->setUrl_delete($url_page_delete);

$table->render();
