<?php

class SelectionListView extends BaseView
{
	protected $name;
	protected $id;
	private $viewfile;
	private $method = 'POST';
	private $ajx_url;
	private $destinationField;
	protected $title = 'Choose a record';
	protected $textSelector;
	protected $idSelector;
	protected $JSCode;
	protected $action_buttons = array();

	public function __construct($name = '', $id = null, $viewfile = 'selection-list')
	{
		parent::__construct();
		$this->renderTemplate(false);
		$this->name = $name;
		$this->id = $id;
		$this->viewfile = $viewfile;
	}

	public function getViewFile()
	{
		return $this->viewfile;
	}

	public function setMethod($meth = 'POST')
	{
		$this->method = $meth;
	}

	public function getMethod()
	{
		return $this->method;
	}

	public function setAjxUrl($url)
	{
		$this->ajx_url = $url;
	}

	public function getAjxUrl()
	{
		return $this->ajx_url;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function render($view = '', $return_output = false)
	{
		return parent::render($this->viewfile, false, false);
	}

	public function setDestinationField($fieldname)
	{
		$this->destinationField = $fieldname;
	}

	public function getDestinationField()
	{
		return $this->destinationField;
	}

	public function setDestTextSelector($textSelect)
	{
		$this->textSelector = $textSelect;
	}

	public function setDestIdSelector($idSelect)
	{
		$this->idSelector = $idSelect;
	}

	public function addJSCode($JSCode)
	{
		$this->JSCode = $JSCode;
	}

	public function addButton($button)
	{
		array_push($this->action_buttons, $button);
	}
}

class SelectionList_Table
{
	private $name;
	private $id;
	private $elements;
	private $fields;
	private $currency_symbol;

	public function __construct($name = '', $id = null)
	{
		$this->name = $name;
		$this->id = $id;
		$this->currency_symbol = '$';
	}

	public function setElements($elements)
	{
		$this->elements = $elements;
	}

	public function setFields($fields)
	{
		$this->fields = $fields;
	}

	public function setCurrencySymbol($curr)
	{
		$this->currency_symbol = $curr;
	}

	public function renderTable()
	{
		$id = (!is_null($this->id)) ? ('_'.$this->id) : '';

		echo "<table id='responseTable_{$this->name}{$id}' class='table table-striped table-bordered nowrap responseTable' width='100%'>";
		echo '<thead>';
		echo '<tr>';

		foreach ($this->fields as $field) {
			if ($field->type == 'hidden') {
				$display = 'none';
			} else {
				$display = 'table-cell';
			}

			echo "<th style='display: $display'>";
			echo $field->label;
			echo '</th>';
		}

		echo "<th><i class='fa fa-check-square-o'></i></th>";
		echo '</tr>';

		foreach ($this->elements as $element) {
			echo "<tr id='id_{$element->id}{$id}'>";
			foreach ($this->fields as $field) {
				if ($field->type == 'hidden') {
					$display = 'none';
				} else {
					$display = 'table-cell';
				}

				echo "<td style='text-align:{$field->align}; display: $display' class='{$field->name}'>";

				switch ($field->type) {
					case 'curr':
						($element->{$field->name} == 0) ? $element->{$field->name} = '-' : ($element->{$field->name} = $this->currency_symbol.' '.$element->{$field->name});
						break;
					case 'bool':
						($element->{$field->name} > 0 || $element->{$field->name} == true) ? $element->{$field->name} = '<i class="fa fa-check-square" style="color:green"></i>' : $element->{$field->name} = '<i class="fa fa-square-o" style="color:green"></i>';
						break;
					case 'byte':
						$element->{$field->name} = Utils::formatSizeUnits($element->{$field->name});
						break;
					case 'image':
						($element->{$field->name} != '') ? $element->{$field->name} = '<img src="'.$element->{$field->name}.'?w=70&h=70&far=1&bg=FFFFFF" width="70" />' : $element->{$field->name} = '';
						break;
					case 'hidden':
						$element->{$field->name};
						break;
					default:
						break;
				}

				echo $element->{$field->name};

				echo '</td>';
			}
			echo '<td>';
			echo "<input type='radio' name='SelectedElement{$id}' value='{$element->id}'/>";
			echo "</td>\n";
			echo '</tr>';
		}

		echo '</thead>';
		echo '</table>';

		echo "<script>$(document).ready(function() {
                $('#responseTable_{$this->name}{$id} tr').click(function() {
                    $(this).find(\"input:radio\").prop(\"checked\", true);
                });
                $('#responseTable_{$this->name}{$id} tr').dblclick(function() {
                    $( '#RespondButton_{$this->name}{$id}' ).trigger( 'click' );
                    $(this).find(\"input:radio\").prop(\"checked\", true);
                });
            });</script>";
	}
}

class SelectionList_Field
{
	public $name;
	public $label;
	public $type;
	public $align;

	public function __construct($name, $label = '', $type = 'string', $align = 'left')
	{
		$this->name = $name;
		$this->label = ($label == '') ? $name : $label;
		$this->type = $type;
		$this->align = $align;
	}
}
