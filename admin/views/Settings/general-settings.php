<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-5" data-init="true">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$GeneralSettings]</h4>
        </div>
        <div class="panel-body">
            <form method="post" enctype="multipart/form-data">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tbody>
							<?php foreach ($settings as $setting) {
	?>

								<?php if ($setting->field == 'logo') {
		?>
									<tr>
									<td>
										[$<?= $setting->field ?>]
									</td>
									<td>
										<img
											src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.CMSSettings::$logo ?>"
											alt="Current Logo" width="150px"/>
										<div class="form-group col-md-6">
											<label>Logo</label>
											<input type="file" name="<?= $setting->field ?>"/>
										</div>
									</td>
									</tr>
									<?php
									continue;
	} elseif ($setting->field == 'logo_inverse') {
		?>
									<tr>
									<td>
										[$<?= $setting->field ?>]
									</td>
									<td>
										<img
											src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.CMSSettings::$logo_inverse ?>"
											alt="Current Logo Inverse" width="150px"/>
										<div class="form-group col-md-6">
											<label>Logo Inverse</label>
											<input type="file" name="<?= $setting->field ?>"/>
										</div>
									</td>
									</tr>
									<?php
									continue;
	} elseif ($setting->field == 'marker_icon') {
		?>
									<tr>
									<td>
										[$<?= $setting->field ?>]
									</td>
									<td>
										<img
											src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.CMSSettings::$marker_icon ?>"
											alt="Current marker icon" width="150px"/>
										<div class="form-group col-md-6">
											<label>Maps Maker icon</label>
											<input type="file" name="<?= $setting->field ?>"/>
										</div>
									</td>
									</tr>
									<?php continue;
	} ?>
								<tr>
								<td>
									[$<?= $setting->field ?>]
								</td>
								<td>
									<input type="text" class="form-control" name="<?= $setting->field ?>"
										   value='<?= (isset($_POST[$setting->field])) ? $_POST[$setting->field] : $setting->value; ?>'>
								</td>
								</tr>
<?php
} ?>
                        </tbody>
                    </table>
                    <table class="table table-hover" id="addNewField">
                        <tr>
                        <td>
                            <input class="btn btn-success" type="button" id="addButton" value="Add New Field" />
                        </td>
                        <td>
                            <input class="btn btn-warning" type="button" id="removeButton" value="Remove" />
                        </td>
                        <td></td>
                        </tr>
                        <tr class="addfield">
                        <td>
                            <input class="form-control" type="text" placeholder="name of field" name="addfieldname[]" />
                        </td>
                        <td>
                            <input class="form-control" type="text" placeholder="value" name="addfieldvalue[]" />
                        </td>
                        <td/>
                        </tr>
                    </table>

                    <script>
                        $(function () {
                            $("#addButton").on("click", function () {
                                var newName = $(".addfield").first().clone().addClass("newAdded");

                                newName.appendTo("#addNewField");
                            });

                            $("#removeButton").click(function () {
                                var counter = $(".addfield").length;

                                if (counter == 1) {
                                    alert("No more textbox to remove");

                                    return false;
                                }
                                $(".addfield").last().remove();
                            });


                        });
                        function deleteRoom(room)
                        {
                            var i = room.parentNode.parentNode.rowIndex;
                            document.getElementById('addNewField').deleteRow(i);
                        }
                    </script>
                    <button type="submit" class="btn btn-info btn-lg pull-right" name='save'><i
                            class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
