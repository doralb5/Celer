<?php

class TableListView extends BaseView
{
	protected $name;
	private $viewfile;
	private $elements;
	private $elements_per_page;
	private $totalElements;
	private $fields;
	private $url_action;
	protected $images_path;
	public $image_base_url = '';
	private $url_delete;
	private $url_enable;
	private $url_delete_params = array();
	private $url_enable_params = array();
	private $action_buttons;
	protected $renderTopBar = true;
	protected $multiple_deletion = true;
	protected $delete_option = true;
	protected $renderPanel = true;
	protected $renderFilter = true;
	protected $action_filters = array();
	protected $show_icon = false;
	protected $title = '[$List]';
	protected $code = '';
	protected $show_messages = false;
	protected $messages_id = '';
	protected $show_result_info = true;

	public function __construct($name = '', $viewfile = 'list_v2')
	{
		parent::__construct();
		$this->renderTemplate(false);
		$this->name = $name;
		$this->viewfile = $viewfile;

		$this->elements = array();
		$this->elements_per_page = 20;
		$this->fields = array();

		$this->action_buttons = array();

		$this->url_action = '';
		$this->url_delete = '';
	}

	public function getViewFile()
	{
		return $this->viewfile;
	}

	public function setTotalElements($totalElements)
	{
		$this->totalElements = $totalElements;
	}

	public function getTotalElements()
	{
		return $this->totalElements;
	}

	public function setElements($elements)
	{
		if (count($elements)) {
			if (is_object($elements[0])) {
				foreach ($elements as &$el) {
					$el = Utils::ObjectToArray($el);
				}
			}
			$this->elements = $elements;

			$columns = array_keys($elements[0]);
			foreach ($columns as $c) {
				array_push($this->fields, new TableList_Field($c));
			}
		} else {
			$this->elements = array();
			$this->fields = array();
		}
	}

	public function setElements_per_page($elements_per_page)
	{
		$this->elements_per_page = $elements_per_page;
	}

	public function setFields($fields)
	{
		$this->fields = $fields;
	}

	public function setUrl_action($url_action)
	{
		$this->url_action = $url_action;
	}

	public function setUrl_delete($url_delete)
	{
		$this->url_delete = $url_delete;
	}

	public function setUrl_enable($url_enable)
	{
		$this->url_enable = $url_enable;
	}

	public function setUrl_delete_params($params)
	{
		$this->url_delete_params = $params;
	}

	public function setAction_buttons($action_buttons)
	{
		$this->action_buttons = $action_buttons;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function setImagesPath($path)
	{
		$this->images_path = $path;
	}

	public function getElements()
	{
		return $this->elements;
	}

	public function getElements_per_page()
	{
		return $this->elements_per_page;
	}

	public function getFields()
	{
		return $this->fields;
	}

	public function getUrl_action()
	{
		return $this->url_action;
	}

	public function getUrl_delete()
	{
		return $this->url_delete.'/';
	}

	public function getUrl_enable()
	{
		return $this->url_enable.'/';
	}

	public function getUrl_delete_params()
	{
		return $this->url_delete_params;
	}

	public function getUrl_enable_params()
	{
		return $this->url_enable_params;
	}

	public function getAction_buttons()
	{
		return $this->action_buttons;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function renderTopBar($render = true)
	{
		$this->renderTopBar = (bool) $render;
	}

	public function renderFilter($render = true)
	{
		$this->renderFilter = (bool) $render;
	}

	public function renderPanel($render = true)
	{
		$this->renderPanel = (bool) $render;
	}

	public function multipleDeletion($render = true)
	{
		$this->multiple_deletion = (bool) $render;
	}

	public function deleteOption($del = true)
	{
		$this->delete_option = (bool) $del;
	}

	public function addSelectFilter($select)
	{
		array_push($this->action_filters, $select);
	}

	public function showIcon($show)
	{
		$this->show_icon = $show;
	}

	public function addCode($code)
	{
		$this->code = $code;
	}

	public function printCode()
	{
		echo $this->code;
	}

	public function showMessages($show = true)
	{
		$this->show_messages = $show;
	}

	public function setMessagesId($id)
	{
		$this->messages_id = $id;
	}

	public function showResultInfo($show = true)
	{
		$this->show_result_info = $show;
	}

	public function render($view = '', $return_output = false)
	{
		return parent::render($this->viewfile, false, false);
	}
}

class TableList_Field
{
	public $name;
	public $label;
	public $type;
	public $align;
	public $class;

	public function __construct($name, $label = '', $type = 'string', $align = 'left', $class = '')
	{
		$this->name = $name;
		$this->label = ($label == '') ? $name : $label;
		$this->type = $type;
		$this->align = $align;
		$this->class = $class;
	}
}
