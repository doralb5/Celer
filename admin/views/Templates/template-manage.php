<style>
    .colorpicker-2x .colorpicker-saturation {
        width: 200px;
        height: 200px;
    }

    .colorpicker-2x .colorpicker-hue,
    .colorpicker-2x .colorpicker-alpha {
        width: 30px;
        height: 200px;
    }

    .colorpicker-2x .colorpicker-color,
    .colorpicker-2x .colorpicker-color div {
        height: 30px;
    }
</style>
<?php
HeadHTML::AddControllerCSS('Templates', 'simpletree.css');
HeadHTML::AddControllerJS('Templates', 'simpletreemenu.js');
?>

<?php if (!is_null($template)) {
	?>

	<div class="col-md-12">
		<div class="image">
			<img src="<?= $template->image ?>" alt="Image"/>
		</div>
		<div class="title">
			<h3><?= $template->name ?></h3>
		</div>
		<div class="info">
			<?= $template->author ?><br/>
			<?= $template->email ?><br/>
			<?= $template->date ?><br/>
			<?= $template->website ?><br/><br/>
			<?= $template->description ?>
		</div>
	</div>

	<?php
	$active_tab = (isset($_GET['active'])) ? $_GET['active'] : 'tab_general';
	$filetype = (isset($_GET['filetype'])) ? $_GET['filetype'] : 'css';

	switch ($filetype) {
		case 'css':
			break;
		case 'php':
			break;
		case 'js':
			$filetype = 'javascript';
			break;
		case 'txt':
			$filetype = 'cypher';
			break;
		default:
			break;
	} ?>

	<div class="col-md-12">
		<ul class="nav nav-tabs">
			<li class="text-uppercase <?= ($active_tab == 'tab_general') ? 'active' : '' ?>"><a
					href="#tab_general" data-toggle="tab">General</a></li>
			<li class="text-uppercase <?= ($active_tab == 'tab_editor') ? 'active' : '' ?>"><a href="#tab_editor"
																							   data-toggle="tab">Editor</a>
			</li>
			<li class="text-uppercase <?= ($active_tab == 'tab_overrides') ? 'active' : '' ?>"><a href="#tab_overrides"
																								  data-toggle="tab">Overrides</a>
			</li>
			<li class="text-uppercase <?= ($active_tab == 'tab_options') ? 'active' : '' ?>"><a href="#tab_options"
																								data-toggle="tab">Options</a>
			</li>
		</ul>
		<div class="tab-content">
			<span>&nbsp;</span>

			<!--Tab General-->
			<div class="tab-pane <?= ($active_tab == 'tab_general') ? 'active' : '' ?>" id="tab_general">
				<form method="post">
					<div class="row row-modify-header-additional">
						<div class="col-md-2">
							<label>Header Additional Code</label>
						</div>
						<div class="col-md-9">
							<textarea class="form-control"  name="head-extra" cols="25" rows="5"><?= $head_extra ?></textarea>  
						</div>  

					</div>    
					<div class="row row-modify-header-additional">
						<div class="col-md-2">
							<label>Footer Additional Code</label> 
						</div>
						<div class="col-md-9">
							<textarea class="form-control" name="footer-extra" cols="25" rows="5"><?= $footer_extra ?></textarea>  
						</div>       
					</div>
					<div class="row text-center">
						<input type="submit" name="save_general" value="Save" class="btn btn-info text-center"/>
					</div>
				</form>
			</div>


			<!--Tab Editor-->
			<div class="tab-pane <?= ($active_tab == 'tab_editor') ? 'active' : '' ?>" id="tab_editor">
				<span>&nbsp;</span>
				<div class="row">
					<div class="col-md-3">
						<h4><strong>Files</strong></h4>
						<hr/>
						<?php printFileTree('tab_editor', $template_path); ?>
					</div>
					<div class="col-md-9">
						<?php if (isset($sourceCode)) {
		?>
							<form method="post" action="">
								<h4>File: <?= (isset($_GET['filepath'])) ? $_GET['filepath'] : '' ?></h4>
								<p>
									<a href="javascript:autoFormatSelection()" class="btn btn-xs btn-default">Format
										Selection</a>
									<a href="javascript:commentSelection(true)" class="btn btn-xs btn-default">Comment
										Selection</a>
									<a href="javascript:commentSelection(false)" class="btn btn-xs btn-default">Uncomment
										Selection</a>
								</p>
								<textarea style="width: 100%;" id="sourceCode"
										  name="editorCode"><?= $sourceCode ?></textarea>
								<p>&nbsp;</p>
								<p class="text-center">
									<input type="submit" name="save" class="btn btn-info" value="Save"/>
									<input type="submit" name="create_override" class="btn btn-default"
										   value="Create Override"/>
								</p>
							</form>
						<?php
	} else {
		?>
							Please select one file on the left.
						<?php
	} ?>
					</div>
				</div>
			</div>


			<!--Tab Overrides-->
			<div class="tab-pane <?= ($active_tab == 'tab_overrides') ? 'active' : '' ?>" id="tab_overrides">
				<span>&nbsp;</span>
				<div class="row">
					<div class="col-md-3">
						<h4><strong>Files</strong></h4>
						<hr/>
						<?php printFileTree('tab_overrides', $template_data_path); ?>
					</div>
					<div class="col-md-9">
						<?php if (isset($sourceCode)) {
		?>
							<form method="post" action="">
								<h4>File: <?= (isset($_GET['filepath'])) ? $_GET['filepath'] : '' ?></h4>
								<p>
									<a href="javascript:autoFormatSelection()" class="btn btn-xs btn-default">Format
										Selection</a>
									<a href="javascript:commentSelection(true)" class="btn btn-xs btn-default">Comment
										Selection</a>
									<a href="javascript:commentSelection(false)" class="btn btn-xs btn-default">Uncomment
										Selection</a>
								</p>
								<textarea style="width: 100%;" id="sourceCodeOverride"
										  name="editorCodeOverride"><?= $sourceCode ?></textarea>
								<p>&nbsp;</p>
								<p class="text-center">
									<input type="submit" name="save_override" class="btn btn-info" value="Save"/>
								</p>
							</form>
						<?php
	} else {
		?>
							Please select one file on the left.
						<?php
	} ?>
					</div>
				</div>
			</div>


			<!--Tab Options-->
			<div class="tab-pane <?= ($active_tab == 'tab_options') ? 'active' : '' ?>" id="tab_options">
				<span>&nbsp;</span>
				<div class="col-md-6">
					<?php if (count($options)) {
		?>
						<form method="post" action="" enctype="multipart/form-data">
							<?php foreach ($options as $opt) {
			?>

								<div class="form-group">
									<?php if ($opt['type'] == 'int' || $opt['type'] == 'string') {
				?>
										<label><?= $opt['label'] ?></label>
										<input type="text" class="form-control" name="<?= $opt['name'] ?>"
											   placeholder="<?= $opt['label'] ?>"
											   value="<?= (isset($options_values[$opt['name']])) ? $options_values[$opt['name']] : '' ?>">
										   <?php
			} ?>

									<?php if ($opt['type'] == 'image') {
				?>
										<label><?= $opt['label'] ?></label>

										<?php $img = (isset($options_values[$opt['name']])) ? $options_values[$opt['name']] : '' ?>
										<?php if ($img != '') {
					?>
											<img class="pull-right" src="<?= $temp_path.DS.'images'.DS.$img ?>"
												 alt="Current Logo" width="150px"/>
											 <?php
				} ?>

										<input type="file" name="<?= $opt['name'] ?>"/>
										<div class="checkbox">
											<label><input type="checkbox" name="reset_<?= $opt['name'] ?>" value="">Reset</label>
										</div>

									<?php
			} ?>

									<?php if ($opt['type'] == 'color') {
				?>
										<label><?= $opt['label'] ?></label>

										<input type="text" id="colorPicker_<?= $opt['name'] ?>"
											   name="<?= $opt['name'] ?>"
											   value="<?= (isset($options_values[$opt['name']])) ? $options_values[$opt['name']] : '#ffaa00' ?>"
											   class="form-control"/>

									<?php
			} ?>


								</div>
								<div class="clearfix"></div>
							<?php
		} ?>
							<p>&nbsp;</p>
							<p class="text-center">
							<button type="submit" class="btn btn-info" name="save_options">SAVE</button>
							</p>
						</form>
					<?php
	} ?>
				</div>
			</div>
		</div>
	</div>


	<style>
		.CodeMirror {
			height: 500px !important
		}

		.treeview .submenu ul li {
			cursor: pointer !important;
		}
	</style>
	<script type="text/javascript">
		ddtreemenu.createTree("tab_editor_treemenu", false);
		ddtreemenu.flatten('tab_editor_treemenu', 'expand');
		ddtreemenu.createTree("tab_overrides_treemenu", false);
		ddtreemenu.flatten('tab_overrides_treemenu', 'expand');
	</script>
	<script>
		function codeEditor(id) {
			window[id] = CodeMirror.fromTextArea(document.getElementById(id), {
				mode: "<?= $filetype ?>",
				lineNumbers: true,
				styleActiveLine: true, matchBrackets: true,
				smartIndent: true,
				theme: "cobalt",
				extraKeys: {"Ctrl-Space": "autocomplete"},
			});
		}

		function getSelectedRange(editor) {
			return {from: editor.getCursor(true), to: editor.getCursor(false)};
		}

		function autoFormatSelection() {
			var range1 = getSelectedRange(sourceCode);
			sourceCode.autoFormatRange(range1.from, range1.to);
			var range2 = getSelectedRange(sourceCodeOverride);
			sourceCodeOverride.autoFormatRange(range2.from, range2.to);
		}

		function commentSelection(isComment) {
			var range1 = getSelectedRange(sourceCode);
			sourceCode.commentRange(isComment, range1.from, range1.to);
			var range2 = getSelectedRange(sourceCodeOverride);
			sourceCodeOverride.commentRange(isComment, range2.from, range2.to);
		}

		codeEditor('sourceCode');
		codeEditor('sourceCodeOverride');
	</script>
	<script>
		$(function () {
	<?php foreach ($options as $opt) {
		?>
		<?php if ($opt['type'] == 'color') {
			?>
					$('#colorPicker_<?= $opt['name'] ?>').colorpicker({
						customClass: 'colorpicker-2x',
						sliders: {
							saturation: {
								maxLeft: 200,
								maxTop: 200
							},
							hue: {
								maxTop: 200
							},
							alpha: {
								maxTop: 200
							}
						}
					});
			<?php
		}
	} ?>
		});
	</script>
<?php
} ?>


<?php

function printFileTree($active_tab, $basedir, $dir = '')
{
	$fulldir = rtrim($basedir, '/').'/'.ltrim($dir, '/');

	if (is_dir($fulldir)) {
		echo '<ul id="'.$active_tab.'_treemenu" class="treeview">';
		if ($dh = opendir($fulldir)) {
			while (($file = readdir($dh)) !== false) {
				//echo $file.'-';
				if ($file != '.' && $file != '..' && $file != 'config.xml' && (Utils::allowedFileType($file, array('js', 'css', 'php', 'txt')) || is_dir($fulldir.DS.$file))) {
					echo '<li>';
					if (is_file($fulldir.DS.$file)) {
						echo "<a class='filetype' href='".'?filepath='.$dir.DS.$file.'&active='.$active_tab.'&filetype='.pathinfo($file, PATHINFO_EXTENSION)."'>";
						echo $file;
						echo '</a>';
					} else {
						echo $file;
						printFileTree($active_tab, $basedir, $dir.DS.$file);
					}
					echo '</li>';
				}
			}
			closedir($dh);
		}
		echo '</ul>';
	}
}
