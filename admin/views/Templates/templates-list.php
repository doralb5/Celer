<div class="col-md-12">
	<?php if (count($templates) > 0) {
	?>

		<?php
		$i = 0;
	foreach ($templates as $template) {
		?>

			<form method="post">
				<div class="panel panel-<?= ($template->enabled) ? 'primary' : 'warning' ?>">
					<div class="panel-heading text-right">
						<div class="pull-left">
							<span class="text-uppercase"><?= $template->name ?></span>
						</div>
						<input type="hidden" name="enabled_item" value="<?= $template->id ?>">

						<a type="submit"
						   href="<?= Utils::getComponentUrl('Templates/manage_template/'.$template->id) ?>"
						   class="btn btn-xs btn-<?= ($template->enabled) ? 'primary' : 'warning' ?>">MANAGE</a>

		<?php if (!$template->enabled) {
			?>
							<button type="submit" name="enable"
									class="btn btn-xs btn-<?= ($template->enabled) ? 'primary' : 'warning' ?>">ENABLE
							</button>
							<a class="btn btn-danger btn-xs" data-toggle="modal" href="#myModal<?= $i ?>"><i
									class="fa fa-times"></i></a>
		<?php
		} ?>
					</div>
					<div class="panel-body">
						<!--Description-->
					</div>
				</div>
			</form>


			<!-- Modal -->
			<div class="modal fade" id="myModal<?= $i ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
				 aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Delete Template: <strong><?= $template->name ?></strong></h4>
						</div>
						<div class="modal-body">
							Are you sure you want to delete this template ?
						</div>
						<div class="modal-footer">
							<a href="<?= Utils::getControllerUrl('Templates/delete_template/'.$template->id) ?>"
							   class="btn btn-danger" type="submit"> Delete</a>
							<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
						</div>
					</div>
				</div>
			</div>
			<!-- modal -->

			<?php
			$i++;
	} ?>
<?php
} else {
		?>
		<div class="alert alert-warning fade in">
			<button data-dismiss="alert" class="close close-sm" type="button">
				<i class="icon_error-circle"></i>
			</button>
			There are no templates !
		</div>
<?php
	} ?>
</div>
