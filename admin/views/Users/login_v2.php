<?php
$brand_info = UserAuth::getBrandInfoSession();
?>
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<div class="login-cover">
    <!--    <div class="login-cover-image">
            <img src="assets/img/login-bg/bg-1.jpg" data-id="login-cover-image" alt="" />
        </div>-->
    <div class="login-cover-bg"></div>
</div>
<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin login -->
    <div class="login login-v2" data-pageload-addclass="animated fadeIn">
        <!-- begin brand -->
        <div class="login-header">
			<?php
			if (!is_null($brand_info) && $brand_info['logo'] != '') {
				$logo = 'http://serv.bluehat.al/data/serv.bluehat.al/media/brands/'.$brand_info['logo'];
				if (@get_headers($logo)[0] != 'HTTP/1.1 404 Not Found') {
					?>
					<div class=" col-md-8 col-md-offset-2 text-center">
						<img src="<?= $logo ?>" alt="BRAND LOGO" width="100%"/>
					</div>
				<?php
				}
			} elseif (!is_null($brand_info)) {
				?>

				<div class="brand text-center">
					<span class="logo"></span> <?= $brand_info['name'] ?>
				</div>
<?php
			} else {
				?>
				<div class="brand text-center">
					<span class="logo"></span> WeWeb
				</div>
<?php
			} ?>

        </div>
        <!-- end brand -->
        <div class="login-content">
            <form method="POST" class="margin-bottom-0" method="post">
                <div class="form-group m-b-20">
<?php Messages::showErrors(); ?>
                </div>
                <div class="form-group m-b-20">
                    <input name="username" type="text" class="form-control input-lg" placeholder="Username"
                           required=""/>
                </div>
                <div class="form-group m-b-20">
                    <input name="password" type="password" class="form-control input-lg" placeholder="Password"
                           required=""/>
                </div>
                <!--                <div class="checkbox m-b-20">
                                    <label>
                                        <input type="checkbox" /> Remember Me
                                    </label>
                                </div>-->
                <div class="login-buttons">
                    <button type="submit" name="login" class="btn btn-success btn-block btn-lg">Sign me in</button>
                </div>
                <div class="m-t-20">
                    Not a member yet ? Click <a href="<?= Utils::getControllerUrl('Users/register') ?>">here</a> to
                    register.
                </div>
            </form>
        </div>
    </div>
    <!-- end login -->
</div>
<!-- end page container -->