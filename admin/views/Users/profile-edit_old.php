<?php
$username = isset($_POST['username']) ? $_POST['username'] : $user->username;
$email = isset($_POST['email']) ? $_POST['email'] : $user->email;
$firstname = isset($_POST['firstname']) ? $_POST['firstname'] : $user->firstname;
$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : $user->lastname;
?>

<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$ClientInformation]</h4>
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="demo-form"
                  novalidate="" enctype="multipart/form-data">

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Username] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $username ?>"
                               name="username" placeholder="[$Username]" data-parsley-required="true"
                               data-parsley-id="6524" required>
                        <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Email] *</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="email" value="<?= $email ?>"
                               name="email" placeholder="[$Email]" data-parsley-required="true"
                               data-parsley-id="6524" required>
                        <ul class="parsley-errors-list" id="parsley-id-6524"></ul>
                    </div>
                </div>
				<?php if (is_null($user->id)) {
	?>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4">[$Password] *</label>

						<div class="col-md-8 col-sm-8">
							<input class="form-control" type="password"
								   name="password" placeholder="[$Password]" data-parsley-required="true"
								   data-parsley-id="6524" required>
							<ul class="parsley-errors-list" id="parsley-id-6524"></ul>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4">[$ConfirmPassword] *</label>

						<div class="col-md-8 col-sm-8">
							<input class="form-control" type="password"
								   name="cpassword" placeholder="[$ConfirmPassword]" data-parsley-required="true"
								   data-parsley-id="6524" required>
							<ul class="parsley-errors-list" id="parsley-id-6524"></ul>
						</div>
					</div>
				<?php
} ?>

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Firstname]</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $firstname ?>"
                               name="firstname" placeholder="[$Firstname]">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4">[$Lastname]</label>

                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="<?= $lastname ?>"
                               name="lastname" placeholder="[$Lastname]">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label">[$Image]</label>

                    <div class="col-md-6 col-sm-6">
                        <input type="file" name="image">
						<?php if ($user->image != '') {
		?>
							<p>
								<img
									src="<?= substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')).MEDIA_ROOT.'users/'.$user->image ?>"
									width="100px" alt="Image"/></p>
							<?php
	} ?>
                    </div>
                </div>
                <div class="col-md-12 text-right">

                    <p>&nbsp;</p>
                    <button type="submit" name="save" class="btn btn-sm btn-info m-r-5"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                    </button>
                    <p>&nbsp;</p>
                </div>
            </form>
        </div>
    </div>

	<?php if (!is_null($user->id)) {
		?>
		<div class="panel panel-inverse" data-sortable-id="form-stuff-5">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
							class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
							class="fa fa-repeat"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
					   data-click="panel-collapse"><i
							class="fa fa-minus"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
					   data-click="panel-remove"><i
							class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title">[$ChangePassword]</h4>
			</div>
			<div class="panel-body">
				<form class="form-inline" method="POST">
					<div class="form-group m-r-10">
						<input type="password" name="password" class="form-control" id="exampleInputPassword2"
							   placeholder="New Password">
					</div>
					<div class="form-group m-r-10">
						<input type="password" name="rpassword" class="form-control" id="exampleInputPassword2"
							   placeholder="Repeat Password">
					</div>
					<div class="checkbox m-r-10">
						<label>
							<input name="sendmail" type="checkbox"> Send by Email
						</label>
					</div>
					<button type="submit" name="save_password" class="btn btn-sm btn-info m-r-5"><i
							class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
					</button>
				</form>
			</div>
		</div>
	<?php
	} ?>
</div>