<?php
$code = (isset($_POST['code'])) ? $_POST['code'] : '';
$firstname = (isset($_POST['firstname'])) ? $_POST['firstname'] : '';
$lastname = (isset($_POST['lastname'])) ? $_POST['lastname'] : '';
$username = (isset($_POST['username'])) ? $_POST['username'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
?>

<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin register -->
    <div class="register register-with-news-feed">
        <!-- begin news-feed -->
        <div class="news-feed">
            <div class="news-image">
                <img src="<?= $this->template_path ?>assets/img/login-bg/bg-8.jpg" alt="" />
            </div>
            <div class="news-caption">
                <h4 class="caption-title"><i class="fa fa-xing text-success"></i> WeWeb</h4>
<!--                <p>
                    As a Color Admin Apps administrator, you use the Color Admin console to manage your organization’s account, such as add new users, manage security settings, and turn on the services you want your team to access.
                </p>-->
            </div>
        </div>
        <!-- end news-feed -->
        <!-- begin right-content -->
        <div class="right-content">
            <!-- begin register-header -->
            <h1 class="register-header">
                Sign Up
                <small>Create your WeWeb Account.</small>
            </h1>
            <!-- end register-header -->
            <!-- begin register-content -->
            <div class="register-content">
                <form method="POST" class="margin-bottom-0">
                    <div class="row m-b-15">
                        <div class="col-md-12">
							<?php Messages::showErrors(); ?>
							<?php Messages::showInfoMsg(); ?>
                        </div>
                    </div>
                    <label class="control-label">Name</label>
                    <div class="row row-space-10">
                        <div class="col-md-6 m-b-15">
                            <input type="text" class="form-control" placeholder="First name" name="firstname" value="<?= $firstname ?>"required=""/>
                        </div>
                        <div class="col-md-6 m-b-15">
                            <input type="text" class="form-control" placeholder="Last name" name="lastname" value="<?= $lastname ?>"required=""/>
                        </div>
                    </div>
                    <label class="control-label">Username</label>
                    <div class="row m-b-15">
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="Username" name="username" value="<?= $username ?>"required=""/>
                        </div>
                    </div>
                    <label class="control-label">Email</label>
                    <div class="row m-b-15">
                        <div class="col-md-12">
                            <input type="email" class="form-control" placeholder="Email address" name="email" value="<?= $email ?>"required=""/>
                        </div>
                    </div>
                    <label class="control-label">Password</label>
                    <div class="row m-b-15">
                        <div class="col-md-12">
                            <input type="password" class="form-control" placeholder="Password" name="password" required=""/>
                        </div>
                    </div>
                    <label class="control-label">Re-enter Password</label>
                    <div class="row m-b-15">
                        <div class="col-md-12">
                            <input type="password" class="form-control" placeholder="Re-enter Password" name="rpassword" required=""/>
                        </div>
                    </div>


                    <div class="checkbox m-b-30">
                        <label>
                            <input type="checkbox" name="agree"/> By clicking Sign Up, you agree to our <a href="#">Terms</a> and that you have read our <a href="#">Data Policy</a>, including our <a href="#">Cookie Use</a>.
                        </label>
                    </div>
                    <div class="register-buttons">
                        <button type="submit" name="register" class="btn btn-primary btn-block btn-lg">Sign Up</button>
                    </div>
                    <div class="m-t-20 m-b-40 p-b-40">
                        Already a member? Click <a href="<?= Utils::getControllerUrl('Users/login') ?>">here</a> to login.
                    </div>
                    <hr />
                    <p class="text-center text-inverse">
                        &copy; WeWeb <?= date('Y') ?>
                    </p>
                </form>
            </div>
            <!-- end register-content -->
        </div>
        <!-- end right-content -->
    </div>
    <!-- end register -->
</div>
<!-- end page container -->