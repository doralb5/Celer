<?php
$loggeduser = UserAuth::getLoginSession();

$user_admin = isset($_POST['admin']) ? $_POST['admin'] : $user->admin;
$user_super = isset($_POST['super']) ? $_POST['super'] : $user->super;
?>

<?php
include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Users');

$table->setElements($comp_permitted);

$table->setTotalElements(count($comp_permitted));
$table->setElements_per_page(100);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('comp_name', '[$ComponentName]', 'string', 'left'),
));
$table->setUrl_delete($url_permission_delete);
$table->setUrl_delete_params(array('id_user' => $user->id));
$table->multipleDeletion(false);
$table->renderTopBar(false);
$table->setTitle('[$UserPermissions]');
$table->render();
?>

<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="ui-widget-16">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$AddPermission]</h4>
        </div>
        <div class="panel-body">
            <div class="form">
                <form role="form" method="post">
                    <div class="form-group col-md-6">
                        <label>[$Component]</label>

                        <div class="input-group">
                            <select class="form-control" name="comp_id[]">
								<?php foreach ($comp_unpermitted as $comp) {
	?>
									<option value="<?= $comp->id ?>"><?= $comp->name ?></option>
								<?php
} ?>
                            </select>

                            <div class="input-group-btn">
                                <button type="submit" name="add" class="btn btn-info"><i
                                        class="fa fa-plus-circle"></i>&nbsp;&nbsp;[$add]
                                </button>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">


                            </div>
                        </div>
                    </div>
                    <p class="text-center"></p>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$ChangeType]</h4>
        </div>
        <div class="panel-body">
            <form method="POST">

                <div class="form-group col-md-6">
                    <label>[$Admin]</label>
                    <select class="form-control" name="admin" id="choose-admin">
                        <option value="0" <?= ($user_admin == 0) ? 'selected' : '' ?>>No</option>
                        <option value="1" <?= ($user_admin == 1) ? 'selected' : '' ?>>Yes</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>[$Super]</label>

                    <div class="input-group">
                        <select class="form-control" name="super" id="choose-super">
                            <option value="0" <?= ($user_super == 0) ? 'selected' : '' ?>>No</option>
                            <option value="1" <?= ($user_super == 1) ? 'selected' : '' ?>>Yes</option>
                        </select>

                        <div class="input-group-btn">
                            <button type="submit" name="change" class="btn btn-info m-r-5">
                                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                   data-click="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                   data-click="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">[$ChangePassword]</h4>
        </div>
        <div class="panel-body">
            <form class="form-inline" method="POST">
                <div class="form-group m-r-10">
                    <input type="password" name="password" class="form-control" id="exampleInputPassword2"
                           placeholder="New Password">
                </div>
                <div class="form-group m-r-10">
                    <input type="password" name="rpassword" class="form-control" id="exampleInputPassword2"
                           placeholder="Repeat Password">
                </div>
                <div class="checkbox m-r-10">
                    <label>
                        <input name="sendmail" type="checkbox"> Send by Email
                    </label>
                </div>
                <button type="submit" name="save" class="btn btn-sm btn-info m-r-5"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;[$Save]
                </button>
            </form>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        function adminType() {
            if ($('#choose-admin').val() == 0) {
                $('#choose-super').val(0);
                $('#choose-super').attr('disabled', true);
            } else {
                $('#choose-super').attr('disabled', false);
            }
        }

        adminType();
        $('#choose-admin').change(function () {
            adminType();
        });
    });

</script>