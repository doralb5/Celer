<?php

include(VIEWS_PATH.'TableList_class.php');

$table = new TableListView('Users');

foreach ($users as &$row) {
	if ($row->enabled) {
		$btn1 = new Button("<i class='fa fa-circle-o'></i>", $url_user_disable.'/'.$row->id, 'xs', 'btn-warning', '[$Disable]');
	} else {
		$btn1 = new Button("<i class='fa fa-check-circle-o'></i>", $url_user_enable.'/'.$row->id, 'xs', 'btn-success', '[$Enable]');
	}
	$row->rows_buttons = array(
		$btn1,
		new Button("<i class='fa fa-pencil'></i>", $url_user_edit.'/'.$row->id, 'xs', 'btn-xs btn-info', '[$Edit]'),
		new Button("<i class='fa fa-wrench'></i>", $url_user_permissions.'/'.$row->id, 'xs', 'btn-xs btn-default', '[$Manage]'),
		new Button("<i class='fa fa-eye'></i>", $url_user_details.'/'.$row->id, 'xs', 'btn-xs btn-info', '[$Details]'),
	);

	$logged_user = UserAuth::getLoginSession();
	if ($logged_user['super'] == 1) {
		array_push($row->rows_buttons, new Button("<i class='fa fa-sign-in'></i>", $url_user_loginas.'/'.$row->id, 'xs', 'btn-xs btn-success', '[$LoginAs]'));
	}
}

$table->setElements($users);

$table->setTotalElements($totalElements);
$table->setElements_per_page($elements_per_page);

$table->setFields(array(
	new TableList_Field('id', '[$Id]', 'int', 'left'),
	new TableList_Field('username', '[$Username]', 'string', 'left'),
	new TableList_Field('firstname', '[$Firstname]', 'string', 'left'),
	new TableList_Field('lastname', '[$Lastname]', 'string', 'left'),
	new TableList_Field('enabled', '[$Enabled]', 'bool', 'left'),
));
$table->setUrl_action($url_users_list);
$table->setUrl_delete($url_user_delete);

$table->render();
