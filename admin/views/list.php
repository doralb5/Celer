<!-- #### LIST VIEW #### -->

<?php
require_once DOCROOT.LIBS_PATH.'Pager.php';

$readonly = (isset($readonly)) ? $readonly : false;

$elements = $this->getElements();
$action_buttons = $this->getAction_buttons();

$btnNew_url = '#';

$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
$totalElements = $this->getTotalElements();
$elements_per_page = $this->getElements_per_page();

$currency_symbol = (isset($currency_symbol)) ? $currency_symbol : '€';
?>

<?php if ($this->renderTopBar) {
	?>
	<div class="row">

		<form class="form-inline" role="form" method="get" action="<?= $this->getUrl_action() ?>">

			<div class="col-md-8 col-sm-12">
				<?php
				if (count($action_buttons) > 0) {
					foreach ($action_buttons as $button) {
						echo '<a class="btn '.(($button->getClass() != '') ? $button->getClass() : 'btn-info')."\" href=\"{$button->getAction()}\">{$button->getName()}</a>&nbsp";
					}
				} ?>
			</div>
			<div class="col-md-4 col-sm-12">

				<div class="input-group">
					<?php if (isset($_GET['query']) && $_GET['query'] != '') {
					?>
						<span class="input-group-btn">
							<a href="<?= $this->getUrl_action() ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
						</span>
					<?php
				} ?>
					<input class="form-control" type="text" name="query" value="<?= (isset($_GET['query'])) ? $_GET['query'] : '' ?>" placeholder="[$Filter]" />
					<span class="input-group-btn">
						<button type="submit" class="btn btn-info"><i class="fa fa-filter"></i>&nbsp;&nbsp;[$Filter]</button>
					</span>
				</div>

			</div>
		</form>
	</div>



	<hr>

<?php
} ?>

<?php
if (count($elements) > 0) {
					$fields = $this->getFields(); ?>

	<?php Pager::printPager($page, $totalElements, $elements_per_page, $this->getUrl_action().((isset($_GET['query'])) ? '?query='.$_GET['query'] : '')); ?>

	<form action="" method="post">   
		<div class="table-responsive article-list">
			<table class="table">
				<thead>
					<tr>
					<?php if ($this->multiple_deletion) {
						?><th><i class="fa fa-check-square-o"></i></th><?php
					} ?>
					<?php
					foreach ($fields as $col) {
						if (!in_array($col->name, array('rows_buttons', 'row_className'))) {
							echo "<th style=\"text-align:$col->align; white-space: nowrap;\">$col->label</th>\n";
						}
					} ?>
					<?php if ((isset($elements[0]['rows_buttons']) && count($elements[0]['rows_buttons']) > 0) || $this->delete_option) {
						?>
						<th style="text-align: right; white-space: nowrap;"><i class="icon_cog"></i> [$Action]</th>
					<?php
					} ?>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($elements as $row) {
						$className = isset($row['row_className']) ? "class='{$row['row_className']}'" : ''; ?>
						<tr <?= $className ?>>
						<?php if ($this->multiple_deletion) {
							?><td><input type='checkbox' name='elements[]' value='<?= $row['id'] ?>'/></td><?php
						} ?>
						<?php
						foreach ($fields as $col) {
							if (!in_array($col->name, array('rows_buttons', 'row_className'))) {
								switch ($col->type) {
									case 'curr': ($row[$col->name] == 0) ? $row[$col->name] = '-' : ($row[$col->name] = $currency_symbol.' '.$row[$col->name]);
										break;
									case 'bool': ($row[$col->name] > 0 || $row[$col->name] == true) ? $row[$col->name] = '<i class="fa fa-check-square" style="color:green"></i>' : $row[$col->name] = '<i class="fa fa-square-o" style="color:red"></i>';
										break;
									case 'byte': $row[$col->name] = Utils::formatSizeUnits($row[$col->name]);
										break;
									case 'date':
										$row[$col->name] = date('d/m/Y H:i', strtotime($row[$col->name]));
										break;
									case 'image':
										if (isset($row['row_imagePath']) && $row['row_imagePath'] != '') {
											$this->images_path = $row['row_imagePath'];
										}
										($row[$col->name] != '') ? $row[$col->name] = '<img src="'.$this->images_path.DS.$row[$col->name].'?w=70&h=70&far=1&bg=FFFFFF" width="70" />' : $row[$col->name] = 'No Image';
										break;
									case 'text':
										($row[$col->name] != '') ? $row[$col->name] = (substr(nl2br(htmlentities($row[$col->name], ENT_QUOTES, 'UTF-8')), 0, 400).'...') : $row[$col->name] = '';
										break;
								}
								echo '<td style="text-align:'.$col->align.'">'.$row[$col->name]."</td>\n";
							}
						} ?>
						<?php if ((isset($elements[0]['rows_buttons']) && count($elements[0]['rows_buttons']) > 0) || $this->delete_option) {
							?>
							<td style="text-align: right">
								<?php
								if (isset($row['rows_buttons']) && is_array($row['rows_buttons'])) {
									foreach ($row['rows_buttons'] as $button) {
										$button->render();
										//echo "<a title=\"\"  class=\"btn " . (($button->getClass() != '') ? $button->getClass() : 'btn-xs btn-info') . "\" href=\"{$button->getAction()}\">{$button->getName()}</a>&nbsp";
									}
								} ?>
								<a title="[$Delete]" class="btn btn-danger btn-xs deleteRow" data-toggle="modal" href="#myModal_<?= $this->name ?>" data-id="<?= $row['id'] ?>"><i class="fa fa-times"></i></a>
							</td>
						<?php
						} ?>
						</tr>
						<?php
					} ?>
				</tbody>
			</table>
		</div>

		<?php if ($this->multiple_deletion) {
						?><button type="submit" name="delete_selected" class="btn btn-info"><i class="fa fa-minus-circle"></i>&nbsp;&nbsp;[$DeleteSelected]</button><?php
					} ?>
	</form>
	<?php Pager::printPager($page, $totalElements, $elements_per_page, $this->getUrl_action().((isset($_GET['query'])) ? '?query='.$_GET['query'] : '')); ?>
<?php
				} else {
					?>
	<div class="col-md-12">
		<div class="alert alert-warning fade in">
			<button data-dismiss="alert" class="close close-sm" type="button">
				<i class="icon_error-circle"></i>
			</button>
			[$norecords]
		</div>
	</div>

<?php
				} ?>




<!-- Modal -->
<div class="modal fade" id="myModal_<?= $this->name ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">[$delete_record]<strong></strong></h4>
            </div>
            <div class="modal-body">
                [$delete_record_question]
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-danger deleteButton" type="submit"> [$Delete]</a>
                <button data-dismiss="modal" class="btn btn-default" type="button">[$Close]</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->

<?php
$params = $this->getUrl_delete_params();
if (count($params)) {
	$url_params = '?';

	foreach ($params as $k => $v) {
		$url_params .= ($k.'='.$v.'&');
	}
	$url_params = rtrim($url_params, '&');
} else {
	$url_params = '';
}
?>

<script>

    $(document).ready(function () {

        var modal = $('#myModal_<?= $this->name ?>');
        var modal_title = $('#myModal_<?= $this->name ?> .modal-title');
        var modal_delete_btn = $('#myModal_<?= $this->name ?> .deleteButton');

        $('.deleteRow').click(function () {

            var id = $(this).data('id');

            modal_title.html("[$delete_record] " + id);
            modal_delete_btn.attr('href', "<?= $this->getUrl_delete() ?>" + id + "<?= $url_params ?>");
        });

    });

</script>


<!-- #### end LIST VIEW #### -->