<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css') ?>
<?php HeadHTML::AddStylesheet($this->template_path.'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css') ?>
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- #### LIST VIEW #### -->

<?php
require_once DOCROOT.LIBS_PATH.'Pager.php';

$readonly = (isset($readonly)) ? $readonly : false;

$elements = $this->getElements();
$action_buttons = $this->getAction_buttons();

$btnNew_url = '#';

$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
$totalElements = $this->getTotalElements();
$elements_per_page = $this->getElements_per_page();

$currency_symbol = (isset($currency_symbol)) ? $currency_symbol : '€';
?>


<?php $fields = $this->getFields(); ?>

<!-- begin col-12 -->

<?php if ($this->renderPanel) {
	?>
	<!-- begin panel -->
	<div class="panel panel-inverse" data-sortable-id="ui-widget-2">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
						class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
						class="fa fa-repeat"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
						class="fa fa-minus"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i
						class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title"><?= $this->getTitle() ?></h4>
		</div>
		<!--        <div class="alert alert-warning fade in">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
					</button>
					Message...
				</div>-->
		<div class="panel-body">
		<?php
} ?>
		<?php if ($this->renderTopBar) {
		?>
			<div class="row">
				<form class="form-inline" role="form" method="get" action="<?= $this->getUrl_action() ?>">
					<?php if ($this->renderFilter) {
			?>
						<div style="float: right;">
							<div class="input-group">
								<?php if (isset($_GET['query']) && $_GET['query'] != '') {
				?>
									<span class="input-group-btn">
										<a href="<?= $this->getUrl_action() ?>" class="btn btn-danger"><i
												class="fa fa-times"></i></a>
									</span>
								<?php
			} ?>
								<input class="form-control" type="text" name="query"
									   value="<?= (isset($_GET['query'])) ? $_GET['query'] : '' ?>"
									   placeholder="[$Filter]"/>
								<span class="input-group-btn">
									<button type="submit" class="btn btn-info"><i class="fa fa-filter"></i>&nbsp;&nbsp;[$Filter]
									</button>
								</span>
							</div>
						</div>
					<?php
		} ?>
					<div style="float: right">
						<?php
						if (count($action_buttons) > 0) {
							foreach ($action_buttons as $button) {
								echo '<a class="btn '.(($button->getClass() != '') ? $button->getClass() : 'btn-info')."\" href=\"{$button->getAction()}\">{$button->getName()}</a>&nbsp";
							}
						} ?>
					</div>
				</form>
				<div class="action_filters" style="display:inline-block; float: right;">
					<?php
					foreach ($this->action_filters as $filter) {
						echo '<div style="float:left;">';
						$filter->render();
						echo '</div>';
						echo "\n";
					} ?>
				</div>
			</div>
			<hr>

			<?php
	}

		if (count($elements) > 0) {
			?>

			<form action="" method="post">
				<table id="<?= $this->name ?>" class="table table-bordered nowrap" width="100%">
					<thead>
						<tr>
							<?php if ($this->multiple_deletion) {
				?>
							<th class="no-sort"><i class="fa fa-check-square-o"></i></th><?php
			} ?>
						<?php
						foreach ($fields as $col) {
							if (!in_array($col->name, array('rows_buttons', 'row_className'))) {
								echo "<th style=\"text-align:$col->align; white-space: nowrap;\">$col->label</th>\n";
							}
						} ?>
						<?php if ((isset($elements[0]['rows_buttons']) && count($elements[0]['rows_buttons']) > 0) || $this->delete_option) {
							?>
							<th style="text-align: right; white-space: nowrap;"><i class="icon_cog"></i> [$Action]
							</th>
						<?php
						} ?>
						</tr>
					</thead>
					<tbody>

						<?php
						foreach ($elements as $row) {
							$className = isset($row['row_className']) ? "class='{$row['row_className']}'" : ''; ?>
							<tr <?= $className ?>>
								<?php if ($this->multiple_deletion) {
								?>
								<td><input type='checkbox' name='elements[]' class="elements"
										   value='<?= $row['id'] ?>'/></td><?php
							} ?>
								<?php
								foreach ($fields as $col) {
									if (!in_array($col->name, array('rows_buttons', 'row_className', 'row_imagePath'))) {
										switch ($col->type) {
											case 'curr':
												($row[$col->name] == 0) ? $row[$col->name] = '-' : ($row[$col->name] = $currency_symbol.' '.$row[$col->name]);
												break;
											case 'bool':
												($row[$col->name] > 0 || $row[$col->name] == true) ? $row[$col->name] = '<i class="fa fa-check-square" style="color:green"></i>' : $row[$col->name] = '<i class="fa fa-square-o" style="color:red"></i>';
												break;
											case 'byte':
												$row[$col->name] = Utils::formatSizeUnits($row[$col->name]);
												break;
											case 'date':
												$row[$col->name] = date('d/m/Y H:i', strtotime($row[$col->name]));
												break;
											case 'image':
												if (isset($row['row_imagePath']) && $row['row_imagePath'] != '') {
													$this->images_path = $row['row_imagePath'];
												}
												($row[$col->name] != '') ? $row[$col->name] = '<img src="'.$this->images_path.DS.$row[$col->name].'?w=70&h=70&far=1&bg=FFFFFF" width="70" />' : $row[$col->name] = 'No Image';
												break;
											case 'text':
												($row[$col->name] != '') ? $row[$col->name] = (substr(nl2br(htmlentities($row[$col->name], ENT_QUOTES, 'UTF-8')), 0, 400).'...') : $row[$col->name] = '';
												break;
										}
										echo '<td style="text-align:'.$col->align.'">'.$row[$col->name]."</td>\n";
									}
								} ?>

							<?php if ((isset($row['rows_buttons']) && count($row['rows_buttons']) > 0) || $this->delete_option) {
									?>
								<td style="text-align: right">
									<?php
									if (isset($row['rows_buttons']) && is_array($row['rows_buttons'])) {
										foreach ($row['rows_buttons'] as $button) {
											$button->render();
											//echo "<a title=\"\"  class=\"btn " . (($button->getClass() != '') ? $button->getClass() : 'btn-xs btn-info') . "\" href=\"{$button->getAction()}\">{$button->getName()}</a>&nbsp";
										}
									} ?>
									<?php if ($this->delete_option) {
										?>
										<a title="[$Delete]" class="btn btn-danger btn-xs deleteRow"
										   data-toggle="modal" href="#myModal_<?= $this->name ?>"
										   data-id="<?= $row['id'] ?>">
											<i class="fa fa-times"></i></a>
									<?php
									} ?>
								</td>

							<?php
								} ?>
							</tr>
							<?php
						} ?>

					</tbody>
				</table>
			</form>
			<div style=" float: right">
				<?php Pager::printPager($page, $totalElements, $elements_per_page, $this->getUrl_action().((isset($_GET['query'])) ? '?query='.$_GET['query'] : '')); ?>
			</div>
			<?php if ($this->multiple_deletion) {
							?>
				<div style="float: left; margin-top: 20px">
					<a data-toggle="modal" href="#myModal2_<?= $this->name ?>" class="btn btn-danger"><i
							class="fa fa-minus-circle"></i>&nbsp;&nbsp;[$DeleteSelected]</a>
				</div>
			<?php
						} ?>

		<?php
		} else {
			?>

			<div class="alert alert-warning fade in">
				<button data-dismiss="alert" class="close close-sm" type="button">
					<i class="fa fa-exclamation-circle"></i>
				</button>
				[$norecords]
			</div>


		<?php
		} ?>
    </div>
	<?php if ($this->renderPanel) {
			?>
	</div>
	<!-- end panel -->
<?php
		} ?>

<!-- Modal -->
<div class="modal fade" id="myModal_<?= $this->name ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                [$delete_record_question]
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-danger deleteButton" type="submit"><i class="fa fa-minus-circle"></i>&nbsp;&nbsp;[$Delete]</a>
                <button data-dismiss="modal" class="btn btn-default" type="button">[$Close]</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->

<!-- Modal -->
<div class="modal fade" id="myModal2_<?= $this->name ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">[$delete_records]<strong></strong></h4>
            </div>
            <div class="modal-body">
                [$delete_records_question]
            </div>
            <div class="modal-footer">
                <form method="post" class="form-inline">
                    <input id="element_id" type="hidden" name="elements" value=""/>
                    <button type="submit" name="delete_selected" class="btn btn-danger"><i
                            class="fa fa-minus-circle"></i>&nbsp;&nbsp;[$Delete]
                    </button>
                    <button data-dismiss="modal" class="btn btn-default" type="button">[$Close]</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- modal -->

<!-- #### end LIST VIEW #### -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/DataTables/media/js/jquery.dataTables.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/js/table-manage-responsive.demo.js'); ?>
<?php HeadHTML::AddJS($this->template_path.'assets/js/apps.min.js'); ?>
<!-- ================== END PAGE LEVEL JS ================== -->

<?php
$params = $this->getUrl_delete_params();
if (count($params)) {
	$url_params = '?';

	foreach ($params as $k => $v) {
		$url_params .= ($k.'='.$v.'&');
	}
	$url_params = rtrim($url_params, '&');
} else {
	$url_params = '';
}
?>

<script>

    if ($('#<?= $this->name ?>').length !== 0) {
        $('#<?= $this->name ?>').DataTable({
            paging: false,
            responsive: true,
            bFilter: false,
            "info": false,
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
            "aaSorting": []
        });
    }

    $(document).ready(function () {

        var modal = $('#myModal_<?= $this->name ?>');
        var modal_title = $('#myModal_<?= $this->name ?> .modal-title');
        var modal_delete_btn = $('#myModal_<?= $this->name ?> .deleteButton');

        $('.deleteRow').click(function () {
            var id = $(this).data('id');
            modal_title.html("[$delete_record] " + id);
            modal_delete_btn.attr('href', "<?= $this->getUrl_delete() ?>" + id + "<?= $url_params ?>");
        });

        $('.elements').click(function () {

            $("#element_id").val('');
            $('#data-table input:checked').each(function () {

                var current_val = $("#element_id").val();

                var $value = current_val;
                if (current_val != '') {
                    $value += ',';
                }

                $value += $(this).attr('value');

                $("#element_id").val($value);
            });
        });
    });

</script>

