<?php $id = (!is_null($this->id)) ? '_'.$this->id : '' ?>
<!-- Modal -->
<div class="modal fade" id="myModal_<?= $this->name ?><?= $id ?>" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= $this->getTitle() ?><strong></strong></h4>
            </div>
            <div class="modal-body" id="record-list">
                <form class="form-bordered">
                    <div class="form-group">
                        <label class="col-md-2 control-label">[$Search]</label>
                        <div class="col-md-10">
                            <input type="text" name="searchList" class="form-control" placeholder="[$Search]"
                                   id="searchList_<?= $this->name ?><?= $id ?>">
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <div class="form-group">
                        <div class="col-md-12" id="responseDiv_<?= $this->name ?><?= $id ?>">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">

				<?php
				foreach ($this->action_buttons as $button) {
					$button->render();
				}
				?>

                <a id="RespondButton_<?= $this->name ?><?= $id ?>" data-dismiss="modal" class="btn btn-success"
                   type="submit">[$Select]</a>
                <button data-dismiss="modal" class="btn btn-default" type="button">[$Close]</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->

<script>
    $(document).ready(function () {

        //Pa kerkuar (Elementet fillestare)
        $.ajax({
            type: '<?= $this->getMethod() ?>',
            url: '<?= $this->getAjxUrl() ?>',
            success: function (html) {
                $('#responseDiv_<?= $this->name ?><?= $id ?>').html(html);
            }
        });


        $("#searchList_<?= $this->name ?><?= $id ?>").keyup(function () {
            var $value = $('#searchList_<?= $this->name ?><?= $id ?>').val();
            $.ajax({
                type: '<?= $this->getMethod() ?>',
                url: '<?= $this->getAjxUrl() ?>',
                data: {query: $value},
                success: function (html) {
                    $('#responseDiv_<?= $this->name ?><?= $id ?>').html(html);
                },
                error: function (e) {
                    //console.log('Error: ' + e);
                }
            });

        });

        $("#RespondButton_<?= $this->name ?><?= $id ?>").click(function () {
            var SelectedId = $("#responseTable_<?= $this->name ?><?= $id ?> input:radio[name='SelectedElement<?= $id ?>']:checked").val();

            var RowSelected = 'tr#id_' + SelectedId + '<?= $id ?>';
            var TableData = $('#responseDiv_<?= $this->name ?><?= $id ?> ' + RowSelected + ' .<?= $this->getDestinationField() ?>');
            $('#<?= $this->textSelector ?>').val(TableData.html());
            $('#<?= $this->idSelector ?>').val(SelectedId);
<?= $this->JSCode ?>;
        });
    });
</script>
<style>
    .responseTable tr {
        background-color: #eee;
        border-top: 1px solid #fff;
    }

    .responseTable tr:hover {
        background-color: #ccc;
    }

    .responseTable th {
        background-color: #fff;
    }

    .responseTable th, #responseTable td {
        padding: 3px 5px;
    }

    .responseTable td:hover {
        cursor: pointer;
    }
</style>

