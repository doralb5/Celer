<?php

class AjaxResponse
{
	private $response;
	private $message;
	private $data;
	private $errors;

	public function __construct()
	{
		$this->message = '';
		$this->data = array();
		$this->errors = array();
	}

	public function setMessage($message)
	{
		$this->message = $message;
	}

	public function setData($data)
	{
		$this->data = $data;
	}

	public function setErrors($errors)
	{
		$this->errors = $errors;
	}

	public function getResponse()
	{
		$this->response = array('message' => $this->message, 'data' => $this->data, 'errors' => $this->errors);
		return $this->response;
	}

	public function printResponse($json = true)
	{
		if ($json) {
			echo json_encode($this->getResponse());
		} else {
			print_r($this->getResponse());
		}
	}
}
