<?php

/**
 * Description of T_brand
 *
 * @author alessandro
 */
class O_brand
{

	//VARIABILI DELL'OGGETTO BRAND
	public $id;
	public $name;
	public $num_prodotti;
	public $logo;
	public $logo_mini;
	//ARRAY CHE CONTIENE LA RELAZIONE TRA VARIABILI DELLA CLASSE E GLI ATTRIBUTI DELLA TABELLA

	private $new = array();

	public function setNew()
	{
		$this->new = array(
			'marca' => $this->name,
			'img_mini' => $this->logo_mini,
			'img_big' => $this->logo
		);
	}

	private static $pre_query = "SELECT m.id, m.marca AS name, count(p.id) AS num_prodotti, m.img_big AS logo, m.img_mini AS logo_mini
                          FROM marche as m
                          LEFT JOIN prodotti as p ON m.id=p.id_marca AND p.visibile='1'";
	private static $attributes = array(
		':id' => 'm.id',
		':name' => 'm.marca',
		':num_prodotti' => 'count(p.id)',
		':logo' => 'm.img_big',
		':logo_mini' => 'm.img_mini'
	);

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function __construct()
	{
	}
}
