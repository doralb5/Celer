<?php

/**
 * Description of T_category
 *
 * @author Antonio Mazza
 */
class O_cambi
{
	public $id;
	public $da;
	public $a;
	public $cambio;
	private static $attributes = array(
		':id' => 'id',
		':da' => 'da',
		':a' => 'a',
		':cambio' => 'cambio'
	);
	private static $pre_query = 'SELECT id, da,a, cambio 
                                 FROM cambi';

	public function __construct($row = '')
	{
		if (is_array($row)) {
			parent::_loadByRow($row);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":id='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}
}
