<?php

/**
 * Description of T_category
 *
 * @author alessandro
 */
class O_category
{
	public $id;
	public $categoria_it;
	public $categoria_en;
	public $categoria_fr;
	public $categoria_pr;
	public $id_top_cat;
	//public $img_mini;
	//public $img_big;
	public $reserved;
	public $ordine;
	public $abilitata;
	public $withphoto;
	public $name;
	public $num_art;
	public $descrizione;
	public $enabled = true;
	public $subcategory = array();
	public $marche = array();
	public $img;
	public $link;
	private static $attributes = array(
		':id' => 'c.id',
		':categoria_it' => 'c.categoria_it',
		':categoria_en' => 'c.categoria_en',
		':categoria_fr' => 'c.categoria_fr',
		':categoria_pr' => 'c.categoria_pr',
		':id_top_cat' => 'c.id_top_cat',
		':img_mini' => 'c.img_mini',
		':img_big' => 'c.img_big',
		':reserved' => 'c.reserved',
		':ordine' => 'c.ordine',
		':abilitata' => 'c.abilitata',
		':withphoto' => 'lenght(c.img_mini)>0',
		':name' => 'name',
		':reserved' => 'c.reserved',
		':ordine' => 'c.ordine',
		':descrizione' => 'c.descrizione'
	);
	private static $pre_query = "SELECT c.id, c.reserved, id_top_cat, c.img_mini, c.img_big, c.ordine, c.abilitata, 
                                    CASE WHEN (c.categoria:user_lang != '') THEN c.categoria:user_lang
                                         ELSE c.categoria:default_lang END
                                         AS name,
                                    CASE WHEN (c.descrizione:user_lang != '') THEN c.descrizione:user_lang
                                         ELSE c.descrizione:default_lang END
                                         AS descrizione,
                                    CASE WHEN (length(c.img_mini)>0) THEN '1'
                                         ELSE '0' END
                                         AS withphoto,
                                    (SELECT GROUP_CONCAT(DISTINCT(p.id_marca) SEPARATOR '|')
                                     FROM prodotti AS p WHERE p.id_categoria=c.id) AS marche     
                                FROM categorie AS c";

	public function __construct($row = '')
	{
		if (is_array($row)) {
			parent::_loadByRow($row);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":id='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function getSeoUrl()
	{
		return UrlUtils::url_slug($this->name) . '-' . $this->id;
	}
}
