<?php

/**
 * Description of T_category
 *
 * @author Antonio Mazza
 */
class O_citta
{
	public $id;
	public $cap;
	public $comune;
	public $prefisso;
	public $provincia;
	public $sigla_prov;
	public $regione;
	public $regione_id;
	private static $attributes = array(
		':id' => 'id',
		':cap' => 'cap',
		':comune' => 'comune',
		':prefisso' => 'prefisso',
		':provincia' => 'provincia',
		':sigla_prov' => 'sigla_prov',
		':regione' => 'regione',
		':regione_id' => 'regione_id'
	);
	private static $pre_query = 'SELECT id, cap,comune, prefisso, provincia, sigla_prov, regione, regione_id 
                                 FROM comuni';

	public function getProvince()
	{
		return $this->em->findByQuery('SELECT DISTINCT provincia, sigla_prov', 'O_citta');
	}

	public function __construct($row = '')
	{
		if (is_array($row)) {
			parent::_loadByRow($row);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":id='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}
}
