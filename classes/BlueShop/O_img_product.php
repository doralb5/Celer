<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of O_img_product
 *
 * @author alessandro
 */
class O_img_product
{

	//VARIABILI DELL'OGGETTO IMG_PRODUCT
	public $id;
	public $id_prodotto;
	public $img_big;
	public $img_mini;
	private static $pre_query = 'SELECT * FROM img_prodotti';
	private static $attributes = array(
		':id' => 'id',
		':id_prodotto' => 'id_prodotto',
		':img_big' => 'img_big',
		':img_mini' => 'img_mini',
		':filename' => 'filename'
	);

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function __construct()
	{
	}
}
