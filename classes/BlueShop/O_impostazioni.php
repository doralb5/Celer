<?php

/**
 * Description of T_category
 *
 * @author Antonio Mazza
 */
class O_impostazioni
{
	public $id;
	public $cognome;
	public $nome;
	public $cap;
	public $indirizzo;
	public $id_comune;
	public $codice_fiscale;
	public $ragione_sociale;
	public $partita_iva;
	public $rea;
	public $telefono;
	public $cellulare;
	public $email;
	public $fax;
	public $url_sito;
	public $mittente;
	public $destinatario;
	public $html_google_map;
	public $oggetto_registrazione;
	public $msg_registrazione;
	public $msg_registrazione_html;
	public $oggetto_conferma_registrazione;
	public $msg_conferma_registrazione;
	public $msg_conferma_registrazione_html;
	public $msg_conferma_password_html;
	public $msg_conferma_ordine;
	public $mostra_prezzi;
	public $id_variante_template;
	public $logo;
	public $piu_venduti;
	public $offerte;
	public $novita;
	public $primo_piano;
	public $num_col_elenco_articoli;
	public $metaTitle;
	public $titolo_sito_it;
	public $titolo_sito_en;
	public $titolo_sito_fr;
	public $titolo_sito_pr;
	public $id_site_piwik;
	public $modo;
	public $manutenzione;
	public $sospeso;
	public $valuta_sito;
	public $feedaty_enabled = '0';
	public $feedaty_merchant_code;
	public $feedaty_api_secret;
	public $lang_default;

	/*
	 *          case '0': $abilitato = 1;   //Attivazione automatica dell'utente
	  break;
	  case '1': $abilitato = 0;   //Attivazione utente con email di conferma
	  break;
	  case '2': $abilitato = 0;   //Attivazione Manuale dell'utente
	  break;
	 */
	public $attivazione_utente;
	public $description_homepage_it;
	public $description_homepage_en;
	public $description_homepage_fr;
	public $description_homepage_pr;
	public $keywords_homepage_it;
	public $keywords_homepage_en;
	public $keywords_homepage_fr;
	public $keywords_homepage_pr;
	public $abilita_img_cat;
	public $num_prod_cat;
	public $msg_home_it;
	public $msg_home_en;
	public $msg_home_fr;
	public $msg_home_pr;
	public $msg_login_it;
	public $msg_login_en;
	public $msg_login_fr;
	public $msg_login_pr;
	public $iva_predefinita;
	public $ricerca_avanzata;
	public $oggetto_richiesta_registrazione;
	public $msg_richiesta_registrazione;
	public $msg_richiesta_registrazione_html;
	public $msg_richiesta_password_html;
	public $watermark;
	private static $attributes = array(
		':id' => 'id',
		':cognome' => 'conome',
		':nome' => 'nome',
		':cap' => 'cap',
		':indirizzo' => 'indirizzo',
		':id_comune' => 'id_comune',
		':codice_fiscale' => 'codice_fiscale',
		':ragione_sociale' => 'ragione_sociale',
		':partita_iva' => 'partita_iva',
		':rea' => 'rea',
		':telefono' => 'telefono',
		':cellulare' => 'cellulare',
		':email' => 'email',
		':fax' => 'fax',
		':url_sito' => 'url_sito',
		':mittente' => 'mittente',
		':destinatario' => 'destinatario',
		':html_google_map' => 'html_google_map',
		':oggetto_registrazione' => 'oggetto_registrazione',
		':msg_registrazione' => 'msg_registrazione',
		':msg_registrazione_html' => 'msg_registrazione_html',
		':oggetto_conferma_registrazione' => 'oggetto_conferma_registrazione',
		':msg_conferma_registrazione' => 'msg_conferma_registrazione_html',
		':msg_conferma_registrazione_html' => 'msg_comferma_registrazone_html',
		':msg_conferma_ordine' => 'msg_conferma_ordine',
		':mostra_prezzi' => 'mostra_prezzi',
		':id_variante_template' => 'id_variante_template',
		':logo' => 'logo',
		':piu_venduti' => 'piu_venduti',
		':offerte' => 'offerte',
		':novita' => 'novita',
		':primo_piano' => 'primo_piano',
		':num_col_elenco_articoli' => 'num_col_elenco_articoli',
		':metaTitle' => 'metaTitle',
		':titolo_sito_it' => 'titolo_sito_it',
		':titolo_sito_en' => 'titolo_sito_en',
		':titolo_sito_fr' => 'titolo_sito_fr',
		':titolo_sito_pr' => 'titlo_sito_pr',
		':id_site_piwik' => 'id_site_piwik',
		':modo' => 'modo',
		':manutenzione' => 'manutenzione',
		':sospeso' => 'sospeso',
		':attivazione_utente' => 'attivazione_utente',
		':description_homepage_it' => 'description_homepage_it',
		':description_homepage_en' => 'description_homepage_en',
		':description_homepage_fr' => 'description_homepage_fr',
		':description_homepage_pr' => 'description_homepage_pr',
		':keywords_homepage_it' => 'keywords_homepage_it',
		':keywords_homepage_en' => 'keywords_homepage_en',
		':keywords_homepage_fr' => 'keywords_homepage_fr',
		':keywords_homepage_pr' => 'keywords_homepajge_pr',
		':abilita_img_cat' => 'abilita_img_cat',
		':num_prod_cat' => 'num_prod_cat',
		':msg_home_it' => 'msg_home_it',
		':msg_home_en' => 'msg_home_en',
		':msg_home_fr' => 'msg_home_fr',
		':msg_home_pr' => 'msg_home_pr',
		':msg_login_it' => 'msg_login_it',
		':msg_login_en' => 'msg_login_en',
		':msg_login_fr' => 'msg_login_fr',
		':msg_login_pr' => 'msg_login_pr',
		':iva_predefinita' => 'iva_predefinita',
		':ricerca_avanzata' => 'ricerca_avanzata',
		':valuta_sito' => 'valuta_sito',
		':oggetto_richiesta_registrazione' => 'oggetto_richiesta_registrazione',
		':msg_richiesta_registrazione' => 'msg_richiesta_registrazione',
		':msg_richiesta_password_html' => 'msg_richiesta_password_html',
		':msg_richiesta_registrazione_html' => 'msg_richiesta_registrazione_html',
		':lang_default' => ' (SELECT campo_lingua FROM tab_lingue WHERE preferita = 1 LIMIT 1) AS lang_default '
	);
	private static $pre_query = 'SELECT *, (SELECT campo_lingua FROM tab_lingue WHERE preferita = 1 LIMIT 1) AS lang_default FROM impostazioni';

	public function __construct($row = '')
	{
		if (is_array($row)) {
			parent::_loadByRow($row);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":id='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}
}
