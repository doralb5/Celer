<?php

class O_order
{
	private $em;
	public $data_creazione;
	public $id;
	public $numero;
	public $spese_spedizione;
	public $cognome;
	public $nome;
	public $id_cliente;
	public $codice_cl;
	public $codice_fiscale_cl;
	public $ragione_sociale;
	public $partita_iva_cl;
	public $indirizzo_cl;
	public $cap_cl;
	public $comune_cl;
	public $provincia_cl;
	public $nazione_cl;
	public $nazione_dest;
	public $indirizzo_dest;
	public $cap_dest;
	public $provincia_dest;
	public $comune_dest;
	public $data;
	public $metodo_spedizione;
	public $id_stato;
	public $stato;
	public $costo_aggiuntivo = 0;
	public $coupon_code;
	public $discount = 0;
	public $pagato;
	public $modopag_id;
	public $modopag;
	public $val_pagamento;
	public $email;
	public $telefono;
	public $cellulare;
	public $destinatario;
	public $aliquota_iva;
	public $numero_articoli;
	public $imponibile;
	public $imponibile_ivato;
	public $imposta;
	public $totale;
	public $regione_spedizione;
	public $prezzo_variabile;
	public $peso_totale;
	public $id_metodo_spedizione;
	public $id_comune_residenza_cl;
	public $id_comune_dest;
	public $note;
	public $id_sessione;
	public $tracking_number;
	public $details = array();
	public $payment_methods;
	public $allPaymentsAccepted;
	public $shipment_method;
	public $TABLE_NAME = 'ordini';
	public $new = array();
	private static $pre_query = "SELECT o.id,o.id_comune_residenza_cl as id_comune_residenza_cl, o.tracking_number as tracking_number, o.id_sessione as id_sessione, o.note as note, o.id_comune_dest AS id_comune_dest, c.id AS id_metodo_spedizione ,o.numero, o.spese_spedizione, o.cognome_cl AS cognome, o.nome_cl AS nome, o.id_cliente, o.codice_cl, o.codice_fiscale_cl, o.ragione_sociale_cl AS ragione_sociale, o.partita_iva_cl,o.indirizzo_cl,cap_cl,provincia_cl,comune_cl,nazione_cl,nazione_dest,indirizzo_dest,cap_dest,
                                        (CASE WHEN (provincia_dest IS NOT NULL) THEN provincia_dest ELSE provincia_cl END) AS provincia_dest,
                                        comune_dest,o.data_creazione AS data,c.descrizione AS metodo_spedizione,c.mod_pagamento AS allPaymentsAccepted, id_stato,stati_ordine.descrizione AS stato, o.costo_aggiuntivo,pagato,
                                        pag.id AS modopag_id, pag.descrizione AS modopag, pag.valore AS val_pagamento, cl.email AS email, cl.telefono, cl.cellulare, o.destinatario,aliquota_iva,
                                        (SELECT SUM(do.quantita_ar) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id) AS numero_articoli,
                                        (SELECT SUM(do.prezzo_ar*quantita_ar) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id ) AS imponibile,
                                        (SELECT SUM(do.prezzo_ar*quantita_ar) + SUM(((do.prezzo_ar*quantita_ar*do.iva_ar)/100)) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id ) AS imponibile_ivato,
                                        (SELECT SUM(((do.prezzo_ar*quantita_ar*do.iva_ar)/100)) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id) + (o.spese_spedizione * o.aliquota_iva)/100 + (o.costo_aggiuntivo * o.aliquota_iva)/100 AS imposta,
                                        ROUND(
                                              ROUND( (SELECT SUM(do.prezzo_ar*quantita_ar) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id ), 2)
                                              + ROUND( (SELECT SUM((do.prezzo_ar*quantita_ar*do.iva_ar)/100) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id), 2)
                                              + ROUND( (o.spese_spedizione + ( (o.spese_spedizione * o.aliquota_iva) /100)), 2)
                                              + ROUND( (o.costo_aggiuntivo + ( (o.costo_aggiuntivo * o.aliquota_iva) /100)), 2)
                                              - o.discount
                                              ,2) AS totale,

                                        (SELECT ROUND(SUM(peso_ar*quantita_ar),2)
                                                FROM dettagli_ordine
                                               INNER JOIN ordini ON dettagli_ordine.id_ordine=ordini.id
                                               WHERE ordini.id=o.id) AS peso_totale,
                                               '' AS regione_spedizione,

                                        CASE WHEN ((SELECT prezzo_variabile FROM dettagli_ordine WHERE id_ordine = o.id AND prezzo_variabile = 1 LIMIT 1) = 1) THEN 1 ELSE 0 END AS prezzo_variabile,
                                        o.data_creazione,
                                        o.discount,
                                        o.coupon_code
                                 FROM ordini AS o
                                 LEFT JOIN consegne AS c ON o.id_metodo_spedizione=c.id
                                 LEFT JOIN registrati AS cl ON cl.id = id_cliente
                                 INNER JOIN stati_ordine ON o.id_stato=stati_ordine.id
                                 LEFT JOIN modalita_pagamento AS pag ON pag.id = o.id_modalita_pagamento";
	private static $attributes = array(
		':id' => 'o.id',
		':numero' => 'o.numero',
		':spese_spedizione' => 'o.spese_spedizione',
		':cognome' => 'o.cognome_cl',
		':nome' => 'o.nome_cl',
		':id_cliente' => 'o.id_cliente',
		':codice_cl' => 'o.codice_cl',
		':codice_fiscale_cl' => 'o.codice_fiscale_cl',
		':ragione_sociale' => 'o.ragione_sociale_cl',
		':partita_iva_cl' => 'o.partita_iva_cl',
		':indirizzo_cl' => 'o.indirizzo_cl',
		':data_creazione' => 'o.data_creazione',
		':cap_cl' => 'cap_cl',
		':provincia_cl' => 'provincia_cl',
		':comune_cl' => 'comune_cl',
		':nazione_cl' => 'nazione_cl',
		':nazione_dest' => 'nazione_dest',
		':indirizzo_dest' => 'indirizzo_dest',
		':cap_dest' => 'cap_dest',
		':provincia_dest' => '(CASE WHEN (provincia_dest IS NOT NULL) THEN provincia_dest ELSE provincia_cl END)',
		':comune_dest' => 'comune_dest',
		':data' => 'o.data_creazione',
		':id_metodo_spedizione' => 'o.id_metodo_spedizione',
		':metodo_spedizione' => 'c.descrizione',
		':id_stato' => 'id_stato',
		':stato' => 'stati_ordine.descrizione',
		':costo_aggiuntivo' => 'o.costo_aggiuntivo',
		':discount' => 'o.discount',
		':coupon_code' => 'o.coupon_code',
		':pagato' => 'pagato',
		':modopag_id' => 'pag.id',
		':val_pagamento' => 'pag.valore',
		':email' => 'cl.mail',
		':telefoto' => 'cl.telefono',
		':celullare' => 'cl.cellulare',
		':destinatario' => 'o.destinatario',
		':aliquota_iva' => 'aliquota_iva',
		':numero_articoli' => '(SELECT SUM(do.quantita_ar) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id)',
		':imponibile' => '(SELECT SUM(do.prezzo_ar*quantita_ar) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id )',
		':imponibile_ivato' => '(SELECT SUM(do.prezzo_ar*quantita_ar) + SUM(((do.prezzo_ar*quantita_ar*do.iva_ar)/100)) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id )',
		':imposta' => '(SELECT SUM(((do.prezzo_ar*quantita_ar*do.iva_ar)/100)) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id) + (o.spese_spedizione * o.aliquota_iva)/100 + (o.costo_aggiuntivo * o.aliquota_iva)/100',
		':totale' => 'ROUND(
                                                            (SELECT SUM(do.prezzo_ar*quantita_ar) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id )
                                                            +(SELECT SUM((do.prezzo_ar*quantita_ar*do.iva_ar)/100) FROM dettagli_ordine AS do WHERE do.id_ordine = o.id)
                                                            + o.spese_spedizione
                                                            +( (o.spese_spedizione * o.aliquota_iva) /100)
                                                            + o.costo_aggiuntivo
                                                            +( (o.costo_aggiuntivo * o.aliquota_iva) /100)
                                                            - o.discount
                                                          ,2)',
		':peso_totale' => '(SELECT ROUND(SUM(peso_ar*quantita_ar),2)
                                                            FROM dettagli_ordine
                                                           INNER JOIN ordini ON dettagli_ordine.id_ordine=ordini.id
                                                           WHERE ordini.id=o.id) ',
		':prezzo_variabile' => 'CASE WHEN ((SELECT prezzo_variabile FROM dettagli_ordine WHERE id_ordine = o.id AND prezzo_variabile = 1 LIMIT 1) = 1) THEN 1 ELSE 0 END'
	);

	public function __construct()
	{
		$this->em = ORM::getInstance();
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function setNew()
	{
		$this->new = array(
			'numero' => $this->numero,
			'data_creazione' => $this->data,
			'id_stato' => $this->id_stato,
			'id_modalita_pagamento' => $this->modopag_id,
			'pagato' => $this->pagato,
			'id_metodo_spedizione' => $this->id_metodo_spedizione,
			'spese_spedizione' => $this->spese_spedizione,
			'id_cliente' => $this->id_cliente,
			'id_comune_residenza_cl' => $this->id_comune_residenza_cl,
			'codice_cl' => $this->codice_cl,
			'cognome_cl' => $this->cognome,
			'nome_cl' => $this->nome,
			'codice_fiscale_cl' => $this->codice_fiscale_cl,
			'ragione_sociale_cl' => $this->ragione_sociale,
			'partita_iva_cl' => $this->partita_iva_cl,
			'indirizzo_cl' => $this->indirizzo_cl,
			'comune_cl' => $this->comune_cl,
			'cap_cl' => $this->cap_cl,
			'cap_dest' => $this->cap_dest,
			'nazione_cl' => $this->nazione_cl,
			'provincia_cl' => $this->provincia_cl,
			'id_comune_dest' => $this->id_comune_dest,
			'indirizzo_dest' => $this->indirizzo_dest,
			'destinatario' => $this->destinatario,
			'comune_dest' => $this->comune_dest,
			'provincia_dest' => $this->provincia_dest,
			'nazione_dest' => $this->nazione_dest,
			'note' => $this->note,
			'id_sessione' => $this->id_sessione,
			'costo_aggiuntivo' => $this->costo_aggiuntivo,
			'discount' => $this->discount,
			'coupon_code' => $this->coupon_code,
			'aliquota_iva' => $this->aliquota_iva,
			'tracking_number' => $this->tracking_number,
			'data_creazione' => $this->data_creazione
		);
	}

	/**
	 * Ritorna i dettagli dell'ordine
	 * @return O_order_details
	 */
	public function getDetails()
	{
		$this->em->cleanAll();
		$this->em->setWhere("  :id_ordine='$this->id' ");
		$this->em->setRepository('O_order_details');
		$this->details = $this->em->find();
		return $this->details;
	}

	/**
	 * @return O_payment_method
	 */
	public function getPayment_methods()
	{
		$this->em->cleanAll();

		$id_shipment = $this->id_metodo_spedizione;

		if ($this->allPaymentsAccepted == '0') {
			$query = "SELECT mpag.* FROM modalita_pagamento mpag
                        INNER JOIN mod_pagamento_consegne cpag ON cpag.id_mod_pagamento = mpag.id
                        INNER JOIN consegne c ON c.id = cpag.id_consegna
                    WHERE attiva = '1' AND c.mod_pagamento = '0' AND id_consegna = '$id_shipment' ORDER BY mpag.id";
			$this->payment_methods = $this->em->findByQuery($query, 'O_payment_method');
		} else {
			$this->em->setWhere(" attiva='1'");
			$this->em->setOrder_by(' :id');

			$this->em->setRepository('O_payment_method');
			$this->payment_methods = $this->em->find();
		}

		return $this->payment_methods;
	}

	/**
	 * Return payment_method se l'id del metodo di pagamento non è settato nell'oggeto restituisce il metodo di pagamento dell'ordine
	 * @return O_payment_method
	 */
	public function getPayment_method($id = '')
	{
		$this->em->cleanAll();

		if ($id == '' && $this->modopag_id != '' && !is_null($this->modopag_id)) {
			$this->em->setWhere(" attiva='1' AND :id='$this->modopag_id'");
		} else {
			$this->em->setWhere(" attiva='1' AND :id='$id'");
		}
		$this->em->setLimit(1);
		$this->em->setRepository('O_payment_method');
		$this->payment_methods = $this->em->find();
		return $this->payment_methods;
	}

	/**
	 * Setta l'oggetto e fa un update nella tabella col metodo di pagamento scelto
	 * @param int $id => id nel tipo di spedizione scelta
	 * return false | num. righe affette
	 */
	public function setPayment_methods($id)
	{
		$trovato = false;
		$payment_method;
		for ($i = 0; $i < count($this->payment_methods) && !$trovato; ++$i) {
			if ($this->payment_methods[$i]->id == $id) {
				$payment_method = $this->payment_methods[$i];
				$trovato = true;
			}
		}
		$this->modopag_id = $payment_method->id;
		$this->costo_aggiuntivo = $payment_method->costo_aggiuntivo;
		$this->setNew();
		return $this->em->save($this); // deve passare questo oggetto
	}

	/**
	 * Salva la destinazione dell'ordine sia sulla tabella e nell'oggetto
	 * @param array $dest_array ['destinatario'=>'','nazione_dest'=>'' , 'comune_dest'=>'', 'provincia_dest'=>'', 'cap_dest'=>'', 'indirizzo_dest'=>'' ]
	 * return false | num. righe affette
	 */
	public function setDestination()
	{
		$this->setNew();
		$this->em->cleanAll();
		return $this->em->save($this); // deve passare questo oggetto
	}

	/**
	 * Ritorna un array di oggetti per la selezione del metodo di pagamento
	 * @return O_shipment_method
	 */
	public function getShipment_method()
	{
		$country = '';

		$qSelCountry = 'SELECT country FROM impostazioni LIMIT 1';
		$res = $this->em->findByQuery($qSelCountry);
		if (count($res)) {
			$country = $res[0]['country'];
		}
		if ($country == '' || is_null($country)) {
			$country = 'Italia';
		}

		if ($this->nazione_dest == $country) {
			$sql_select = "SELECT c.id AS id, c.descrizione, c.mod_pagamento AS allPayments, c.url_tracking, zonesped.descrizione AS zona, regione, soglia_promozione,
                                MIN((CASE WHEN(a IS NULL) THEN(ROUND( (prezzo*CEILING('$this->peso_totale'/scaglione)),2 )) ELSE ROUND(prezzo,2) END)) AS prezzo,
                                CASE WHEN (soglia_promozione > '0') THEN(CASE WHEN('$this->imponibile' >= soglia_promozione)THEN '1' ELSE '0' END) ELSE '0' END AS gratis
                                FROM tariffe_spedizioni AS ts INNER JOIN consegne AS c ON ts.id_consegna = c.id
                                INNER JOIN zone_spedizioni AS zonesped ON zonesped.id = ts.id_zona 
                                LEFT JOIN zone_naz_reg AS zreg ON zreg.id_zona = zonesped.id ";
			$sql_where = " WHERE da <= ROUND('$this->peso_totale',2) AND (CASE WHEN(a IS NULL) THEN 999999999999999 ELSE a END) >= ROUND('$this->peso_totale',2) AND nazionale=0 ";
			$sql_where .= " AND
                            ( 
                                (CASE WHEN otherwise = '0' THEN id_naz_reg ELSE 0 END) = (SELECT DISTINCT(regione_id) FROM comuni WHERE sigla_prov = '$this->provincia_dest')
                                OR
                                (CASE WHEN otherwise = '1' AND (SELECT COUNT(znreg.id) FROM zone_naz_reg AS znreg INNER JOIN zone_spedizioni AS zsp ON zsp.id = znreg.id_zona WHERE nazionale = '0' AND znreg.id_naz_reg = (SELECT DISTINCT(regione_id) FROM comuni WHERE sigla_prov = '$this->provincia_dest')) = 0 THEN 1 ELSE 0 END) = 1
                            )";
			$sql_end = ' GROUP BY c.id ORDER BY prezzo;';
			$q_tariffe = $sql_select . $sql_where . ' AND c.abilitato = 1' . $sql_end;
		} else {
			$sql_select = "SELECT c.id AS id, c.descrizione, c.mod_pagamento AS allPayments, zonesped.descrizione AS zona, regione, soglia_promozione,
                                MIN((CASE WHEN(a IS NULL) THEN(ROUND( (prezzo*CEILING('$this->peso_totale'/scaglione)),2 )) ELSE ROUND(prezzo,2) END)) AS prezzo,
                                CASE WHEN (soglia_promozione > '0') THEN(CASE WHEN('$this->imponibile' >= soglia_promozione)THEN '1' ELSE '0' END) ELSE '0' END AS gratis
                                FROM tariffe_spedizioni AS ts INNER JOIN consegne AS c ON ts.id_consegna = c.id
                                INNER JOIN zone_spedizioni AS zonesped ON zonesped.id = ts.id_zona
                                LEFT JOIN zone_naz_reg zonenaz ON zonesped.id = zonenaz.id_zona
                                LEFT JOIN nazioni naz ON zonenaz.id_naz_reg = naz.id";
			$sql_where = " WHERE da <= ROUND('$this->peso_totale',2) AND (CASE WHEN(a IS NULL) THEN 999999999999999 ELSE a END) >= ROUND('$this->peso_totale',2) AND nazionale=1";
			$sql_where .= " AND
                            ( 
                                (CASE WHEN otherwise = '0' THEN naz.stato ELSE 0 END) = '$this->nazione_dest'
                                OR
                                (otherwise = '1' AND (SELECT CASE WHEN
                                                                (
                                                                '$this->nazione_dest' NOT IN (SELECT stato FROM zone_naz_reg AS znr INNER JOIN nazioni AS naz ON naz.id = znr.id_naz_reg)
                                                                )
                                                            THEN 1
                                                            ELSE 0
                                                            END))
                            )";
			$sql_end = ' GROUP BY c.id ORDER BY prezzo;';
			$q_tariffe = $sql_select . $sql_where . ' AND c.abilitato = 1' . $sql_end;
		}

		$this->shipment_method = $this->em->findByQuery($q_tariffe, 'O_shipment_method');
		return $this->shipment_method;
	}

	/**
	 * Setta nell'oggetto il metodo di spedizione scelta e da un update nella tabella ordini
	 * @param int $id id del metodo di spedizione scelto
	 * @return false || num row affect
	 */
	public function setShipment_method($id)
	{
		$this->getShipment_method();
		$trovato = false;
		$shipment_method;
		for ($i = 0; $i < count($this->shipment_method) && !$trovato; ++$i) {
			if ($this->shipment_method[$i]->id == $id) {
				$shipment_method = $this->shipment_method[$i];
				$trovato = true;
			}
		}
		if ($trovato) {
			$this->id_metodo_spedizione = $shipment_method->id;
			if ($shipment_method->gratis == 1) {
				$this->spese_spedizione = 0;
			} else {
				$this->spese_spedizione = $shipment_method->prezzo;
			}
			$this->metodo_spedizione = $shipment_method->descrizione;
			$this->allPaymentsAccepted = $shipment_method->allPayments;
		} else {
			$this->id_metodo_spedizione = 0;
			$this->spese_spedizione = 0;
			$this->metodo_spedizione = '';
			$this->allPaymentsAccepted = 0;
		}
		$this->setNew();
		return $this->em->save($this);
	}

	public function setPaid()
	{
		$this->pagato = '1';
		$this->setNew();
		$this->em->save($this);
		$this->em->updatesObj($this);
	}

	/**
	 * Aggiorna l'oggetto ordine
	 * @return boolean
	 */
	public function update()
	{
		/* $this->em->cleanAll();
		  $this->em->setWhere(" :id='{$this->id}'");
		  $this->em->setLimit(1); */
		$outcome = $this->em->updatesObj($this);
		if ($outcome) {
			$this->getDetails();
			return true;
		}
		return false;
	}

	public function addDetail($detail)
	{
		$detail->id_ordine = $this->id;
		$detail->setNew();
		return $this->em->save($detail);
	}
}
