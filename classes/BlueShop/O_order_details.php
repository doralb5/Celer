<?php

class O_order_details
{
	public $TABLE_NAME = 'dettagli_ordine';
	public $id;
	public $id_prodotto;
	public $id_ordine;
	public $codice;
	public $titolo;
	public $unita_misura_ar;
	public $quantita;
	public $peso_ar;
	public $prezzo;
	public $iva_ar;
	public $totale;
	public $prezzo_ivato;
	public $totale_ivato;
	public $withphoto;
	public $prezzo_variabile;
	public $prezzi_ivati;
	public $img_big;
	public $img_mini;
	public $id_variante;
	public $id_img;
	public $new = array();

	/* private static $pre_query = "SELECT do.id,do.id_variante as id_variante, do.id_ordine as id_ordine, do.id_articolo AS id_prodotto, do.codice_ar AS codice, do.titolo_ar AS titolo, do.unita_misura_ar, do.quantita_ar AS quantita, do.peso_ar, do.prezzo_ar AS prezzo, do.iva_ar, (do.prezzo_ar*do.quantita_ar) AS totale,
	  ROUND( do.prezzo_ar + (do.prezzo_ar * do.iva_ar / 100) ,2) AS prezzo_ivato,
	  ROUND( (do.prezzo_ar*do.quantita_ar) + ((do.prezzo_ar*do.quantita_ar) * do.iva_ar / 100) ,2) AS totale_ivato,
	  CASE WHEN (img.img_mini IS NULL) THEN 0 ELSE 1 END AS withphoto,
	  CASE WHEN ((SELECT prezzo_variabile FROM dettagli_ordine WHERE id_ordine = o.id AND prezzo_variabile = 1 LIMIT 1) = 1) THEN 1 ELSE 0 END AS prezzo_variabile,

	  (SELECT i.img_big FROM img_prodotti as i WHERE i.id_prodotto=p.id LIMIT 1) as img_big,
	  (SELECT i.img_mini FROM img_prodotti as i WHERE i.id_prodotto=p.id LIMIT 1) as img_mini,

	  (SELECT prezzi_ivati FROM listini WHERE campo_prodotto='listino:nlistino') as prezzi_ivati

	  FROM dettagli_ordine AS do
	  INNER JOIN ordini AS o ON do.id_ordine=o.id
	  LEFT JOIN img_prodotti AS img ON img.id_prodotto=do.id_articolo"; */

	/*
	  private static $pre_query = "SELECT do.id,do.id_variante as id_variante, do.id_ordine as id_ordine, do.id_articolo AS id_prodotto, do.codice_ar AS codice, do.titolo_ar AS titolo, do.unita_misura_ar, do.quantita_ar AS quantita, do.peso_ar, do.prezzo_ar AS prezzo, do.iva_ar, (do.prezzo_ar*do.quantita_ar) AS totale,
	  ROUND( do.prezzo_ar + (do.prezzo_ar * do.iva_ar / 100) ,2) AS prezzo_ivato,
	  ROUND( (do.prezzo_ar*do.quantita_ar) + ((do.prezzo_ar*do.quantita_ar) * do.iva_ar / 100) ,2) AS totale_ivato,

	  CASE WHEN (img.img_mini IS NULL) THEN 0 ELSE 1 END AS withphoto,
	  CASE WHEN (do.prezzo_variabile = 1)
	  THEN 1
	  ELSE 0 END AS prezzo_variabile ,
	  (SELECT i.img_big FROM img_prodotti as i WHERE i.id_prodotto=do.id_articolo LIMIT 1) as img_big,
	  (SELECT i.img_mini FROM img_prodotti as i WHERE i.id_prodotto=do.id_articolo LIMIT 1) as img_mini,
	  (SELECT i.id AS id_img FROM img_prodotti as i WHERE i.id_prodotto=do.id_articolo LIMIT 1) as id_img,
	  (SELECT prezzi_ivati FROM listini WHERE campo_prodotto='listino1') as prezzi_ivati
	  FROM dettagli_ordine AS do
	  LEFT JOIN img_prodotti AS img ON img.id_prodotto=do.id_articolo";
	 */
	private static $pre_query = "SELECT do.id,do.id_variante as id_variante, do.id_ordine as id_ordine, do.id_articolo AS id_prodotto, do.codice_ar AS codice, do.titolo_ar AS titolo, do.unita_misura_ar, do.quantita_ar AS quantita, do.peso_ar, do.prezzo_ar AS prezzo, do.iva_ar, (do.prezzo_ar*do.quantita_ar) AS totale,
                                                                                            ROUND( do.prezzo_ar + (do.prezzo_ar * do.iva_ar / 100) ,2) AS prezzo_ivato,
                                                                                            ROUND( (do.prezzo_ar*do.quantita_ar) + ((do.prezzo_ar*do.quantita_ar) * do.iva_ar / 100) ,2) AS totale_ivato,

                                                                                         1 AS withphoto,
                                                                                         CASE WHEN (do.prezzo_variabile = 1) 
                                                                                            THEN 1
                                                                                         ELSE 0 END AS prezzo_variabile ,
                                                                                        (SELECT i.id AS id_img FROM img_prodotti as i WHERE i.id_prodotto=do.id_articolo LIMIT 1) as id_img,
                                                                                        (SELECT prezzi_ivati FROM listini WHERE campo_prodotto='listino:nlistino') as prezzi_ivati                                   
                                                                                        FROM dettagli_ordine AS do ";
	private static $attributes = array(
		':id' => 'do.id',
		':id_prodotto' => 'do.id_articolo',
		':id_ordine' => ' do.id_ordine',
		':codice' => 'do.codice_ar',
		':titolo' => 'do.titolo_ar',
		':unita_misura_ar' => 'do.unita_misura_ar',
		':quantita' => 'do.quantita_ar',
		':peso_ar' => 'do.peso_ar',
		':prezzo' => 'do.prezzo_ar',
		':iva_ar' => 'do.iva_ar',
		':totale' => '(do.prezzo_ar*do.quantita_ar)',
		':prezzo_ivato' => 'ROUND( do.prezzo_ar + (do.prezzo_ar * do.iva_ar / 100) ,2)',
		':totale_ivato' => 'ROUND( (do.prezzo_ar*do.quantita_ar) + ((do.prezzo_ar*do.quantita_ar) * do.iva_ar / 100) ,2)',
		':withphoto' => 'CASE WHEN (img.img_mini IS NULL) THEN 0 ELSE 1 END',
		':prezzo_variabile' => 'CASE WHEN ((SELECT prezzo_variabile FROM dettagli_ordine WHERE id_ordine = o.id AND prezzo_variabile = 1 LIMIT 1) = 1) THEN 1 ELSE 0 END',
		':prezzi_ivati' => "(SELECT prezzi_ivati FROM listini WHERE campo_prodotto='listino:nlistino')",
		':img_big' => '(SELECT i.img_big FROM img_prodotti as i WHERE i.id_prodotto=p.id LIMIT 1)',
		':img_mini' => '(SELECT i.img_mini FROM img_prodotti as i WHERE i.id_prodotto=p.id LIMIT 1)',
		':id_img' => '(SELECT i.id AS id_img FROM img_prodotti as i WHERE i.id_prodotto=p.id LIMIT 1)'
	);

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function __construct()
	{
	}

	public function setNew()
	{
		$this->new = array(
			'id_ordine' => $this->id_ordine,
			'id_articolo' => $this->id_prodotto,
			'id_variante' => $this->id_variante,
			'codice_ar' => $this->codice,
			'titolo_ar' => $this->titolo,
			'unita_misura_ar' => $this->unita_misura_ar,
			'quantita_ar' => $this->quantita,
			'peso_ar' => $this->peso_ar,
			'prezzo_ar' => $this->prezzo,
			'iva_ar' => $this->iva_ar,
			'prezzo_variabile' => $this->prezzo_variabile
		);
	}
}
