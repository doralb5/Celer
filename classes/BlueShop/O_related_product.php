<?php

class O_related_product
{
	public $id;
	public $titolo;
	public $descrizione;
	public $id_img;
	public $id_categoria;
	public $sorting = 100;
	public $new = array();
	private static $pre_query = "SELECT prodotti.id,
                                    CASE WHEN (prodotti.titolo:user_lang!='') THEN prodotti.titolo:user_lang ELSE prodotti.titolo:default_lang END AS titolo,
                                    CASE WHEN (prodotti.descrizione_html:user_lang!='') THEN prodotti.descrizione_html:user_lang ELSE prodotti.descrizione_html:default_lang END AS descrizione,

                                    (SELECT id FROM img_prodotti WHERE img_prodotti.id_prodotto = prodotti.id LIMIT 1) as id_img,
                                    prodotti.id_categoria, prodotti.sorting
                                FROM prodotti_correlati
                                INNER JOIN prodotti ON prodotti.id = prodotti_correlati.id_parent";
	private static $attributes = array(
		':id' => 'prodotti.id',
		':titolo' => 'titolo',
		':descrizione' => 'descrizione',
		':img_prodotto' => 'img_prodotto',
		':id_prodotto' => 'prodotti_correlati.id_prodotto',
		':id_categoria' => 'prodotti.id_categoria',
		':sorting' => 'prodotti.sorting'
	);

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function __construct()
	{
	}
}
