<?php

class O_shipment_method
{
	public $id;
	public $descrizione;
	public $zona;
	public $regione;
	public $prezzo;
	public $url_tracking;
	public $gratis;
	public $soglia_promozione;
	public $allPayments;
	private static $pre_query = ' ';
	private static $attributes = array(
		':id' => 'c.id',
		':descrizione' => 'c.descrizione',
		':zona' => 'zonesped.descrizione',
		':regione' => 'regione',
		':prezzo' => '',
		':gratis' => '',
		':soglia_promozione' => '0',
		':url_tracking' => '',
		':allPayments' => 'mod_pagamento'
	);

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function __construct()
	{
	}
}
