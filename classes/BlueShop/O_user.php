<?php

class O_user
{
	private $em;
	public $id;
	public $codice;
	public $data_registrazione;
	public $cognome;
	public $nome;
	public $data_nascita;
	public $sesso;
	public $nazione;
	public $provincia;
	public $comune;
	public $cap;
	public $indirizzo;
	public $id_comune_nascita;
	public $id_comune_residenza;
	public $codice_fiscale;
	public $ragione_sociale;
	public $partita_iva;
	public $telefono;
	public $cellulare;
	public $email;
	public $id_listino;
	public $abilitato;
	public $username;
	public $password;
	public $codice_attivazione;
	public $id_categoria;
	public $fax;
	public $web;
	public $newsletter;
	public $data_iscrizione_newsletter;
	public $sigla_prov_residenza;
	public $prov_residenza;
	public $comune_residenza;
	public $sigla_prov_nascita;
	public $prov_nascita;
	public $comune_nascita;
	public $categoria_utente;
	public $business;
	public $UE;
	public $titolo;
	public $TABLE_NAME = 'registrati';
	public $new = array();

	/* private static $pre_query = "SELECT u.*, u.nazione, u.provincia AS sigla_prov_residenza,u.provincia AS prov_residenza,u.comune AS comune_residenza,
	  c2.sigla_prov AS sigla_prov_nascita,c2.provincia AS prov_nascita,c2.comune AS comune_nascita,
	  catreg.descrizione AS categoria_utente
	  FROM registrati AS u
	  LEFT JOIN categorie_registrati AS catreg ON catreg.id = u.id_categoria
	  LEFT JOIN bshop.comuni c1 ON u.id_comune_residenza=c1.id
	  LEFT JOIN bshop.comuni c2 ON u.id_comune_nascita=c2.id "; */
	private static $pre_query = 'SELECT u.*, u.nazione, u.provincia AS sigla_prov_residenza,u.provincia AS prov_residenza,u.comune AS comune_residenza,
                                    catreg.descrizione AS categoria_utente, catreg.tipo_utente AS business,
                                    nazioni.UE AS UE
                                FROM registrati AS u
                                LEFT JOIN categorie_registrati AS catreg ON catreg.id = u.id_categoria
                                LEFT JOIN nazioni on nazioni.stato = u.nazione
                                
                               ';
	private static $attributes = array(
		':id' => 'u.id',
		':codice' => 'u.codice',
		':data_registrazione' => 'u.data_registrazione',
		':cognome' => 'u.cognome',
		':nome' => 'u.nome',
		':data_nascita' => 'u.data_nascita',
		':sesso' => 'u.sesso',
		':nazione' => 'u.nazione',
		':provincia' => 'u.provincia',
		':comune' => 'u.comune',
		':cap' => 'u.cap',
		':indirizzo' => 'u.id_comune_residenza',
		':id_comune_nascita' => 'u.id_comune_residenza',
		':id_comune_residenza' => 'u.id_comune_residenza',
		':codice_fiscale' => 'u.codice_fiscale',
		':ragione_sociale' => 'u.ragione_sociale',
		':partita_iva' => 'u.partita_iva',
		':telefono' => 'u.telefono',
		':cellulare' => 'u.cellulare',
		':email' => 'u.email',
		':id_listino' => 'u.id_listino',
		':abilitato' => 'u.abilitato',
		':username' => 'u.username',
		':password' => 'u.password',
		':codice_attivazione' => 'u.codice_attivazione',
		':id_categoria' => 'u.id_categoria',
		':fax' => 'u.fax',
		':web' => 'u.web',
		':newsletter' => 'u.newsletter',
		':data_iscrizione_newsletter' => 'u.data_iscrizione_newsletter',
		':nazione' => 'u.nazione',
		':sigla_prov_residenza' => 'u.provincia',
		':prov_residenza' => 'u.provincia',
		':comune_residenza' => 'u.comune',
		':sigla_prov_nascita' => 'c2.sigla_pro',
		':prov_nascita' => 'c2.provincia',
		':comune_nascita' => 'c2.comune',
		':categoria_utente' => 'catreg.descrizione',
		':business' => 'catreg.tipo_utente',
		':titolo' => 'u.titolo'
	);

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function __construct()
	{
	}

	public function setNew()
	{
		$this->new = array(
			'codice' => $this->codice,
			'data_registrazione' => $this->data_registrazione,
			'cognome' => $this->cognome,
			'nome' => $this->nome,
			'nazione' => $this->nazione,
			'provincia' => $this->provincia,
			'comune' => $this->comune,
			'cap' => $this->cap,
			'indirizzo' => $this->indirizzo,
			'codice_fiscale' => $this->codice_fiscale,
			'data_nascita' => $this->data_nascita,
			'ragione_sociale' => $this->ragione_sociale,
			'partita_iva' => $this->partita_iva,
			'telefono' => $this->telefono,
			'cellulare' => $this->cellulare,
			'email' => $this->email,
			'id_listino' => $this->id_listino,
			'abilitato' => $this->abilitato,
			'username' => $this->username,
			'password' => $this->password,
			'codice_attivazione' => $this->codice_attivazione,
			'id_categoria' => $this->id_categoria,
			'fax' => $this->fax,
			'web' => $this->web,
			'titolo' => $this->titolo,
			'newsletter' => $this->newsletter
		);
	}

	/**
	 * Ritorna il numero di listino dell'utente
	 * @param int $id_user
	 * @return boolean| int
	 */
	public static function getNlistino($id_user)
	{
		$em = ORM::getInstance();
		$sql = "SELECT  id_listino FROM registrati WHERE id='$id_user'";
		$em->cleanAll();
		$res = $em->findByQuery($sql);
		if (!$res) {
			return false;
		}
		return $res[0]['id_listino'];
	}

	public static function getListino($id_user)
	{
		$em = ORM::getInstance();
		$sql = "SELECT listini.* FROM listini INNER JOIN registrati ON listini.id=registrati.id_listino WHERE registrati.id='$id_user'";
		$em->cleanAll();
		$res = $em->findByQuery($sql);
		if (!$res) {
			return null;
		}
		return $res[0];
	}

	/**
	 * Ritorna true se l'email già esiste false se non esiste
	 * @param string $email
	 * @return boolean
	 */
	public static function exist_mail($email)
	{
		$em = ORM::getInstance();
		$query = "SELECT id FROM registrati WHERE email='$email' LIMIT 1";
		$em->cleanAll();
		$res = $em->findByQuery($query);
		if (!$res) {
			return false;
		}
		return true;
	}

	/**
	 * Ritorna true se l'username esiste false altrimenti
	 * @param string $username
	 * @return boolean
	 */
	public static function exist_username($username)
	{
		$em = ORM::getInstance();
		$query = "SELECT id FROM registrati WHERE username='{$username}' LIMIT 1";
		$em->cleanAll();
		$res = $em->findByQuery($query);
		if (!$res) {
			return false;
		}
		return true;
	}

	/**
	 * Invia una mail per rigenerare la pwd
	 * @param string $email
	 * @return array
	 */
	public static function password_recovery($email)
	{
		$em = ORM::getInstance();
		$query = "select id,abilitato,codice_attivazione,email from registrati where email='$email'";
		$res = $em->findByQuery($query);
		if (!$res && count($res) > 0) {
			$id_utente = $res[0]['id'];
			$abilitato = $res[0]['abilitato'];
			$codice_attivazione = $res[0]['codice_attivazione'];
			$email_to = $res[0]['email'];
		} else {
			return array('esito' => 0, 'mail' => 0); //non esiste utente e non manda email
		}
		if ($abilitato == '1') {
			$sent_mail = Message::resend_password($id_utente);

			return array('esito' => 1, 'mail' => $sent_mail);
		} elseif ($abilitato == '0' && $codice_attivazione != '') {
			$sent_mail = Message::resend_activation_code($id_utente);

			return array('esito' => 2, 'mail' => $sent_mail);
		}
		return array('esito' => 2, 'mail' => 0); //esiste utente registrato e NON confermato ma non manda email
		
		return array('esito' => 0, 'mail' => 0); //non esiste utente e non manda email
	}

	/**
	 * Completa la registrazione se va a buon fine ritorna l'id dell'utente che ha completato la registrazione altrimenti ritorna 0
	 * @param int $codatt
	 * @return int
	 */
	public static function reg_complete($codatt)
	{
		$em = ORM::getInstance();
		$query = "SELECT id from registrati where codice_attivazione='$codatt'";
		$res = $em->findByQuery($query);
		if (!$res && count($res) > 0) {
			$id = $res[0]['id'];
			$query = "UPDATE registrati set abilitato=1, codice_attivazione='' where codice_attivazione='$codatt'";
			$res = $em->executeSql($sql);
			if ($res > 0) {
				Message::send_regmail($id, 0);
				//invio notifica all'admin
				Message::notify_newreg_to_admin($id);

				return $id;
			}
		}
		return 0;
	}
}
