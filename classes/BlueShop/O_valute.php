<?php

/**
 * Description of T_category
 *
 * @author Antonio Mazza
 */
class O_valute
{
	public $codice;
	public $simbolo;
	private static $attributes = array(
		':codice' => 'codice',
		':simbolo' => 'simbolo'
	);
	private static $pre_query = 'SELECT codice,simbolo 
                                 FROM valute';

	public function __construct($row = '')
	{
		if (is_array($row)) {
			parent::_loadByRow($row);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":codice='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}
}
