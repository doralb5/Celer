<?php

/**
 * Description of T_variant
 *
 * @author alessandro
 */
class O_variant
{
	private $TABLE_NAME = 'varianti_prodotti';
	private $nlistino;
	public $id;
	public $id_prodotto;
	public $codice;
	public $quantita_disponibile;
	public $id_disponibilita;
	public $disponibilita;
	public $iva;
	public $prezzo_listino;
	public $prezzo_vendita;
	public $prezzo_listino_ivato;
	public $prezzo_vendita_ivato;
	public $sconto;
	public $risparmio;
	public $risparmio_ivato;
	public $ivainc1;
	private $attributes = array();
	private $pre_query = '';
	private $db;
	private $default_lang;
	private $user_lang;

	public function getNlistino()
	{
		return $this->nlistino;
	}

	public function setNlistino($nlistino)
	{
		$this->nlistino = $nlistino;

		$this->attributes = array(
			'id' => 'var.id',
			'id_prodotto' => 'var.id_prodotto',
			'codice' => 'var.codice',
			'quantita_disponibile' => 'var.disponibilita',
			'id_disponibilita' => 'p.id_disponibilita',
			'disponibilita' => 'd.descrizione',
			'iva' => 'p.iva',
			'prezzo_listino' => "ROUND( l{$nlistino} ,2)",
			'prezzo_vendita' => "ROUND( l{$nlistino}_sc ,2)",
			'prezzo_listino_ivato' => "ROUND( l{$nlistino} + (l{$nlistino} * p.iva / 100) ,2)",
			'prezzo_vendita_ivato' => "ROUND( l{$nlistino}_sc + (l{$nlistino}_sc * p.iva / 100) ,2)",
			'sconto' => "ROUND(100-(l{$nlistino}_sc * 100)/l{$nlistino},2)",
			'risparmio' => "ROUND( (l{$nlistino} - l{$nlistino}_sc) ,2)",
			'risparmio_ivato' => "ROUND( (l{$nlistino} - l{$nlistino}_sc) + (l{$nlistino} - l{$nlistino}_sc) * p.iva / 100 ,2)",
		);

		$this->pre_query = "SELECT var.id,var.id_prodotto,var.codice,var.disponibilita as quantita_disponibile,p.iva,p.id_disponibilita,d.descrizione as disponibilita,
                                ROUND( l{$nlistino} ,2) AS prezzo_listino,
                                ROUND( l{$nlistino}_sc ,2) AS prezzo_vendita,
                                
                                ROUND( l{$nlistino} + (l{$nlistino} * p.iva / 100) ,2) AS prezzo_listino_ivato,
                                ROUND( l{$nlistino}_sc + (l{$nlistino}_sc * p.iva / 100) ,2) AS prezzo_vendita_ivato,
                                
                                CASE WHEN (l{$nlistino}_sc=l{$nlistino}) THEN ROUND(0,2) ELSE ROUND(100-(l{$nlistino}_sc * 100)/l{$nlistino},2) END AS sconto,
                                
                                ROUND( (l{$nlistino} - l{$nlistino}_sc) ,2) AS risparmio,
                                ROUND( (l{$nlistino} - l{$nlistino}_sc) + (l{$nlistino} - l{$nlistino}_sc) * p.iva / 100 ,2) AS risparmio_ivato
                                
                           FROM {$this->TABLE_NAME} AS var 
                           INNER JOIN prodotti p ON p.id = id_prodotto
                           LEFT JOIN disponibilita AS d ON p.id_disponibilita=d.id";
	}

	public function __construct($row = '', $nlistino = 1)
	{
		$this->nlistino = $nlistino;

		$this->attributes = array(
			'id' => 'var.id',
			'id_prodotto' => 'var.id_prodotto',
			'codice' => 'var.codice',
			'quantita_disponibile' => 'var.disponibilita',
			'id_disponibilita' => 'p.id_disponibilita',
			'disponibilita' => 'd.disponibilita',
			'iva' => 'p.iva',
			'prezzo_listino' => "ROUND( l{$nlistino} ,2)",
			'prezzo_vendita' => "ROUND( l{$nlistino}_sc ,2)",
			'prezzo_listino_ivato' => "ROUND( l{$nlistino} + (l{$nlistino} * p.iva / 100) ,2)",
			'prezzo_vendita_ivato' => "ROUND( l{$nlistino}_sc + (l{$nlistino}_sc * p.iva / 100) ,2)",
			'sconto' => "ROUND(100-(l{$nlistino}_sc * 100)/l{$nlistino},2)",
			'risparmio' => "ROUND( (l{$nlistino} - l{$nlistino}_sc) ,2)",
			'risparmio_ivato' => "ROUND( (l{$nlistino} - l{$nlistino}_sc) + (l{$nlistino} - l{$nlistino}_sc) * p.iva / 100 ,2)",
		);

		$this->pre_query = "SELECT var.id,var.id_prodotto,var.codice,var.disponibilita,p.iva,p.id_disponibilita
                                ROUND( l{$nlistino} ,2) AS prezzo_listino,
                                ROUND( l{$nlistino}_sc ,2) AS prezzo_vendita,
                                
                                ROUND( l{$nlistino} + (l{$nlistino} * p.iva / 100) ,2) AS prezzo_listino_ivato,
                                ROUND( l{$nlistino}_sc + (l{$nlistino}_sc * p.iva / 100) ,2) AS prezzo_vendita_ivato,
                                
                                CASE WHEN (l{$nlistino}_sc=l{$nlistino}) THEN ROUND(0,2) ELSE ROUND(100-(l{$nlistino}_sc * 100)/l{$nlistino},2) END AS sconto,
                                
                                ROUND( (l{$nlistino} - l{$nlistino}_sc) ,2) AS risparmio,
                                ROUND( (l{$nlistino} - l{$nlistino}_sc) + (l{$nlistino} - l{$nlistino}_sc) * p.iva / 100 ,2) AS risparmio_ivato
                                
                           FROM varianti_prodotti AS var 
                           INNER JOIN prodotti p ON p.id = id_prodotto
                           LEFT JOIN disponibilita AS d ON p.id_disponibilita=d.id";

		if (is_array($row)) {
			parent::_loadByRow($row);
		} else {
			// Instanzio oggetto di tipo db con i parametri del db-giusto
			$this->default_lang = $_SESSION['default_lang'];
			if ($_SESSION['user_lang'] == '' || !isset($_SESSION['user_lang'])) {
				$this->user_lang = $this->default_lang;
			} else {
				$this->user_lang = $_SESSION['user_lang'];
			}
			$this->default_lang = $_SESSION['default_lang'];

			// DECIDERE COME PASSARE I VALORI DEL DB SPECIFICO
			$this->db = new Database($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":id='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}
}
