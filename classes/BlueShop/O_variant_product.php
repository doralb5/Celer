<?php

class O_variant_product
{
	public $id;
	public $prodotto;
	public $id_prodotto;
	public $codice;
	public $disponibilita;
	public $quantita_disponibile;
	public $id_disponibilita;
	public $iva;
	public $prezzo_listino;
	public $prezzo_vendita;
	public $prezzo_listino_raw;
	public $prezzo_vendita_raw;
	public $prezzo_listino_ivato;
	public $prezzo_vendita_ivato;
	public $sconto;
	public $risparmio;
	public $risparmio_ivato;
	public $prezzi_ivati;
	public $attribute_variant;
	private $em;

	/**
	 * @return O_attribute_variant
	 */
	public function getAttribute_variant()
	{
		$this->em->cleanAll();
		$this->em->setWhere(" :id_varianteprodotto= '{$this->id}'");
		$this->em->setRepository('O_attribute_variant');
		$this->attribute_variant = $this->em->find();
		return $this->attribute_variant;
	}

	private static $pre_query = "SELECT var.id,var.id_prodotto,var.codice,var.disponibilita as quantita_disponibile,p.iva,p.id_disponibilita,d.descrizione as disponibilita,
                                    ROUND( l:nlistino ,2) AS prezzo_listino,
                                    ROUND( l:nlistino_sc ,2) AS prezzo_vendita,
                                    l:nlistino AS prezzo_listino_raw,
                                    l:nlistino_sc AS prezzo_vendita_raw,

                                    ROUND( l:nlistino + (l:nlistino * p.iva / 100) ,2) AS prezzo_listino_ivato,
                                    ROUND( l:nlistino_sc + (l:nlistino_sc * p.iva / 100) ,2) AS prezzo_vendita_ivato,

                                    CASE WHEN (l:nlistino_sc=l:nlistino) THEN ROUND(0,2) ELSE ROUND(100-(l:nlistino_sc * 100)/l:nlistino,2) END AS sconto,

                                    ROUND( (l:nlistino - l:nlistino_sc) ,2) AS risparmio,
                                    ROUND( (l:nlistino - l:nlistino_sc) + (l:nlistino - l:nlistino_sc) * p.iva / 100 ,2) AS risparmio_ivato,
                                    
                                    (SELECT prezzi_ivati FROM listini WHERE campo_prodotto='listino:nlistino') AS prezzi_ivati
                                    
                                
                                FROM varianti_prodotti AS var
                                
                                INNER JOIN prodotti p ON p.id = id_prodotto
                                LEFT JOIN disponibilita AS d ON p.id_disponibilita=d.id";
	private static $attributes = array(
		':id' => 'var.id',
		':prodotto' => 'var.id_prodotto',
		':codice' => 'var.codice',
		':quantita_disponibile' => 'var.disponibilita',
		':id_disponibilita' => 'p.id_disponibilita',
		':disponibilita' => 'd.descrizione',
		':iva' => 'p.iva',
		':prezzo_listino' => 'ROUND( l:nlistino ,2)',
		':prezzo_vendite' => 'ROUND( l:nlistino_sc ,2)',
		':prezzo_listino_raw' => 'l:nlistino',
		':prezzo_vendite_raw' => 'l:nlistino_sc',
		':prezzo_listino_ivato' => 'ROUND( l:nlistino + (l:nlistino * p.iva / 100) ,2)',
		':prezzo_listino_ivato' => 'ROUND( l:nlistino_sc + (l:nlistino_sc * p.iva / 100) ,2)',
		':sconto' => 'CASE WHEN (l:nlistino_sc=l:nlistino) THEN ROUND(0,2) ELSE ROUND(100-(l:nlistino_sc * 100)/l:nlistino,2)',
		':risparmio' => 'ROUND( (l:nlistino - l:nlistino_sc)',
		':risparmio_ivato' => 'ROUND( (l:nlistino - l:nlistino_sc) + (l:nlistino - l:nlistino_sc) * p.iva / 100 ,2)',
		':id_prodotto' => 'id_prodotto'
	);

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function __construct()
	{
		$this->em = ORM::getInstance();
	}
}
