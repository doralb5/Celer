<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BShop_session
 *
 * @author antoniom
 */
require_once BS_OBJECTS_PATH . 'O_valute.php';
require_once BS_OBJECTS_PATH . 'O_cambi.php';
require_once BS_OBJECTS_PATH . 'O_listini.php';

class BS_session
{
	private static $prezzi_ivati_def = true;
	private static $nlistino_def = 1;

	public static function clearUser()
	{
		unset($_SESSION['user_id']);
		unset($_SESSION['user_firstname']);
		unset($_SESSION['user_lastname']);

		unset($_SESSION['user_surname']);
		unset($_SESSION['user_name']);

		unset($_SESSION['user_UE']);
		unset($_SESSION['user_Country']);
		unset($_SESSION['user_business']);

		unset($_SESSION['nlistino']);
		unset($_SESSION['prezzi_ivati']);
	}

	public static function setCurrency($code, $symbol, $exchange)
	{
		$_SESSION['currency'] = $code;
		$_SESSION['currencySymbol'] = $symbol;
		$_SESSION['currencyExchange'] = $exchange;
	}

	public static function getValuta()
	{
		if (isset($_SESSION['currencySymbol'])) {
			return $_SESSION['currencySymbol'];
		}
		return '€';
	}

	public static function getCambio()
	{
		if (isset($_SESSION['currencyExchange'])) {
			return $_SESSION['currencyExchange'];
		}
		return 1;
	}

	public static function getUser()
	{
		//        echo "<pre>";
		//        print_r($_SESSION);
		//        echo "</pre>";

		if (isset($_SESSION['user_name']) && !isset($_SESSION['user_firstname'])) {
			$_SESSION['user_firstname'] = $_SESSION['user_name'];
		}
		if (isset($_SESSION['user_surname']) && !isset($_SESSION['user_lastname'])) {
			$_SESSION['user_lastname'] = $_SESSION['user_surname'];
		}

		$user = array();
		if (isset($_SESSION['user_id'])) {
			$user['id'] = $_SESSION['user_id'];
		}
		if (isset($_SESSION['user_firstname'])) {
			$user['firstname'] = $_SESSION['user_firstname'];
		}
		if (isset($_SESSION['user_lastname'])) {
			$user['lastname'] = $_SESSION['user_lastname'];
		}

		if (isset($_SESSION['user_UE'])) {
			$user['UE'] = $_SESSION['user_UE'];
		}

		if (isset($_SESSION['user_Country'])) {
			$user['country'] = $_SESSION['user_Country'];
		}
		if (isset($_SESSION['user_business'])) {
			$user['business'] = $_SESSION['user_business'];
		}

		if (isset($user['id'])) {
			return $user;
		}
		
		return null;
	}

	public static function getUserId()
	{
		if (isset($_SESSION['user_id'])) {
			return $_SESSION['user_id'];
		}
		
		return null;
	}

	public static function decimalSep()
	{
		$locale = LOCALE;
		if (isset($_SESSION['BlueShop']['locale'])) {
			$locale = $_SESSION['BlueShop']['locale'];
		}
		if ($locale == 'it_IT') {
			return ',';
		}
		return '.';
	}

	public static function intSep()
	{
		$locale = LOCALE;
		if (isset($_SESSION['BlueShop']['locale'])) {
			$locale = $_SESSION['BlueShop']['locale'];
		}
		if ($locale == 'it_IT') {
			return '.';
		}
		return ',';
	}

	private static function load_prezzi_ivati($nlistino = '')
	{
		if ($nlistino == '') {
			$nlistino = self::$nlistino;
		}
		// @var $em ORM
		$em = ORM::getInstance();
		$em->setNlistino($nlistino);
		$em->cleanAll();
		$em->setRepository('O_listini');
		$em->setWhere(":campo_prodotto = 'listino$nlistino'");
		// @var $oblistino O_listini
		$oblistino = $em->find();

		if (!$oblistino) {
			echo 'Si è verificato un errore durante la configurazione del listino corrente!';
			return;
		}

		$_SESSION['prezzi_ivati'] = $oblistino[0]->prezzi_ivati;
		if ($_SESSION['prezzi_ivati'] == '1') {
			return true;
		}
		
		return false;
	}

	/**
	 * Setta il listino corrente e quindi aggiorna lo stato della variabile prezzi_ivati
	 * @param integer $nlistino
	 */
	public static function set_listino($nlistino = '')
	{
		if ($nlistino == '') {
			if (isset($_SESSION['nlistino'])) {
				$nl = $_SESSION['nlistino'];
			} else {
				$nl = self::$nlistino_def;
			}
		} else {
			$nl = $nlistino;
		}

		self::load_prezzi_ivati($nl);

		$_SESSION['nlistino'] = $nl;
	}

	public static function get_listino()
	{
		return (isset($_SESSION['nlistino'])) ? $_SESSION['nlistino'] : self::$nlistino_def;
	}

	/**
	 * Restituisce true o false in base a come devono essere mostrati i prezzi secondo il listino corrente
	 * @return boolean
	 */
	public static function get_prezzi_ivati()
	{
		if (isset($_SESSION['nlistino'])) {
			$nl = $_SESSION['nlistino'];
		} else {
			$nl = self::$nlistino_def;
		}

		if (!isset($_SESSION['prezzi_ivati'])) {
			self::load_prezzi_ivati($nl);
		}

		return $_SESSION['prezzi_ivati'];
	}

	public static function getErrors()
	{
		return Session::get('BSErrors');
	}

	public static function Error($e)
	{
		$errors = Session::get('BSErrors');
		if (is_array($errors)) {
			array_push($errors, $e);
		} else {
			$errors = array($e);
		}
		Session::set('BSErrors', $errors);
	}

	public static function clearErrors()
	{
		Session::set('BSErrors', array());
	}

	public static function getNotices()
	{
		return Session::get('BSNotices');
	}

	public static function Notice($n)
	{
		$notices = Session::get('BSNotices');
		if (is_array($notices)) {
			array_push($notices, $n);
		} else {
			$notices = array($n);
		}
		Session::set('BSNotices', $notices);
	}

	public static function clearNotices()
	{
		Session::set('BSNotices', array());
	}
}
