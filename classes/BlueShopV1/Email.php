<?php

require_once LIBS_PATH . 'PhpMailer/class.phpmailer.php';

class Email
{
	private static $use_smtp = USE_SMTP;
	private static $smtp_host = SMTP_HOST;
	private static $smtp_username = SMTP_USERNAME;
	private static $smtp_password = SMTP_PASSWORD;

	public static function sendMail($to, $from_name, $from, $subject, $testo, $bcc = array())
	{
		$messaggio = new PHPmailer();

		if (self::$use_smtp == '1') {
			$messaggio->IsSMTP();
			$messaggio->SMTPAuth = true;
			$messaggio->Host = self::$smtp_host;
			$messaggio->Username = self::$smtp_username;
			$messaggio->Password = self::$smtp_password;
		} else {
			$messaggio->IsMail();
		}
		$messaggio->IsHTML(true);

		$messaggio->CharSet = 'UTF-8';
		$messaggio->FromName = $from_name;
		$messaggio->From = $from;

		if (defined('DEVELOPMENT_ENVIRONMENT') && DEVELOPMENT_ENVIRONMENT === true) {
			$messaggio->AddAddress(DEVELOPMENT_MAIL);
		} else {
			$arr_to = explode(';', $to);

			for ($i = 0; $i < count($arr_to); $i++) {
				$messaggio->AddAddress($arr_to[$i]);
			}
			if (count($bcc)) {
				foreach ($bcc as $bcc_address) {
					$messaggio->AddBCC($bcc_address);
				}
			}
		}

		$messaggio->AddReplyTo($from);
		$messaggio->Subject = $subject;
		$messaggio->Body = $testo;

		if (!$messaggio->Send()) {
			$messaggio->SmtpClose();
			unset($messaggio);
			return 0;
		}
		$messaggio->SmtpClose();
		unset($messaggio);
		return 1;
		
		return 1;
	}

	public static function generateMailBody($text)
	{
		$msg = '<html><head></head><body>';
		$msg .= $text;
		$msg .= '</body></html>';
		return $msg;
	}
}
