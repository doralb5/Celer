<?php

class ORM
{

	//VARIABILI USATE PER LE QUERY
	private $where = '';
	private $group_by = '';
	private $order_by = '';
	private $limit = 1;
	private $offset = '';
	private $db;
	private $parameters = array(
		':user_lang' => '_it',
		':default_lang' => '_it',
		':nlistino' => 1
	);
	private $repository = '';
	private $lastsql;
	private static $instance;

	public function __construct()
	{
		//$this->db = Database::getInstance();
		$this->db = new Database(DB_TYPE, BS_DB_HOST, BS_DB_NAME, BS_DB_USER, BS_DB_PASS);
	}

	/**
	 *
	 * @return ORM
	 */
	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function getDefault_lang()
	{
		return $this->parameters[':default_lang'];
	}

	public function getUser_lang()
	{
		return $this->parameters[':user_lang'];
	}

	public function setRepository($repository)
	{
		$this->repository = $repository;
	}

	public function setDb()
	{
		$this->db = Database::getInstance();
	}

	public function setUser_lang($lang = '')
	{
		$this->parameters[':user_lang'] = $lang;
	}

	public function setDefault_lang($lang = '')
	{
		$this->parameters[':default_lang'] = $lang;
	}

	public function setNlistino($nlistino)
	{
		$this->parameters[':nlistino'] = $nlistino;
	}

	public function setWhere($where)
	{
		$this->where = $where;
	}

	public function setGroup_by($group_by)
	{
		$this->group_by = $group_by;
	}

	public function setOrder_by($order_by)
	{
		$this->order_by = $order_by;
	}

	public function setLimit($limit)
	{
		$this->limit = $limit;
	}

	public function setOffset($offset)
	{
		$this->offset = $offset;
	}

	public function cleanWhere()
	{
		$this->where = '';
	}

	public function cleanGroup_by()
	{
		$this->group_by = '';
	}

	public function cleanOrder_by()
	{
		$this->order_by = '';
	}

	public function cleanLimit()
	{
		$this->limit = '';
	}

	public function cleanOffset()
	{
		$this->offset = '';
	}

	public function cleanAll()
	{
		$this->where = '';
		$this->group_by = '';
		$this->order_by = '';
		$this->limit = '';
		$this->offset = '';
	}

	/**
	 * Inizializza l'oggetto corrente dall'array associativo specificato
	 *
	 * @param mixed $row Array associativo del record da caricare
	 */
	public static function _loadByRow($row, $stripSlashes = false, $callbackOnExists = false)
	{
		Utils::FillObjectFromRow($this, $row, $stripSlashes, $callbackOnExists);
	}

	public static function Load($row, $classname)
	{
		return new $classname($row);
	}

	public static function createObj($row, $classname)
	{
		$obj = new $classname();

		self::FillObjectFromRow($obj, $row, false, false);
		return $obj;
	}

	/**
	 * Restituisce un array di oggetti specifici convertendo un array associativo
	 * @param array $rows
	 * @param string $classname
	 * @return array
	 */
	public static function LoadRecords($rows, $classname)
	{
		$obj = array();
		foreach ($rows as $row) {
			// $obj[] = self::Load($row, $classname);
			$obj[] = self::createObj($row, $classname);
		}

		return $obj;
	}

	public static function FillObjectFromRow(&$obj, $row, $stripSlashes = false, $callbackOnExists = false)
	{
		$props = get_class_vars(get_class($obj));

		foreach ($props as $prop => $value) {
			if (array_key_exists($prop, $row)) {
				if (!$callbackOnExists) {
					$value = ($stripSlashes ? trim(stripslashes($row[$prop])) : $row[$prop]);
					if ($stripSlashes && self::IsUtf8($value)) {
						$value = utf8_decode($value);
					}
					$obj->$prop = $value;
				} else {
					$obj->$callbackOnExists($prop, $row[$prop]);
				}
			}
		}
	}

	/**
	 * EFFETTUA LA RICERCA IN BASE AL WHERE,ORDERBY... SETTATI PRECEDENTEMENTE RITORNA ARRAY DI OGGETTI
	 * @return $this->repository
	 */
	public function find()
	{
		$filter = '';
		if ($this->where != '') {
			$filter .= ' WHERE ' . $this->where;
		}
		if ($this->group_by != '') {
			$filter .= ' GROUP BY ' . $this->group_by;
		}
		if ($this->order_by != '') {
			$filter .= ' ORDER BY ' . $this->order_by;
		}
		if ($this->offset != '') {
			$filter .= " LIMIT {$this->limit} OFFSET {$this->offset}";
		} elseif ($this->limit != '') {
			$filter .= ' LIMIT ' . $this->limit;
		}
		$classname = $this->repository;
		$filter = strtr($filter, $classname::getAttributes());
		$query = strtr($classname::getPre_query() . $filter, $this->parameters);

		//if($classname == 'O_product')
		//echo $query."<br/><br/><br/>";
		$this->lastsql = $query;
		$rows = $this->db->select($query);

		if (count($rows)) {
			return self::LoadRecords($rows, $classname);
		}
		return false; //MESSAGGIO DI ERRORE
	}

	public function count()
	{
		$filter = '';
		if ($this->where != '') {
			$filter .= ' WHERE ' . $this->where;
		}
		if ($this->group_by != '') {
			$filter .= ' GROUP BY ' . $this->group_by;
		}
		if ($this->order_by != '') {
			$filter .= ' ORDER BY ' . $this->order_by;
		}
		if ($this->offset != '') {
			$filter .= " LIMIT {$this->limit} OFFSET {$this->offset}";
		} elseif ($this->limit != '') {
			$filter .= ' LIMIT ' . $this->limit;
		}
		$classname = $this->repository;
		$filter = strtr($filter, $classname::getAttributes());
		$query = 'SELECT COUNT(*) AS num_rows FROM ( ' . strtr($classname::getPre_query() . $filter, $this->parameters) . ' ) AS tcount';

		//if($classname == 'O_product')
		//echo $query."<br/><br/><br/>";
		$this->lastsql = $query;
		$rows = $this->db->select($query);

		if (count($rows)) {
			return $rows[0]['num_rows'];
		}
		return false; //MESSAGGIO DI ERRORE
	}

	public function bindAttributes(&$query, $obj_name)
	{
		if (class_exists($obj_name)) {
			$query = strtr($query, $obj_name::getAttributes());
			$query = strtr($query, $this->parameters);
		} else {
			throw new Exception('Classe ' . $obj_name . ' non esiste!');
		}
		return $query;
	}

	/**
	 * CERCA LE TUPLE PASSANDO TUTTA LA QUERY RITORNA ARRAY DI OGGETTI
	 * @param string $query
	 * @return type
	 */
	public function findByQuery($query, $classname = null)
	{
		//$query = strtr($query , $this->parameters);

		if ($query != '') {
			if (!(is_null($classname))) {
				$this->bindAttributes($query, $classname);
			}
			//echo $query."....<br/><br/><br/>";
			$this->lastsql = $query;
			$rows = $this->db->select($query, $this->parameters);

			if ($rows != false) {
				if (is_null($classname)) {
					return $rows;
				}
				return self::LoadRecords($rows, $classname);
			}
			//MESSAGGIO DI ERRORE
		}
		// MESSAGGIO ERRORE $QUERY=''
	}

	/**
	 * ELIMINA LA TUPLA/E CHE RISPETTANO IL WHERE SE IL WHERE NON È STATO SETTATO ELIMINA QUELLA CON ID=$THIS->ID
	 */
	public function delete($obj)
	{
		if ($this->where == '') {
			return $this->db->delete($obj->TABLE_NAME, " id={$obj->id}", 1);
		}

		return $this->db->delete($obj->TABLE_NAME, $this->where);
	}

	/**
	 * SE NON È SETTATO L'ID INSERISCE NUOVA TUPLA ALTRIMENTI MODIFICA QUELLA CON ID = THIS->ID
	 * @return boolean
	 */
	public function save($obj)
	{
		if ($obj->id == '' || is_null($obj->id)) {
			$res = $this->db->insert($obj->TABLE_NAME, $obj->new);
			if ($res) {
				return $res;
			}
			return false;
			
			/*
			  if ($res)
			  {

			  return $this->db->lastInsertId();
			  }
			  else
			  {
			  return false;
			  }
			 *
			 */
		}
		return $this->db->update($obj->TABLE_NAME, $obj->new, "id='{$obj->id}'");
	}

	public function updatesObj($obj)
	{
		/* $filter = '';
		  if ($this->where != '') {
		  $filter.=" WHERE " . $this->where;
		  }
		  if ($this->group_by != '') {
		  $filter.=" GROUP BY " . $this->group_by;
		  }
		  if ($this->order_by != '') {
		  $filter.=" ORDER BY " . $this->order_by;
		  }
		  if ($this->offset != '') {
		  $filter.=" LIMIT {$this->offset}, {$this->limit}";
		  } else if ($this->limit != '') {
		  $filter.=" LIMIT " . $this->limit;
		  } */
		$filter = " WHERE :id='{$obj->id}' LIMIT 1";
		$classname = $obj;
		$filter = strtr($filter, $classname::getAttributes());
		$query = strtr($classname::getPre_query() . $filter, $this->parameters);

		$this->lastsql = $query;
		$rows = $this->db->select($query);

		if ($rows != false) {
			self::FillObjectFromRow($obj, $rows[0], false, false);
			return true;
		}
		return false; //MESSAGGIO DI ERRORE
	}

	public function executeSql($sql)
	{
		$this->lastsql = $sql;
		return $this->db->execute($sql);
	}

	public function getLastSQL()
	{
		return $this->lastsql;
	}
}
