<?php

class Button
{
	private $id;
	private $name;
	private $size;
	private $action;
	private $class;
	private $onclick;
	private $tooltip;
	private $target;

	public function __construct($name, $action = '', $size = 'md', $class = '', $tooltip = '', $target = '')
	{
		$this->setName($name);
		$this->setId('button_' . rand(100, 999));
		$this->setAction($action);
		$this->setSize($size);
		$this->setClass($class);
		$this->setTooltip($tooltip);
		$this->setTarget($target);
	}

	public function setOnclick($onclick)
	{
		$this->onclick = $onclick;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getSize()
	{
		return $this->size;
	}

	public function getAction()
	{
		return $this->action;
	}

	public function getClass()
	{
		return $this->class;
	}

	public function getTooltip()
	{
		return $this->tooltip;
	}

	public function getTarget()
	{
		return $this->target;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setSize($size)
	{
		switch ($size) {
			case 'lg':
			case 'md':
			case 'sm':
			case 'xs':
				$this->size = $size;
				break;
			default: 'md';
		}
	}

	public function setAction($action)
	{
		$this->action = $action;
	}

	public function setClass($class)
	{
		$this->class = $class;
	}

	public function setTooltip($tooltip)
	{
		$this->tooltip = $tooltip;
	}

	public function setTarget($target)
	{
		$this->target = $target;
	}

	public function render()
	{
		echo "<a title='$this->tooltip' id=\"$this->id\" class=\"btn btn-" . ($this->size) . ' ' . (($this->class == '') ? 'btn-info' : $this->class) . "\" href=\"$this->action\" " . (($this->onclick != '') ? 'onclick="' . $this->onclick . '"' : '') . '  ' . (($this->target != '') ? 'target="' . $this->target . '"' : '') . ">$this->name</a>&nbsp;";
	}
}
