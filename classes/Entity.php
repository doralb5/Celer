<?php

class Entity
{
	protected $entity_fields = array();
	public static $table_prefix;

	public function __construct($index = '')
	{
		if ($index != '') {
			//load data in object
			$this->em->setRepository();
		}

		$class = get_class($this);
		$this->entity_fields = get_class_vars($class);

		unset($this->entity_fields['entity_fields']);
		unset($this->entity_fields['table_prefix']);
		foreach ($this->entity_fields as $key => $value) {
			$this->entity_fields[$key] = $key;
		}
	}

	public static function getSelectQueryObj($pdo)
	{
	}

	public static function getIndex()
	{
		return self::$INDEX;
	}

	public function getEntityFields()
	{
		return $this->entity_fields;
	}

	public function setTablePrefix($prefix)
	{
		self::$table_prefix = $prefix;
	}

	public function getTablePrefix()
	{
		return self::$table_prefix;
	}

	// Helper for get full field name

	public static function getFieldName($entity, $field)
	{
		$class = $entity . '_Entity';
		return self::$table_prefix . $class::TABLE_NAME . '.' . $field;
	}
}
