<?php

class EntityManager
{
	private $db;
	private $repository_path;
	private $repository;
	private $entity;
	private $entities = array();

	public function __construct()
	{
		$this->repository_path = DOCROOT . ENTITIES_PATH;
	}

	public function setDatabase($db_object)
	{
		if (is_object($db_object) && get_class($db_object) == 'Database') {
			$this->db = $db_object;
			return true;
		}
		throw new Exception('Argument passed not is a Database Object!');
	}

	public function setRepository($name)
	{
		$repository_dir = $this->repository_path . $name;
		if (is_dir($repository_dir)) {
			$this->repository = $name;
			return true;
		}
		return false;
	}

	public function setTablePrefix($prefix = '')
	{
		$this->entity->setTablePrefix($prefix);
	}

	public function setEntity($name)
	{
		$entity_file = $this->repository_path . $this->repository . '/' . $name . '.php';

		if (file_exists($entity_file)) {
			require_once $entity_file;
			$class = $name . '_Entity';
			$this->entity = new $class();
			$this->entity->setTablePrefix(TABLE_PREFIX);
			return true;
		}
		return false;
	}

	public function countByQuery($q, $bindArray = array())
	{
		$q = "SELECT COUNT(*) AS totcount FROM ($q) AS tottable";

		$res = $this->db->select($q, $bindArray);
		if (count($res)) {
			$res = $res[0]['totcount'];
		} else {
			$res = 0;
		}
		return $res;
	}

	public function countByFilter($filter = '', $bindArray = array())
	{
		$res = null;
		$this->cleanEntities();
		$class = get_class($this->entity);
		$refl = new ReflectionClass($class);
		$datasource = ($refl->getConstant('DATASOURCE') !== false) ? $refl->getConstant('DATASOURCE') : 'db';
		$idField = $refl->getConstant('INDEX');

		$table_prefix = $this->entity->getTablePrefix();
		$table = $table_prefix . $refl->getConstant('TABLE_NAME');
		$alias = $refl->getConstant('TABLE_ALIAS');
		$result = array();
		if ($datasource == 'db') {
			$q = $class::getSelectQueryObj($this->db);
			if (!is_null($q)) {
				($filter != '') ? $q = $q->where($filter) : '';

				$q = $q->groupBy(((!empty($alias)) ? $alias : $table) . '.' . $idField);

				$objquery = $q->getQuery();
			//$res = $this->db->select($q->getQuery(), $bindArray);
			} else {
				if ($alias) {
					$objquery = "SELECT * FROM $table as $alias WHERE 1 " . (($filter != '') ? "AND $filter " : ' ');
				} else {
					$objquery = "SELECT * FROM $table WHERE 1 " . (($filter != '') ? "AND $filter " : ' ');
				}
			}

			$res = $this->countByQuery($objquery, $bindArray);
		}

		return $res;
	}

	public function loadById($id)
	{
		$this->cleanEntities();
		$class = get_class($this->entity);
		$refl = new ReflectionClass($class);
		$datasource = ($refl->getConstant('DATASOURCE') !== false) ? $refl->getConstant('DATASOURCE') : 'db';

		//Te behet me getIndex()
		$idField = $refl->getConstant('INDEX');

		$table_prefix = $this->entity->getTablePrefix();
		$table = $table_prefix . $refl->getConstant('TABLE_NAME');
		$alias = $refl->getConstant('TABLE_ALIAS');

		if ($datasource == 'db') {
			$q = $class::getSelectQueryObj($this->db);

			if (!is_null($q)) {
				if ($alias) {
					$q = $q->where($alias . '.' . $idField . '= :id');
				} else {
					$q = $q->where($table . '.' . $idField . '= :id');
				}
				//echo '<!--' . $q->getQuery() . '-->';
				$res = $this->db->select($q->getQuery(), array(':id' => $id));
			} else {
				$res = $this->db->select("SELECT * FROM $table WHERE $idField = :id", array(':id' => $id));
			}
			$this->entities = $this->fillArray($res, $class);
		}
	}

	/**
	 *
	 * @param type $filter
	 * @param type $limit
	 * @param type $offset
	 * @param string $sorting
	 * @param type $bindArray
	 * @return type
	 */
	public function loadByFilter($filter = '', $limit = -1, $offset = 0, $sorting = '', $bindArray = array())
	{
		$res = null;
		$this->cleanEntities();
		$class = get_class($this->entity);
		$refl = new ReflectionClass($class);
		$datasource = ($refl->getConstant('DATASOURCE') !== false) ? $refl->getConstant('DATASOURCE') : 'db';

		$idField = $refl->getConstant('INDEX');

		$table_prefix = $this->entity->getTablePrefix();
		$table = $table_prefix . $refl->getConstant('TABLE_NAME');
		$alias = $refl->getConstant('TABLE_ALIAS');

		if ($limit > 0) {
			$strLimit = " LIMIT $offset, $limit";
		} else {
			$strLimit = '';
		}
		$res = array();
		if ($datasource == 'db') {
			$q = $class::getSelectQueryObj($this->db);
			if (!is_null($q)) {
				if ($limit > 0) {
					$q = $q->offset($offset)->limit($limit);
				}
				($filter != '') ? $q = $q->where($filter) : '';

				if ($sorting == 'RAND()') {
					$sorting = 'rand()';
				}

				($sorting != '') ? $q = $q->orderBy($sorting) : '';
				//Shtuar se fundmi groupby
				$q = $q->groupBy(((!empty($alias)) ? $alias : $table) . '.' . $idField);
				//echo "<!--" . $q->getQuery() . "-->\n\n";
				$res = $this->db->select($q->getQuery(), $bindArray);
			} else {
				$res = $this->db->select("SELECT * FROM $table WHERE 1 " . (($filter != '') ? "AND $filter " : ' ') . (($sorting != '') ? "ORDER BY $sorting" : '') . $strLimit, $bindArray);
				//echo "<!-- SELECT * FROM $table WHERE 1 " . (($filter != '') ? "AND $filter " : " ") . (($sorting != '') ? "ORDER BY $sorting" : "") . $strLimit . "-->";
			}
			$this->entities = $this->fillArray($res, $class);
		}
		return $res;
	}

	public function loadByQuery($q, $bindArray = array())
	{
		$this->cleanEntities();
		$class = get_class($this->entity);
		//echo '<!--' . $q . '-->';
		$res = $this->db->select($q, $bindArray);
		$this->entities = $this->fillArray($res, $class);
	}

	public function getEntities()
	{
		return $this->entities;
	}

	public function cleanEntities()
	{
		$this->entities = array();
	}

	public function persist($entity)
	{
		$class = get_class($entity);
		$fields = $entity->getEntityFields();

		$index = $class::INDEX;

		$table_prefix = $this->entity->getTablePrefix();

		$query_arr = array();

		foreach ($fields as $key => $field) {
			$query_arr[$field] = $entity->$key;
		}
		unset($query_arr[$fields[$index]]);
		if ($entity->$index != '') {
			//echo "<!--$table_prefix ".$class::TABLE_NAME."-->";
			$res = $this->db->update($table_prefix . $class::TABLE_NAME, $query_arr, "$index = " . $entity->$index);
		} else {
			$res = $this->db->insert($table_prefix . $class::TABLE_NAME, $query_arr);
		}
		return $res;
	}

	public function delete($entity_name, $id)
	{
		$class = $entity_name . '_Entity';
		$index = $class::INDEX;

		$this->setEntity($entity_name);
		$table_prefix = $this->entity->getTablePrefix();

		if (is_array($id)) {
			$where = '1';
			foreach ($id as $key => $value) {
				$where .= " AND $key = '$value' ";
			}
			$res = $this->db->delete($table_prefix . $class::TABLE_NAME, "$where");
		} else {
			$res = $this->db->delete($table_prefix . $class::TABLE_NAME, "$index = $id");
		}
		return $res;
	}

	private function fillArray($arr, $entityClass)
	{
		$temp_arr = array();
		if (count($arr)) {
			foreach ($arr as $r) {
				$entity = new $entityClass();
				//Utils::FillObjectFromRow($entity, $r);
				$this->FillEntityFromRow($entity, $r);

				array_push($temp_arr, $entity);
			}
		}
		return $temp_arr;
	}

	private function FillEntityFromRow($obj, $row)
	{
		$class_props = get_class_vars(get_class($obj));
		$props = $obj->getEntityFields();
		foreach ($class_props as $classprop => $v) {
			if (array_key_exists($classprop, $row)) {
				$obj->$classprop = $row[$classprop];
			}
		}
		foreach ($props as $field => $prop) {
			$prop = str_replace($obj->getTablePrefix() . $obj::TABLE_NAME . '.', '', $prop);
			if (array_key_exists($prop, $row)) {
				$obj->$field = $row[$prop];
			}
		}
	}

	public function errorInfo()
	{
		return $this->db->errorInfo();
	}
}
