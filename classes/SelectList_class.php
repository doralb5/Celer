<?php

class SelectList
{
	private $id;
	private $name;
	private $size;
	private $class;
	private $onchange;
	private $tooltip;
	private $elements = array();
	private $select_label;
	private $select_value;
	private $default = '[$All]';
	private $show_default = true;
	private $auto_submit = true;

	public function __construct($name, $elements = array(), $index = array(), $size = 'input-md', $class = '', $tooltip = '')
	{
		$this->setName($name);
		$this->setId('select_' . rand(100, 999));
		$this->setSize($size);
		$this->setClass($class);
		$this->setTooltip($tooltip);
		$this->setOnchange();
		if (count($index)) {
			$this->setElements($elements, $index[0], $index[1]);
		}
	}

	public function setOnchange($onchange = null)
	{
		//0 ne menyre qe ta lejojme te vendose string boshe
		if (is_null($onchange) && $this->auto_submit) {
			$this->onchange = 'this.form.submit()';
		} elseif (!is_null($onchange)) {
			$this->onchange = $onchange;
		} else {
			$this->onchange = '';
		}
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getSize()
	{
		return $this->size;
	}

	public function getClass()
	{
		return $this->class;
	}

	public function getTooltip()
	{
		return $this->tooltip;
	}

	public function getElements()
	{
		return $this->elements;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setSize($size)
	{
		switch ($size) {
			case 'input-lg':
			case 'input-md':
			case 'input-sm':
			case 'input-xs':
				$this->size = $size;
				break;
			default:
				'input-md';
		}
	}

	public function setClass($class)
	{
		$this->class = $class;
	}

	public function setTooltip($tooltip)
	{
		$this->tooltip = $tooltip;
	}

	public function setElements($elements = array(), $label = '', $value = '')
	{
		$this->elements = $elements;
		$this->select_label = $label;
		$this->select_value = $value;
	}

	public function addElement($element)
	{
		array_push($this->elements, $element);
	}

	public function setDefault($element)
	{
		$this->default = $element;
	}

	public function showDefault($show)
	{
		$this->show_default = $show;
	}

	public function setSelectLabel($label)
	{
		$this->select_label = $label;
	}

	public function setSelectValue($value)
	{
		$this->select_value = $value;
	}

	public function autoSubmit($auto = true)
	{
		$this->auto_submit = $auto;
		$this->setOnchange();
	}

	public function getOptions($selected_id = '')
	{
		$options = '';
		foreach ($this->elements as $elem) {
			$selected = ($selected_id == $elem->{$this->select_value}) ? 'selected' : '';
			$options .= "<option value='{$elem->{$this->select_value}}' {$selected}>{$elem->{$this->select_label}}</option>";
		}
		return $options;
	}

	public function render()
	{
		$current_url = WEBROOT . FCRequest::getUrl();

		$selected_id = isset($_GET["$this->name"]) ? $_GET["$this->name"] : '';
		$show_default = ($this->show_default ? "<option value=''>$this->default</option>" : '');

		print "     <div class='form-group' style='margin-bottom: 0px;'>
                            <select class='form-control $this->size $this->class' name='{$this->name}' title='{$this->tooltip}' id='{$this->id}' onchange='{$this->onchange}'>
                                    {$show_default}
                                    {$this->getOptions($selected_id)}
                            </select>
                    </div>";
	}
}
