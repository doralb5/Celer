<?php

require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Email.php';

class Announces_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		//$this->view->set('url_b_list', $this->getActionUrl("b_list"));
		$this->view->set('component_settings', $this->ComponentSettings);
		if (isset($this->ComponentSettings['image_baseurl'])) {
			$this->view->set('baseurl', $this->ComponentSettings['image_baseurl']);
		} else {
			$this->view->set('baseurl', '');
		}
	}

	public function listAnnounces($category_id = '')
	{
		if (!is_numeric($category_id)) {
			$category_id = substr($category_id, strripos($category_id, '-') + 1);
		}

		$parameters = WebPage::getParameters();
		$order = '';
		if (isset($_GET['order'])) {
			$order .= ' ' . $_GET['order'];
		}
		$filter = '';
		if ($category_id != '') {
			$category = $this->model->getCategory($category_id);
			if ($category) {
				$filter = '  category_id = ' . $category_id;
				HeadHTML::setTitleTag($this->view->getTerm('Announces') . ' | ' . $category->category . ' | ' . CMSSettings::$website_title);
				WebPage::$breadcrumb->addDir($this->view->getTerm('AllAnnounces'), $this->getActionUrl('listAnnounces'));
				WebPage::$breadcrumb->addDir($category->category, $this->getActionUrl('listAnnounces') . DS . $category_id);
			} else {
				WebPage::$breadcrumb->addDir($this->view->getTerm('AllAnnounces'), $this->getActionUrl('listAnnounces'));
				$this->view->AddError('[$no_such_category]');
			}
		} else {
			HeadHTML::setTitleTag($this->view->getTerm('Announces') . ' | ' . CMSSettings::$website_title);
			WebPage::$breadcrumb->addDir($this->view->getTerm('AllAnnounces'), $this->getActionUrl('listAnnounces'));
		}
		$categories = $this->model->getCategories();
		$this->view->set('categories', $categories);
		$elements_per_page = 15;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		$Announces_tbl = TABLE_PREFIX . 'Announces';
		if (isset($_REQUEST['query'])) {
			$searchFields = array(
				// array('field' => " $Announces_tbl.id", 'peso' => 100),
				array('field' => " $Announces_tbl.title", 'peso' => 100),
				array('field' => " $Announces_tbl.description", 'peso' => 70),
				array('field' => " $Announces_tbl.category", 'peso' => 30),
			);
			$announces = $this->model->search($_REQUEST['query'], $searchFields, $filter, $order, $elements_per_page, $offset);
		} else {
			$announces = $this->model->getAnnounces($elements_per_page, $offset, $filter, $order);
		}

		//$announces = $this->model->getAnnounces($elements_per_page, $offset, $filter, $order);
		$this->view->set('announces', $announces);
		$totalElements = $this->model->getLastCounter();
		if ($totalElements == 0) {
			$noitem = 0;
			$this->view->set('noitem', $noitem);
			$this->view->AddError('[$NoAnnounces]');
		}
		$this->view->set('totalElements', $totalElements);
		$this->view->set('page', $page);
		$this->view->set('elements_per_page', $elements_per_page);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'announce-list';
		$this->view->render($view);
		$this->view->cleanErrors();
	}

	public function listAnnouncesUser($category_id = '')
	{
		if (!is_numeric($category_id)) {
			$category_id = substr($category_id, strripos($category_id, '-') + 1);
		}

		if (isset($_SESSION['user_auth']['id'])) {
			$userID = $_SESSION['user_auth']['id'];
			$username = $_SESSION['user_auth']['username'];
			HeadHTML::setTitleTag($this->view->getTerm('MyAnnounces') . ' | ' . CMSSettings::$website_title);
			$parameters = WebPage::getParameters();
			$order = '';
			$filter = ' user_id = ' . $userID;

			if ($category_id != '') {
				$category = $this->model->getCategory($category_id);
				if ($category) {
					$filter .= ' AND  category_id = ' . $category_id;
					HeadHTML::setTitleTag($this->view->getTerm('Announces') . ' | ' . $category->category . ' | ' . CMSSettings::$website_title);
					WebPage::$breadcrumb->addDir($this->view->getTerm('AllAnnounces'), $this->getActionUrl('listAnnounces'));
					WebPage::$breadcrumb->addDir($this->view->getTerm('MyAnnounces'), $this->getActionUrl('listAnnouncesUser'));
					WebPage::$breadcrumb->addDir($category->category, $this->getActionUrl('listAnnouncesUser') . DS . $category_id);
				} else {
					WebPage::$breadcrumb->addDir($this->view->getTerm('AllAnnounces'), $this->getActionUrl('listAnnounces'));
					WebPage::$breadcrumb->addDir($this->view->getTerm('MyAnnounces'), $this->getActionUrl('listAnnouncesUser'));
					$this->view->AddError('[$no_such_category]');
				}
			} else {
				HeadHTML::setTitleTag($this->view->getTerm('Announces') . ' | ' . CMSSettings::$website_title);
				WebPage::$breadcrumb->addDir($this->view->getTerm('AllAnnounces'), $this->getActionUrl('listAnnounces'));
				WebPage::$breadcrumb->addDir($this->view->getTerm('MyAnnounces'), $this->getActionUrl('listAnnouncesUser'));
			}
			$elements_per_page = 21;
			$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
			$offset = ($page - 1) * $elements_per_page;
			$this->view->set('parameters', $parameters);
			$this->view->set('settings', $this->ComponentSettings);
			if (isset($_GET['order'])) {
				$order .= ' ' . $_GET['order'];
			}
			$Announces_tbl = TABLE_PREFIX . 'Announces';
			if (isset($_REQUEST['query'])) {
				$searchFields = array(
					array('field' => " $Announces_tbl.id", 'peso' => 100),
					array('field' => " $Announces_tbl.company_name", 'peso' => 100),
					array('field' => " $Announces_tbl.title", 'peso' => 100),
					//array('field' => " $Announces_tbl.tags", 'peso' => 70),
					//array('field' => " $Announces_tbl.tags", 'peso' => 50),
					array('field' => " $Announces_tbl.description", 'peso' => 30),
				);
				$announces = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
			} else {
				$announces = $this->model->getAnnounces($elements_per_page, $offset, $filter, $order);
			}

			$categories = $this->model->getCategories();
			$this->view->set('categories', $categories);
			$this->view->set('announces', $announces);
			$totalElements = $this->model->getLastCounter();
			if ($totalElements == 0) {
				$noitem = 0;
				$this->view->set('noitem', $noitem);
				$this->view->AddError('[$NoAnnounces]');
			}
			$this->view->set('totalElements', $totalElements);
			$this->view->set('page', $page);
			$this->view->set('elements_per_page', $elements_per_page);
			$view = (isset($parameters['view'])) ? $parameters['view'] : 'an-list-u';
			$this->view->render($view);
			$this->view->cleanErrors();
		} else {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . $this->getActionUrl(listAnnouncesUser));
		}
	}

	public function show_announce($id = '')
	{
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		}
		if (!$id) {
			$this->view->renderError('404');
		}
		$Announce_Table = TABLE_PREFIX . 'Announce';
		$announce = $this->model->getAnnounce($id);
		if ($announce == null) {
			$this->view->renderError('404');
			return;
		}
		if (!isset($_COOKIE['vc' . $id])) {
			setcookie('vc' . $id, $id, time() + 3600);
			$announce->views++;
			$this->model->saveAnnounce($announce);
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Title tag
		HeadHTML::setTitleTag(StringUtils::CutString($announce->title, 50) . ' | ' . CMSSettings::$website_title);
		$category = $this->model->getCategory($announce->category_id);
		WebPage::$breadcrumb->addDir($this->view->getTerm('AllAnnounces'), $this->getActionUrl('listAnnounces'));
		WebPage::$breadcrumb->addDir($announce->category, $this->getActionUrl('listAnnounces') . DS . Utils::url_slug($announce->category . "-{$announce->category_id}"));
		WebPage::$breadcrumb->addDir($this->view->getTerm('announce_details'), $this->getActionUrl('show_announce') . DS . Utils::url_slug($announce->title . "-{$announce->id}"));

		$this->view->set('category', $category);
		$this->view->set('announce', $announce);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'announce-detail';
		$this->view->render($view);
	}

	public function insertAnnounce()
	{
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'insert-announces';
		if (isset($_GET['insert']) && $_GET['insert'] == 'ok' && isset($_GET['id'])) {
			$announce = $this->model->getAnnounce($_GET['id']);
			$this->view->set('announce', $announce);
			$this->view->render('announce-ok');
			return;
		}

		HeadHTML::setTitleTag($this->view->getTerm('InsertAnnounce') . ' | ' . CMSSettings::$website_title);
		WebPage::$breadcrumb->addDir($this->view->getTerm('InsertAnnounce'), $this->getActionUrl('insertAnnounce'));
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		$categories = $this->model->getCategories(200);
		$this->view->set('categories', $categories);

		if (isset($_POST['save'])) {
			($_POST['name_a'] == '') ? $this->AddError('[$name_required]') : $name = $_POST['name_a'];
			($_POST['description'] == '') ? $this->AddError('[$description_required]') : $description = $_POST['description'];
			($_POST['title'] == '') ? $this->AddError('[$title_required]') : $title = $_POST['title'];
			($_POST['email'] == '') ? $this->AddError('[$email_required]') : $email = $_POST['email'];
			($_POST['tel'] == '') ? $this->AddError('[$phone_required]') : $tel = $_POST['tel'];
			($_POST['location'] == '') ? $this->AddError('[$city_required]') : $location = $_POST['location'];
			($_POST['category_id'] == '') ? $category_id = 1 : $category_id = $_POST['category_id'];

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('[$invalid_image_format]') : '';
			}
			//$expiration_date = date('Y-m-d', strtotime('+5 years'));
			if (isset($_SESSION['user_auth']['id'])) {
				$draft = 0;
				$user_id = $_SESSION['user_auth']['id'];
			} else {
				$draft = 1;
				$user_id = null;
			}

			if (count($this->view->getErrors()) == 0) {
				$imagefile = '';
				if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
					$imagefile = time() . '_' . Utils::clean($_POST['title']);
					$imagefile = $this->saveImage($_FILES['image']['tmp_name'], $imagefile, 'announce');
				}

				$this->model->startTransaction();
//
				//                $users_md = Loader::getModel("Users");
				//                $Users_tbl = TABLE_PREFIX . "User";
				//                $usr = $users_md->getList(1, 0, "$Users_tbl.email = '{$_POST['email']}'");
//
				//                if (count($usr) > 0 && is_null(UserAuth::getLoginSession())) {
				//                    $id_user = $usr[0]->id;
				//                    $this->view->AddError('[$email_exist]');
				//                    $this->view->render($view);
				//                    return;
				//                } else if (!is_null(UserAuth::getLoginSession())) {
				//                    $user = UserAuth::getLoginSession();
				//                    $id_user = $user['id'];
				//                } else {
				//                    $usr = new User_Entity();
				//                    $usr->email = $_POST['email'];
				//                    $usr->username = $_POST['email'];
				//                    $usr->firstname = $_POST['name_a'];
				//                    $usr->lastname = $_POST['name_a'];
				//                    $password = Utils::GenerateRandomPassword(8);
				//                    $usr->password = md5($password);
				//                    $usr->plain_password = $password;
				//                    $id_user = $users_md->saveUser($usr);
				//                }
				//                if (!is_array($id_user)) {

				$announce = new BusinessAnnounces_Entity();
				$announce->name = $_POST['name_a'];
				$announce->location = $_POST['location'];
				$announce->title = $_POST['title'];
				$announce->description = $_POST['description'];
				$announce->publish_date = date('Y-m-d H:i:s');
				$announce->expiration_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d  H:i:s', time()) . ' + 365 day'));
				$announce->user_id = null;
				$announce->email = $_POST['email'];
				$announce->image = $imagefile;
				$announce->tel = $_POST['tel'];
				$announce->category_id = $_POST['category_id'];
				$inserted_id = $this->model->saveAnnounce($announce);

				if (!is_array($inserted_id)) {
					$errors = 0;
					if ($errors == 0) {
						$this->model->commit();

						//I dergojme email
						//admin
						$this->notifyAdminOnInsertAnnounce($inserted_id);
						//user
						$this->notifyAnnounceInserted($inserted_id);

						$this->LogsManager->registerLog('Announce', 'insert', 'Announce inserted with id : ' . $inserted_id, $inserted_id);

						Utils::RedirectTo($this->getActionUrl('insertAnnounce') . '?insert=ok&id=' . $inserted_id);
					} else {
						$this->model->rollback();
						$this->view->AddError('Problem while inserting');
					}
				} else {
					$this->model->rollback();
					$this->view->AddError('Problem while inserting the announce.');
				}
				//                } else {
//                    $this->model->rollback();
//                    $this->view->AddError('Problem while adding the user.');
//                }
			}
		}

		$this->view->render($view);
		$this->cleanErrors();
	}

	public function notifyAdminOnInsertAnnounce($inserted_id)
	{
		$announce = $this->model->getAnnounce($inserted_id);
		$user_md = Loader::getModel('Users');
		$user = $user_md->getUser($announce->user_id);
		$category = $this->model->getCategory($announce->category_id);
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH . 'Announces' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Announces' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('announce', $announce);
		$message->set('category', $category);
		$message->set('user', $user);
		$txt = $message->render('mails/email_announce_online_admin', true);
		$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, $announce->title . ' - ' . 'Është shtuar një njoftim i ri në portalin ' . CMSSettings::$webdomain, $txt);
		if ($sended) {
			return true;
		}
		return false;
	}

	public function notifyAnnounceInserted($announce)
	{
		$announce = $this->model->getAnnounce($inserted_id);
		$user_md = Loader::getModel('Users');
		$user = $user_md->getUser($announce->user_id);
		$category = $this->model->getCategory($announce->category_id);
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH . 'Announces' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Announces' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('announce', $announce);
		$message->set('user', $user);
		$message->set('category', $category);
		$txt = $message->render('mails/email_announce_online', true);

		$sended = Email::sendMail($announce->email, CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, $announce->title . ' - ' . 'Njoftimi juaj në potalin ' . CMSSettings::$webdomain, $txt);
		if ($sended) {
			return true;
		}
		return false;
	}

	public function announce_edit($id)
	{
		if (isset($_SESSION['user_auth']['id'])) {
			if (!is_numeric($id)) {
				$id = substr($id, strripos($id, '-') + 1);
			} elseif (is_null($id)) {
				Utils::RedirectTo(Utils::getComponentUrl('Webpage/render_error/404'));
			}

			if (is_null($id)) {
				$this->AddError("Annonuce $id not found");
			}
			if (!is_null($id)) {
				$announce = $this->model->getAnnounce($id);
			}

			if ($announce != null && $announce->user_id == $_SESSION['user_auth']['id']) {
				if (isset($_POST['save'])) {
					($_POST['title'] == '') ? $this->AddError('Title is required!') : '';
					($_POST['description'] == '') ? $this->AddError('Content is required!') : '';
					($_POST['email'] == '') ? $this->AddError('Email is required!') : '';
					($_POST['tel'] == '') ? $this->AddError('Tel is required!') : '';
					if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
						(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
					}
					if (count($this->view->getErrors()) == 0) {
						if (strlen($_FILES['image']['tmp_name'])) {
							$_POST['image'] = time() . '_' . $_FILES['image']['name'];
							$old_image = $announce->image;
						} else {
							$_POST['image'] = $announce->image;
							$old_image = null;
						}

						$announce->image = $_POST['image'];
						$announce->name = $_POST['name_a'];
						$announce->title = $_POST['title'];
						$announce->description = $_POST['description'];
						$announce->email = $_POST['email'];
						$announce->tel = $_POST['tel'];
						$this->model->startTransaction();
						$inserted_id = $this->model->saveAnnounce($announce);
						if (!is_array($inserted_id)) {
							if (strlen($_FILES['image']['tmp_name'])) {
								$target = DOCROOT . MEDIA_ROOT . DS . 'announce';
								Utils::createDirectory($target);
								$filename = $target . DS . $_POST['image'];
								move_uploaded_file($_FILES['image']['tmp_name'], $filename);
								if (!is_null($id)) {
									unlink($target . DS . $old_image);
								}
							}
							$this->AddNotice('Announce has been saved successfully.');
							$this->model->commit();
							$this->LogsManager->registerLog('Announce', 'update', 'Announce updated with id : ' . $id, $id);
							Utils::RedirectTo($this->getActionUrl('announce_edit') . "/$id");
						} else {
							$this->AddError('Saving failed!');
							$this->model->rollback();
						}
					}
				}
				WebPage::$breadcrumb->addDir($this->view->getTerm('AllAnnounces'), $this->getActionUrl('listAnnounces'));
				WebPage::$breadcrumb->addDir($this->view->getTerm('MyAnnounces'), $this->getActionUrl('listAnnouncesUser'));
				WebPage::$breadcrumb->addDir($this->view->getTerm('edit_my_announces'), $this->getActionUrl('show_announce') . DS . Utils::url_slug($announce->title . "-{$announce->id}"));
				$this->view->set('announce', $announce);
				$parameters = WebPage::getParameters();
				$this->view->set('parameters', $parameters);
				$view = (isset($parameters['view'])) ? $parameters['view'] : 'announce-details-edit';
				$this->view->render($view);
			} else {
				$this->view->renderError('404');
				exit;
			}
		} else {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . $this->getActionUrl(announce_edit) . '/' . $id);
		}
	}

	public function announce_delete($id)
	{
		$announce = $this->model->getAnnounce($id);

		$result = $this->model->deleteAnnounce($id);
		if ($result !== false) {
			$this->LogsManager->registerLog('Announcement', 'delete', "Announcement deleted with id : $id and title : " . $announce->title, $id);
			$this->AddNotice('Announcement deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('listAnnouncesUser'));
	}

	public function ajx_showContactInfo($id = '')
	{ // reveal email and phone number and incrase view_contct field
		if (isset($_POST['ajx_request'])) {
			if ($id != '') {
				$ann = $this->model->getAnnounce($id);
				if ($ann != null) {
					$ann->email != '' ? $email = $ann->email : $email = '[$Missing]';
					$ann->tel != '' ? $tel = $ann->tel : $tel = '[$Missing]';
					if (!isset($_COOKIE['cv' . $id])) {
						setcookie('cv' . $id, $id, time() + 1800);
						$ann->view_contact++;
						$this->model->startTransaction();
						$this->model->saveAnnounce($ann);
						$this->model->commit();
					}
					$returnArray = array($tel, $email);
				} else {
					$returnArray = array('Error', 'Error');
				}
				unset($ann);
				unset($id);
				echo json_encode($returnArray);
			}
		} else {
			echo 'No Direct Entry!';
		}
	}

	private function saveImageToDb($file, $filename, $path, $todelete = false)
	{
		$tempImage = new TempImage_Entity();
		$tempImage->image = file_get_contents($file);
		$tempImage->filename = $filename;
		$tempImage->path = $path;
		$tempImage->delete_action = ($todelete) ? '1' : '0';
		$inserted_temp_image = $this->model->saveTempImage($tempImage);
		return $inserted_temp_image;
	}

	public function syncImagesFromDb()
	{
		$images = $this->model->getTempImages(-1);
		$target = DOCROOT . MEDIA_ROOT;

		foreach ($images as $image) {
			$filename = $target . $image->path . DS . $image->filename;
			$filename = str_replace('//', '/', $filename);
			if ($image->delete_action == '1') {
				$file_to_delete = $target . rtrim($image->path, DS) . DS . $image->filename;
				if (file_exists($file_to_delete)) {
					if (unlink($file_to_delete)) {
						$this->model->deleteTempImage($image->id);
					}
				} else {
					$this->model->deleteTempImage($image->id);
				}
			} else {
				if (!is_dir(dirname($filename))) {
					mkdir(dirname($filename), 0755, true) or die('Errore durante la creazione della folder!');
				}
				$result = file_put_contents($filename, $image->image);
				if ($result) {
					$this->model->deleteTempImage($image->id);
				}
			}
		}
	}

	private function saveImage($tmpfile, $newfile, $folder)
	{
		$path = DS . rtrim(ltrim($folder, DS), DS) . DS;
		$newfile = rand(0, 999) . DS . $newfile;
		if (isset($this->ComponentSettings['image_baseurl'])) {
			$this->saveImageToDb($tmpfile, $newfile, $path);
			$ch = curl_init('http://www.neshqiperi.al/com/Businesses/syncImagesFromDb');
			curl_exec($ch);
			curl_close($ch);
		} else {
			$target = DOCROOT . MEDIA_ROOT . $path;
			if (!is_dir(dirname($target . $newfile))) {
				mkdir(dirname($target . $newfile), 0755, true) or die('Errore durante la creazione della folder!');
			}
			move_uploaded_file($tmpfile, $target . $newfile);
		}
		return $newfile;
	}

	private function deleteImage($filename, $folder)
	{
		if (isset($this->ComponentSettings['image_baseurl'])) {
			$this->saveImageToDb(null, $filename, $folder, true);
			$ch = curl_init('http://www.neshqiperi.al/com/Businesses/syncImagesFromDb');
			curl_exec($ch);
			curl_close($ch);
		} else {
			$target = DOCROOT . MEDIA_ROOT . $folder . DS;
			if (file_exists($target . $filename)) {
				unlink($target . $filename);
			}
		}
	}
}
