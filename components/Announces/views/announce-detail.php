
<?php $image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : ''; ?>
<?php HeadHTML::addMeta('keywords', trim($announce->title . ' , ' . $announce->category . ' , ' . CMSSettings::$website_title . ' , njoftime , biznese , ' . $announce->category . ' ' . CMSSettings::$website_title)); ?>
<?php HeadHTML::addMeta('description', trim(StringUtils::CutString(strip_tags($announce->description), 200))); ?>
<?php HeadHTML::addMetaProperty('og:title', trim($announce->title . ' | ' . CMSSettings::$website_title)); ?>
<?php HeadHTML::addMetaProperty('og:type', 'article'); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', trim(StringUtils::CutString(strip_tags($announce->description), 200))); ?>
<?php 
	$w = (isset($parameters['w'])) ? $parameters['w'] : 960;
	$h = (isset($parameters['h'])) ? $parameters['h'] : 320;
?>
<?php if ($announce->image != '') {
	HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('announce/' . $announce->image, $w, $h, array('zc' => 1), true, $baseurl));
} else {
	if (isset($baseurl) && $baseurl != '') {
		HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('announce/announce-category/' . $announce->category_image, $w, $h, array('zc' => 1), true, $baseurl));
	} else {
		HeadHTML::addMetaProperty('og:image', 'http://' . strtolower(CMSSettings::$webdomain) . Utils::genThumbnailUrl('announce/announce-category/' . $announce->category_image));
	}
}
?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/announce-detail.css'); ?>

<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 200;

?>

<script type="text/javascript">
    'function' != typeof loadGsLib && (loadGsLib = function () {
        var e = document.createElement("script");
        e.type = "text/javascript", e.async = !0, e.src = '//api.at.getsocial.io/widget/v1/gs_async.js?id=080ddc';
        var t = document.getElementsByTagName("script")[0];
        t.parentNode.insertBefore(e, t)
    })();</script>

<div id="announce-detail">
    <div class="row">
        <div class="col-lg-12">

            <div class="card">

                <!--Announce Image-->
                <div class="row">
                    <div class="col-md-12">
                        <?php
						if ($announce->image != '') {
							if (isset($baseurl) && $baseurl != '') {
								HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('announce/' . $announce->image, $w, $h, array('zc' => 1), true, $baseurl));
							} else {
								HeadHTML::addMetaProperty('og:image', 'http://' . strtolower(CMSSettings::$webdomain) . Utils::genThumbnailUrl('announce/' . $announce->image));
							} ?>
                            <a class="" href="<?= Utils::genThumbnailUrl('announce/' . $announce->image, $w, $h, array(), false, $baseurl) ?>" data-lightbox="image-1">
                                <img class="group list-group-image" src="<?= Utils::genThumbnailUrl('announce/' . $announce->image, $w, $h, array('zc' => 1), true, $baseurl) ?>" width="100%">
                            </a>
                            <?php
						} else {
							if (isset($baseurl) && $baseurl != '') {
								HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('announce/announce-category/' . $announce->category_image, $w, $h, array('zc' => 1), true, $baseurl));
							} else {
								HeadHTML::addMetaProperty('og:image', 'http://' . strtolower(CMSSettings::$webdomain) . Utils::genThumbnailUrl('announce/announce-category/' . $announce->category_image));
							} ?>
                            <!--Default NoImage-->
                            <img class="group list-group-image" src="<?= Utils::genThumbnailUrl('announce/announce-category/' . $announce->category_image, $w, $h, array('zc' => 1), true, $baseurl) ?>" alt="" style="width: 100%; background: #cccccc;">
                        <?php
						} ?>
                    </div>
                </div>

                <div class="card-header">
                    <span class="announce-category"><i class="<?= $announce->class ?>" aria-hidden="true"></i></span>
                    <h2 class="card-title"><?= $announce->title ?></h2>
<!--                    <span class="pull-right announce-views"><i class="fa fa-eye" aria-hidden="true"></i> <?= $announce->views ?></span>-->
                </div>

                <!--Announce Additional Image-->
                <div class="row additional-images">
                    <div class="col-md-12">

                        <?php
						if (count($announce->images)) {
							foreach ($announce->images as $image) {
								?>
                                <div class="col-md-2" style="padding: 0px;">
                                    <div class="item">
                                        <a class="" href="<?= Utils::genThumbnailUrl('announce/additional/' . $image->image, 800, 800, array('zc' => 1), true, $baseurl) ?>" data-lightbox="image-1">
                                            <img class="lazyOwl" src="<?= Utils::genThumbnailUrl('announce/additional/' . $image->image, 200, 200, array('zc' => 1), true, $baseurl) ?>" alt="Immagine" width="100%" style="padding: 3px 3px 0px 0px;">
                                        </a>
                                    </div>
                                </div>
                                <?php
							}
						}
						?>

                    </div>
                </div>


                <div class="card-inner">
                    <hr>

                    <div class="card-main">
                        <?= nl2br($announce->description) ?>
                    </div>
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td class="icons">Kategoria</td>
                                <td><?= $announce->category ?></td>
                            </tr>
                            <tr>
                                <td class="icons">Publikuar</td>
                                <td><?= date('d', strtotime($announce->publish_date)) . ' ' . Utils::AlbanianWordMonth(date('n', strtotime($announce->publish_date)), 'ucfirst') . ' ' . date('Y -  H:i', strtotime($announce->publish_date)); ?></td>
                            </tr>
                            <?php if ($announce->tel != '') {
							?>
                                <tr>
                                    <td class="icons">Telefon</td>
                                    <td id="tel"><a id='show-tel' href="#"  >Shfaq numrin</a></td>
                                </tr>
                            <?php
						} ?>

                            <?php if ($announce->email != '') {
							?>
                                <tr>
                                    <td class="icons">Email</td>
                                    <td id="email"><a id='show-email' href="#" >Shfaq email</a></td>
                                </tr>
                            <?php
						} ?>

                        </tbody>
                    </table>
                    <hr/>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 marg-bottm">




                            <script>
                                $(document).ready(function () {


                                    function checkContact(id = '', tipi = '') {
                                        $.ajax({
                                            type: 'POST',
                                            data: {ajx_request: 'new'},
                                            url: "<?= Utils::getComponentUrl('Announces/ajx_showContactInfo') . '/' . $announce->id ?>",
                                            dataType: 'JSON',
                                            success: function (result) {
                                                var data = result.toString();
                                                var res = data.split(',');
                                                if (tipi == 'email') {
                                                    id.html(res[1]);
                                                } else {
                                                    id.html(res[0]);
                                                }


                                            },
                                            error: function (result) {
                                                console.log(result);
                                            }

                                        });
                                    }

                                    $('#show-tel').click(function (event) {
                                        event.preventDefault();
                                        checkContact($('#tel'), 'tel');
                                    });
                                    $('#show-email').click(function (event) {
                                        //alert('Hello');
                                        event.preventDefault();
                                        checkContact($('#email'), 'email');
                                    });
                                });


                            </script>


                        </div>
                    </div>
                    <div class="card-footer">
                        <span class="announce-name">
                            <i class="fa fa-user" aria-hidden="true"></i><?= $announce->name ?>
                        </span>

                        <div class="pull-right">
                            <div class="social-button">
                                <div class="share-button">
                                    <div class="row ">
                                        <!-- Event Social Share -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="getsocial gs-inline-group"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="add-announcement">
    <a href="<?= Utils::getComponentUrl('Announces/insertAnnounce/') ?>">
        <button class="btn btn-block alert alert-grey alert-business">
            <div class="row">
                <div class="col-md-12">
                    <i class="fa fa-plus-circle"></i>
                    [$InsertAnnounces]
                </div>
            </div>
        </button>
    </a>
</div>

