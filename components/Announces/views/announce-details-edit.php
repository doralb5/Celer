<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/announce-detail.css'); ?>

<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 200;
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 300;
$title = (isset($_POST['title'])) ? $_POST['title'] : $announce->title;
$name = (isset($_POST['name_a'])) ? $_POST['name_a'] : $announce->name;
$description = (isset($_POST['description'])) ? $_POST['description'] : $announce->description;
$email = (isset($_POST['email'])) ? $_POST['email'] : $announce->email;
$tel = (isset($_POST['tel'])) ? $_POST['tel'] : $announce->tel;
?>

<script type="text/javascript">
    'function' != typeof loadGsLib && (loadGsLib = function () {
        var e = document.createElement("script");
        e.type = "text/javascript", e.async = !0, e.src = '//api.at.getsocial.io/widget/v1/gs_async.js?id=080ddc';
        var t = document.getElementsByTagName("script")[0];
        t.parentNode.insertBefore(e, t)
    })();
</script>

<form id="insert-company" method="post" action="<?php Utils::getComponentUrl('Announces/announce_edit/') ?>" enctype="multipart/form-data" class="alert-grey">
    <div id="announce-detail">
        <div class="row">
            <div class="col-lg-12">
                <?php Messages::showErrors(); ?>
                <?php Messages::showInfoMsg(); ?>


                <?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
	?>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php $this->ComponentErrors->renderHTML(); ?>
                            <?php $this->ComponentMessages->renderHTML(); ?>
                        </div>
                    </div>
                <?php
} ?>
                <div class="">
                    <div class="card-header">

                        <div class="business-insertion">
                            <fieldset>
                                <input type="text" class="title" id="title" name="title" value="<?= $title ?>" required="" />
                                <label for="title"> Titulli i njoftimit</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>

                    </div>

                    <!--Announce Image-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($announce->image != '') {
		?>
                                <a class="" href="<?= Utils::genThumbnailUrl('announce/' . $announce->image, 800, 500, array('far' => '1', 'bg' => 'FFFFFF')) ?>" data-lightbox="image-1">
                                    <img class="group list-group-image" src="<?= Utils::genThumbnailUrl('announce/' . $announce->image, $w, $h, array('zc' => 1)) ?>" width="100%">
                                </a>
                            <?php
	} else {
		?>
                                <!--Default NoImage-->
                                <img class="group list-group-image" src="<?= Utils::genThumbnailUrl('announce/announce-category/' . $announce->category_image, $w, $h, array('zc' => '1')) ?>" alt="" style="width: 100%; background: #cccccc;">
                            <?php
	} ?>
                        </div>
                    </div>


                    <!--Announce Additional Image-->
                    <div class="row additional-images">
                        <div class="col-md-12">

                            <?php
							if (count($announce->images)) {
								foreach ($announce->images as $image) {
									?>
                                    <div class="col-md-2" style="padding: 0px;">
                                        <div class="item">
                                            <a class="" href="http://<?= CMSSettings::$webdomain . Utils::genThumbnailUrl('announce/additional/' . $image->image, 800, 800, array('zc' => '1')) ?>" data-lightbox="image-1">
                                                <img class="lazyOwl" src="<?= Utils::genThumbnailUrl('announce/additional/' . $image->image, 200, 200, array('zc' => '1')) ?>" alt="Immagine" width="100%" style="padding: 3px 3px 0px 0px;">
                                            </a>
                                        </div>
                                    </div>
                                    <?php
								}
							}
							?>
                        </div>
                    </div>


                    <div class="card-inner">
                       
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td class="icons">Kategoria</td>
                                    <td><?= $announce->category ?></td>
                                </tr>
                                <tr>
                                    <td class="icons">Publikuar</td>
                                    <td><?= date('d', strtotime($announce->publish_date)) . ' ' . Utils::AlbanianWordMonth(date('n', strtotime($announce->publish_date)), 'ucfirst') . ' ' . date('Y -  H:i', strtotime($announce->publish_date)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <hr/>

                        <?php if ($announce->tel != '') {
								?>
                            <div class="business-insertion">
                                <fieldset>
                                    <input type="text" class="tel" id="tel" name="tel" value="<?= $tel ?>" required="" />
                                    <label for="tel"><i class="fa fa-phone" aria-hidden="true"></i> Tel</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>
                        <?php
							} ?>

                        <?php if ($announce->email != '') {
								?>
                            <div class="business-insertion">
                                <fieldset>
                                    <input type="email" class="email" id="email" name="email" value="<?= $email ?>" required="" />
                                    <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> Email</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>
                        <?php
							} ?>
                        <textarea  rows="10" cols="100" class="form-control" id="message" name="description" placeholder="[$message_content]" required><?= $description ?></textarea>
                        <br>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <button type="submit" name="save" class="btn btn-primary btn-sm btn-block">Save</button>
            </div>
        </div>
    </div>


</form>



<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });
        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });
    }).call(this);
    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>