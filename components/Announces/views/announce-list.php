
<?php
$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';
HeadHTML::AddStylesheet('bootstrap-select.min.css');
HeadHTML::AddJS('noframework.waypoints.min.js');
HeadHTML::AddJS('bootstrap-select.min.js');
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/vertical-carousel.css');
?>

<?php
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';
?>
<?php
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 25;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 110;
$w = (isset($parameters['w'])) ? $parameters['w'] : 400;
$h = (isset($parameters['h'])) ? $parameters['h'] : 225;
?>

<div id="vertical-slider">

    <div class="recentArticles">
        <!--        <div class="recentArticlesHeader">-->

        <div id="search-result">
            <div class="alert alert-grey">
                <div class="row">
                    <div class="col-md-6" style="line-height: 30px">
                        <div id="category-filter-dropdown" class="ddmenu">
                            <span class="glyphicon glyphicon-menu-down glyphicon-filters " aria-hidden="true"></span> Zgjidhni Kategorine 

                            <ul>
                                <?php foreach ($categories as $category) {
	?>    
                                    <li>
                                        <a href="<?= Utils::getComponentUrl('Announces/listAnnounces/' . Utils::url_slug($category->category . "-{$category->id}")) ?>">
                                            <span class="icon-container"><i class="<?= $category->class ?> category-icon-list"></i></span>
                                                <?= $category->category ?>
                                        </a>
                                    </li>
                                <?php
} ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="btn-group">
                            <a href="#" id="list" class="btn btn-sm btn-filter">
                                <span class="glyphicon glyphicon-th-list glyphicon-filters"></span> List
                            </a> 
                            <a href="#" id="grid" class="btn btn-sm btn-filter">
                                <span class="glyphicon glyphicon-th glyphicon-filters"></span> Grid
                            </a>
                        </div>


                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <?php
						$order = isset($_GET['order']) ? $_GET['order'] : 'publish_date DESC';
						?>

                        <form method="GET">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="fa fa-sort"></span>
                                </div>
                                <select class="form-control input-sm" id="order-select"
                                        onchange="this.form.submit();" name="order">
                                    <option
                                        value="publish_date DESC" <?= ($order == 'publish_date DESC') ? 'selected' : '' ?>
                                        data-icon="fa fa-sort-amount-desc">&nbsp;[$publish_date]
                                    </option>
                                    <option
                                        value="publish_date ASC" <?= ($order == 'publish_date ASC') ? 'selected' : '' ?>
                                        data-icon="fa fa-sort-amount-asc">&nbsp;[$publish_date]
                                    </option>
                                    <option
                                        value="title DESC" <?= ($order == 'title DESC') ? 'selected' : '' ?>
                                        data-icon="fa fa-sort-alpha-desc">&nbsp;[$title]
                                    <option
                                        value="title ASC" <?= ($order == 'title ASC') ? 'selected' : '' ?>
                                        data-icon="fa fa-sort-alpha-asc">&nbsp;[$title]
                                    </option>



                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--        </div>-->

        <?php Messages::showErrors(); ?>
        <?php Messages::showInfoMsg(); ?>

        <?php if (isset($noitem)) {
							?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">[$no_item]</div>
                </div>
            </div>
        <?php
						} ?>

        <!--Announce List-->
        <div id="products" class="row list-group">
            <?php $i = 0; foreach ($announces as $ann) {
							?>

            <div class="item  col-xs-12 col-sm-6 col-md-4 col-lg-4" style="<?php if ($i == 3) {
								echo 'clear:both';
								$i = 0;
							}
							$i++; ?> ">

                    <div class="thumbnail">
                        <!--Announce Image-->


                               <a href="<?= Utils::getComponentUrl('Announces/show_announce/' . Utils::url_slug($ann->title . '-' . $ann->id)) ?>"   

                           class="title_target">
                               <?php if ($ann->image != '') {
								?>
                                    <img class="group list-group-image" src="<?= Utils::genThumbnailUrl('announce/' . $ann->image, $w, $h, array('zc' => 1), false, $image_baseurl) ?>" >
                                <?php
							} else {
								?>
                                    <!--Default NoImage-->

                                    <img class="group list-group-image" src="<?= Utils::genThumbnailUrl('announce/announce-category/' . $ann->category_image, $w, $h, array('zc' => '1')) ?>" alt="" style="background: #cccccc;">
                                <?php
							} ?>
                            </a>
                            <div class="announce-category"><i class="<?= $ann->class ?>"></i><?= $ann->category ?></div>

                            <div class="caption">
                                <!--Announce Views-->
<!--                                <span class="ann-views pull-right"><i class="fa fa-eye" aria-hidden="true">&nbsp;</i><?= $ann->views ?></span>-->

                                <!--Announce Title-->

                                       <a href="<?= Utils::getComponentUrl('Announces/show_announce/' . Utils::url_slug($ann->title . '-' . $ann->id)) ?>"   

                                   class="title_target">
                                        <h4 class="group inner list-group-item-heading announce-title"><?= strip_tags($ann->title)?></h4>
                                    </a>
                                    <p class="group inner list-group-item-text announce-content">
                                        <?= StringUtils::CutString(strip_tags($ann->description), $taglia_content) ?>
                                    </p>

                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">

                                            <!--Announce Author-->
                                            <span class="ann-author">Nga: <?= $ann->name ?></span>

                                            <!--Announce Publish Date-->
                                            <span class="ann-date"><?= date('d', strtotime($ann->publish_date)) . ' ' . Utils::AlbanianWordMonth(date('n', strtotime($ann->publish_date)), 'ucfirst') . ' ' . date('Y', strtotime($ann->publish_date)); ?></span>



                                        </div>
                                    </div>
                            </div>
                    </div>
                </div>

            <?php
						} ?>
        </div>    


    </div>


</div>

<!--/* --------------- Pagination ---------------*/-->
<?php if ($totalElements > $elements_per_page) {
							?>
    <div class="col-md-12 pagination">
        <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
    </div>
<?php
						} ?>

<script>
    $('#order-select').selectpicker({
        style: 'btn-default btn-sm'
    });
</script>

<script>

    $(document).ready(function () {
        $('#list').click(function (event) {
            event.preventDefault();
            $('#products .item').addClass('list-group-item');
        });

        $('#grid').click(function (event) {
            event.preventDefault();
            $('#products .item').removeClass('list-group-item');
            $('#products .item').addClass('grid-group-item');
        });
    });

</script>

<script>
    $("#category-filter-dropdown").on("click", function (e) {

        if ($(this).hasClass("open")) {
            $(this).removeClass("open");
            $(this).children("ul").slideUp("fast");
        } else {
            $(this).addClass("open");
            $(this).children("ul").slideDown("fast");
        }
    });

</script>
