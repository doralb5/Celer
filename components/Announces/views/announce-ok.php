<style>
    .header_bg{
        padding: 1em 0 12em 0px;
        /* margin: -16px -16px 0px -16px; */
        background-color: #81c3cf;
        background-image: url(http://www.netirane.al/data/netirane.al/media/register.png);
        -webkit-background-size: cover !important;
        -moz-background-size: cover !important;
        -ms-background-size: cover !important;
        -o-background-size: cover !important;
        background-size: contain;
        background-position: center bottom;
        background-repeat: no-repeat;
        text-align: center;
        width: 100%;
    }
    @media (max-width:992px) {
        .resizable_bg {
            margin-right: -15px;
            margin-left: -15px;;
        }
    }
    @media (min-width:767px) {
        .resizable_bg {
            margin-right: 0px;
            margin-left: 0px;;
        }
    }
</style>

<div class="resizable_bg" style="margin-top:20px;">
    <div class="header_bg">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <h2 class="" style="color: white;text-shadow: 0px 2px 2px rgba(33, 31, 31, 0.65);">URIME!</h2>

                    <h4 class="" style="color: white;text-shadow: 0px 2px 2px rgba(33, 31, 31, 0.3);"></span></h4>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="insert_ok" class="alert-grey">

    <div class="row">
        <div class="col-md-12 text-center">

            <?php if ($announce->image != '' && file_exists(DOCROOT . MEDIA_ROOT . 'announce' . DS . $announce->image)) {
	?>
                <img  class="img-responsive" style="display: inline-block;"
                     src="<?= Utils::genThumbnailUrl('announce/' . $announce->image, 200, 200, array()) ?>">
            <?php
} else {
		?>
                <img class="little-logo"
                     src="<?= WEBROOT . $this->view_path ?>img/default.jpg" width="200px" alt=""/>
            <?php
	} ?>
            <h1 style="font-weight: bold;"></h1>

            <h3>
                Në këtë moment ju krijuat njoftimin tuaj në këtë portal.
            </h3>
            <h5>
                <br/> Në 24 orët në vazhdim njoftimi juaj do të jetë i reklamuar online.
                <br/> Ju do të merrni një email në momentin që biznesi juaj do të aktivizohet.
            </h5>
            <br/>
            <br/>
            <a href="/" class="btn btn-md btn-default">Kthehuni në faqen kryesore</a>
            <p><br/></p>


        </div>
    </div>
</div>