<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/insert_announce.css'); ?>
<?php HeadHTML::AddStylesheet('select2.min.css'); ?>
<?php HeadHTML::AddJS('select2.full.min.js'); ?>

<?php
$title = (isset($_POST['title'])) ? $_POST['title'] : '';
$name = (isset($_POST['name_a'])) ? $_POST['name_a'] : '';
$description = (isset($_POST['description'])) ? $_POST['description'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
$tel = (isset($_POST['tel'])) ? $_POST['tel'] : '';
$location = (isset($_POST['location'])) ? $_POST['location'] : '';
$id_category = (isset($_POST['category_id'])) ? $_POST['category_id'] : '';
?>

<style>
    .header_bg{
        padding: 1em 0 12em 0px;
        /* margin: -16px -16px 0px -16px; */
        background-color: #81c3cf;
        background-image: url(http://www.netirane.al/data/netirane.al/media/register.png);
        -webkit-background-size: cover !important;
        -moz-background-size: cover !important;
        -ms-background-size: cover !important;
        -o-background-size: cover !important;
        background-size: contain;
        background-position: center bottom;
        background-repeat: no-repeat;
        text-align: center;
        width: 100%;
    }
    @media (max-width:992px) {
        .resizable_bg {
            margin-right: -15px;
            margin-left: -15px;;
        }
    }
    @media (min-width:767px) {
        .resizable_bg {
            margin-right: 0px;
            margin-left: 0px;;
        }
    }
</style>

<!--<div class="resizable_bg" style="margin-top:20px;">
    <div class="header_bg">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <h2 class="" style="color: white;text-shadow: 0px 2px 2px rgba(33, 31, 31, 0.65);">[$data_insert]</h2>

                    <h4 class="" style="color: white;text-shadow: 0px 2px 2px rgba(33, 31, 31, 0.3);">[$your_announce] <span style="color: #c90000;"><?= CMSSettings::$webdomain ?></span></h4>
                </div>
            </div>
        </div>
    </div>
</div>-->

<form id="insert-company" method="post" action="" enctype="multipart/form-data" class="alert-grey">
    
<div class="row">
    <div class="col-md-12">
        <div class="title-login text-center">
            <h1 class="login-main-title">[$data_insert]</h1>
            <h1 class="login-main-subtitle">[$your_announce] <span style="color: #c90000;"><?= CMSSettings::$webdomain ?></span></h1><br>
        </div>
    </div>
</div>
    
    <div class="row">


        <div class="col-md-12 border-left">

            <?php Messages::showErrors(); ?>
            <?php Messages::showInfoMsg(); ?>


            <?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
	?>
                <div class="row">
                    <div class="col-xs-12">
                        <?php $this->ComponentErrors->renderHTML(); ?>
                        <?php $this->ComponentMessages->renderHTML(); ?>
                    </div>
                </div>
            <?php
} ?>


            <div class="row">

                <div class="col-md-6">

                    <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
		?>
                        <br><br>
                        <div class="business-insertion">
                            <fieldset>
                                <input type="text" class="firstname" id="firstname"  name="name_a" value="<?= $_SESSION['user_auth']['firstname'] ?>" required="" autofocus="" readonly="true"/>
                                <label for="firstname">[$announce_user]* </label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>

                    <?php
	} else {
		?>
                        <br><br>
                        <div class="business-insertion">
                            <fieldset>
                                <input type="text" class="firstname" id="firstname"  name="name_a" value="<?= $name ?>" required="" autofocus=""/>
                                <label for="firstname">[$announce_user]* </label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>
                    <?php
	} ?>
                </div>

                <div class="col-md-6">
                    <h5 class="titlecatege">[$annonuce_category]* </h5>
                    <div class="form-group formgroup">
                        <select class="form-control" id="id_category" name="category_id">
                            <option value=""></option>
                            <?php foreach ($categories as $categ) {
		?>
                                <option
                                    value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
                                <?php
	} ?>
                        </select>
                        <label class="well-label">[$announce_example]</label>
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="col-md-6">
                    <br><br>
                    <div class="business-insertion">
                        <fieldset>
                            <input type="text" class="company_name" id="company_name"  name="title" value="<?= $title ?>" required="" />
                            <label for="company_name">[$announce_title]*</label>
                            <div class="underline"></div>
                        </fieldset>
                    </div>

                </div>
                <div class="col-md-6">
                    <h5 class="titlecatege">[$announce_image]</h5>
                    <span class="file-input"><div class="file-preview ">
                            <div class="file-preview-thumbnails">
                                <div class="file-preview-frame">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="file-preview-status text-center text-success"></div>
                            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                        </div>
                    </span>

                    <div class="upload-btn">
                        <input type="file" name="image">
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <?php if (isset($settings['location'])) {
		?>
                        <div class="business-insertion">
                            <fieldset>
                                <input type="text" name="location" value="<?= $settings['location'] ?> required="" autofocus="">
                                <label for="location">[$location]* </label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>
                    <?php
	} else {
		?>
                        <div class="business-insertion">
                            <fieldset>
                                <input type="text" name="location" value="" required="" autofocus="">
                                <label for="location">[$location]* </label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>
                    <?php
	} ?>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9">
                    <h5 class="titlecatege">[$announce_description]* </h5>

                    <div class="form-group">
                        <textarea class="form-control" rows="6"  name="description" placeholder=""
                                  required=""><?= $description ?></textarea>
                    </div>
                    <br>
                </div>
            </div>


            <br>
            <div class="row">
                <div class="col-md-6">

                    <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
		?>
                        <div class="business-insertion">
                            <fieldset>
                                <input type="email" class="email" id="email"  name="email" value="<?= $_SESSION['user_auth']['email'] ?>" required="" />
                                <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> [$announce_email]*</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>
                    <?php
	} else {
		?>
                        <div class="business-insertion">
                            <fieldset>
                                <input type="email" class="email" id="email"  name="email" value="<?= $email ?>" required="" />
                                <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> [$announce_email]*</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>

                    <?php
	} ?>
                </div>

                <div class="col-md-6">

                    <div class="business-insertion">
                        <fieldset>
                            <input type="text" class="tel" id="tel"  name="tel" value="<?= $tel ?>" required="" />
                            <label for="tel"><i class="fa fa-phone" aria-hidden="true"></i>[$announce_phone]*</label>
                            <div class="underline"></div>
                        </fieldset>
                    </div>

                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center finish-button">
                    <button type="submit" id="feedbackSubmit" name="save" class="btn btn-md btn-default">[$continue]
                    </button>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="warning" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title">[$warning]</h5>
                        </div>
                        <div class="modal-body alert-warning">
                            <p>[$insert_valid_input]</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">[$close]</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<script>
    $(document).ready(function () {
        fileInput();
    });
</script>
<script>
    // Function to replace inputs

    function fileInput(fi_container_class, fi_button_class, fi_filename_class, fi_button_text) {

        // Arguments
        fi_container_class = fi_container_class || 'fileUpload'; // Classname of the wrapper that contains the button & filename.
        fi_button_class = fi_button_class || 'fileBtn'; // Classname for the button
        fi_filename_class = fi_filename_class || 'fileName'; // Name of the text element's class
        fi_button_text = fi_button_text || '[$upload_image]'; // Text inside the button

        // Variables
        var fi_file = $('input[type=file]'); // Type of input to look for

        // Hide file inputs
        fi_file.css('display', 'none');

        // String to append
        var fi_str = '<div class="' + fi_container_class + '"><div class="' + fi_button_class + '">' + fi_button_text + '</div><div class="' + fi_filename_class + '"></div></div>';
        // Append "fake input" after the original input (which have been hidden)
        fi_file.after(fi_str);

        // Count amount of inputs
        var fi_count = fi_file.length;
        // Loop while "count" is greater than or equal to "i".
        for (var i = 1; i <= fi_count; i++) {
            // Get original input-name
            var fi_file_name = fi_file.eq(i - 1).attr('name');
            // Assign the name to the equivalent "fake input".
            $('.' + fi_container_class).eq(i - 1).attr('data-name', fi_file_name);
        }

        // Button: action
        $('.' + fi_button_class).on('click', function () {
            // Get the name of the clicked "fake-input"
            var fi_active_input = $(this).parent().data('name');
            // Trigger "real input" with the equivalent input-name
            $('input[name=' + fi_active_input + ']').trigger('click');
        });

        // When the value of input changes
        fi_file.on('change', function () {
            // Variables
            var fi_file_name = $(this).val(); // Get the name and path of the chosen file
            var fi_real_name = $(this).attr('name'); // Get the name of changed input

            // Remove path from file-name
            var fi_array = fi_file_name.split('\\'); // Split on backslash (and escape it)
            var fi_last_row = fi_array.length - 1; // Deduct 1 due to 0-based index
            fi_file_name = fi_array[fi_last_row]; //

            // Loop through each "fake input container"
            $('.' + fi_container_class).each(function () {
                // Name of "this" fake-input
                var fi_fake_name = $(this).data('name');
                // If changed "fake button" is equal to the changed input-name
                if (fi_real_name == fi_fake_name) {
                    // Add chosen file-name to the "fake input's label"
                    $('.' + fi_container_class + '[data-name=' + fi_real_name + '] .' + fi_filename_class).html(fi_file_name);
                }
            });
        });
    }
</script>
<script type="text/javascript">
    var lat = <?= $lat ?>;
    var lng = <?= $lng ?>;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: {lat: lat, lng: lng}
    });
    var geocoder = new google.maps.Geocoder();

    var marker;
    marker = new google.maps.Marker({
        icon: 'http://www.netirane.al/data/netirane.al/media/marker1.png'
    });

    geocodeAddress("<?= $location ?>", geocoder, map);
    function setMarker() {

        if (marker != null)
            marker.setMap(null);

        var address = document.getElementById('address').value + '<?= $location ?>';

        geocodeAddress(address, geocoder, map);

        if (document.getElementById('address').value != '')
            map.setZoom(15);
        else
            map.setZoom(15);

    }


    function geocodeAddress(address, geocoder, resultsMap) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                });


                google.maps.event.addListener(marker, 'dragend', function () {
                    geocodePosition(marker.getPosition());
                });


                $('#coordinates').val(results[0].geometry.location);
                console.log($('#coordinates').val());
            } else {
                $("#warning").modal();
            }
        });
    }


    function geocodePosition(pos) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode
                ({
                    latLng: pos
                },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                $("#address").val(results[0].formatted_address);
                                $('#coordinates').val(results[0].geometry.location.toString());
                            } else {
                                $("#warning").modal();
                                //$("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
                            }
                        }
                );
    }

    var checkeventcount = 1, prevTarget;
    $('.modal').on('show.bs.modal', function (e) {
        if (typeof prevTarget == 'undefined' || (checkeventcount == 1 && e.target != prevTarget)) {
            prevTarget = e.target;
            checkeventcount++;
            e.preventDefault();
            $(e.target).appendTo('body').modal('show');
        } else if (e.target == prevTarget && checkeventcount == 2) {
            checkeventcount--;
        }
    });


    //auto gps
    function gps() {
        navigator.geolocation.getCurrentPosition(function (location) {
            console.log(location.coords.latitude);
            console.log(location.coords.longitude);
            console.log(location.coords.accuracy);
        });
    }
</script>


<script>
    $("#additional_categ").select2();
    $("#id_category").select2();

    $('#business-name').blur(function () {
        var newval = $('#business-name').val().replace(/[^A-Z0-9]/ig, "-").toLowerCase();
        $('#subdomain').val(newval);
        $('#business_mail').val(newval);
    });


</script>

<script>
//auto gps
    /*
     function gps(){
     navigator.geolocation.getCurrentPosition(function(location) {
     $("#coordinates").value = location.coords.latitude+","+location.coords.longitude;
     geocodeAddress(location.coords.latitude+","+location.coords.longitude, geocoder, map);
     setMarker();
     });
     }
     */
</script>
<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });
        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });
    }).call(this);
    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>