<?php

require_once LIBS_PATH . 'Email.php';
require_once LIBS_PATH . 'StringUtils.php';

class Articles_Component extends BaseComponent
{
	public static $currCategory;

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_articles_list', $this->getActionUrl('articles_list'));
	}

	public function add_article()
	{
		if (isset($_POST['save'])) {
			(strlen($_POST['title']) > 50) ? $this->view->AddError($this->view->getTerm('title_len')) : '';
			($_POST['title'] == '') ? $this->view->AddError('Title is required') : '';
			($_POST['content'] == '') ? $this->view->AddError('Content is required') : '';
			($_POST['email'] == '') ? $this->view->AddError('Email is required') : '';
			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Invalid file type. Please upload an image.') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				$article = new Article_Entity();

				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time() . '_' . $_FILES['image']['name'];
				} else {
					$_POST['image'] = $article->image;
				}
				$article->title = $_POST['title'];
				$article->subtitle = $_POST['subtitle'];
				$article->content = $_POST['content'];
				$article->id_category = $_POST['id_category'];
				$article->image = $_POST['image'];
				$article->enabled = 0;
				$article->publish_date = date('Y-m-d H:i:s');

				if (UserAuth::checkFrontLoginSession()) {
					$logged_user = UserAuth::getLoginSession();
					$article->id_user = $logged_user['id'];
					$_POST['email'] = $logged_user['email'];
				}
				if (isset($_POST['author'])) {
					$article->author = $_POST['author'];
				}

				$users_md = Loader::getModel('Users');
				$user = $users_md->getList(1, 0, "email LIKE '{$_POST['email']}'");
				if (count($user) == 0) {
					$user = new User_Entity();
					$user->firstname = $_POST['author'];
					$user->email = $_POST['email'];
					$insertedId = $users_md->saveUser($user);
					$article->id_user = $insertedId;
				}

				$inserted_id = $this->model->saveArticle($article);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Article has been saved successfully.');
					$this->NewArticleNotification($article);

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT . MEDIA_ROOT . DS . 'articles' . DS . $article->id_category;
						Utils::createDirectory($target);
						$filename = $target . DS . $_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Article', 'insert', 'Article inserted with id : ' . $inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('articles_list'));
					}
				} else {
					$this->AddError('Saving failed!');
				}
			}
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		//BreadCrumb
		WebPage::$breadcrumb->addDir('[$Articles]', Utils::getComponentUrl('Articles/articles_list'));
		WebPage::$breadcrumb->addDir('$NewArticle', Utils::getComponentUrl('Articles/add_article'));

		//Title tag
		HeadHTML::setTitleTag('[$NewArticle]' . ' | ' . CMSSettings::$website_title);

		$id_NewsSection = (isset($parameters['news_section'])) ? $parameters['news_section'] : '';
		$categories = $this->model->getCategories(100, 0, "id_section = {$id_NewsSection}");

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-article-add';
		$this->view->render($view);
	}

	public function preload_show_article($id = null)
	{
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		} elseif (is_null($id)) {
			Utils::RedirectTo(Utils::getComponentUrl('Webpage/render_error/404'));
		}

		$Article_Table = TABLE_PREFIX . 'Article';
		$ArticleDetail_Table = TABLE_PREFIX . 'ArticleDetail';
		//$article = $this->model->getList(1, 0, "{$Article_Table}.id = $id AND {$ArticleDetail_Table}.lang = '" . FCRequest::getLang() . "'");

		$article = $this->model->getArticle($id, FCRequest::getLang());

		if (is_null($article)) {
			$this->view->AddError('Article not found');
			return;
		}

		self::$currCategory = $article->id_category;

		//BreadCrumb
		WebPage::$breadcrumb->removeLast();
		WebPage::$breadcrumb->addDir($article->category, Utils::getComponentUrl('Articles/articles_list') . '/' . Utils::url_slug($article->category) . "-{$article->id_category}");
		WebPage::$breadcrumb->addDir(StringUtils::CutString($article->title, 30), Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title) . "-$id");

		$result['article'] = $article;
		return $result;
	}

	public function show_article($id = null)
	{
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		} elseif (is_null($id)) {
			Utils::RedirectTo(Utils::getComponentUrl('Webpage/render_error/404'));
		}

		$article = $this->preload_result['article'];

		$ArticleImage_Table = TABLE_PREFIX . 'ArticleImage';
		$article->images = $this->model->getArticleImages(30, 0, "{$ArticleImage_Table}.id_article = {$article->id} ");

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'articles' . ((!is_null($article->id_category)) ? "/{$article->id_category}" : ''));

		//Title tag
		HeadHTML::setTitleTag(StringUtils::CutString($article->title, 60) . ' - ' . CMSSettings::$website_title);
		WebPage::setContentHeading($article->category);

		$category = $this->model->getCategory($article->id_category);
		$this->view->set('category', $category);
		$section = $this->model->getSection($article->id_section);
		$this->view->set('section', $section);

		$this->view->set('article', $article);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'article';
		$view = (!is_null($section) && $section->view != '') ? $section->view : $view;
		$this->view->render($view);
	}

	public function preload_articles_list($id_category = null)
	{
		if (!is_numeric($id_category) && !is_null($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}

		//BreadCrumb
		if (!is_null($id_category)) {
			$category = $this->model->getCategory($id_category);
			if (!is_null($category)) {
				$this->view->set('category', $category);

				WebPage::$breadcrumb->removeLast();
				$this->createBreadcrumbByCateg($category);

				if (WebPage::getContentHeading() == '') {
					HeadHTML::setTitleTag($category->category . ' - ' . CMSSettings::$website_title);
					WebPage::setContentHeading($category->category);
				}

				//                $section = $this->model->getSection($category->id_section);
//                $this->view->set('section', $section);
			}
		} else {
			$heading = ($this->ComponentSettings['title'] != '') ? $this->ComponentSettings['title'] : WebPage::getContentHeading();
			WebPage::setContentHeading($heading);
			WebPage::$breadcrumb->addDir($heading, Utils::getComponentUrl('Articles/articles_list'));
			//HeadHTML::setTitleTag('Articles' . ' | ' . CMSSettings::$website_title);
		}
		if (isset($_GET['query']) || isset($_GET['tags'])) {
			WebPage::$breadcrumb->addDir($this->view->getTerm('Filtered_result'));
		}
	}

	private function createBreadcrumbByCateg($categ)
	{
		$crumbs = array();
		$categories = $this->model->getCategories(-1);

		$crumbs = $this->getRecursiveCategs($categ, $categories);

		foreach ($crumbs as $item) {
			WebPage::$breadcrumb->addDir($item->category, Utils::getComponentUrl('Articles/articles_list/' . $item->url_key));
		}
	}

	private function getRecursiveCategs($categ, $all_categs, $recarray = array())
	{
		array_unshift($recarray, $categ);

		if ($categ->parent_id > 0) {
			foreach ($all_categs as $parent) {
				if ($parent->id == $categ->parent_id) {
					$recarray = array_merge($this->getRecursiveCategs($parent, $all_categs, $recarray), $recarray);
					break;
				}
			}
		}
		return $recarray;
	}

	public function articles_list($id_category = null)
	{
		if (!is_numeric($id_category) && !is_null($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}

		$elements_per_page = 12;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;

		$Article_Table = TABLE_PREFIX . 'Article';
		$ArticleDetail_Table = TABLE_PREFIX . 'ArticleDetail';
		$ArticleCategory_Table = TABLE_PREFIX . 'ArticleCategory';
		$Section_Table = TABLE_PREFIX . 'ArticleSection';
		$multiLang = isset($this->ComponentSettings['multilanguage']) ? $this->ComponentSettings['multilanguage'] : 0;

		$sorting = 'publish_date desc';
		$filter = "{$Article_Table}.enabled = '1' ";

		$filter .= "AND {$ArticleDetail_Table}.lang = '" . FCRequest::getLang() . "'";

		if (!is_null($id_category)) {
			$subcategs_ids = $this->model->getSubCategoriesIds($id_category, true);
			$filter .= "AND {$Article_Table}.id_category IN ('" . implode("','", $subcategs_ids) . "') ";
		}

		$id_section = isset($_GET['id_section']) ? $_GET['id_section'] : null;
		if (!is_null($id_section)) {
			$filter .= "AND {$Section_Table}.id = {$id_section} ";
		}

		if (isset($_GET['tags'])) {
			$TagArticle_Table = TABLE_PREFIX . 'TagArticle';
			$filter .= ' AND ( ';
			foreach (explode(';', $_GET['tags']) as $tag_id) {
				$filter .= " {$TagArticle_Table}.tag_id = $tag_id OR";
			}
			$filter = rtrim($filter, 'OR');
			$filter .= ' )';
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		$id_NewsSection = (isset($parameters['news_section'])) ? $parameters['news_section'] : null;

		$categ_filter = '';
		if (!is_null($id_NewsSection)) {
			$categ_filter = "id_section = {$id_NewsSection}";
		}
		$categories = $this->model->getCategories(100, 0, $categ_filter);
		$this->view->set('categories', $categories);

		if (!is_null($id_NewsSection) && is_null($id_category)) {
			$Section_Table = TABLE_PREFIX . 'Section';
			$filter .= "AND {$Section_Table}.id = {$id_NewsSection}";
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				//array('field' => "{$Article_Table}.id", 'peso' => 100),
				array('field' => "{$ArticleDetail_Table}.title", 'peso' => 90),
				array('field' => "{$ArticleDetail_Table}.subtitle", 'peso' => 90),
				array('field' => "{$ArticleCategory_Table}.category", 'peso' => 50),
			);
			$articles = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$articles = $this->model->getList($elements_per_page, $offset, $filter, $sorting);
		}

		$totalElements = $this->model->getLastCounter();
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('articles', $articles);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'articles' . ((!is_null($id_category)) ? "/$id_category" : ''));

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'list';
		$this->view->render($view);
	}

	public function categories_list()
	{
	}

	private function NewArticleNotification($article)
	{
		$message = new BaseView();
		$message->setViewPath(VIEWS_PATH . 'Articles' . DS);
		$message->setLangPath(LANGS_PATH . 'Articles' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal('http://' . $_SERVER['HTTP_HOST'] . substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')) . MEDIA_ROOT . CMSSettings::$logo);
		$message->placeholder('MEDIA_PATH')->setVal('http://' . $_SERVER['HTTP_HOST'] . substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')) . MEDIA_ROOT);

		$subject = $message->getTerm('NewArticleInserted') . ' - ' . CMSSettings::$website_title;
		$message->renderTemplate(false);
		$message->set('article', $article);
		$txt = $message->render('mails/email_new_article', true);
		Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, $subject, $txt);
	}
}
