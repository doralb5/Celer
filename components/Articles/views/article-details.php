<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/article-standard.css'); ?>
<?php HeadHTML::AddStylesheet('lightbox.css'); ?>
<?php HeadHTML::addJS('lightbox-2.6.min.js'); ?>

<?php
require_once LIBS_PATH . 'StringUtils.php';
$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : '');
?>
<?php HeadHTML::addMetaProperty('og:title', $article->title); ?>
<?php HeadHTML::addMetaProperty('og:type', 'article'); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl("articles/{$article->id_category}/" . urlencode($article->image), 1000, 0, array(), true)); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($article->content), 200)); ?>

<?php
// modificare
//if (!is_null($article->id_section)) {
//    $show_Author = false;
//    $show_Date = false;
//    $show_ShareButton = false;
//    $show_Comments = false;
//}
//

$show_Title = ($article->show_title == 'no') ? false : true;
$show_Subtitle = ($article->show_subtitle == 'no') ? false : true;
$show_Author = ($article->show_author == 'no') ? false : true;
$show_Date = ($article->show_date == 'no') ? false : true;
$show_ShareButton = ($article->show_share == 'no') ? false : true;
$show_Comments = ($article->show_comments == 'no') ? false : true;
$show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0;
$show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0;
$show_ArticleCategory = (isset($parameters['show_ArticleCategory'])) ? $parameters['show_ArticleCategory'] : 0;
?>

<div class="post_details_inner">
    <div class="post_details_block details_block2">
        <div class="post-header">
            <?php if ($show_ArticleCategory) {
	?>
                <ul class="td-category">
                    <li><a class="post-category" href="#"><?= $article->category ?></a></li>
                </ul>
            <?php
} ?>
            <?php if ($show_Title || $show_Subtitle) {
		?>
                <?php if ($show_Title) {
			?>
                    <h2><?= $article->title ?></h2>
                <?php
		} ?>

                <?php if ($show_Subtitle) {
			?>
                    <h4><?= $article->subtitle ?></h4>
                <?php
		} ?>
            <?php
	} ?>
            <ul class="authar-info">
                <?php if ($show_Author && $article->author != '') {
		?><li><a href="#" class="link"><?= $article->author; ?></a></li><?php
	} ?>
                <?php if ($show_Date) {
		?><li><?= date('j [$F] Y', strtotime($article->publish_date)) ?></li><?php
	} ?>
                <?php if ($show_visitors) {
		?><li><a href="#" class="link"><span class="visits-tot"><i class="fa fa-eye"></i> 0</span>&nbsp;&nbsp;</a></li><?php
	} ?>
            </ul>
        </div> 
        <?php if (!is_null($article->image)) {
		?>
            <figure class="social-icon">
                <a href="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", 1000, 0) ?>" data-lightbox="article-image">
                    <img class="img-responsive" src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", 800, 0, array()) ?>" width="100%"/>
                </a>
                <?php if ($show_ShareButton) {
			?>
                    <div>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href, 'FacebookShare', 600, 250); return false;" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/home?status=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href, 'LinkedinShare', 600, 250); return false;" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="https://plus.google.com/share?url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href, 'GooglePlusShare', 600, 250); return false;" target="_blank"><i class="fa fa-google-plus"></i></a>
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href, 'LinkedinShare', 600, 250); return false;"  target="_blank" class="hidden-xs"><i class="fa fa-linkedin"></i></a>
                    </div>
                <?php
		} ?>
            </figure>
        <?php
	} else {
		?>
            <figure class="social-icon">
                <a href="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", 1000, 0) ?>" data-lightbox="article-image">
                    <img class="img-responsive" src="/templates/Portal01/images/details-690x460-2.jpg" width="100%"/>
                </a>
                <?php if ($show_ShareButton) {
			?>
                    <div>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href, 'FacebookShare', 600, 250); return false;" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/home?status=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href, 'LinkedinShare', 600, 250); return false;" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="https://plus.google.com/share?url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href, 'GooglePlusShare', 600, 250); return false;" target="_blank"><i class="fa fa-google-plus"></i></a>
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href, 'LinkedinShare', 600, 250); return false;"  target="_blank" class="hidden-xs"><i class="fa fa-linkedin"></i></a>
                    </div>
                <?php
		} ?>
            </figure>
        <?php
	} ?>
        <?= $article->content ?>

    </div>
</div>

<?php if ($show_Comments) {
		?>
    <div class="comments-container">
        <h3>[$Comments]<?= ($show_commentNr) ? ' (' . count($article->comments) . ')' : ''; ?></h3>
        <ul class="comments-list">
            <?php foreach ($article->comments as $comment) {
			?>
                <li>
                    <div class="comment-main-level">
                        <!-- Avatar -->
                        <!--<div class="comment-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div>-->
                        <div class="comment-box">
                            <div class="comment-content">
                                <div class="comment-header"> <cite class="comment-author"><?=$comment->user; ?></cite>
                                    <time datetime="<?=$comment->creation_date; ?>" class="comment-datetime"><?=date('j [$F] Y - H:i', strtotime($comment->creation_date))?></time>
                                </div>
                                <p><?=$comment->content; ?></p>
                                <a href="#" class="btn btn-news"> Reply</a>
                            </div>
                        </div>
                    </div>
                    <?php if (count($comment->replies)) {
				?>
                        <ul class="comments-list reply-list">
                            <?php foreach ($comment->replies as $reply) {
					?>
                                <li>
                                    <!-- Avatar -->
                                    <!-- <div class="comment-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div> -->
                                    <div class="comment-box">
                                        <div class="comment-content">
                                            <div class="comment-header"> <cite class="comment-author"><?=$reply->user; ?></cite>
                                                <time datetime="<?=$reply->creation_date?>" class="comment-datetime"><?=date('j [$F] Y - H:i', strtotime($reply->creation_date))?></time>
                                            </div>
                                            <p><?=$reply->content; ?></p>
                                            <a href="#" class="btn btn-news"> Reply</a>
                                        </div>
                                    </div>
                                </li>
                            <?php
				} ?>
                        </ul>
                    <?php
			} ?>
                </li>
            <?php
		} ?>
        </ul>
    </div>


    <form class="comment-form" action="#" method="post">
        <h3><strong>Leave</strong> a Comment</h3>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="name">full name*</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Your name*">
                </div>
            </div>
            <div class="col-sm-6">
                <label for="email">Email*</label>
                <div class="form-group">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Your email address here">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="name">website</label>
                    <input type="text" class="form-control" id="website" name="website" placeholder="Your website url">
                </div>
            </div>
            <div class="col-sm-6">
                <label for="email">Subject</label>
                <div class="form-group">
                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Write subject here">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="email">message</label>
            <textarea class="form-control" id="message" name="message" placeholder="Your Comment*" rows="5"></textarea>
        </div>
        <a href="#" class="btn btn-news"> Submit</a>
    </form>
<?php
	} ?>








