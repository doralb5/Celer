<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/article-standard.css'); ?>
<?php HeadHTML::AddStylesheet('lightbox.css'); ?>
<?php HeadHTML::addJS('lightbox-2.6.min.js'); ?>

<?php
require_once LIBS_PATH . 'StringUtils.php';
$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : '');
?>
<?php HeadHTML::addMetaProperty('og:title', $article->title); ?>
<?php HeadHTML::addMetaProperty('og:type', 'article'); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl("articles/{$article->id_category}/" . urlencode($article->image), 1000, 0, array(), true)); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($article->content), 200)); ?>

<?php
// modificare
//if (!is_null($article->id_section)) {
//    $show_Author = false;
//    $show_Date = false;
//    $show_ShareButton = false;
//    $show_Comments = false;
//}
//

$show_Title = ($article->show_title == 'no') ? false : true;
$show_Subtitle = ($article->show_subtitle == 'no') ? false : true;
$show_Author = ($article->show_author == 'no') ? false : true;
$show_Date = ($article->show_date == 'no') ? false : true;
$show_ShareButton = ($article->show_share == 'no') ? false : true;
$show_Comments = ($article->show_comments == 'no') ? false : true;
$show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0;
$show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0;
?>

<?php if ($show_ShareButton) {
	?>
<script type="text/javascript">
    'function' !== typeof loadGsLib && (loadGsLib = function () {
        var e = document.createElement("script");
        e.type = "text/javascript", e.async = !0, e.src = '//api.at.getsocial.io/widget/v1/gs_async.js?id=080ddc';
        var t = document.getElementsByTagName("script")[0];
        t.parentNode.insertBefore(e, t);
    })();
</script>
<?php
}?>





<div class="article-standard col-md-12">
    
    <?php if ($show_Title || $show_Subtitle) {
		?>
            <div class="row">
            <?php if ($show_Title) {
			?>
                <h2 class="title"><?= $article->title ?></h2>
            <?php
		} ?>

            <?php if ($show_Subtitle) {
			?>
                <h3 class="subtitle"><?= $article->subtitle ?></h3>
            <?php
		} ?>
            </div>
    <?php
	} ?>
    
    
    <div class="info row">
        <?if ($show_Author && $article->author != '') {
		?><span class="author"><?=$article->author; ?></span>&nbsp;&nbsp;<?
	}?>
        <?if ($show_Date) {
		?><span class="date"><?= date('j [$F] - Y', strtotime($article->publish_date)) ?></span>&nbsp;&nbsp;<?
	}?>
        <?if ($show_visitors) {
		?><span class="visits-tot"><i class="fa fa-eye"></i> 0</span>&nbsp;&nbsp;<?
	}?>
        <?if ($show_commentNr) {
		?><span class="comments-tot"><i class="fa fa-comments"></i><a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">0</a></span><?
	}?>
    </div>
    
    <div class="row">
        <div class="description">

        <?php if (!is_null($article->image)) {
		?>
            <div class="image image-to-left <?=(isset($parameters['image-full-width']) && $parameters['image-full-width'] == 1) ? 'col-md-12' : 'col-md-6'; ?>">
                <div class="row">
                <?php $categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : ''); ?>
                <a
                   href="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", 1000, 0) ?>"
                   data-lightbox="article-image">
                    <img class="img-responsive"
                         src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", 800, 0, array()) ?>"
                         width="100%"/>
                </a>
                </div>
            </div>
        <?php
	} ?>


        <?= $article->content ?>

        </div>
    </div>
    
    
    <div class="row additional-images">
        <?php
		foreach ($article->images as $img) {
			$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : ''); ?>
            <div class="col-md-2 col-sm-3 col-xs-6 additional-image">
                <a class="thumbnail"
                   href="<?= Utils::genThumbnailUrl("articles{$categoryPath}/additional/{$img->image}", 700, 0) ?>"
                   data-lightbox="article-image" data-gallery="multiimages" data-title="">
                    <img src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/additional/{$img->image}", 200, 200, array('zc' => '1')) ?>"/></a>
            </div>
        <?php
		} ?>

    </div>
    
    
</div>