<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/article.css'); ?>

<?php
$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : '');
?>
<?php HeadHTML::addMetaProperty('og:title', $article->title); ?>
<?php HeadHTML::addMetaProperty('og:type', 'article'); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl("articles/{$article->id_category}/" . urlencode($article->image), 1000, 0, array(), true)); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($article->content), 200)); ?>

<?php
// modificare
if (!is_null($article->id_section)) {
	$show_Author = false;
	$show_Date = false;
	$show_ShareButton = false;
	$show_Comments = false;
}
//

$show_Title = ($article->show_title == 'no') ? false : true;
$show_Subtitle = ($article->show_subtitle == 'no') ? false : true;
$show_Author = ($article->show_author == 'no') ? false : true;
$show_Date = ($article->show_date == 'no') ? false : true;
$show_ShareButton = ($article->show_share == 'no') ? false : true;
$show_Comments = ($article->show_comments == 'no') ? false : true;
$show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0;
$show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0;
?>

<div class="article-container">
   <?php if ($show_Title || $show_Subtitle) {
	?>
        <?php if ($show_Title) {
		?>
            <div class="title">
                <span><?= $article->title ?></span>
            </div>
        <?php
	} ?>

        <?php if ($show_Subtitle) {
		?>
            <div class="subtitle">
                <span><?= $article->subtitle ?></span>
            </div>
        <?php
	} ?>

    <?php
} ?>
    <?php if ($show_Author || $show_Date || $show_visitors || $show_commentNr) {
		?>
        <ul class="article-information">
            <?php if ($show_Date) {
			?>
                <li class="article-date"><!--<i class="fa fa-clock-o"></i>-->
                    <?php
					if (FCRequest::getLang() == 'al') {
						echo '<span class="day">' . date('j', strtotime($article->publish_date)) . '</span>' . ' ' . '<span class="month">' . Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date))) . '</span>';
					} else {
						echo '<span class="day">' . date('j', strtotime($article->publish_date)) . '</span>' . ' ' . '<span class="month">' . date('[$F]', strtotime($article->publish_date)) . '</span>';
					} ?>
                </li>
            <?php
		} ?>
                
              
           <?php if ($show_ShareButton) {
			?>
                <!--div class="sharepost">
                    <ul>
                         <li> <i class="fa fa-facebook-official" aria-hidden="true"> </i> </li>
                        <li> <i class="fa fa-twitter-square" aria-hidden="true"></i></li>
                        <li> <i class="fa fa-google-plus-square" aria-hidden="true"></i></li>
                        <li> <i class="fa fa-pinterest-square" aria-hidden="true"></i></li>
                        <li><i class="fa fa-linkedin-square" aria-hidden="true"></i></li>
                    </ul>
                </div-->
               
               
               
            <?
		} ?>
            <?php if ($show_Author) {
			?>
                <?php if (!is_null($article->author) && $article->author != '') {
				?>
                    <li class="article-author"> [$by]
                        <?= $article->author; ?>
                    </li>
                <?php
			} ?>
            <?php
		} ?>

            <?php if ($show_visitors) {
			?>
                <li>
                    <i class="fa fa-eye"></i> 0
                </li>
            <?php
		} ?>

            <?php if ($show_commentNr) {
			?>
                <li>
                    <i class="fa fa-comments"></i>
                    <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">0</a>
                </li>
            <?php
		} ?>
        </ul>
    <?php
	} ?>
    <div class="article-body">
        <?php if (!is_null($article->image)) {
		?>
            <div class="article-image">
                <?php
				$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : ''); ?>
                <a class=""
                   href="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", 1200, 0) ?>"
                   data-lightbox="image-1">
                    <img class="image img-responsive"
                         src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", 800, 0, array()) ?>"
                         width="100%"/>
                </a>
            </div>
        <?php
	} ?>
        <div class="article-content">
            <?= $article->content ?>
        </div>
    </div>
    <div class="article-additional-images">
        <div class="row">
            <?php
			foreach ($article->images as $img) {
				$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : ''); ?>
                <div class="col-md-2 col-sm-3 col-xs-6">
                    <div class="additional-image">
                        <a
                           href="<?= Utils::genThumbnailUrl("articles{$categoryPath}/additional/{$img->image}", 700, 0) ?>"
                           data-lightbox="lightbox" data-gallery="multiimages" data-title="">
                            <img class="additional-img img-responsive" src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/additional/{$img->image}", 200, 200, array('zc' => '1')) ?>"/>
                        </a>
                    </div>
                </div>
            <?php
			} ?>
        </div>
    </div>
</div>
<?php if ($show_ShareButton) {
				?>
     <!-- Social Share Buttons -->

     <!-- // Social Share Buttons -->
 <?php
			} ?>

     
  