<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/article_archieve.css'); ?>

 <?php foreach ($archive as $year => $months) {
	?> 
    <div class="panel-group archieve_articles" id="accordion_<?=$year?>">    
        <div class="panel panel-default archive-year-panel">
            <div class="panel-heading">
                 <h4 class="panel-title archive-year">
                     <a data-toggle="collapse" data-parent="#accordion3" href="#year_<?=$year?>">
                         <h4>[$year] <?php echo $year ?></h4>
                     </a>
                 </h4>
            </div>
            <div id="year_<?=$year?>" class="panel-collapse collapse out">
                <div class="panel-body archive-month-panel"> 
                    <div class="archive_month_wrapper"> 
                       <ul class="archive_month">
                       <?php foreach ($months as $m => $counter) {
		?>     
                       <?php $link = Utils::getComponentUrl('Articles/articles_list') . "?year=$year&month=$m" ?>
                           <li>
                               <a href="<?=$link?>">
                                   <?php echo Utils::AlbanianWordMonth($m) ?>
                                   <span> <?php echo $counter?></span>
                               </a> 
                           </li>
                       <?php
	} ?>
                       </ul>                                          
                   </div>                             
                </div>
            </div>
        </div>
    </div>
 <?php
} ?>