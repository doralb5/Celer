<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/card-list.css'); ?>
<?php
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';
?>
<?php $show_date = (isset($parameters['show_date'])) ? $parameters['show_date'] : 1; ?>
<?php $show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0; ?>
<?php $show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0; ?>
<?php $show_Subtitle = (isset($parameters['show_Subtitle'])) ? $parameters['show_Subtitle'] : 1; ?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 400;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 350;
$button = (isset($parameters['button'])) ? $parameters['button'] : 'primary';
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 2;
$col_size = 12 / $cols;

?>

<!--/* --------------- Card List - MDB---------------*/-->

<?php if ($title != '') {
	?>
    <div class="main-title-description">
        <h1 class="page-header header-title"><?= $title ?></h1>
    </div>
<?php
} ?>


<?php if (count($articles)) {
		?>
    <div class="row">

        <?php
		$i = 0;
		foreach ($articles as $article) {
			?>

            <?php if ($i % $cols == 0 && $i != 0) {
				?>
                <div class="clearfix"></div>
            <?php
			} ?>

            <div class="col-md-<?= $col_size ?> news-item">
                <article class="card">
                    <!--/* --------------- Image ---------------*/-->
                    <header class="card__thumb">
                        <?php if ($article->image != '') {
				?>
                            <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">
                                <?php
								$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : ''); ?>
                                <img src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", $w, $h, array('zc' => 1)) ?>" >
                            </a>
                        <?php
			} ?>
                    </header>

                    <div class="card__date">
                        <span class="card__date__day">
                            <?php echo date('j', strtotime($article->publish_date)); ?>
                        </span>
                        <span class="card__date__month">
                            <?php
							if (FCRequest::getLang() == 'al') {
								echo Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date)));
							} ?>
                        </span>
                    </div>
                    <!--/* --------------- Content ---------------*/-->
                    <div class="card__body">
                        <div class="card__category">
                           
                                <?= $article->category?>
                        </div>

                        <div class="card__title"><a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>"><?= StringUtils::CutString(strip_tags($article->title), $taglia_title) ?></a></div>
                        <?php if ($show_Subtitle) {
								?>
                            <div class="card__subtitle"><?= $article->subtitle ?></div>
                        <?php
							} ?>
                        <p class="card__description"><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></p>
                    </div>
                    <!--/* --------------- Tags ---------------*/-->
                    <footer class="card__footer">
                        <?php if ($show_date) {
								?>
                            <span class="icon icon--time">
                                <?php
								if (FCRequest::getLang() == 'al') {
									echo Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date))) . ' ' . date('j, Y', strtotime($article->publish_date));
								} else {
									echo strftime('%d %B, %Y', strtotime($article->publish_date));
								} ?>
                            </span>
                        <?php
							} ?>

                        <?php if ($show_commentNr) {
								?>
                            <span class="icon icon--comment"><a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">0 [$Comments]</a></span>
                        <?php
							} ?>

                        <?php if ($show_visitors) {
								?>
                            <span><i class="icon icon--eye"></i> 0 [$Views]</span>
                        <?php
							} ?>

                    </footer>

                </article>

            </div>

            <?php
			$i++;
		} ?>
    </div>
<?php
	} ?>
