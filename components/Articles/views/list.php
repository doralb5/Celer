<?php
require_once LIBS_PATH . 'Pager.php';
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/articles-style.css');
?>
<?php require_once DOCROOT . LIBS_PATH . 'StringUtils.php'; ?>
<?php $show_date = (isset($parameters['show_date'])) ? $parameters['show_date'] : 0; ?>
<?php $show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0; ?>
<?php $show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0; ?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 200;
$h = (isset($parameters['h'])) ? $parameters['h'] : 200;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';

$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 2;
$col_size = 12 / $cols;
?>
<!--/* --------------- News List ---------------*/-->
<div id="news-list">
    <?php if ($title != '') {
	?>
    <div class="row">
        <div class="col-md-12">
            <p class="categ-title"> <?= $title ?></p>
        </div>
    </div>
    <?php
} ?>
    <?php
	if (count($articles) > 0) {
		?>
    <div class="row is-flex">
        <?$i = 0;
		foreach ($articles as $article) {
			?>
            <div class="col-md-<?=$col_size?> news-items">
                <?php if ($article->image == '') {
				?>
                    <!--/* --------------- Content ---------------*/-->
                    <div class="col-md-12 col-sm-12 col-xs-12 news-body">

                        <p class="news-title">
                            <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">
                                <?= $article->title ?>
                            </a>
                        </p>

                        <p class="news-description">
                            <?= StringUtils::CutString(strip_tags($article->content), 130) ?>

                        </p>

                        <p class="meta-tags">
                            <?php if ($show_date) {
					?>
                            <span><i class="fa fa-clock-o"></i>
                                <?php
								if (FCRequest::getLang() == 'al') {
									echo Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date))) . ' ' . date('j, Y', strtotime($article->publish_date));
								} else {
									echo date('j [$F] Y', strtotime($article->publish_date));
								} ?>
                            </span>
                        <?php
				} ?>
                        <?php if ($show_visitors) {
					?>
                            <span><i class="fa fa-eye"></i>0</span>
                        <?php
				} ?>
                        <?php if ($show_commentNr) {
					?>
                            <span><i class="fa fa-comments"></i>
                                <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">0</a>
                            </span>
                        <?php
				} ?>
                        <span class="leggi pull-right">
                            <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>"> [$read_more] »</a>
                        </span>
                        </p>
                    </div>
                    <?php
			} else {
				?>
                    <div class="row">
                        <!--/* --------------- Image ---------------*/-->
                        <div class="col-sm-6 img-div">
                            <a class="hover-img" href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">
                                <img src="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, $w, $h, array('zc' => 1)) ?>" class="news-img img-responsive">
                                <i class="fa fa-link box-icon-big round hover-icon"></i>
                            </a>
                        </div

                        <!--/* --------------- Content ---------------*/-->
                        <div class="col-sm-6 news-body">

                            <p class="news-title">
                                <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">
                                    <?= StringUtils::CutString(strip_tags($article->title), 30) ?>
                                </a>
                            </p>
                            <p class="news-subtitle">
                                <?= $article->subtitle ?>
                            </p>

                            <p class="news-description">
                                <?= StringUtils::CutString(strip_tags($article->content), 130) ?>

                            </p>

                            <p class="meta-tags">
                                <?php if ($show_date) {
					?>
                                <span><i class="fa fa-clock-o"></i>
                                    <?php
									if (FCRequest::getLang() == 'al') {
										echo Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date))) . ' ' . date('j, Y', strtotime($article->publish_date));
									} else {
										echo date('j [$F] Y', strtotime($article->publish_date));
									} ?>
                                </span>
                            <?php
				} ?>
                            <?php if ($show_visitors) {
					?>
                                <span><i class="fa fa-eye"></i>0</span>
                            <?php
				} ?>
                            <?php if ($show_commentNr) {
					?>
                                <span><i class="fa fa-comments"></i>
                                    <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">0</a>
                                </span>
                            <?php
				} ?>
                            <span class="leggi pull-right">
                                <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>"> [$read_more] »</a>
                            </span>
                            </p>
                        </div> 
                    </div>
                <?php
			} ?>
            </div>
            <?php
			$i++;
		} ?>
    </div>  

        <!--/* --------------- Pagination ---------------*/-->
        <div class="col-md-12 pagination">
            <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
        </div>
    <?php
	} ?>
</div>