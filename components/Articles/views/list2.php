<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/articles-style.css'); ?>
<?php
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';
?>
<?php $show_date = (isset($parameters['show_date'])) ? $parameters['show_date'] : 1; ?>
<?php $show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0; ?>
<?php $show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0; ?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 400;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 255;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 350;
$button = (isset($parameters['button'])) ? $parameters['button'] : 'primary';
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 2;
$col_size = 12 / $cols;
?>

<!--/* --------------- News List v2---------------*/-->

<style>
    .thumbnail {
        padding: 0px;
        margin-bottom: 0px;
        border: 0px solid white;
        border-radius: 0px;
    }

    .button_align {
        text-align: center;
    }

    .fff {
        margin: 30px 0;
        box-shadow: 0 1px 2px rgba(0,0,0,0.2);
        transition: .5s;
    }
    .fff:hover {
    -moz-box-shadow: 0px 5px 10px 0px rgba(0,0,0,0.25);
    -webkit-box-shadow: 0px 5px 10px 0px rgba(0,0,0,0.25);
    box-shadow: 0px 5px 10px 0px rgba(0,0,0,0.2);
    /* transform: perspective(400px); */
}
    .fff .caption {
        padding: 0 10px 10px 10px;
    }

    .fff .meta-tags {
        padding: 5px 10px 5px 10px;
        border-bottom: 1px solid #efefef;
    }
</style>

<?php if (count($articles)) {
	?>
    <div id="article-list">
        <div class="item items">
            <div class="thumbnails">
                <div class="row">
                    <?php
					$i = 0;
	foreach ($articles as $article) {
		?>

                        <?php if ($i % $cols == 0 && $i != 0) {
			?>
                            <div class="clearfix"></div>
                        <?php
		} ?>

                        <div class="col-md-<?= $col_size ?> news-item">
                            <div class="fff">
                                <?php if ($article->image != '') {
			?>
                                    <!--/* --------------- Image ---------------*/-->
                                    <div class="thumbnail">
                                        <a class="hover-img"
                                           href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">
                                               <?php
											   $categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : ''); ?>
                                            <img alt=""
                                                 src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", $w, $h, array('zc' => 1)) ?>"/>
                                        </a>
                                    </div>
                                    <!--/* --------------- Tags ---------------*/-->
                                    <div class="meta-tags">
                                        <?php if ($show_date) {
											   	?>
                                            <span>
                                                <?php
												if (FCRequest::getLang() == 'al') {
													echo Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date))) . ' ' . date('j, Y', strtotime($article->publish_date));
												} else {
													echo date('j [$F] Y', strtotime($article->publish_date));
												} ?>
                                            </span>
                                        <?php
											   } ?>
                                        <?php if ($show_visitors) {
											   	?>
                                            <span><i class="fa fa-eye"></i> 0</span>
                                        <?php
											   } ?>
                                        <?php if ($show_commentNr) {
											   	?>
                                            <span><i class="fa fa-comments"></i>
                                                <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">0</a>
                                            </span>
                                        <?php
											   } ?>
                                    </div>
                                <?php
		} ?>
                                <!--/* --------------- Content ---------------*/-->
                                <div class="caption">
                                    <h4 class="title"><a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>"><?= strip_tags($article->title) ?></a></h4>

                                    <p class="content"><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></p>
                                    <div class="button_align">
                                        <a class="btn btn-sm btn-<?= $button ?> btn-read-more"
                                           href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">[$ReadMore]</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <?php
						$i++;
	} ?>
                </div>
            </div>
        </div>
    </div>
<?php
} else {
		?>
    <p class="noresult">[$NoResults]</p>
<?php
	} ?>
    


<!--/* --------------- Pagination ---------------*/-->
<?php if ($totalElements > $elements_per_page) {
		?>
    <div class="col-md-12 pagination">
        <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
    </div>
<?php
	} ?>
