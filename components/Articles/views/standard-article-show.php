<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/articles-style.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/social-media-share.css'); ?>


<?php
require_once LIBS_PATH . 'StringUtils.php';
$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : '');
?>
<?php HeadHTML::addMetaProperty('og:title', $article->title); ?>
<?php HeadHTML::addMetaProperty('og:type', 'article'); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl("articles/{$article->id_category}/" . urlencode($article->image), 1000, 0, array(), true)); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($article->content), 200)); ?>

<?php
// modificare
if (!is_null($article->id_section)) {
	$show_Author = false;
	$show_Date = false;
	$show_ShareButton = false;
	$show_Comments = false;
}
//

$show_Title = ($article->show_title == 'no') ? false : true;
$show_Subtitle = ($article->show_subtitle == 'no') ? false : true;
$show_Author = ($article->show_author == 'no') ? false : true;
$show_Date = ($article->show_date == 'no') ? false : true;
$show_ShareButton = ($article->show_share == 'no') ? false : true;
$show_Comments = ($article->show_comments == 'no') ? false : true;
$show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0;
$show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0;
?>

<div class="article_std_container">

    <?php if ($show_Title || $show_Subtitle) {
	?>
        <div class="row">
            <div class="col-md-12 article-header">
                <?php if ($show_Title) {
		?>
                    <div class="article-title">
                        <?= $article->title ?>
                    </div>
                <?php
	} ?>

                <?php if ($show_Subtitle) {
		?>
                    <div class="article-subtitle">
                        <?= $article->subtitle ?>
                    </div>
                <?php
	} ?>



            </div>
        </div>
    <?php
} ?>

    <div class="row">
        <div class="col-md-12 article-body">
            <?php if (!is_null($article->image)) {
		?>
                <div class="article-image">
                    <?php
					$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : ''); ?>
                    <a class=""
                       href="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", 1000, 0) ?>"
                       data-lightbox="image-1">
                        <img class="image"
                             src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", 0, 0, array()) ?>"
                             width="100%"/>
                    </a>
                </div>
            <?php
	} ?>

 

            <div class="content">
                <?= $article->content ?>
            </div>
        </div>
    </div>

    <br>
</div>
<div class="article_std_container">    
    <div class="row additional-images">
        <?php
		foreach ($article->images as $img) {
			$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : ''); ?>
            <div class="col-md-2 col-sm-3 col-xs-6 single-image">
                <a class="thumbnail"
                   href="<?= Utils::genThumbnailUrl("articles{$categoryPath}/additional/{$img->image}", 700, 0) ?>"
                   data-lightbox="lightbox" data-gallery="multiimages" data-title="">
                    <img class="additional-img"
                         src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/additional/{$img->image}", 200, 200, array('zc' => '1')) ?>"/></a>
            </div>
        <?php
		} ?>

    </div>
              <?php if ($show_Author || $show_Date || $show_visitors || $show_commentNr) {
			?>
                <div class="content-head">

                    <?php if ($show_Date) {
				?>
                        <span class="article-date"><!--<i class="fa fa-clock-o"></i>-->
                            <?php
							if (FCRequest::getLang() == 'al') {
								echo '<span class="day">' . date('j', strtotime($article->publish_date)) . '</span>' . ' ' . '<span class="month">' . Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date))) . '</span>';
							} else {
								echo '<span class="day">' . date('j', strtotime($article->publish_date)) . '</span>' . ' ' . '<span class="month">' . date('[$F]', strtotime($article->publish_date)) . '</span>';
							} ?>
                        </span>
                    <?php
			} ?>
                    
                    <?php if ($show_Author) {
				?>
                        <?php if (!is_null($article->author) && $article->author != '') {
					?>
                            <span class="article-author">[$by]
                                <?= $article->author; ?>
                            </span>
                        <?php
				} ?>
                    <?php
			} ?>
                    
                      <?php if ($show_ShareButton) {
				?>
                       
                        <div class="sharepost">
                            
                            <ul>
                                <li class="share-title">[$share_in_social_media] </li>
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'FacebookShare',600,250); return false;" target="_blank"> <i class="fa fa-facebook-official" aria-hidden="true"> </i> </a></li>
                                <li><a href="https://twitter.com/home?status=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'LinkedinShare',600,250); return false;" target="_blank"> <i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                                <li><a href="https://plus.google.com/share?url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'GooglePlusShare',600,250); return false;" target="_blank"> <i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'LinkedinShare',600,250); return false;"  target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                     
                    <?php
			} ?>

                    
                    
                    
                    
                    
                    
                    
             
                    <?php if ($show_visitors) {
				?>
                        <span><i class="fa fa-eye"></i> 0</span>
                    <?php
			} ?>

                    <?php if ($show_commentNr) {
				?>
                        <span><i class="fa fa-comments"></i>
                            <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">0</a>
                        </span>
                    <?php
			} ?>

                </div>
            <?php
		} ?>

  

</div>
