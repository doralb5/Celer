<?php

class Banners_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_banners_list', $this->getActionUrl('banners_list'));
	}

	public function preload_banners_list($id_category = null)
	{
		if (!is_numeric($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}

		//BreadCrumb
		if (!is_null($id_category)) {
			$category = $this->model->getCategory($id_category);
			$this->view->set('category', $category);

			WebPage::$breadcrumb->addDir($category->category, Utils::getComponentUrl("Banners/banners_list/$id_category"));
			HeadHTML::setTitleTag($category->category . ' | ' . CMSSettings::$website_title);
		} else {
			WebPage::$breadcrumb->addDir('[$Banners]', Utils::getComponentUrl('Banners/banners_list'));
			HeadHTML::setTitleTag('Banners' . ' | ' . CMSSettings::$website_title);
		}
	}

	public function banners_list($id_category = null)
	{
		if (!is_numeric($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}

		$elements_per_page = 12;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;

		$Banner_Table = TABLE_PREFIX . 'Banner';
		$BannerCategory_Table = TABLE_PREFIX . 'BannerCategory';

		$filter = "{$Banner_Table}.enabled = '1' AND {$Banner_Table}.publish_date < NOW() AND ({$Banner_Table}.expire_date IS NULL || {$Banner_Table}.expire_date > NOW()) ";

		if (!is_null($id_category)) {
			$filter .= "AND id_category = $id_category ";
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		$sorting = isset($parameters['order']) ? $parameters['order'] : 'publish_date desc';
		$title = isset($parameters['title']) ? $parameters['title'] : '';
		//$sorting = "creation_date desc";

		$banners = $this->model->getList($elements_per_page, $offset, $filter, $sorting);
		$totalElements = $this->model->getLastCounter();
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('banners', $banners);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'banners');

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-banners-list';
		$this->view->render($view);
	}
}
