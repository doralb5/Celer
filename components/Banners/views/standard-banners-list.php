<?php
$w = isset($parameters['w']) ? $parameters['w'] : 500;
$h = isset($parameters['h']) ? $parameters['h'] : 0;
$title = isset($parameters['title']) ? $parameters['title'] : '';

$cols = isset($parameters['cols']) ? $parameters['cols'] : 4;
$c = 12 / $cols;

require_once LIBS_PATH . 'Pager.php';
?>
<div class="banners-list">
    <?php if ($title != '') {
	?>
        <div class="row">
            <div class="col-md-12">
                <div class="main-title-description">
                    <h1 class="page-header  header-title"><?= $title ?></h1>
                </div>
            </div>
        </div>
    <?php
} ?>
    <div class="row">
        <?php for ($i = 0; $i < count($banners); $i++) {
		?>
            <li class="col-sm-<?= $c ?> item">
                <a class="ln-black single-banner" href="">
                    <img width="100%"
                         src="<?= Utils::genThumbnailUrl('banners/' . $banners[$i]->filename, $w, $h, array('far' => '1', 'bg' => 'FFFFFF')) ?>">
                </a>
            </li>
        <?php
	} ?>
    </div>
</div>

<!--/* --------------- Pagination ---------------*/-->
<div class="col-md-12 pagination">
    <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
</div>

