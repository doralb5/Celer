<?php

require_once LIBS_PATH . 'StringUtils.php';
require_once CLASSES_PATH . 'BlueShopV1/BS_session.php';
require_once CLASSES_PATH . 'Email.php';

class Orders_Component extends BaseComponent
{
	private $sito;

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_products_list', $this->getActionUrl('products_list'));

		//$this->model = Loader::getModel('Products', 'BlueShop');
		require_once CONFIG_PATH . 'blueshop.php';
		$this->model = Loader::getModel('Orders', 'BlueShopV1');
	}

	//    public function initialize()
	//    {
	//        $this->sito = $this->model->getImpostazioni();
	//    }

	public function updateCart()
	{
		// @var $cart O_cart
		$cart = '';
		//Cerco il carrello in sesione
		//     session_destroy();
		if (isset($_SESSION['cart0']) && $_SESSION['cart'] != false) { //Se esiste in sessione lo recupero
			$cart = $_SESSION['cart'];
		}
		// Se il carrello non esiste in sessione
		else {
			$id_user = '';
			if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
				$id_user = $_SESSION['user_id'];
			}
			$carrello = $this->model->getCart($id_user, session_id()); //Cerco nel db se l'utente ha già un carrello creato
			//if (!$carrello)
			if (is_null($carrello->id)) { // se l'utente non ha ancora creato un carrelo lo creo
				if (isset($_POST['action']) && $_POST['action'] == 'add') {// se è un'operazione di aggiungi dettaglio devo creare il carrelo altrimenti ritorno false
					$id_carrello = $this->model->createCart();
					if (!$id_carrello) {
						throw new Exception('Error: create cart');
					}
					echo 'id_carrello creato:' . $id_carrello;
					$carrello = $this->model->getCartbyId($id_carrello);
				}
			}

			$cart = $carrello;
		}
		if ($cart != false) { // se sono riuscito a recuperare/creare il carrelo
			if (isset($_POST['action']) && $_POST['action'] != '') { // fare uno switch è più pulito
				switch ($_POST['action']) {

					case 'remove': // Operazione delete details
						if (isset($_POST['delete_']) && $_POST['delete_'] != '') {
							if (!$this->removeDetails($cart, $_POST['delete_'])) {
								throw new Exception('Error: delete items');
							}
						} elseif (isset($_POST['delete']) && $_POST['delete'] > 0) {
							if (!$this->removeDetails($cart, $_POST['delete'])) {
								throw new Exception('Error: delete item');
							}
						}
						$cart->update();
						break;
					case 'update_qta': // Operazione update quantità details

						if (isset($_POST['qta_']) && $_POST['qta_'] != '') {
							$this->updateQtaDetails($cart, $_POST['qta_']);
							$cart->update();
						}
						break;
					case 'add': // Operazione add details

						if (isset($_POST['id_product']) && $_POST['id_product'] != '') {
							$id_var = '';
							if (isset($_POST['id_var']) && $_POST['id_var'] != '') {
								$id_var = $_POST['id_var'];
							}
							if (!$this->addProduct($cart, $_POST['id_product'], $_POST['qta'], $id_var)) {
								throw new Exception('Error: add detail');
							}
							$cart->update();
						}
						break;
					default:
						break;
				}
			} else {
				$cart->getDetails();
			}
		}

		$this->model->updateDiscount($cart);

		Utils::RedirectTo(UrlUtils::getActionUrl('Orders/cart'));
	}

	/**
	 * Funzione del modulo carrello
	 * serve per visualizzare il carrello
	 * se in carrello non esiste e non è stato selezionata la funzione add_cart non crea il carrelo ma restituisce false, quindi il carrello è vuoto
	 * in caso siano settati $_GET['add_cart'] | $_GET['delete_cart'] | $_GET['qta_cart']
	 * svolge una delle seguenti operazioni aggiungi dettaglio | rimovi dettaglio | modifica quantità dettaglio
	 * dopo aver svolto le operazioni riaggiorna l'oggetto carrello e lo passa alla vista
	 * @throws Exception
	 * @return boolean
	 */
	public function cart()
	{
		if (isset($_SESSION['cart0']) && $_SESSION['cart'] != false) { //Se esiste in sessione lo recupero
			$cart = $_SESSION['cart'];
		} else {
			$id_user = '';
			if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
				$id_user = $_SESSION['user_id'];
			}
			$cart = $this->model->getCart($id_user, session_id(), 'order'); //Cerco nel db se l'utente ha già un carrello creato

			if (is_null($cart) || !$cart) {
				$id_carrello = $this->model->createCart();
				if (!$id_carrello) {
					throw new Exception('Error: create cart');
				}
				$cart = $this->model->getCartbyId($id_carrello);
			} else {
				// DEVE RESETTARE COSTI DI SPEDIZIONE E SPESE AGGIUNTIVE
				$cart->setShipment_method(0);
				$cart = $this->model->getCart($id_user, session_id(), 'order');
			}
		}

		$cart->getDetails();
		$cart->getPayment_methods();

		$this->signAvailability($cart);

		$params = array();

		$user = BS_session::getUser();
		if ($user['UE'] == '0') {
			$params['prezzi_ivati'] = false;
			$params['extraUE'] = true;
			$params['EsenzioneIva'] = 'E08';	//Codice di esenzione iva per rivenditori extra UE
		} elseif ($user['UE'] == '1' && $user['country'] != 'Italia' && $user['business'] == '1') {
			$params['prezzi_ivati'] = false;
			$params['extraUE'] = false;
			$params['EsenzioneIva'] = 'E41';	//Codice di esenzione iva per rivenditori in UE ma fuori dall'Italia
		} else {
			$params['prezzi_ivati'] = BS_session::get_prezzi_ivati();
			$params['extraUE'] = false;
		}

		$params['valuta'] = BS_session::getValuta();
		$params['cambio'] = BS_session::getCambio();

		$this->view->params = $params;
		$this->view->cart = $cart;
		$this->view->impostazioni = $this->sito;
		$this->view->url_next_step = UrlUtils::getActionUrl('Orders/stepSetShipment');

		$this->checkMinOrder($cart);

		if (isset($_SESSION['CartErrors'])) {
			$this->view->msg_error = $_SESSION['CartErrors'];
			unset($_SESSION['CartErrors']);
		}
		$errors = BS_session::getErrors();
		if ($errors != null) {
			$this->view->msg_error .= implode('<br/>', $errors);
			BS_session::clearErrors();
		}
		$notices = BS_session::getNotices();
		if ($notices != null) {
			$this->view->msg_notices .= implode('<br/>', $notices);
			BS_session::clearNotices();
		}

		$this->render('Cart');
	}

	private function checkMinOrder($cart)
	{
		echo '<!-- check Min -->';
		$listino_attivo = BS_session::get_listino();
		$ordine_min = $this->model->getMinOrder($listino_attivo);
		if ($cart->imponibile < $ordine_min) {
			$_SESSION['CartErrors'] = "Attenzione: Per procedere con l'ordine è necessario inserire nel carrello almeno " . $ordine_min . ' € di merce.';
			return false;
		}
		return true;
	}

	private function signAvailability(&$cart)
	{
		$product_model = Loader::loadModel('Products', true, 'BlueShop');

		if (is_array($cart->details)) {
			for ($i = 0; $i < count($cart->details); $i++) {
				$prod = $product_model->getProduct($cart->details[$i]->id_prodotto);
				$cart->details[$i]->hasAvailable = $product_model->ProdIsAvailable($cart->details[$i]->id_prodotto, $cart->details[$i]->id_variante, $cart->details[$i]->quantita);

				if ($cart->details[$i]->quantita < $prod->conf_min) {
					$cart->details[$i]->minRequired = $prod->conf_min;
				}
				$prod->loadMultiple(BS_session::get_listino());

				if (isset($prod->multiple) && $cart->details[$i]->quantita % $prod->multiple != 0) {
					$cart->details[$i]->multipleOf = $prod->multiple;
				}
			}
		}
	}

	/**
	 * Funzione per aggiungere un dettaglio al carrelo se è settata la variabile id_var prende alcuni valori dalla variante del prodotto
	 * @param O_cart $cart
	 * @param int $id_product
	 * @param int $qta
	 * @param int $id_var default =''
	 */

	/**
	 * PROBABILMENTE QUESTA FUNZIONE è INUTILIZZATA... quella utilizzata è nel controller Products!!!!!!!
	 */
	private function addProduct($cart, $id_product, $qta = 1, $id_var = '')
	{
		$prodcut_model = Loader::loadModel('Products', true, 'BlueShop');
		if (!$prodcut_model->ProdIsAvailable($id_product, $id_var, $qta)) {
			return false;
		}
		// @var $detail O_order_details
		$detail = new O_order_details();

		// @var $product O_product
		$product = $prodcut_model->getProduct($id_product);

		$titolo = $product->titolo;
		$codice_art = $product->codice;
		$iva = $product->iva;

		$imponibile_netto = $product->prezzo_vendita;
		if ($id_var != '') {
			// @var $variant O_variant_product
			$variant = $prodcut_model->getVariant($id_var);
			if ($variant != '') {
				$codice_art = $variant->codice;
				$imponibile_netto = $variant->prezzo_vendita;
				//$imposta=$variant->prezzo_vendita_ivato;
				$variant->getAttribute_variant();
				foreach ($variant->attribute_variant as $var) {
					$titolo .= ' - ' . $var->nome . ': ' . $var->valore;
				}
			}
		}
		$detail->id_ordine = $cart->id;
		$detail->id_prodotto = $product->id;
		$detail->id_variante = $id_var;
		$detail->codice = $codice_art;
		$detail->titolo = $titolo;
		$detail->quantita = $qta;
		$detail->peso_ar = $product->peso;
		$detail->iva_ar = $iva;
		$detail->prezzo = $imponibile_netto;
		$detail->prezzo_variabile = 0;
		$detail->setNew();
		return $cart->addDetail($detail);
	}

	/**
	 * Rimuove i dettagli dal carrello
	 * @param O_cart $cart
	 * @param array $id_detail
	 */
	private function removeDetails($cart, $id_detail)
	{
		if (is_array($id_detail)) {
			foreach ($id_detail as $key => $value) {
				if (!$cart->deleteDetail($value)) {
					return false;
				}
			}
		} else {
			if (!$cart->deleteDetail($id_detail)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Modifica quantità del dettaglio del carrello
	 * @param O_cart $cart
	 * @param array $qta
	 */
	private function updateQtaDetails($cart, $qta)
	{
		$_SESSION['CartErrors'] = '';

		$prodcut_model = Loader::loadModel('Products', true, 'BlueShop');

		foreach ($qta as $key => $value) {
			$error = '';

			$id_detail = $key;

			// @var $detail O_order_details
			$detail = $this->model->getDetail($id_detail);
			$id_var = '';
			if ($detail->id_variante != '' && !is_null($detail->id_variante) && isset($detail->id_variante)) {
				$id_var = $detail->id_variante;
			}

			if ($value == 0) {
				if ($this->removeDetails($cart, array($key))) {
					return true;
				}
				return false;
			}

			// @var $prod O_product
			$prod = $prodcut_model->getProduct($detail->id_prodotto);

			if ($prodcut_model->ProdIsAvailable($detail->id_prodotto, $id_var, $value)) {
				$prod->loadMultiple(BS_session::get_listino());

				if ($qta < $prod->conf_min) {
					$error = "Attenzione! Per l'articolo con codice \"" . $prod->codice . "\" il minimo d'ordine è di " . $prod->conf_min;
				} elseif (isset($prod->multiple) && $value % $prod->multiple != 0) {
					$error = "Attenzione! L'articolo con codice \"" . $prod->codice . '" viene venduto solo a multipli di ' . $prod->multiple;
				}

				if ($error != '') {
					$_SESSION['CartErrors'] .= $error . '<br/>';
				} else {
					if (!$cart->changeAmountDetail($id_detail, $value)) {
						throw new Exception('Error: chage amount detail');
					}
				}
			} else {
				$error = "Attenzione! L'articolo con codice \"" . $prod->codice . '" non è disponibile nella quantità richiesta!';
				$_SESSION['CartErrors'] .= $error . '<br/>';
			}
		}
		return true;
	}

	public function MyOrders()
	{
		$userId = BS_session::getUserId();

		if (is_null($userId)) {
			BS_session::clearUser();
			Utils::RedirectTo(UrlUtils::getActionUrl('Users/Login'));
			return;
		}

		// @var $OrdersModel Orders_Model
		$this->view->orders = $this->model->getOrders(BS_session::getUserId(), 3, 999999);

		$this->render('listOrders');
	}

	public function stepSetShipment()
	{
		$userId = BS_session::getUserId();

		$cart = $this->model->getCart($userId, session_id(), 'order');
		if ($cart === false) {
			Utils::RedirectTo(UrlUtils::getActionUrl('Orders/cart'));
			return;
		}
		if (!$this->cartAvailable($cart)) {
			$_SESSION['CartErrors'] = '[$PleaseCheckCart]';
			Utils::RedirectTo(UrlUtils::getActionUrl('Orders/cart'));
			return;
		}
		
		if (is_null($userId)) {
			BS_session::clearUser();
			Utils::RedirectTo(UrlUtils::getActionUrl('Users/Login'));
			return;
		}

		$id_order = $cart->id;

		// @var $order O_order
		$order = $this->model->getOrderInfo($_SESSION['user_id'], $id_order);
		if (!is_null($order->numero)) {
			Utils::RedirectTo(UrlUtils::getActionUrl('Orders/cart'));
			return;
		}

		$params = array();

		if (isset($_POST['step']) && $_POST['step'] != '') {
			if ($_POST['step'] == 'changeDestination' || $_POST['step'] == 'setShipmentMethod') {
				$order = $this->setDestination($order, $_POST);

				if (!isset($order->destinatario) || $order->destinatario == '') {
					$order->destinatario = $order->nome . ' ' . $order->cognome;
				}

				$this->view->nazioni = $this->model->getNazioni();
				$this->view->province = $this->model->getProvince();
				$this->view->comuni = $this->model->getComuni($order->provincia_dest);

				$user = BS_session::getUser();
				if ($user['UE'] == '0') {
					$params['prezzi_ivati'] = false;
					$params['extraUE'] = true;
					$params['EsenzioneIva'] = 'E08';	//Codice di esenzione iva per rivenditori extra UE
				} elseif ($user['UE'] == '1' && $user['country'] != 'Italia' && $user['business'] == '1') {
					$params['prezzi_ivati'] = false;
					$params['extraUE'] = false;
					$params['EsenzioneIva'] = 'E41';	//Codice di esenzione iva per rivenditori in UE ma fuori dall'Italia
				} else {
					$params['prezzi_ivati'] = BS_session::get_prezzi_ivati();
					$params['extraUE'] = false;
				}

				$params['valuta'] = BS_session::getValuta();
				$params['cambio'] = BS_session::getCambio();
				$params['valuta_code'] = $this->model->getValuteCode($params['valuta']);
				$this->view->params = $params;
				if (isset($order->nazione_dest) && $order->nazione_dest != '' &&
						isset($order->provincia_dest) && $order->provincia_dest != '' &&
						isset($order->comune_dest) && $order->comune_dest != '' &&
						isset($order->cap_dest) && $order->cap_dest != '' &&
						isset($order->indirizzo_dest) && $order->indirizzo_dest != ''
				) {
					$order->getPayment_methods();
					$order->getShipment_method();
				} else {
					$this->view->incompleto = true;
				}
				unset($order->metodo_spedizione);
				$order->spese_spedizione = 0;
				$order->getDetails();

				if ($_POST['step'] == 'changeDestination') {
					$this->view->order = $order;
					$this->render('createOrder_destination');
					return;
				}

				if (!$order->setShipment_method($_POST['method_shipment'])) {
					throw new Exception('Error: set destination order');
				}
				Utils::RedirectTo(UrlUtils::getActionUrl('Orders/stepSetPayment'));
				return;
			}
		} else {

			// questo if viene eseguito solo al momento della creazione dell'ordine quando tutti i campi della destinazione sono vuoti
			if (!isset($order->nazione_dest) || $order->nazione_dest == '' &&
					!isset($order->provincia_dest) || $order->provincia_dest == '' &&
					!isset($order->comune_dest) || $order->comune_dest == '' &&
					!isset($order->cap_dest) || $order->cap_dest == '' &&
					!isset($order->indirizzo_dest) || $order->indirizzo_dest == ''
			) {
				$order->destinatario = $order->nome . ' ' . $order->cognome;
				$order->nazione_dest = $order->nazione_cl;
				$order->provincia_dest = $order->provincia_cl;
				$order->comune_dest = $order->comune_cl;
				$order->cap_dest = $order->cap_cl;
				$order->indirizzo_dest = $order->indirizzo_cl;
			}

			$this->view->nazioni = $this->model->getNazioni();
			$this->view->province = $this->model->getProvince();
			$this->view->comuni = $this->model->getComuni($order->provincia_dest);
			$destination = array();
			$this->setDestination($order, $destination);
			$order->getPayment_methods();
			$shipment = $order->getShipment_method();

			$order->getDetails();

			$user = BS_session::getUser();
			if ($user['UE'] == '0') {
				$params['prezzi_ivati'] = false;
				$params['extraUE'] = true;
				$params['EsenzioneIva'] = 'E08';	//Codice di esenzione iva per rivenditori extra UE
			} elseif ($user['UE'] == '1' && $user['country'] != 'Italia' && $user['business'] == '1') {
				$params['prezzi_ivati'] = false;
				$params['extraUE'] = false;
				$params['EsenzioneIva'] = 'E41';	//Codice di esenzione iva per rivenditori in UE ma fuori dall'Italia
			} else {
				$params['prezzi_ivati'] = BS_session::get_prezzi_ivati();
				$params['extraUE'] = false;
			}

			unset($order->metodo_spedizione);
			$this->view->order = $order;
			$params['valuta'] = BS_session::getValuta();
			$params['cambio'] = BS_session::getCambio();
			$params['valuta_code'] = $this->model->getValuteCode($params['valuta']);
			$this->view->params = $params;
			$this->render('createOrder_destination');
			return;
		}
	}

	public function stepSetPayment()
	{
		$userId = BS_session::getUserId();

		if (is_null($userId)) {
			BS_session::clearUser();
			Utils::RedirectTo(UrlUtils::getActionUrl('Users/Login'));
			return;
		}

		$cart = $this->model->getCart($userId, session_id(), 'order');
		if ($cart === false) {
			Utils::RedirectTo(UrlUtils::getActionUrl('Orders/cart'));
			return;
		}
		if (!$this->cartAvailable($cart)) {
			$_SESSION['CartErrors'] = '[$PleaseCheckCart]';
			Utils::RedirectTo(UrlUtils::getActionUrl('Orders/cart'));
			return;
		}
		
		$id_order = $cart->id;

		// @var $order O_order
		$order = $this->model->getOrderInfo($_SESSION['user_id'], $id_order);
		if (!is_null($order->numero)) {
			Utils::RedirectTo(UrlUtils::getActionUrl('Orders/cart'));
			return;
		}

		if (isset($_POST['action']) && $_POST['action'] == 'setPaymentMethod') {
			if (isset($_POST['payment_method'])) {
				$paym = $order->getPayment_method($_POST['payment_method']);
				$setPaym = $this->model->setPaymentMethod($order->id, $_POST['payment_method'], $paym[0]->costo_aggiuntivo);
				if (!$setPaym) {
					$this->view->msgerror = 'Si è verificato un errore durante il salvataggio del Metodo di Pagamento!';
				} else {
					Utils::RedirectTo(UrlUtils::getActionUrl('Orders/stepSendOrder'));
					return;
				}
			} else {
				$this->view->msgerror = 'Non è stato selezionato il metodo di pagamento!';
			}
		}

		$users_model = Loader::loadModel('Users', true, 'BlueShop');
		$user = $users_model->getUser($order->id_cliente);
		$this->view->user = $user;
		$order->getPayment_methods();
		$order->getShipment_method();
		$order->getDetails();
		$this->view->order = $order;

		$params['aliqiva'] = $this->sito->iva_predefinita;

		$user = BS_session::getUser();
		if ($user['UE'] == '0') {
			$params['aliqiva'] = 0;
			$params['prezzi_ivati'] = false;
			$params['extraUE'] = true;
			$params['EsenzioneIva'] = 'E08';	//Codice di esenzione iva per rivenditori extra UE
		} elseif ($user['UE'] == '1' && $user['country'] != 'Italia' && $user['business'] == '1') {
			$params['aliqiva'] = 0;
			$params['prezzi_ivati'] = false;
			$params['extraUE'] = false;
			$params['EsenzioneIva'] = 'E41';	//Codice di esenzione iva per rivenditori in UE ma fuori dall'Italia
		} else {
			$params['prezzi_ivati'] = BS_session::get_prezzi_ivati();
			$params['extraUE'] = false;
		}

		$params['valuta'] = BS_session::getValuta();
		$params['cambio'] = BS_session::getCambio();
		$params['valuta_code'] = $this->model->getValuteCode($params['valuta']);

		$this->view->params = $params;
		$this->render('createOrder_payment');
	}

	public function stepSendOrder($notify = '')
	{
		if ($notify == 'OK') {
			$this->paymentOK();
			return;
		} elseif ($notify == 'KO') {
			$this->paymentKO();
			return;
		}

		$userId = BS_session::getUserId();

		if (is_null($userId)) {
			BS_session::clearUser();
			Utils::RedirectTo(UrlUtils::getActionUrl('Users/Login'));
			return;
		}

		$cart = $this->model->getCart($userId, session_id(), 'order');
		if ($cart === false) {
			Utils::RedirectTo(UrlUtils::getActionUrl('Orders/cart'));
			return;
		}
		if (!$this->cartAvailable($cart)) {
			$_SESSION['CartErrors'] = '[$PleaseCheckCart]';
			Utils::RedirectTo(UrlUtils::getActionUrl('Orders/cart'));
			return;
		}
		
		$id_order = $cart->id;

		// @var $order O_order
		$order = $this->model->getOrderInfo($_SESSION['user_id'], $id_order);

		if (!is_null($order->numero)) {
			Utils::RedirectTo(UrlUtils::getActionUrl('Orders/cart'));
			return;
		}

		$params['valuta'] = BS_session::getValuta();
		$params['cambio'] = BS_session::getCambio();
		$params['valuta_code'] = $this->model->getValuteCode($params['valuta']);

		$user = BS_session::getUser();
		if ($user['UE'] == '0') {
			$params['prezzi_ivati'] = false;
			$params['extraUE'] = true;
			$params['EsenzioneIva'] = 'E08';	//Codice di esenzione iva per rivenditori extra UE
		} elseif ($user['UE'] == '1' && $user['country'] != 'Italia' && $user['business'] == '1') {
			$params['prezzi_ivati'] = false;
			$params['extraUE'] = false;
			$params['EsenzioneIva'] = 'E41';	//Codice di esenzione iva per rivenditori in UE ma fuori dall'Italia
		} else {
			$params['prezzi_ivati'] = BS_session::get_prezzi_ivati();
			$params['extraUE'] = false;
		}

		$params['aliqiva'] = $this->sito->iva_predefinita;
		$this->view->params = $params;

		if (isset($_POST['action']) && $_POST['action'] == 'sendOrder') {
			if (!isset($_POST['policy']) || $_POST['policy'] != '1') {
				$this->msgerror = "Attenzione! E' necessario spuntare il check per l'accettazione delle Condizioni di Vendita";
			} else {
				if ($order->coupon_code == '' || $this->model->checkCoupon($order->coupon_code, $order->id_cliente)) {
					if (defined('DEVELOPMENT_ENVIRONMENT') && DEVELOPMENT_ENVIRONMENT == true) {
						echo "DEVELOP MODE FOR ORDERS IS ENABLED!<br/>\n\n";
						//$Sended = true;
						//$order->numero = date("YmdHi");
						$Sended = $this->model->sendOrder($id_order);
					} else {
						$Sended = $this->model->sendOrder($id_order);
					}
				} else {
					$Sended = false;
				}

				$users_model = Loader::loadModel('Users', true, 'BlueShop');

				if ($Sended) {
					$order = $this->model->getOrderInfo($_SESSION['user_id'], $id_order);
					$order->getPayment_method();
					$order->getShipment_method();
					$order->getDetails();
					//$this->model->payOrder($id_order,"");

					$user = $users_model->getUser($order->id_cliente);
					$to = $user->email;
					$replyTo = $this->sito->mittente;

					$message = new View();
					$message->setTerms($this->terms);

					// @var $order O_order

					//var_dump($order);

					$message->placeholder('ORDER_NUMBER')->setVal($order->numero);
					$message->placeholder('ORDER_DATE')->setVal(date_format(new DateTime($order->data_creazione), 'd/m/Y'));

					$message->placeholder('COMPANYNAME')->setVal($order->ragione_sociale);
					$message->placeholder('FIRSTNAME')->setVal($order->nome);
					$message->placeholder('LASTNAME')->setVal($order->cognome);
					$message->placeholder('ADDRESS')->setVal($order->indirizzo_cl);
					$message->placeholder('CITY')->setVal($order->comune_cl);
					$message->placeholder('PROV')->setVal($order->provincia_cl);
					$message->placeholder('CAP')->setVal($order->cap_cl);
					$message->placeholder('COUNTRY')->setVal($order->nazione_cl);
					$message->placeholder('PHONE')->setVal($user->telefono . (($user->cellulare != '') ? ' / ' . $user->cellulare : ''));
					$message->placeholder('PIVA')->setVal($order->partita_iva_cl);
					$message->placeholder('CFISC')->setVal($order->codice_fiscale_cl);
					$message->placeholder('DEST_DENOM')->setVal($order->destinatario);
					$message->placeholder('DEST_ADDRESS')->setVal($order->indirizzo_dest);
					$message->placeholder('DEST_CAP')->setVal($order->cap_dest);
					$message->placeholder('DEST_CITY')->setVal($order->comune_dest);
					$message->placeholder('DEST_PROV')->setVal($order->provincia_dest);
					$message->placeholder('DEST_COUNTRY')->setVal($order->nazione_dest);

					$message->placeholder('ORDER_SHIPMENT')->setVal($order->shipment_method[0]->descrizione);
					$message->placeholder('ORDER_PAYMENT')->setVal($order->payment_methods[0]->descrizione);

					switch ($order->payment_methods[0]->id) {
						case '10':
							$bank = $order->getPayment_method(10);
							break;
						default:
							$bank = $order->getPayment_method(2);
							break;
					}
					$message->placeholder('ORDER_BANK_INFO')->setVal($bank[0]->valore);
					$order->getPayment_method();

					$message->placeholder('SITENAME')->setVal($this->sito->titolo_sito_it);
					$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim($this->sito->url_sito, '/')));
					$message->placeholder('URL_MYACCOUNT')->setVal(UrlUtils::getActionUrl('Users/myaccount', true));
					$message->placeholder('URL_LOGO')->setVal(UrlUtils::genUrl('content/media/images/logo.png?w=128&f=png', true));

					$message->user = $user;
					$message->order = $order;
					$message->params = $params;
					$message->impostazioni = $this->sito;
					$user = BS_session::getUser();
					if ($user['UE'] == '0') {
						$message->prezzi_ivati = false;
						$message->extraUE = true;
						$message->EsenzioneIva = 'E08';	//Codice di esenzione iva per rivenditori extra UE
					} elseif ($user['UE'] == '1' && $user['country'] != 'Italia' && $user['business'] == '1') {
						$message->prezzi_ivati = false;
						$message->extraUE = false;
						$message->EsenzioneIva = 'E41';	//Codice di esenzione iva per rivenditori in UE ma fuori dall'Italia
					} else {
						$message->prezzi_ivati = BS_session::get_prezzi_ivati();
						$message->extraUE = false;
					}
					$message->cambio = BS_session::getCambio();
					$message->valuta = BS_session::getValuta();
					$subject = $this->terms['subjectOrderNumber'] . ' ' . $order->numero;
					$txt = $message->render_module('BlueShop/Orders', 'mails/mail_Order', false, true);
					Email::sendMail($to, $this->sito->titolo_sito_it, $replyTo, $subject, $txt);

					$currLang = Lang::getCurrent();
					Lang::setLang('it');
					$adminTerms = Lang::loadTerms(dirname(__FILE__) . '/lang/');
					Lang::setLang($currLang);
					$message->setTerms($adminTerms);
					$txt = $message->render_module('BlueShop/Orders', 'mails/mail_admin_Order', false, true);
					Email::sendMail($this->sito->destinatario, $this->sito->titolo_sito_it, $to, "Nuovo Ordine ($order->numero)", $txt);
				} else {
					$this->view->msgerror = "Siamo spiacenti, per motivi tecnici non è possibile trasmettere l'ordine.";
				}

				$user = $users_model->getUser($order->id_cliente);
				$this->view->user = $user;
				$this->view->impostazioni = $this->sito;
				$this->view->order = $order;
				$this->render('Confirm');
				return;
			}
		}

		$order->getPayment_method();
		$order->getShipment_method();
		$order->getDetails();

		$this->view->order = $order;
		$users_model = Loader::loadModel('Users', true, 'BlueShop');
		$user = $users_model->getUser($order->id_cliente);
		$this->view->user = $user;

		$this->render('createOrder_send');
	}

	/**
	 *
	 * @param O_order $order
	 * @param array $destination
	 */
	private function setDestination($order, $destination)
	{
		if (isset($destination['destinatario'])) {
			$order->destinatario = $destination['destinatario'];
		}
		if (isset($destination['nazione'])) {
			$order->nazione_dest = $destination['nazione'];
		}
		if ($order->nazione_dest == 'Italia') {
			if (isset($destination['prov'])) {
				$order->provincia_dest = $destination['prov'];
			}
			if (isset($destination['citta'])) {
				$order->comune_dest = $destination['citta'];
			}
		} else {
			if (isset($destination['prov'])) {
				$order->provincia_dest = $destination['provincia_estero'];
			}
			if (isset($destination['citta_estero'])) {
				$order->comune_dest = $destination['citta_estero'];
			}
		}
		if (isset($destination['indirizzo'])) {
			$order->indirizzo_dest = $destination['indirizzo'];
		}
		if (isset($destination['cap'])) {
			$order->cap_dest = $destination['cap'];
		}

		if (!$order->setDestination()) {
			return false;
		}
		return $order;
	}

	public function OrderSummary($id)
	{
		$userId = BS_session::getUserId();

		if (is_null($userId)) {
			BS_session::clearUser();
			Utils::RedirectTo(UrlUtils::getActionUrl('Users/Login'));
			return;
		}

		// @var $order O_order
		$order = $this->model->getOrderInfo($userId, $id);

		if ($order === false || is_object($order) && is_null($order->numero)) {
			$errorView = new View('error/index');
			$errorView->err = 'Invalid Address!';
			$errorView->render();
			return;
		}

		$order->getPayment_method();
		$order->getShipment_method();
		$order->getDetails();

		$this->view->order = $order;
		$this->view->PaymentsAvailable = $order->getPayment_methods();

		$params['valuta'] = BS_session::getValuta();
		$params['cambio'] = BS_session::getCambio();
		$params['valuta_code'] = $this->model->getValuteCode($params['valuta']);

		$user = BS_session::getUser();
		if ($user['UE'] == '0') {
			$params['prezzi_ivati'] = false;
			$params['extraUE'] = true;
			$params['EsenzioneIva'] = 'E08';	//Codice di esenzione iva per rivenditori extra UE
		} elseif ($user['UE'] == '1' && $user['country'] != 'Italia' && $user['business'] == '1') {
			$params['prezzi_ivati'] = false;
			$params['extraUE'] = false;
			$params['EsenzioneIva'] = 'E41';	//Codice di esenzione iva per rivenditori in UE ma fuori dall'Italia
		} else {
			$params['prezzi_ivati'] = BS_session::get_prezzi_ivati();
			$params['extraUE'] = false;
		}

		$params['aliqiva'] = $this->sito->iva_predefinita;
		$this->view->params = $params;

		$users_model = Loader::loadModel('Users', true, 'BlueShop');
		$user = $users_model->getUser($userId);
		$this->view->user = $user;

		$this->render('OrderSummary');
	}

	public function sendMail_Order($order)
	{
		$message = new View();
		$message->setTerms($this->terms);

		$userModel = Loader::loadModel('Users', true, 'BlueShop');
		$user = $userModel->getUser($order->id_cliente);

		// @var $order O_order
		$message->placeholder('ORDER_NUMBER')->setVal($order->numero);
		$message->placeholder('ORDER_DATE')->setVal(date_format(new DateTime($order->data_creazione), 'd/m/Y'));

		$message->placeholder('COMPANYNAME')->setVal($order->ragione_sociale);
		$message->placeholder('FIRSTNAME')->setVal($order->nome);
		$message->placeholder('LASTNAME')->setVal($order->cognome);
		$message->placeholder('ADDRESS')->setVal($order->indirizzo_cl);
		$message->placeholder('CITY')->setVal($order->comune_cl);
		$message->placeholder('PROV')->setVal($order->provincia_cl);
		$message->placeholder('CAP')->setVal($order->cap_cl);
		$message->placeholder('COUNTRY')->setVal($order->nazione_cl);
		$message->placeholder('PHONE')->setVal($user->telefono . (($user->cellulare != '') ? ' / ' . $user->cellulare : ''));
		$message->placeholder('PIVA')->setVal($order->partita_iva_cl);
		$message->placeholder('CFISC')->setVal($order->codice_fiscale_cl);
		$message->placeholder('DEST_DENOM')->setVal($order->destinatario);
		$message->placeholder('DEST_ADDRESS')->setVal($order->indirizzo_dest);
		$message->placeholder('DEST_CAP')->setVal($order->cap_dest);
		$message->placeholder('DEST_CITY')->setVal($order->comune_dest);
		$message->placeholder('DEST_PROV')->setVal($order->provincia_dest);
		$message->placeholder('DEST_COUNTRY')->setVal($order->nazione_dest);

		$message->placeholder('ORDER_SHIPMENT')->setVal($order->shipment_method[0]->descrizione);
		$message->placeholder('ORDER_PAYMENT')->setVal($order->payment_methods[0]->descrizione);

		$bank = $order->getPayment_method(2);
		$message->placeholder('ORDER_BANK_INFO')->setVal($bank[0]->valore);

		$message->placeholder('SITENAME')->setVal($this->sito->titolo_sito_it);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim($this->sito->url_sito, '/')));
		$message->placeholder('URL_MYACCOUNT')->setVal(UrlUtils::getActionUrl('Users/myaccount', true));
		$message->placeholder('URL_LOGO')->setVal(UrlUtils::genUrl('content/media/images/logo.png?w=128&f=png', true));

		$to = $user->email;

		if ($user->UE == '0') {
			$message->prezzi_ivati = false;
			$message->extraUE = true;
			$message->EsenzioneIva = 'E08';	//Codice di esenzione iva per rivenditori extra UE
		} elseif ($user->UE == '1' && $user->nazione != 'Italia' && $user->business == '1') {
			$message->prezzi_ivati = false;
			$message->extraUE = false;
			$message->EsenzioneIva = 'E41';	//Codice di esenzione iva per rivenditori in UE ma fuori dall'Italia
		} else {
			$message->prezzi_ivati = BS_session::get_prezzi_ivati();
			$message->extraUE = false;
		}
		$message->cambio = BS_session::getCambio();
		$message->valuta = BS_session::getValuta();
		$message->user = $user;
		$message->order = $order;
		//$message->params = $params;
		$message->impostazioni = $this->sito;
		$subject = $this->terms['subjectOrderNumber'] . ' ' . $order->numero;
		$txt = $message->render_module('BlueShop/Orders', 'mails/mail_Order', false, true);
		if (Email::sendMail($to, $this->sito->titolo_sito_it, $this->sito->mittente, $subject, $txt)) {
			return true;
		}
		
		return false;
	}

	public function sendMail_OrderStatus($order)
	{
		if (($this->sito->feedaty_enabled == '1' && $order->stato == 'Evaso')) {
			require_once CLASSES_PATH . 'Feedaty.php';
			$fd_apisecret = $this->sito->feedaty_api_secret;
			$fd_merchant_code = $this->sito->feedaty_merchant_code;
			$Feedaty = new Feedaty($fd_merchant_code, $fd_apisecret);
			$Feedaty->SendOrder($order->id);
			return true;
		}

		$message = new View();
		$message->setTerms($this->terms);

		// @var $order O_order
		$order->getPayment_method();
		$order->getShipment_method();

		$message->placeholder('ORDER_NUMBER')->setVal($order->numero);
		$message->placeholder('ORDER_DATE')->setVal(date_format(new DateTime($order->data_creazione), 'd/m/Y'));
		$message->placeholder('ORDER_STATUS')->setVal($order->stato);
		$message->placeholder('ORDER_TRACKING')->setVal($order->tracking_number);
		$message->placeholder('ORDER_URL_TRACKING')->setVal($order->shipment_method[0]->url_tracking);

		$message->placeholder('COMPANYNAME')->setVal($order->ragione_sociale);
		$message->placeholder('FIRSTNAME')->setVal($order->nome);
		$message->placeholder('LASTNAME')->setVal($order->cognome);
		$message->placeholder('ADDRESS')->setVal($order->indirizzo_cl);
		$message->placeholder('CITY')->setVal($order->comune_cl);
		$message->placeholder('PROV')->setVal($order->provincia_cl);
		$message->placeholder('CAP')->setVal($order->cap_cl);
		$message->placeholder('COUNTRY')->setVal($order->nazione_cl);
		//$message->placeholder('PHONE')->setVal($user->telefono . (($user->cellulare!="")?" / " . $user->cellulare:""));
		$message->placeholder('DEST_DENOM')->setVal($order->destinatario);
		$message->placeholder('DEST_ADDRESS')->setVal($order->indirizzo_dest);
		$message->placeholder('DEST_CAP')->setVal($order->cap_dest);
		$message->placeholder('DEST_CITY')->setVal($order->comune_dest);
		$message->placeholder('DEST_PROV')->setVal($order->provincia_dest);
		$message->placeholder('DEST_COUNTRY')->setVal($order->nazione_dest);

		//$message->placeholder('ORDER_SHIPMENT')->setVal($order->shipment_method[0]->descrizione);
		//$message->placeholder('ORDER_PAYMENT')->setVal($order->payment_methods[0]->descrizione);

		$message->placeholder('SITENAME')->setVal($this->sito->titolo_sito_it);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim($this->sito->url_sito, '/')));
		$message->placeholder('URL_MYACCOUNT')->setVal(UrlUtils::getActionUrl('Users/myaccount', true));
		$message->placeholder('URL_LOGO')->setVal(UrlUtils::genUrl('content/media/images/logo.png?w=128&f=png', true));

		$userModel = Loader::loadModel('Users', true, 'BlueShop');
		$user = $userModel->getUser($order->id_cliente);

		$to = $user->email;

		if ($user->UE == '0') {
			$message->prezzi_ivati = false;
			$message->extraUE = true;
			$message->EsenzioneIva = 'E08';	//Codice di esenzione iva per rivenditori extra UE
		} elseif ($user->UE == '1' && $user->nazione != 'Italia' && $user->business == '1') {
			$message->prezzi_ivati = false;
			$message->extraUE = false;
			$message->EsenzioneIva = 'E41';	//Codice di esenzione iva per rivenditori in UE ma fuori dall'Italia
		} else {
			$message->prezzi_ivati = BS_session::get_prezzi_ivati();
			$message->extraUE = false;
		}
		$message->cambio = BS_session::getCambio();
		$message->valuta = BS_session::getValuta();
		$message->user = $user;
		$message->order = $order;
		//$message->params = $params;
		$message->impostazioni = $this->sito;

		$subject = $this->terms['subjectUpdatingOrderNumber'] . ' ' . $order->numero;

		$dicitura_stato = str_replace(' ', '', ucwords($order->stato));

		$txt = $message->render_module('BlueShop/Orders', 'mails/mail_OrderStatus_' . $dicitura_stato, false, true);
		if (Email::sendMail($to, $this->sito->titolo_sito_it, $this->sito->mittente, $subject, $txt)) {
			return true;
		}
			
		return false;
	}

	private function cartAvailable($cart)
	{
		$product_model = Loader::loadModel('Products', true, 'BlueShop');

		$cart->getDetails();
		if (is_array($cart->details)) {
			foreach ($cart->details as $det) {
				if (!$product_model->ProdIsAvailable($det->id_prodotto, $det->id_variante, $det->quantita)) { //VERIFICA DISPONIBILITà DEI PRODOTTI
					return false;
				}
				$prod = $product_model->getProduct($det->id_prodotto);
				if ($det->quantita < $prod->conf_min) {
					return false;
				}
				$prod->loadMultiple(BS_session::get_listino());
				if (isset($prod->multiple) && $det->quantita % $prod->multiple != 0) {
					return false;
				}
			}
		}

		if ($this->checkMinOrder($cart) === false) {
			return false;
		}

		return true;
	}

	public function insertCoupon()
	{
		if (isset($_REQUEST['coupon_code'])) {
			$code = $_REQUEST['coupon_code'];
			$backUrl = $_REQUEST['backUrl'];

			$userid = (isset($_SESSION['user_id'])) ? $_SESSION['user_id'] : '';

			if ($this->model->checkCoupon($code, $userid)) {
				$cart = $this->model->getCart($userid, session_id());
				if ($cart != false) {
					$discountValue = $this->model->getCouponDiscountVal($code, $cart->totale);

					$this->model->insertCoupon($cart->id, $code, $discountValue);

					BS_session::Notice('[$CouponAppliedSuccessfully]');
				} else {
					error('Cart of user not exist!');
				}

				Utils::RedirectTo($backUrl);
				return;
			}

			BS_session::Error("[\$Coupon] $code [\$is_not_valid]!");
		} else {
			BS_session::Error('coupon not sended');
		}

		Utils::RedirectTo($backUrl);
	}

	public function removeCoupon()
	{
		$userid = (isset($_SESSION['user_id'])) ? $_SESSION['user_id'] : '';
		$cart = $this->model->getCart($userid, session_id());
		$this->model->removeCoupon($cart->id);
		$backUrl = $_REQUEST['backUrl'];
		Utils::RedirectTo($backUrl);
	}

	public function checkPaymentS2S($service)
	{
		if ($service == 'GestPayTest') {
			$url = 'https://testecomm.sella.it/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL';
			$service = 'GestPay';
		} elseif ($service == 'GestPay') {
			$url = 'https://ecomms2s.sella.it/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL';
		}

		switch ($service) {
			case 'GestPay':

				$client = new SoapClient($url);

				$params->shopLogin = $_GET['a'];
				$params->CryptedString = $_GET['b'];
				$objectresult = $client->Decrypt($params);
				$simpleresult = $objectresult->DecryptResult;

				$xml = simplexml_load_string($simpleresult->any);

				//Se la transazione è andata a buon fine, scrive i dati
				if ($xml->TransactionResult == 'OK') {
					$ShopTransactionID = $xml->ShopTransactionID;
					echo $ShopTransactionID . '
                    ';

					$BankTransactionID = $xml->BankTransactionID;
					echo $BankTransactionID . '
                    ';

					$AuthorizationCode = $xml->AuthorizationCode;
					echo $AuthorizationCode . '
                    ';

					$Currency = $xml->Currency;
					echo $Currency . '
                    ';

					$Amount = $xml->Amount;
					echo $Amount . '
                    ';

					$BuyerName = $xml->BuyerName;
					echo $BuyerName . '
                    ';

					$BuyerEmail = $xml->BuyerEmail;
					echo $BuyerEmail . '
                    ';

					$testo = "ShopTransactionID => $ShopTransactionID";
					//Email::sendMail(DEVELOPMENT_MAIL, 'BlueShop', 'noreply@blueshop.it' , 'S2S GestPay', $testo);

					// @var $order O_order
					$order = $this->model->getOrderInfo(null, $ShopTransactionID);
					if ($order->totale == $Amount) {
						$this->model->payOrder($order->id);
					}
				}

				//Altrimenti restituisce l'errore
				else {
					$error = $xml->ErrorCode . ' – ' . $xml->ErrorDescription;
					Email::sendMail('sviluppo@bluehat.it', 'BlueShop', 'noreply@blueshop.it', 'S2S GestPay', $error);
				}

				break;
		}
	}

	public function paymentOK($prov = '', $order_id = '')
	{
		ob_start();

		$result = false;

		print_r($_REQUEST);
		echo '<br/><br/><br/>';

		if (isset($_GET['provider']) || $prov != '') {
			$provider = (isset($_GET['provider'])) ? $_GET['provider'] : $prov;

			if ($provider == 'bnl') {
				if (isset($_REQUEST['status']) && ($_REQUEST['status'] == 'APPROVATO' || $_REQUEST['status'] == 'APPROVED' || $_REQUEST['status'] == 'GENEHMIGT')) {
					$onum = $_REQUEST['oid'];

					// @var $order O_order
					$order = $this->model->getOrderInfoByNum($onum);
					include(dirname(__FILE__) . '/libs/Payments/libs/ipg-util.php');

					$date = new DateTime($order->data);
					$DateTime = $date->format('Y:m:d-H:i:s');
					$o_tot_std = number_format($order->totale, 2, '.', '');

					$payment = $order->getPayment_method(11);
					$params = $this->getPayParams($payment[0]);
					if (defined('DEVELOPMENT_ENVIRONMENT') && DEVELOPMENT_ENVIRONMENT == true) {
						$storename = '08000001_S';
						$ksig = 'xHosiSb08fs8BQmt9Yhq3Ub99E8=';
					} else {
						$storename = $params['storename'];
						$ksig = rtrim($params['ksig'], '=') . '=';
					}

					//echo $ksig ."<br/>". $_REQUEST['approval_code'] ."<br/>". $o_tot_std ."<br/>". "EUR" ."<br/>". $DateTime ."<br/>". $storename;

					$hash = sha1(bin2hex($ksig . $_REQUEST['approval_code'] . $o_tot_std . 'EUR' . $DateTime . $storename));

					//echo "calculated hash = $hash <br/> response_hash = ".$_REQUEST['response_hash'];

					if ($hash == $_REQUEST['response_hash']) {
						echo '<br/>hash is OK ------- Set Ordine Pagato';
						$result = true;
					} else {
						echo '<br/>hash NOT is correct ------- Ordine NON pagato';
						$result = false;
					}
				} else {
					echo '<br/>Transazione non Approvata';
					$result = false;
				}
			}
		}

		$testo = ob_get_contents();
		ob_end_clean();

		if (defined('DEVELOPMENT_ENVIRONMENT') && DEVELOPMENT_ENVIRONMENT == true) {
			Email::sendMail(DEVELOPMENT_MAIL, 'BlueShop', 'noreply@blueshop.it', 'S2S BNL POSitivity', $testo);
		}

		if ($result) {
			echo '<br/>Complimenti, il suo pagamento è andato a buon fine!';
		} else {
			echo '<br/>Attenzione, per un problema tecnico non è stato possibile verificare il pagamento!';
		}

		return $result;
	}

	public function paymentKO()
	{
		echo 'Siamo spiacenti, si è verificato un errore durante la procedura di pagamento!';
		echo '<br/>';
	}

	private function getPayParams($paym)
	{
		$params = array();

		$arr = explode(';', $paym->valore);
		if (count($arr)) {
			foreach ($arr as $param) {
				$row = explode('=', $param);
				if (count($row)) {
					$row[0] = strtolower($row[0]);
					$params[$row[0]] = $row[1];
				}
			}
		} else {
			return array();
		}

		return $params;
	}

	/**
	 * @var string payservice PP for PayPal; GP for GestPay; BNL for Positivity; Sella for Banca Sella;
	 */
	public function checkPayments($payservice, $id_order)
	{

		// @var $order O_order
		$order = $this->model->getOrderInfo(null, $id_order);

		include(dirname(__FILE__) . '/libs/Payments/Payments.php');

		$result = false;

		$pay = new Payments($id_order);

		switch ($payservice) {
			case 'PP':
				$result = $pay->listenerPaypal($order->numero, $order->totale);
				break;
			case 'BNL':
				$result = $this->paymentOK('bnl');
				break;
		}

		if ($result) {
			$this->model->payOrder($order->id);
			//$this->executeCallback($order->id);
		}
	}

	public function testFeedaty($order_id)
	{
		require_once CLASSES_PATH . 'Feedaty.php';
		$fd_apisecret = $this->sito->feedaty_api_secret;
		$fd_merchant_code = $this->sito->feedaty_merchant_code;
		$Feedaty = new Feedaty($fd_merchant_code, $fd_apisecret);
		$Feedaty->setDebugMode(true);
		$Feedaty->SendOrder($order_id);
		return true;
	}
}
