<?php

require_once LIBS_PATH . 'StringUtils.php';

class Products_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_products_list', $this->getActionUrl('products_list'));
		require_once CONFIG_PATH . 'blueshop.php';
		$this->model = Loader::getModel('BSProducts', 'BlueShopV1');
	}

	public function products_list($id_category = null)
	{
		if (!is_null($id_category) && !is_numeric($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}

		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$Product_tbl = 'prodotti';
		$sorting = '';
		$filter = '1 AND visibile=1 ';
		if (!is_null($id_category)) {
			$filter .= "AND ($Product_tbl.id_categoria = {$id_category}) ";
		}
		$products = $this->model->getList($elements_per_page, $offset, $filter, $sorting);
		$totalElements = $this->model->getLastCounter();
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('products', $products);
		HeadHTML::setTitleTag('Products' . ' | ' . CMSSettings::$website_title);
		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		//BreadCrumb
		if (!is_null($id_category)) {
			//to do
		}
		//WebPage::$breadcrumb->addDir('[$Products]', $this->getActionUrl('products_list'));
		//HeadHTML::setTitleTag('Products' . ' | ' . CMSSettings::$website_title);
		
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'products-list';
		$this->view->render($view);
	}

	public function product_details($id, $id_variante = null)
	{
		$id_product = $id;
		// Se esco fuori prima di aver caricato il prodotto eventuali widget che condividono id_prodotto dal model non funzioneranno
		if (is_array($id)) {
			$id_product = $id[count($id) - 1];
			if (!$this->model->check($id)) {
				echo('Prodotto non trovato!');
				return;
			}
		}

		$product = null;
		if (is_numeric($id)) {
			$product = $this->model->getProduct($id_product);
		}
		if (!is_numeric($id)) {
			$alias = mb_convert_encoding((string) $id, 'UTF-8', mb_list_encodings());
			$id_product = substr($alias, strrpos($alias, '-') + 1);
			$product = $this->model->getProduct($id_product);
			//echo UrlUtils::url_slug($product->titolo).'-'.$id_product . "<br/>" . $alias;exit;
			//if($alias != UrlUtils::url_slug($product->titolo).'-'.$id_product) {
			//    Utils::RedirectTo(UrlUtils::genUrl('/error/404'));
			//    return;
			//}
		}

		if ($product == null || $product->id == null) {
			Utils::RedirectTo(UrlUtils::genUrl('/error/404'));
			return;
		} elseif ($id_variante != null && $product->geVariantById($id_variante) == null) {
			Utils::RedirectTo(UrlUtils::getActionUrl('Products/productDetails/') . $this->getSeoString($product));
			return;
		}

		$params = array(
			'w_img' => 430,
			'h_img' => 443,
			'zoom_mini' => 8,
			'zoom_rel_prod' => 7,
			'withImgs' => 3,
			'withRelatedProducts' => 3,
			'view' => 'default',
			'withCart' => true,
			'qta_add' => 10,
			'withShare' => true,
			'withVariants' => true,
			'requestInfo' => true,
			'withBreadcrumb' => false,
			'prezzi_ivati' => true,
			'showFlags' => false
		);
		$params_view = array('view' => '');
		//        if ($this->PositionParams) {
//
//
		//            foreach ($params as $key => $value) {
		//                if (array_key_exists($key, $this->PositionParams)) {
		//                    $params[$key] = $this->PositionParams[$key];
		//                }
		//            }
		//            foreach ($params_view as $key => $value) {
		//                if (array_key_exists($key, $this->PositionParams)) {
		//                    $params_view[$key] = $this->PositionParams[$key];
		//                }
		//            }
		//        }
		//$this->view->settings = $this->sito;
		$this->view->set('settings', array());

		// @var $product O_product
		$attributes = array();
		$selected_attributes = array();

		if (!is_null($product)) {
			if ($product->visibile != '1') {
				Utils::RedirectTo('/');
				return;
			}

			Session::set('urlback', Utils::getComponentUrl('Products/productDetails/') . $this->getSeoString($product));

			$product->getCaratteristiche();

			if (isset($_POST['attributes'])) {
				$id_variante = $this->model->findVarianteByAttributes($_POST['attributes'], $product->id);
				$selected_attributes = $_POST['attributes'];
				if (!is_null($id_variante)) {
					Utils::RedirectTo(UrlUtils::getActionUrl('Products/' . $params_view['view'] . 'productDetails/' . $this->getSeoString($product)) . '/' . $id_variante);
				}
			}
			if ($params['withImgs']) {
				$product->getImagesId();
				//                $product->getImages();
				for ($i = 0; $i < count($product->imgs); $i++) {
					$product->imgs[$i]['url'] = $this->model->getImgUrl($product->imgs[$i]['id']);
					//                    $product->imgs[$i]['url'] = WEBROOT . UrlUtils::getMediaUrl("/media/products/".$product->imgs[$i]['filename']);
//                    if($_SERVER['HTTP_HOST']=='www.convenienzaufficio.it')
//                        echo $product->imgs[$i]['url'];
				}
			}
			if ($params['withRelatedProducts']) {
				$product->getRelated_product();
			}
			if (!isset($_POST['attributes'])) {
				if (is_null($id_variante)) {
					$product->getVariant_product();
				} else {
					$attributes_variant = $product->geAttribite_variant($id_variante);
					$attributes_v = array();
					for ($i = 0; $i < count($attributes_variant); $i++) {
						$attributes_v[$attributes_variant[$i]->id_attributo] = $attributes_variant[$i]->valore;
					}
					$product->getVariant_product($attributes_v);
					$selected_attributes = $attributes_v;
				}
			} else {
				$product->getVariant_product($_POST['attributes']);
			}

			if ($product->variant_product != false && count($product->variant_product) > 0) {
				for ($i = 0; $i < count($product->variant_product); $i++) {
					$values = $product->variant_product[$i]->getAttribute_variant();
					for ($j = 0; $j < count($values); $j++) {
						$value = $values[$j];

						if (!isset($attributes[$value->nome])) {
							$attributes[$value->nome] = array();
							$attributes[$value->nome][0] = $value->id_attributo;
							$attributes[$value->nome][1] = array();
							$attributes[$value->nome][2] = array();
						}
						if (!is_null($id_variante) && $id_variante == $product->variant_product[$i]->id) {
							//$selected_attributes[$value->id_attributo]=$value->valore;
						}
						$attributes[$value->nome][1][$value->valore] = $value->id;
						$attributes[$value->nome][2][$value->valore] = $value->background;
					}
				}
			}
			if (!is_null($id_variante)) {
				$variante = $product->geVariantById($id_variante);
				if (count($variante)) {
					$product->iva = $variante[0]->iva;
					$product->prezzo_listino = $variante[0]->prezzo_listino;
					$product->prezzo_vendita = $variante[0]->prezzo_vendita;

					$product->prezzo_listino_ivato = $variante[0]->prezzo_listino_ivato;
					$product->prezzo_vendita_ivato = $variante[0]->prezzo_vendita_ivato;

					$product->disponibilita = $variante[0]->disponibilita;
					$product->id_disponibilita = $variante[0]->id_disponibilita;
					$product->quantita_disponibile = $variante[0]->quantita_disponibile;

					$product->codice = $variante[0]->codice;
					$product->sconto = $variante[0]->sconto;
					$product->risparmio = $variante[0]->risparmio;
					$product->risparmio_ivato = $variante[0]->risparmio_ivato;
				}
			}
			$this->view->set('attributes', $attributes);
			$this->view->set('var_id', $id_variante);

			//            $breadcrumb_page = $this->PAGE->getBreadcrumb();
			//            $breadcrumb = $this->model->getBreadCrumb($product->id_categoria);
			//            for ($i = 0; $i < count($breadcrumb); $i++) {
			//                $breadcrumb_page->addDir($breadcrumb[$i]["name"], $breadcrumb[$i]['url']);
			//            }
			//            $breadcrumb_page->addDir($product->titolo, "");
			//            $this->PAGE->setTitle($product->titolo);

			WebPage::$breadcrumb->addDir('...', '');

			if ($params['withBreadcrumb']) {
				$breadcrumb = array_reverse($breadcrumb);
				$crumb = array();
				$crumb['name'] = $product->titolo;
				$crumb['url'] = '';
				array_push($breadcrumb, $crumb);
				$this->nameSito = $_SERVER['HTTP_HOST'];
				$this->view->set('nameSito', $_SERVER['HTTP_HOST']);
				$this->view->set('breadcrumb', $breadcrumb);
			} else {
				$this->view->set('breadcrumb', null);
				$this->view->set('nameSito', $_SERVER['HTTP_HOST']);
			}
			$product->titolo = ucfirst($product->titolo);
			$this->view->set('id_varinte', $id_variante);
			$product->loadMultiple(BS_session::get_listino());
			$this->view->set('product', $product);
			$rel_products = $product->related_product;
			$this->view->set('params', $params);

			$this->view->set('valuta', BS_session::getValuta());
			$this->view->set('cambio', BS_session::getCambio());
			$this->view->set('selected_attributes', $selected_attributes);

			$this->view->set('brand_img', $this->model->getImgUrl($product->id_marca, 'marca'));
			$this->view->set('linkBrandProducts', Utils::getComponentUrl('Products/getListProducts/?type_search=brand&params_search=' . $product->marca));

			if (isset($_SESSION['addCartErrors'])) {
				$this->AddError($_SESSION['addCartErrors']);
				unset($_SESSION['addCartErrors']);
			}
			if (isset($_SESSION['addCartMessage'])) {
				$this->AddNotice($_SESSION['addCartMessage']);
				unset($_SESSION['addCartMessage']);
			}

//
			//            if (isset($this->sito->metaTitle))
			//                Headers::setTitleTag($product->titolo . ' - ' . $this->sito->metaTitle);

			HeadHTML::setTitleTag($product->titolo);

			if ($product->metakeywords == '') {
				HeadHTML::setMetaTag('keywords', $product->titolo);
			} else {
				HeadHTML::setMetaTag('keywords', $product->metakeywords);
			}
			if ($product->metadescription != '') {
				HeadHTML::setMetaTag('description', substr(trim(preg_replace('/\s+/', ' ', $product->metadescription)), 0, 255));
			} else {
				HeadHTML::setMetaTag('description', substr(trim(preg_replace('/\s+/', ' ', $product->descrizione)), 0, 255));
			}

			$this->view->set('microdata', '');

			//            if( ($this->sito->feedaty_enabled == '1') ) {
//
			//                $file = "http://white.zoorate.com/gen?w=wp&MerchantCode=".$this->sito->feedaty_merchant_code."&t=microdata&sku=".$product->id;
			//                $file_headers = @get_headers($file); //echo $file_headers[0];
			//                if($file_headers[0] != 'HTTP/1.1 404 Not Found' && $file_headers[0] != 'HTTP/1.0 500 Internal Server Error') {
			//                    $this->view->microdata = file_get_contents($file);
			//                    // file_put_contents("microdata.html",$cnt);
			//                }
			//            }

			$this->view->render((isset($parameters['view'])) ? $parameters['view'] : 'details');
		} else {
			Utils::RedirectTo(Utils::getComponentUrl('Webpage/render_error/404'));
		}

		return;

		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		} elseif (is_null($id)) {
			Utils::RedirectTo(Utils::getComponentUrl('Webpage/render_error/404'));
		}
		$product = $this->model->getProduct($id);

		if (is_null($product)) {
			Utils::RedirectTo($this->getActionUrl('products_list'));
			return;
		}

		$this->view->set('product', $product);

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'product' . ((!is_null($product->id_categoria)) ? "/{$product->id_categoria}" : ''));

		//BreadCrumb
		//WebPage::$breadcrumb->addDir($business->category, Utils::getComponentUrl('Businesses/businesses_list/' . $business->id_category));
		//$this->addCategoriesToBreadCrumb($product->id_categoria);
		//WebPage::$breadcrumb->addDir(StringUtils::CutString($product->titolo_it, 50), $this->getActionUrl('product_details') . Utils::url_slug($product->titolo_it) . '-' . $id);
		//Title tag
		HeadHTML::setTitleTag(StringUtils::CutString($product->titolo_it, 50) . ' | ' . CMSSettings::$website_title);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'details';
		$this->view->render($view);
	}

	private function addCategoriesToBreadCrumb($id_categ)
	{
		$category = $this->model->getCategory($id_categ);
		if (is_null($category->parent_id)) {
			WebPage::$breadcrumb->addDir($category->category, Utils::getComponentUrl('Businesses/business_list/' . $id_categ));
		} else {
			$this->addCategoryToBreadCrumb($category->parent_id);
			WebPage::$breadcrumb->addDir($category->category, Utils::getComponentUrl('Businesses/business_list/' . $id_categ));
		}
	}

	public function getProductImage($id)
	{
		//$id = rtrim($id, '.jpg');

		$w = @$_GET['w'];
		$w = (isset($w) && $w != '') ? $w : '';
		$h = @$_GET['h'];
		$h = (isset($h) && $h != '') ? $h : '';

		$stretch = (isset($_GET['s'])) ? $_GET['s'] : 0;

		$type = 'image/jpeg';

		$img = $this->model->getProductImage($id);

		if (!is_null($img)) {
			echo base64_encode($img->img_big);
		//header('Content-type: ' . $type);
			//return base64_decode($img->img_mini);
		} else {
			return 'File vuoto o Content-Type non impostato!';
		}
	}

	private static function resizeImg($fileS, $width, $height, $s = 0, $watermark = null)
	{
		require_once LIBS_PATH . 'phpThumb/phpthumb.class.php';
		$phpThumb = new phpthumb();
		$phpThumb->setSourceData($fileS);

		if ($width < 250) {
			$quality = 94;
		} elseif ($width < 1500) {
			$quality = 80;
		} else {
			$quality = 70;
		}

		$phpThumb->setParameter('q', $quality);

		if ($watermark != null) {
			//$phpThumb->fltr = array("wmt|EmporioDellaBellezza.it|2|BR|FF0000||90");
			//$phpThumb->fltr = array("wmi|http://www.emporiodellabellezza.it/content/media/images/img-dropshipping.png|C|75|20|20");
//            $phpThumb->setParameter('fltr', 'wmi|http://www.emporiodellabellezza.it/content/media/images/logo.png|C|75|20|20');
		}

		$type = Utils::get_mimetype_from_buffer($fileS);
		if (strpos($type, 'image/png;') !== false) {
			$phpThumb->setParameter('f', 'png');
		}
		if ($width > 0) {
			$phpThumb->setParameter('w', $width);
		}
		if ($height > 0) {
			$phpThumb->setParameter('h', $height);
		}
		$phpThumb->setParameter('aoe', true);
		if ($s == '1' || $s == true) {
			$phpThumb->setParameter('iar', true);
		}
		if (isset($_GET['far'])) {
			$phpThumb->setParameter('far', $_GET['far']);
		}
		if (isset($_GET['bg'])) {
			$phpThumb->setParameter('bg', $_GET['bg']);
		}

		if (isset($_GET['zc'])) {
			$phpThumb->setParameter('zc', $_GET['zc']);
		}

		if (@is_readable($phpThumb->cache_filename)) {
			$nModified = filemtime($phpThumb->cache_filename);
			header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $nModified) . ' GMT');
			if (@$_SERVER['HTTP_IF_MODIFIED_SINCE'] && ($nModified == strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])) && @$_SERVER['SERVER_PROTOCOL']) {
				header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
				exit;
			}

			if ($getimagesize = @getimagesize($phpThumb->cache_filename)) {
				header('Content-Type: ' . phpthumb_functions::ImageTypeToMIMEtype($getimagesize[2]));
			} elseif (preg_match('#\\.ico$#i', $phpThumb->cache_filename)) {
				header('Content-Type: image/x-icon');
			}
			//@readfile($phpThumb->cache_filename);
			$cont = file_get_contents($phpThumb->cache_filename);
			return $cont;
		}
		$phpThumb->GenerateThumbnail();
		$res = $phpThumb->RenderToFile($phpThumb->cache_filename);
		if ($res) {
			$cont = file_get_contents($phpThumb->cache_filename);
		} else {
			$phpThumb->RenderOutput();
			$cont = $phpThumb->outputImageData;
		}
		return $cont;
	}

	private function getSeoString($obj, $strtitle = '')
	{
		return $this->model->getSeoString($obj, $strtitle);
	}

	public function addCart()
	{
		$_POST['id_variante'] = 2114609;
		$_POST['action'] = 'add';
		$error = '';

		// @var $prod O_product
		$prod = $this->model->getProduct($_POST['id_product']);
		$prod->loadMultiple(BS_session::get_listino());
		$qta = $_POST['quantita'];
		if ($qta < $prod->conf_min) {
			$error = "Attenzione! Per questo articolo la quantità minima d'ordine è di $prod->conf_min pezzi.";
		} elseif (isset($prod->multiple) && $qta % $prod->multiple != 0) {
			$error = "Attenzione! Per questo articolo è consentito effettuare ordini solo per multipli di $prod->multiple";
		} elseif ($prod->serie == '1' && (!isset($_POST['id_variante']) || $_POST['id_variante'] == '')) {
			$error = 'Selezionare una variante del prodotto tra quelle disponibili.';
		}

		if ($error != '') {
			$_SESSION['addCartErrors'] = $error;
			Utils::RedirectTo(Utils::getComponentUrl('Products/productDetails/' . $this->getSeoString($prod) . (($id_var != '') ? '/' . $id_var : '') . '?addcart=0'));
			return;
		}

		// @var $cart O_cart
		$cart = '';
		//Cerco il carrello in sesione
			//     session_destroy();
			if (isset($_SESSION['cart0']) && $_SESSION['cart'] != false) { //Se esiste in sessione lo recupero
				$cart = $_SESSION['cart'];
			}
		// Se il carrello non esiste in sessione
		else {
			$id_user = '';
			if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
				$id_user = $_SESSION['user_id'];
			}
			$orders_model = Loader::getModel('Orders', 'BlueShopV1');
			$carrello = $orders_model->getCart($id_user, session_id()); //Cerco nel db se l'utente ha già un carrello creato

			if (!$carrello) { // se l'utente non ha ancora creato un carrelo lo creo
					if (isset($_POST['action']) && $_POST['action'] == 'add') {// se è un'operazione di aggiungi dettaglio devo creare il carrelo altrimenti ritorno false
						$id_carrello = $orders_model->createCart();
						if (!$id_carrello) {
							throw new Exception('Error: create cart');
						}
						//echo 'id_carrello creato:'.$id_carrello;
						$carrello = $orders_model->getCartbyId($id_carrello);
					}
			}
			$cart = $carrello;
		}
		$cart->getDetails();

		$id_var = 0;

		if ($cart != false) {
			if (isset($_POST['action']) && $_POST['action'] == 'add') {
				if (isset($_POST['id_product']) && $_POST['id_product'] != '') {
					if (isset($_POST['id_variante']) && $_POST['id_variante'] != '') {
						$id_var = $_POST['id_variante'];
					}

					$alreadyInCart = false;
					for ($jj = 0; $jj < count($cart->details); $jj++) {
						$det = $cart->details[$jj];
						if ($det->id_prodotto == $_POST['id_product'] && ($det->id_variante == $id_var)) {
							$tot_qty = $det->quantita + $_POST['quantita'];
							if ($this->model->ProdIsAvailable($_POST['id_product'], $id_var, $tot_qty)) {
								$cart->details[$jj]->quantita += $_POST['quantita'];
								$this->model->updateCartDetail($cart->details[$jj]);
							} else {
								$_SESSION['addCartErrors'] = $this->terms['quantita_eccede_disponibilita'];
							}
							$alreadyInCart = true;
							break;
						}
					}

					if (!$alreadyInCart) {
						if ($this->model->ProdIsAvailable($_POST['id_product'], $id_var, $_POST['quantita'])) {
							if (!$this->addProduct($cart, $_POST['id_product'], $_POST['quantita'], $id_var)) {
								$_SESSION['addCartErrors'] = 'Si è verificato un errore!';
							}
						} else {
							$_SESSION['addCartErrors'] = $this->terms['quantita_eccede_disponibilita'];
						}
					}
					$cart->update();
					$orders_model->updateDiscount($cart);
				}
			}
		}

		if (isset($_SESSION['addCartErrors']) && $_SESSION['addCartErrors'] != '') {
			Utils::RedirectTo(UrlUtils::getComponentUrl('Products/productDetails/' . $this->getSeoString($prod) . (($id_var != '') ? '/' . $id_var : '') . '?addcart=0'));
		} else {
			$_SESSION['addCartMessage'] = $this->terms['AddCartOK'];
			Utils::RedirectTo(UrlUtils::getComponentUrl('Products/productDetails/' . $this->getSeoString($prod) . (($id_var != '') ? '/' . $id_var : '') . '?addcart=1'));
		}
	}

	public function getListProducts($id_cat = '', $id_marca = 0)
	{
		$id_cat = 165;

		$order = '';
		$flag = '';

		$npage = 1;
		if (isset($_GET['page'])) {
			$npage = $_GET['page'];
		}

		$alias = '';
		$id_categ = $id_cat;
		if (is_array($id_cat)) {
			$id_categ = $id_cat[count($id_cat) - 1];
		} elseif (!is_numeric($id_categ)) {
			$alias = $id_categ;
			$id_categ = $this->model->getIdFromAlias($alias);
		}

		//$_SESSION["urlback"] = UrlUtils::getActionUrl('Products/getListProducts/'.$id_categ);
		Session::set('urlback', Utils::getComponentUrl('Products/getListProducts/') . (($alias != '') ? $alias : $id_categ));

		// @var $breadcrumb Breadcrumb_class
		//        $breadcrumb = $this->PAGE->getBreadcrumb();
		//        $cat_breadcrumb = $this->model->getBreadcrumb($id_categ);
		//        for ($i = 0; $i < count($cat_breadcrumb); $i++) {
		//            $breadcrumb->addDir($cat_breadcrumb[$i]["name"], $cat_breadcrumb[$i]['url']);
		//        }
		//        if (count($cat_breadcrumb) > 0) {
		//            $metaTitleCateg = $cat_breadcrumb[count($cat_breadcrumb) - 1]["name"];
//
		//            if (isset($this->sito->metaTitle))
		//                $this->PAGE->setTitle($metaTitleCateg . (($npage > 1)?' - ' . $npage:'') . ' - ' . $this->sito->metaTitle);
		//            elseif ($this->sito->{'titolo_sito_' . Lang::getCurrent()} != '')
		//                $this->PAGE->setTitle($metaTitleCateg . (($npage > 1)?' - ' . $npage:'') . ' - ' . $this->sito->{'titolo_sito_' . Lang::getCurrent()});
		//            else
		//                $this->PAGE->setTitle($metaTitleCateg . (($npage > 1)?' - ' . $npage:''));
		//        }

		$type_search = null;
		$params_search = null;
		$upper_price = null;
		$lower_search = null;
		if (isset($_GET['order'])) {
			$order = $_GET['order'];
		}

		if (isset($_GET['type_search'])) {
			$search = true;
			$type_search = $_GET['type_search'];
			if (isset($_GET['params_search'])) {
				$params_search = $_GET['params_search'];
			}
			if (isset($_GET['lower_price'], $_GET['upper_price'])) {
				$upper_price = $_GET['upper_price'];
				$lower_price = $_GET['lower_price'];
			}
		}

		$params = array(
			'view' => 'default',
			'baseAlias' => 'default',
			'limit' => 6,
			'limit_search' => null,
			'n_art_page' => 1,
			'prezzi_ivati' => true,
			'all_products' => 1,
			'classe' => '',
			'showFlags' => false
		);

		//        if (!$this->PositionParams) {
		//            $params_view = array();
		//        } else {
		//            foreach ($this->PositionParams as $key => $value) {
		//                $params[$key] = $this->PositionParams[$key];
		//            }
		//        }
		if (isset($_GET['limit'])) {
			$params['limit'] = $_GET['limit'];
		}
		$offset = ($npage - 1) * $params['limit'];
		$params['offset'] = $offset;

		$products = array();
		$num_products = 0;
		$subcategories = $id_categ;
		if ($id_categ != '' && $params['all_products'] == 1) {
			$this->view->set('title', "Categoria $id_categ");

			$arr_sub_cat = $this->model->getSubCategories($id_categ);
			for ($i = 0; $i < count($arr_sub_cat); $i++) {
				if (!is_null($arr_sub_cat[$i]['id'])) {
					$subcategories .= ',' . $arr_sub_cat[$i]['id'];
				}
			}
		}

		$category = null;

		if (!is_null($id_categ) && is_numeric($id_categ)) {
			$category = $this->model->getCategory($id_categ);

			$this->view->set('title', $category->name);
			$this->view->set('description', $category->descrizione);
		}
		if ($type_search == null) {
			$num_products = $this->model->getCount($subcategories);

			//$products = $this->model->getListProducts($subcategories, $id_marca, $params['limit'] * $params['n_art_page'], $order, $flag, $offset);
			//            if($_SERVER['HTTP_HOST'] == 'dev.blueshop.it') {
			//                $products = $this->model->getListProducts($subcategories, $id_marca, 99999, $order, $flag, $offset);
			//            } else {
			$products = $this->model->getListProducts($subcategories, $id_marca, $params['limit'] * $params['n_art_page'], $order, $flag, $offset);
		//            }
		} else {
			$settings = $this->model->getBlueShopSettings();
			//            if ($settings->{"titolo_sito_" . Lang::getCurrent()} != '') {
			//                $titolo_var = "titolo_sito_" . Lang::getCurrent();
			//            } else {
			//                $titolo_var = "titolo_sito_it";
			//            }

			if ($type_search != 'bound_price') {
				Headers::setTitleTag($this->terms['search_for'] . " '$params_search'" . (($npage > 1) ? ' - ' . $npage : '') . ' - ' . $settings->{$titolo_var});
			}
			$this->view->set('title', '[$search_result]');
			$this->view->set('description', '[$search_for] <i>' . $params_search . '</i>');

			$search_cat = null;
			if (isset($_GET['search_cat'])) {
				$search_cat = $_GET['search_cat'];
			}
			switch ($type_search) {

				case 'simple':
					//$products = $this->model->getProductSearch($params_search, $order, 0, 0, $search_cat);
					//$num_products = $this->model->getCountProductSearch($params_search, $search_cat);
					$products = $this->model->search($params_search, (($params['limit_search'] != null) ? $params['limit_search'] : $params['limit']), $offset, true, $num_products);
					break;
				case 'flags':
					$aflags = explode(' ', $params_search);
					$products = $this->model->getProductSearch('', $order, $params['limit'], $offset, $search_cat, true, $aflags, null, $num_products);
					$this->view->set('title', '');
					foreach ($aflags as $flag) {
						$this->view->title .= $this->terms[ucfirst($flag)] . ' + ';
					}
					$this->view->title = rtrim($this->view->title, ' + ');
					$this->view->description = '';
					Headers::setTitleTag($this->view->title . (($npage > 1) ? ' - ' . $npage : '') . ' - ' . $settings->{$titolo_var});
					break;
				case 'brand':
					$products = $this->model->getProductSearch('', $order, $params['limit'], $offset, $search_cat, true, null, "m.marca = '$params_search'", $num_products);
					$this->view->description = '[$Brand]: <i>' . ucwords($params_search) . '</i>';
					Headers::setTitleTag(ucwords($params_search) . (($npage > 1) ? ' - ' . $npage : '') . ' - ' . $settings->{$titolo_var});
					break;
				case 'tags':
					$aTags = explode(' ', $params_search);
					$products = $this->model->getProductSearchForTags($aTags, $order, $params['limit'], $offset, $num_products);
					Headers::setTitleTag(ucwords($params_search) . (($npage > 1) ? ' - ' . $npage : '') . ' - ' . $settings->{$titolo_var});
					break;
				case 'features':
					$products = $this->model->getProductSearchForFeatures($params_search, $order, $params['limit'], $offset);
					$num_products = $this->model->getCountProductSearchForFeatures($params_search);
					break;
				case 'bound_price':
					$products = $this->model->getProductSearchForPrice('', $order, $params['limit'] * $params['n_art_page'], $offset, $lower_price, $upper_price, $params['prezzi_ivati'], $subcategories);
					$num_products = $this->model->getCountProductSearchForPrice('', $params['prezzi_ivati'], $lower_price, $upper_price, $subcategories);
					break;
				default:
					$num_products = $this->model->getCount($subcategories, $flag);
					$products = $this->model->getListProducts($subcategories, $id_marca, 0, $order, $flag, 0);
					break;
			}
		}
		if (is_array($products)) {
			for ($i = 0; $i < count($products); $i++) {
				if (!is_null($products[$i])) {
					$products[$i]->img_url = $this->model->getImgUrl($products[$i]->img);
					$products[$i]->getCaratteristiche();
					$products[$i]->seoUrl = Utils::url_slug($products[$i]->titolo) . '-' . $products[$i]->id;
				} else {
					unset($products[$i]);
				}
			}
		}

		$this->view->set('valuta', BS_session::getValuta());
		$this->view->set('cambio', BS_session::getCambio());
		$params['valuta'] = BS_session::getValuta();
		$params['cambio'] = BS_session::getCambio();
		$parameters = WebPage::getParameters();
		$this->view->set('num_art', $num_products);
		$this->view->set('products', $products);
		$this->view->set('params', $params);
		$this->view->set('objCateg', $category);
		$this->view->set('id_categ', $id_categ);
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'products-list';
		$this->view->render($view);

//
//        if ($type_search == null || $type_search == "bound_price") {
//            if ($params['n_art_page'] == 1) {
//                $this->render('Catalogo_' . $params['view']);
//            } else {
//                $this->render('Catalogo_' . $params['view'] . "_page");
//            }
//        } else {
//            $this->render('Ricerca_' . $params['view']);
//        }
	}

	public function showBrandList()
	{
		$brands = $this->model->getBrandsList();
		$this->view->set('items', $brands);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'brandList';
		$this->view->render($view);
	}
}
