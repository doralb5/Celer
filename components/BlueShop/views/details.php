<?php $arr_varianti = $product->variant_product;
$id_product = $product->id;
$variante_sel = $var_id;
$prezzi_ivati = true;
if (isset($_SESSION['prezzi_ivati']) && $_SESSION['prezzi_ivati'] == '0') {
	$prezzi_ivati = false;
}

$prezzo_listino = $product->prezzo_listino;
$prezzo_vendita = $product->prezzo_vendita;
$sconto = $product->sconto;
$risparmio = $product->risparmio;
$iva = $product->iva;

$codice = $product->codice;

if ($prezzi_ivati) {
	$prezzo_listino = $product->prezzo_listino_ivato;
	$prezzo_vendita = $product->prezzo_vendita_ivato;
	$risparmio = $product->risparmio_ivato;
}

if (is_null($variante_sel) && !is_null($product->id_variante_def)) {
	$sconto = '';
	$risparmio = '';
	if ($prezzi_ivati) {
		$prezzo_listino = $product->listino_variante_min_ivato;
		$prezzo_vendita = $product->prezzo_variante_min_ivato;
	} else {
		$prezzo_listino = $product->listino_variante_min;
		$prezzo_vendita = $product->prezzo_variante_min;
	}
}

//MICRODATA SERP GOOGLE GENERATI DA ZOORATE FEEDATY
//    echo $this->microdata; FEEDATY , IMPORTANT!
?>




<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/product-details.css'); ?>

<style>
    /*-----------------------------------------*/
    /* affix  */
    /*----------------------------------------*/
    @media (min-width: 991px){
        .affix {
            bottom: 300px;
            width: 350px;
            top: 116px;
            z-index: 999;
            position:fixed;
        }  
    }
    @media (max-width: 991px){
        .affix {
            bottom: 0;
            width: 100%;
            top: 0;
            z-index: 999;
            position: relative;
            border-top: 1px solid #e5b951;
            padding-top: 2em;
            margin-top: 2em;
        }  
    }



</style>

<div id="page">
    <!-- Global Breadcrumbs -->
    <!--    <nav class="breadcrumb category-breadcrumb" data-module="breadcrumbs">
            <ul>
                <li itemscope="" itemtype="http://schema.org/Breadcrumb" class="breadcrumb-item">
                    <a itemprop="url" href="/at/en_gb/ca/men/mens-ready-to-wear-c-men-readytowear"><span itemprop="title">Desta</span></a>
                </li>
                <li class="content-select basic-content-select breadcrumb-item">
                    <div class="selectricWrapper selectric-custom selectric-breadcrumb-select selectric-_disable-mobile">
                        <div class="selectric">
                            <p class="label"><a href="/at/en_gb/ca/men/mens-ready-to-wear/mens-suits-c-men-readytowear-suits">Categories</a></p>
                        </div>
                    </div>
                </li>
            </ul>
        </nav>-->
    <a class="share page-share product-detail-share-button overlay-open" href="#top-right-share">
        Share</a>


    <div class="content">
        <div class="product-detail-wrap">   
            <div class="row">




                <div class="col-md-12">
                    <?php $prd = new Products_Component() ?>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">




                            <?php
							if ($product->imgs[0]['url'] != '') {
								$i = 0;
								foreach ($product->imgs as $img) {
									?>
                                    
                                  <div class="item <?php if ($i === 0) {
										echo 'active';
									} ?>">
                                <img src="<?= $img['url'] ?>" alt="First slide">

                                 </div>
                                    

                                    <?php
								$i++;
								} ?>

                            <?php
							} else {
								?>

                                                            <div class="item active">
                                <img src="<?= WEBROOT ?>content/media/images/noimage.jpg" alt="First slide">

                            </div>
                                
                            <?php
							} ?>

                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span></a><a class="right carousel-control"
                                                                                     href="#carousel-example-generic" data-slide="next"><span class="glyphicon glyphicon-chevron-right">
                            </span></a>
                    </div>

                </div>

                <div id="push">
                </div>

            </div>

            <div class="row">
                <div class="detail-accordion"> 
                    <div class="col-md-7">

                        <div id="main-wrapper">
                            <div class="pxlr-accordion-wrap">
                                <section class="pxlr-accordion-twelve ptb-100">
                                    <div class="row">
                                        <div class="col-sm-12 pxlraccordionTwelve">
                                            <div class="panel-group" id="pxlraccordionTwelveLeft">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading heart">
                                                        <h6 class=""><a data-parent="#pxlraccordionTwelveLeft" data-toggle="collapse" href="#collapseTenLeftone" class="" aria-expanded="true">Product Description </a></h6>
                                                    </div>

                                                    <div class="panel-collapse collapse in" id="collapseTenLeftone" aria-expanded="true">
                                                        <div class="panel-body">
                                                            <div class="facts">
                                                                <p><?=$product->descrizione_html?>
                                                                </p>
<!--




                                                                <ul>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Research</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Design and Development</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Porting and Optimization</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>System integration</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Verification, Validation and Testing</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Maintenance and Support</li>
                                                                </ul>-->






                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.panel-default -->

                                                <div class="panel panel-default">
                                                    <div class="panel-heading heart">
                                                        <h6 class=""><a class="collapsed" data-parent="#pxlraccordionTwelveLeft" data-toggle="collapse" href="#collapseTenLeftTwo" aria-expanded="false">Additional Information</a></h6>
                                                    </div>

                                                    <div class="panel-collapse collapse" id="collapseTenLeftTwo" style="height: 0px;" aria-expanded="false">
                                                        <div class="panel-body">
                                                            <div class="facts">
                                                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections</p>

                                                                <ul>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Multimedia Systems</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Digital media adapters</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Set top boxes for HDTV and IPTV Player</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.panel-default -->

                                                <div class="panel panel-default">
                                                    <div class="panel-heading heart">
                                                        <h6 class=""><a class="collapsed" data-parent="#pxlraccordionTwelveLeft" data-toggle="collapse" href="#collapseTenLeftThree" aria-expanded="false">Reviews</a></h6>
                                                    </div>

                                                    <div class="panel-collapse collapse" id="collapseTenLeftThree" style="height: 0px;" aria-expanded="false">
                                                        <div class="panel-body">
                                                            <div class="facts">
                                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined</p>

                                                                <ul>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Research</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Design and Development</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Porting and Optimization</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>System integration</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Verification, Validation and Testing</li>
                                                                    <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Maintenance and Support</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.panel-default -->


                                            </div>
                                            <!--end of /.panel-group--></div>
                                        <!--end of /.col-sm-6--></div>
                                </section>
                                <!-- end of pxlr-accordion-twelve --></div>
                            <!-- end of pxlr-accordion-wrap--></div>

                    </div>

                    <div class="col-md-3" >
                        <div class="purchase-column" data-spy="affix" data-offset-bottom="100" id="myAffix">
                            <div class="single-top-in">
                                <div class="span_2_of_a1 simpleCart_shelfItem">
                                    <h3><?= $product->titolo ?></h3>

                                    <div class="price_single"><span class="reducedfrom item_price"><?=$valuta ?> <?=$prezzo_vendita ?></span> <a href="#">click for offer</a>
                                        <div class="clearfix">&nbsp;</div>
                                    </div>

                                    <h4 class="quick">Quick Overview:</h4>

                                    <div class="wish-list">
                                        <ul>
                                            <li class="wish"><a href="#"><span aria-hidden="true" class="glyphicon glyphicon-heart"></span>Add to Wishlist</a></li>
                                            <li class="compare"><a href="#"><span aria-hidden="true" class="glyphicon glyphicon-resize-horizontal"></span>Add to Compare</a></li>
                                        </ul>
                                    </div>
<form id="form_add_cart_<?=$id_product?>">
    <input name="id_product" value="<?=$product->id?>" type="hidden">
                                    <div class="quantity">
                                        <div class="quantity-select">
                                            <div class="entry value-minus">&nbsp;</div>

                                            <div class="entry value"><span>1</span></div>
                                            <input type="hidden" class="qta" name="quantita" value="1">
                                            <div class="entry value-plus active">&nbsp;</div>
                                        </div>
                                    </div>
                                    <!--quantity--><script>
                                        $('.value-plus').on('click', function () {
                                            var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.val(), 10) + 1;
                                            divUpd.text(newVal);
                                        });

                                        $('.value-minus').on('click', function () {
                                            var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
                                            if (newVal >= 1)
                                                divUpd.text(newVal);
                                        });
                                    </script><!--quantity--><a class="add-to item_add hvr-skew-backward" id="addCart" href="#">Add to cart</a>
                                    
                                    </form>

                                    <div class="clearfix">&nbsp;</div>
                                    <div class="show_details">
                                        <a href="#main-wrapper" data-scroll="true" data-id="#main-content" class="page-scroll">Show Details</a>
                                        <div class="clearfix">&nbsp;</div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>










<script type="text/javascript">
    
        $("#addCart").click(function () {







            $.ajax({
                type: "POST",
                url: '<?= Utils::getComponentUrl('BlueShop/Products/showBrandList') ?>',
                data: $("#form_add_cart_<?=$id_product?>").serialize(),
                success: function (data) {

alert(data);

                },
                error: function (e) {
                    console.log(e);
                }


            });

      

        setTimeout(function () {
            $(".alert-costumized").hide('slow');
        }, 3500);
    });
    
    
    
    
    
    
    
    
    
    
    
    function addCart<?=$id_product?>()
    {

            var form=document.getElementById('form_add_cart_<?=$id_product?>');
            if( !isPositiveInteger(form.quantita.value) || form.quantita.value < 1)
            {
                errorForm("[$specificare_quantita]","[$alert]");
                return false;
            }
            
            <?if ($product->id_disponibilita == '1' && $product->quantita_disponibile > 0) {
								?> 
            
                if(form.quantita.value > <?=$qta?>)
                {
                    errorForm("[$quantita_eccede_disponibilita]","[$alert]");
                    return false;
                }
                
            <?
							}?>
                
            if( form.quantita.value < <?=$product->conf_min?> ) {
                errorForm("Attenzione! Per questo articolo la quantità minima d'ordine è di <?=$product->conf_min?> pezzi.");
                return false;
            }
            if( form.quantita.value % <?=$product->multiple?> != 0 ) {
                errorForm("Attenzione! Per questo articolo è consentito effettuare ordini solo per multipli di <?=$product->multiple?>");
                return false;
            }
                
                
            if($("#add_article").length > 0)
            {
                
                

                if($("#su_misura").length > 0 )
                {

                        
                    $("#box_msg").dialog("destroy");
                    $('#msg_box_container').remove();

                    var box=$('<div style="display:none" id="msg_box_container"><div id="box_msg" title="[$prodotto_su_misura]">'+
                    '<p>'+'[$msg_su_misura]'+'</p>'+
                    '</div></div>');
                    $('body').append(box);
                    $("#box_msg").dialog(
                    { 
                        minHeight: 150,
                        minWidth: 450,
                        close: function( event, ui )
                        {
                           document.getElementById("form_add_cart_<?=$id_product?>").submit();
                           return false;   
                        },
                        buttons:
                        {
                            "[$continua]": function() {
                                
                               document.getElementById("form_add_cart_<?=$id_product?>").submit();
                                return false;
                            }
                          }    
                        } 
                    );
                return false;

                        
                }
                else
                {
                    document.getElementById("form_add_cart_<?=$id_product?>").submit();
                    return false;
                }
            }
            else
            {
               if($("#su_misura").length > 0 )
                {

                        
                    $("#box_msg").dialog("destroy");
                    $('#msg_box_container').remove();

                    var box=$('<div style="display:none" id="msg_box_container"><div id="box_msg" title="[$prodotto_su_misura]">'+
                    '<p>'+'[$msg_su_misura]'+'</p>'+
                    '</div></div>');
                    $('body').append(box);
                    $("#box_msg").dialog(
                    { 
                        minHeight: 150,
                        minWidth: 450,
                        close: function( event, ui )
                        {
                           document.getElementById("form_add_cart_<?=$id_product?>").submit();
                           return false;   
                        },
                        buttons:
                        {
                            "[$continua]": function() {
                                
                               document.getElementById("form_add_cart_<?=$id_product?>").submit();
                                return false;
                            }
                          }    
                        } 
                    );
                return false;

                        
                }
                else
                {
                    
                    return true;
                }
            }
            
            
            
           
        
    }
    
    function noChart<?=$id_product?>()
    {
                errorForm("[$selezionare_variante]","[$alert]");
                return false;   
    }
    
    
    <?if (isset($_GET['addcart'])) {
								?>
    function animateCart()
    {
        
        if($("#add_article").length>0)
        {

            //$("#small_report").appendTo( $("#add_article") )
            //$("#add_article").fadeIn(500).delay(500).fadeOut(500, function(){cartminiOpenClose();});
            cartminiOpenClose();
       
            return false;
        }
        else
        {
            return true;
        }
    }
    
    $(window).load(function(){animateCart();});
    <?
							}?>
    
    
</script>

















<script>
    $('#myAffix').affix({
        offset: {

            bottom: function () {
                return (this.bottom = $('.end').outerHeight(true))
            }
        }
    })
</script>
<script>
    //jQuery for page scrolling feature - requires jQuery Easing plugin
    $(function () {
        $('a.page-scroll').bind('click', function (event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });

</script>