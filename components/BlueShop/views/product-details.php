
<?php $arr_varianti = $product->variant_product;
$id_product = $product->id;
$variante_sel = $var_id;
$prezzi_ivati = true;
if (isset($_SESSION['prezzi_ivati']) && $_SESSION['prezzi_ivati'] == '0') {
	$prezzi_ivati = false;
}

$prezzo_listino = $product->prezzo_listino;
$prezzo_vendita = $product->prezzo_vendita;
$sconto = $product->sconto;
$risparmio = $product->risparmio;
$iva = $product->iva;

$codice = $product->codice;

if ($prezzi_ivati) {
	$prezzo_listino = $product->prezzo_listino_ivato;
	$prezzo_vendita = $product->prezzo_vendita_ivato;
	$risparmio = $product->risparmio_ivato;
}

if (is_null($variante_sel) && !is_null($product->id_variante_def)) {
	$sconto = '';
	$risparmio = '';
	if ($prezzi_ivati) {
		$prezzo_listino = $product->listino_variante_min_ivato;
		$prezzo_vendita = $product->prezzo_variante_min_ivato;
	} else {
		$prezzo_listino = $product->listino_variante_min;
		$prezzo_vendita = $product->prezzo_variante_min;
	}
}

//MICRODATA SERP GOOGLE GENERATI DA ZOORATE FEEDATY
//    echo $this->microdata; FEEDATY , IMPORTANT!
?>



<div class="container">
<div class="col-md-9">
<div class="row">
    <div class="col-md-5 grid">
        <div class="flexslider">
            <ul class="slides">
                <?php $prd = new Products_Component() ?>


<?php
if ($product->imgs[0]['url'] != '') {
	foreach ($product->imgs as $img) {
		?>

                                <li data-thumb="<?= $img['url'] ?>">
                                    <div class="thumb-image"><img class="img-responsive" data-imagezoom="true" src="<?= $img['url'] ?>" /></div>
                </li>

                                <?php
	} ?>

                        <?php
} else {
		?>
                            <li data-thumb="<?= WEBROOT ?>content/media/images/noimage.jpg">
                                <div class="thumb-image"><img class="img-responsive" data-imagezoom="true" src="<?= WEBROOT ?>content/media/images/noimage.jpg" /></div>
                </li>
<?php
	} ?>







            </ul>
        </div>
    </div>

    <div class="col-md-7 single-top-in">
        <div class="span_2_of_a1 simpleCart_shelfItem">
                    <h3><?= $product->titolo ?></h3>

                    <p class="in-para"><?=  $product->descrizione_html?></p>

                    <div class="price_single"><span class="reducedfrom item_price"><?= $product->prezzo_vendita ?></span> <a href="#">click for offer</a>
                <div class="clearfix">&nbsp;</div>
            </div>


            <div class="wish-list">
                <ul>
                    <li class="wish"><a href="#"><span aria-hidden="true" class="glyphicon glyphicon-check"></span>Add to Wishlist</a></li>
                    <li class="compare"><a href="#"><span aria-hidden="true" class="glyphicon glyphicon-resize-horizontal"></span>Add to Compare</a></li>
                </ul>
            </div>

            <div class="quantity">
                <div class="quantity-select">
                    <div class="entry value-minus">&nbsp;</div>

                    <div class="entry value"><span>1</span></div>

                    <div class="entry value-plus active">&nbsp;</div>
                </div>
            </div>
            <!--quantity--><script>
                $('.value-plus').on('click', function () {
                    var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
                    divUpd.text(newVal);
                });

                $('.value-minus').on('click', function () {
                    var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
                    if (newVal >= 1)
                        divUpd.text(newVal);
                });
            </script><!--quantity--><a class="add-to item_add hvr-skew-backward" href="#">Add to cart</a>

            <div class="clearfix">&nbsp;</div>
        </div>
    </div>
    </div>

    <div class="clearfix">&nbsp;</div>

    <div class="tab-head">
        <nav class="nav-sidebar">
            <ul class="nav tab">
                <li class="active"><a data-toggle="tab" href="#tab1">Product Description </a></li>
                <li><a data-toggle="tab" href="#tab2">Additional Information</a></li>
                <li><a data-toggle="tab" href="#tab3">Reviews</a></li>
            </ul>
        </nav>

        <div class="tab-content one">
            <div class="tab-pane active text-style" id="tab1">
                <div class="facts">
                        <p><?= $product->descrizione_html ?></p>

                    <ul>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Research</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Design and Development</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Porting and Optimization</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>System integration</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Verification, Validation and Testing</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Maintenance and Support</li>
                    </ul>
                </div>
            </div>

            <div class="tab-pane text-style" id="tab2">
                <div class="facts">
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections</p>

                    <ul>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Multimedia Systems</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Digital media adapters</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Set top boxes for HDTV and IPTV Player</li>
                    </ul>
                </div>
            </div>

            <div class="tab-pane text-style" id="tab3">
                <div class="facts">
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined</p>

                    <ul>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Research</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Design and Development</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Porting and Optimization</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>System integration</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Verification, Validation and Testing</li>
                        <li><span aria-hidden="true" class="glyphicon glyphicon-ok"></span>Maintenance and Support</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="clearfix">&nbsp;</div>
    </div>
</div>
</div>













<script type="text/javascript">
    function addCart<?=$id_product?>()
    {

            var form=document.getElementById('form_add_cart_<?=$id_product?>');
            if( !isPositiveInteger(form.quantita.value) || form.quantita.value < 1)
            {
                errorForm("[$specificare_quantita]","[$alert]");
                return false;
            }
            
            <?if ($product->id_disponibilita == '1' && $product->quantita_disponibile > 0) {
		?> 
            
                if(form.quantita.value > <?=$qta?>)
                {
                    errorForm("[$quantita_eccede_disponibilita]","[$alert]");
                    return false;
                }
                
            <?
	}?>
                
            if( form.quantita.value < <?=$product->conf_min?> ) {
                errorForm("Attenzione! Per questo articolo la quantità minima d'ordine è di <?=$product->conf_min?> pezzi.");
                return false;
            }
            if( form.quantita.value % <?=$product->multiple?> != 0 ) {
                errorForm("Attenzione! Per questo articolo è consentito effettuare ordini solo per multipli di <?=$product->multiple?>");
                return false;
            }
                
                
            if($("#add_article").length > 0)
            {
                
                

                if($("#su_misura").length > 0 )
                {

                        
                    $("#box_msg").dialog("destroy");
                    $('#msg_box_container').remove();

                    var box=$('<div style="display:none" id="msg_box_container"><div id="box_msg" title="[$prodotto_su_misura]">'+
                    '<p>'+'[$msg_su_misura]'+'</p>'+
                    '</div></div>');
                    $('body').append(box);
                    $("#box_msg").dialog(
                    { 
                        minHeight: 150,
                        minWidth: 450,
                        close: function( event, ui )
                        {
                           document.getElementById("form_add_cart_<?=$id_product?>").submit();
                           return false;   
                        },
                        buttons:
                        {
                            "[$continua]": function() {
                                
                               document.getElementById("form_add_cart_<?=$id_product?>").submit();
                                return false;
                            }
                          }    
                        } 
                    );
                return false;

                        
                }
                else
                {
                    document.getElementById("form_add_cart_<?=$id_product?>").submit();
                    return false;
                }
            }
            else
            {
               if($("#su_misura").length > 0 )
                {

                        
                    $("#box_msg").dialog("destroy");
                    $('#msg_box_container').remove();

                    var box=$('<div style="display:none" id="msg_box_container"><div id="box_msg" title="[$prodotto_su_misura]">'+
                    '<p>'+'[$msg_su_misura]'+'</p>'+
                    '</div></div>');
                    $('body').append(box);
                    $("#box_msg").dialog(
                    { 
                        minHeight: 150,
                        minWidth: 450,
                        close: function( event, ui )
                        {
                           document.getElementById("form_add_cart_<?=$id_product?>").submit();
                           return false;   
                        },
                        buttons:
                        {
                            "[$continua]": function() {
                                
                               document.getElementById("form_add_cart_<?=$id_product?>").submit();
                                return false;
                            }
                          }    
                        } 
                    );
                return false;

                        
                }
                else
                {
                    
                    return true;
                }
            }
            
            
            
           
        
    }
    
    function noChart<?=$id_product?>()
    {
                errorForm("[$selezionare_variante]","[$alert]");
                return false;   
    }
    
    
    <?if (isset($_GET['addcart'])) {
		?>
    function animateCart()
    {
        
        if($("#add_article").length>0)
        {

            //$("#small_report").appendTo( $("#add_article") )
            //$("#add_article").fadeIn(500).delay(500).fadeOut(500, function(){cartminiOpenClose();});
            cartminiOpenClose();
       
            return false;
        }
        else
        {
            return true;
        }
    }
    
    $(window).load(function(){animateCart();});
    <?
	}?>
    
    
</script>














<!-----><script>
// Can also be used with $(document).ready()
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    });
</script>



















