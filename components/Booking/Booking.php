<?php

class Booking_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		require_once DOCROOT . LIBS_PATH . 'Email.php';
		require VIEWS_PATH . 'MailMessage.php';
	}

	public function booking()
	{
		if (isset($_GET['check_availability'])) {
			$availrooms = array();
			if ($this->check_AvailabilityRequiredFields($_GET)) {
				$filter = '';
				$checkin = $_GET['checkin'];
				$checkout = $_GET['checkout'];
				$guests = $_GET['guests'];
				$children = $_GET['children'];

				$searchparams = array('checkin' => $checkin, 'checkout' => $checkout, 'guests' => $guests, 'children' => $children);
				$availrooms = $this->model->findRoomsPrices($checkin, $checkout, $guests, $children, $filter);
				$this->view->set('searchparams', $searchparams);
			}
			$this->view->set('availrooms', $availrooms);
			// $this->view->set('url_form_action', Utils::getComponentUrl('Booking/booking'));
			$this->view->render('findAvailRooms');
			return;
		} elseif (isset($_REQUEST['reserve'])) {
			$this->reservation_room($_REQUEST);
			return;
		}
	}

	private function check_AvailabilityRequiredFields($fields)
	{
		$this->cleanErrors();
		if (!isset($fields['checkin']) || $fields['checkin'] == '') {
			$this->AddError('CheckIn date is required!');
		}
		if (!isset($fields['checkout']) || $fields['checkout'] == '') {
			$this->AddError('CheckOut date is required!');
		}
		if (!isset($fields['guests']) || $fields['guests'] == '') {
			$this->AddError('Number of Guests is required!');
		}

		if (count($this->getErrors())) {
			return false;
		}
		return true;
	}

	public function reservation_room($data)
	{
		$filter = "t1.id = '" . $data['room_type_id'] . "'";
		$availrooms = $this->model->findRoomsPrices($data['checkin'], $data['checkout'], $data['sleeps'], $data['children'], $filter);
		$reg_completed = false;
		if (count($availrooms) == 1) {
			$room = $availrooms[0];
		} else {
			require_once DOCROOT . ENTITIES_PATH . 'Booking/BookingAvailRoomPrice.php';
			$room = new BookingAvailRoomPrice_Entity();
			$this->AddError('RoomNotAvailableForReservation');
		}

		require_once DOCROOT . ENTITIES_PATH . 'Booking/Booking.php';
		$booking = new Booking_Entity();
		$booking->reservation_start = $room->checkin;
		$booking->reservation_end = $room->checkout;
		$booking->guests_qty = $room->guests;
		$booking->children_qty = $room->children;
		$booking->room_type_id = $room->room_type_id;
		$booking->final_price = $room->final_price;

		if (isset($_REQUEST['confirm'])) {
			($data['firstname'] == '') ? $this->AddError('[$firstname_required]') : $booking->guest_firstname = $data['firstname'];
			($data['lastname'] == '') ? $this->AddError('[$lastname_required]') : $booking->guest_lastname = $data['lastname'];
			($data['email'] == '') ? $this->AddError('[$email_required]') : $booking->guest_email = $data['email'];
			($data['address'] == '') ? $this->AddError('[$address_required]') : $booking->guest_address = $data['address'];
			($data['birth'] == '') ? $this->AddError('[$birth_required]') : $booking->guest_birthday = $data['birth'];
			($data['city'] == '') ? $this->AddError('[$city_required]') : $booking->guest_city = $data['city'];
			($data['zipcode'] == '') ? $this->AddError('[$zipcode_required]') : $booking->guest_zipcode = $data['zipcode'];
			($data['country'] == '') ? $this->AddError('[$country_required]') : $booking->guest_country = $data['country'];
			($data['phone'] == '') ? $this->AddError('[$phone_required]') : $booking->guest_phone = $data['phone'];
			($data['personal_id'] == '') ? $this->AddError('[$personal_id_required]') : $booking->guest_personalid = $data['personal_id'];
			$booking->note = $data['note'];
			if (count($this->getErrors()) == 0) {
				if ($availrooms[0]->avail_qty > 0) {
					$booking->status_id = $this->model->getDefaultBookingStatus();
					$inserted_id = $this->model->saveBooking($booking);
					if (!is_array($inserted_id)) {
						$booking->id = $inserted_id;
						$reg_completed = true;
						$this->AddNotice('Reservation successfully Completed');
						$this->sendReservationMail($booking, true);
						$this->sendReservationMail($booking);
					} else {
						$this->addError('[$Saving_failed]');
					}
				} else {
					$this->AddError('RoomNotAvailableForReservation');
				}
			}
		}
		$this->view->set('booking', $booking);
		$this->view->set('room', $room);
		$this->view->set('reg_completed', $reg_completed);
		$this->view->render('reservation-form');
	}

	public function sendReservationMail($booking, $admin = false)
	{
		if ($admin) {
			$email = 's.lika@bluehat.al'; //CMSSettings::$EMAIL_ADMIN;
			$template = 'email-admin-notify';
		} else {
			$email = $booking->guest_email;
			$template = 'reservation-request-mail';
		}
		$roomtypeimages = $this->model->getRoomImagesByType($booking->room_type_id);
		$message = new MailMessage();
		$message->setViewPath(COMPONENTS_PATH . 'Booking' . DS . 'views/mails' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Booking' . DS . 'lang' . DS);
		$message->set('roomtypeimages', $roomtypeimages);
		$message->set('booking', $booking);
		$subject = $this->view->getTerm('reservation_request');
		$sended = $message->send($email, $subject, $template);
		return $sended;
	}
}
