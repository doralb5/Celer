<?php
//echo "<!--<pre>";
//print_r($availrooms);
//print_r($searchparams);
//echo "</pre>-->";
?>


<?php $this->showComponentErrors(); ?>
<?php $this->showComponentMessages(); ?>

<?php
if (count($availrooms)) {
	foreach ($availrooms as $room) {
		?>

        <div class="de-popular-wrapper">
            <div class="de-popular-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="de-popular-place">
                            <div class="popular-img">
                                <a href="#" class="img-place">
                                    <img src="<?= Utils::genThumbnailUrl('booking/rooms/' . $room->room_type_id . '/' . $room->images[0], 500, 400, array())?>" alt="[Image]" title="<?= $room->name ?>">
                                </a>
                            </div>
                            <div class="popular-info">
                                <div class="popular-title">
                                    <h2>
                                        <a href=""><?= $room->name ?></a>
                                    </h2>
                                    <div class="popular-address">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-bed"></i><?= $room->bedrooms ?> [$beds]</span>

                                                <span><i class="fa fa-male"></i><?= $room->sleeps ?> [$max_guests]</span>

                                                <span><i class="fa fa-wifi"></i><?= $room->free_wifi ?> [$free_wifi]</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="popular-author">
                                    <div class="row">

                                        
                                        <div class="col-md-12">
                                            <?php
											if ($room->no_price) {
												echo '[$NotAvailable]';
											} else {
												echo '<span>' . Utils::price_format($room->final_price, 'EUR', true) . '</span>';
											} ?>
                                        </div>

                                    </div>

                                    <div class="row">
                                        
                                        <div class="col-md-9">
                                            <p><?=$room->nights ?> <?=($room->nights == 1)?'[$night]':'[$nights]'?> [$for] <?= $room->guests ?> <?=($room->guests == 1)?'[$adult]':'[$adults]'?>
                                                <?=($room->children) ? ('+ ' . $room->children . (($room->children == 1)?' [$children]':' [$children]')): ''?></p>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="reserve-button">
                                                <form action="<?= Utils::getComponentUrl('Booking/booking') ?>" method="post">
                                                    <input type="hidden" name="reserve" value="1">
                                                    <input type="hidden" name="room_type_id" value="<?= $room->room_type_id ?>">
                                                    <input type="hidden" name="checkin" value="<?= $searchparams['checkin'] ?>">
                                                    <input type="hidden" name="checkout" value="<?= $searchparams['checkout'] ?>">
                                                    <input type="hidden" name="sleeps" value="<?= $searchparams['guests'] ?>">
                                                    <input type="hidden" name="children" value="<?= $searchparams['children'] ?>">
                                                    <button class="btn btn-sm btn-primary btn-block btn-readall">Reserve now</button>
                                                </form>              
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
	}
} else {
	echo '[$NoRecordsFound]';
}
?>

