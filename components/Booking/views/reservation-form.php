<?php
$checkin = (isset($_GET['checkin']))?$_GET['checkin']:$booking->reservation_start;
$checkout = (isset($_GET['checkout']))?$_GET['checkout']:$booking->reservation_end;
$room_type_id = (isset($_GET['room_type_id']))?$_GET['room_type_id']:$booking->room_type_id;
$sleeps = (isset($_GET['sleeps']))?$_GET['sleeps']:$booking->guests_qty;
$children = (isset($_GET['children']))?$_GET['children']:$booking->children_qty;

$firstname = (isset($_POST['firstname']))?$_POST['firstname']:'';
$lastname = (isset($_POST['lastname']))?$_POST['lastname']:'';
$email = (isset($_POST['email']))?$_POST['email']:'';
$country = (isset($_POST['country']))?$_POST['country']:'';
$city = (isset($_POST['city']))?$_POST['city']:'';
$address = (isset($_POST['address']))?$_POST['address']:'';
$zipcode = (isset($_POST['zipcode']))?$_POST['zipcode']:'';
$birth = (isset($_POST['birth']))?$_POST['birth']:'';
$personalid = (isset($_POST['personal_id']))?$_POST['personal_id']:'';
$phone = (isset($_POST['phone']))?$_POST['phone']:'';
$note = (isset($_POST['note']))?$_POST['note']:'';

?>

<style>
    .glyphicon {  margin-bottom: 10px;margin-right: 10px;}

    small {
        display: block;
        line-height: 1.428571429;
        color: #999;
    }
</style>


<div class="de-popular-wrapper">
    <div class="de-popular-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="de-popular-place">
                    <div class="popular-img">
                        <a href="#" class="img-place">
                            <img src="<?= Utils::genThumbnailUrl('booking/rooms/' . $room->images[0], 500, 400, array()) ?>" alt="[Image]" title="<?= $room->name ?>">
                        </a>
                    </div>
                    <div class="popular-info">
                        <div class="popular-title">
                            <h2>
                                <a href=""><?= $room->name ?></a>
                            </h2>
                            <div class="popular-address">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p><b><?= $room->nights ?> </b><?= ($room->nights == 1) ? '[$booking]' : '[$nights]' ?> [$for] <b><?= $room->guests ?></b> <?= ($room->guests == 1) ? '[$adult]' : '[$adults]' ?>
                                            <?= ($room->children) ? ('+  <b> ' . $room->children . '</b>' . (($room->children == 1) ? ' [$children]' : ' [$children]')) : '' ?></p>

                                        <p>[$from] <?= date('d.m.Y', strtotime($checkin)) ?> [$to] <?= date('d.m.Y', strtotime($checkout)) ?></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="popular-author">
                            <div class="row">


                                <div class="col-md-12">
                                    <?php
									if ($room->no_price) {
										echo '[$NotAvailable]';
									} else {
										echo '<span>' . Utils::price_format($room->final_price, 'EUR', true) . '</span>';
									}
									?>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php $this->showComponentErrors(); ?>
<?php $this->showComponentMessages(); ?>

<div class="col-md-10 col-md-offset-1">
    <form class="form-capsule" method="post">
        <input type="hidden" name="reserve" value="1">
        <input type="hidden" name="checkin" value="<?= $checkin ?>">
        <input type="hidden" name="checkout" value="<?= $checkout ?>">
        <input type="hidden" name="room_type_id" value="<?= $room_type_id ?>">
        <input type="hidden" name="sleeps" value="<?= $sleeps ?>">
        <input type="hidden" name="children" value="<?= $children ?>">

        <h4 class="page-header">[$General_Information]</h4>
        <div class="row">
            <div class="form-group col-sm-4">
                <label for="firstname">[$Firstname]</label>
                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="[$Firstname]" value="<?=$firstname?>">
            </div>
            <div class="form-group col-sm-4">
                <label for="lastname">[$Lastname]</label>
                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="[$Lastname]" value="<?=$lastname?>">
            </div>
            <div class="form-group col-sm-4">
                <label for="email">[$Email]</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="[$Email]" value="<?=$email?>">
            </div>
        </div>

        <h4 class="page-header">[$Address_Information]</h4>
        <div class="row">
            <div class="form-group col-sm-6">
                <label for="city">[$Country]</label>
                <input type="text" class="form-control" id="country" name="country" placeholder="[$Country]" value="<?=$country?>">
            </div>
            <div class="form-group col-sm-6">
                <label for="city">[$City]</label>
                <input type="text" class="form-control" id="city" name="city" placeholder="[$City]" value="<?=$city?>">
            </div>
            <div class="form-group col-sm-6">
                <label for="address">[$Address]</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="[$Address]" value="<?=$address?>">
            </div>
            <div class="form-group col-sm-6">
                <label for="zipcode">[$ZipCode]</label>
                <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="[$ZipCode]" value="<?=$zipcode?>">
            </div>
        </div>

        <h4 class="page-header">[$Personal_Information]</h4>
        <div class="row">
            <div class="form-group col-sm-4">
                <label for="birth">[$Date_of_birth]</label>
                <input type="text" class="form-control flatpickr flatpickr-input" id="birth" name="birth" placeholder="[$Date_of_birth]" value="<?=$birth?>">
            </div>
            <div class="form-group col-sm-4">
                <label for="personal_id">[$PersonalId]</label>
                <input type="text" class="form-control" id="personal_id" name="personal_id" placeholder="[$PersonalId]" value="<?=$personalid?>">
            </div>
            <div class="form-group col-sm-4">
                <label for="phone">[$Phone]</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="[$Phone]" value="<?=$phone?>">
            </div>
        </div>

        <h4 class="page-header">[$Leave_message]</h4>
        <div class="form-group form-group-textarea">
            <label for="note">[$Note]</label>
            <textarea type="text" class="form-control" id="note" name="note" placeholder="[$Note]"><?=$note?></textarea>
        </div>

        <div class="form-group"> 

            <div class="controls col-md-6 col-md-offset-3 text-center">
                <input type="submit" name="confirm" value="[$Confirm]" class="btn btn-primary btn-md btn-info" id="confirm" />
            </div>
            <br><br>
        </div> 

    </form>
    
    </div>


<style>
    /********************************************
************ Global settings ****************
*********************************************/
    html {
        box-sizing: border-box;
    }

    *, *:before, *:after {
        box-sizing: inherit;
    }

    /********************************************
    ************ Floating label ****************
    *********************************************/
    .form-capsule .form-group {
        /* Form group*/
        position: relative;
    }

    /******* Label *******/
    .form-capsule .form-group label {
        /* Input field floating label */
        position: absolute;
        font-size: 0.8em;
        top: 0px;
        width: 100%;
        padding: 0 10px;
        margin: 5px 0 10px 0;
        opacity: 0;
        -webkit-transition: opacity 0.2s ease-out;
        transition: opacity 0.2s ease-out;
        /* prevent flickering */
        -webkit-backface-visibility: hidden;
        pointer-events: none;
    }

    .form-capsule .show-label label {
        /* Active state for label when visible */
        opacity: 1;
    }

    /******** Form control fields ********/
    .form-capsule .form-control {
        /* Form control fields */
        height: 50px;
        padding: 15px 10px;
        box-shadow: 0 0 0 2px transparent;
        -webkit-transition: all 0.2s ease-out;
        transition: all 0.2s ease-out;
    }

    .form-capsule .form-control:focus {
        /* Focus state for form-control fields */
        box-shadow: 0 0 0 2px #000066;
        outline: transparent;
        border-color: transparent;
    }

    .form-capsule .show-label .form-control {
        /* Active state for form-control fields */
        padding-top: 25px;
        padding-bottom: 5px;
    }

    /********************************************
    ************ Textarea ***********************
    *********************************************/
    .form-capsule .form-group-textarea label {
        /* Textarea floating label */
        background-color: white;
        width: auto;
        right: 0;
        left: 0;
        margin: 1px 18px 1px 1px;
        border-radius: 3px 3px 0 0;
        padding-top: 4px;
    }

    .form-capsule .form-group-textarea .form-control {
        /* Textarea */
        min-height: 100px;
        height: auto;
        max-width: 100%;
        min-width: 100%;
    }

    /********************************************
    ************ Markdown ***********************
    *********************************************/
    .form-capsule .form-group-markdown label {
        /* Markdown textarea floating label */
        background-color: white;
        width: auto;
        right: 0;
        left: 0;
        margin: 1px 18px 1px 1px;
        border-radius: 3px 3px 0 0;
        padding-top: 4px;
    }

    .form-capsule .form-group-markdown .form-control {
        /* Markdown textarea */
        margin-top: 10px;
        min-height: 200px;
        height: auto;
        max-width: 100%;
        min-width: 100%;
    }

    .form-capsule .markdown-upload-button {
        /* Markdown file upload buttong */
        cursor: pointer;
        color: blue;
    }

    .form-capsule .markdown-preview {
        /* Markdown preview */
        margin-top: 10px;
        margin-bottom: 10px;
        height: 230px;
        width: 100%;
        border: 1px solid #CCC;
        border-radius: 4px;
    }

    /********************************************
    ************ Select *************************
    *********************************************/
    .form-capsule .form-group-select label {
        /* Select floating label */
        opacity: 1;
        z-index: 1;
    }

    .form-capsule .form-group-select .form-control {
        /* Select */
        padding-top: 25px;
        padding-bottom: 5px;
        -webkit-appearance: none;
        -moz-appearance: none;
    }

    .select-wrapper {
        /* Select wrapper */
        position: relative;
        z-index: 0;
    }

    .select-wrapper:before {
        /* Select arrow image background */
        content: '';
        position: absolute;
        right: 0;
        top: 0;
        bottom: 0;
        width: 50px;
        background-position: center center;
        background-repeat: no-repeat;
        pointer-events: none;
        background-image: url(https://tlc-web.qcode.co.uk/Graphics/dropdown-arrow-down-dark.svg);
    }

    /********************************************
    ************ Help block *********************
    *********************************************/
    .help-block-inline {
        /* Inline help block */
        position: absolute;
        top: 0;
        right: 25px;
        line-height: 50px;
        margin: 0;
        font-size: 90%;
        color: #CCC;
    }

    /********************************************
    ************ Units help-block ***************
    *********************************************/
    .help-block-floating {
        /* Inline help block for units */
        color: #555;
    }

    .help-block-background {
        right: 0;
        background-color: #eee;
        color: #555;
        border-radius: 0 3px 3px 0;
        height: 48px;
        margin: 1px;
        padding: 0 12px;
    }

    /********************************************
    ************ Tooltip ************************
    *********************************************/
    .tooltip-icon {
        /* Tooltip icon */
        position: absolute;
        top: 15px;
        right: 30px;
    }

    /********************************************
    ************ Radio group ********************
    *********************************************/
    .radio-group {
        border: 1px solid #ccc;
        border-radius: 4px;
    }

    .radio, .radio + .radio {
        border-bottom: 1px solid #ccc;
        margin: 0;
    }

    .radio:last-child {
        border-bottom: 0;
    }

    .radio-option {
        display: none;
    }

    .radio-option:checked + .radio-option-label:before {
        border-color: #000666;
        border-width: 5px;
    }

    .radio-option-label {
        display: table;
    }

    .radio-option-label:before {
        content: "";
        display: block;
        width: 16px;
        height: 16px;
        border: 1px solid #ccc;
        margin: 17px;
        border-radius: 50%;
        background-color: white;
        -webkit-transition: border-width 0.3s ease;
        transition: border-width 0.3s ease;
    }

    .radio-option-label-text {
        display: table-cell;
        vertical-align: middle;
        padding: 10px 10px 10px 0;
        width: 100%;
        margin: 0;
    }


</style>




<script>
    $(function () {
        $(
                "body"
                ).on(
                "keyup keydown cut paste change focus drop",
                ".form-control",
                function () {
                    if ($(this).val().length != 0) {
                        $(this).closest(".form-group").addClass("show-label");
                    } else {
                        $(this).closest(".form-group").removeClass("show-label");
                    }
                }
        );

        $(".form-control").each(function () {
            if ($(this).val().length != 0) {
                $(this).closest(".form-group").addClass("show-label");
            }
        });

        $('[data-toggle="tooltip"]').tooltip({
            container: ".container",
            viewport: {
                selector: ".container",
                padding: 15
            }
        });
        
        $('#birth').flatpickr({});
        
    });

</script>
