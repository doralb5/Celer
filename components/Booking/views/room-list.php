<?php
	if (count($accomodations)) {
		foreach ($accomodations as $a) {
			?>

            <div class = "panel panel-default">
                <div class = "panel-heading">
                   <h3 class = "panel-title">
                      <?=$a->name?>
                   </h3>
                </div>

                <div class = "panel-body">
                    <?php
					if ($a->available) {
						echo $a->total_price;
					} else {
						echo '[$NotAvailable]';
					} ?>
                </div>
                <a href="<?= Utils::getComponentUrl('Booking/getApartment') . '/' . $a->id?>">[$datails]</a>
             </div>

            <?php
		}
	} else {
		echo '[$NoRecordsFound]';
	}
?>

