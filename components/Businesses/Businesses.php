<?php

require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Email.php';

class Businesses_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_b_list', $this->getActionUrl('b_list'));
		$this->model = Loader::getModel('Businesses_sub', 'Businesses');
		$this->view->set('settings', $this->ComponentSettings);
		$this->view->set('PageParameters', WebPage::getParameters());
		$image_baseurl = (isset($this->ComponentSettings['image_baseurl'])) ? $this->ComponentSettings['image_baseurl'] : '';
		$this->view->set('image_baseurl', $image_baseurl);
	}

	public function preload_b_list($id_category = null)
	{
		$_SESSION['locations'] = array();
		$aliascategory = $id_category;
		if (!is_null($id_category) && !is_numeric($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}
			
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$Business_tbl = TABLE_PREFIX . 'Business';
		$BusinessCateg_tbl = TABLE_PREFIX . 'BusinessCategory';
		$Profile_tbl = TABLE_PREFIX . 'BusinessProfile';
		$BC_tbl = TABLE_PREFIX . 'BusinessCateg';
		$sorting = "$Business_tbl.featured DESC, $Profile_tbl.payment DESC";
		if (isset($_POST['order'])) {
			if (isset($_GET['req'])) {
				unset($_GET['req']);
			}
			if (isset($_GET['order'])) {
				unset($_GET['order']);
			}
			$_GET['order'] = $_POST['order'];
			$query_str = http_build_query($_GET);
			$base_uri = explode('?', $_SERVER['REQUEST_URI']);
			$url = "http://{$_SERVER['HTTP_HOST']}{$base_uri[0]}" . (($query_str != '') ? "?$query_str" : '');
			Utils::RedirectTo($url);
		}

		$filter = "$Business_tbl.enabled = '1' AND $Business_tbl.publish_date < NOW() AND $Business_tbl.expire_date > NOW() ";

		$orderby = isset($_GET['order']) ? $_GET['order'] : '';
		if ($orderby != '') {
			$sorting .= ", $orderby";
		}
		if (!is_null($id_category)) {
			$filter .= " AND ($Business_tbl.id_category = {$id_category} OR $BC_tbl.id_category = {$id_category}) ";
			$sorting .= ", $Profile_tbl.relevance DESC, rating DESC, $Business_tbl.company_name";
			$businesses = $this->model->getList($elements_per_page, $offset, $filter, $sorting);
			$totalElements = $this->model->getLastCounter();
		} elseif (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$sorting .= ", $Profile_tbl.relevance DESC, pertinence DESC, rating DESC, $Business_tbl.company_name";
			WebPage::$breadcrumb->removeLast();
			WebPage::$breadcrumb->addDir($this->view->getTerm('businesses'), $this->getActionUrl('categ_list'));
			WebPage::$breadcrumb->addDir($this->view->getTerm('searching_for') . $_REQUEST['query'], $this->getActionUrl('b_list') . '?query=' . $_REQUEST['query']);

			$searchFields = array(
				array('field' => "$Business_tbl.company_name", 'peso' => 10000),
				array('field' => "$BusinessCateg_tbl.category", 'peso' => 30),
				array('field' => "$Business_tbl.tags", 'peso' => 70),
				array('field' => "$BusinessCateg_tbl.tags", 'peso' => 50),
				array('field' => "$Business_tbl.description", 'peso' => 1),
			);
			$businesses = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
			$totalElements = $this->model->getLastCounter();
		} else {
			$businesses = array();
			$totalElements = 0;
		}

		foreach ($businesses as &$business) {
			$business->subdomain = $this->model->getSubdomain("target_id = {$business->id} AND type = 'B'");
		}
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('businesses', $businesses);

		if (!is_null($id_category)) {
			$category = $this->model->getCategory($id_category);
			$this->view->set('category', $category);
		}
		$_SESSION['locations'] = serialize($businesses);
		$result['businesses'] = $businesses;
		return $result;
	}

	public function preload_business_list_u($id_categ = '')
	{
		if (isset($_SESSION['user_auth']['id'])) {
			$id_user = $_SESSION['user_auth']['id'];

			if ($_SESSION['user_auth']['id'] === $id_user) {
				$elements_per_page = 20;
				$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
				$offset = ($page - 1) * $elements_per_page;
				$Business_tbl = TABLE_PREFIX . 'Business';
				$Profile_tbl = TABLE_PREFIX . 'BusinessProfile';
				$sorting = "$Business_tbl.featured DESC, $Profile_tbl.payment DESC";
				$filter = ' 1 ';

				if (!is_null($id_user)) {
					$filter .= "AND ($Business_tbl.id_user = {$id_user}) ";
				}

				$businesses = $this->model->getList($elements_per_page, $offset, $filter, $sorting);

				foreach ($businesses as &$business) {
					$business->subdomain = $this->model->getSubdomain("target_id = {$business->id} AND type = 'B'");
				}

				$totalElements = $this->model->getLastCounter();
				$this->view->set('page', $page);
				$this->view->set('totalElements', $totalElements);
				$this->view->set('elements_per_page', $elements_per_page);
				$this->view->set('businesses', $businesses);
			} else {
				$this->view->set('page', $page);
				$this->view->set('totalElements', $totalElements);
				$this->view->set('elements_per_page', $elements_per_page);
				$this->view->set('businesses', $businesses);
				$this->view->set('settings', $this->ComponentSettings);
				$parameters = WebPage::getParameters();
				$this->view->set('parameters', $parameters);
				$this->view->set('settings', $this->ComponentSettings);
				//Placeholders
				//$this->AddError("Ndalohet hyrja!");
			}

			$result['businesses'] = $businesses;
			return $result;
		}
		Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
	}

	public function business_list_u($id_categ = '')
	{
		HeadHTML::setTitleTag('Businesses' . ' | ' . CMSSettings::$website_title);
		if (isset($_SESSION['user_auth']['id'])) {
			$id_user = $_SESSION['user_auth']['id'];

			//Webpage Parameters and Component settings
			$parameters = WebPage::getParameters();
			$this->view->set('parameters', $parameters);
			$this->view->set('settings', $this->ComponentSettings);
			//Placeholders
			WebPage::$breadcrumb->addDir($this->view->getTerm('my_businesses'), $this->getActionUrl('business_list_u'));
			$businesses = $this->preload_result['businesses'];
			$view = (isset($parameters['view'])) ? $parameters['view'] : 'business-list-row';
			$this->view->render($view);
		} else {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}

	public function b_list($id_category = null)
	{
		if (!is_null($id_category) && !is_numeric($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}

		HeadHTML::setTitleTag('Businesses' . ' | ' . CMSSettings::$website_title);
		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		//$businesses = $this->preload_result['businesses'];
		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'events' . ((!is_null($id_category)) ? "/$id_category" : ''));
		//BreadCrumb
		if (!is_null($id_category)) {
			WebPage::$breadcrumb->removeLast();
			WebPage::$breadcrumb->addDir($this->view->getTerm('Categories'), $this->getActionUrl('categ_list'));
			$category = $this->model->getCategory($id_category);
			if ($category != '') {
				WebPage::$breadcrumb->addDir($category->category, $this->getActionUrl('b_list'));
				$this->view->set('category', $category);
				HeadHTML::setTitleTag($category->category . ' | ' . CMSSettings::$website_title);
			}
		} elseif (isset($_REQUEST['query'])) {
			HeadHTML::setTitleTag($this->view->getTerm('searching_for') . ' ' . $_REQUEST['query'] . ' | ' . CMSSettings::$website_title);
		}
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'business-list-row';
		$this->view->render($view);
	}

	public function preload_b_show($id = null)
	{
		$_SESSION['locations'] = array();
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		} elseif (is_null($id)) {
			HeadHTML::setTitleTag('Error 404' . ' | ' . CMSSettings::$website_title);
			$this->view->renderError('404');
			exit;
		}
		$business = $this->model->getBusiness($id);
		$comments = $this->model->getComments($id);
		$this->view->set('comments', $comments);

		WebPage::$breadcrumb->removeLast();
		if (is_null($business)) {
			HeadHTML::setTitleTag('Error 404' . ' | ' . CMSSettings::$website_title);
			$this->view->renderError('404');
			exit;
		}
		$_SESSION['locations'] = serialize($business);
		$result['business'] = $business;
		return $result;
	}

	public function b_show($id = null)
	{
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		} elseif (is_null($id)) {
			$this->view->renderError('404');
			exit;
		}

		$business = $this->preload_result['business'];
		$offers = $this->model->getOfferList(' id_biz = ' . $id, 10, 0);
		$offers_count = $this->model->getLastCounter();
		$business->subdomain = $this->model->getSubdomain("target_id = {$business->id} AND type = 'B'");
		$this->view->set('business', $business);
		$this->view->set('offers', $offers);
		$this->view->set('offers_count', $offers_count);
		if (isset($_POST['send'])) { // $_POST['links'] should be epmty to check and avoid spaming from robots.
			if (isset($_POST['links']) && $_POST['links'] == '') {
				($_POST['message'] == '') ? $this->AddError('[$message_required]') : '';
				($_POST['email'] == '') ? $this->AddError('[$email_required]') : '';
				($_POST['name'] == '') ? $this->AddError('[$name_required]') : '';
				($_POST['tel'] == '') ? $this->AddError('[$name_required]') : '';

				if (count($this->view->getErrors()) == 0) {
					$template = 'mails/email_user_contact_business';
					$this->contactForm($id, $business, $_POST, $template);
				}
			} else {
				$this->view->AddError('Robot Detected 004100');
			}
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'businesses');
		$this->addCategoriesToBreadCrumb($business->id_category);
		WebPage::$breadcrumb->addDir(StringUtils::CutString($business->company_name, 50), $this->getActionUrl('b_show') . '/' . Utils::url_slug($business->company_name) . '-' . $id);
		//Title tag
		HeadHTML::setTitleTag(StringUtils::CutString($business->company_name, 50) . ' | ' . CMSSettings::$website_title);
		$category = $this->model->getCategory($business->id_category);
		$this->view->set('category', $category);
		$this->view->set('settings', $this->ComponentSettings);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'business-details-map';
		if ($business->enabled == '0') {
			HeadHTML::setTitleTag('Error 404' . ' | ' . CMSSettings::$website_title);
			$this->view->renderError('404');
			exit;
		}

		//*******Business Related****//
		if ($business->id_profile == 1 || $business->id_profile == 4) {
			$businesses_related = $this->getBusinessRelated($business->id_category, $id);
			$this->view->set('businesses_related', $businesses_related);
		}
		//            echo '<!--';
		//            print_r($businesses_related);
		//            echo '-->';
		//*******Business Related****//
		// ****Business Offers**** //
		$openh = $this->model->getBusinessOpenHours($id);
		$openh_ordered = array();
		for ($day = 0; $day < 7; $day++) {
			$h = Utils::in_ObjectArray($openh, 'day', $day);
			if (!$h) {
				$h = new BusinessOpenHours_Entity();
				$h->business_id = $id;
				$h->day = "$day";
				$h->closed = '1';
			}
			$openh_ordered[$day] = $h;
		}
		$this->view->set('openh', $openh_ordered);
		$this->view->render($view);
	}

	public function offer_details($id)
	{
		$offer = $this->model->getOffer($id);
		$business = $this->model->getBusiness($offer->id_biz);

		//related offers
		$filter_related = "cms_Business.id != $business->id";
		$related_offers = $this->model->getOfferList($filter_related);

		//similar offers
		$filter_similar = "cms_Business.id = $business->id and cms_BusinessOffer.id != $id";
		$similar_offers = $this->model->getOfferList($filter_similar);

		$this->view->set('similar_offers', $similar_offers);
		$this->view->set('related_offers', $related_offers);
		$this->view->set('offer', $offer);
		$this->view->set('business', $business);
		$this->view->render('offer-details');
	}

	public function categ_list($parent_id = null)
	{
		$order = 'category';
		$filter = '';

		if (!is_null($parent_id)) {
			$filter = "parent_id = $parent_id";
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		$categories = $this->model->getCategories(-1, 0, $filter, $order);
		$totalElements = $this->model->getLastCounter();
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', count($categories));
		$this->view->set('categories', $categories);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'businesses/categories');

		//BreadCrumb
		if (!is_null($parent_id)) {
			$this->addCategoriesToBreadCrumb($parent_id);
		} else {
			WebPage::$breadcrumb->addDir($this->view->getTerm('Categories'), $this->getActionUrl('categ_list'));
		}
		HeadHTML::setTitleTag($this->view->getTerm('Categories') . ' | ' . CMSSettings::$website_title);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'categories_list';
		$this->view->render($view);
	}

	public function checkAviableSubdomain()
	{
		$domain = strtolower(CMSSettings::$webdomain);
		$newSub = trim(str_replace(' ', '', $_POST['subdomain'])) . '.' . $domain;
		$newSub = strtolower($newSub);
		$res = $this->model->getSubdomainByName($newSub);
		$returnArray = array($res['count'], $newSub);
		echo json_encode($returnArray);
	}

	public function ajx_checkAviableEmail()
	{
		$domain = strtolower(CMSSettings::$webdomain);
		$newSub = trim(str_replace(' ', '', $_POST['business_mail'])) . '@' . $domain;
		$newSub = strtolower($newSub);
		$res = $this->model->getEmailByName($newSub);
		$returnArray = array($res['count'], $newSub);
		echo json_encode($returnArray);
	}

	public function insert_business()
	{
		if (isset($_GET['insert']) && $_GET['insert'] == 'ok') {
			$business = $this->model->getBusiness($_SESSION['biz_id_inserted']);
			$newuser = $_SESSION['newuser'];
			$this->view->set('newuser', $newuser);
			$this->view->set('business', $business);
			$this->view->render('insert_ok');
			return;
		}

		WebPage::$breadcrumb->addDir($this->view->getTerm('InsertBusiness'), $this->getActionUrl('insert_business'));

		$categories = $this->model->getCategories(200);
		$this->view->set('categories', $categories);

		if (isset($_POST['save'])) {
			session_start();
			($_POST['company_name'] == '') ? $this->AddError('[$company_name_required]') : '';
			($_POST['additional_categ'] == '') ? $this->AddError('[$main_category_required]') : '';
			($_POST['address'] == '') ? $this->AddError('[$address_required]') : '';
			($_POST['firstname'] == '') ? $this->AddError('[$firstname_required]') : '';
			($_POST['lastname'] == '') ? $this->AddError('[$lastname_required]') : '';
			($_POST['email'] == '') ? $this->AddError('[$email_required]') : '';
			//$regex = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";
			//(!preg_match($regex, $_POST['phone'])) ? $this->AddError('[$number_invalid]') : '';
			(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) ? $this->AddError('[$email_not_valid]') : '';
			(strlen($_POST['company_name']) > 100) ? $this->AddError('[$company_name_length]') : '';
			//(!isset($_FILES['image']['tmp_name']) || strlen($_FILES['image']['tmp_name']) == 0) ? $this->view->AddError('[$image_required]') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('[$invalid_image_format]') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				$this->model->startTransaction();

				$users_md = Loader::getModel('Users');
				$Users_tbl = TABLE_PREFIX . 'User';
				$usr = $users_md->getList(1, 0, "$Users_tbl.email = '{$_POST['email']}'");

				if (count($usr) > 0 && is_null(UserAuth::getLoginSession())) {
					$id_user = $usr[0]->id;
					$_SESSION['newuser'] = false;
				} elseif (!is_null(UserAuth::getLoginSession())) {
					$user = UserAuth::getLoginSession();
					$id_user = $user['id'];
					$_SESSION['newuser'] = false;
				} else {
					$usr = new User_Entity();
					$usr->email = $_POST['email'];
					$usr->username = $_POST['email'];
					$usr->firstname = $_POST['firstname'];
					$usr->lastname = $_POST['lastname'];
					$usr->enabled = '1';
					$password = Utils::GenerateRandomPassword(8);
					$usr->password = md5($password);
					$usr->hash = md5(rand(0, 1000));
					$usr->plain_password = $password;
					$usr->lang = CMSSettings::$default_lang;
					$id_user = $users_md->saveUser($usr);
					if ($id_user) {
						$_SESSION['newuser'] = true;
					}
				}

				$imagefile = '';
				if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
					$imagefile = time() . '_' . Utils::clean($_POST['company_name']);
					$imagefile = $this->saveImage($_FILES['image']['tmp_name'], $imagefile, 'businesses');
				}

				$business = new Business_Entity();
				$business->company_name = $_POST['company_name'];
				$domain = strtolower(CMSSettings::$webdomain);
				$newSub = trim(str_replace(' ', '', $_POST['subdomain'])) . '.' . $domain;
				$newSub = strtolower($newSub);
				$business->address = $_POST['address'];
				$business->city = $_POST['city'];
				$business->coordinates = ltrim(rtrim($_POST['coordinates'], ')'), '(');
				$business->description = $_POST['description'];
				$business->subdomain = $newSub;
				$business->website = $_POST['website'];
				$business->publish_date = date('Y-m-d H:i:s');
				$business->expire_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d  H:i:s', time()) . ' + 365 day'));
				$business->location = $_POST['location'];
				$business->facebook = $_POST['facebook'];
				$business->instagram = $_POST['instagram'];
				$business->linkedin = $_POST['linkedin'];
				$business->twitter = $_POST['twitter'];
				$business->google = $_POST['google'];
				if ($id_user == '') {
					$business->id_user = null;
				} else {
					$business->id_user = $id_user;
				}
				$business->email = $_POST['email'];
				$business->phone = $_POST['phone'];
				$business->image = $imagefile;
				$business->business_mail = $_POST['business_mail'] . '@' . $_POST['domain'];
				$id_web_profile = isset($this->ComponentSettings['web_profile']) ? $this->ComponentSettings['web_profile'] : 4;
				$business->id_profile = $id_web_profile;
				$business->enabled = 0;
				if (isset($_POST['additional_categ']) && count($_POST['additional_categ'])) {
					$business->id_category = array_shift($_POST['additional_categ']);
				}
				$inserted_id = $this->model->saveBusiness($business);

				if (!is_array($inserted_id)) {
					//Shtojme kategorite shtese

					$errors = 0;
					if (isset($_POST['additional_categ']) && count($_POST['additional_categ'])) {
						foreach ($_POST['additional_categ'] as $categ) {
							$bc = new BusinessCateg_Entity();
							$bc->id_business = $inserted_id;
							$bc->id_category = $categ;
							$res = $this->model->saveBusinessCategory($bc);
							if (is_array($res)) {
								$errors++;
							}
						}
					}

					if ($errors == 0) {
						$this->model->commit();

						// $this->model->startTransaction();
						$new = new Subdomain_Entity();
						$new->subdomain = $newSub;
						$new->target_id = $inserted_id;
						$new->type = 'B';
						$new->target_url = '';
						$new->meta_keywords = str_replace(' ', ',', $_POST['description']);
						$new->meta_description = $_POST['description'] . CMSSettings::$webdomain;
						if ($this->model->saveSubdomain($new)) {
							$this->view->AddNotice('Webfaqja e re <a href="' . $newSub . '"> ' . $newSub . ' </a> e biznesit tuaj u krijua me sukses!');
						} else {
							$this->view->AddError('Pati nje gabim gjate krijimit te webfaqes se re: ' . $newSub);
						}

						$this->notifyAdminOnInsertBusiness($business);
						//user
						$this->notifyBusinessInserted($business, $password);
						$this->LogsManager->registerLog('Business', 'insert', 'Business inserted with id : ' . $inserted_id, $inserted_id);
						$_SESSION['biz_id_inserted'] = $inserted_id;
						Utils::RedirectTo($this->getActionUrl('insert_business') . '?insert=ok');
					} else {
						$this->model->rollback();
						$this->view->AddError('Problem while adding additional categories.');
					}
				} else {
					$this->model->rollback();
					$this->view->AddError('Problem while inserting the business.');
				}
			}
		}
		if ($_SERVER['HTTP_HOST'] == 'cittanelweb.it') {
			$locations = $this->model->getItalianLocations();
		} else {
			$locations = $this->model->getAlbanianLocations();
		}
		$this->view->set('ComSettings', $this->ComponentSettings);
		$this->view->set('locations', $locations);
		$this->view->render('insert_business');
	}

	// Advance search
	public function ajx_advanceSearch()
	{
		$return_arr = array();

		$query = $_REQUEST['query'];
		if ($query == '') {
			return json_encode($return_arr);
		}

		$Business_tbl = TABLE_PREFIX . 'Business';
		$BusinessCateg_tbl = TABLE_PREFIX . 'BusinessCategory';
		$filter = "$Business_tbl.enabled = '1'  AND $Business_tbl.publish_date < NOW() AND $Business_tbl.expire_date > NOW() ";

		$searchFields = array(
			array('field' => "$Business_tbl.company_name", 'peso' => 100),
			array('field' => "$BusinessCateg_tbl.category", 'peso' => 30),
		);
		$Profile_tbl = TABLE_PREFIX . 'BusinessProfile';
		$sorting = "$Business_tbl.featured DESC, $Profile_tbl.payment DESC, $Profile_tbl.relevance DESC, pertinence DESC, rating DESC, $Business_tbl.company_name";
		$businesses = $this->model->search($query, $searchFields, $filter, $sorting, 6);

		foreach ($businesses as $biz) {
			$row_array['id'] = $biz->id;
			$row_array['image'] = $biz->image;
			$row_array['image_url'] = $biz->image_url;
			$row_array['company_name'] = $biz->company_name;
			$row_array['category'] = $biz->category;
			$row_array['url_slug'] = Utils::url_slug($biz->company_name) . '-' . $biz->id;
			array_push($return_arr, $row_array);
		}
		echo json_encode($return_arr);
	}

	private function saveImage($tmpfile, $newfile, $folder)
	{
		$path = DS . rtrim(ltrim($folder, DS), DS) . DS;
		$newfile = rand(0, 999) . DS . $newfile;
		if (isset($this->ComponentSettings['image_baseurl'])) {
			$this->saveImageToDb($tmpfile, $newfile, $path);
			$ch = curl_init('http://www.neshqiperi.al/com/Businesses/syncImagesFromDb');
			curl_exec($ch);
			curl_close($ch);
		} else {
			$target = DOCROOT . MEDIA_ROOT . $path;
			if (!file_exists(dirname($target . $newfile))) {
				mkdir(dirname($target . $newfile), 0755, true) or die('Errore durante la creazione della folder!');
			}
			move_uploaded_file($tmpfile, $target . $newfile);
		}
		return $newfile;
	}

	private function deleteImage($filename, $folder)
	{
		if (isset($this->ComponentSettings['image_baseurl'])) {
			$this->saveImageToDb(null, $filename, $folder, true);
			$ch = curl_init('http://www.neshqiperi.al/com/Businesses/syncImagesFromDb');
			curl_exec($ch);
			curl_close($ch);
		} else {
			$target = DOCROOT . MEDIA_ROOT . $folder . DS;
			if (file_exists($target . $filename)) {
				unlink($target . $filename);
			}
		}
	}

	private function addCategoriesToBreadCrumb($id_categ)
	{
		$category = $this->model->getCategory($id_categ);
		if (is_null($category->parent_id)) {
			WebPage::$breadcrumb->addDir($category->category, Utils::getComponentUrl('Businesses/b_list/' . $id_categ));
		} else {
			$this->addCategoryToBreadCrumb($category->parent_id);
			WebPage::$breadcrumb->addDir($category->category, Utils::getComponentUrl('Businesses/b_list/' . $id_categ));
		}
	}

	public function sendBusinessCredencials($id_user, $password)
	{
		$user_md = Loader::getModel('Users');
		$user = $user_md->getUser($id_user);

		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH . 'Businesses' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Businesses' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('password', $password);
		$message->set('user', $user);
		$txt = $message->render('mails/email_business_online', true);

		$sended = Email::sendMail($user->email, CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, $business->company_name . ' - ' . $this->view->getTerm('your_business_in') . CMSSettings::$webdomain, $txt);
		if ($sended) {
			return true;
		}
		return false;
	}

	public function notifyBusinessInserted($business, $password)
	{
		$user_md = Loader::getModel('Users');
		$user = $user_md->getUser($business->id_user);
		if ($password == '') {
			$password = $user->plain_password;
		}
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH . 'Businesses' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Businesses' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('password', $password);
		$message->set('business', $business);
		$message->set('user', $user);
		$txt = $message->render('mails/email_business_online', true);

		$sended = Email::sendMail($business->email, CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, $business->company_name . ' - ' . $this->view->getTerm('your_business_in') . CMSSettings::$webdomain, $txt);
		if ($sended) {
			return true;
		}
		return false;
	}

	public function notifyAdminOnInsertBusiness($business)
	{
		$user_md = Loader::getModel('Users');
		$user = $user_md->getUser($business->id_user);

		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH . 'Businesses' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Businesses' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('business', $business);
		$message->set('user', $user);
		$txt = $message->render('mails/email_business_online_admin', true);

		$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, $business->company_name . ' - ' . $this->view->getTerm('new_business_inserted') . CMSSettings::$webdomain, $txt);
		if ($sended) {
			return true;
		}
		return false;
	}

	public function notifyAdminOnRatedBusiness($id_business, $business, $name, $comment, $email, $rating)
	{
		$message = new BaseView();
		$link = Utils::getComponentUrl('Businesses/b_show/' . $id_business);
		$message->setViewPath(COMPONENTS_PATH . 'Businesses' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Businesses' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('business', $business);
		$message->set('name', $name);
		$message->set('comment', $comment);
		$message->set('email', $email);
		$message->set('rating', $rating);
		$message->set('id_business', $id_business);
		$message->set('link', $link);
		$txt = $message->render('mails/email_business_reviews_admin', true);
		$sended = Email::sendMail('developers@bluehat.al', $name, $email, 'neTirane.al - Reçension i ri në portalin ' . $business, $txt);
		if ($sended) {
			return true;
		}
		return false;
	}

	public function notifyBusinessRated($business, $name, $comment, $email, $rating, $id_business)
	{
		$message = new BaseView();
		$link = Utils::getComponentUrl('Businesses/business_edit/' . $id_business);
		$message->setViewPath(COMPONENTS_PATH . 'Businesses' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Businesses' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('business', $business);
		$message->set('name', $name);
		$message->set('comment', $comment);
		$message->set('email', $email);
		$message->set('rating', $rating);
		$message->set('link', $link);
		$txt = $message->render('mails/email_business_reviews', true);

		$sended = Email::sendMail($business, $name, $email, 'neTirane.al - Reçension i ri në biznesin tuaj', $txt);

		if ($sended) {
			return true;
		}
		return false;
	}

	public function requireSticker()
	{
		if (isset($_GET['applicant_business_token'])) {
			$business_sticker_entity = $this->model->checkTokenSticker($_GET['applicant_business_token']);

			if ($business_sticker_entity->required != 0) {
				Utils::RedirectTo(Utils::getComponentUrl('StaticBlocks/printBlock/33'));
			} elseif ($business_sticker_entity != null) {
				$business_sticker_entity->required = 1;

				$business_md = Loader::getModel('Businesses_sub', 'Businesses');
				$business_md->saveStickerRecord($business_sticker_entity);

				$business = $business_md->getBusiness($business_sticker_entity->id_business);

				$user_md = Loader::getModel('Users');
				$user = $user_md->getUser($business->id_user);

				$message = new BaseView();
				$message->setViewPath(COMPONENTS_PATH . 'Businesses' . DS . 'views' . DS);
				$message->setLangPath(COMPONENTS_PATH . 'Businesses' . DS . 'lang' . DS);
				$message->setLang($this->getLang());
				$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
				$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
				$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
				$message->renderTemplate(false);
				$message->set('business', $business);
				$message->set('user', $user);
				$txt = $message->render('mails/sticker_required', true);

				$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, $this->view->getTerm('sticker_required') . ' ' . CMSSettings::$webdomain, $txt);
				if ($sended) {
					Utils::RedirectTo(Utils::getComponentUrl('StaticBlocks/printBlock/31'));
				} else {
					return false;
				}
			} else {
				//error page, token is not valid
				Utils::RedirectTo(Utils::getComponentUrl('StaticBlocks/printBlock/32'));
			}
		} else {
			Utils::RedirectTo(Utils::getComponentUrl('Webpage/render_error/404'));
		}
	}

	public function contactForm($id_business, $business, $post, $template = '')
	{
		$message = new BaseView();
		$link = Utils::getComponentUrl('Businesses/messages_list/' . $id_business);
		$message->setViewPath(COMPONENTS_PATH . 'Businesses' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Businesses' . DS . 'lang' . DS);
		$message->setLang(CMSSettings::$default_lang);
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('post', $post);
		$message->set('business', $business);
		$message->set('link', $link);
		if ($template == '') {
			$template = 'mails/email_user_contact_business';
		}
		$txt = $message->render($template, true);
		$res = $this->model->saveMessage($business->id, $post['name'], $post['email'], $post['tel'], $post['message']);

		if (is_array($res)) {
			Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, CMSSettings::$sender_mail, 'Error Reporting' . CMSSettings::$webdomain, 'An error has occured during the saving the Business Message:<br/><br/>' . $res[0] . $txt);
		}

		$sended = @Email::sendMail($business->email, $_POST['name'], $_POST['email'], $message->getTerm('new_message') . CMSSettings::$webdomain, $txt, array(CMSSettings::$EMAIL_ADMIN));

		if ($sended) {
			$this->view->AddNotice('[$message_sent]');
			return true;
		}
		$this->view->AddError('[$message_failed]');
		return false;
	}

	public function ajx_rateBusiness()
	{
		if (isset($_POST['id_biz'])) {
			if (!isset($_COOKIE[$_POST['id_biz'] . '_rating_' . str_replace('.', '_', $_SERVER['HTTP_HOST'])]) or true) {
				if ($_POST['rating'] > 0 && $_POST['rating'] <= 5) {
					//                    $rating = new BusinessRating_Entity();
					//                    $rating->id_biz = $_POST['id_biz'];
					//                    $rating->rating = $_POST['rating'];
					//                    $rating->ip_client = $_SERVER['REMOTE_ADDR'];
					//                    $filter = ' ip_client = "' . $_SERVER['REMOTE_ADDR'] . '" AND TIMESTAMPDIFF(DAY, cms_BusinessRating.publish_date, now()) < 1 AND id_biz = "' . $_POST['id_biz'] . '"';
					//                    $ip_check = $this->model->getRating($filter);
					//                    if (count($ip_check) === 0) {
					//                        $id = $this->model->saveRating($rating);
					//                        $_SESSION[$_POST['id_biz'] . '_rating'] = $id; // creates a new Session value to be used to ajxReviewBusiness
					//                        echo ",";
//
					//                    } else {
					//                        $_SESSION[$_POST['id_biz'] . '_rating'] = $ip_check[0]->id;
					//                        echo 'Ju mund te jepni vetem nje here ne dite vlersimin tuaj!<br/> Brenda dite ju vetem mund te modifikoni vlersimin tuaj,';
					//                    }
					$filter = ' ip_client = "' . $_SERVER['REMOTE_ADDR'] . '" AND TIMESTAMPDIFF(DAY, cms_BusinessRating.publish_date, now()) < 1 AND id_biz = "' . $_POST['id_biz'] . '"';
					$ip_check = $this->model->getRating($filter);
					if (count($ip_check) != 0) {
						echo 'Ju mund te jepni vetem nje here ne dite vlersimin tuaj!<br/> Brenda dite ju vetem mund te modifikoni vlersimin tuaj,';
					}
					$cookie_name = $_POST['id_biz'] . '_rating_' . str_replace('.', '_', $_SERVER['HTTP_HOST']);

					setcookie($cookie_name, $_POST['rating'], time() + 2678400, '/', $_SERVER['HTTP_HOST']);

				//echo $_POST['id_biz'] . ',' . $_POST['rating'];
				} else {
					"Don't hack me!";
				}
			}
		}
	}

	//    public function review_business(){
	//        $cookie_name = $_POST['id_biz'] . "_rating_" . str_replace('.', '_', $_SERVER['HTTP_HOST']);
	//        $this->view->set('cookie_name', $cookie_name);
	//        $this->view->render('review_business');
	//    }

	public function ajx_reviewBusiness()
	{
		if (isset($_POST['s-review'], $_POST['id_biz'])) {
			$cookie_name = $_POST['id_biz'] . '_rating_' . str_replace('.', '_', $_SERVER['HTTP_HOST']);
			if (isset($_COOKIE[$cookie_name])) {
				$filter = ' ip_client = "' . $_SERVER['REMOTE_ADDR'] . '" AND TIMESTAMPDIFF(DAY, cms_BusinessRating.publish_date, now()) < 1 AND id_biz = "' . $_POST['id_biz'] . '"';
				$ip_check = $this->model->getRating($filter);
				$rating = new BusinessRating_Entity();

				if (count($ip_check) != 0) {
					$rating->id = $ip_check[0]->id;
				}
				if ($_POST['comment'] == '' || $_POST['name'] == '' || $_POST['email'] == '' || $_POST['tel'] == '' || intval($_COOKIE[$cookie_name]) < 1) {
					echo '<p class="msg-success alert-danger alert-costumized" name="messageajx" >All fields are required! </p>';
					return;
				}
				$rating->id_biz = $_POST['id_biz'];
				$rating->rating = intval($_COOKIE[$cookie_name]);
				$rating->comment = strip_tags($_POST['comment']);
				$rating->name = $_POST['name'];
				$rating->email = $_POST['email'];
				$rating->tel = $_POST['tel'];
				$rating->ip_client = $_SERVER['REMOTE_ADDR'];
				$saved = $this->model->saveRating($rating);
				if ($saved) {
					unset($_SESSION[$_POST['id_biz'] . '_rating']);
				}
				echo '<p class="msg-success alert-success alert-costumized" name="messageajx" >' . $this->view->getTerm('review_submited') . '</p>';
			}
		}

		$sended = false;
		$biz = $this->model->getBusiness($_POST['id_biz']);
		if (!is_null($biz)) {
			//admin
			$admin_notified = @$this->notifyAdminOnRatedBusiness($_POST['id_biz'], $biz->company_name, $_POST['name'], $_POST['comment'], $_POST['email'], $_POST['rating']);
			//user
			$business_notified = @$this->notifyBusinessRated($biz->email, $_POST['name'], $_POST['comment'], $_POST['email'], $_POST['rating'], $_POST['id_biz']);
			if (!$admin_notified) {
				echo '<p class="msg-success alert-danger alert-costumized" name="messageajx" >Email sending to admin failed! </p>';
			}
			if (!$business_notified) {
				echo '<p class="msg-success alert-danger alert-costumized" name="messageajx" >Email sending to business failed! </p>';
			}
		}
	}

	public function ajx_checkRatingCookie()
	{
		if (isset($_POST['id_biz'])) {
			$filter = ' ip_client = "' . $_SERVER['REMOTE_ADDR'] . '" AND TIMESTAMPDIFF(HOUR, cms_BusinessRating.publish_date, now()) < 1 AND id_biz = "' . $_POST['id_biz'] . '"';
			$ip_check = $this->model->getRating($filter);
			if (count($ip_check === 0)) {
				$id_biz = $_POST['id_biz'];
				$cookie_name = $id_biz . '_rating_' . str_replace('.', '_', $_SERVER['HTTP_HOST']);

				if (isset($_COOKIE[$cookie_name])) {
					echo json_encode(intval($_COOKIE[$cookie_name]));
					exit;
				}
			} else {
				echo json_encode(intval($ip_check[0]->id));
				exit;
			}
			echo json_encode(0);
		}
	}

	public function ajx_getRatingValue()
	{
		if (isset($_POST['id_biz'])) {
			$result = $this->model->getRatingValue($_POST['id_biz']);

			if (!is_null($result)) {
				if ($result['count'] == 0) {
					echo '0|0';
				} else {
					$average = round(intval($result['sum']) / intval($result['count']), 2);
					echo $average . '|' . intval($result['count']);
				}
			} else {
				echo '0|0';
			}
		}
	}

	public function ajx_getRatingValueBList()
	{
		if (isset($_POST['id_biz'])) {
			$arr = array();
			$result = $this->model->getRatingValue($_POST['id_biz']);

			if (!is_null($result)) {
				if ($result['count'] == 0) {
					$arr = array('rate' => 0, 'half' => 0);
				} else {
					$average = round(intval($result['sum']) / intval($result['count']), 2);
					$average_floor = floor($average);
					$points = $average - $average_floor;
					if ($points >= 0.25 && $points <= 0.75) {
						//is half
						$average = $average_floor;
						$arr = array('rate' => $average, 'half' => 1);
					} elseif ($points > 0.75) {
						//is up
						$average = $average_floor + 1;
						$arr = array('rate' => $average, 'half' => 0);
					} else {
						//is low
						$average = $average_floor;
						$arr = array('rate' => $average, 'half' => 0);
					}
				}
			}

			echo json_encode($arr);
		}
	}

	public function ajx_addStatistic()
	{
		$id_business = isset($_POST['id_business']) ? $_POST['id_business'] : null;
		$type = isset($_POST['type']) ? $_POST['type'] : null;

		if (is_null($id_business) || is_null($type)) {
			return;
		}

		$this->model->startTransaction();
		$stat = new BusinessStatistic_Entity();
		$stat->id_business = $id_business;
		$stat->type = $type;
		$stat->ip_client = $_SERVER['REMOTE_ADDR'];
		$result = $this->model->saveStatistic($stat);

		if (!is_array($result)) {
			$busStat = $this->model->getBusStatictics(1, 0, "id_business = {$id_business}");

			$statCreated = true;
			if (count($busStat) == 0) {
				$busStat = new BusinessStatistics_Entity();
				$busStat->id_business = $id_business;
				$statCreated = $this->model->saveBusStatistic($busStat);
			}

			if (!is_array($statCreated)) {
				$incremented = $this->model->incrementStatistic($id_business, $type);
			} else {
				$this->model->rollback();
			}

			if (!is_array($incremented)) {
				$this->model->commit();
				return true;
			}
		} else {
			$this->model->rollback();
		}

		return false;
	}

	public function business_panel()
	{
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'business-panel';
		$this->view->render($view);
	}

	public function business_edit($id = null)
	{
		if (isset($_SESSION['user_auth']['id'])) {
			$business = $this->model->getBusiness($id);
			$id_user = $_SESSION['user_auth']['id'];
			if ($business && $id_user === $business->id_user) {
				WebPage::$breadcrumb->addDir($this->view->getTerm('my_businesses'), $this->getActionUrl('business_list_u'));
				Utils::saveCurrentPageUrl();
				WebPage::getParameters();
				WebPage::$breadcrumb->addDir($business->company_name, $this->getActionUrl('business_edit') . DS . $id);
				$comments = $this->model->getComments($id);
				if (isset($_POST['save-offer'])) {
					$offer = $this->AddOffer($id, $_POST);
					if ($offer != null) {
						$this->view->addNotice('[$offer_inserted]');
					}
				}

				if (isset($_POST['AddImage'])) {
					$this->BusinessOfferImageUpload($this->model->getOffer($id_offer), $_FILES);
				}

				$openinghours = $this->model->getBusinessOpenHours($business->id);
				$availPayments = $this->model->getPayMethods();
				$currPayments = $this->model->getPaymentMethodsOfBusiness($business->id);

				$this->view->set('opnhours', $openinghours);
				$this->view->set('comments', $comments);
				$this->view->set('business', $business);
				$this->view->set('availPayments', $availPayments);
				$this->view->set('currPayments', $currPayments);

				$this->view->render('business-details-map-admin');
			} else {
				HeadHTML::setTitleTag('Error 404' . ' | ' . CMSSettings::$website_title);
				$this->view->renderError('404');
			}
		} else {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . $this->getActionUrl(business_edit) . '/' . $id);
		}
	}

	public function myBizReviews($id)
	{
		$comments = $this->model->getComments($id);
		$this->view->set('comments', $comments);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'reviews-list';
		$this->view->render($view);
	}

	public function edit_your_web($id)
	{
		$business = $this->model->getBusiness($id);
		$subdomain = $this->model->getSubdomain("target_id = {$id} AND type = 'B'");
		$this->view->set('subdomain', $subdomain);
		$this->view->set('business', $business);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'edit-your-web';
		$this->view->render($view);
	}

	public function messages_list($id)
	{
		$business = $this->model->getBusiness($id);
		$this->view->set('id_user', $business->id_user);
		if (isset($_SESSION['user_auth'])) {
			if ($business->id_user == $_SESSION['user_auth']['id']) {
				$messages = $this->model->getMessages($id);
				$this->view->set('messages', $messages);
			}
		} else {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . $this->getActionUrl(messages_list) . '/' . $id);
		}
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'messages-list';
		$this->view->render($view);
	}

	public function ajx_setMessageRead($id = '')
	{
		if (!is_null($id)) {
			$message = $this->model->setMessageRead($id, true);
			if ($message) {
				echo json_encode('1');
			}
		} else {
			echo json_encode('0');
		}
	}

	public function ajx_saveDescription()
	{
		$business = new Business_Entity();
		if (isset($_POST['save_description'])) {
			$this->model->startTransaction();
			$business->description = $_POST['description'];
			$inserted_id = $this->model->saveBusiness($business);
			if (!is_array($inserted_id)) {
				if (is_null($id)) {
					$business->id = $inserted_id;
				}
				$this->model->commit();
				$this->AddNotice('Business has been saved successfully.');
				$this->LogsManager->registerLog('Business', 'update', 'Business updated with id : ' . $id, $id);
				Utils::RedirectTo($this->getActionUrl('b_show') . "/$id");
			} else {
				$this->AddError('Saving failed!');
				$this->model->rollback();
			}
		}
	}

	public function ajx_saveBusinessInfo($id)
	{
		$business = $this->model->getBusiness($id);
		if (isset($_POST['save'])) {
			($_POST['company_name'] == '') ? die("<h6 class='alert alert-danger'>Company Name is required!</h6>") : $business->company_name;
			$business->company_name = $_POST['company_name'];
			$business->address = $_POST['address'];
			$business->phone = $_POST['phone'];
			$business->mobile = $_POST['phone'];
			$business->website = $_POST['website'];
			$inserted_id = $this->model->saveBusiness($business);
			echo '<h6 class="alert alert-success">' . $this->view->getTerm('modifications_saved_successfully') . '</h6>';
		} else {
			echo '<h6 class="alert alert-danger">' . $this->view->getTerm('something_wrong') . ' :( </h6>';
		}
	}

	public function ajx_updateBusinessImage($id)
	{
		//print_r($_FILES);exit;
		$image_baseurl = (isset($this->ComponentSettings['image_baseurl'])) ? $this->ComponentSettings['image_baseurl'] : '';
		$business = $this->model->getBusiness($id);
		if ($_FILES['photos']['tmp_name']) {
			$_FILES['photos']['name'][0] = time() . '_' . $_FILES['photos']['name'][0];
			if (strlen($_FILES['photos']['tmp_name'][0])) {
				$newfile = time() . '_' . $_FILES['photos']['name'][0];
				$newfile = $this->saveImage($_FILES['photos']['tmp_name'][0], $newfile, 'businesses');
				$old_image = $business->image;
				$business->image = $newfile;
				$inserted_id = $this->model->saveBusiness($business);
			}
			echo(Utils::genThumbnailUrl('businesses/' . $business->image, 750, 500, array(), true, $image_baseurl));
			$this->deleteImage($old_image, 'businesses');
		}
	}

	public function ajx_updateBusinessCoverImage($id)
	{
		$image_baseurl = (isset($this->ComponentSettings['image_baseurl'])) ? $this->ComponentSettings['image_baseurl'] : '';
		$subdomain = $this->model->getSubdomain("target_id = {$id} AND type = 'B'");
		if (isset($_FILES['photos']) && $_FILES['photos']['tmp_name']) {
			$_FILES['photos']['name'] = time() . '_' . $_FILES['photos']['name'];
			if (strlen($_FILES['photos']['tmp_name'])) {
				$newfile = time() . '_' . $_FILES['photos']['name'];
				$newfile = $this->saveImage($_FILES['photos']['tmp_name'], $newfile, 'businesses/covers/');

				$old_image = $subdomain->cover_image;
				$result = $this->model->updateCoverImage($id, $newfile);
				if ($result) {
					echo(Utils::genThumbnailUrl('businesses/covers/' . $newfile, 750, 500, array(), true, $image_baseurl));
					if ($old_image != '') {
						$res = $this->deleteImage($old_image, 'businesses/covers/');
					}
				}
			}
		}
	}

	public function ajx_GalleryImageUpload($id)
	{
		$business = $this->model->getBusiness($id);
		if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['id'] == $business->id_user) {
			$profile = $this->model->getProfile($business->id_profile);
			if (count($business->images) > $profile->max_photo) {
				echo json_encode('error01');
			} else {
				foreach ($_FILES['files']['name'] as $key => $val) {
					$file_name = $_FILES['files']['name'][$key];
					if (!Utils::allowedFileType($file_name)) {
						echo json_encode('error02');
						return;
					}
					$imagefile = time() . '_' . $_FILES['files']['name'][$key];
					$newfile = $this->saveImage($_FILES['files']['tmp_name'][$key], $imagefile, 'businesses' . DS . 'additional');

					$evImage = new BusinessImage_Entity();
					$evImage->id_business = $business->id;
					$evImage->image = $newfile;
					$evImage->size = $_FILES['files']['size'][0];
					$this->model->saveBusinessImage($evImage);
					$business->images = array_push($business->images, $newfile);
					$inserted_id = $this->model->saveBusiness($business);
				}

				echo json_encode($newfile);
			}
		}
	}

	public function ajx_GalleryImageDelete()
	{
		$id = $_POST['id_img'];
		$ids = explode('-', $id);
		$id_bussines = $ids[0];
		$id_image = $ids[1];
		$business = $this->model->getBusiness($id_bussines);
		if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['id'] == $business->id_user) {
			$res = $this->model->getBusinessImage($id_image);
			if ($res) {
				$del = $this->model->deleteBusinessImage($id_image);
				$this->deleteImage($res->image, 'businesses/additional');
				echo '1';
				return;
			}
			echo '0';
			return;
		}
		echo '0';
	}

	public function ajx_UpdateBusinessDesciption($id = '')
	{
		$business = $this->model->getBusiness($id);
		if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['id'] == $business->id_user) {
			if (isset($_POST['save-description'])) {
				$business->description = Utils::xss_clean($_POST['description']);
				$inserted_id = $this->model->saveBusiness($business);
				echo '<h6 id="msg-success" class="alert alert-success">' . $this->view->getTerm('modifications_saved_successfully') . '</h6>';
			} else {
				echo '<h6  id="msg-success" class="alert alert-danger">' . $this->view->getTerm('something_wrong') . '</h6>';
			}
		} else {
			echo '<h6 id="msg-success"  class="alert alert-danger"> Ju nuk keni mjaftueshem te drejta</h6>';
		}
	}

	public function ajx_updateBusinessPayment($id = '')
	{
		$business = $this->model->getBusiness($id);
		$user = UserAuth::getLoginSession();
		if (UserAuth::checkLoginSession() && $user['id'] == $business->id_user) {
			if (isset($_POST['updatePayments'])) {
				$payments_selected = (isset($_POST['pay'])) ? $_POST['pay'] : array();
				$payments_selected = array_values($payments_selected);
				$inserted = $this->model->savePaymentMethod($business->id, $payments_selected);
				echo '<h6 id="msg-success" class="alert alert-success">' . $this->view->getTerm('modifications_saved_successfully') . '</h6>';
			} else {
				echo '<h6  id="msg-error" class="alert alert-danger">' . $this->view->getTerm('something_wrong') . '</h6>';
			}
		} else {
			echo '<h6 id="msg-error"  class="alert alert-danger"> Ju nuk keni mjaftueshem te drejta</h6>';
		}
	}

	public function ajx_updateBusinessOpeningHours($id = '')
	{
		$business = $this->model->getBusiness($id);
		$currOpnhours = $this->model->getOpenHours($id);
		$user = UserAuth::getLoginSession();
		if (UserAuth::checkLoginSession() && $user['id'] == $business->id_user) {
			if (isset($_POST['updateOpeningHours'])) {
				for ($day = 1; $day <= 7; $day++) {
					if ($day == 7) {
						$day = 0;
					}
					$openstart = (isset($_POST['open_start'][$day])) ? $_POST['open_start'][$day] : '';
					$openend = (isset($_POST['open_end'][$day])) ? $_POST['open_end'][$day] : '';
					$pausestart = (isset($_POST['pause_start'][$day])) ? $_POST['pause_start'][$day] : '';
					$pauseend = (isset($_POST['pause_end'][$day])) ? $_POST['pause_end'][$day] : '';

					$openinghours = new BusinessOpenHours_Entity;
					$openinghours->business_id = $business->id;
					$openinghours->day = $day;
					$openinghours->closed = false;
					$openinghours->hours24 = false;
					if ($openstart == $this->view->getTerm('Closed')) {
						$openinghours->closed = true;
					} elseif ($openstart == $this->view->getTerm('24hours')) {
						$openinghours->hours24 = true;
					} else {
						$openinghours->open_start = (isset($openstart) && $openstart != '') ? $openstart : null;
						$openinghours->open_end = (isset($openend) && $openend != '' && $openinghours->open_start != null) ? $openend : (($openinghours->open_start != null) ? '23:59' : '');
						if (($pausestart != '') && ($pauseend != '')) {
							$openinghours->pause_start = $pausestart;
							$openinghours->pause_end = $pauseend;
						}
					}
					if (Utils::in_ObjectArray($currOpnhours, 'day', $day)) {
						$openinghours->id = $currOpnhours[$day]->id;
					}
					$inserted = $this->model->saveOpeningHours($openinghours);
					if ($day == 0) {
						break;
					}
				}

				echo '<h6 id="msg-success" class="alert alert-success">' . $this->view->getTerm('modifications_saved_successfully') . '</h6>';
			} else {
				echo '<h6  id="msg-error" class="alert alert-danger">' . $this->view->getTerm('something_wrong') . '</h6>';
			}
		} else {
			echo '<h6 id="msg-error"  class="alert alert-danger"> Ju nuk keni mjaftueshem te drejta</h6>';
		}
	}

//
	//    public function createMassSubdomains() {
	//        $errors = 0;
	//        $domain = 'netirane.al';
	//        $rows = $this->model->getList(2000);
	//        foreach ($rows as $row) {
	//            $new = new Subdomain_Entity();
	//            $newSub = $this->createSubdomainName($row->company_name, $domain);
	//            $newSub = strtolower($newSub);
	//            $new->subdomain = $newSub;
	//            $row->subdomain = $newSub;
	//            $new->target_id = $row->id;
	//            $new->type = 'B';
	//            $new->target_url = '';
	//            $new->meta_keywords = str_replace(' ', ',', $row->description);
	//            $new->meta_description = $this->limit_text($row->description, 22) . ' ' . $row->category . ' ne Tirane ';
	//            if ($this->model->saveSubdomain($new))
	//                echo "SAVED SUBDOMAIN " . $newSub . "<br><br>";
	//            else
	//                $errors++;
	//            $this->model->saveBusiness($row);
	//            echo $errors;
	//        }
	//    }
//

	public function createSubdomainName($text, $domain)
	{
		$text = trim($text);
		$text = preg_replace('/[^A-Za-z0-9\-]/', '', $text);
		$text = str_replace(' ', '-', $text);
		$newSubdomainName = $text . '.' . $domain;
		$filter = ' subdomain = ' . $newSubdomainName;
		$check = $this->model->getList($limit = 1, '', $filter);
		if (count($check) != 0) {
			$newSubdomainName = str_replace('-', '', $text) . '.' . $domain;
		}
		return $newSubdomainName;
	}

	public function AddBusinessOffer($id_biz)
	{
	}

	public function callAddOffer()
	{
		$id_user = $_SESSION['user_auth']['id'];
		$filter = ' id_user = ' . $id_user;
		$business_usr = $this->model->getList(30, '', $filter);

		if (isset($_POST['save-offer'])) {
			$is_my_biz = false;
			foreach ($business_usr as $biz) {
				if ($_POST['id_biz'] == $biz->id) {
					$is_my_biz = true;
					break;
				}
			}
			if ($is_my_biz) {
				$id_offer = $this->AddOffer($_POST['id_biz'], $_POST);
				if (!is_null($id_offer)) {
					$this->view->set('id_offer', $id_offer);
					$this->view->addNotice('[$offer_inserted]');
				} else {
					$this->addError('[$offer_not_inserted]');
				}
			}
		}

		new WebPage;
		WebPage::$breadcrumb->addDir($this->view->getTerm('add_offer'), $this->getActionUrl('callAddOffer'));

		//**** Render View ****//
		$this->view->set('business_usr', $business_usr);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'insert-offer';
		$this->view->render($view);
		//$this->view->cleanErrors();
	}

	public function edit_offer($id = null)
	{
		if ($id != null) {
			$offer = $this->model->getOffer($id);
			$this->view->set('offer', $offer);
			$offer->id = $id;
			$offer->id_biz = $offer->id_biz;
		} else {
			require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessOffer.php';
			$offer = new BusinessOffer_Entity;
			$id_user = $_SESSION['user_auth']['id'];
			$filter = ' id_user = ' . $id_user;
			$business_usr = $this->model->getList(30, '', $filter);
			$this->view->set('offer', $offer);
			$this->view->set('business_usr', $business_usr);
		}
		$failures = 0;
		$image_baseurl = (isset($this->ComponentSettings['image_baseurl'])) ? $this->ComponentSettings['image_baseurl'] : '';
		$this->view->set('image_baseurl', $image_baseurl);

		if (isset($_POST['save-offer'])) {
			if ($id == null) {
				($_POST['id_biz'] == '') ? $this->AddError('[$id_biz]') : $offer->id_biz = $_POST['id_biz'];
			}
			($_POST['offer_description'] == '') ? $this->AddError('[$description_required]') : $offer->description = $_POST['offer_description'];
			($_POST['offer_title'] == '') ? $this->AddError('[$title_required]') : $offer->title = $_POST['offer_title'];
			($_POST['offer_expiration_date'] == '') ? $offer->expiration_date = null : $offer->expiration_date = $_POST['offer_expiration_date'];
			($_POST['offer_price'] == '') ? $offer->price = null : $offer->price = $_POST['offer_price'];
			($_POST['offer_final_price'] == '') ? $offer->final_price = null : $offer->final_price = $_POST['offer_final_price'];
			($_POST['currency'] == '') ? $this->AddError('[$currency_required]') : $offer->currency = $_POST['currency'];

			$inserted_id = $this->model->saveBusinessOffer($offer);

			if (!is_array($inserted_id)) {
				if (isset($_FILES['images']) && $_FILES['images']['tmp_name']) {
					foreach ($_FILES['images']['name'] as $key => $val) {
						$_FILES['images']['name'][$key] = time() . '_' . $_FILES['images']['name'][$key];
						if (strlen($_FILES['images']['tmp_name'][$key])) {
							$newfile = time() . '_' . $_FILES['images']['name'][$key];
							$newfile = $this->saveImage($_FILES['images']['tmp_name'][$key], $newfile, 'businesses/offers');
							//Utils::genThumbnailUrl("businesses/offers/" . $old_image, 750, 500, array(), true, $image_baseurl))
							$addImage = new BusinessOfferImages_Entity();
							if ($id == null) {
								$addImage->id_offer = $inserted_id;
							} else {
								$addImage->id_offer = $offer->id;
							}
							$addImage->image = $newfile;
							$res = $this->model->saveOfferImage($addImage);
							if (is_array($res)) {
								$failures++;
							}
						}
						if ($failures > 0) {
							$this->AddError('There was ' . $failures . ' failures during image upload!');
						} else {
							$this->view->AddNotice('Image has been uploaded successfully.');
						}
					}
				}
				$this->AddNotice('Me sukses');
			} else {
				$this->addError('[$saving_failed]');
				return null;
			}
		}
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'edit-offer';
		$this->view->render($view);
	}

	public function ajx_OfferImageDelete()
	{
		$id = $_POST['id_img'];
		$ido = explode('-', $id);
		$id_offer = $ido[0];
		$id_image = $ido[1];
		$offer = $this->model->getOffer($id_offer);
		$res = $this->model->getOfferImageById($id_image);
		if ($res) {
			$del = $this->model->deleteOfferImage($id_image);
			$this->deleteImage($res->image, 'businesses/offers/');
			echo '1';
			return;
		}
		echo '0';
	}

	public function delete_offer($id = '')
	{
		$offer = $this->model->getOffer($id);
		$image = $offer->offer_image;
		$result = $this->model->deleteOffer($id);
		if ($result !== false) {
			$this->deleteImage($image, 'businesses/offers');

			$this->LogsManager->registerLog('offer', 'delete', "Offer deleted with id : $id and title : ", $id);
			$this->AddNotice('Offer deleted successfully.');
		} else {
			$this->AddError('Something went wrong!');
		}
		Utils::RedirectTo($this->getActionUrl('ListOffertsUser'));
	}

	public function ListOffertsUser()
	{
		$id_user = $_SESSION['user_auth']['id'];
		$filter = ' id_user = ' . $id_user;
		$order = '';

		$cookie_name = 'id_biz';
		if (isset($_GET['id_biz']) && $_GET['id_biz'] != '') {
			$filter .= ' and id_biz=' . $_GET['id_biz'];
			setcookie($cookie_name, $_GET['id_biz'], time() + (86400 * 30), '/'); // 86400 = 1 day
		}
		if (isset($_GET['order'])) {
			$order .= '' . $_GET['order'];
		}
		if (isset($_COOKIE[$cookie_name])) {
			if (!isset($_GET['id_biz']) && !empty($_COOKIE[$cookie_name])) {
				$filter .= ' and id_biz=' . $_COOKIE[$cookie_name];
				$this->view->set('cookie', $_COOKIE[$cookie_name]);
			} else {
				unset($_COOKIE[$cookie_name]);
				setcookie($cookie_name, null, -1, '/');
			}
		}
		$offer_user = $this->model->getOfferList($filter, 30, 0, $order);
		$businesses = $this->model->getBusinessByUserId($id_user);
		$this->view->set('businesses', $businesses);
		$this->view->set('offer_user', $offer_user);

		WebPage::$breadcrumb->addDir($this->view->getTerm('offers_list'), $this->getActionUrl('ListOffertsUser'));

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'bis-offer-list';
		$this->view->render($view);
	}

	public function ajx_getOffersByUser()
	{
		$id_user = $_SESSION['user_auth']['id'];
		$filter = ' id_user = ' . $id_user;
		$order = '';
		$offer_user = $this->model->getOfferList($filter, 30, 0, $order);
		$offers = json_encode($offer_user);
		echo $offers;
	}

	public function ajx_offer_requestinfo($id)
	{
		if (isset($_POST['request'])) {
			$offer = $this->model->getOffer($id);
			if ($offer != '') {
				$business = $this->model->getBusiness($offer->id_biz);
				$request = array();
				($_POST['name-request'] == '') ? $this->AddError('[$name_required]') : $request['name'] = $_POST['name-request'];
				($_POST['email-request'] == '') ? $this->AddError('[$email_required]') : $request['email'] = $_POST['email-request'];
				($_POST['tel-request'] == '') ? $this->AddError('[$name_required]') : $request['tel'] = $_POST['tel-request'];
				($_POST['message-request'] == '') ? $this->AddError('[$message_required]') : $request['message'] = $_POST['message-request'];
				$request['title'] = $_POST['title'];
				$res = $this->sendRequest($business->id, $business, $request, 'mails/email_user_request_offer');
				if ($res) {
					echo '<div class="alert alert-success">
                            <strong>Kerkesa juaj u dergua me sukses!</strong>.
                          </div>';
				} else {
					echo 0;
				}
			} else {
				$this->view->AddError('Offer doesn\'t exist!');
			}
		}
	}

	public function sendRequest($id_business, $business, $post, $template)
	{
		$message = new BaseView();
		$link = Utils::getComponentUrl('Businesses/messages_list/' . $id_business);
		$message->setViewPath(COMPONENTS_PATH . 'Businesses' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Businesses' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('post', $post);
		$message->set('business', $business);
		$message->set('link', $link);
		$txt = $message->render($template, true);
		$res = $this->model->saveMessage($business->id, $post['name'], $post['email'], $post['tel'], $post['message']);

		if (!$res) {
			Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, CMSSettings::$sender_mail, 'Error Reporting' . CMSSettings::$webdomain, 'An error has occured during the saving the Business Message:<br/><br/>' . $txt);
		}

		$sended = @Email::sendMail(CMSSettings::$EMAIL_ADMIN, $_POST['name'], $_POST['email'], $this->view->getTerm('new_message') . CMSSettings::$webdomain, $txt, array(CMSSettings::$EMAIL_ADMIN));
		if ($sended) {
			$this->view->AddNotice('[$message_sent]');
			return true;
		}
		$this->view->AddError('[$message_failed]');
		return false;
	}

	//****************************PRIVATE FUNCTIONS***************//
	private function BusinessOfferImageUpload($offer, $files)
	{
		$id_biz = $offer->id_biz;
		$business = $this->model->getBusiness($id_biz);
		$failures = 0;
		if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['id'] == $business->id_user) {
			if ($files['images']['tmp_name'][0] == '') {
				$this->view->AddError('Please upload a file...');
			}
			$file_names = $files['images']['name'];
			foreach ($file_names as $file_name) {
				(!Utils::allowedFileType($file_name)) ? $this->view->AddError('Please upload a valid image!') : '';
			}
			if (count($this->view->getErrors()) == 0) {
				for ($i = 0; $i < count($files['images']['name']); $i++) {
					$tmp_name = time() . '_' . $files['images']['name'][$i];
					$imagefile = $this->saveImage($files['images']['tmp_name'][$i], $tmp_name, 'offers');

					$addImage = new BusinessOfferImages_Entity();
					$addImage->id_offer = $offer->id;
					$addImage->image = $imagefile;
					$res = $this->model->saveOfferImage($addImage);
					if (is_array($res)) {
						$failures++;
					}
				}
				if ($failures > 0) {
					$this->AddError('There was ' . $failures . ' failures during image upload!');
				} else {
					$this->view->AddNotice('Image has been uploaded successfully.');
				}
			}
			Utils::RedirectTo($this->getActionUrl('business_edit') . "/$id");
		} else {
			$this->AddError('_not_your_business');
		}
	}

	private function AddOffer($id_biz, $post)
	{
		require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessOffer.php';
		$offer = new BusinessOffer_Entity;
		$offer->id_biz = $id_biz;
		$filter = "cms_BusinessOffer.id = $offer->id";
		$image_baseurl = (isset($this->ComponentSettings['image_baseurl'])) ? $this->ComponentSettings['image_baseurl'] : '';

		if (isset($_FILES['image']) && $_FILES['image']['tmp_name']) {
			$_FILES['image']['name'] = time() . '_' . $_FILES['image']['name'];
			if (strlen($_FILES['image']['tmp_name'])) {
				$newfile = time() . '_' . $_FILES['image']['name'];
				$newfile = $this->saveImage($_FILES['image']['tmp_name'], $newfile, 'businesses/offers');
				//Utils::genThumbnailUrl("businesses/offers/" . $old_image, 750, 500, array(), true, $image_baseurl))

				$old_image = $offer->offer_image;

				if ($old_image != '') {
					$this->deleteImage($old_image, 'businesses/offers');
				}
			}
		} else {
			$newfile = null;
		}

		($post['offer_description'] == '') ? $this->AddError('[$description_required]') : $offer->description = $post['offer_description'];
		($post['offer_title'] == '') ? $this->AddError('[$title_required]') : $offer->title = $post['offer_title'];
		($post['offer_expiration_date'] == '') ? $offer->expiration_date = null : $offer->expiration_date = $post['offer_expiration_date'];
		($post['offer_price'] == '') ? $offer->price = null : $offer->price = $post['offer_price'];
		($post['offer_final_price'] == '') ? $offer->final_price = null : $offer->final_price = $post['offer_final_price'];
		($post['currency'] == '') ? $this->AddError('[$currency_required]') : $offer->currency = $post['currency'];
		$offer->offer_image = $newfile;
		if (count($this->view->getErrors()) == 0) {
			$inserted_id = $this->model->saveBusinessOffer($offer);
			if (!is_array($inserted_id)) {
				return $inserted_id;
			}
			$this->addError('[$saving_failed]');
			return null;
		}
	}

	//********************Bussines Related************//

	private function getBusinessRelated($category_id, $id_biz)
	{
		$filter = " cms_Business.id_category = '" . $category_id . "' AND  cms_Business.id != '" . $id_biz . "' AND cms_Business.enabled = '1'";
		return $this->model->getList($limit = 12, 0, $filter);
	}

	//********************Bussines Related************//

	private function saveImageToDb($file, $filename, $path, $todelete = false)
	{
		$tempImage = new TempImage_Entity();
		if (!is_null($file)) {
			$tempImage->image = file_get_contents($file);
		}
		$tempImage->filename = $filename;
		$tempImage->path = $path;
		$tempImage->delete_action = ($todelete) ? '1' : '0';
		$inserted_temp_image = $this->model->saveTempImage($tempImage);
		return $inserted_temp_image;
	}

	public function syncImagesFromDb()
	{
		$images = $this->model->getTempImages(-1);
		$target = DOCROOT . MEDIA_ROOT;

		foreach ($images as $image) {
			$filename = $target . $image->path . DS . $image->filename;
			$filename = str_replace('//', '/', $filename);

			if ($image->delete_action == '1') {
				$file_to_delete = $target . rtrim($image->path, DS) . DS . $image->filename;
				if (file_exists($file_to_delete) && is_file($file_to_delete) && $image->filename != '') {
					if (unlink($file_to_delete)) {
						$this->model->deleteTempImage($image->id);
					}
				} else {
					$this->model->deleteTempImage($image->id);
				}
			} else {
				if (!is_dir(dirname($filename))) {
					mkdir(dirname($filename), 0755, true) or die('Errore durante la creazione della folder!');
				}
				$result = file_put_contents($filename, $image->image);
				if ($result) {
					$this->model->deleteTempImage($image->id);
				}
			}
		}
	}

	public function register($step = 'info')
	{
		switch ($step) {
			case 'info':
			case 'contacts':
			case 'profile':
			case 'account':
			case 'completed':
				$this->view->set('step', $step);
				$this->view->set('ajax_request_url', $this->getActionUrl('ajx_register'));
				$this->view->set('request_url', $this->getActionUrl('register'));
				break;
			default:
				Utils::RedirectTo($this->getActionUrl('register/info'));
		}
		$this->view->render('registration_ajax');
	}

	public function ajx_register($step)
	{
		$this->view->renderTemplate(false);
		$this->view->setLayout(null);
		$this->view->set('step', $step);
		switch ($step) {
			case 'info':
				return $this->register_step_info();
			case 'contacts':
				return $this->register_step_contacts();
			case 'profile':
				return $this->register_step_profile();
			case 'account':
				return $this->register_step_account();
			case 'completed':
				return $this->register_step_completed();
		}
		$this->view->renderError('404');
	}

	private function register_step_info()
	{
		if (isset($_POST['next_info'])) {
			$business_data = array();

			if ($_POST['company_name'] != '') {
				$business_data['business_name'] = addslashes($_POST['company_name']);
			} else {
				$this->AddError($this->view->getTerm('Business_name_cannot_be_empty'));
			}

			if ($_POST['address'] != '') {
				$business_data['address'] = addslashes($_POST['address']);
			} else {
				$this->AddError($this->view->getTerm('Business_address_cannot_be_empty'));
			}

			if ($_POST['city'] != '') {
				$business_data['city'] = addslashes($_POST['city']);
			} else {
				$this->AddError($this->view->getTerm('Business_city_cannot_be_empty'));
			}

			if ($_POST['coordinates'] != '') {
				$coordinates = str_replace('(', '', $_POST['coordinates']);
				$coordinates = str_replace(')', '', $coordinates);
				$business_data['coordinates'] = addslashes($coordinates);
			} else {
				$this->AddError($this->view->getTerm('Business_city_cannot_be_empty'));
			}

			if (count($this->getErrors()) == 0) {
				Session::set('step_info', $business_data);
				return $this->ajx_register('contacts');
			}
		}
		Session::clear('step_info');
		$this->view->render('reg_step_info');
	}

	private function register_step_contacts()
	{
		if (is_null(Session::get('step_info'))) {
			Utils::RedirectTo($this->getActionUrl('ajx_register/info'));
		}
		if (isset($_POST['next_contacts'])) {
			//Do sommething...
			Session::set('step_contacts', '1');
			return $this->ajx_register('profile');
		}
		Session::clear('step_contacts');
		$this->view->render('reg_step_contacts');
	}

	private function register_step_profile()
	{
		if (is_null(Session::get('step_contacts'))) {
			Utils::RedirectTo($this->getActionUrl('ajx_register/info'));
		}
		if (isset($_POST['next_profile'])) {
			// Do sommething...
			Session::set('step_profile', '1');
			return $this->ajx_register('account');
		}
		Session::clear('step_profile');
		$this->view->render('reg_step_profile');
	}

	private function register_step_account()
	{
		if (is_null(Session::get('step_profile'))) {
			Utils::RedirectTo($this->getActionUrl('ajx_register/info'));
		}
		if (isset($_POST['next_account'])) {
			//Do sommething...
			Session::set('step_account', '1');
			return $this->ajx_register('completed');
		}
		Session::clear('step_account');
		$this->view->render('reg_step_account');
	}

	private function register_step_completed()
	{
		if (is_null(Session::get('step_account'))) {
			Utils::RedirectTo($this->getActionUrl('ajx_register/info'));
		}
		//Do anything...
		Session::clear('step_info');
		Session::clear('step_contacts');
		Session::clear('step_profile');
		Session::clear('step_account');
		$this->view->render('reg_step_completed');
	}
}
