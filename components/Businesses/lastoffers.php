<?php

class LastOffers_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
	}

	public function listoffers()
	{
		if (isset($_POST['send-request'])) {
			if (isset($_POST['request']) && $_POST['request'] == '') {
				$request = array();
				($_POST['name-request'] == '') ? $this->AddError('[$name_required]') : $request['name'] = $_POST['name-request'];
				($_POST['email-request'] == '') ? $this->AddError('[$email_required]') : $request['email'] = $_POST['email-request'];
				($_POST['tel-request'] == '') ? $this->AddError('[$name_required]') : $request['tel'] = $_POST['tel-request'];
				($_POST['message-request'] == '') ? $this->AddError('[$message_required]') : $request['message'] = $_POST['message-request'];
				var_dump($request);
				exit;
				if (count($this->view->getErrors()) == 0) {
					$template = 'mails/email_user_request_offer';
					$this->contactForm($id, $business, $request, $template);
				}
			} else {
				$this->view->AddError('Robot Detected 004100');
			}
		}
	}

	public function contactForm($id_business, $business, $post, $template)
	{
		$message = new BaseView();
		$link = Utils::getComponentUrl('Businesses/messages_list/' . $id_business);
		$message->setViewPath(COMPONENTS_PATH . 'Businesses' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Businesses' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('post', $post);
		$message->set('business', $business);
		$message->set('link', $link);
		$txt = $message->render($template, true);

		$res = $this->model->saveMessage($business->id, $post['name'], $post['email'], $post['tel'], $post['message']);

		if (!$res) {
			Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, CMSSettings::$sender_mail, 'Error Reporting' . CMSSettings::$webdomain, 'An error has occured during the saving the Business Message:<br/><br/>' . $txt);
		}

		$sended = @Email::sendMail($business->email, $_POST['name'], $_POST['email'], $this->view->getTerm('new_message') . CMSSettings::$webdomain, $txt, array(CMSSettings::$EMAIL_ADMIN));
		if ($sended) {
			$this->view->AddNotice('[$message_sent]');
		} else {
			$this->view->AddError('[$message_failed]');
		}
	}
}
