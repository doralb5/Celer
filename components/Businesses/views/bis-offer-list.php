<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/business-offers.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/business-list.css'); ?>
<?php
HeadHTML::AddStylesheet('bootstrap-select.min.css');
HeadHTML::AddJS('bootstrap-select.min.js');
?>
<?php $taglia_offer_content = (isset($parameters['taglia_offer_content'])) ? $parameters['taglia_offer_content'] : 125; ?>
<?php
$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';
?>

<!----- Search result alert ----->
<div class="row">
    <div class="col-md-12">
        <div id="search-result">
            <div class="alert alert-grey">

                <div class="row">
                    <div class="col-md-4" style="line-height: 30px">
                        <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
	?>    

                            <h1 class="list-header-logged">
                                [$offers_list]
                                <span class="result"></span>
                            </h1>

                        <?php
} ?>

                        <?php if (isset($_GET['query']) && $_GET['query'] != '') {
		?>
                            <i class="fa fa-search" aria-hidden="true">&nbsp&nbsp</i>[$result_for]: <span
                                class="result"><?= $_GET['query'] ?></span>
                            <?php
	} ?>

                        <?php if (isset($category)) {
		?>
                            <i class="fa fa-search" aria-hidden="true">&nbsp&nbsp</i>[$result_for]: <span
                                class="result"><?= $category->category ?></span>
                            <?php
	} ?>

                    </div>
                    
                    <div class="col-md-4 col-sm-4" >
                        <?php
							$id_biz = isset($_GET['id_biz']) ? $_GET['id_biz'] : '';
							if (!isset($_GET['id_biz']) && isset($cookie)) {
								$id_biz = $cookie;
							}
						?>
                        <form method="get">
                            <select class="form-control" id="id_biz" name="id_biz" onchange="this.form.submit();" required>
                                <option value="" >all</option>
                                <?php foreach ($businesses as $business) {
							?>
                                    <option value="<?= $business->id ?>" <?= ($id_biz == $business->id) ? 'selected' : ''?> ><?= $business->company_name ?></option>
                                    <?php
						} ?>
                            </select>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <?php
							$order = isset($_GET['order']) ? $_GET['order'] : 'relevance desc';
						?>

                        <form method="get">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="fa fa-sort"></span>
                                </div>
                                <select class="form-control input-sm" id="order-select"
                                        onchange="this.form.submit();" name="order">
                                    <option
                                        value="publish_date DESC" <?= ($order == 'publish_date DESC') ? 'selected' : '' ?>
                                        data-icon="fa fa-sort-amount-desc">&nbsp;[$newer]
                                    </option>
                                    <option
                                        value="publish_date ASC" <?= ($order == 'publish_date ASC') ? 'selected' : '' ?>
                                        data-icon="fa fa-sort-amount-asc">&nbsp;[$older]
                                    </option>
                                    <option
                                        value="title ASC" <?= ($order == 'title ASC') ? 'selected' : '' ?>
                                        data-icon="fa fa-sort-alpha-asc">&nbsp;[$title]
                                    </option>
                                    <option
                                        value="title DESC" <?= ($order == 'title DESC') ? 'selected' : '' ?>
                                        data-icon="fa fa-sort-alpha-desc">&nbsp;[$title]
                                    </option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--****************************************************************************************************************-->


<!--****************************************************************************************************************-->



<?php foreach ($offer_user as $offer) {
							?>
    <div id="carousel-example-generic-<?= $offer->id ?>" class="carousel slide offers-carousel" data-ride="carousel" data-interval="false">

        <div class="carousel-inner">
            <?php
			$first = true;
							if (count($offer->images)) {
								foreach ($offer->images as $img) {
									?>
                    <div class="item <?= ($first) ? 'active' : ''; ?> srle">
                        <img src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 500, 300, array(), true, $image_baseurl) ?>" alt="" class="img-responsive">
                    </div>
            <?php $first = false;
								} ?>
            <?php
							} else {
								?>
                <div class="item <?= ($first) ? 'active' : ''; ?> srle">
                    <img src="<?= Utils::genThumbnailUrl('businesses/offers/default.jpg', 500, 300, array(), true) ?>" alt="" class="img-responsive">
                </div>
    <?php
							} ?>
        </div>
        <?php if (count($offer->images) > 1) {
								?>
            <a class="left carousel-control" href="#carousel-example-generic-<?= $offer->id ?>" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic-<?= $offer->id ?>" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        <?php
							} ?>
        
        <ul class="thumbnails-carousel clearfix"></ul>
        
        <div class="alldiv-text">
            <p class="main-comune"></p>
            <h5 class="main-title1">
                <a title=""><?= $offer->title ?></a>
            </h5>
            <p></p>
            <div class="location-height">
                <p class="main-addres">
                <?= StringUtils::CutString(strip_tags($offer->description), $taglia_offer_content) ?>
                </p>
            </div>
            <div class="offer-price">
                <?php if ($offer->final_price != '') {
								?>
                    <div class="price"><?= Utils::price_format($offer->price, $offer->currency) ?></div>
                    <div class="final-price"><?= Utils::price_format($offer->final_price, $offer->currency) ?></div>
                <?php
							} else {
								?>
                    <span class="only-price"><div class="price"><?= Utils::price_format($offer->price, $offer->currency) ?></div></span>                                                   
                <?php
							} ?>
            </div>
            <span style=" float: right; ">
                <a class="btn btn-xs btn-default" href="<?= Utils::getComponentUrl('Businesses/edit_offer/' . $offer->id) ?>" >edit</a>
                <a class="btn btn-xs btn-default" href="<?= Utils::getComponentUrl('Businesses/delete_offer/' . $offer->id) ?>" >delete</a>
            </span>                                      
        </div>
    </div>
    
    <script>
        function ShowModal(id)
        {
            var modal = document.getElementById(id);
            modal.style.display = "block";
        }
    </script>    

    <script>
        // thumbnails.carousel.js jQuery plugin
        ;
        (function (window, $, undefined) {

            var conf = {
                center: true,
                backgroundControl: false
            };

            var cache = {
                $carouselContainer: $('.thumbnails-carousel').parent(),
                $thumbnailsLi: $('.thumbnails-carousel li'),
                $controls: $('.thumbnails-carousel').parent().find('.carousel-control')
            };

            function init() {
                cache.$carouselContainer.find('ol.carousel-indicators').addClass('indicators-fix');
                cache.$thumbnailsLi.first().addClass('active-thumbnail');

                if (!conf.backgroundControl) {
                    cache.$carouselContainer.find('.carousel-control').addClass('controls-background-reset');
                } else {
                    cache.$controls.height(cache.$carouselContainer.find('.carousel-inner').height());
                }

                if (conf.center) {
                    cache.$thumbnailsLi.wrapAll("<div class='center clearfix'></div>");
                }
            }

            function refreshOpacities(domEl) {
                cache.$thumbnailsLi.removeClass('active-thumbnail');
                cache.$thumbnailsLi.eq($(domEl).index()).addClass('active-thumbnail');
            }

            function bindUiActions() {
                cache.$carouselContainer.on('slide.bs.carousel', function (e) {
                    refreshOpacities(e.relatedTarget);
                });

                cache.$thumbnailsLi.click(function () {
                    cache.$carouselContainer.carousel($(this).index());
                });
            }

            $.fn.thumbnailsCarousel = function (options) {
                conf = $.extend(conf, options);

                init();
                bindUiActions();

                return this;
            }

        })(window, jQuery);

        $('.thumbnails-carousel').thumbnailsCarousel();

    </script> 


<?php
						} ?>
<script>
    $('#id_biz').selectpicker({
        style: 'btn-default btn-sm'
    });
</script>
<script>
    $('#order-select').selectpicker({
        style: 'btn-default btn-sm'
    });
</script>
