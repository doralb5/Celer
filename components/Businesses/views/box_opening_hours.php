<?php
// $opnhours Current Hours of this business

HeadHTML::AddJS('jquery.timepicker.min.js');
HeadHTML::AddStylesheet('jquery.timepicker.css');
?>


<style>
    .jobdays {

    }
    .jobdays .inputhour {
        width: 45%;
        float: left;
        margin: 2px;
        border: 0px solid #ccc;
        border-radius: 2px;
    }
</style>


<h4 class="page-header">[$opening_hours]</h4>


<form id="openinghours">
    <input type="hidden" name="updateOpeningHours" value="1" />
    <div class="col-md-12 jobdays">
        <div class="row">
            <div class="col-md-2">
                <h4>Days</h4>
            </div>
            <div class="col-md-5">
                <h4>Opens</h4>
            </div>
            <div class="col-md-5">
                <h4>Pauses</h4>
            </div>
            <br>
        </div>
        <?php
		for ($i = 1; $i <= 7; $i++) {
			if ($i == 7) {
				$i = 0;
			}
			if (isset($opnhours[$i])) {
				$open_start = ($opnhours[$i]->hours24 == '1') ? '24hours' : (($opnhours[$i]->closed) ? '[$Closed]' : $opnhours[$i]->open_start);
				$open_end = (isset($opnhours[$i]->open_end)) ? $opnhours[$i]->open_end : '';
				$pause_start = (isset($opnhours[$i]->pause_start)) ? $opnhours[$i]->pause_start : '';
				$pause_end = (isset($opnhours[$i]->pause_end)) ? $opnhours[$i]->pause_end : '';
			} else {
				$open_start = '[$Closed]';
				$open_end = '';
				$pause_start = '';
				$pause_end = '';
			} ?>
            <div class="row" id='day_<?= $i ?>'>
                <div class="col-md-2"><?= Utils::numbertoweekdate($i, FCRequest::getLang()) ?></div>
                <div class="col-md-5">
                    <input class="form-control inputhour openstart" type="text" name="open_start[<?= $i ?>]" value="<?= $open_start ?>" style="">
                    <input class="form-control inputhour openend" type="text" name="open_end[<?= $i ?>]" value="<?= $open_end ?>" style="">
                </div>
                <div class="col-md-5">
                    <input class="form-control inputhour pausestart" type="text" name="pause_start[<?= $i ?>]" value="<?= $pause_start ?>"  style="">
                    <input class="form-control inputhour pauseend" type="text" name="pause_end[<?= $i ?>]" value="<?= $pause_end ?>" style="">
                </div>
            </div>
            <?php
			if ($i == 0) {
				break;
			}
		}
		?>
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <input class="btn btn-sm btn-default save-business-button" type="submit" value='[$SaveHours]' />
            </div>
        </div>
    </div>


</form>
<div id='OpenResponse'></div>
<script>
    $(document).ready(function () {
        $('#openinghours').submit(function () {

            // show that something is loading
            $('#OpenResponse').html("<b>Loading response...</b>");

            /*
             * 'post_receiver.php' - where you will pass the form data
             * $(this).serialize() - to easily read form data
             * function(data){... - data contains the response from post_receiver.php
             */
            $.ajax({
                type: 'POST',
                url: '<?= Utils::getComponentUrl('Businesses/ajx_updateBusinessOpeningHours/') . $business->id ?>',
                data: $(this).serialize()
            })
                    .done(function (data) {
                        // show the response
                        $('#OpenResponse').html(data);
                    })
                    .fail(function () {
                        // just in case posting your form failed
                        alert("Posting failed.");
                    });

            // to prevent refreshing the whole page page
            return false;

        });
    });
</script>


<script>


    $(document).ready(function () {


        function updateOpenStart() {
            var openstart = $('.inputhour.openstart');
            openstart.timepicker({
                'timeFormat': 'H:i',
                'step': 15,
                'scrollDefault': '08:00',
                'noneOption': [{'label': '[$Closed]', 'className': 'closed', 'value': '[$Closed]'}, {'label': '[$24hours]', 'className': '24h', 'value': '[$24hours]'}]
            });
            openstart.each(function () {
                if ($(this).val() === '') {
                    $(this).val("[$Closed]");
                }
            });
            updateOpenEnd();
        }

        function updateOpenEnd() {
            var openend = $('.inputhour.openend');
            openend.each(function () {
                var dayrow_id = $(this).parent().parent().attr('id');
                var parentelem = $('#' + dayrow_id + ' .inputhour.openstart');
                var re = /([01]?[0-9]|2[0-3]):[0-5][0-9]/;
                if ($(this).val() === '' && parentelem.val() === '' || !parentelem.val().match(re)) {
                    $(this).attr("disabled", true);
                } else {
                    $(this).attr("disabled", false);
                    $(this).timepicker({
                        'timeFormat': 'H:i',
                        'step': 15,
                        'minTime': parentelem.val(),
                        'maxTime': '23:59',
                        'scrollDefault': parentelem.val()
                    });
                }
            });
            updatePauseStart();
        }

        function updatePauseStart() {
            var pausestart = $('.inputhour.pausestart');
            pausestart.each(function () {
                var dayrow_id = $(this).parent().parent().attr('id');
                var openstart = $('#' + dayrow_id + ' .inputhour.openstart');
                var openend = $('#' + dayrow_id + ' .inputhour.openend');
                if ($(this).val() === '' && openend.val() === '') {
                    $(this).attr("disabled", true);
                } else {
                    $(this).attr("disabled", false);
                    $(this).timepicker({
                        'timeFormat': 'H:i',
                        'step': 15,
                        'minTime': openstart.val(),
                        'maxTime': openend.val(),
                        'scrollDefault': openstart.val()
                    });
                }
            });
            updatePauseEnd();
        }

        function updatePauseEnd() {
            var pauseend = $('.inputhour.pauseend');
            pauseend.each(function () {
                var dayrow_id = $(this).parent().parent().attr('id');
                var pausestart = $('#' + dayrow_id + ' .inputhour.pausestart');
                var openend = $('#' + dayrow_id + ' .inputhour.openend');
                if ($(this).val() === '' && pausestart.val() === '') {
                    $(this).attr("disabled", true);
                } else {
                    $(this).attr("disabled", false);
                    $(this).timepicker({
                        'timeFormat': 'H:i',
                        'step': 15,
                        'minTime': pausestart.val(),
                        'maxTime': openend.val(),
                        'scrollDefault': pausestart.val()
                    });
                }
            });
        }
        updateOpenStart();



        $('.inputhour.openstart').on('change', function () {
            var dayrow_id = $(this).parent().parent().attr('id');
            var openend = $('#' + dayrow_id + ' .inputhour.openend');
            if (openend.val() < $(this).val() || $(this).val() === '') {
                openend.val("");
            }
            openend.change();
            updateOpenEnd();
        });
        $('.inputhour.openend').on('change', function () {
            var dayrow_id = $(this).parent().parent().attr('id');
            var openstart = $('#' + dayrow_id + ' .inputhour.openstart');
            var pausestart = $('#' + dayrow_id + ' .inputhour.pausestart');
            if (pausestart.val() < openstart.val() || pausestart.val() > $(this).val() || $(this).val() === '') {
                pausestart.val("");
            }
            pausestart.change();
            updatePauseStart();
        });
        $('.inputhour.pausestart').on('change', function () {
            var dayrow_id = $(this).parent().parent().attr('id');
            var openend = $('#' + dayrow_id + ' .inputhour.openend');
            var pauseend = $('#' + dayrow_id + ' .inputhour.pauseend');
            if (pauseend.val() < $(this).val() || pauseend.val() > openend.val() || $(this).val() === '') {
                pauseend.val("");
            }
            updatePauseEnd();
        });


        $('.inputhour.openstart').on('blur', function () {
            updateOpenEnd();
        });
        $('.inputhour.openend').on('blur', function () {
            updatePauseStart();
        });
        $('.inputhour.pausestart').on('blur', function () {
            updatePauseEnd();
        });

    });
</script>