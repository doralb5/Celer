<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/business-categories.css'); ?>

    <?php
$w = (isset($width)) ? $width : 245;
$h = (isset($height)) ? $height : 163;
?>

    <div class="row businesses">
        <?php
		$i = 0;
		if (count($categories)) {
			foreach ($categories as $categ) {
				if ($i % 4 == 0 && $i != 0) {
					?>
                    <div class="clearfix"></div>
                <?php
				} ?>

                <div class="col-sm-6 col-md-3 business-category">
                    <div class="thumbnail">
                        <a href="<?= Utils::getComponentUrl('Businesses/business_list/' . Utils::url_slug($categ['name'] . '-' . $categ['id'])) ?>"><?php if ($categ['image'] != '') {
					?><img src="<?=  Utils::genThumbnailUrl('businesses/categories/' . $categ['image'], $w, $h)?>" alt="" style="width: 100%; border-radius: 2px;" /> <?php
				} else {
					?><p class="text-center" style="height: 50px">No Image</p><?php
				} ?></a>
                        <div class="caption">
                            <a href="<?= Utils::getComponentUrl('Businesses/business_list/' . Utils::url_slug($categ['name'] . '-' . $categ['id'])) ?>"><h4 class="text-center"><?= $categ['name'] ?></h4></a>
                        </div>                                    
                    </div>
                </div>

                <?php
				$i++;
			}
		}
		?>
    </div>