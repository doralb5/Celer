<?php
$comnum = 0;
foreach ($comments as $comment) {
	if ($comment['comment'] != '') {
		$comnum++;
	}
}

$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';
?>
<?php HeadHTML::addMeta('keywords', $business->company_name . ' , ' . $business->category . ' , ' . $business->address . '  , ' . CMSSettings::$website_title . ' , njoftime , biznese , ' . $business->category . ' ' . CMSSettings::$website_title); ?>
<?php HeadHTML::addMeta('description', StringUtils::CutString(strip_tags($business->description), 200)); ?>
<?php HeadHTML::addMetaProperty('og:title', $business->company_name . ' | ' . CMSSettings::$website_title); ?>
<?php HeadHTML::addMetaProperty('og:type', 'article'); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('businesses/' . urlencode($business->image), 1000, 600, array('far' => '1', 'bg' => 'ffffff'), true, $image_baseurl)); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($business->description), 200)); ?>

<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/business-details-map.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/insert_business-remove.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/jquery-filer.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/jquery.filer.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/jquery.filer-dragdropbox-theme.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/mpg.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/business-offers.css'); ?>
<?php HeadHtml::linkStylesheet('owl.carousel.css'); ?>

<script src='https://www.google.com/recaptcha/api.js'></script>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/business-details-map.js'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.filer.js'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/mpg.js'); ?>
<?php HeadHtml::linkJS('owl.carousel.js'); ?>
<?php // HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/custom.js');?>


<?php require_once LIBS_PATH . 'StringUtils.php'; ?>

<?php $lat = (isset($settings['map_lat'])) ? $settings['map_lat'] : '41.3280456' ?>
<?php $lng = (isset($settings['map_lng'])) ? $settings['map_lng'] : '19.8119486' ?>

<?php
$show_ShareButton = (isset($parameters['show_Share']) && $parameters['show_Share'] == 'no') ? false : true;
$w = (isset($parameters['w'])) ? $parameters['w'] : 750;
$h = (isset($parameters['h'])) ? $parameters['h'] : 500;
$w_gallery = (isset($parameters['w'])) ? $parameters['w'] : 1000;
$h_gallery = (isset($parameters['h'])) ? $parameters['h'] : 0;

$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$tel = isset($_POST['tel']) ? $_POST['tel'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';
$map = ($business->coordinates != '') ? true : false;

$id_profile = $business->id_profile;
?>

<?php
$company_name = (isset($_POST['company_name'])) ? $_POST['company_name'] : '';
$description = (isset($_POST['description'])) ? $_POST['description'] : '';

$email = (isset($_POST['email'])) ? $_POST['email'] : '';
$tel = (isset($_POST['phone'])) ? $_POST['phone'] : '';
$website = (isset($_POST['website'])) ? $_POST['website'] : '';
$facebook = (isset($_POST['facebook'])) ? $_POST['facebook'] : '';
$linkedin = (isset($_POST['linkedin'])) ? $_POST['linkedin'] : '';
$instagram = (isset($_POST['instagram'])) ? $_POST['instagram'] : '';
$google = (isset($_POST['google'])) ? $_POST['google'] : '';
$twitter = (isset($_POST['twitter'])) ? $_POST['twitter'] : '';
$id_category = (isset($_POST['id_category'])) ? $_POST['id_category'] : '';
$category = (isset($_POST['category'])) ? $_POST['category'] : '';

$address = (isset($_POST['address'])) ? $_POST['address'] : '';
$coordinates = (isset($_POST['coordinates'])) ? $_POST['coordinates'] : '';

$add_ids = array();
if (isset($_POST['additional_categ'])) {
	if (is_array($_POST['additional_categ'])) {
		foreach ($_POST['additional_categ'] as $add_categ) {
			array_push($add_ids, $add_categ);
		}
	} else {
		array_push($add_ids, $_POST['additional_categ']);
	}
}
?>




<script type="text/javascript">
    'function' != typeof loadGsLib && (loadGsLib = function () {
        var e = document.createElement("script");
        e.type = "text/javascript", e.async = !0, e.src = '//api.at.getsocial.io/widget/v1/gs_async.js?id=080ddc';
        var t = document.getElementsByTagName("script")[0];
        t.parentNode.insertBefore(e, t)
    })();</script>


<?php if ($business->coordinates != '' || $business->coordinates != null) {
	?>
    <style>
        .breadcrumb {
            margin-top: -220px;
        }
    </style>
<?php
} ?>
<style>

    #map {
        height: 500px;
    }
    #map-controls {
        display: none;
    }
    /* Admin Menu Affix */
    .affix {
        top: 140px;
        z-index: 97;
        width: 262px;
    }

    div[contenteditable=true] {
        background: rgba(0,0,0,.5);
        border: 1px dotted rgb(123, 123, 123);

    }
    .breadcrumb {
        background-color: rgba(0, 0, 0, 0.8);
        margin-left: -15px;
        margin-right: -15px;
        border-bottom: 1px solid #424242;
    }
    .breadcrumb li:last-child a {
        background: transparent;
    }
    breadcrumb li:last-child a:hover {
        background: transparent;
    }
    .breadcrumb li a {
        color: #ffffff;
    }
    .breadcrumb li a:hover {
        background-image: linear-gradient(to left, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0));
        color: #ffffff !important;
        background: transparent;
    }
    .breadcrumb li:nth-child(2) a, .breadcrumb li:nth-child(3) a, .breadcrumb li:nth-child(4) a, .breadcrumb li:nth-child(5) a {
        background-color: rgba(255, 255, 255, 0);
        background-image: linear-gradient(to left, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0));
    }
    .breadcrumb li a:after {
        border-left: 15px solid rgb(48, 48, 48);
    }
    .breadcrumb li a:before {
        border-left: 15px solid #414141;
    }
    .breadcrumb li a:hover:after {
        border-left-color: #303030;
    }
    .breadcrumb li:nth-child(2) a:after, .breadcrumb li:nth-child(3) a:after, .breadcrumb li:nth-child(4) a:after,.breadcrumb li:nth-child(5) a:after {
        border-left-color: #303030;
    }
    .breadcrumb li:last-child a {
        color: #ffffff;
    }

    @media (max-width: 500px) {
        .breadcrumb {
            display:none;
        }
    }
</style>


<div class="business_std_container first-container" <?php if ($map) {
		?> style="margin-top: -20px;" <?php
	} ?> >
    <div class="content">
        <div class="row">
            <div class="col-md-12 business-body alert-grey bis-head">

                <!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->  
                <!--***** Business HEAD *****-->
                <div class="row content-body">

                    <div class="col-md-9">

                        <div class="row">



                            <!--***** Business Main Image *****-->
                            <div class="col-md-4 col-img">
                                <div class="business-image">

                                    <div class="profile large">
                                        <div class="user-info">
                                            <div class="profile-pic">
                                                <?php if ($business->image != '') {
		?>
                                                    <img id="business-picture" class="image" src="<?= Utils::genThumbnailUrl('businesses/' . $business->image, 800, 500, array(), true, $image_baseurl) ?>" />
                                                <?php
	} else {
		?>
                                                    <img id="business-picture" class="image" src="<?= Utils::genThumbnailUrl('businesses/default.jpg', 0, 0, array(), true) ?>" />
                                                <?php
	} ?>
                                                <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '' && $_SESSION['user_auth']['id'] === $business->id_user) {
		?>
                                                    <!--***** Update only in admin mode *****-->
                                                    <div id="the-layer" class="layer">
                                                        <div class="loader">&nbsp;</div>
                                                    </div>

                                                    <!--                                                        <a class="image-wrapper" href="#"> </a>-->

                                                    <form id="file-form" enctype='multipart/form-data' action="<?= Utils::getComponentUrl('Businesses/ajx_updateBusinessImage/') . $business->id ?>" method="POST">
                                                        <!--                                                            <a class="image-wrapper" href="#">-->
                                                        <input type="file" class="hidden-input image-wrapper" id="file-select" name="photos[]" multiple/>
                                                        <label id="profile-pic-label" class="edit fa fa-camera" for="file-select" title="Change picture" type="file"></label> 
                                                        <!--                                                            </a>-->
                                                    </form>
                                                <?php
	} ?>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <script>


                                var form = document.getElementById('file-form');
                                var fileSelect = document.getElementById('file-select');
                                var roll = document.getElementById("the-layer");
                                var layer = document.getElementsByClassName("layer");
                                function changeImage(a) {
                                    document.getElementById("business-picture").src = a;
                                }
                                ;
                                function  startLoader(e) {
                                    e.classList.add("visible");
                                }
                                ;
                                function destroyLoader(e) {
                                    e.classList.remove("visible");
                                }
                                ;
                                fileSelect.onchange = function (event) {
                                    event.preventDefault();
                                    var files = fileSelect.files;
                                    var formData = new FormData();
                                    for (var i = 0; i < files.length; i++) {
                                        var file = files[i];
                                        if (!file.type.match('image.*')) {
                                            continue;
                                        }
                                        formData.append('photos[]', file, file.name);
                                        startLoader(roll);
                                    }
                                    var xhr = new XMLHttpRequest();
                                    xhr.open('POST', '<?= Utils::getComponentUrl('Businesses/ajx_updateBusinessImage/') . $business->id ?>', true);
                                    xhr.onreadystatechange = function () {
                                        if (xhr.readyState == XMLHttpRequest.DONE) {
                                            changeImage(xhr.responseText);

                                        }
                                    }
                                    xhr.onload = function () {
                                        if (xhr.status === 200) {
                                            destroyLoader(roll);
                                        } else {
                                            alert('An error occurred!');
                                        }
                                    };
                                    // Send the Data.  
                                    xhr.send(formData);
                                };
                            </script>



                            <!--***** Business Main Information *****-->
                            <div class="col-md-8 col-info">

                                <!--Name & Category-->
                                <div class="row">
                                    <div class="col-md-12 col-title">
                                        <div class="business-title">
                                            <div>
                                                <h3 class="business-name" ><?= $business->company_name ?></h3>
                                            </div>


                                            <div class="business-address">
                                                <!--<span class="main-addres">
                                                <?php if ($business->address != '') {
		?> 
                                                                                                                                                                                                                            <i class="fa fa-map-marker" aria-hidden="true"></i> 
                                                    <?= $business->address ?>
                                                <?php
	} ?>
                                                </span>-->
                                                <span class="main-comune">
                                                    <?php
													for ($j = 0; $j < count($business->id_category); $j++) {
														if ($j > 0) {
															break;
														} ?>
                                                        <a href="<?= Utils::getComponentUrl('Businesses/b_list') . '/' . Utils::url_slug($business->category . '-' . $business->id_category) ?>">
                                                            <?php
															if (!is_null($business->category) && $business->category != '') {
																echo $business->category;
															} elseif (isset($business->categories) && count($business->categories)) {
																echo $business->categories[0];
															} ?>
                                                        </a>


                                                    <?php
													} ?>
                                                </span>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="business-contact-info">
                                            <div class="business-phone">
                                                <table style="">
                                                    <tr>
                                                        <?php if ($business->location != '') {
														?>
                                                        <span class="business-url">
                                                            <td> <?= $business->address ?> </td>
                                                        </span>
                                                    <?php
													} ?>
                                                    </tr>
                                                    <tr>
                                                        <?php if ($business->phone != '' || $business->mobile != '') {
														?>
                                                        <span class="business-url">
                                                            <td > <?= $business->phone ?><?= ($business->mobile != '' && $business->phone != '') ? ' &nbsp;&nbsp;-&nbsp;&nbsp; ' : ''; ?> <?= $business->mobile ?></td>
                                                        </span>
                                                    <?php
													} ?>
                                                    </tr>

                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Social Networks-->
                                <div class="row">
                                    <div class="col-md-12 col-social">
                                        <div class="business-social">   

                                            <style>
                                                .modal{
                                                    color:black;   
                                                }

                                                .modal-header {
                                                    padding-bottom: 5px;
                                                }

                                                .modal-footer {
                                                    padding: 0;
                                                }

                                                .modal-footer .btn-group button {
                                                    height:40px;
                                                    border-top-left-radius : 0;
                                                    border-top-right-radius : 0;
                                                    border: none;
                                                    border-right: 1px solid #ddd;
                                                }

                                                .modal-footer .btn-group:last-child > button {
                                                    border-right: 0;
                                                }
                                            </style>

                                            <div class="">
                                                <button data-toggle="modal" data-target="#EditData" class="btn btn-sm btn-default center-block">[$edit_business_data]</button>
                                            </div>

                                            <div class="business_std_container row-nav" data-height="" id="row2" style="margin-top: 0px;" >

                                                <div class="modal fade" id="EditData" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">[$Close]</span></button>
                                                                <h3 class="modal-title" id="lineModalLabel">[$edit_business_data]</h3>
                                                            </div>
                                                            <div class="modal-body">

                                                                <!-- content goes here -->
                                                                <form id="business-info-form" method="POST">
                                                                    <div class="col-md-12">
                                                                        <?php Messages::showErrors(); ?>
                                                                        <?php Messages::showInfoMsg(); ?>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div col-md-12>
                                                                            <span class="file-input"><div class="file-preview ">
                                                                                    <div class="file-preview-thumbnails">
                                                                                        <div class="file-preview-frame">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="clearfix"></div>
                                                                                    <div class="file-preview-status text-center text-success"></div>
                                                                                    <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                                                                </div>
                                                                            </span>

                                                                            <div class="upload-btn">
                                                                                <input type="file" name="image">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <h5 class="titlecatege">[$name]* </h5>

                                                                            <div class="form-group formgroup">
                                                                                <input type="text" required="" class="form-control" id="business-name" name="company_name"
                                                                                       value="<?= $business->company_name ?>" placeholder="<?= $business->company_name ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <h5 class="titlecatege">[$phone]*</h5>

                                                                            <div class="input-group form-group">
                                                                                <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                                                                <input type="text" required="" class="form-control" id="" name="phone" value="<?= $business->phone ?>"placeholder="+(000)000000000">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">

                                                                        <div class="col-md-3 ">
                                                                            <h5 class="titlecatege">[$city]</h5>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                                                                <input type="text" class="form-control" value="<?= $business->location ?>" name="city" placeholder="[$city]" disabled="">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-9">
                                                                            <h5 class="titlecatege">[$address]* </h5>
                                                                            <div class=" formgroup">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon gps"><a onclick="getGPSLocation()"><i class="fa fa-compass" aria-hidden="true"></i></a></div>
                                                                                    <input type="text" class="form-control" id="address" name="address" value="<?= $business->address ?>"
                                                                                           placeholder="[$address_placeholder]" onblur="setMarker();" value="">
                                                                                    <input type="hidden" id="coordinates" name="coordinates" value="<?= $business->coordinates ?>">
                                                                                </div>
                                                                            </div>
                                                                            <label class="well-label">[$drag_marker]</label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12 col-sm-12 col-xs-12 marg-bottm">
                                                                            <h5 class="titlecatege">[$map]</h5>
                                                                            <div id="map-edit" style="width: 100%; height: 300px;"></div>
                                                                        </div>
                                                                    </div>
                                                                    <br>

                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <h5 class="titlecatege">[$website]</h5>

                                                                            <div class="input-group form-group">
                                                                                <div class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                                                                                <input type="text" class="form-control" id="" name="website" value="<?= $business->website ?>" placeholder="www.website.com">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <h5 class="titlecatege">[$facebook]</h5>

                                                                            <div class="input-group form-group">
                                                                                <div class="input-group-addon"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                                                                                <input type="text" class="form-control" id="" name="facebook" value="<?= $business->facebook ?>" placeholder="www.facebook.com/username">
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <h5 class="titlecatege">[$linkedin]</h5>

                                                                            <div class="input-group form-group">
                                                                                <div class="input-group-addon"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
                                                                                <input type="text" class="form-control" id="" name="linkedin" value="<?= $business->linkedin ?>" placeholder="www.linkedin.com/username">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <h5 class="titlecatege">[$instagram]</h5>

                                                                            <div class="input-group form-group">
                                                                                <div class="input-group-addon"><i class="fa fa-instagram" aria-hidden="true"></i></div>
                                                                                <input type="text" class="form-control" id="" name="instagram" value="<?= $business->instagram ?>" placeholder="www.instagram.com/username">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <h5 class="titlecatege">[$google]</h5>

                                                                            <div class="input-group form-group">
                                                                                <div class="input-group-addon"><i class="fa fa-google-plus" aria-hidden="true"></i></div>
                                                                                <input type="text" class="form-control" id="" name="google" value="<?= $business->google ?>" placeholder="https://plus.google.com/+username">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <h5 class="titlecatege">[$twitter]</h5>

                                                                            <div class="input-group form-group">
                                                                                <div class="input-group-addon"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                                                                                <input type="text" class="form-control" id="" name="twitter" value="<?= $business->twitter ?>" placeholder="www.twitter.com/username">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <input type="hidden" value="<?= md5('hello') ?>" name="save" >

                                                                </form>
                                                                <div class="msg-display col-md-12">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                                                    <div class="btn-group" role="group">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">[$Close]</button>
                                                                    </div>
                                                                    <div class="btn-group btn-delete hidden" role="group">
                                                                        <button type="button" id="deldata" class="btn btn-default btn-hover-red" data-dismiss="modal"  role="button">[$Delete]</button>
                                                                    </div>
                                                                    <div class="btn-group" role="group">
                                                                        <button type="button" id="savedata" class="btn btn-default btn-hover-green" data-action="save" name="save" role="button">[$Update]</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <script>
                                                    $(document).ready(function () {

                                                        $("#EditData").on("shown.bs.modal", function () {
                                                            google.maps.event.trigger(map, "resize");
                                                        });
                                                        $('#savedata').click(function () {
                                                            var form = $('#business-info-form');
                                                            $.ajax({
                                                                type: "POST",
                                                                url: '<?= Utils::getComponentUrl('Businesses/ajx_saveBusinessInfo/' . $business->id); ?>',
                                                                data: form.serialize(),
                                                                success: function (response) {
                                                                    console.log(response);
                                                                    $('.msg-display').prepend(response);
                                                                    //$('#squarespaceModal').modal('hide');

                                                                },
                                                                error: function (e) {
                                                                    console.log(e);
                                                                    $('.msg-display').prepend('<h6 class="alert alert-danger">' + e + '</h6>');
                                                                }

                                                            });
                                                            //modal_1 is the id 1
                                                        });
                                                    });
                                                </script>



                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div> 
                    </div>
                    <?php if (in_array($id_profile, array(1, 2, 3, 4, 5))) {
														?>

                        <!--***** Business Rating *****-->
                        <div class="col-md-3 show-rating">
                            <div id="showrate"></div>

                            <div class="rate-it rate-cus">
                                <i class="fa fa-star-o star-on-png" id="star_1_<?= $business->id ?>"></i>

                                <i data-alt="2" class="fa fa-star-o star-on-png"
                                   id="star_2_<?= $business->id ?>"></i>

                                <i data-alt="3" class="fa fa-star-o star-on-png"
                                   id="star_3_<?= $business->id ?>"></i>

                                <i data-alt="4" class="fa fa-star-o star-on-png"
                                   id="star_4_<?= $business->id ?>"></i>

                                <i data-alt="5" class="fa fa-star-o star-on-png"
                                   id="star_5_<?= $business->id ?>"></i>
                            </div>

                            <li style="list-style:none; cursor: pointer;">
                                <a class="btn btn-sm btn-default bussiness-comments"href="<?= Utils::getComponentUrl('Businesses/myBizReviews') . '/' . $business->id ?>">[$Show_reviews]</a>
                            </li>


                        </div>
                    <?php
													} ?>

                </div>




                <?php
				$num = 2;
				if ($business->website != '') {
					$num++;
				} if ($business->subdomain != '') {
					$num++;
				}
				$col = 12 / $num;
				?>        

            </div><!--***** Close the Content *****-->
        </div><!--***** Row *****-->
    </div><!--***** Content *****-->
</div><!--***** business_std_container *****-->



<!--***** Page Scroll Menu tab *****-->

<div id="nav-wrap" class="row">
    <div class="">
        <div id="scroller-anchor"></div>
        <nav class="main-nav">
            <ul>

                <?php if ($business->description != '') {
					?>
                    <li class="nav-btn active" data-row-id="row1">
                        <a href=""><i class="fa fa-home fa-icon" aria-hidden="true"></i><span class="fa-text">[$Description]</span></a>
                    </li>
                <?php
				} ?>

                <?php if (count($business->images) > 0) {
					?>
                    <li class="nav-btn" data-row-id="row2">
                        <a href=""><i class="fa fa-camera fa-icon" aria-hidden="true"></i><span class="fa-text">[$Fotogallery]</span></a>
                    </li>
                <?php
				} ?>

            </ul><!--/.nav-collapse -->
<!--            <i class="active-marker"></i>-->
        </nav>
    </div>
</div>


<div id="content-wrap">


    <?php if ($business->description != '') {
					?>

        <div class="business_std_container row-nav" data-height="" id="row1" style="margin-top: 0px;" >
            <div class="content">
                <div class="row">
                    <div class="col-md-12 business-body alert-grey">

                        <!--***** Business Content BODY *****-->
                        <div class="row">
                            <div class="col-md-12">



                                <h4 class="page-header">[$Description]</h4>
                                <div class="  business-description no-margin-top">

                                    <div id="business-description" class="description-ajx" contenteditable="false">
                                        <?php
										if ($business->description != strip_tags($business->description)) {
											echo $business->description;
										} else {
											echo nl2br($business->description);
										} ?>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-sm btn-default" id="EditButton">
                                            [$Edit_description]
                                        </button>
                                        <button id="save-description" class="btn btn-sm btn-default save-business-button ">
                                            [$Save_description]
                                        </button>
                                    </div>
                                    <!--// ********** Editable Function **********-->
                                    <script>
                                        var myBtn = document.getElementById('EditButton');
                                        //add event listener
                                        myBtn.addEventListener('click', function (event) {
                                            $('div[contenteditable="false"]').attr('contenteditable', true);
                                            $('div[contenteditable="false"]').addClass('test');
                                        });
                                        var myBtn1 = document.getElementById('save-description');
                                        //add event listener
                                        myBtn1.addEventListener('click', function (event) {
                                            $('div[contenteditable="true"]').attr('contenteditable', false);
                                        });
                                    </script>

                                </div>
                            </div>
                        </div>

                    </div><!--***** Close the Content *****-->
                </div><!--***** Row *****-->
            </div><!--***** Content *****-->
        </div><!--***** business_std_container *****-->

    <?php
				} ?>

    <div class="business_std_container row-nav" data-height="" id="row2" style="margin-top: 0px;" >
        <div class="content">
            <div class="row">
                <div class="col-md-12 business-body alert-grey">
                    <h4 class="page-header">[$Fotogallery]</h4>

                    <!--***** Insert new additional images *****-->

                    <div id="p-gallery">     
                        <div class="row">     
                            <?php if (count($business->images) > 0) {
					?>

                                <style>
                                    .ui-widget {
                                        font-family: Verdana,Arial,sans-serif;
                                        font-size: 1.1em;
                                    }
                                    .ui-dialog-titlebar-close {
                                        background: rgb(201, 0, 0);
                                        border: 0;
                                        color: white;
                                        /* border-radius: 50%; */
                                        font-weight: bold;
                                    }
                                    .ui-dialog-titlebar-close:after {
                                        content: 'X'; /* ANSI X letter */
                                    }
                                    .ui-widget-content {
                                        background: #fff;
                                        /* border: 1px solid #90d93f; */
                                        color: #222222;
                                        padding: 10px;
                                    }
                                    .ui-dialog {
                                        left: 0;
                                        outline: 0 none;
                                        padding: 0 !important;
                                        position: absolute;
                                        top: 0;
                                        box-shadow: 0 3px 15px rgba(0,0,0,0.5);
                                    }
                                    #success {
                                        padding: 0;
                                        margin: 0; 
                                    }
                                    .ui-dialog .ui-dialog-content {
                                        background: none repeat scroll 0 0 transparent;
                                        border: 0 none;
                                        overflow: auto;
                                        position: relative;
                                        padding: 20px 10px;
                                        display: block;
                                        width: auto;
                                        min-height: 31px;
                                        max-height: none;
                                        height: auto;
                                        border-bottom: 1px solid #b5b6ba;
                                    }
                                    .ui-widget-header {
                                        background: #c90000;
                                        border: 0;
                                        color: #fff;
                                        font-weight: normal;
                                        /* padding: 0 5px 10px 5px; */
                                        text-align: right;
                                    }
                                    .ui-dialog .ui-dialog-titlebar {
                                        position: relative;
                                        font-size: 1em;
                                    }
                                    .ui-dialog-buttonset {
                                        text-align: right;
                                    }
                                    .ui-dialog-buttonset button{
                                        background-color: #C90000;
                                        color: white;
                                        display: inline-block;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        touch-action: manipulation;
                                        cursor: pointer;
                                        user-select: none;
                                        border: 0px solid transparent;
                                        border-radius: 2px;
                                        margin-left: 5px;
                                    }
                                </style>
<!--                                    <div id="gResponse" ></div>-->
                                <?php foreach ($business->images as $img) {
						?>
                                    <div class="col-md-2" id="image-<?= $business->id . '-' . $img->id ?>">
                                        <div class="item" style=" margin-bottom: 30px;background: white;">
                                            <a class=""
                                               href="<?= Utils::genThumbnailUrl('businesses/additional/' . $img->image, 800, 500, array('far' => '1', 'bg' => 'FFFFFF'), true, $image_baseurl) ?>"
                                               data-lightbox="image-1">
                                                <img class="lazyOwl" src="<?= Utils::genThumbnailUrl('businesses/additional/' . $img->image, 205, 205, array('zc' => 1), true, $image_baseurl) ?>" alt="Immagine" width="100%" style="padding: 3px;">
                                            </a>

                                            <li href="#" class="fa fa-trash lb-close" id="<?= $business->id . '-' . $img->id ?>" style="font-size: 20px;color: #c90000;text-shadow: none;margin: 0 auto;display: table;padding: 5px 0;">  </li> 

                                        </div>
                                    </div>
                                <?php
					} ?>
                                <script>
                                    $(document).ready(function ()
                                    {
                                        $('li.fa-trash').css('cursor', 'pointer');
                                        $('li.fa-trash').click(function (e)
                                        {
                                            var id_img = $(this).attr('id');
                                            $("<div>[$Are_you_sure_you_want_to_delete_this_image] </div>").dialog({
                                                buttons: {
                                                    "[$yes_option]": function () {

                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?= Utils::getComponentUrl('Businesses/ajx_GalleryImageDelete/') ?>",
                                                            //dataType: 'json',
                                                            data: {
                                                                'id_img': id_img,
                                                            },
                                                            success: function (data) {
                                                                $("#image-" + id_img).remove();
                                                                console.log(data);
                                                            },
                                                            error: function (e) {
                                                                console.log(e);
                                                            }
                                                        });
                                                        $(this).dialog("close");
                                                    },
                                                    "[$Cancel]": function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            });
                                        });
                                    });
                                </script>
                            <?php
				} ?>
                        </div>
                    </div>

                    <div id="content">
                        <!-- Drag&Drop Files -->
                        <input type="file" name="files[]" id="filer_input2" multiple="multiple">
                    </div>

                </div><!--***** Close the Content *****-->
            </div><!--***** Row *****-->
        </div><!--***** Content *****-->
    </div><!--***** business_std_container *****-->




    <?php if ($business->id != '') {
					?>
        <div class="row">
            <div class="col-md-9">
                <div class="business_std_container left-container" style="margin-top: 0px;" >
                    <div class="content">
                        <div class="row">
                            <div class="col-md-12 business-body alert-grey">
                                <?php include dirname(__FILE__) . '/box_opening_hours.php' ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-3">
                <div class="business_std_container right-container row-nav" data-height="" id="row2" style="margin-top: 0px;" >
                    <div class="content">
                        <div class="row">

                            <div class="col-md-12 business-body alert-grey">
                                <h4 class="page-header">[$Payment_Method]</h4>
                                <div class="paper-toggle">
                                    <form id="paymentsmethod">
                                        <input type="hidden" name="updatePayments" value="1" />

                                        <?php
										foreach ($availPayments as $p) {
											$checked = '';
											if (Utils::in_ObjectArray($currPayments, 'id_payment', $p->id)) {
												$checked = ' checked ';
											}
											echo "  <div class=\"checkbox\"><input class=\"btn-checkbox switch checkbox\" id=\"wifi_{$p->id}\" type=\"checkbox\" name=\"pay[]\" value=\"{$p->id}\" $checked> <label class=\"checkbox-inline\" for=\"wifi_{$p->id}\"><span class=\"label-text\">[$" . $p->name . "]</span></label></div>\n";
										} ?>

                                        <div><input class="btn btn-sm btn-default save-business-button" type="submit" value='Ruaj Metodat e Pageses' /></div>

                                    </form>
                                </div>
                                <div id='payResponse'></div>
                                <script>
                                    $(document).ready(function () {
                                        $('#paymentsmethod').submit(function () {

                                            // show that something is loading
                                            $('#payResponse').html("<b>Loading response...</b>");

                                            $.ajax({
                                                type: 'POST',
                                                url: '<?= Utils::getComponentUrl('Businesses/ajx_updateBusinessPayment/') . $business->id ?>',
                                                data: $(this).serialize()
                                            })
                                                    .done(function (data) {
                                                        // show the response
                                                        $('#payResponse').html(data);
                                                    })
                                                    .fail(function () {
                                                        // just in case posting your form failed
                                                        alert("Posting failed.");
                                                    });

                                            // to prevent refreshing the whole page page
                                            return false;

                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php
				} ?>





    <!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->               
</div><!--***** content-wrap End *****-->


<script>
    $('#save-description').click(
            function () {
                var description = $('.description-ajx').html();
                //console.log(description);
                $.ajax({
                    type: 'POST',
                    url: '<?= Utils::getComponentUrl('Businesses/ajx_UpdateBusinessDesciption/') . $business->id ?>',
                    data: {'save-description': '1475',
                        'description': description},
                    success: function (response) {
                        $('.business-description').prepend(response);
                        setTimeout(function () {
                            $("#msg-success").hide('slow');
                        }, 3500);
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });
            }

    );</script>



<!--// ************ Fixed Nav ***********-->
<script>
    jQuery(document).ready(function ($) {

        $(window).scroll(function () {
            header = $('#nav-wrap');
            if ($(window).width() < 991)
            {
                var scrollTop = 0;
                if ($(window).scrollTop() >= scrollTop) {
                    header.addClass('nav-shrink')
                    $('#nav-wrap').css({
                        position: 'fixed',
                        bottom: '0px'
                    });
                }
            } else if ($(window).width() > 991) {
                var scrollTop = 500;
                if ($(window).scrollTop() >= scrollTop) {
                    header.addClass('nav-shrink');
                    $('#nav-wrap').css({
                        position: 'fixed',
                        top: '61px'
                    });
                }
            }

            if ($(window).scrollTop() < scrollTop) {
                header.removeClass('nav-shrink');
                $('#nav-wrap').removeAttr('style');
            }
        })

    })
</script>

<!--//************** Menu Active ****************-->
<script>
    $(document).ready(function () {
        function moveMarker() {
            var activeNav = $('.active a');
            var activewidth = $(activeNav).width();
            var activePadLeft = parseFloat($(activeNav).css('padding-left'));
            var activePadRight = parseFloat($(activeNav).css('padding-right'));
            var totalWidth = activewidth + activePadLeft + activePadRight;
            var precedingAnchorWidth = anchorWidthCounter();
            // TODO: 
            // Find the total widths of all of the anchors
            // to the left of the active anchor.

            var activeMarker = $('.active-marker');
            $(activeMarker).css('display', 'block');
            $(activeMarker).css('width', totalWidth);
            $(activeMarker).css('left', precedingAnchorWidth);
            // TODO: 
            // Using the calculated total widths of preceding anchors,
            // Set the left: css value to that number.
        }
        moveMarker();
        function anchorWidthCounter() {
            var anchorWidths = 0;
            var a;
            var aWidth;
            var aPadLeft;
            var aPadRight;
            var aTotalWidth;
            $('.main-nav li').each(function (index, elem) {
                var activeTest = $(elem).hasClass('active');
                if (activeTest) {
                    // Break out of the each function.
                    return false;
                }

                a = $(elem).find('a');
                aWidth = a.width();
                aPadLeft = parseFloat(a.css('padding-left'));
                aPadRight = parseFloat(a.css('padding-right'));
                aTotalWidth = aWidth + aPadLeft + aPadRight;
                anchorWidths = anchorWidths + aTotalWidth;
            });
            return anchorWidths;
        }

        $('.main-nav a').click(function (e) {
            e.preventDefault();
            $('.main-nav li').removeClass('active');
            $(this).parents('li').addClass('active');
            moveMarker();
        });
    });</script>

<script>

    //*********Scroll Page Menu tab ***********//
    /******************************
     ADD ANIMATION TO THE TEXT 
     ******************************/
    var count = 0;
    var number = 10;
    /*************************
     ON WINDOW SCROLL FUNCTION
     *************************/
    var sectionIds = {};
    $(".row-nav").each(function () {
        var $this = $(this);
        sectionIds[$this.attr("id")] = $this.first().offset().top - 120;
    });
    var count2 = 0;
    $(window).scroll(function (event) {

        var scrolled = $(this).scrollTop();
        //If it reaches the top of the row, add an active class to it
        $(".row-nav").each(function () {

            var $this = $(this);
            if (scrolled >= $this.first().offset().top - 120) {
                $(".row-nav").removeClass("active");
                $this.addClass("active");
                $(".animation").removeClass('animationActive');
                $this.find(".animation").addClass('animationActive');
            }
        });
        //when reaches the row, also add a class to the navigation
        for (key in sectionIds) {
            if (scrolled >= sectionIds[key]) {
                $(".nav-btn").removeClass("active");
                var c = $("[data-row-id=" + key + "]");
                c.addClass("active");
                var i = c.index();
                $('#nav-indicator').css('left', i * 100 + 'px');
            }
        }


        //Check if we've reached the top
        if (scrolled > count2) {
            count2++;
        } else {
            count2--;
        }

        count2 = scrolled;
        if (count2 == 0) {
            $('h1 ,h2').addClass('animationActive');
        } else {
            $('h1 ,h2').removeClass('animationActive');
        }

    });
    /**************
     IN-NAVIGATION
     **************/
    $(".nav-btn").click(function () {
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
        var i = $(this).index();
        $('#nav-indicator').css('left', i * 100 + 'px');
        var name = $(this).attr("data-row-id");
        var id = "#" + name;
        var top = $(id).first().offset().top - 150;
        $('html, body').animate({scrollTop: top + 'px'}, 300);
    });</script>


<script>
    $(document).ready(function () {

        //Example 2
        $("#filer_input2").filer({
            limit: null,
            maxSize: null,
            extensions: null,
            changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop</h3> <span style="display:inline-block; margin: 15px 0">[$or]</span></div><a class="btn btn-sm btn-default">[$choose_photos]</a></div></div>',
            showThumbs: true,
            theme: "dragdropbox",
            templates: {
                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                item: '<li class="jFiler-item">\
                                                <div class="jFiler-item-container">\
                                                        <div class="jFiler-item-inner">\
                                                                <div class="col-md-12">\
                                                                <div class="jFiler-item-thumb">\
                                                                        <div class="jFiler-item-status"></div>\
                                                                        <div class="jFiler-item-thumb-overlay">\
                                                                                <div class="jFiler-item-info">\
                                                                                        <div style="display:table-cell;vertical-align: middle;">\
                                                                                                <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
                                                                                                <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                                                        </div>\
                                                                                </div>\
                                                                        </div>\
                                                                        {{fi-image}}\
                                                                </div>\
                                                                <div class="jFiler-item-assets jFiler-row">\
                                                                        <ul class="list-inline pull-left">\
                                                                                <li>{{fi-progressBar}}</li>\
                                                                        </ul>\
                                                                        <ul class="list-inline pull-right">\
                                                                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                                        </ul>\
                                                                </div>\
                                                                </div>\
                                                        </div>\
                                                </div>\
                                        </li>',
                itemAppend: '<li class="jFiler-item">\
                                                        <div class="jFiler-item-container">\
                                                                <div class="jFiler-item-inner">\
                                                                        <div class="col-md-12">\
                                                                        <div class="jFiler-item-thumb">\
                                                                                <div class="jFiler-item-status"></div>\
                                                                                <div class="jFiler-item-thumb-overlay">\
                                                                                        <div class="jFiler-item-info">\
                                                                                                <div style="display:table-cell;vertical-align: middle;">\
                                                                                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
                                                                                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                                                                </div>\
                                                                                        </div>\
                                                                                </div>\
                                                                                {{fi-image}}\
                                                                        </div>\
                                                                        <div class="jFiler-item-assets jFiler-row">\
                                                                                <ul class="list-inline pull-left">\
                                                                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                                                                </ul>\
                                                                                <ul class="list-inline pull-right">\
                                                                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                                                </ul>\
                                                                        </div>\
                                                                        </div>\
                                                                </div>\
                                                        </div>\
                                                </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                canvasImage: true,
                removeConfirmation: true,
                _selectors: {
                    list: '.jFiler-items-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action'
                }
            },
            dragDrop: {
                dragEnter: null,
                dragLeave: null,
                drop: null,
                dragContainer: null,
            },
            uploadFile: {
                url: '<?= Utils::getComponentUrl('Businesses/ajx_GalleryImageUpload/' . $business->id); ?>',
                data: null,
                type: 'POST',
                enctype: 'multipart/form-data',
                synchron: true,
                beforeSend: function () {},
                success: function (data, itemEl, listEl, boxEl, newInputEl, inputEl, id) {

                    $('#gResponse').html(data);

                    var parent = itemEl.find(".jFiler-jProgressBar").parent(),
                            new_file_name = data,
                            filerKit = inputEl.prop("jFiler");
                    filerKit.files_list[id].name = new_file_name;
                    if (new_file_name == "error01") {
                        itemEl.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                            $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-check-circle\"></i> [$upgrade_wanted]</div>").hide().appendTo(parent).fadeIn("slow");
                        });
                    } else if (new_file_name == "error02") {
                        itemEl.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                            $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-check-circle\"></i> [$file_format_not_allowed]</div>").hide().appendTo(parent).fadeIn("slow");
                        });
                    } else {
                        itemEl.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                            $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i>[$success]</div>").hide().appendTo(parent).fadeIn("slow");
                        });
                    }

                },
                error: function (el) {
                    var parent = el.find(".jFiler-jProgressBar").parent();
                    el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                        $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                    });
                },
                statusCode: null,
                onProgress: null,
                onComplete: null
            },
            files: null,
            addMore: false,
            allowDuplicates: true,
            clipBoardPaste: true,
            excludeName: null,
            beforeRender: null,
            afterRender: null,
            beforeShow: null,
            beforeSelect: null,
            onSelect: null,
            afterShow: null,
            onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                var filerKit = inputEl.prop("jFiler"),
                        file_name = filerKit.files_list[id].name;
                $.post('./php/ajax_remove_file.php', {file: file_name});
            },
            onEmpty: null,
            options: null,
            dialogs: {
                alert: function (text) {
                    return alert(text);
                },
                confirm: function (text, callback) {
                    confirm(text) ? callback() : null;
                }
            },
            captions: {
                button: "Choose Files",
                feedback: "Choose files To Upload",
                feedback2: "files were chosen",
                drop: "Drop file here to Upload",
                removeConfirmation: "Are you sure you want to remove this file?",
                errors: {
                    filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                    filesType: "Only Images are allowed to be uploaded.",
                    filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                    filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
                }
            }
        });
    })
</script>


<?php
if ($business->coordinates != '') {
					list($lat, $lng) = explode(',', $business->coordinates);
				}
?>


<script type="text/javascript">
    var lat = <?= $lat ?>;
    var lng = <?= $lng ?>;
    var map = new google.maps.Map(document.getElementById('map-edit'), {
        zoom: 15,
        center: {lat: lat, lng: lng}
    });
    var geocoder = new google.maps.Geocoder();
    var marker;
    marker = new google.maps.Marker({
        icon: 'http://www.netirane.al/data/netirane.al/media/marker1.png'
    });
<?php
$location = $business->coordinates;
?>

    geocodeAddress("<?= $location ?>", geocoder, map);
    function setMarker() {

        if (marker != null)
            marker.setMap(null);
        var address = document.getElementById('address').value + '<?= $location ?>';
        geocodeAddress(address, geocoder, map);
        if (document.getElementById('address').value != '')
            map.setZoom(15);
        else
            map.setZoom(15);
    }



    function geocodeAddress(address, geocoder, resultsMap) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                marker.set('map', resultsMap);
                marker.set('position', results[0].geometry.location);
                marker.set('draggable', true);
                marker.set('animation', google.maps.Animation.DROP);
                marker.set('icon', 'http://www.netirane.al/data/netirane.al/media/marker2.png');
//                marker = new google.maps.Marker({
//                    map: resultsMap,
//                    position: results[0].geometry.location,
//                    draggable: true,
//                    animation: google.maps.Animation.DROP,
//                    icon: 'http://www.netirane.al/data/netirane.al/media/marker2.png'
//                });


                google.maps.event.addListener(marker, 'dragend', function () {
                    geocodePosition(marker.getPosition());
                });
                $('#coordinates').val(results[0].geometry.location);
                //console.log($('#coordinates').val());
            } else {
                $("#warning").modal();
            }
        });
    }


    function geocodePosition(pos) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode
                ({
                    latLng: pos
                },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                $("#address").val(results[0].formatted_address);
                                $('#coordinates').val(results[0].geometry.location.toString());
                            } else {
                                $("#warning").modal();
                                //$("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
                            }
                        }
                );
    }

    var checkeventcount = 1, prevTarget;
    $('.modal').on('show.bs.modal', function (e) {
        if (typeof prevTarget == 'undefined' || (checkeventcount == 1 && e.target != prevTarget)) {
            prevTarget = e.target;
            checkeventcount++;
            e.preventDefault();
            $(e.target).appendTo('body').modal('show');
        } else if (e.target == prevTarget && checkeventcount == 2) {
            checkeventcount--;
        }
    });
</script>
<!-- Rating system -->
<script>
    $(document).ready(function () {


        function checkFieldsIfEmpty() {
            var count = 0;

            if ($('#r-name').val() == '') {
                count++;
            }
            if ($('#r-email').val() == '') {
                count++;
            }
            if ($('#r-tel').val() == '') {
                count++;
            }
            if ($('#r-comment').val() == '') {
                count++;
            }
            return count;

        }


        function hideMessages() {
            setTimeout(function () {
                $("#msg-success").hide('slow');
            }, 3500);
            setTimeout(function () {
                $(".alert-danger").hide('slow');
            }, 3500);
        }

        $("#s-review").click(function () {

            if (checkFieldsIfEmpty() > 0) {
                $('#reviews').prepend('<p class="msg-success alert-danger alert-costumized" name="messageajx" >[$all_fields_are_required]</p>');
                hideMessages();

            } else {

                $.ajax({
                    type: "POST",
                    url: '<?= Utils::getComponentUrl('Businesses/ajx_reviewBusiness') ?>',
                    data: $("#r-form").serialize(),
                    success: function (data) {
                        console.log(data);
                        $('#reviews').append(data);
                        $("#fieldset-review").hide('slow');
                        hideMessages();
                        hideMessages();
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            }
        });
        setTimeout(function () {
            $(".alert-costumized").hide('slow');
        }, 3500);
        function addStatistic(id_business, type) {
            $("#reviews").hide();
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_addStatistic') ?>",
                data: {
                    'id_business': id_business,
                    'type': type,
                },
                success: function (data) {
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function rate(rating, id_biz) {
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_rateBusiness') ?>",
                data: {
                    'id_biz': id_biz,
                    'rating': rating
                },
                success: function (data) {
                    console.log(data);
                    var arr = data.split(",");
                    $('#r-form').prepend('<input type="hidden" name="id" value="' + arr[0] + '" />');
                    $('#r-form').prepend('<input type="hidden" name="id_biz" value="' + arr[1] + '" />');
                    $('#r-form').prepend('<input type="hidden" name="rating" value="' + arr[2] + '" />');
                    //success
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function controllCookie(id_biz) {

            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_checkRatingCookie') ?>",
                data: {
                    'id_biz': id_biz,
                },
                success: function (rating) {
                    console.log(rating);
                    if (rating != 0) {
                        for (var i = 1; i <= rating; i++) {
                            $('#star_' + i).attr("class", "fa fa-star star-on-png fa-3x ratings_stars");
                        }
                        $('.ratings_stars').unbind();
                    }

                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function ratingDataText(id_biz) {
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValue') ?>",
                data: {
                    'id_biz': id_biz,
                },
                success: function (data) {
                    d = data.split("|");
                    $('#average').html("[$rating]: " + d[0] + " - [$rating_count]: " + d[1]);
                },
                error: function (e) {
                    $('#average').html("Pati probleme");
                    console.log(e);
                }
            });
        }

        function ratingDataTextShow(id_biz) {
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValue') ?>",
                data: {
                    'id_biz': id_biz,
                },
                success: function (data) {
                    d = data.split("|");
                    $('#showrate').html(d[0]);
                },
                error: function (e) {
                    $('#average').html("Pati probleme");
                    console.log(e);
                }
            });
        }

        function ratingDataStar(id_biz) {

            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValueBList') ?>",
                dataType: 'json',
                data: {
                    'id_biz': id_biz,
                },
                success: function (data) {

                    if (data.rate != 0) {
                        for (var i = 0; i <= data.rate; i++)
                            $('#star_' + i + '_' + id_biz).attr("class", "fa fa-star star-on-png");
                        if (data.half == 1)
                            $('#star_' + i + "_" + id_biz).attr("class", "fa fa-star-half-o star-on-png");
                    }

                },
                error: function (e) {
                    console.log(e);
                }
            });
            $("#reviews").slideToggle("slow");
        }

        //reviews toggle
        $("#skip-reviews").click(function () {
            $("#reviews").slideToggle("slow");
        });
        // message toggle

        $(".contact-open").click(function () {
            $("#send-msg").slideToggle("slow");
        });
        $(".request-open").click(function () {
            $("#send-request").slideToggle("slow");
        });
        $("#skip-request").click(function () {
            $("#send-request").slideToggle("slow");
        });
        $("#skip-msg").click(function () {
            $("#send-msg").slideToggle("slow");
        });
        $(document).ready(function () {
            //yje ne title mbushi
            ratingDataStar(<?= $business->id ?>);
            //jep mesataren e rating te bere
            ratingDataText(<?= $business->id ?>);
            //shfaq mesataren e rating
            ratingDataTextShow(<?= $business->id ?>);
            //kontroll cookie nqs ka votuar
            controllCookie(<?= $business->id ?>);
            addStatistic(<?= $business->id ?>, 'view');
            $('#website_link').click(function () {
                addStatistic(<?= $business->id ?>, 'website_click');
            });
            $('#facebook_link').click(function () {
                addStatistic(<?= $business->id ?>, 'facebook_click');
            });
            $('#linkedin_link').click(function () {
                addStatistic(<?= $business->id ?>, 'linkedin_click');
            });
            $('#instagram_link').click(function () {
                addStatistic(<?= $business->id ?>, 'instagram_click');
            });
            //hover
            $('.ratings_stars').hover(
                    //mouseover
                            function () {
                                $(this).prevAll().andSelf().attr("class", "fa fa-star star-on-png  fa-3x ratings_stars");
                            },
                            //mouseout
                                    function () {
                                        $(this).prevAll().andSelf().attr("class", "fa fa-star-o star-on-png fa-3x ratings_stars");
                                    }
                            );
                            //click
                            $('.ratings_stars').click(
                                    function () {
                                        $(this).prevAll().andSelf().attr("class", "fa fa-star star-on-png  fa-3x ratings_stars");
                                        var rating = $(this).prevAll().andSelf().length;
                                        rate(rating,<?= $business->id ?>);
                                        $('.ratings_stars').unbind();
                                        ratingDataText(<?= $business->id ?>);
                                        ratingDataStar(<?= $business->id ?>);
                                    }
                            );
                        });

            });
</script>
