<?php
$comnum = 0;
foreach ($comments as $comment) {
	if ($comment['comment'] != '') {
		$comnum++;
	}
}
?><?php
$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';
?>
<?php HeadHTML::addMeta('keywords', $business->company_name . ' , ' . $business->category . ' , ' . $business->address . '  , ' . CMSSettings::$website_title . ' , njoftime , biznese , ' . $business->category . ' ' . CMSSettings::$website_title); ?>
<?php HeadHTML::addMeta('description', StringUtils::CutString(strip_tags($business->description), 200)); ?>
<?php HeadHTML::addMetaProperty('og:title', $business->company_name . ' | ' . CMSSettings::$website_title); ?>
<?php HeadHTML::addMetaProperty('og:type', 'article'); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('businesses/' . str_replace('%2F', '/', urlencode($business->image)), 980, 600, array('zc' => '1'), false, $image_baseurl)); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($business->description), 200)); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/business-details-map.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/jquery-filer.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/jquery.filer.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/jquery.filer-dragdropbox-theme.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/mpg.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/business-offers.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . COMPONENTS_PATH . 'Businesses/' . VIEWS_PATH . 'css/offer-details-style.css'); ?>
<link rel="stylesheet" href="https://cdn.pannellum.org/2.3/pannellum.css"/>
<?php HeadHtml::linkStylesheet('owl.carousel.css'); ?>

<script type="text/javascript" src="http://cdn.pannellum.org/2.3/pannellum.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jssor-slider/23.1.0/jssor.slider.min.js"></script>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jssor.slider-23.1.5.mini.js'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/business-details-map.js'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.filer.js'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/mpg.js'); ?>
<?php if (count($business->images) > 0) {
	?>

<script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 600);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/
        });
    </script>
<?php
} ?>
    <style>
        /* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
        body {font-family: Verdana,sans-serif;}
        .jssorb05 {
            position: absolute;
        }
        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
            position: absolute;
            /* size of bullet elment */
            width: 16px;
            height: 16px;
            background: url('http://netirane.al/data/netirane.al/media/slideshow/b05.png') no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb05 div { background-position: -7px -7px; }
        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
        .jssorb05 .av { background-position: -67px -7px; }
        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

        /* jssor slider arrow navigator skin 12 css */
        /*
        .jssora12l                  (normal)
        .jssora12r                  (normal)
        .jssora12l:hover            (normal mouseover)
        .jssora12r:hover            (normal mouseover)
        .jssora12l.jssora12ldn      (mousedown)
        .jssora12r.jssora12rdn      (mousedown)
        */
        .jssora12l, .jssora12r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 30px;
            height: 46px;
            cursor: pointer;
            background: url(<?= Utils::genThumbnailUrl('slideshow/a12.png')?>) no-repeat;
            overflow: hidden;
        }
        .jssora12l { background-position: -16px -37px; }
        .jssora12r { background-position: -75px -37px; }
        .jssora12l:hover { background-position: -136px -37px; }
        .jssora12r:hover { background-position: -195px -37px; }
        .jssora12l.jssora12ldn { background-position: -256px -37px; }
        .jssora12r.jssora12rdn { background-position: -315px -37px; }
    </style>
<?php HeadHtml::linkJS('owl.carousel.js'); ?>




<?php require_once LIBS_PATH . 'StringUtils.php'; ?>

<?php $lat = (isset($settings['map_lat'])) ? $settings['map_lat'] : '41.3280456' ?>
<?php $lng = (isset($settings['map_lng'])) ? $settings['map_lng'] : '19.8119486' ?>

<?php
$show_ShareButton = (isset($parameters['show_Share']) && $parameters['show_Share'] == 'no') ? false : true;
$w = (isset($parameters['w'])) ? $parameters['w'] : 750;
$h = (isset($parameters['h'])) ? $parameters['h'] : 500;
$w_gallery = (isset($parameters['w'])) ? $parameters['w'] : 1000;
$h_gallery = (isset($parameters['h'])) ? $parameters['h'] : 0;

$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$tel = isset($_POST['tel']) ? $_POST['tel'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';
$map = ($business->coordinates != '') ? true : false;

$id_profile = $business->id_profile;
?>

<?php
$company_name = (isset($_POST['company_name'])) ? $_POST['company_name'] : '';
$description = (isset($_POST['description'])) ? $_POST['description'] : '';

$email = (isset($_POST['email'])) ? $_POST['email'] : '';
$tel = (isset($_POST['phone'])) ? $_POST['phone'] : '';
$website = (isset($_POST['website'])) ? $_POST['website'] : '';
$facebook = (isset($_POST['facebook'])) ? $_POST['facebook'] : '';
$linkedin = (isset($_POST['linkedin'])) ? $_POST['linkedin'] : '';
$instagram = (isset($_POST['instagram'])) ? $_POST['instagram'] : '';
$google = (isset($_POST['google'])) ? $_POST['google'] : '';
$twitter = (isset($_POST['twitter'])) ? $_POST['twitter'] : '';
$id_category = (isset($_POST['id_category'])) ? $_POST['id_category'] : '';
$category = (isset($_POST['category'])) ? $_POST['category'] : '';

$address = (isset($_POST['address'])) ? $_POST['address'] : '';
$coordinates = (isset($_POST['coordinates'])) ? $_POST['coordinates'] : '';

$add_ids = array();
if (isset($_POST['additional_categ'])) {
	if (is_array($_POST['additional_categ'])) {
		foreach ($_POST['additional_categ'] as $add_categ) {
			array_push($add_ids, $add_categ);
		}
	} else {
		array_push($add_ids, $_POST['additional_categ']);
	}
}
?>


<?php if ($business->coordinates != '' || $business->coordinates != null) {
	?>
    <style>
        .breadcrumb {
            margin-top: -220px;
        }
    </style>
<?php
} ?>
<style>

    #map {
        height: 500px;
    }
    #map-controls {
        display: none;
    }
    /* Admin Menu Affix */
    .affix {
        top: 140px;
        z-index: 97;
        width: 262px;
    }

    div[contenteditable=true] {
        background: rgba(0,0,0,.5);
        border: 1px dotted rgb(123, 123, 123);

    }
    .breadcrumb {
        background-color: rgba(0, 0, 0, 0.8);
        margin-left: -15px;
        margin-right: -15px;
        border-bottom: 1px solid #424242;
    }
    .breadcrumb li:last-child a {
        background: transparent;
    }
    breadcrumb li:last-child a:hover {
        background: transparent;
    }
    .breadcrumb li a {
        color: #ffffff;
    }
    .breadcrumb li a:hover {
        background-image: linear-gradient(to left, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0));
        color: #ffffff !important;
        background: transparent;
    }
    .breadcrumb li:nth-child(2) a, .breadcrumb li:nth-child(3) a, .breadcrumb li:nth-child(4) a, .breadcrumb li:nth-child(5) a {
        background-color: rgba(255, 255, 255, 0);
        background-image: linear-gradient(to left, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0));
    }
    .breadcrumb li a:after {
        border-left: 15px solid rgb(48, 48, 48);
    }
    .breadcrumb li a:before {
        border-left: 15px solid #414141;
    }
    .breadcrumb li a:hover:after {
        border-left-color: #303030;
    }
    .breadcrumb li:nth-child(2) a:after, .breadcrumb li:nth-child(3) a:after, .breadcrumb li:nth-child(4) a:after,.breadcrumb li:nth-child(5) a:after {
        border-left-color: #303030;
    }
    .breadcrumb li:last-child a {
        color: #ffffff;
    }

    @media (max-width: 500px) {
        .breadcrumb {
            display:none;
        }
    }

</style>

<div class="business_std_container first-container" <?php if ($map) {
		?> style="margin-top: -20px;" <?php
	} ?> >
    <div class="content">
        <div class="row">
            <div class="col-md-12 business-body alert-grey bis-head">

                <!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->  
                <!--***** Bussiness HEAD *****-->
                <div class="row content-body">

                    <div class="col-md-9">

                        <div class="row">

                            <!--***** Business Main Image *****-->
                            <div class="col-md-4 col-img">
                                <div class="business-image">

                                    <div class="profile large">
                                        <div class="user-info">
                                            <div class="profile-pic">

                                                <?php if ($business->image != '') {
		?>
                                                    <a class="" href="<?= Utils::genThumbnailUrl('businesses/' . $business->image, $w, $h, array('zc' => '0'), false, $image_baseurl) ?>" data-lightbox="image-1">
                                                        <img id="business-picture" class="image" src="<?= Utils::genThumbnailUrl('businesses/' . $business->image, $w, $h, array('zc' => '0'), false, $image_baseurl) ?>"/>
                                                    </a>
                                                <?php
	} else {
		?><img class="little-logo"
                                                    src="<?= WEBROOT . $this->view_path ?>img/default.jpg"
                                                    style="width:100%">
                                                <?php
	} ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--***** Business Main Information *****-->
                            <div class="col-md-8 col-info">

                                <!--Name & Category-->
                                <div class="row">
                                    <div class="col-md-12 col-title">
                                        <div class="business-title">
                                            <div>
                                                <h3 class="business-name" ><?= $business->company_name ?></h3>
                                            </div>


                                            <div class="business-address">
                                                <!--<span class="main-addres">
                                                <?php if ($business->address != '') {
		?> 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <i class="fa fa-map-marker" aria-hidden="true"></i> 
                                                    <?= $business->address ?>
                                                <?php
	} ?>
                                                </span>-->
                                                <span class="main-comune">
                                                    <?php
													for ($j = 0; $j < count($business->id_category); $j++) {
														if ($j > 0) {
															break;
														} ?>
                                                        <a href="<?= Utils::getComponentUrl('Businesses/b_list') . '/' . Utils::url_slug($business->category . '-' . $business->id_category) ?>">
                                                            <?php
															if (!is_null($business->category) && $business->category != '') {
																echo $business->category;
															} elseif (isset($business->categories) && count($business->categories)) {
																echo $business->categories[0];
															} ?>
                                                        </a>
                                                    <?php
													} ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Adress & Phone Number-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="business-contact-info">
                                            <div class="business-phone">
                                                <table style="">
                                                    <tr>
                                                        <?php if ($business->location != '') {
														?>
                                                        <span class="business-url">
                                                            <td> <?= $business->address ?> </td>
                                                        </span>
                                                    <?php
													} ?>
                                                    </tr>
                                                    <tr>
                                                        <?php if ($business->phone != '' || $business->mobile != '') {
														?>
                                                        <span class="business-url">
                                                            <td > <?= $business->phone ?><?= ($business->mobile != '' && $business->phone != '') ? ' &nbsp;&nbsp;-&nbsp;&nbsp; ' : ''; ?> <?= $business->mobile ?></td>
                                                        </span>
                                                    <?php
													} ?>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Social Networks-->
                                <div class="row">
                                    <div class="col-md-12 col-social">
                                        <div class="business-social">

                                            <?php if (in_array($id_profile, array(1, 2, 3, 4, 5)) && CMSSettings::$webdomain != 'cittanelweb.it') {
														?>
                                                <!--Facebook-->
                                                <?php
												if ($business->facebook != '') {
													if (strpos($business->facebook, 'https://') !== false) {
														$facebook = substr($business->facebook, 8);
													}
													if (strpos($business->facebook, 'http://') !== false) {
														$facebook = substr($business->facebook, 7);
													} ?>
                                                    <span class="main-url"><a id="facebook_link" href="https://<?= $facebook ?>" target="_blank">
                                                            <i class="fa fa-facebook main-color"></i></a>
                                                    </span>
                                                <?php
												} ?>

                                                <!--Instagram-->
                                                <?php
												if ($business->instagram != '') {
													if (strpos($business->instagram, 'https://') !== false) {
														$instagram = substr($business->instagram, 8);
													}
													if (strpos($business->instagram, 'http://') !== false) {
														$instagram = substr($business->instagram, 7);
													} ?>
                                                    <span class="main-url"><a id="instagram_link" href="https://<?= $instagram ?>" target="_blank">
                                                            <i class="fa fa-instagram main-color"></i></a>
                                                    </span>
                                                <?php
												} ?>

                                                <!--Twitter-->
                                                <?php
												if ($business->twitter != '') {
													if (strpos($business->twitter, 'https://') !== false) {
														$twitter = substr($business->twitter, 8);
													}
													if (strpos($business->twitter, 'http://') !== false) {
														$twitter = substr($business->twitter, 7);
													} ?>
                                                    <span class="main-url"><a id="twitter_link" href="https://<?= $twitter ?>" target="_blank">
                                                            <i class="fa fa-twitter main-color"></i></a>
                                                    </span>
                                                <?php
												} ?>

                                                <!--Google-->
                                                <?php
												if ($business->google != '') {
													if (strpos($business->google, 'https://') !== false) {
														$google = substr($business->google, 8);
													}
													if (strpos($business->google, 'http://') !== false) {
														$google = substr($business->google, 7);
													} ?>
                                                    <span class="main-url"><a id="google_link" href="https://<?= $google ?>" target="_blank">
                                                            <i class="fa fa-google-plus main-color"></i></a>
                                                    </span>
                                                <?php
												} ?>

                                                <!--Linkedin-->
                                                <?php
												if ($business->linkedin != '') {
													if (strpos($business->linkedin, 'https://') !== false) {
														$linkedin = substr($business->linkedin, 8);
													}
													if (strpos($business->linkedin, 'http://') !== false) {
														$linkedin = substr($business->linkedin, 7);
													} ?>
                                                    <span class="main-url"><a id="linkedin_link" href="https://<?= $linkedin ?>" target="_blank">
                                                            <i class="fa fa-linkedin main-color"></i></a>
                                                    </span>
                                                <?php
												} ?>

                                                <!-- WEBSITE -->
                                                <?php
												if ($business->website != '' && $business->id_profile != 1 && $business->id_profile != 4) {
													$website = $business->website;
												} else {
													if (!is_null($business->subdomain)) {
														$website = $business->subdomain->subdomain;
													} else {
														$website = null;
													}
												}

														if (strpos($website, 'http://')) {
															$website = substr($website, 7);
														} elseif (strpos($website, 'https://')) {
															$website = substr($website, 8);
														} elseif (!is_null($website)) {
															$website = 'http://' . $website; ?>

                                                    <span class="main-url">
                                                        <a id="website_link" href="<?= $website ?>" target="_blank">
                                                            WWW
                                                        </a>
                                                    </span>

                                                    <?php
														}
													} else {
														if ($business->facebook != '') {
															if (strpos($business->facebook, 'https://') !== false) {
																$facebook = substr($business->facebook, 8);
															}
															if (strpos($business->facebook, 'http://') !== false) {
																$facebook = substr($business->facebook, 7);
															} ?>
                                                    <span class="main-url"><a id="facebook_link" href="https://<?= $facebook ?>" target="_blank">
                                                            <i class="fa fa-facebook main-color"></i></a>
                                                    </span>
                                                <?php
														} ?>

                                                <!--Instagram-->
                                                <?php
												if ($business->instagram != '') {
													if (strpos($business->instagram, 'https://') !== false) {
														$instagram = substr($business->instagram, 8);
													}
													if (strpos($business->instagram, 'http://') !== false) {
														$instagram = substr($business->instagram, 7);
													} ?>
                                                    <span class="main-url"><a id="instagram_link" href="https://<?= $instagram ?>" target="_blank">
                                                            <i class="fa fa-instagram main-color"></i></a>
                                                    </span>
                                                <?php
												} ?>

                                                <!--Twitter-->
                                                <?php
												if ($business->twitter != '') {
													if (strpos($business->twitter, 'https://') !== false) {
														$twitter = substr($business->twitter, 8);
													}
													if (strpos($business->twitter, 'http://') !== false) {
														$twitter = substr($business->twitter, 7);
													} ?>
                                                    <span class="main-url"><a id="twitter_link" href="https://<?= $twitter ?>" target="_blank">
                                                            <i class="fa fa-twitter main-color"></i></a>
                                                    </span>
                                                <?php
												} ?>

                                                <!--Google-->
                                                <?php
												if ($business->google != '') {
													if (strpos($business->google, 'https://') !== false) {
														$google = substr($business->google, 8);
													}
													if (strpos($business->google, 'http://') !== false) {
														$google = substr($business->google, 7);
													} ?>
                                                    <span class="main-url"><a id="google_link" href="https://<?= $google ?>" target="_blank">
                                                            <i class="fa fa-google-plus main-color"></i></a>
                                                    </span>
                                                <?php
												} ?>

                                                <!--Linkedin-->
                                                <?php
												if ($business->linkedin != '') {
													if (strpos($business->linkedin, 'https://') !== false) {
														$linkedin = substr($business->linkedin, 8);
													}
													if (strpos($business->linkedin, 'http://') !== false) {
														$linkedin = substr($business->linkedin, 7);
													} ?>
                                                    <span class="main-url"><a id="linkedin_link" href="https://<?= $linkedin ?>" target="_blank">
                                                            <i class="fa fa-linkedin main-color"></i></a>
                                                    </span>
                                                <?php
												} ?>

                                                <!-- WEBSITE -->
                                                <?php
												if ($business->website != '') {
													$website = $business->website;
												} else {
													$website = null;
												}

														if (strpos($website, 'http://')) {
															$website = substr($website, 7);
														} elseif (strpos($website, 'https://')) {
															$website = substr($website, 8);
														} elseif (!is_null($website)) {
															$website = 'http://' . $website; ?>

                                                    <span class="main-url">
                                                        <a id="website_link" href="<?= $website ?>" target="_blank">
                                                            WWW
                                                        </a>
                                                    </span>

                                                    <?php
														}
													}
											?>    



                                            <!--Share-->
                                            <div class="service-post">
                                                <div class="business-social-share">
                                                    <div class="getsocial gs-inline-group"></div>
                                                </div>
                                                <div class="service-content">
                                                    <div class="service-icon">
                                                        <i class="fa fa-share-alt"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!--***** Business Rating *****-->
                    <div class="col-md-3 show-rating">
                        <div id="showrate"></div>

                        <div class="rate-it rate-cus">
                            <i class="fa fa-star-o star-on-png" id="star_1_<?= $business->id ?>"></i>

                            <i data-alt="2" class="fa fa-star-o star-on-png"
                               id="star_2_<?= $business->id ?>"></i>

                            <i data-alt="3" class="fa fa-star-o star-on-png"
                               id="star_3_<?= $business->id ?>"></i>

                            <i data-alt="4" class="fa fa-star-o star-on-png"
                               id="star_4_<?= $business->id ?>"></i>

                            <i data-alt="5" class="fa fa-star-o star-on-png"
                               id="star_5_<?= $business->id ?>"></i>
                        </div>
                        <li style="list-style:none; cursor: pointer;" class="nav-btn" data-row-id="row3">
                            <a class="btn btn-sm btn-default bussiness-comments">[$insert_review]</a>
                        </li>
                    </div>
                </div>

                <?php
				$num = 2;
				if ($business->website != '') {
					$num++;
				} if ($business->subdomain != '') {
					$num++;
				}
				$col = 12 / $num;
				?>        

                <!--***** Send Message *****-->
                <section id="send-msg" style="display:none;">
                    <fieldset>
                        <legend class="scheduler-border" id="row6">[$send_a_message]</legend>
                        <div class="col-md-10 col-md-offset-1">
                            <form class="form-horizontal" action="" method="post">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="business-insertion">
                                            <fieldset>
                                                <input type="text" class="name required" id="name"  name="name" <?php if (isset($_SESSION['user_auth'])) {
					?> value="<?= $_SESSION['user_auth']['firstname'] . ' ' . $_SESSION['user_auth']['lastname'] ?>" <?php
				} ?> data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." required="" />
                                                <label for="name">[$Name]*</label>
                                                <div class="underline"></div>
                                            </fieldset>
                                        </div>
                                        <div class="business-insertion">
                                            <fieldset>
                                                <input type="email" class="email required" id="email"  name="email" value="<?= $email ?>" data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@email.com)" required="" />
                                                <label for="email">[$Email]*</label>
                                                <div class="underline"></div>
                                            </fieldset>
                                        </div>
                                        <div class="business-insertion">
                                            <fieldset>
                                                <input type="text" class="tel required" id="tel"  name="tel"  value="<?= $tel ?>" data-placement="top" data-trigger="manual" data-content="Must be a valid phone number" required="" />
                                                <label for="tel">[$Tel]*</label>
                                                <div class="underline"></div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="">
                                            <textarea  rows="7" cols="100" class="form-control send-msg-textarea" id="message" name="message" placeholder="[$message_content]" required><?= $message ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <center><div class="g-recaptcha" id="recaptcha1" required=""></div></center>
                                <br>
                                <input type="hidden" name="links" value="">
                                <div class="form-group">
                                    <center>
                                        <button type="submit" name="send" class="btn btn-default btn-sm">[$Send]</button>
                                        <a class="btn btn-default btn-sm" id="skip-msg">[$Close]</a>
                                    </center>
                                    <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; Ndodhi nje gabim. </p>
                                </div>
                            </form>
                        </div>
                    </fieldset>
                </section>

                <!--Render Msg-->

                <?php Messages::showErrors(); ?>
                <?php Messages::showInfoMsg(); ?>


                <?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
					?>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <?php $this->ComponentErrors->renderHTML(); ?>
                            <?php $this->ComponentMessages->renderHTML(); ?>
                        </div>
                    </div>
                <?php
				} ?>


            </div><!--***** Close the Content *****-->
        </div><!--***** Row *****-->
    </div><!--***** Content *****-->
</div><!--***** business_std_container *****-->



<!--***** Page Scroll Menu tab *****-->

<div id="nav-wrap" class="row">
    <div class="">
        <div id="scroller-anchor"></div>
        <nav class="main-nav">
            <ul>
                <?php if ($business->description != '') {
					?>
                    <li class="nav-btn active" data-row-id="row1">
                        <a href=""><i class="fa fa-home fa-icon" aria-hidden="true"></i><span class="fa-text">[$Description]</span></a>
                    </li>
                <?php
				} ?>

                <?php if (count($business->images) > 0) {
					?>
                    <li class="nav-btn" data-row-id="row2">
                        <a href=""><i class="fa fa-camera fa-icon" aria-hidden="true"></i><span class="fa-text">[$Fotogallery]</span></a>
                    </li>
                <?php
				} ?>

                <li class="nav-btn" data-row-id="row3">
                    <a href=""><i class="fa fa-star-o fa-icon" aria-hidden="true"></i><span class="fa-text">[$Rate]</span></a>
                </li>

                <?php if (isset($comments) && $comnum != 0) {
					?>
                    <li class="nav-btn" data-row-id="row4">
                        <a href=""><i class="fa fa-comments fa-icon" aria-hidden="true"></i><span class="fa-text">[$Reviews]</span></a>
                    </li>
                <?php
				} ?>

                <li class="nav-btn contact-open" data-row-id="row6">
                    <a href=""><i class="fa fa-envelope fa-icon contact-open" aria-hidden="true"></i><span class="fa-text">[$Contact]</span></a>
                </li>
            </ul><!--/.nav-collapse -->
<!--            <i class="active-marker"></i>-->
        </nav>
    </div>
</div>


<div id="content-wrap">

    <?php if ($business->description != '') {
					?>
        <div class="business_std_container row-nav" data-height="" id="row1" style="margin-top: 0px;" >
            <div class="content">
                <div class="row">
                    <div class="col-md-12 business-body alert-grey">

                        <!--***** Business Content BODY *****-->
                        <div class="row">
                            <div class="col-md-7">

                                <h4 class="page-header">[$Description]</h4>
                                <div class="  business-description no-margin-top">

                                    <div id="business-description" class="description-ajx" contenteditable="false">
                                        <?php
										if ($business->description != strip_tags($business->description)) {
											echo $business->description;
										} else {
											echo nl2br($business->description);
										} ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <h4 class="page-header">[$Fotogallery]</h4>
                                <?php if (count($business->images) > 0) {
											?>
                                <div class="slideshow">
                                    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0;width:550px;height:320px;overflow:hidden;visibility:hidden;">
                                        <!-- Loading Screen -->
                                        <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                                            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                                            <div style="position:absolute;display:block;background:url('http://netirane.al/data/netirane.al/media/slideshow/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                                        </div>
                                        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:550px;height:320px;overflow:hidden;">
                                            <?php if (count($business->images) > 0) {
												?>
                                                <?php foreach ($business->images as $img) {
													?>
                                                <div>
                                                    <a class="" href="<?= Utils::genThumbnailUrl('businesses/additional/' . $img->image, 1000, 0, array(), false, $image_baseurl) ?>" data-lightbox="image-1">
                                                        <img data-u="image" src="<?= Utils::genThumbnailUrl('businesses/additional/' . $img->image, 550, 320, array(), false, $image_baseurl) ?>" />
                                                    </a>
                                                </div>
                                                <?php
												} ?>
                                            <?php
											} ?>
                                        </div>
                                        <!-- Bullet Navigator -->
<!--                                        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                                             bullet navigator item prototype 
                                            <div data-u="prototype" style="width:16px;height:16px;"></div>
                                        </div>-->
                                        <!-- Arrow Navigator -->
                                        <span data-u="arrowleft" class="jssora12l" style="top:0px;left:0px;width:30px;height:46px;" data-autocenter="2"></span>
                                        <span data-u="arrowright" class="jssora12r" style="top:0px;right:0px;width:30px;height:46px;" data-autocenter="2"></span>
                                    </div>
                                </div>
                                <?php
										} else {
											?>
                                    <img src="/components/Businesses/views/img/add-photo-gallery.png" class="" style="width: 100%; padding: 2em 8em; opacity: 0.25;">    
                                <?php
										} ?>
                            </div>
                        </div>

                    </div><!--***** Close the Content *****-->
                </div><!--***** Row *****-->
            </div><!--***** Content *****-->
        </div><!--***** business_std_container *****-->

    <?php
				} ?>

    <?php if (isset($businesses_related)) {
					?>
        <div id="related_businesses">
            <div class="business_std_container" style="margin-top: 0px;" >
                <div class="content">
                    <div class="row">

                        <!--***** Business Related *****-->
                        <div class="col-md-12 business-body alert-grey">
                            <h4 class="page-header">[$similar_businesses]</h4>
                            <?php include 'inc/business-related.php'; ?>
                        </div>

                        <!--***** END Business Related *****-->

                    </div>
                </div>
            </div>
        </div>
    <?php
				} ?>


    <?php if (in_array($id_profile, array(3)) && ($offers_count != null)) {
					?>
        <div id="Business-Offers">
        <div class="business_std_container" style="margin-top: 0px;" >
            <div class="content">
                <div class="row">

                    <!--***** Business Offers *****-->
                    <div class="col-md-12 business-body alert-grey">
                        <style>
                            #Business-Offers .main-offer-info {background: #1f1f1f;padding: 10px 10px;}
                            #Business-Offers .business-offer-name {
                                font-size:12px;
                                margin-top: 0px;
                                margin-bottom: 0px;
                            }
                            #Business-Offers .business-offer-name a{
                                font-size: 10px;
                                line-height: 0;
                                letter-spacing: 1px;
                                color: #828282;
                                text-transform: uppercase;
                                text-align: left;
                            }
                            #Business-Offers .business-offer-title a{
                                color:white;
                            }
                            #Business-Offers .business-offer-title {
                                font-size:20px;
                                margin-top: 5px;
                                margin-bottom: 0px;
                                text-align: left;
                            }
                            #Business-Offers .btn-xs {
                                padding: 10px 10px;
                            }

                            #Business-Offers hr {
                                margin-top: 10px;
                                margin-bottom: 10px;
                            }
                            #Business-Offers .owl-theme .owl-controls .owl-page span {
                                background: #ffffff;
                            }
                            .offer-price {
                                margin-top: 7px;
                            }
                            .modal{
                                color:black;   
                            }
                            .modal-header {
                                padding-bottom: 5px;
                            }
                            .modal-footer {
                                padding: 0;
                            }
                            .modal-footer .btn-group button {
                                height:40px;
                                border-top-left-radius : 0;
                                border-top-right-radius : 0;
                                border: none;
                                border-right: 1px solid #ddd;
                            }
                            .modal-footer .btn-group:last-child > button {
                                border-right: 0;
                            }

                        </style>
                        <!-- Modal Offers Preview -->
                        <?php
						if (!is_null($offers)) {
							?>
                            <!-- Ofertat -->
                            <?php
							foreach ($offers as $offer) {
								?>
                                <div class="modal fade" id="<?= $offer->id ?>" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <!-- content goes here -->
                                                <form id="business-info-form" method="POST">
                                                    <div class="row">
                                                        <div class="preview col-md-6">

                                                        <div class="preview-pic tab-content">
                                                                <?php $first = true;
								foreach ($offer->images as $key => $img) {
									?>
                                                                    <div class="tab-pane <?= $first == true ? 'active' : '' ?>" id="pic-<?= $key ?>"><img src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 500, 300, array(), true, $image_baseurl) ?>" /></div>
                                                                <?php $first = false;
								} ?>
                                                            </div>
                                                            <ul class="preview-thumbnail nav nav-tabs">
                                                                <?php foreach ($offer->images as $key => $img) {
									?>
                                                                    <li class="<?= $key == 0 ? 'active' : '' ?>"><a data-target="#pic-<?= $key ?>" data-toggle="tab"><img src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 400, 400, array(), true, $image_baseurl) ?>" /></a></li>
                                                                <?php
								} ?>
                                                            </ul>

                                                        </div>

                                                        <div class="col-md-6">
                                                            <h3 class="modal-title" id="lineModalLabel"><?= $offer->title ?></h3>
                                                            <h1 class="business-offer-name">
                                                                <a href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($offer->business_name . '-' . $offer->id_biz)) ?>"><?= $offer->business_name ?></a>
                                                            </h1>
                                                            <hr>
                                                            <div class="offer-price" style="float:none";>
                                                                <?php if ($offer->final_price != '') {
									?>
                                                                    <div class="price"><?= $offer->price ?>&nbsp;<?= $offer->currency ?></div>
                                                                    <div class="final-price"><?= $offer->final_price ?>&nbsp;<?= $offer->currency ?></div>
                                                                <?php
								} else {
									?>
                                                                    <span class="only-price"><?= $offer->price ?>&nbsp;<?= $offer->currency ?></span>                                                   
                                                                <?php
								} ?>
                                                            </div>
                                                            <hr>
                                                            <h5 class="titlecatege"><?= strip_tags($offer->description) ?></h5>


                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                            <!--***** Send Request *****-->
                                            <section id="send-request-<?=$offer->id?>" style="display:none;">
                                                <fieldset>
                                                    <legend class="scheduler-border" id="row6">[$send_a_request]</legend>
                                                    <div class="col-md-10 col-md-offset-1">
                                                        <div id="reqRespond" ></div>
                                                        <form class="form-request" class="post-request" id="<?=$offer->id?>" action="" method="post">
                                                            <input type="hidden" name="title" value="<?=$offer->title?>" />
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="business-insertion">
                                                                        <fieldset>
                                                                            <input type="text" class="name required" id="name"  name="name-request" <?php if (isset($_SESSION['user_auth'])) {
									?> value="<?= $_SESSION['user_auth']['firstname'] . ' ' . $_SESSION['user_auth']['lastname'] ?>" <?php
								} ?> data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." required="" />
                                                                            <label for="name">[$Name]*</label>
                                                                            <div class="underline"></div>
                                                                        </fieldset>
                                                                    </div>
                                                                    <div class="business-insertion">
                                                                        <fieldset>
                                                                            <input type="email" class="email required" id="email"  name="email-request" value="<?= $email ?>" data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@email.com)" required="" />
                                                                            <label for="email">[$Email]*</label>
                                                                            <div class="underline"></div>
                                                                        </fieldset>
                                                                    </div>
                                                                    <div class="business-insertion">
                                                                        <fieldset>
                                                                            <input type="text" class="tel required" id="tel"  name="tel-request"  value="<?= $tel ?>" data-placement="top" data-trigger="manual" data-content="Must be a valid phone number" required="" />
                                                                            <label for="tel">[$Tel]*</label>
                                                                            <div class="underline"></div>
                                                                        </fieldset>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="">
                                                                        <textarea  rows="7" cols="100" class="form-control send-msg-textarea" id="message-request" name="message-request" ><?= $message ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <center><div class="g-recaptcha" id="recaptcha1" required=""></div></center>
                                                            <br>
                                                            <input type="hidden" name="request" value="1">
                                                            <div class="form-group">
                                                                <center>
                                                                    <button type="submit" name="send-request" id_biz="<?= $offer->id_biz ?>" class="btn btn-default btn-sm">[$Send]</button>
                                                                    <a class="btn btn-default btn-sm" id="close-request-<?=$offer->id?>">[$Close]</a>
                                                                </center>
                                                                <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; Ndodhi nje gabim. </p>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </fieldset>
                                            </section>
                                            <div class="modal-footer">
                                                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                                    <div class="btn-group" role="group">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Mbyll</button>
                                                    </div>
                                                    <div class="btn-group" role="group">
                                                        <button type="button" id="savedata" class="btn btn-default btn-hover-green request-<?=$offer->id?>" data-action="save" name="save" role="button">Request Offer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $(".request-<?=$offer->id?>").click(function () {
                                        $("#send-request-<?=$offer->id?>").slideToggle("slow");
                                    });
                                    $("#close-request-<?=$offer->id?>").click(function () {
                                        $("#send-request-<?=$offer->id?>").slideToggle("slow");
                                    });
                                </script>
                            <?php
							} ?>
                            <script>
                            $(document).ready(function () {

                                $('.form-request').on('submit',function (e) {
                                    // show that something is loading
                                    $('#reqRespond').html("<b>Loading response...</b>");
                                    $.ajax({
                                        type: 'POST',
                                        url: '<?= Utils::getComponentUrl('Businesses/ajx_offer_requestinfo/')?>' + this.id ,
                                        data: $(this).serialize()
                                    })
                                            .done(function (data) {
                                                // show the response
                                                $('#reqRespond').html(data);
                                            })
                                            .fail(function () {
                                                // just in case posting your form failed
                                                alert("Posting failed.");
                                            });
                                            e.preventDefault();
                                    // to prevent refreshing the whole page page
                                    return false;

                                });
                            });
                        </script>
                        <?php
						} ?>
                            <h4 class="page-header">OFERTAT E BIZNESIT</h4>

                            <div id="owl-offers" class="owl-carousel">
                                <?php if (!is_null($offers)) {
							?>
                                    <?php $taglia_offer_content = (isset($parameters['taglia_offer_content'])) ? $parameters['taglia_offer_content'] : 125; ?>
                                    <!-- Ofertat -->
                                    <?php foreach ($offers as $offer) {
								?>

                                        <div id="carousel-example-generic-<?= $offer->id ?>" class="carousel slide offers-carousel" data-ride="carousel" data-interval="false">
                                           
                                            <div class="carousel-inner">
                                                <?$first = true;
								if (count($offer->images)) {
									foreach ($offer->images as $img) {
										?>
                                                    <div class="item <?=($first)?'active':''; ?> srle">
                                                        <img src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 500, 300, array(), true, $image_baseurl) ?>" alt="" class="img-responsive">
                                                    </div>
                                                    <?php $first = false;
									} ?>
                                                <?
								} else {
									?>
                                                    <div class="item <?=($first)?'active':''; ?> srle">
                                                        <img src="<?= Utils::genThumbnailUrl('businesses/offers/default.jpg', 500, 300, array(), true) ?>" alt="" class="img-responsive">
                                                    </div>
                                                <?
								} ?>
                                            </div>
                                            <?if (count($offer->images) > 1) {
									?>
                                                <a class="left carousel-control" href="#carousel-example-generic-<?= $offer->id ?>" role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                                </a>
                                                <a class="right carousel-control" href="#carousel-example-generic-<?= $offer->id ?>" role="button" data-slide="next">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                            <?
								} ?>

                                            <ul class="thumbnails-carousel clearfix"></ul>

                                            <div class="alldiv-text">
                                                <p class="main-comune"></p>
                                                <h5 class="main-title1">
                                                    <a title=""><?= $offer->title ?></a>
                                                </h5>
                                                <p></p>
                                                <div class="location-height">
                                                    <p class="main-addres">
                                                        <?= StringUtils::CutString(strip_tags($offer->description), $taglia_offer_content) ?>
                                                    </p>
                                                </div>
                                                <div class="offer-price">
                                                    <?php if ($offer->final_price != '') {
									?>
                                                    <div class="price"><?= Utils::price_format($offer->price, $offer->currency) ?></div>
                                                        <div class="final-price"><?= Utils::price_format($offer->final_price, $offer->currency) ?></div>
                                                    <?php
								} else {
									?>
                                                        <span class="only-price"><div class="price"><?= Utils::price_format($offer->price, $offer->currency) ?></div></span>                                                   
                                                    <?php
								} ?>
                                                </div>
                                                <span style=" float: right; ">
                                                    <a class="btn btn-xs btn-default bussiness-comments" data-toggle="modal" data-target="#<?=$offer->id?>">Me Shume</a>
                                                </span>                                      
                                            </div>
                                        </div>
                                    <?php
							} ?>


                                    <script>
                                        // thumbnails.carousel.js jQuery plugin
                                        ;
                                        (function (window, $, undefined) {

                                            var conf = {
                                                center: true,
                                                backgroundControl: false
                                            };

                                            var cache = {
                                                $carouselContainer: $('.thumbnails-carousel').parent(),
                                                $thumbnailsLi: $('.thumbnails-carousel li'),
                                                $controls: $('.thumbnails-carousel').parent().find('.carousel-control')
                                            };

                                            function init() {
                                                cache.$carouselContainer.find('ol.carousel-indicators').addClass('indicators-fix');
                                                cache.$thumbnailsLi.first().addClass('active-thumbnail');

                                                if (!conf.backgroundControl) {
                                                    cache.$carouselContainer.find('.carousel-control').addClass('controls-background-reset');
                                                } else {
                                                    cache.$controls.height(cache.$carouselContainer.find('.carousel-inner').height());
                                                }

                                                if (conf.center) {
                                                    cache.$thumbnailsLi.wrapAll("<div class='center clearfix'></div>");
                                                }
                                            }

                                            function refreshOpacities(domEl) {
                                                cache.$thumbnailsLi.removeClass('active-thumbnail');
                                                cache.$thumbnailsLi.eq($(domEl).index()).addClass('active-thumbnail');
                                            }

                                            function bindUiActions() {
                                                cache.$carouselContainer.on('slide.bs.carousel', function (e) {
                                                    refreshOpacities(e.relatedTarget);
                                                });

                                                cache.$thumbnailsLi.click(function () {
                                                    cache.$carouselContainer.carousel($(this).index());
                                                });
                                            }

                                            $.fn.thumbnailsCarousel = function (options) {
                                                conf = $.extend(conf, options);
                                                init();
                                                bindUiActions();
                                                return this;
                                            }

                                        })(window, jQuery);

                                        $('.thumbnails-carousel').thumbnailsCarousel();
                                        
                                       
                                    </script>
                                <?php
						} ?>
                            </div>

                            <!-- Owl-Carousel-JavaScript -->
                            <script>
                                $(document).ready(function () {
                                    $("#owl-offers").owlCarousel({
                                        items: 2,
                                        lazyLoad: false,
                                        autoPlay: false,
                                        pagination: true,
                                        loop: false,
                                    });
                                });
                            </script>

                        </div>
                        <!--***** END Business Offers *****-->
                    </div>
                </div>
            </div>
        </div>
    <?php
				} ?>

    <?php if ($business->id == '1674') {
					?>
        <!-- Video -->
        <div class="business_std_container" style="margin-top: 0px;" >
            <div class="content">
                <div class="row">
                    <div class="col-md-12 business-body alert-grey">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <h4 class="page-header" style="text-transform: uppercase;">Video</h4>

                                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/t1qEb2ajqkk" frameborder="0" allowfullscreen></iframe>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
				} ?>
    <?php if ($business->id == '3897') {
					?>
        <!-- Panoramic Photo -->
<!--        <div class="business_std_container" style="margin-top: 0px;" >
            <div class="content">
                <div class="row">
                    <div class="col-md-12 business-body alert-grey">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <h4 class="page-header" style="text-transform: uppercase;">Panorama 360°</h4>
                                    <style>
                                        #panorama {
                                            width: 100%;
                                            height: 400px;
                                        }
                                    </style>
                                    <div id="panorama"></div>
                                    <script>
                                  
                                        pannellum.viewer('panorama', {
                                            "type": "equirectangular",
                                            "panorama": "http://netirane.al/data/netirane.al/media/businesses/panorama360/americanshop-min.jpg",
                                            "autoLoad": true
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    <?php
				} ?>


    <div class="row">
        <div class="col-md-8">

            <div class="business_std_container left-container row-nav" data-height="" id="row3" style="margin-top: 0px;" >
                <div class="content">
                    <div class="row">
                        <div class="col-md-12 business-body alert-grey">

                            <!--***** Rate It *****-->
                            <section id="rates">
                                <center>
                                    <h3 class="title">[$leave_your_review]</h3>
                                    <div style="cursor:pointer" class="rating-div">
                                        <i class="fa fa-star-o star-on-png fa-3x ratings_stars rating" id="star_1"></i>

                                        <i data-alt="2" class="fa fa-star-o star-on-png fa-3x ratings_stars rating" id="star_2"></i>

                                        <i data-alt="3" class="fa fa-star-o star-on-png fa-3x ratings_stars rating" id="star_3"></i>

                                        <i data-alt="4" class="fa fa-star-o star-on-png fa-3x ratings_stars rating" id="star_4"></i>

                                        <i data-alt="5" class="fa fa-star-o star-on-png fa-3x ratings_stars rating" id="star_5"></i>
                                    </div>
                                </center>
                                <center>
                                    <div id="average"></div>
                                </center>
                            </section>

                            <!--***** Reviews *****-->
                            <section id="reviews">
                                <fieldset id="fieldset-review">
                                    <legend class="scheduler-border">[$leave_a_review]</legend>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form id="r-form" name="r-form">
                                                <input type="hidden" name="id_biz" value="<?= $business->id ?>">
                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="business-insertion">
                                                            <fieldset>
                                                                <input type="text" class="name" id="r-name" name="name" <?php if (isset($_SESSION['user_auth'])) {
					?> value="<?= $_SESSION['user_auth']['firstname'] . ' ' . $_SESSION['user_auth']['lastname'] ?>" <?php
				} ?>  required="" />
                                                                <label for="r-name">[$name]</label>
                                                                <div class="underline"></div>
                                                            </fieldset>
                                                        </div>

                                                        <div class="business-insertion">
                                                            <fieldset>
                                                                <input type="email" class="email required" id="r-email"  name="email" <?php if (isset($_SESSION['user_auth'])) {
					?> value="<?= $_SESSION['user_auth']['email'] ?>" <?php
				} ?> required="" />
                                                                <label for="r-email">[$Email]</label>
                                                                <div class="underline"></div>
                                                            </fieldset>
                                                        </div>

                                                        <div class="business-insertion">
                                                            <fieldset>
                                                                <input type="text" class="tel required" id="r-tel" name="tel" required="" />
                                                                <label for="r-tel">[$Tel]*</label>
                                                                <div class="underline"></div>
                                                            </fieldset>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">
                                                        <textarea class="form-control send-msg-textarea"  id="r-comment" name="comment" placeholder="[$leave_your_comment]" rows="7" required=""></textarea>
                                                        <br>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <center>
                                                                <!--                                                                <div class="g-recaptcha" id="recaptcha2" required=""></div>-->
                                                            </center>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <center>
                                                            <input class="btn btn-default" name="s-review"  id="s-review" value="[$Send]" style="width: inherit;"/>

                                                            <a class="btn btn-default" id="skip-reviews" type="clear" >[$Cancel]</a> 
                                                        </center>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </fieldset>
                            </section>

                            <!--***** Reviews List *****-->
                            <section id="reviews-list">
                                <div class="row">
                                    <div class="col-md-12">

                                        <?php if (isset($comments) && $comnum != 0) {
					?>
                                            <a class="collapsed" role="button" data-toggle="collapse" href="#Reviewscollapse" aria-expanded="false" aria-controls="Reviewscollapse">
                                                <h4 id="row4" class="page-header" style="text-transform: uppercase;">[$Reviews]</h4>
                                            </a>

                                            <script type="application/ld+json">
                                                {
                                                "@context": "http://schema.org",
                                                "@type": "LocalBusiness",
                                                "image": "<?= 'http://' . $_SERVER['HTTP_HOST'] . Utils::genThumbnailUrl('businesses/' . $business->image, $w, $h, array('far' => '1', 'bg' => 'FFFFFF')) ?>",
                                                "name": "<?= addslashes($business->company_name) ?>",
                                                "address" : "<?= addslashes($business->address) ?>",
                                                "priceRange" : "medium",
                                                "url": "http://<?php print_r($business->subdomain->subdomain) ?>",
                                                "telephone" : "<?= $business->phone ?>",


                                                <?php
												if ($business->coordinates != '' && $business->coordinates != null) {
													$coordinates = explode(',', $business->coordinates);
													$lat = $coordinates[0];
													$long = $coordinates[1]; ?>     
                                                    "geo": {
                                                    "@type": "GeoCoordinates",
                                                    "latitude": <?= $lat ?>,
                                                    "longitude": <?= $long ?>
                                                    },
                                                <?php
												} ?>

                                                "aggregateRating": {
                                                "@type": "AggregateRating",
                                                "ratingValue": "<?= number_format($business->rating, 1); ?>",
                                                "reviewCount": "<?= $business->rating_count ?>"
                                                }
                                                }
                                            </script>



                                        <?php
				} ?>
                                        <div class="panel-collapse collapse in" id="Reviewscollapse" role="tabpanel" aria-labelledby="Reviewscollapse" aria-expanded="false">

                                            <?php
											foreach ($comments as $com) {
												if ($com['comment'] != '') {
													?>
                                                    <ul id="comments">
                                                        <li>
                                                            <div class="avatar-container">
                                                                <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar" alt="avatar">
                                                            </div>
                                                            <div class="post-container">
                                                                <div>
                                                                    <span class="author_name"><?= $com['name'] ?>
                                                                    </span></siv>

                                                                    <span class="bullet" aria-hidden="true">•</span>

                                                                    <span class="comment-stars">
                                                                        <?php
																		$i = $com['rating'];
													$j = 5 - $com['rating'];
													for ($u = 1; $u <= $i; $u++) {
														?>
                                                                            <i class="fa fa-star star-on-png "></i>
                                                                        <?php
													}
													for ($b = 1; $b <= $j; $b++) {
														?>

                                                                            <i class="fa fa-star-o star-on-png "></i>

                                                                        <?php
													} ?>
                                                                    </span>

                                                                    <p></p>

                                                                    <p><?= strip_tags($com['comment']) ?></p>

                                                                    <p></p>

                                                                    <span class="timeago">Postuar ne:  <span><?= date('d', strtotime($com['publish_date'])) ?>   [$<?= date('M', strtotime($com['publish_date'])) ?>] <?= date('Y', strtotime($com['publish_date'])) ?></span></span>

                                                                </div>
                                                            </div>

                                                        </li>

                                                    </ul>
                                                <?php
												} ?>
                                            <?php
											} ?>
                                        </div>
                                    </div>
                                </div>
                            </section>

                        </div><!--***** Close the Content *****-->
                    </div><!--***** Row *****-->
                </div><!--***** Content *****-->
            </div><!--***** business_std_container *****-->
        </div>
        <div class="col-md-4">
            <?php if (!is_null($openh) && count($openh)) {
												?>
                <div class="business_std_container right-container" style="margin-top: 0px;" >
                    <div class="content">
                        <div class="row">
                            <div class="col-md-12 business-body alert-grey">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-center">
                                            <a class="collapsed" role="button" data-toggle="collapse" href="#Timetablecollapse" aria-expanded="false" aria-controls="Timetablecollapse">
                                                <h4 class="page-header" style="text-transform: uppercase;">[$open_hours]</h4>
                                            </a>


                                            <div class="timetable panel-collapse collapse in" id="Timetablecollapse" role="tabpanel" aria-labelledby="Timetablecollapse" aria-expanded="false">
                                                <table class="flat-table">
                                                    <tbody>
                                                        <?php for ($day = 1; $day <= 7; $day++) {
													if ($day == 7) {
														$day = 0;
													} ?>
                                                             <tr class="<?= (date('w') == $day) ? 'today' : ''; ?><?php if ($openh[$day]->closed) {
														?> closed <?php
													} ?>">
                                                                <td class="day"><i class="fa fa-clock-o" aria-hidden="true"></i> <?= Utils::numbertoweekdate($openh[$day]->day, FCRequest::getLang()) ?></td>
                                                                <?php if (($openh[$day]->closed || $openh[$day]->hours24) != 1) {
														?>
                                                                <td class="hour">
                                                                    <?= date('H:i', strtotime($openh[$day]->open_start)) ?>
                                                                    <?php if ($openh[$day]->pause_start != null && $openh[$day]->pause_end != null) {
															echo ' - ' . date('H:i', strtotime($openh[$day]->pause_start));
															echo '&nbsp;&nbsp;&nbsp;';
															echo date('H:i', strtotime($openh[$day]->pause_end));
														} ?>
                                                                    <?= ' - ' . date('H:i', strtotime($openh[$day]->open_end)); ?></td>
                                                                <?php
													} else {
														?>
                                                                    <?php if ($openh[$day]->closed) {
															?>
                                                                        <td class="hour closed">[$closed]</td>
                                                                    <?php
														} else {
															?>
                                                                        <td class="hour">[$hours24]</td>
                                                                    <?php
														} ?>
                                                                <?php
													} ?>
                                                            </tr>
                                                        <?php  if ($day == 0) {
														break;
													}
												} ?>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
											} ?>
       
            <!-- Log In -->
            <div class="business_std_container right-container" style="margin-top: 0px;" >
                <div class="content">
                    <div class="row">
                        <div class="col-md-12 business-body alert-grey">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <h4 class="page-header" style="text-transform: uppercase;">[$login_to_your_business]</h4>

                                        <a href="/login"><button class="btn btn-sm btn-default">
                                                [$login]
                                            </button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->               
</div><!--***** content-wrap End *****-->
<!--<div class="g-recaptcha" data-theme="dark" data-sitekey="6LdTnwsUAAAAADyxbvJUb5yqashi7O82vVsA9S2d"required=""></div>-->

<script>
<?php if (isset(CMSSettings::$google_recaptcha_key) && CMSSettings::$google_recaptcha_key != '' && CMSSettings::$google_recaptcha_key != null) {
												?>
        var recaptcha1;
        var recaptcha2;
        var myCallBack = function () {
            //Render the recaptcha1 on the element with ID "recaptcha1"
            recaptcha1 = grecaptcha.render('recaptcha1', {
                'sitekey': '<?= CMSSettings::$google_recaptcha_key ?>', //Replace this with your Site key
                'theme': 'dark'
            });
            //
            //        //Render the recaptcha2 on the element with ID "recaptcha2"
            //        recaptcha2 = grecaptcha.render('recaptcha2', {
            //            'sitekey': '<?= CMSSettings::$google_recaptcha_key ?>', //Replace this with your Site key
            //            'theme': 'dark'
            //        });
        };

<?php
											} else {
												?>
        var recaptcha1;
        var recaptcha2;
        var myCallBack = function () {
            //Render the recaptcha1 on the element with ID "recaptcha1"
            recaptcha1 = grecaptcha.render('recaptcha1', {
                'sitekey': '6LdTnwsUAAAAADyxbvJUb5yqashi7O82vVsA9S2d', //Replace this with your Site key
                'theme': 'dark'
            });

            //        //Render the recaptcha2 on the element with ID "recaptcha2"
            //        recaptcha2 = grecaptcha.render('recaptcha2', {
            //            'sitekey': '6LdTnwsUAAAAADyxbvJUb5yqashi7O82vVsA9S2d', //Replace this with your Site key
            //            'theme': 'dark'
            //        });
        };


<?php
											} ?>

</script>




<!-- Rating system -->
<script>
    $(document).ready(function () {


        function checkFieldsIfEmpty() {
            var count = 0;

            if ($('#r-name').val() == '') {
                count++;
            }
            if ($('#r-email').val() == '') {
                count++;
            }
            if ($('#r-tel').val() == '') {
                count++;
            }
            if ($('#r-comment').val() == '') {
                count++;
            }
            return count;

        }


        function hideMessages() {
            setTimeout(function () {
                $("#msg-success").hide('slow');
            }, 3500);
            setTimeout(function () {
                $(".alert-danger").hide('slow');
            }, 3500);
        }

        $("#s-review").click(function () {

            if (checkFieldsIfEmpty() > 0) {
                $('#reviews').prepend('<p class="msg-success alert-danger alert-costumized" name="messageajx" >[$all_fields_are_required]</p>');
                hideMessages();

            } else {

                $.ajax({
                    type: "POST",
                    url: '<?= Utils::getComponentUrl('Businesses/ajx_reviewBusiness') ?>',
                    data: $("#r-form").serialize(),
                    success: function (data) {
                        console.log(data);
                        $('#reviews').append(data);
                        $("#fieldset-review").hide('slow');
                        hideMessages();
                        hideMessages();
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            }
        });
        setTimeout(function () {
            $(".alert-costumized").hide('slow');
        }, 3500);
        function addStatistic(id_business, type) {
            $("#reviews").hide();
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_addStatistic') ?>",
                data: {
                    'id_business': id_business,
                    'type': type,
                },
                success: function (data) {
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function rate(rating, id_biz) {
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_rateBusiness') ?>",
                data: {
                    'id_biz': id_biz,
                    'rating': rating
                },
                success: function (data) {
                    var arr = data.split(",");
                    console.log(data);
                    
                    var showError = document.getElementById('Error');
                    
                    if(arr[0] !== '' && showError === null){
                        $('#r-form').prepend('<div id="Error" class="alert alert-info today" style="background: lightseagreen;border-radius: 3px;">'+ arr[0] + '</div>');
                        
                        setTimeout(function(){
                            $('#Error').remove();
                          }, 5000);
                    }
                    $('#r-form').prepend('<input type="hidden" name="id" value="' + arr[0] + '" />');
                    $('#r-form').prepend('<input type="hidden" name="id_biz" value="' + arr[1] + '" />');
                    $('#r-form').prepend('<input type="hidden" name="rating" value="' + arr[2] + '" />');
                    //location.href = '<?= Utils::getComponentUrl('Businesses/review_business') ?>';
                    //success
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function controllCookie(id_biz) {

            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_checkRatingCookie') ?>",
                data: {
                    'id_biz': id_biz,
                },
                success: function (rating) {
                    //console.log(rating);
                    if (rating != 0) {
                        for (var i = 1; i <= rating; i++) {
                            $('#star_' + i).attr("class", "fa fa-star star-on-png fa-3x ratings_stars");
                        }
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function ratingDataText(id_biz) {
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValue') ?>",
                data: {
                    'id_biz': id_biz,
                },
                success: function (data) {
                    d = data.split("|");
                    $('#average').html("[$rating]: " + d[0] + " - [$rating_count]: " + d[1]);
                },
                error: function (e) {
                    $('#average').html("Pati probleme");
                    console.log(e);
                }
            });
        }

        function ratingDataTextShow(id_biz) {
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValue') ?>",
                data: {
                    'id_biz': id_biz,
                },
                success: function (data) {
                    d = data.split("|");
                    $('#showrate').html(d[0]);
                },
                error: function (e) {
                    $('#average').html("Pati probleme");
                    console.log(e);
                }
            });
        }

        function ratingDataStar(id_biz) {

            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValueBList') ?>",
                dataType: 'json',
                data: {
                    'id_biz': id_biz,
                },
                success: function (data) {

                    if (data.rate != 0) {
                        for (var i = 0; i <= data.rate; i++)
                            $('#star_' + i + '_' + id_biz).attr("class", "fa fa-star star-on-png");
                        if (data.half == 1)
                            $('#star_' + i + "_" + id_biz).attr("class", "fa fa-star-half-o star-on-png");
                    }

                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        // reviews toggle
        $("#skip-reviews").click(function () {
            $("#reviews").slideToggle("slow");
        });
        // message toggle

        $(".contact-open").click(function () {
            $("#send-msg").slideToggle("slow");
        });
        $(".request-open").click(function () {
            $("#send-request").slideToggle("slow");
        });
        $("#skip-request").click(function () {
            $("#send-request").slideToggle("slow");
        });
        $("#skip-msg").click(function () {
            $("#send-msg").slideToggle("slow");
        });
        $(document).ready(function () {
            //yje ne title mbushi
            ratingDataStar(<?= $business->id ?>);
            //jep mesataren e rating te bere
            ratingDataText(<?= $business->id ?>);
            //shfaq mesataren e rating
            ratingDataTextShow(<?= $business->id ?>);
            //kontroll cookie nqs ka votuar
            controllCookie(<?= $business->id ?>);
            addStatistic(<?= $business->id ?>, 'view');
            $('#website_link').click(function () {
                addStatistic(<?= $business->id ?>, 'website_click');
            });
            $('#facebook_link').click(function () {
                addStatistic(<?= $business->id ?>, 'facebook_click');
            });
            $('#linkedin_link').click(function () {
                addStatistic(<?= $business->id ?>, 'linkedin_click');
            });
            $('#instagram_link').click(function () {
                addStatistic(<?= $business->id ?>, 'instagram_click');
            });
//            var checkifclicked = false;
//            var countclicked = null;
//            //hover
//            $('.ratings_stars').hover(
//                //mouseover
//                function () {
//                        $(this).prevAll().andSelf().attr("class", "fa fa-star star-on-png  fa-3x ratings_stars"); 
//                },
//                //mouseout
//                        function () {
//                            if(checkifclicked === false){
//                                $(this).prevAll().andSelf().attr("class", "fa fa-star-o star-on-png fa-3x ratings_stars");
//                            } else {
//                                for (i = countclicked+1; i < 6; i++) {
//                                    $('#star_'+i).removeClass('fa-star');
//                                    $('#star_'+i).addClass('fa-star-o');
//                                  }
//                            }
//                        }
//                );
            //click
            $('.ratings_stars').click(
                function () {
                    //checkifclicked = true;
                    var rating = $(this).prevAll().andSelf().length;
                    var stars = $(this).parent().children('i');
                    //countclicked = rating;
                    for (i = 0; i < rating+1; i++) {
                      $('#star_'+i).removeClass('fa-star-o');
                      $('#star_'+i).addClass('fa fa-star star-on-png  fa-3x ratings_stars');
                    }
                    for (i = rating+1; i < 6; i++) {
                      $('#star_'+i).removeClass('fa-star');
                      $('#star_'+i).addClass('fa-star-o');
                    }
                    
                    $("#reviews").show("slow");
                    
                    rate(rating,<?= $business->id ?>);
                    //$('.ratings_stars').bind();
                    ratingDataText(<?= $business->id ?>);
                    ratingDataStar(<?= $business->id ?>);
                }
            );
        });

            });
</script>






<!--// ************ Fixed Nav ***********-->
<script>
    jQuery(document).ready(function ($) {

        $(window).scroll(function () {
            header = $('#nav-wrap');
            if ($(window).width() < 991)
            {
                var scrollTop = 0;
                if ($(window).scrollTop() >= scrollTop) {
                    header.addClass('nav-shrink')
                    $('#nav-wrap').css({
                        position: 'fixed',
                        bottom: '0px'
                    });
                }
            } else if ($(window).width() > 991) {
                var scrollTop = 500;
                if ($(window).scrollTop() >= scrollTop) {
                    header.addClass('nav-shrink');
                    $('#nav-wrap').css({
                        position: 'fixed',
                        top: '61px'
                    });
                }
            }

            if ($(window).scrollTop() < scrollTop) {
                header.removeClass('nav-shrink');
                $('#nav-wrap').removeAttr('style');
            }
        })

    })
</script>


<!--//************** Menu Active ****************-->
<script>
    $(document).ready(function () {
        function moveMarker() {
            var activeNav = $('.active a');
            var activewidth = $(activeNav).width();
            var activePadLeft = parseFloat($(activeNav).css('padding-left'));
            var activePadRight = parseFloat($(activeNav).css('padding-right'));
            var totalWidth = activewidth + activePadLeft + activePadRight;
            var precedingAnchorWidth = anchorWidthCounter();
            // TODO: 
            // Find the total widths of all of the anchors
            // to the left of the active anchor.

            var activeMarker = $('.active-marker');
            $(activeMarker).css('display', 'block');
            $(activeMarker).css('width', totalWidth);
            $(activeMarker).css('left', precedingAnchorWidth);
            // TODO: 
            // Using the calculated total widths of preceding anchors,
            // Set the left: css value to that number.
        }
        moveMarker();
        function anchorWidthCounter() {
            var anchorWidths = 0;
            var a;
            var aWidth;
            var aPadLeft;
            var aPadRight;
            var aTotalWidth;
            $('.main-nav li').each(function (index, elem) {
                var activeTest = $(elem).hasClass('active');
                if (activeTest) {
                    // Break out of the each function.
                    return false;
                }

                a = $(elem).find('a');
                aWidth = a.width();
                aPadLeft = parseFloat(a.css('padding-left'));
                aPadRight = parseFloat(a.css('padding-right'));
                aTotalWidth = aWidth + aPadLeft + aPadRight;
                anchorWidths = anchorWidths + aTotalWidth;
            });
            return anchorWidths;
        }

        $('.main-nav a').click(function (e) {
            e.preventDefault();
            $('.main-nav li').removeClass('active');
            $(this).parents('li').addClass('active');
            moveMarker();
        });
    });</script>


<!--//************** Owl-Carousel-PhotoGallery ****************-->
<script>

    //*********Scroll Page Menu tab ***********//
    /******************************
     ADD ANIMATION TO THE TEXT 
     ******************************/
    var count = 0;
    var number = 10;
    /*************************
     ON WINDOW SCROLL FUNCTION
     *************************/
    var sectionIds = {};
    $(".row-nav").each(function () {
        var $this = $(this);
        sectionIds[$this.attr("id")] = $this.first().offset().top - 120;
    });
    var count2 = 0;
    $(window).scroll(function (event) {

        var scrolled = $(this).scrollTop();
        //If it reaches the top of the row, add an active class to it
        $(".row-nav").each(function () {

            var $this = $(this);
            if (scrolled >= $this.first().offset().top - 120) {
                $(".row-nav").removeClass("active");
                $this.addClass("active");
                $(".animation").removeClass('animationActive');
                $this.find(".animation").addClass('animationActive');
            }
        });
        //when reaches the row, also add a class to the navigation
        for (key in sectionIds) {
            if (scrolled >= sectionIds[key]) {
                $(".nav-btn").removeClass("active");
                var c = $("[data-row-id=" + key + "]");
                c.addClass("active");
                var i = c.index();
                $('#nav-indicator').css('left', i * 100 + 'px');
            }
        }


        //Check if we've reached the top
        if (scrolled > count2) {
            count2++;
        } else {
            count2--;
        }

        count2 = scrolled;
        if (count2 == 0) {
            $('h1 ,h2').addClass('animationActive');
        } else {
            $('h1 ,h2').removeClass('animationActive');
        }

    });
    /**************
     IN-NAVIGATION
     **************/
    $(".nav-btn").click(function () {
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
        var i = $(this).index();
        $('#nav-indicator').css('left', i * 100 + 'px');
        var name = $(this).attr("data-row-id");
        var id = "#" + name;
        var top = $(id).first().offset().top - 150;
        $('html, body').animate({scrollTop: top + 'px'}, 300);
    });
</script>

<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });

        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });

    }).call(this);



    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>
