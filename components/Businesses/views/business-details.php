<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/businesses-style.css'); ?>
<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php HeadHTML::addMetaProperty('og:title', $business->company_name); ?>
<?php HeadHTML::addMetaProperty('og:type', 'website'); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl("businesses/{$business->id_category}/" . urlencode($business->image), 900, 0, array(), true)); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($business->description), 200)); ?>
<div class="row business">
    <div class="col-md-10 col-md-offset-1">
        <div class="thumbnail">
            <h3 class="business-title text-center page-header"><?php if ($business->image != '') {
	?><img
                        class="logo img-rounded"
                        src="{MEDIA_PATH}businesses/<?= $business->id_category ?>/<?= $business->image ?>"
                        alt="<?= $business->image ?>"/>&nbsp;&nbsp;<?php
} ?><?= $business->company_name ?></h3>
            <div class="caption">
                <div class="business-description"><?= $business->description ?></div>

                <div class="row business-additional-images">
                    <?php foreach ($business->images as $img) {
		?>

                        <div class="col-sm-4 col-md-3 col-xs-6 thumb">
                            <a class="thumbnail" href="{MEDIA_PATH}businesses/additional/<?= $img->filename ?>"
                               data-toggle="lightbox" data-gallery="multiimages" data-title="">
                                <img class="img-responsive"
                                     src="{MEDIA_PATH}businesses/<?= $business->id_category ?>/additional/<?= $img->filename ?>"/>
                            </a>
                        </div>
                    <?php
	} ?>
                </div>
                <div class="text-center well">
                    <p><i class="fa fa-map-marker main-color"></i><?= $business->address ?></p>
                    <?php if ($business->phone != '') {
		?> <p>
                        <i class="fa fa-phone main-color"></i>Tel:&nbsp;&nbsp;<u><?= $business->phone ?></u>
                        </p> <?php
	} ?>
                    <?php if ($business->mobile != '') {
		?> <p>
                        <i class="fa fa-mobile-phone main-color"></i>Cellulare:&nbsp;&nbsp;<u><?= $business->mobile ?></u>
                        </p> <?php
	} ?>
                    <?php if ($business->email != '') {
		?> <p>
                        <i class="fa fa-envelope main-color"></i>E-mail:&nbsp;&nbsp;<?= $business->email ?></p> <?php
	} ?>
                    <?php if ($business->website != '') {
		?> <p>
                        <i class="fa fa-desktop main-color"></i>Website:&nbsp;&nbsp;<a target="_blank" href="http://<?= $business->website ?>/"><?= $business->website ?></a>
                        </p> <?php
	} ?>
                    <?php if ($business->facebook != '') {
		?> <p>Facebook:<a href="<?= $business->facebook ?>"
                                       target="_blank"><i
                                    class="fa fa-facebook"></i><?= $business->company_name ?></a></p> <?php
	} ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-90682060-1', 'auto');
    ga('send', 'pageview');

</script>