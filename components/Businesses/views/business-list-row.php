<?php

$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';
		
if (isset($category)) {
	$keywords = $category->category . ' , ' . $category->category . ' ' . CMSSettings::$website_title . ' ' . $category->description . ' , ';
	foreach ($businesses as $business) {
		$keywords .= $business->company_name . ' , ';
	}

	HeadHTML::addMeta('keywords', $keywords);
	HeadHTML::addMeta('description', $category->category . ' ' . CMSSettings::$website_title . ' ' . $category->description);
} elseif (isset($_GET['query'])) {
	HeadHTML::addMeta('keywords', 'Kerko ne tirane per:' . $_GET['query']);
	HeadHTML::addMeta('description', 'Kerko ne tirane per:' . $_GET['query']);
} else {
	HeadHTML::addMeta('keywords', 'biznes,tirane,dyqan,hotel,restorant,market,bar,palester,kerkim');
	HeadHTML::addMeta('description', 'Motori i kerkimit te bizneseve ' . ' ' . CMSSettings::$website_title);
}
?>


<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/business-list.css');
HeadHTML::AddStylesheet('bootstrap-select.min.css');
HeadHTML::AddJS('noframework.waypoints.min.js');
HeadHTML::AddJS('bootstrap-select.min.js');

require_once LIBS_PATH . 'Pager.php';
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 50;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 180;
$w = (isset($parameters['w'])) ? $parameters['w'] : 401;
$h = (isset($parameters['h'])) ? $parameters['h'] : 301;
?>


<script>
    jQuery(document).ready(function () {
        jQuery('#hideshow').click(function (event) {
            if ($("#map").is(":visible")) {
                jQuery('#map').toggle('show');
                $("#hideshow").text("Shfaq Harten");
            } else if (!$("#map").is(":visible")) {
                jQuery('#map').toggle('show');
                $("#hideshow").text("Mbyll Harten");
            }
        });
        controllMap();
    });
//        function controllMap() {
//            if ($(window).width() < 500) {
//                $('#map').hide();
//            }
//        }
</script>
<script>


    function addStatistic(id_business, type) {

        $.ajax({
            type: "POST",
            url: "<?= Utils::getComponentUrl('Businesses/ajx_addStatistic') ?>",
            data: {
                'id_business': id_business,
                'type': type,
            },
            success: function (data) {
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function ratingData(id_biz) {

        $.ajax({
            type: "POST",
            url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValueBList') ?>",
            dataType: 'json',
            data: {
                'id_biz': id_biz,
            },
            success: function (data) {

                if (data.rate != 0) {
                    for (var i = 0; i <= data.rate; i++)
                        $('#star_' + i + '_' + id_biz).attr("class", "fa fa-star star-on-png");
                    if (data.half == 1)
                        $('#star_' + i + "_" + id_biz).attr("class", "fa fa-star-half-o star-on-png");
                }

            },
            error: function (e) {
                console.log(e);
            }
        });
        return false;
    }



</script>
<!--<button class="btn btn-sm btn-danger" id='hideshow' >Shfaq Harten</button>-->
<div class="row businesses">
    <div class="col-md-12">

        <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
	?>

            <!----- Insert New Business ----->
            <div id="add-business">
                <a href="<?= Utils::getComponentUrl('Businesses/insert_business/') ?>">
                    <button class="btn btn-block alert alert-grey alert-business">
                        <div class="row">
                            <div class="col-md-12">
                                <i class="fa fa-plus-circle"></i>
                                [$InsertBusiness]
                            </div>
                        </div>
                    </button>
                </a>
            </div>

        <?php
} ?> 

        <!----- Search result alert ----->
        <div class="row">
            <div class="col-md-12">
                <div id="search-result">
                    <div class="alert alert-grey">

                        <div class="row">
                            <div class="col-md-9" style="line-height: 30px">
                                <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
		?>    

                                    <h1 class="list-header-logged">

                                        Lista juaj e bizneseve 
                                        <span class="result"></span>
                                    </h1>

                                <?php
	} ?>

                                <?php if (isset($_GET['query']) && $_GET['query'] != '') {
		?>
                                    <i class="fa fa-search" aria-hidden="true">&nbsp&nbsp</i>[$result_for]: <span
                                        class="result"><?= $_GET['query'] ?></span>
                                    <?php
	} ?>

                                <?php if (isset($category)) {
		?>
                                    <i class="fa fa-search" aria-hidden="true">&nbsp&nbsp</i>[$result_for]: <span
                                        class="result"><?= $category->category ?></span>
                                    <?php
	} ?>

                            </div>
                            <div class="col-md-3">
                                <?php
								$order = isset($_GET['order']) ? $_GET['order'] : 'relevance desc';
								?>

                                <form method="post">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="fa fa-sort"></span>
                                        </div>
                                        <select class="form-control input-sm" id="order-select"
                                                onchange="this.form.submit();" name="order">
                                            <option
                                                value="company_name asc" <?= ($order == 'company_name asc') ? 'selected' : '' ?>
                                                data-icon="fa fa-sort-alpha-asc">&nbsp;[$comp_name]
                                            </option>
                                            <option
                                                value="company_name desc" <?= ($order == 'company_name desc') ? 'selected' : '' ?>
                                                data-icon="fa fa-sort-alpha-desc">&nbsp;[$comp_name]
                                            </option>
                                            <option
                                                value="relevance desc" <?= ($order == 'relevance desc') ? 'selected' : '' ?>
                                                data-icon="fa fa-sort-amount-desc">&nbsp;[$relevance]
                                            </option>
                                            <option
                                                value="relevance asc" <?= ($order == 'relevance asc') ? 'selected' : '' ?>
                                                data-icon="fa fa-sort-amount-asc">&nbsp;[$relevance]
                                            </option>
                                            <option value="rating desc" <?= ($order == 'rating desc') ? 'selected' : '' ?>
                                                    data-icon="fa fa-sort-amount-desc">&nbsp;[$rating]
                                            </option>
                                            <option value="rating asc" <?= ($order == 'rating asc') ? 'selected' : '' ?>
                                                    data-icon="fa fa-sort-amount-asc">&nbsp;[$rating]
                                            </option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--****************************************************************************************************************-->


        <!--****************************************************************************************************************-->




        <?php
		$i = 1;
		$showedOtherLabel = false;
		$existPaid = false;
		if (count($businesses)) {
			foreach ($businesses as $business) {
				?>

                <?php if (!$showedOtherLabel && $business->payment == 0 && $existPaid) {
					?>

                    <!--                    <div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-grey text-center" style="background-color: rgba(255,255,255,.15)">
                                                    [$OtherResults]
                                                </div>
                                            </div>
                                        </div>-->

                    <!-- Banner On Body -->

                    <div id="pos-banner" style="margin-bottom:20px;">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
								$array = array();
					$array['id_category'] = 6;
					$array['view'] = 'banner-single';
					$array['limit'] = 1;
					$array['w'] = 1800;
					$array['h'] = 300;

					$banner = Loader::loadModule('Banners');
					$banner->execute($array); ?> 
                            </div>
                        </div>
                    </div>


                    <?php
					$showedOtherLabel = true;
				} elseif ($business->payment == 1) {
					$existPaid = true;
				} ?>

                <!-- box listing -->
                <div class="block-section-sm box-list-area" id="businessDiv_<?= $business->id ?>">
                    <!-- item list -->
                    <div class="box-list">
                        <div class="item">

                            <div class="ribbon-listing">
                                <span class="ui teal left ribbon label"><?= $i ?></span>
                            </div>

                            <div class="row">


                                <?php if (in_array($business->id_profile, array(1, 4))) {
					?>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                    <?php
				} else {
					?>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                        <?php
				} ?>
                                        <?php if ($business->featured) {
					?>
                                            <div class="ribbon-wrapper-green ribbon-mobile">
                                                <div class="ribbon-green">PREMIUM</div>
                                            </div>
                                        <?php
				} ?>
                                        <div class="bus-pic">
                                            <div class="img-item">
                                                <a href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business->company_name . '-' . $business->id)) ?>"
                                                   class="title_target">
                                                       <?php if ($business->image != '') {
					?>
                                                        <img class="little-logo"
                                                             src="<?= Utils::genThumbnailUrl('businesses/' . $business->image, $w, $h, array('zc' => '1'), false, $image_baseurl) ?>"
                                                             style="width:100%">
                                                         <?php
				} else {
					?><img class="little-logo"
                                                             src="<?= WEBROOT . $this->view_path ?>img/default.jpg"
                                                             style="width:100%">
                                                         <?php
				} ?>
                                                </a>

                                            </div>
                                        </div>
                                    </div>


                                    <?php if (in_array($business->id_profile, array(1, 4))) {
					?>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                        <?php
				} else {
					?>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                            <?php
				} ?>
                                            <div class="business-info">

                                                <?php if ($business->featured) {
					?>
                                                    <div class="ribbon-wrapper-green ribbon-desktop">
                                                        <div class="ribbon-green">PREMIUM</div>
                                                    </div>
                                                <?php
				} ?>

                                                <div class="info">
                                                    <h3 class="main-business-title no-margin-top">

                                                        <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '' && $_SESSION['user_auth']['id'] == $business->id_user) {
					?>
                                                            <a href="<?= Utils::getComponentUrl('Businesses/business_edit/' . $business->id) ?>"
                                                        <?php
				} else {
					?> 

                                                               <a href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business->company_name) . '-' . $business->id) ?>"   

                                                           <?php
				} ?>
                                                           class="title_target">

                                                                <?= StringUtils::CutString(strip_tags($business->company_name), $taglia_content) ?>
                                                            </a>
                                                    </h3>
                                                </div>

                                                <div class="info-1">
                                                    <a href="<?= Utils::getComponentUrl('Businesses/b_list') . '/' . Utils::url_slug($business->category) . "-{$business->id_category}"; ?>">
                                                        <?= $business->category ?>
                                                    </a>
                                                </div>
                                                <?php if (in_array($business->id_profile, array(1, 4))) {
					?>
                                                    <div class="business-content-free">
                                                    <?php
				} else {
					?>
                                                        <div class="business-content">
                                                        <?php
				} ?>

                                                        <?= StringUtils::CutString(strip_tags($business->description), $taglia_content) ?>
                                                    </div>

                                                    <?php if (in_array($business->id_profile, array(1, 4))) {
					?>
                                                        <div class="info-2-free">
                                                        <?php
				} else {
					?>
                                                            <div class="info-2">
                                                            <?php
				} ?>

                                                            <?php if (in_array($business->id_profile, array(1, 4))) {
					?>
                                                                <span class="main-fax"><?= $business->address ?></span>
                                                            <?php
				} else {
					?>
                                                                <span class="main-fax"><?= $business->address ?></span>
                                                                <br/>
                                                                <?php
//                                                                if (substr($business->phone, 0, 1) === '0')
//                                                                    $business->phone = '+355 (0) ' . substr($business->phone, 1);
																if ($business->phone != '') {
																	?>
                                                                    <span class="main-tel"> <?= $business->phone ?></span>
                                                                <?php
																} elseif ($business->mobile != '') {
																	?>
                                                                    <span class="main-tel"> <?= $business->mobile ?></span>
                                                                <?php
																} ?>

                                                            <?php
				} ?>
                                                        </div>

                                                        <div class="info-3">
                                                            <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '' && $_SESSION['user_auth']['id'] === $business->id_user) {
					?>    

                                                                <style>
                                                                    .box-list .main-email a, .box-list .main-url a, .box-list .main-subdomain a, .box-list .main-nverde a, .social-share a {
                                                                        font-size: 14px;
                                                                        background: #2e2e2e;
                                                                        padding: 5px;
                                                                    }
                                                                </style>

                                                                <span class="main-url" style="">
                                                                    <a id="facebook_link" href="<?= Utils::getComponentUrl('Businesses/business_edit/' . $business->id) ?>">
                                                                        <span>[$Edit]</span>
                                                                    </a>
                                                                </span>
                                                                <span class="main-url" style="">
                                                                    <a id="facebook_link" href="<?= Utils::getComponentUrl('Businesses/messages_list') . '/' . $business->id ?>">
                                                                        <span>[$Messages]</span>
                                                                    </a>
                                                                </span>
                                                                <span class="main-url" style="">
                                                                    <a id="facebook_link" href="<?= Utils::getComponentUrl('Businesses/myBizReviews') . '/' . $business->id ?>">
                                                                        <span>[$Reviews]</span>
                                                                    </a>
                                                                </span>
                                                                <span class="main-url" style="">
                                                                    <a id="facebook_link" href="<?= Utils::getComponentUrl('Businesses/edit_your_web') . '/' . $business->id ?>">
                                                                        <span>[$Edityourweb]</span>
                                                                    </a>
                                                                </span>
                                                                
                                                                <span class="main-url" style="float:right;">
                                                                    <a id="facebook_link" href="<?= Utils::getComponentUrl('Businesses/messages_list') . '/' . $business->id ?>">
                                                                        <span>[$Offers]</span>
                                                                    </a>
                                                                </span>


                                                            <?php
				} else {
					?>   

                                                                <?php if (in_array($business->id_profile, array(2, 3, 5))) {
						?>
                                                                    <?php
																	if ($business->facebook != '') {
																		if (strpos($business->facebook, 'https://') !== false) {
																			$facebook = substr($business->facebook, 8);
																		}
																		if (strpos($business->facebook, 'http://') !== false) {
																			$facebook = substr($business->facebook, 7);
																		} ?>
                                                                        <span class="main-url" style=" "><a id="facebook_link" onclick="addStatistic(<?= $business->id ?>, 'facebook_click')" href="https://<?= $facebook ?>" target="_blank">
                                                                                <i class="fa fa-facebook main-color"></i>
                                                                            </a>
                                                                        </span>

                                                                    <?php
																	} ?>

                                                                    <?php
																	if ($business->instagram != '') {
																		if (strpos($business->instagram, 'https://') !== false) {
																			$instagram = substr($business->instagram, 8);
																		}
																		if (strpos($business->instagram, 'http://') !== false) {
																			$instagram = substr($business->instagram, 7);
																		} ?>
                                                                        <span class="main-url" style=""><a id="instagram_link"
                                                                                                           onclick="addStatistic(<?= $business->id ?>, 'instagram_click')"
                                                                                                           href="https://<?= $instagram ?>"
                                                                                                           target="_blank">
                                                                                <i class="fa fa-instagram main-color"></i>
                                                                            </a>
                                                                        </span>

                                                                    <?php
																	} ?>

                                                                    <?php
																	if ($business->linkedin != '') {
																		if (strpos($business->linkedin, 'https://') !== false) {
																			$linkedin = substr($business->linkedin, 8);
																		}
																		if (strpos($business->linkedin, 'http://') !== false) {
																			$linkedin = substr($business->linkedin, 7);
																		} ?>
                                                                        <span class="main-url" style="">
                                                                            <a id="linkedin_link"
                                                                               onclick="addStatistic(<?= $business->id ?>, 'linkedin_click')"
                                                                               href="https://<?= $linkedin ?>"
                                                                               target="_blank">
                                                                                <i class="fa fa-linkedin main-color"></i>
                                                                            </a>
                                                                        </span>

                                                                    <?php
																	} ?>


                                                                    <?php
																	if ($business->website != '') {
																		$website = $business->website;
																		if (strpos($business->website, 'http://') !== false) {
																			$website = substr($business->website, 7);
																		} ?>
                                                                        <span class="main-url">
                                                                            <a id="website_link"
                                                                               onclick="addStatistic(<?= $business->id ?>, 'website_click')"
                                                                               href="http://<?= $website ?>"
                                                                               target="_blank"><i
                                                                                    class="fa fa-globe main-color"></i>
                                                                            </a>
                                                                        </span>
                                                                    <?php
																	} ?>

                                                                    <?php
																	if (!is_null($business->subdomain)) {
																		if (strpos($business->subdomain->subdomain, 'http://') !== false) {
																			$sub_url = $business->subdomain->subdomain;
																		} else {
																			$sub_url = 'http://' . $business->subdomain->subdomain;
																		} ?>
                                                                        <span class="main-url">
                                                                            <a href="<?= $sub_url ?>"
                                                                               target="_blank"><i
                                                                                    class="fa fa-share main-color"></i>
                                                                            </a>
                                                                        </span>
                                                                    <?php
																	} ?>








                                                                    <?php if (in_array($business->id_profile, array(2, 3, 5))) {
																		?>
                                                                        <div class="rate-it rate-cus">
                                                                            <i class="fa fa-star-o star-on-png"
                                                                               id="star_1_<?= $business->id ?>"></i>

                                                                            <i data-alt="2" class="fa fa-star-o star-on-png"
                                                                               id="star_2_<?= $business->id ?>"></i>

                                                                            <i data-alt="3" class="fa fa-star-o star-on-png"
                                                                               id="star_3_<?= $business->id ?>"></i>

                                                                            <i data-alt="4" class="fa fa-star-o star-on-png"
                                                                               id="star_4_<?= $business->id ?>"></i>

                                                                            <i data-alt="5" class="fa fa-star-o star-on-png"
                                                                               id="star_5_<?= $business->id ?>"></i>
                                                                        </div>
                                                                        <script>ratingData(<?= $business->id ?>);</script>

                                                                    <?php
																	} ?>

                                                                <?php
					} ?>
                                                            <?php
				} ?>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end item list -->
                                </div>
                                <!-- end box listing -->


                                <script>


                                    var businessDiv_<?= $business->id ?> = false;
                                    var waypoint = new Waypoint({
                                        element: document.getElementById('businessDiv_<?= $business->id ?>'),
                                        handler: function () {
                                            if (businessDiv_<?= $business->id ?> == false) {
                                                addStatistic(<?= $business->id ?>, 'impression');
                                                businessDiv_<?= $business->id ?> = true;
                                            }
                                        },
                                        offset: '50%'
                                    })


                                </script>

                                <?php
								$i++;
			}
		} else {
			?>

                            <?php if (isset($_GET['query']) && $_GET['query'] != '') {
				?>
                                <!----- No results alert ----->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-warning" role="alert">
                                            <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                                            [$no_search_results]<a href="/shto-biznes">[$here]</a>
                                        </div>
                                    </div>
                                </div>
                            <?php
			} else {
				?>
                                <!----- No results alert ----->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-warning" role="alert">
                                            <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                                            [$no_results]<a href="/shto-biznes">[$here]</a>
                                        </div>
                                    </div>
                                </div>
                                <?php
			}
		}
						?>
                        <!----- Paggination ----->
                        <div class="col-md-12 pagination">
                            <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
                        </div>
                    </div>

                </div>

                <script>
                    $('#order-select').selectpicker({
                        style: 'btn-default btn-sm'
                    });
                </script>
