<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/businesses-style.css'); ?>
    <div class="row businesses">
        <?php
		$i = 0;
		if (count($businesses)) {
			foreach ($businesses as $business) {
				if ($i % 3 == 0 && $i != 0) {
					?>
                    <div class="clearfix"></div>
                <?php
				} ?>
                <div class="col-sm-6 col-md-4 business">
                    <div class="thumbnail" style="">
                        <?php if ($business['logo'] != '') {
					?>
                            <div class="logo-div">
                                <a href="<?= Utils::getComponentUrl('Businesses/business_details/' . Utils::url_slug($business['company_name'] . '-' . $business['id'])) ?>" class="title_target">
                                    <img class="little-logo" src="{MEDIA_PATH}businesses/<?= $business['logo'] ?>" alt="<?= $business['logo'] ?>"/>
                                </a>
                            </div>
                        <?php
				} ?>
                        <a href="<?= Utils::getComponentUrl('Businesses/business_details/' . Utils::url_slug($business['company_name'] . '-' . $business['id'])) ?>" class="title_target">
                            <h4 class="business-title page-header"><?= $business['company_name'] ?></h4>
                        </a>
                        <div class="caption">
                            <?php if ($business['address'] != '') {
					?> <p><i class="fa fa-map-marker main-color"></i> <?= $business['address'] ?></p><?php
				} ?>
                            <?php if ($business['phone'] != '') {
					?> <p><i class="fa fa-phone main-color"></i> Tel:&nbsp;&nbsp;<?= $business['phone'] ?></p> <?php
				} ?>
                            <?php if ($business['mobile'] != '') {
					?> <p><i class="fa fa-mobile-phone main-color"></i> Cellulare:&nbsp;&nbsp;<?= $business['mobile'] ?></p> <?php
				} ?>
                            <?php if ($business['email'] != '') {
					?> <p><i class="fa fa-envelope main-color"></i> E-mail:&nbsp;&nbsp;<?= $business['email'] ?></p> <?php
				} ?>
                        </div>
                    </div>
                </div>

                <?php
				$i++;
			}
		} else {
			?>

            <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                Al momento questa categoria non contiene elementi.
            </div>
        <?php
		}
		?>
    </div>
