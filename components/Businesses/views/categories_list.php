<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/business-categories.css'); ?>

<?php
$keywords = ' ';
foreach ($categories as $categ) {
	$keywords .= $categ->category . ' , ';
	$keywords .= $categ->category . ' ' . CMSSettings::$website_title . ' ';
}
?>

<?php HeadHTML::addMeta('keywords', $keywords); ?>
<?php HeadHTML::addMeta('description', 'Lista e kategorive te bizneseve ' . CMSSettings::$website_title . ' ') ?>

<?php
HeadHTML::AddJS('list.js');

/*
 * Variables:
 * $categories => Array of Categories
 */
?>

<div id="categories">

    <div class="category-list">

        <div class="col-md-12 alert alert-grey">
            <div class="row">
                <div class="col-md-3">
                    <h2 style="margin: 0;">[$CategoriesList]</h2>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control search" id="search" placeholder="[$SearchCategory]">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="all-categories">
                <ul class="list">
                    <?php foreach ($categories as $categ) {
	?>

                        <li class="col-md-3 col-sm-3 col-xs-12 categories-list">
                            <a class="ln-black single-category name" href="<?= Utils::getComponentUrl('Businesses/b_list/' . Utils::url_slug($categ->category . "-{$categ->id}")) ?>">
                                <span><?= $categ->category ?></span>
                            </a>
                            <span class="number-categories"><?= $categ->total_businesses ?></span>
                        </li>

                    <?php
} ?>
                </ul>
            </div>
        </div>
    </div>

</div>

<script>

    $(document).ready(function () {
        var options = {
            valueNames: ['name']
        };
        var userList = new List('categories', options);
    });


</script>
