<style>
    .ui-widget {
        font-family: Verdana,Arial,sans-serif;
        font-size: 1.1em;
    }
    .ui-dialog-titlebar-close {
        background: rgb(201, 0, 0);
        border: 0;
        color: white;
        /* border-radius: 50%; */
        font-weight: bold;
    }
    .ui-dialog-titlebar-close:after {
        content: 'X'; /* ANSI X letter */
    }
    .ui-widget-content {
        background: #fff;
        /* border: 1px solid #90d93f; */
        color: #222222;
        padding: 10px;
    }
    .ui-dialog {
        left: 0;
        outline: 0 none;
        padding: 0 !important;
        position: absolute;
        top: 0;
        box-shadow: 0 3px 15px rgba(0,0,0,0.5);
    }
    #success {
        padding: 0;
        margin: 0; 
    }
    .ui-dialog .ui-dialog-content {
        background: none repeat scroll 0 0 transparent;
        border: 0 none;
        overflow: auto;
        position: relative;
        padding: 20px 10px;
        display: block;
        width: auto;
        min-height: 31px;
        max-height: none;
        height: auto;
        border-bottom: 1px solid #b5b6ba;
    }
    .ui-widget-header {
        background: #c90000;
        border: 0;
        color: #fff;
        font-weight: normal;
        /* padding: 0 5px 10px 5px; */
        text-align: right;
    }
    .ui-dialog .ui-dialog-titlebar {
        position: relative;
        font-size: 1em;
    }
    .ui-dialog-buttonset {
        text-align: right;
    }
    .ui-dialog-buttonset button{
        background-color: #C90000;
        color: white;
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        touch-action: manipulation;
        cursor: pointer;
        user-select: none;
        border: 0px solid transparent;
        border-radius: 2px;
        margin-left: 5px;
    }
    .container-fluid {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    min-height: 90px !important;
}
</style>
<?php 
	HeadHTML::AddStylesheet('bootstrap-select.min.css');
	HeadHTML::AddJS('bootstrap-select.min.js');
?>
<?php
	$description = isset($_POST['description']) ? $_POST['description'] : (!empty($offer->description) ? $offer->description : '');
	$business_name = isset($_POST['business_name']) ? $_POST['business_name'] : (!empty($offer->business_name) ? $offer->business_name : '');
	$title = isset($_POST['title']) ? $_POST['title'] : (!empty($offer->title) ? $offer->title : '');
	$price = isset($_POST['price']) ? $_POST['price'] : (!empty($offer->price) ? $offer->price : '');
	$final_price = isset($_POST['final_price']) ? $_POST['final_price'] : (!empty($offer->final_price) ? $offer->final_price : '');
	$currency = isset($_POST['currency']) ? $_POST['currency'] : (!empty($offer->currency) ? $offer->currency : '');
	$images = isset($_POST['offer_image']) ? $_POST['offer_image'] : (!empty($offer->images) ? $offer->images : '');
?>
<form action="" method="post" enctype='multipart/form-data' >

    <div class="business_std_container" style="margin-top: 0px;" >
        <div class="content">
            <div class="row">
                <div class="col-md-12 business-body alert-grey">
                    <div class="row">
                        <div class="col-md-12">
                        <?php Messages::showComponentErrors('Businesses'); ?>
                        <?php Messages::showComponentMsg('Businesses'); ?>

                            <div class="save-business-block">
                                <?php if (!empty($business_usr)) {
	?> 
                                <h4 class="page-header">[$Add_new_offer]</h4>
                                
                                <div class="row">

                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <div class="row">
                                        <label class="col-md-6 col-sm-4 control-label">[$Offer_For_Business]</label>
                                       
                                        <div class="col-md-6 col-sm-8">
                                            <select class="form-control" id="id_biz" name="id_biz" required>
                                                <?php foreach ($business_usr as $bis) {
		?>
                                                    <option value="<?= $bis->id ?>"><?= $bis->company_name ?></option>
                                                    <?php
	} ?>
                                            </select>
                                        </div>
                                        </div>
                                <?php
} else {
		?>
                                <h4 class="page-header">[$Change-offer]</h4>
                                
                                
                                <div class="row">
                                    
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">[$Company_name]></div>
                                                <div class="col-md-6"><strong><?=$business_name?></strong></div>
                                            </div>
                                        </div>
                                <?php
	} ?>
                                        <h5 class="titlecatege">[$Tittle]</h5>
                                        <div class="form-group">
                                            <input class="form-control" id="name" name="offer_title" placeholder="[$tittle]" type="text" required="" value="<?=$title?>">
                                        </div>
                                        
                                        <h5 class="titlecatege">[$Description]</h5>
                                        <div class="form-group">
                                            <textarea class="form-control" id="comment" name="offer_description" placeholder="[$offer_description]" rows="4" required=""><?=$description ?></textarea>
                                        </div>
                                        
                                        <h5 class="titlecatege">[$End_date_of_offer]</h5>
                                        <div class="form-group">
                                            <input class="form-control" id="name" name="offer_expiration_date" value="" placeholder="[$offer_expiration_date]" type="datetime">
                                        </div>
                                        <div class="form-group">
                                        <?php if ($offer != '' && !empty($offer)) {
		?>
                                            <?php if (count($offer->images) > 0) {
			?>
                                            <?php foreach ($offer->images as $key => $img) {
				?>
                                                <div class="col-md-2" id="image-<?= $offer->id . '-' . $key ?>">
                                                    <div class="item" style=" margin-bottom: 30px;background: white;">
                                                        <a class=""
                                                           href="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 800, 500, array('far' => '1', 'bg' => 'FFFFFF'), true, $image_baseurl) ?>"
                                                           data-lightbox="image-1">
                                                            <img class="lazyOwl" src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 205, 205, array('zc' => 1), true, $image_baseurl) ?>" alt="Immagine" width="100%" style="padding: 3px;">
                                                        </a>

                                                        <li href="#" class="fa fa-trash lb-close" id="<?= $offer->id . '-' . $key ?>" style="font-size: 20px;color: #c90000;text-shadow: none;margin: 0 auto;display: table;padding: 5px 0;">  </li> 

                                                    </div>
                                                </div>
                                            <?php
			} ?>
                                        
                                            <script>
                                                $(document).ready(function ()
                                                {
                                                    $('li.fa-trash').css('cursor', 'pointer');
                                                    $('li.fa-trash').click(function (e)
                                                    {
                                                        var id_img = $(this).attr('id');
                                                        $("<div>[$Are_you_sure_you_want_to_delete_this_image] </div>").dialog({
                                                            buttons: {
                                                                "[$yes_option]": function () {

                                                                    $.ajax({
                                                                        type: "POST",
                                                                        url: "<?= Utils::getComponentUrl('Businesses/ajx_OfferImageDelete/') ?>",
                                                                        //dataType: 'json',
                                                                        data: {
                                                                            'id_img': id_img,
                                                                        },
                                                                        success: function (data) {
                                                                            $("#image-" + id_img).remove();
                                                                            console.log(data);
                                                                        },
                                                                        error: function (e) {
                                                                            console.log(e);
                                                                        }
                                                                    });
                                                                    $(this).dialog("close");
                                                                },
                                                                "[$Cancel]": function () {
                                                                    $(this).dialog("close");
                                                                }
                                                            }
                                                        });
                                                    });
                                                });
                                            </script>
                                            <?php
		} ?>
                                        <?php
	} ?>
                                        </div>
                                            <input type="file" name="images[]" id="images" multiple>
                                            <div class="preview" ></div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <h5 class="titlecatege">[$Price]</h5>
                                        <div class="form-group">
                                            <input class="form-control" id="name" name="offer_price" value="<?=$offer->price ?>" placeholder="[$offer_price]" type="number">
                                        </div>

                                        <h5 class="titlecatege">[$Final_Price]</h5>
                                        <div class="form-group">
                                            <input class="form-control" id="name" name="offer_final_price" value="<?=$offer->final_price ?>" placeholder="[$offer_final_price]" type="number">
                                        </div>
                                        
                                        <h5 class="titlecatege">[$Coins]</h5>
                                        <div class="form-group" >
                                            <select class="form-control" name="currency" required>
                                                  <option <?= $currency == 'ALL' ? 'selected' : '' ?> value="ALL">[$Leke]</option>
                                                  <option <?= $currency == 'EUR' ? 'selected' : '' ?> value="EUR">[$Euro]</option>
                                                  <option <?= $currency == 'USD' ? 'selected' : '' ?> value="USD">[$Dollar]</option>
                                            </select>
                                        </div>
                                    </div> 
                                </div> 
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <input  id="save-description" type="submit" name="save-offer" value="[$save_offer]" class="btn btn-md btn-default save-business-button ">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    $(function() {
        // Multiple images preview in browser
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        $($.parseHTML('<img width="150">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#images').on('change', function() {
            imagesPreview(this, 'div.preview');
        });
    });
    
</script>
<script>
    $('#id_biz').selectpicker({
        style: 'btn-default btn-sm'
    });
</script>