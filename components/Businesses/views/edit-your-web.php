<?php HeadHTML::addMeta('keywords', $business->company_name . ' , ' . $business->category . ' , ' . $business->address . '  , ' . CMSSettings::$website_title . ' , njoftime , biznese , ' . $business->category . ' ' . CMSSettings::$website_title); ?>
<?php HeadHTML::addMeta('description', StringUtils::CutString(strip_tags($business->description), 200)); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/edit-your-web.css'); ?>
<?php $image = isset($_POST['image']) ? $_POST['image'] : $subdomain->cover_image; ?>
<style>
    /* Admin Menu Affix */
    .affix {
        top: 90px;
        z-index: 97;
        width: 262px;
    }
    .btn-file {
    position: relative;
    overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        width: 100%;
    }
</style>

<main id="main">
    <div class="overlay"></div>
    <header class="header">
        <!--    <div class="search-box">
              <input placeholder="Search..."><span class="icon glyphicon glyphicon-search"></span>
            </div>-->
        <h1 class="page-title"><?=$business->company_name?></h1>
    </header>
</main>
<div class="respons" ></div>

<div id="the-layer" class="layer" style='display:none'>
    <img src="<?= Utils::genThumbnailUrl('businesses/preloader.gif', 0, 0, array(), true) ?>"/>
</div>

<div class="container">
    <div class="col-md-6">
        <div class="form-group">
            <?php if ($subdomain->cover_image != '') {
	?>
                <img name="image" id="subdomian-cover-image" class="image" src="<?= Utils::genThumbnailUrl('businesses/covers/' . $subdomain->cover_image, 800, 500, array(), true, $image_baseurl) ?>" />
            <?php
} else {
		?>
                <img id="subdomian-cover-image" class="image" src="<?= Utils::genThumbnailUrl('businesses/default.jpg', 0, 0, array(), true) ?>" />
            <?php
	} ?>
            <label>Upload Cover Image</label>
            <div class="input-group">
                <span class="input-group-btn">
                    
                    <form id="cover-form" enctype='multipart/form-data' action="<?= Utils::getComponentUrl('Businesses/ajx_updateBusinessCoverImage/') . $business->id ?>" method="POST">
    
                        <span class="btn btn-default btn-file">
                            Browse… <input type="file" class="hidden-input image-wrapper" id="cover-select" name="photos" multiple/>
                        </span>

                    </form>
                </span>
                <input type="text" class="form-control" readonly>
            </div>
        </div>
    </div>
</div>
<script>


    var coverform = document.getElementById('cover-form');
    var coverSelect = document.getElementById('cover-select');
    var roll = document.getElementById("the-layer");
    var layer = document.getElementsByClassName("layer");
    function changeImage(a) {
        document.getElementById("subdomian-cover-image").src = a;
        $('#subdomian-cover-image').show();
    }
    
    coverSelect.onchange = function (event) {
        event.preventDefault();
        var files = coverSelect.files;
        var formData = new FormData();
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if (!file.type.match('image.*')) {
                continue;
            }
            formData.append('photos', file, file.name);
            $('#the-layer').show();
            $('#subdomian-cover-image').hide();
        }
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '<?= Utils::getComponentUrl('Businesses/ajx_updateBusinessCoverImage/') . $business->id ?>', true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                //console.log(xhr.responseText);
                //$('.respons').prepend(xhr.responseText);
                $('#the-layer').hide();
                changeImage(xhr.responseText);
            }
        }
        xhr.onload = function () {
            if (xhr.status === 200) {
            } else {
                alert('An error occurred!');
            }
        };
        // Send the Data.  
        xhr.send(formData);
    };
</script>