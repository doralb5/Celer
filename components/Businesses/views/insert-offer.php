
<form action="" method="post" enctype='multipart/form-data' >

    <div class="business_std_container" style="margin-top: 0px;" >
        <div class="content">
            <div class="row">
                <div class="col-md-12 business-body alert-grey">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="save-business-block">
                                <h4 class="page-header">SHTONI NJE OFERTE</h4>

                                <div class="row">
                                    <?php Messages::showComponentErrors('Businesses'); ?>
                                    <?php Messages::showComponentMsg('Businesses'); ?>

                                    <div class="form-group">
                                        <label class="col-md-4 col-sm-4 control-label">[$Offer_For_Business]</label>

                                        <div class="col-md-8 col-sm-8">
                                            <select class="form-control" id="id_biz" name="id_biz" required>
                                                <?php foreach ($business_usr as $bis) {
	?>
                                                    <option value="<?= $bis->id ?>"><?= $bis->company_name ?></option>
                                                    <?php
} ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <h5 class="titlecatege">Titulli</h5>
                                        <div class="form-group">
                                            <input class="form-control" id="name" name="offer_title" placeholder="Titulli ofertes" type="text" required="">
                                        </div>
                                        
                                        <h5 class="titlecatege">Pershkrimi</h5>
                                        <div class="form-group">
                                            <textarea class="form-control" id="comment" name="offer_description" placeholder="Pershkrimi i Ofertes" rows="4" required=""></textarea>
                                        </div>
                                        
                                        <h5 class="titlecatege">Data e perfundimit te ofertes</h5>
                                        <div class="form-group">
                                            <input class="form-control" id="name" name="offer_expiration_date" placeholder="Data e skadimit" type="datetime">
                                        </div>
                                        <div class="form-group">
                                            <input type="file" name="image[]" id="image" multiple>
                                            <img id="preview" src="" style="display:none" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <h5 class="titlecatege">Cmimi</h5>
                                        <div class="form-group">
                                            <input class="form-control" id="name" name="offer_price" placeholder="Cmimi i ofertes" type="number">
                                        </div>

                                        <h5 class="titlecatege">Cmimi Final</h5>
                                        <div class="form-group">
                                            <input class="form-control" id="name" name="offer_final_price" placeholder="Cmimi final" type="number">
                                        </div>
                                        
                                        <h5 class="titlecatege">Monedha</h5>
                                        <div class="form-group" > 
                                            <select class="form-control" name="currency" required>
                                                  <option value="ALL">Lekë</option>
                                                  <option value="EUR">Euro</option>
                                                  <option value="USD">Dollar</option>
                                            </select>
                                        </div>
                                    </div> 
                                </div> 
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <input  id="save-description" type="submit" name="save-offer" value="Ruaj Oferten" class="btn btn-md btn-default save-business-button ">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    function preview(input) {
        if (input.files && input.files[0]) {
            var freader = new FileReader();
            freader.onload = function (e) {
                $("#preview").show();
                $('#preview').attr('src', e.target.result);
            }
            freader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#image").change(function(){
        preview(this);
    });
</script>