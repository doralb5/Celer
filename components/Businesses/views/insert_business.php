<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/insert_business.css'); ?>
<?php HeadHTML::AddStylesheet('select2.min.css'); ?>
<?php HeadHTML::AddJS('select2.full.min.js'); ?>

<?php $lat = (isset($PageParameters['map_lat'])) ? $PageParameters['map_lat'] : '41.3280456' ?>
<?php $lng = (isset($PageParameters['map_lng'])) ? $PageParameters['map_lng'] : '19.8119486' ?>

<?php

if (isset($CompSettings['google_key']) && $CompSettings['google_key'] != '') {
	HeadHtml::AddJS('https://maps.googleapis.com/maps/api/js?key=' . $CompSettings['google_key']);
} else {
	HeadHtml::AddJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyBEiE-_79eSxQl4b1tdrEichm8vTLw4VC0');
}
?>

<?php
$company_name = (isset($_POST['company_name'])) ? $_POST['company_name'] : '';
$description = (isset($_POST['description'])) ? $_POST['description'] : '';
$firstname = (isset($_POST['firstname'])) ? $_POST['firstname'] : '';
$lastname = (isset($_POST['lastname'])) ? $_POST['lastname'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
$tel = (isset($_POST['phone'])) ? $_POST['phone'] : '';
$website = (isset($_POST['website'])) ? $_POST['website'] : '';
$facebook = (isset($_POST['facebook'])) ? $_POST['facebook'] : '';
$linkedin = (isset($_POST['linkedin'])) ? $_POST['linkedin'] : '';
$instagram = (isset($_POST['instagram'])) ? $_POST['instagram'] : '';
$google = (isset($_POST['google'])) ? $_POST['google'] : '';
$twitter = (isset($_POST['twitter'])) ? $_POST['twitter'] : '';
$id_category = (isset($_POST['id_category'])) ? $_POST['id_category'] : '';
$category = (isset($_POST['category'])) ? $_POST['category'] : '';
$subdomain = (isset($_POST['subdomain'])) ? $_POST['subdomain'] : '';
$business_mail = (isset($_POST['business_mail'])) ? $_POST['business_mail'] : '';
$address = (isset($_POST['address'])) ? $_POST['address'] : '';
$coordinates = (isset($_POST['coordinates'])) ? $_POST['coordinates'] : '';

$add_ids = array();
if (isset($_POST['additional_categ'])) {
	if (is_array($_POST['additional_categ'])) {
		foreach ($_POST['additional_categ'] as $add_categ) {
			array_push($add_ids, $add_categ);
		}
	} else {
		array_push($add_ids, $_POST['additional_categ']);
	}
}
?>

<style>
    .header_bg{
        padding: 1em 0 12em 0px;
        /* margin: -16px -16px 0px -16px; */
        background-color: #81c3cf;
        background-image: url(http://www.netirane.al/data/netirane.al/media/register.png);
        -webkit-background-size: cover !important;
        -moz-background-size: cover !important;
        -ms-background-size: cover !important;
        -o-background-size: cover !important;
        background-size: contain;
        background-position: center bottom;
        background-repeat: no-repeat;
        text-align: center;
        width: 100%;
    }
    @media (max-width:992px) {
        .resizable_bg {
            margin-right: -15px;
            margin-left: -15px;;
        }
    }
    @media (min-width:767px) {
        .resizable_bg {
            margin-right: 0px;
            margin-left: 0px;;
        }
    }
</style>

<!--<div class="resizable_bg" style="margin-top:20px;">
    <div class="header_bg">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <h2 class="" style="color: white;text-shadow: 0px 2px 2px rgba(33, 31, 31, 0.65);">[$data_insert]</h2>
                    <h4 class="" style="color: white;text-shadow: 0px 2px 2px rgba(33, 31, 31, 0.3);">[$your_business] <span style="color: #c90000;"><?= CMSSettings::$webdomain ?></span></h4>
                </div>
            </div>
        </div>
    </div>
</div>-->

<form id="insert-company" method="post" action="" enctype="multipart/form-data" class="alert-grey">

    <div class="row">
        <div class="col-md-12">
            <div class="title-login text-center">
                <h1 class="login-main-title">[$data_insert]</h1>
                <h1 class="login-main-subtitle">[$your_business] <span style="color: #c90000;"><?= CMSSettings::$webdomain ?></span></h1><br>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 border-left">
            <?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
	?>
                <div class="row">
                    <div class="col-xs-12">
                        <?php $this->ComponentErrors->renderHTML(); ?>
                        <?php $this->ComponentMessages->renderHTML(); ?>
                    </div>
                </div>
            <?php
} ?>

            <div class="row">
                <div class="col-md-6">

                    <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
		?>

                        <div class="business-insertion">
                            <fieldset>
                                <input type="text" class="firstname" id="firstname"  name="firstname" value="<?= $_SESSION['user_auth']['firstname'] ?>" required="" autofocus=""/>
                                <label for="username">[$name_p]*</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>

                    <?php
	} else {
		?>


                        <div class="business-insertion">
                            <fieldset>
                                <input type="text" class="firstname" id="firstname"  name="firstname" value="<?= $firstname ?>" required="" autofocus=""/>
                                <label for="firstname">[$name_p]*</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>


                    <?php
	} ?>

                </div>
                <div class="col-md-6">

                    <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
		?>

                        <div class="business-insertion">
                            <fieldset>
                                <input type="text" class="lastname" id="lastname"  name="lastname" value="<?= $_SESSION['user_auth']['lastname'] ?>" required="" />
                                <label for="lastname">[$l_name]*</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>

                    <?php
	} else {
		?>

                        <fieldset>
                            <input type="text" class="lastname" id="lastname"  name="lastname" value="<?= $lastname ?>" required="" />
                            <label for="lastname">[$l_name]*</label>
                            <div class="underline"></div>
                        </fieldset>

                    <?php
	} ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h5 class="titlecatege">[$additional_categories]</h5>

                    <div class="form-group formgroup">
                        <select multiple class="form-control" id="additional_categ" name="additional_categ[]">
                            <?php foreach ($categories as $categ) {
		?>
                                <option
                                    value="<?= $categ->id ?>" <?= (in_array($categ->id, $add_ids)) ? 'selected' : '' ?>><?= $categ->category ?></option>
                                <?php
	} ?>
                        </select>
                        <label class="well-label">[$business_additional]</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <br><br>
                    <div class="business-insertion">
                        <fieldset>
                            <input type="text" class="company_name" id="company_name"  name="company_name" value="<?= $company_name ?>" required="" />
                            <label for="company_name">[$company_name]*</label>
                            <div class="underline"></div>
                        </fieldset>
                    </div>

                </div>
                <div class="col-md-6">
                    <h5 class="titlecatege">[$image]</h5>
                    <span class="file-input"><div class="file-preview ">
                            <div class="file-preview-thumbnails">
                                <div class="file-preview-frame">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="file-preview-status text-center text-success"></div>
                            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                        </div>
                    </span>

                    <div class="upload-btn">
                        <input type="file" name="image">
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h5 class="titlecatege">[$description]* </h5>

                    <div class="form-group">
                        <textarea minlength="100" class="form-control" rows="6" id="descrizione" name="description" placeholder=""
                                  required=""><?= $description ?></textarea>
                        <label class="well-label">[$min_characters]</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 ">
                    <h5 class="titlecatege">[$locations]</h5>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>

                        <select  id="select-location" type="text" class="form-control"  name="location"  readonly="true">
                            <option value="">[$select_a_city]</option>
                            <?php
							asort($locations);
							foreach ($locations as $location) {
								?>
                                <option value="<?= $location ?>"><?= $location ?></option>
                            <?php
							} ?>
                        </select>

                    </div>
                </div>
                <div class="col-md-4">
                    <h5 class="titlecatege">[$city]* </h5>

                    <input type="text" class="email" id="city" name="city" value="" required="">
                    <!--                    <label class="well-label">[$drag_marker]</label>-->
                </div>


                <div class="col-md-4">
                    <h5 class="titlecatege">[$address]* </h5>

                    <div class=" formgroup">
                        <div class="input-group">
                            <div class="input-group-addon gps"><a onclick="getGPSLocation()"><i class="fa fa-compass" aria-hidden="true"></i></a></div>
                            <input type="text" class="form-control" id="address" name="address" value="<?= $address ?>"
                                   placeholder="[$address_placeholder]" onblur="setMarker();" value="">
                            <input type="hidden" id="coordinates" name="coordinates" value="<?= $coordinates ?>">
                        </div>
                    </div>
                    <!--                    <label class="well-label">[$drag_marker]</label>-->
                </div>

            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 marg-bottm">
                    <h5 class="titlecatege">[$map]</h5>

                    <div id="map" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-md-6">
<?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
								?>

                        <div class="business-insertion">
                            <fieldset>
                                <input type="email" class="email" id="email"  name="email" value="<?= $_SESSION['user_auth']['email'] ?>" required="" />
                                <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> [$email]*</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>

<?php
							} else {
								?>
                        <div class="business-insertion">
                            <fieldset>
                                <input type="email" class="email" id="email"  name="email" value="<?= $email ?>" required="" />
                                <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> [$email]*</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>
<?php
							} ?>
                </div>

                <div class="col-md-6">

                    <div class="business-insertion">
                        <fieldset>
                            <input type="text" class="phone" id="phone"  name="phone" value="<?= $tel ?>" required="" />
                            <label for="phone"><i class="fa fa-phone" aria-hidden="true"></i> [$phone]*</label>
                            <div class="underline"></div>
                        </fieldset>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-md-6">

                    <div class="business-insertion">
                        <fieldset>
                            <input type="website" class="website" id="website"  name="website" value="<?= $website ?>"/>
                            <label for="website"><i class="fa fa-globe" aria-hidden="true"></i> [$website]</label>
                            <div class="underline"></div>
                        </fieldset>
                    </div>

                </div>
                <div class="col-md-6">

                    <div class="business-insertion">
                        <fieldset>
                            <input type="text" class="facebook" id="facebook"  name="facebook" value="<?= $facebook ?>" />
                            <label for="facebook"><i class="fa fa-facebook" aria-hidden="true"></i> [$facebook]</label>
                            <div class="underline"></div>
                        </fieldset>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    <div class="business-insertion">
                        <fieldset>
                            <input type="text" class="linkedin" id="linkedin"  name="linkedin" value="<?= $linkedin ?>"/>
                            <label for="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i> [$linkedin]</label>
                            <div class="underline"></div>
                        </fieldset>
                    </div>

                </div>
                <div class="col-md-6">

                    <div class="business-insertion">
                        <fieldset>
                            <input type="text" class="instagram" id="instagram"  name="instagram" value="<?= $instagram ?>"/>
                            <label for="instagram"><i class="fa fa-instagram" aria-hidden="true"></i> [$instagram]</label>
                            <div class="underline"></div>
                        </fieldset>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    <div class="business-insertion">
                        <fieldset>
                            <input type="text" class="google" id="google"  name="google" value="<?= $google ?>"/>
                            <label for="google"><i class="fa fa-google-plus" aria-hidden="true"></i> [$google]</label>
                            <div class="underline"></div>
                        </fieldset>
                    </div>

                </div>
                <div class="col-md-6">

                    <div class="business-insertion">
                        <fieldset>
                            <input type="text" class="twitter" id="twitter"  name="twitter" value="<?= $twitter ?>"/>
                            <label for="twitter"><i class="fa fa-twitter" aria-hidden="true"></i> [$twitter]</label>
                            <div class="underline"></div>
                        </fieldset>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="extra">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="titlecatege" style="text-align: center">[$subdomain]</h4>

                            <div class="row">
                                <div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-5 col-xs-offset-1" style="padding-right: 0px;">
                                    <div class="form-group formgroup">
                                        <span class="check-subdomain glyphicon "></span>
                                        <input type="text" class="form-control input-lg" id="subdomain" name="subdomain"
                                               style="text-align: right; padding-right: 0px;  border: 0px solid #ccc !important;"
                                               value="<?= $subdomain ?>"
                                               placeholder="" required="">
                                    </div>
                                </div>
                                <div class="col-md-4 col-md-offset-right-2 col-sm-4 col-sm-offset-right-2 col-xs-5 col-xs-offset-right-1" style="padding-left: 0px;">
                                    <div class="form-group formgroup">
                                        <input type="text" class="form-control input-lg" value=".<?= CMSSettings::$webdomain ?>" name=""
                                               style="padding-left: 0px; border: 0px solid #ccc !important;"
                                               placeholder="netirane.al" readonly="true">
                                        <input type="hidden" value="<?= CMSSettings::$webdomain ?>" name="domain">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="titlecatege" style="text-align: center">[$business_mail]</h4>

                            <div class="row">
                                <div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-5 col-xs-offset-1" style="padding-right: 0px;">
                                    <div class="form-group formgroup">
                                        <span class="check-email glyphicon "></span>
                                        <input type="text" class="form-control input-lg" id="business_mail" name="business_mail"
                                               style="text-align: right; padding-right: 0px; border: 0px solid #ccc !important;"
                                               value="<?= $business_mail ?>"
                                               placeholder="" required="">
                                    </div>
                                </div>
                                <div class="col-md-4 col-md-offset-right-2 col-sm-4 col-sm-offset-right-2 col-xs-5 col-xs-offset-right-1" style="padding-left: 0px;">
                                    <div class="form-group formgroup">
                                        <input type="text" class="form-control input-lg" value="@<?= CMSSettings::$webdomain ?>" name="webdomain"
                                               style="padding-left: 0px; border: 0px solid #ccc !important;"
                                               placeholder="<?= CMSSettings::$webdomain ?>" readonly="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center finish-button">
                    <button type="submit" id="feedbackSubmit" name="save" class="btn btn-md btn-default">[$continue]
                    </button>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="warning" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title">[$warning]</h5>
                        </div>
                        <div class="modal-body alert-warning">
                            <p>[$insert_valid_input]</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">[$close]</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<script>
    $(document).ready(function () {
    fileInput();
    });
</script>
<script>
    // Function to replace inputs

    function fileInput(fi_container_class, fi_button_class, fi_filename_class, fi_button_text) {

    // Arguments
    fi_container_class = fi_container_class || 'fileUpload'; // Classname of the wrapper that contains the button & filename.
    fi_button_class = fi_button_class || 'fileBtn'; // Classname for the button
    fi_filename_class = fi_filename_class || 'fileName'; // Name of the text element's class
    fi_button_text = fi_button_text || '[$upload_image]'; // Text inside the button

    // Variables
    var fi_file = $('input[type=file]'); // Type of input to look for

    // Hide file inputs
    fi_file.css('display', 'none');
    // String to append
    var fi_str = '<div class="' + fi_container_class + '"><div class="' + fi_button_class + '">' + fi_button_text + '</div><div class="' + fi_filename_class + '"></div></div>';
    // Append "fake input" after the original input (which have been hidden)
    fi_file.after(fi_str);
    // Count amount of inputs
    var fi_count = fi_file.length;
    // Loop while "count" is greater than or equal to "i".
    for (var i = 1; i <= fi_count; i++) {
    // Get original input-name
    var fi_file_name = fi_file.eq(i - 1).attr('name');
    // Assign the name to the equivalent "fake input".
    $('.' + fi_container_class).eq(i - 1).attr('data-name', fi_file_name);
    }

    // Button: action
    $('.' + fi_button_class).on('click', function () {
    // Get the name of the clicked "fake-input"
    var fi_active_input = $(this).parent().data('name');
    // Trigger "real input" with the equivalent input-name
    $('input[name=' + fi_active_input + ']').trigger('click');
    });
    // When the value of input changes
    fi_file.on('change', function () {
    // Variables
    var fi_file_name = $(this).val(); // Get the name and path of the chosen file
    var fi_real_name = $(this).attr('name'); // Get the name of changed input

    // Remove path from file-name
    var fi_array = fi_file_name.split('\\'); // Split on backslash (and escape it)
    var fi_last_row = fi_array.length - 1; // Deduct 1 due to 0-based index
    fi_file_name = fi_array[fi_last_row]; //

    // Loop through each "fake input container"
    $('.' + fi_container_class).each(function () {
    // Name of "this" fake-input
    var fi_fake_name = $(this).data('name');
    // If changed "fake button" is equal to the changed input-name
    if (fi_real_name == fi_fake_name) {
    // Add chosen file-name to the "fake input's label"
    $('.' + fi_container_class + '[data-name=' + fi_real_name + '] .' + fi_filename_class).html(fi_file_name);
    }
    });
    });
    }
</script>






<script type="text/javascript">
    var lat = <?= $lat ?>;
    var lng = <?= $lng ?>;
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
            center: {lat: lat, lng: lng}
    });
    var geocoder = new google.maps.Geocoder();
    var marker;
    marker = new google.maps.Marker({
    icon: 'http://www.netirane.al/data/netirane.al/media/marker1.png'
    });
<?php
if (strtolower(CMSSettings::$webdomain) == 'netirane.al') {
								$location = 'Tirana';
							} elseif (strtolower(CMSSettings::$webdomain) == 'nedurres.al') {
								$location = 'Durres';
							} elseif (strtolower(CMSSettings::$webdomain) == 'neshkoder.al') {
								$location = 'Shkoder';
							} elseif (strtolower(CMSSettings::$webdomain) == 'nefier.al') {
								$location = 'Fier';
							} elseif (strtolower(CMSSettings::$webdomain) == 'nesarande.al') {
								$location = 'Sarande';
							} elseif (strtolower(CMSSettings::$webdomain) == 'nevlore.al') {
								$location = 'Vlore';
							} elseif (strtolower(CMSSettings::$webdomain) == 'neelbasan.al') {
								$location = 'Elbasan';
							} elseif (strtolower(CMSSettings::$webdomain) == 'nepogradec.al') {
								$location = 'Pogradec';
							} elseif (strtolower(CMSSettings::$webdomain) == 'nelibrazhd.al') {
								$location = 'Librazhd';
							} else {
								$location = ($PageParameters['default_location'] != '') ? $PageParameters['default_location'] : '';
							}
?>
    <?php if ($location != '') {
	?>
        geocodeAddress("<?= $location ?>", geocoder, map);
        $('#select-location').on('change', function(){
        geocodeAddress($(this).val(), geocoder, map);
        });
        function setMarker() {

        if (marker != null)
                marker.setMap(null);
            var address = document.getElementById('address').value + '<?= $location ?>';
            geocodeAddress(address, geocoder, map);
//            if (document.getElementById('address').value != '')
//                    map.setZoom(15);
//            else
                    map.setZoom(15);
        }
    <?
}?>


    function geocodeAddress(address, geocoder, resultsMap) {
    geocoder.geocode({'address': address}, function (results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
    resultsMap.setCenter(results[0].geometry.location);
    marker.set('map', resultsMap);
    marker.set('position', results[0].geometry.location);
    marker.set('draggable', true);
    marker.set('animation', google.maps.Animation.DROP);
    marker.set('icon', '<?= Utils::genThumbnailUrl('marker2.png', 35, 51, array(), true) ?>');
//                marker = new google.maps.Marker({
//                    map: resultsMap,
//                    position: results[0].geometry.location,
//                    draggable: true,
//                    animation: google.maps.Animation.DROP,
//                    icon: 'http://www.netirane.al/data/netirane.al/media/marker2.png'
//                });


    google.maps.event.addListener(marker, 'dragend', function () {
    geocodePosition(marker.getPosition());
    });
    $('#coordinates').val(results[0].geometry.location);
    //console.log($('#coordinates').val());
    } else {
    $("#warning").modal();
    }
    });
    }


    function geocodePosition(pos) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode
            ({
            latLng: pos
            },
                    function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                    $("#address").val(results[0].formatted_address);
                    $('#coordinates').val(results[0].geometry.location.toString());
                    } else {
                    $("#warning").modal();
                    //$("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
                    }
                    }
            );
    }

    var checkeventcount = 1, prevTarget;
    $('.modal').on('show.bs.modal', function (e) {
    if (typeof prevTarget == 'undefined' || (checkeventcount == 1 && e.target != prevTarget)) {
    prevTarget = e.target;
    checkeventcount++;
    e.preventDefault();
    $(e.target).appendTo('body').modal('show');
    } else if (e.target == prevTarget && checkeventcount == 2) {
    checkeventcount--;
    }
    });
    var GPSoptions = {
    enableHighAccuracy: true,
            timeout: 3000,
            maximumAge: 0
    };
    function getGPSLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, errorPosition, GPSoptions);
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
    }
    function showPosition(position) {
        //console.log("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
        lat = position.coords.latitude;
        lng = position.coords.longitude;
        latlng = new google.maps.LatLng(lat, lng);
        marker.set('position', latlng);
        map.setCenter(latlng);
        $('#coordinates').val('(' + lat + ',' + lng + ')');
    }
    function errorPosition(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    }
    getGPSLocation();
</script>


<script>

    function checkSubdomnain (){
    $.ajax({
    type: 'POST',
            data: {subdomain: $('#subdomain').val()},
            url: "<?= Utils::getComponentUrl('Businesses/checkAviableSubdomain') ?>",
            dataType: 'JSON',
            success: function (result) {
            console.log(result);
            var data = result.toString();
            var res = data.split(',');
            if (data[0] == 1) {
            $("#subdomain").addClass('danger alert-danger');
            $(".check-subdomain").addClass('glyphicon-remove');
            $("#feedbackSubmit").attr('disabled', 'disabled');
            } else {
            $("#subdomain").removeClass('alert alert-danger');
            $(".check-subdomain").removeClass('glyphicon-remove');
            $("#subdomain").addClass('alert alert-success');
            $(".check-subdomain").addClass('glyphicon-ok');
            $("#feedbackSubmit").removeAttr('disabled', 'disabled');
            }
            },
            error: function (e)     {
            console.log(e);
            },
    });
    }


    function checkEmail (){
    $.ajax({
    type: 'POST',
            data: {business_mail: $('#business_mail').val()},
            url: "<?= Utils::getComponentUrl('Businesses/ajx_checkAviableEmail') ?>",
            dataType: 'JSON',
            success: function (result) {
            console.log(result);
            var data = result.toString();
            var res = data.split(',');
            if (data[0] == 1) {
            $("#business_mail").addClass('danger alert-danger');
            $(".check-email").addClass('glyphicon-remove');
            $("#feedbackSubmit").attr('disabled', 'disabled');
            } else {
            $("#business_mail").removeClass('alert alert-danger');
            $(".check-email").removeClass('glyphicon-remove');
            $("#business_mail").addClass('alert alert-success');
            $(".check-email").addClass('glyphicon-ok');
            $("#feedbackSubmit").removeAttr('disabled', 'disabled');
            }
            },
            error: function (e)     {
            console.log(e);
            },
    });
    }


    $("#company_name").keyup(function () {
        var insertdomain = $('#company_name').val();
        insertdomain = insertdomain.replace(/ /g, '-');
        $('#subdomain').val(insertdomain);
        checkSubdomnain ();
        if ($("#subdomain").val() == ''){
        $("#subdomain").addClass('danger alert-danger');
        }
        $('#business_mail').val(insertdomain);
        checkEmail ();
        if ($("#business_mail").val() == ''){
        $("#business_mail").addClass('danger alert-danger');
        }
    });
    $("#subdomain").keyup(function () {
        var insertdomain = $('#subdomain').val();
        insertdomain = insertdomain.replace(/ /g, '-');
        $('#subdomain').val(insertdomain);
        checkSubdomnain ();
        if ($("#subdomain").val() == ''){
        $("#subdomain").addClass('danger alert-danger');
        }
    });
    $("#subdomain").keyup(function () {
        var insertdomain = $('#subdomain').val();
        insertdomain = insertdomain.replace(/ /g, '-');
        $('#business_mail').val(insertdomain);
        checkEmail ();
        if ($("#business_mail").val() == ''){
        $("#business_mail").addClass('danger alert-danger');
        }
    });
    $("#business_mail").keyup(function () {
        var insertdomain = $('#business_mail').val();
        insertdomain = insertdomain.replace(/ /g, '-');
        $('#business_mail').val(insertdomain);
        checkEmail ();
        if ($("#business_mail").val() == ''){
        $("#business_mail").addClass('danger alert-danger');
        }
    });
    
    $("#additional_categ").select2();
    /*$("#id_category").select2();*/
    $('#business-name').blur(function () {
    var newval = $('#business-name').val().replace(/[^A-Z0-9]/ig, "-").toLowerCase();
    $('#subdomain').val(newval);
    $('#business_mail').val(newval);
    checkSubdomnain ();
    checkEmail();
    });</script>



<script>
    (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
    (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-90682060-1', 'auto');
    ga('send', 'pageview');</script>
<script>
    
    
</script>
<script>
    (function () {
    $('.info a.link').click(function () {
    return false;
    });
    $('input').blur(function () {
    if ($(this).val()) {
    return $(this).addClass('filled');
    } else {
    return $(this).removeClass('filled');
    }
    });
    }).call(this);
    $(document).ready(function () {
    $('input').each(function () {
    if ($(this).val()) {
    $(this).addClass('filled');
    } else {
    $(this).removeClass('filled');
    }
    });
    if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
    $(window).load(function () {
    $('input:-webkit-autofill').each(function () {

    if ($(this).length > 0 || $(this).val().length > 0) {
    $(this).addClass('filled');
    } else {
    $(this).removeClass('filled');
    }
    });
    });
    }
    });
</script>