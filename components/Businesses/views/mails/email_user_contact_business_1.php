
        <title>{SITENAME}</title>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <style>
    .body-main{
      margin: 0 auto;
    }
    .body-info{
        background-color : #2e2e2e;
        color: #9A9A9A;
        padding: 5%;
    }  
        </style>    
        <!-- begin page body -->
        <table class="body-main" style="background: white;">
            <tr>
                <td>
            <center>
                <!-- begin page header -->
                <table>
                    <tr>
                        <td>
                    <center>
                        <!-- begin container -->
                        <table>
                            <tr>
                                <td>
                                    <!-- begin six columns -->
                                    <table>
                                        <tr>
                                            <td>
                                                <a href="http://{URL_WEBSITE}"><img src="{URL_LOGO}"/></a>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <!-- end six columns -->
                                </td>
                                
                            </tr>
                        </table>
                        <!-- end container -->
                    </center>
                    </td>
                    </tr>
                </table>
                <!-- end page header -->

                <!-- begin page container -->
                <table>
                    <tr>
                        <td>
                            <!-- begin row -->
                            <table>
                                <tr>
                                    <td>
                                        <!-- begin twelve columns -->
                                        <table>
                                            <tr>
                                                <td class="body-info">
                                                    <h4>[$At] <?= date('d/m/Y H:i') ?> [$has_been_added_new_business]</h4>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <p></p>
                                                    <table class="body-info">
                                                        <tr>
                                                            <td>
                                                                [$name]:
                                                            </td>
                                                            <td>
                                                                <?= $business->company_name ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                 [$address]:
                                                            </td>
                                                            <td>
                                                                 <?= $business->address ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                [$emer_mbiemer]:
                                                            </td>
                                                            <td>
                                                                <?= $user->firstname . ' ' ?>
                                                                <?= $user->lastname ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                [$administrator_email]:
                                                            </td>
                                                            <td>
                                                                <?= $business->email ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                [$phone]:
                                                            </td>
                                                            <td>
                                                               <?= $business->phone ?>
                                                            </td>
                                                        </tr>
                                                        <?php if ($business->website != '') {
	?>
                                                        <tr>
                                                            <td>
                                                                [$website]:
                                                            </td>
                                                            <td>
                                                                
                                                                <a href="<?= $business->website ?>" target="_blank">Websiti i <?= $business->company_name ?></a>
                                                            </td>
                                                        </tr>
                                                        <?php
} ?>
                                                        
                                                        <?php if ($business->facebook != '') {
		?>
                                                        <tr>
                                                            <td>
                                                                [$facebook]:
                                                            </td>
                                                            <td>
                                                                <a href="<?= $business->facebook ?>" target="_blank"><?= $business->company_name ?> ne Facebook</a>
                                                            </td>
                                                        </tr>
                                                        <?php
	} ?>
                                                        <?php if ($business->instagram != '') {
		?>
                                                        <tr>
                                                            <td>
                                                                [$instagram]:
                                                            </td>
                                                            <td>
                                                                <a href="<?= $business->instagram ?>" target="_blank"><?= $business->company_name ?> ne Instagram</a>
                                                            </td>
                                                        </tr>
                                                        <?php
	} ?>
                                                        <?php if ($business->linkedin != '') {
		?>
                                                        <tr>
                                                            <td>
                                                                [$linkedin]:
                                                            </td>
                                                            <td>
                                                                <a href="<?= $business->linkedin ?>" target="_blank"><?= $business->company_name ?> ne LinkedIn</a>
                                                            </td>
                                                        </tr>
                                                        <?php
	} ?>
                                                    </table>
                                                   
                                                    <br/><br/>
                                                    <hr>
                                                    
                                                    <a href="http://{URL_WEBSITE}">neTirane.al</a><br/>
                                                    [$search_engine]
                                                    
                                                    
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <!-- end twelve columns -->
                                    </td>
                                </tr>
                            </table>
                            <!-- end row -->
                            <!-- begin divider -->
                            <table></table>
                            <!-- end divider -->
                        </td>
                    </tr>
                </table>
                <!-- end page container -->

                <!-- begin page footer -->
                <table>
                    <tr>
                        <td>
                    <center>
                        <!-- begin container -->
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="color:#000000;">
                                                &copy; {SITENAME} <?= date('Y') ?>.
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <!-- end container -->
                    </center>
                    </td>
                    </tr>
                </table>
                <!-- end page footer -->
            </center>
        </td>
    </tr>
</table>
<!-- end page body -->

