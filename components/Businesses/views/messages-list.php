<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/messages-list.css'); ?>

<style>
    /* Admin Menu Affix */
    .affix {
        top: 90px;
        z-index: 97;
        width: 262px;
    }
</style>


<main id="main">
    <div class="overlay"></div>
    <header class="header">
        <!--    <div class="search-box">
              <input placeholder="Search..."><span class="icon glyphicon glyphicon-search"></span>
            </div>-->
        <h1 class="page-title"> Inbox</h1>
    </header>
    <!--  <div class="action-bar">
        <ul>
          <li>
            <a class="icon circle-icon glyphicon glyphicon-chevron-down"></a>
          </li>
          <li>
            <a class="icon circle-icon glyphicon glyphicon-refresh"></a>
          </li>
          <li>
            <a class="icon circle-icon glyphicon glyphicon-share-alt"></a>
          </li>
          <li>
            <a class="icon circle-icon red glyphicon glyphicon-remove"></a>
          </li>
          <li>
            <a class="icon circle-icon red glyphicon glyphicon-flag"></a>
          </li>
        </ul>
      </div>-->


    <div id="main-nano-wrapper" class="nano">
        <div class="nano-content">
            <ul id="accordion" class="message-list">


                <?php
				if (!isset($_SESSION['user_auth'])) {
					echo('<h4 class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span> <strong>Kujdes!</strong>' . ' [$Login_required] ' . '<a href= ' . Utils::getComponentUrl('Users/login') . ' class="pull-right"> Hyr </a>' . '<h4>');
				} elseif ($_SESSION['user_auth']['id'] != $id_user) {
					echo('<h6 class="alert alert-danger">' . '[$not_your_business]' . '<h6>');
				}
				?>

                <?php
				$msg = 0;
				?>
                <?php
				if (count($messages)) {
					foreach ($messages as $message) {
						?>
                        <div class="panel">
                            <li class="<?if ($message['readed'] == 1) {
							echo 'read';
						} else {
							echo 'unread';
						} ?>" data-toggle="collapse" id="<?= $message['id'] ?>" data-parent="#accordion" href="#msg<?= $msg ?>">
                                <div class="col col-1">
                                    <span class="dot"></span>
                                    <div class="checkbox-wrapper">
                                        <input type="checkbox" id="<?= $message['id'] ?>">
                                        <label for="<?= $message['id'] ?>" class="toggle"></label>
                                    </div>
                                    <p class="title"><?= $message['name'] ?></p>
                                    <span class="star-toggle glyphicon glyphicon-star-empty"></span>
                                </div>

                                <div class="col col-2">
                                    <div class="subject"> <span class="teaser"><?= $message['message'] ?></span></div>
                                    <div class="date"><?= date('G:i - j M ', strtotime($message['creation_date'])) ?></div>
                                </div>
                            </li>


                            <div id="msg<?= $msg ?>" class="collapse">
                                <div  class="message-container">

                                    <div class="received">

                                        <div class="message">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td style="border-top: 0px solid #ddd; width:110px;"><b>Mesazhi:</b></td>
                                                        <td style="border-top: 0px solid #ddd;"><?= $message['message'] ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                        <div class="tool-box">
                                            <a href="#" class="circle-icon small glyphicon glyphicon-share-alt"></a>
                                            <a href="#" class="circle-icon small red-hover glyphicon glyphicon-remove"></a>
                                            <a href="#" class="circle-icon small red-hover glyphicon glyphicon-flag"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <?php
						$msg++; ?>




                    <?php
					}
				} else {
					echo '<div class="alert alert-info">[$NoMessages]</div>';
				}
				?> 
            </ul>
        </div>
    </div>

    <script>

        $('.unread').click(function () {
            var id = this.id;
            $.ajax({
                dataType: 'JSON',
                url: "<?= Utils::getComponentUrl('Businesses/ajx_setMessageRead/')?>"+id,
            })
                    .done(function (data) {
                        var res = data.toString();
                            if (res=='1'){
                                $('#'+id).addClass('readed');
                                $('#'+id).removeClass('unread');
                            }
                            else {
                                alert('Ndodhi nje problem gjate leximit te mesazhit. Ne qofte se problemi perseritet, ju lutem kontaktoni me ne.');
                            }
                    });




        });
    </script>

</main>
