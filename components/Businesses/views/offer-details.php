<?php
$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$tel = isset($_POST['tel']) ? $_POST['tel'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';
?>
<?php HeadHTML::addMeta('keywords', $business->company_name . ' , ' . $business->category . ' , ' . $business->address . '  , ' . CMSSettings::$website_title . ' , njoftime , biznese , ' . $business->category . ' ' . CMSSettings::$website_title); ?>
<?php HeadHTML::addMeta('description', StringUtils::CutString(strip_tags($offer->description), 200)); ?>
<?php HeadHTML::addMetaProperty('og:title', $offer->title . ' | ' . $business->company_name); ?>
<?php HeadHTML::addMetaProperty('og:type', 'article'); ?>
<?php
	if ($offer) {
		foreach ($offer->images as $key => $img) {
			HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('businesses/offers/' . str_replace('%2F', '/', urlencode($img)), 980, 600, array('zc' => '1'), false, $image_baseurl));
			if ($key == 0) {
				break;
			}
		}
	} else {
		HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('businesses/offers/' . str_replace('%2F', '/', urlencode('default.jpg')), 980, 600, array('zc' => '1'), false, $image_baseurl));
	}
?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/offer-details-style.css'); ?>
<?php HeadHtml::linkStylesheet('owl.carousel.css'); ?>
<?php HeadHtml::linkJS('owl.carousel.js'); ?>

<div id="offer-details">
    <div class="alert-grey">
        <div class="row">
            <?php if (!is_null($offer)) {
	?>
                <!-- Ofertat -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <!--***** Send Request *****-->
                            
                            <section id="send-request-<?= $offer->id ?>">
                                <fieldset>
                                    <legend class="scheduler-border" id="row6">[$send_a_request]</legend>
                                    <div class="col-md-10 col-md-offset-1">
                                        <div id="reqRespond" ></div>
                                        <form class="form-horizontal" class="post-request" id="<?= $offer->id ?>" action="" method="post">
                                            <input name="title" type="hidden" value="<?= $offer->title ?>" />
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="business-insertion">
                                                        <fieldset>
                                                            <input type="text" class="name required" id="name"  name="name-request" <?php if (isset($_SESSION['user_auth'])) {
		?> value="<?= $_SESSION['user_auth']['firstname'] . ' ' . $_SESSION['user_auth']['lastname'] ?>" <?php
	} ?> data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." required="" />
                                                            <label for="name">[$Name]*</label>
                                                            <div class="underline"></div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="business-insertion">
                                                        <fieldset>
                                                            <input type="email" class="email required" id="email"  name="email-request" value="<?= $email ?>" data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@email.com)" required="" />
                                                            <label for="email">[$Email]*</label>
                                                            <div class="underline"></div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="business-insertion">
                                                        <fieldset>
                                                            <input type="text" class="tel required" id="tel"  name="tel-request"  value="<?= $tel ?>" data-placement="top" data-trigger="manual" data-content="Must be a valid phone number" required="" />
                                                            <label for="tel">[$Tel]*</label>
                                                            <div class="underline"></div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="">
                                                        <textarea  rows="7" cols="100" class="form-control send-msg-textarea" id="message-request" name="message-request" ><?= $message ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <center><div class="g-recaptcha" id="recaptcha1" required=""></div></center>
                                            <br>
                                            <input type="hidden" name="request" value="1">
                                            <div class="form-group">
                                                <center>
                                                    <button type="submit" name="send-request" id_biz="<?= $offer->id_biz ?>" class="btn btn-default btn-sm">[$Send]</button>
                                                    <a class="btn btn-default btn-sm" data-dismiss="modal" id="close-request">[$Close]</a>
                                                </center>
                                                <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; Ndodhi nje gabim. </p>
                                            </div>
                                        </form>
                                    </div>
                                </fieldset>
                            </section>

                        </div>
                    </div>
                </div>
            <?php
} ?>
            <script>
                $(document).ready(function () {

                    $('form').on('submit', function (e) {
                        // show that something is loading
                        $('#reqRespond').html("<b>Loading response...</b>");
                        $.ajax({
                            type: 'POST',
                            url: '<?= Utils::getComponentUrl('Businesses/ajx_offer_requestinfo/') ?>' + this.id,
                            data: $(this).serialize()
                        })
                                .done(function (data) {
                                    // show the response
                                    $('#reqRespond').html(data);
                                })
                                .fail(function () {
                                    // just in case posting your form failed
                                    alert("Posting failed.");
                                });
                        e.preventDefault();
                        // to prevent refreshing the whole page page
                        return false;

                    });
                });
            </script>
                
            <div class="preview col-md-6">

                <div class="preview-pic tab-content">
                    <?php $first = true; foreach ($offer->images as $key => $img) {
		?>
                        <div class="tab-pane <?= $first == true ? 'active' : '' ?>" id="pic-<?= $key ?>"><img src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 500, 300, array(), true, $image_baseurl) ?>" /></div>
                    <?php $first = false;
	} ?>
                </div>
                <ul class="preview-thumbnail nav nav-tabs">
                    <?php foreach ($offer->images as $key => $img) {
		?>
                        <li class="<?= $key == 0 ? 'active' : '' ?>"><a data-target="#pic-<?= $key ?>" data-toggle="tab"><img src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 400, 400, array(), true, $image_baseurl) ?>" /></a></li>
                    <?php
	} ?>
                </ul>

            </div>
            <div class="col-md-6" style="border:0px solid gray">
                <div class="row">
                    <div class="col-md-12">
                        <h3><?= $offer->title ?></h3>    
                        <h5 class="by">[$sold_by] <a href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($offer->business_name . '-' . $offer->id_biz)) ?>"><?= $offer->business_name ?></a></h5>

                        <hr>

                        <div class="offer-price">
                            <?php if ($offer->final_price != '') {
		?>
                                <div class="price"><?= Utils::price_format($offer->price, $offer->currency, true, false); ?></div>
                                <div class="final-price"><?= Utils::price_format($offer->final_price, $offer->currency, true, false); ?></div>
                            <?php
	} else {
		?>
                                <span class="only-price"><?= Utils::price_format($offer->price, $offer->currency, true, false); ?></span>                                                   
                            <?php
	} ?>
                        </div>


                        <hr>
                        <!-- Button -->
                        <div style="padding-bottom:20px;">
                            <button class="btn btn-default btn-block contact-open" ><span style="margin-right:20px" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>[$Request_Offer]</button>
                        </div>    
                    </div>    
                </div>    

                <div class="row">
                    <div class="col-md-12">
                        <ul class="menu-items">
                            <li class="active">[$Details]</li>
                        </ul>
                        <div style="width:100%;border-top:1px solid silver">
                            <p style="padding:15px 0;">
                                <small>
                                    <?= $offer->description ?>
                                </small>
                            </p>
                        </div>
                    </div>	
                </div>	
            </div>      
            
            <div class="col-md-12">
                <!--***** Send Message *****-->
                <section id="send-msg" style="display:none;">
                    <fieldset>
                        <legend class="scheduler-border" id="row6">[$send_a_message]</legend>
                        <div class="col-md-10 col-md-offset-1">
                            <form class="form-horizontal" action="" method="post">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="business-insertion">
                                            <fieldset>
                                                <input type="text" class="name required" id="name"  name="name" <?php if (isset($_SESSION['user_auth'])) {
		?> value="<?= $_SESSION['user_auth']['firstname'] . ' ' . $_SESSION['user_auth']['lastname'] ?>" <?php
	} ?> data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." required="" />
                                                <label for="name">[$Name]*</label>
                                                <div class="underline"></div>
                                            </fieldset>
                                        </div>
                                        <div class="business-insertion">
                                            <fieldset>
                                                <input type="email" class="email required" id="email"  name="email" value="<?= $email ?>" data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@email.com)" required="" />
                                                <label for="email">[$Email]*</label>
                                                <div class="underline"></div>
                                            </fieldset>
                                        </div>
                                        <div class="business-insertion">
                                            <fieldset>
                                                <input type="text" class="tel required" id="tel"  name="tel"  value="<?= $tel ?>" data-placement="top" data-trigger="manual" data-content="Must be a valid phone number" required="" />
                                                <label for="tel">[$Tel]*</label>
                                                <div class="underline"></div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="">
                                            <textarea  rows="8" cols="100" class="form-control send-msg-textarea" id="message" name="message" placeholder="[$message_content]" required><?= $message ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <center><div class="g-recaptcha" id="recaptcha1" required=""></div></center>
                                <br>
                                <input type="hidden" name="links" value="">
                                <div class="form-group">
                                    <center>
                                        <button type="submit" name="send" class="btn btn-default btn-sm">[$Send]</button>
                                        <a class="btn btn-default btn-sm" id="skip-msg">[$Close]</a>
                                    </center>
                                    <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; Ndodhi nje gabim. </p>
                                </div>
                            </form>
                        </div>
                    </fieldset>
                </section>

                <!--Render Msg-->

                <?php Messages::showErrors(); ?>
                <?php Messages::showInfoMsg(); ?>


                <?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
		?>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <?php $this->ComponentErrors->renderHTML(); ?>
                            <?php $this->ComponentMessages->renderHTML(); ?>
                        </div>
                    </div>
                <?php
	} ?>

            </div>


        </div>


    </div>
</div>
<script>
// Carousel Auto-Cycle
    $(document).ready(function () {
        $('.carousel').carousel({
            interval: 6000
        })
        
                // message toggle

        $(".contact-open").click(function () {
            $("#send-msg").slideToggle("slow");
        });
        $(".request-open").click(function () {
            $("#send-request").slideToggle("slow");
        });
        $("#skip-request").click(function () {
            $("#send-request").slideToggle("slow");
        });
        $("#skip-msg").click(function () {
            $("#send-msg").slideToggle("slow");
        });
        
        
    });
</script>


<div class="row">
    <div class="col-md-12">
        <h4 class="page-header">OFERTAT E NGJASHME</h4>
    </div>  
</div> 
<div id="owl-offers" class="owl-carousel">
    <!-- Ofertat -->
    <?php foreach ($similar_offers as $sim) {
		?>
        <div id="carousel-example-generic-<?= $sim->id ?>" class="carousel slide offers-carousel" data-ride="carousel" data-interval="false">
            <div class="carousel-inner">
                <div class="item active srle">
                    <?php if (!empty($sim->images)) {
			foreach ($sim->images as $img) {
				?>
                        <img src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 500, 300, array(), true, $image_baseurl) ?>" alt="<?= $img ?>" class="img-responsive">
                    <?php
			}
		} else {
			?>
                        <img class="little-logo"
                             src="<?= Utils::genThumbnailUrl('businesses/offers/default.jpg', 500, 300, array(), true); ?>"
                             style="width:100%">
                         <?php
		} ?>
                </div>
            </div>

            <?php if (count($sim->images) > 1) {
			?>
                <a class="left carousel-control" href="#carousel-example-generic-<?= $sim->id ?>" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic-<?= $sim->id ?>" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            <?php
		} ?>

            <div class="main-offer-info">
                <h1 class="business-offer-name">
                    <a href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($offer->business_name . '-' . $offer->id_biz)) ?>"><?= $offer->business_name ?></a>
                </h1>
                <h1 class="business-offer-title">
                    <a title=""><?= $sim->title ?></a>
                </h1>
                <span style=" ">
                    <a href="<?= Utils::getComponentUrl('Businesses/offer_details/' . $sim->id) ?>" class="btn btn-xs btn-block btn-default bussiness-comments" data-toggle="modal" >Me Shume</a>
                </span> 
            </div>
        </div>
    <?php
	} ?>


    <script>
        // thumbnails.carousel.js jQuery plugin
        ;
        (function (window, $, undefined) {

            var conf = {
                center: true,
                backgroundControl: false
            };

            var cache = {
                $carouselContainer: $('.thumbnails-carousel').parent(),
                $thumbnailsLi: $('.thumbnails-carousel li'),
                $controls: $('.thumbnails-carousel').parent().find('.carousel-control')
            };

            function init() {
                cache.$carouselContainer.find('ol.carousel-indicators').addClass('indicators-fix');
                cache.$thumbnailsLi.first().addClass('active-thumbnail');

                if (!conf.backgroundControl) {
                    cache.$carouselContainer.find('.carousel-control').addClass('controls-background-reset');
                } else {
                    cache.$controls.height(cache.$carouselContainer.find('.carousel-inner').height());
                }

                if (conf.center) {
                    cache.$thumbnailsLi.wrapAll("<div class='center clearfix'></div>");
                }
            }

            function refreshOpacities(domEl) {
                cache.$thumbnailsLi.removeClass('active-thumbnail');
                cache.$thumbnailsLi.eq($(domEl).index()).addClass('active-thumbnail');
            }

            function bindUiActions() {
                cache.$carouselContainer.on('slide.bs.carousel', function (e) {
                    refreshOpacities(e.relatedTarget);
                });

                cache.$thumbnailsLi.click(function () {
                    cache.$carouselContainer.carousel($(this).index());
                });
            }

            $.fn.thumbnailsCarousel = function (options) {
                conf = $.extend(conf, options);
                init();
                bindUiActions();
                return this;
            }

        })(window, jQuery);

        $('.thumbnails-carousel').thumbnailsCarousel();


    </script>

</div>

<!-- Owl-Carousel-JavaScript -->
<script>
    $(document).ready(function () {
        $("#owl-offers").owlCarousel({
            items: 5,
            lazyLoad: false,
            autoPlay: false,
            pagination: true,
            loop: false,
        });
    });
    $(".request-open").click(function () {
        $(this).children().toggle();
    });
    $("#send-request").click(function (event) {
        event.stopPropagation();
    });
//                        $(".request-open").click(function () {
//                            $("#send-request").slideToggle("slow");
//                        });
//                        $("#skip-request").click(function () {
//                            $(this).find("#send-request").slideToggle("slow");
//                        });
</script>


<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });

        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });

    }).call(this);



    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>