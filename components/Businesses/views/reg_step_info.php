<h2>Step Info</h2>

<?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
	?>
    <div class="row">
        <div class="col-xs-12">
            <?php $this->ComponentErrors->renderHTML(); ?>
            <?php $this->ComponentMessages->renderHTML(); ?>
        </div>
    </div>
<?php
} ?>


<form id="registration_form" data-step="<?= $step ?>">

    <div class="form-group">
        <label for="email">[$BusinessName]*</label>
        <input type="text" class="form-control" id="company_name" placeholder="[$enter_business_name]" name="company_name">
    </div>

    <div class="form-group">
        <label for="street">[$address]</label>
        <input type="text" class="form-control" id="address" placeholder="[$enter_business_address]" name="address">
    </div>


    <div class="form-group">
        <label for="city">[$city]</label>
        <input type="text" class="form-control" id="city" placeholder="[$enter_business_city]" name="city">
    </div>


    <div class="form-group">
        <label for="district">[$Disctrict]</label>
        <input type="text" class="form-control" id="district" placeholder="[$enter_business_district]" name="district">
    </div>

    <style>
        #map_canvas { width: 100%; height: 450px; }
    </style>

    <div id="map_canvas"></div>
    <div id="debug"></div>



    <div class="form-group">
        <label for="coordinates">[$coordinates]</label>
        <input readonly="true" type="text" class="form-control" id="coordinates" placeholder="[$enter_business_province]" name="coordinates">
    </div>


    <input type="hidden" name="next_info" value="1" />



    <button type="submit" class="btn btn-default">[$Next]</button>
</form>



<script>
    var gMapsLoaded = false;
    window.gMapsCallback = function () {
        gMapsLoaded = true;
        $(window).trigger('gMapsLoaded');
    }
    window.loadGoogleMaps = function () {
        if (gMapsLoaded)
            return window.gMapsCallback();
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type", "text/javascript");
        script_tag.setAttribute("src", "http://maps.google.com/maps/api/js?sensor=false&callback=gMapsCallback");
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    }

    function changeAddress(address, city = '', district = '', country = '') {
        if (address !== '') {
            $('#address').val(address);
        }
        if (city !== '') {
            $('#city').val(city);
        }
        if (district !== '') {
            $('#district').val(district);
        }
        $('#address').tooltip("hide");
        return false;
    }

    $(document).ready(function () {

        function initialize() {
            var lat = 41.3280456;
            var lng = 19.8119486;
            var map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 15,
                center: {lat: lat, lng: lng}
            });
            var geocoder = new google.maps.Geocoder();
            var marker;
            marker = new google.maps.Marker({
                icon: 'http://www.netirane.al/data/netirane.al/media/marker1.png'
            });
            geocodeAddress("Tirana", geocoder, map);
            $('#select-location').on('change', function () {
                geocodeAddress($(this).val(), geocoder, map);
            });
            function setMarker() {

                if (marker != null)
                    marker.setMap(null);
                var address = document.getElementById('address').value + 'Tirana';
                geocodeAddress(address, geocoder, map);
                map.setZoom(15);
            }


            function geocodeAddress(address, geocoder, resultsMap) {
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        resultsMap.setCenter(results[0].geometry.location);
                        marker.set('map', resultsMap);
                        marker.set('position', results[0].geometry.location);
                        marker.set('draggable', true);
                        marker.set('animation', google.maps.Animation.DROP);
                        marker.set('icon', 'http://www.netirane.al/data/netirane.al/media/marker2.png');
                        google.maps.event.addListener(marker, 'dragend', function () {
                            geocodePosition(marker.getPosition());
                            ////
                        });
                        $('#coordinates').val(results[0].geometry.location);
                    } else {
                        $("#warning").modal();
                    }
                });
            }



            $('#address').tooltip({trigger: "manual", title: "...", html: true, placement: "bottom"});

            function geocodePosition(pos) {
                geocoder = new google.maps.Geocoder();
                geocoder.geocode
                        ({
                            latLng: pos
                        },
                                function (results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        var alt_address = results[0].formatted_address;
                                        var address_components = results[0].address_components;
                                        var components = {};
                                        jQuery.each(address_components, function (k, v1) {
                                            jQuery.each(v1.types, function (k2, v2) {
                                                components[v2] = v1.long_name
                                            });
                                        })
                                        console.log(components);
                                        if (components.street_number !== undefined) {
                                            newaddress = components.route + ', ' + components.street_number
                                        } else {
                                            newaddress = components.route
                                        }
                                        $('#address').attr('data-original-title', alt_address + "<div class='row' style='position:relative;' ><span class='col-md-1'><br/></span><a class=\"btn btn-default col-md-4\" onclick='changeAddress(\"" + newaddress + "\",\"" + components.administrative_area_level_2 + "\",\"" + components.administrative_area_level_1 + "\"); return false;'>[$yes_option]</a><span class='col-md-2'></span><a class='btn btn-default col-md-4' onclick=\"$('#address').tooltip('hide');return false;\">[$no_option]</a><span class='col-md-1'></span></div>")
                                                .tooltip('show');
                                        $('#coordinates').val(results[0].geometry.location.toString());
                                    } else {
                                        $("#warning").modal();
                                    }
                                }
                        );
            }

            var checkeventcount = 1, prevTarget;
            $('.modal').on('show.bs.modal', function (e) {
                if (typeof prevTarget == 'undefined' || (checkeventcount == 1 && e.target != prevTarget)) {
                    prevTarget = e.target;
                    checkeventcount++;
                    e.preventDefault();
                    $(e.target).appendTo('body').modal('show');
                } else if (e.target == prevTarget && checkeventcount == 2) {
                    checkeventcount--;
                }
            });
            var GPSoptions = {
                enableHighAccuracy: true,
                timeout: 3000,
                maximumAge: 0
            };
            function getGPSLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition, errorPosition, GPSoptions);
                } else {
                    console.log("Geolocation is not supported by this browser.");
                }
            }
            function showPosition(position) {
                lat = position.coords.latitude;
                lng = position.coords.longitude;
                latlng = new google.maps.LatLng(lat, lng);
                marker.set('position', latlng);
                map.setCenter(latlng);
                $('#coordinates').val('(' + lat + ',' + lng + ')');
            }
            function errorPosition(err) {
                console.warn(`ERROR(${err.code}): ${err.message}`);
            }
            getGPSLocation();
            $("#city").on("blur", function (e) {
                geocodeFullAdress();
            });
            $("#street").on("blur", function (e) {
                geocodeFullAdress();
            });
            $("#district").on("blur", function (e) {
                geocodeFullAdress();
            });
            function geocodeFullAdress() {

                var address = $("#address").val();
                var city = $("#city").val();
                var district = $("#district").val();
                var fullAdress = "";
                if (address != '') {
                    fullAdress = fullAdress + ' ' + address;
                }

                if (city != '') {
                    fullAdress = fullAdress + ' ' + city;
                }

                if (district != '') {
                    fullAdress = fullAdress + ' ' + district;
                }

                if (fullAdress != '') {
                    geocodeAddress(fullAdress, geocoder, map);
                }
            }


        }
        $(window).bind('gMapsLoaded', initialize);
        window.loadGoogleMaps();
    });

</script>


