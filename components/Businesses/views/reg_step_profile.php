<h2>Step: <?=$step?></h2>
<form id="registration_form" data-step="<?= $step ?>" >
    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>
    <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
    </div>
    <div class="checkbox">
        <label><input type="checkbox" name="remember"> Remember me</label>
    </div>
    <input type="hidden" name="next_profile" value="1" />
    <button type="submit" class="btn btn-default">Next</button>
</form>
