

<script async defer
        src="http://maps.googleapis.com/maps/api/js?callback=gMapsCallback&key=AIzaSyBEiE-_79eSxQl4b1tdrEichm8vTLw4VC0">
</script>



<div id="map" style="height: 100%; width: 100%; position: relative;"></div>
<style>
    #pos-cmiddle { margin-top: 100px}
    .ajax_response{border: solid black;}
</style>

<div  class="container">
    <span  class="col-md-2"></span>

    <div id="ajax_response" class="col-md-8 ajax_response">

    </div>

    <span class="col-md-2"></span>
</div>




<script>

    $(document).ready(function (e) {

        function sendFormData(data, step) {
            $.ajax({
                type: "POST",
                url: '<?= $ajax_request_url ?>/' + step,
                data: data,
                success: function (response) {
                    $("#ajax_response").html(response);
                    var myForm = $("#registration_form");
                    myForm.on("submit", function (event) {
                        event.preventDefault ? event.preventDefault() : (event.returnValue = false);
                        var step = $(this).data('step');
                        sendFormData($(this).serialize(), step);
                    });

                    var current = $("#registration_form").data('step');
                    window.history.pushState("object or string", "Title", "<?= $request_url ?>/" + current);

                    $("#ajax_response").find("script").each(function (i) {
                        eval($(this).text());
                    });

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        sendFormData([], "<?= $step ?>");

    });

</script>
