<?php
$comnum = 0;
foreach ($comments as $comment) {
	if ($comment['comment'] != '') {
		$comnum++;
	}
}
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/reviews-list.css');
?>


<section id="reviews-list">
    <div class="row">
        <div class="col-md-12">

            <h4 id="row4" class="page-header" style="text-transform: uppercase;"> Reçensionet</h4>


            <?php if (isset($comments) && $comnum != 0) {
	?>
                <?php
				foreach ($comments as $com) {
					if ($com['comment'] != '') {
						?>
                        <ul id="comments">
                            <li>
                                <div class="avatar-container">
                                    <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar" alt="avatar">
                                </div>
                                <div class="post-container">
                                    <div>
                                        <span class="author_name"><?= $com['name'] ?>
                                        </span></siv>

                                        <span class="bullet" aria-hidden="true">•</span>

                                        <span class="comment-stars">
                                            <?php
											$i = $com['rating'];
						$j = 5 - $com['rating'];
						for ($u = 1; $u <= $i; $u++) {
							?>
                                                <i class="fa fa-star star-on-png "></i>
                                            <?php
						}
						for ($b = 1; $b <= $j; $b++) {
							?>

                                                <i class="fa fa-star-o star-on-png "></i>

                                            <?php
						} ?>
                                        </span>

                                        <p></p>

                                        <p><?= strip_tags($com['comment']) ?></p>

                                        <p></p>

                                        <span class="timeago">Postuar ne:  <span><?= date('d', strtotime($com['publish_date'])) ?>   [$<?= date('M', strtotime($com['publish_date'])) ?>] <?= date('Y', strtotime($com['publish_date'])) ?></span></span>

                                    </div>
                                </div>

                            </li>

                        </ul>
                        <?php
					}
				}
} else {
				?>

             <h4 id="row4" class="">
                 <div class="alert alert-danger" role="alert">Nuk ka recensione per momentin!</div> 
             </h4>
            
<?
			}?>


        </div>
    </div>
</section>