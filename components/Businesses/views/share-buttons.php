<aside id="sticky-social">
    <ul class="clearfix">
        <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']?>" onclick="return windowpop(this.href)" class="entypo-facebook popup" target="_blank">
                <i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
                <span>Facebook</span>
            </a>
        </li>
        <li><a href="https://twitter.com/share?url=http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']?>&amp;text=share on twitter&amp;hashtags=<?= CMSSettings::$website_title ?>" onclick="return windowpop(this.href)" class="entypo-twitter" target="_blank">
                <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
                <span>Twitter</span>
            </a>
        </li>
        <li><a href="https://www.instagram.com/netirane.al/" onclick="return windowpop(this.href)" class="entypo-instagrem" taget="_blank">
                <i class="fa fa-instagram fa-2x" aria-hidden="true"></i>
                <span>Instagram</span>
            </a>
        </li>
        <li><a href="mailto:?Subject=your%20subject" class="entypo-email" taget="_blank" >
                <i class="fa fa-envelope-o fa-2x" aria-hidden="true"></i>
                <span>email</span>
            </a>
        </li>
        <li><a href="https://plus.google.com/share?url=http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']?>" onclick="return windowpop(this.href)" class="entypo-gplus" target="_blank">
                <i class="fa fa-google-plus fa-2x" aria-hidden="true"></i>
                <span>Google+</span>
            </a>
        </li>
        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']?>" onclick="return windowpop(this.href)" class="entypo-linkedin" target="_blank">
                <i class="fa fa-linkedin fa-2x" aria-hidden="true"></i>
                <span>LinkedIn</span>
            </a>
        </li>
        <li><a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" class="entypo-pinterest">
                <i class="fa fa-pinterest-p fa-2x" aria-hidden="true"></i>
                <span>Pinterest</span>
            </a>
        </li>
    </ul>
</aside>