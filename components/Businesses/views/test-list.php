<?php
require_once LIBS_PATH . 'Pager.php';

$w = (isset($parameters['w'])) ? $parameters['w'] : 400;
$h = (isset($parameters['h'])) ? $parameters['h'] : 300;
?>
<div class="row businesses">
    <div class="col-md-12">

        <!----- Search result alert ----->
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-grey">
                    <i class="fa fa-search" aria-hidden="true">&nbsp;&nbsp;</i>Resultati per: <span class="result">Static</span>
                </div>
            </div>
        </div>

        <!-- FREE box listing -->
        <div class="block-section-sm box-list-area">
            <!-- item list -->
            <div class="box-list">
                <div class="item">
                    <div class="row">

                        <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="bus-pic">
                            <div class="img-item">
                                <a href="" class="title_target">

                                    <img class="little-logo" src="http://www.placehold.it/250x150" style="width:100%;">
                                </a>

                            </div>
                        </div>
                        </div>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="business-info">
                            <div class="info">
                                <h3 class="main-business-title no-margin-top">
                                    <a href="" class="title_target">
                                        Bluehat Sh.p.k.
                                    </a>
                                </h3>
                            </div>
                            <div class="info-1">
                                <a href="">
                                    Kategoria
                                </a>

                            </div>
                            <div class="info-2-free">
                                


                                <span class="main-tel"> Celular: +355 674881781</span>
                                <br> 
                                <span class="main-fax">
                                    Adresa: Shëtitorja Murat Toptani, Tiranë 1001, Albania</span>
                            </div>
                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end item list -->
        </div><!-- end box listing -->
        
        
        <!-- STANDART box listing -->
        <div class="block-section-sm box-list-area">
            <!-- item list -->
            <div class="box-list">
                <div class="item">
                    <div class="row">

                        <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="bus-pic">
                            <div class="img-item">
                                <a href="" class="title_target">

                                    <img class="little-logo" src="http://www.placehold.it/250x200" style="width:100%">
                                </a>

                            </div>
                        </div>
                        </div>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="business-info">
                            <div class="info">
                                <h3 class="main-business-title no-margin-top">
                                    <a href="" class="title_target">
                                        Bluehat Sh.p.k.
                                    </a>
                                </h3>
                            </div>
                            <div class="info-1">
                                <a href="">
                                    Kategoria
                                </a>

                            </div>
                            <div class="info-2">
                                <div class="rate-it rate-cus"><i class="fa fa-star star-on-png"></i>&nbsp;<i data-alt="2" class="fa fa-star star-on-png" title="good"></i>&nbsp;<i data-alt="3" class="fa fa-star star-on-png" title="good"></i>&nbsp;<i data-alt="4" class="fa fa-star star-on-png" title="good"></i>&nbsp;<i data-alt="5" class="fa fa-star star-on-png" title="good"></i></div>


                                <span class="main-tel"> Celular: +355 674881781</span>
                                <br> 
                                <span class="main-fax">
                                    Adresa: Shëtitorja Murat Toptani, Tiranë 1001, Albania</span>
                            </div>
                            <div class="info-3">

                                <span class="main-url" style="
                                      "><a href="https://facebook.com" target="_blank">
                                        <i class="fa fa-facebook main-color"></i></a></span>

                                <span class="main-url"><a href="https://instagram.com" target="_blank">
                                        <i class="fa fa-instagram main-color"></i></a></span>

                                <span class="main-url"><a href="https://linkedin.com" target="_blank">
                                        <i class="fa fa-linkedin main-color"></i></a></span>

                                <span class="main-url"><a href="http://google.co" target="_blank"><i class="fa fa-globe main-color"></i></a></span>

                                <span class="main-url"><a href="www.netirane.al" target="_blank"><i class="fa fa-share main-color"></i></a></span>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end item list -->
        </div><!-- end box listing -->


        <!-- PREMIUM box listing -->
        <div class="block-section-sm box-list-area">
            <!-- item list -->
            <div class="box-list">
                <div class="item">
                    <div class="row">

                        <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="bus-pic">
                            <div class="img-item">
                                <a href="" class="title_target">

                                    <img class="little-logo" src="http://www.placehold.it/250x200" style="width:100%">
                                </a>

                            </div>
                        </div>
                        </div>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="business-info">
                            
                            <div class="ribbon-wrapper-green">
                                <div class="ribbon-green">PREMIUM</div>
                            </div>

                            <div class="info">
                                <h3 class="main-business-title no-margin-top">
                                    <a href="" class="title_target">
                                        Bluehat Sh.p.k.
                                    </a>
                                </h3>
                            </div>
                            <div class="info-1">
                                <a href="">
                                    Kategoria
                                </a>

                            </div>
                            <div class="info-2">
                                <div class="rate-it rate-cus"><i class="fa fa-star star-on-png"></i>&nbsp;<i data-alt="2" class="fa fa-star star-on-png" title="good"></i>&nbsp;<i data-alt="3" class="fa fa-star star-on-png" title="good"></i>&nbsp;<i data-alt="4" class="fa fa-star star-on-png" title="good"></i>&nbsp;<i data-alt="5" class="fa fa-star star-on-png" title="good"></i></div>


                                <span class="main-tel"> Celular: +355 674881781</span>
                                <br> 
                                <span class="main-fax">
                                    Adresa: Shëtitorja Murat Toptani, Tiranë 1001, Albania</span>
                            </div>
                            <div class="info-3">

                                <span class="main-url" style="
                                      "><a href="https://facebook.com" target="_blank">
                                        <i class="fa fa-facebook main-color"></i></a></span>

                                <span class="main-url"><a href="https://instagram.com" target="_blank">
                                        <i class="fa fa-instagram main-color"></i></a></span>

                                <span class="main-url"><a href="https://linkedin.com" target="_blank">
                                        <i class="fa fa-linkedin main-color"></i></a></span>

                                <span class="main-url"><a href="http://google.co" target="_blank"><i class="fa fa-globe main-color"></i></a></span>

                                <span class="main-url"><a href="www.netirane.al" target="_blank"><i class="fa fa-share main-color"></i></a></span>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end item list -->
        </div><!-- end box listing -->

    </div>
</div>

