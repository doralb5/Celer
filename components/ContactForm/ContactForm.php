<?php

require_once LIBS_PATH . 'Email.php';

class ContactForm_Component extends BaseComponent
{
	public function preload_contact()
	{
		//BreadCrumb
		WebPage::$breadcrumb->addDir($this->view->getTerm('Contact'), Utils::getComponentUrl('ContactForm/contact'));
	}

	public function contact()
	{
		if (isset($_POST['send'])) {
			($_POST['name'] == '') ? $this->view->AddError('Name is required') : '';
			(strlen($_POST['name']) > 60) ? $this->view->AddError('Name is too long') : '';
			($_POST['email'] == '') ? $this->view->AddError('Email is required') : '';
			(strlen($_POST['email']) > 60) ? $this->view->AddError('Email is too long') : '';
			($_POST['message'] == '') ? $this->view->AddError('Message is required') : '';

			if (isset($_POST['phone'])) {
				$_POST['phone'] .= "\n\nPhone. " . $_POST['phone'];
			}

			if (count($this->view->getErrors()) == 0) {
				$sent = $this->sendMail($_POST);
				if ($sent) {
					$this->view->AddNotice('[$msg_sent]');
					Utils::RedirectTo($this->getActionUrl('contact'));
					return;
				}
				$this->view->AddNotice('[$msg_failed]');
				Utils::RedirectTo($this->getActionUrl('contact'));
				return;
			}
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		//Title tag
		HeadHTML::setTitleTag($this->view->getTerm('Contact') . ' | ' . CMSSettings::$website_title);
		WebPage::setContentHeading($this->view->getTerm('Contact'));
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-contact-form-v2';
		$this->view->render($view);
	}

	public function ajx_send_message()
	{
		$errors = array();

		if (isset($_POST['send'])) {
			($_POST['name'] == '') ? $errors[] = $this->view->getTerm('Name') . ' ' . $this->view->getTerm('is_required') : '';
			($_POST['email'] == '') ? $errors[] = $this->view->getTerm('Email') . ' ' . $this->view->getTerm('is_required') : '';
			(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) ? '' : $errors[] = $this->view->getTerm('Email') . ' ' . $this->view->getTerm('not_valid');
			($_POST['message'] == '') ? $errors[] = $this->view->getTerm('Message') . ' ' . $this->view->getTerm('is_required') : '';

			if (isset($_POST['phone'])) {
				$_POST['phone'] .= "\n\nPhone. " . $_POST['phone'];
			}

			if (count($errors) == 0) {
				$sent = $this->sendMail($_POST);
				if ($sent) {
					echo json_encode(array('msg' => $this->view->getTerm('msg_sent')));
					return;
				}
				$errors[] = $this->view->getTerm('msg_failed');
			}

			echo json_encode(array('error' => implode($errors, '<br/>')));
		}
	}

	public function sendMail($post)
	{
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH . 'ContactForm' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'ContactForm' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('post', $post);

		$txt = $message->render('mails/email_format', true);

		$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, $_POST['name'], $_POST['email'], $this->view->getTerm('NewMessageFrom') . ' ' . CMSSettings::$webdomain, $txt);

		if ($sended) {
			return true;
		}
		return false;
	}
}
