<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/contactform-style.css'); ?>

<?php
HeadHtml::linkJS(Utils::googleMapsJsLink(), true, true);
?>
<?php
$map_height = isset($parameters['map_height']) ? $parameters['map_height'] : 362;

$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';

$lat = (isset($settings['map_lat'])) ? $settings['map_lat'] : '41.333966';
$lng = (isset($settings['map_lng'])) ? $settings['map_lng'] : '19.805357';
$tooltip = (isset($settings['map_tooltip'])) ? $settings['map_tooltip'] : '';
?>
<style>
    #map {
        width: 100%;
        height: <?= $map_height ?>px;
    }
</style>


<script>
    function initMap() {
        var myLatLng = {lat: <?= $lat ?>, lng: <?= $lng ?>};

        var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '<?= $tooltip ?>'
        });

    }
</script>

<div class="contact-page alert-grey">
    <div class="row">
        
        <?php if (!isset($parameters['showmap']) || $parameters['showmap'] != '0') {
	?>
            <div class="col-md-12 col-sm-12 contact-form">
            <?php
} else {
		?>
                <div class="col-md-12 contact-form">
                <?php
	} ?>
                <div class="">
                    <div class="">	
                        <h4 class="">[$contact]</h4><br><br>
                    </div>
                    <div class="">
                        <form role="form" id="feedbackForm" method="post">

                            <?php $this->ComponentErrors->renderHTML(); ?>
                            <?php $this->ComponentMessages->renderHTML(); ?>

                            <?php if (isset($success)) {
		?>
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-check"></i>
                                    <?= $success ?>
                                </div>
                            <?php
	} ?>


                            <div class="login-email">
                                <fieldset>
                                    <input type="text" class="name" id="name"  name="name" value="<?= $name ?>" required="" autofocus=""/>
                                    <label for="name"><i class="fa fa-user" aria-hidden="true"></i>[$name_placehoder]</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>

                            <div class="login-email">
                                <fieldset>
                                    <input type="email" class="email" id="email"  name="email" value="<?= $email ?>" required=""/>
                                    <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i>[$email_placehoder]</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>

                            <div class="form-group">
                                <textarea style="max-width:100%" rows="10" cols="100" class="form-control form-bg-color" id="message" name="message" placeholder="[$message_placehoder]" required><?= $message ?></textarea>
                            </div>

                            <div class="text-center"   style="padding-bottom: 15px;">
                                <button type="submit" id="feedbackSubmit" name="send" class="btn btn-default send-button">[$send_request]</button>
                            </div>  

                        </form>
                    </div>
                </div>
            </div>
     

        <?php if (!isset($parameters['showmap']) || $parameters['showmap'] != '0') {
		?>
            <div class="col-md-12 col-sm-12 map">
                <div class="">
                    <div class="">
                        <h4 class="">[$where_we_are]</h4>
                    </div>
                    <div class="">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        <?php
	} ?>

        <div class="col-md-12 contact-info">
            <div class=" text-center">
                <div class="">
                    <address>
                        <?php if (CMSSettings::$company_name != '') {
		?>
                            <h3 class="company-name"><?= CMSSettings::$company_name ?></h3>
                        <?php
	} ?>
                        <?php if (CMSSettings::$company_address != '') {
		?>
                            <p class="company-address"><?= CMSSettings::$company_address ?></p>
                        <?php
	} ?>
                        <p class="company-mail"><?= CMSSettings::$company_mail ?></p>
                        <?php if (CMSSettings::$company_tel != '') {
		?>
                            <p class="company-tel"><?= CMSSettings::$company_tel ?></p>
                        <?php
	} ?>
                        <?php if (CMSSettings::$company_cel != '') {
		?>
                            <p class="company-cel"><?= CMSSettings::$company_cel ?></p>
                        <?php
	} ?>
                    </address>
                </div>
            </div>
        </div>
   </div>
    </div>
</div>

<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });

        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });

    }).call(this);



    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>