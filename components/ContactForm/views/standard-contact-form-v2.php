<?php
HeadHtml::linkJS(Utils::googleMapsJsLink(), true, true);
?>

<?php
$map_height = isset($parameters['map_height']) ? $parameters['map_height'] : 362;

$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';

$lat = (isset($settings['map_lat'])) ? $settings['map_lat'] : '41.333966';
$lng = (isset($settings['map_lng'])) ? $settings['map_lng'] : '19.805357';
$tooltip = (isset($settings['map_tooltip'])) ? $settings['map_tooltip'] : '';
?>
<style>
    #map {
        width: 100%;
        height: <?= $map_height ?>px;
    }
</style>

<script>
    function initMap() {
        var myLatLng = {lat: <?= $lat ?>, lng: <?= $lng ?>};

        var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '<?= $tooltip ?>'
        });

    }
</script>


<div class="contact-page alert-grey" style="padding-top: 15px;">
    <div class="row">
        <?php if (!isset($parameters['showmap']) || $parameters['showmap'] != '0') {
	?>

            <div class="col-md-12 col-sm-12 map">
                <div class="">
                    <div class="">
                        <h4 class="">[$where_we_are]</h4>
                    </div>
                    <div class="">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        <?php
} ?>
        <div class="col-md-12 contact-info">
            <div class=" text-center">
                <div class="">
                    <address>
                        <?php if (CMSSettings::$company_name != '') {
		?>
                            <h3 class="company-name"><?= CMSSettings::$company_name ?></h3>
                        <?php
	} ?>
                        <?php if (CMSSettings::$company_address != '') {
		?>
                            <p class="company-address"><?= CMSSettings::$company_address ?></p>
                        <?php
	} ?>
                        <p class="company-mail"><?= CMSSettings::$company_mail ?></p>
                        <?php if (CMSSettings::$company_tel != '') {
		?>
                            <p class="company-tel"><?= CMSSettings::$company_tel ?></p>
                        <?php
	} ?>
                        <?php if (CMSSettings::$company_cel != '') {
		?>
                            <p class="company-cel"><?= CMSSettings::$company_cel ?></p>
                        <?php
	} ?>
                    </address>
                </div>
            </div>
        </div>

        <?php if (!isset($parameters['showmap']) || $parameters['showmap'] != '0') {
		?>
            <div class="col-md-12 col-sm-12 contact-form">
            <?php
	} else {
		?>
                <div class="col-md-12 contact-form">
                <?php
	} ?>
                <div class="">
                    <div class="">	
                        <h4 class="">[$contact]</h4>
                    </div>
                    <div class="">
                        <form role="form" id="feedbackForm" method="post">

                            <?php $this->ComponentErrors->renderHTML(); ?>
                            <?php $this->ComponentMessages->renderHTML(); ?>

                            <?php if (isset($success)) {
		?>
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-check"></i>
                                    <?= $success ?>
                                </div>
                            <?php
	} ?>

                            <div class="form-group">
                                <input type="text" class="form-control form-bg-color" id="name" name="name" placeholder="[$name_placehoder]" value="<?= $name ?>" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control form-bg-color" id="email" name="email" placeholder="[$email_placehoder]" value="<?= $email ?>" required>
                            </div>
                            <div class="form-group">
                                <textarea style="max-width:100%" rows="10" cols="100" class="form-control form-bg-color" id="message" name="message" placeholder="[$message_placehoder]" required><?= $message ?></textarea>
                            </div>
                            <div class="text-center">
                                <button type="submit" id="feedbackSubmit" name="send" class="btn btn-default send-button">[$send_request]</button>
                            </div>  
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
