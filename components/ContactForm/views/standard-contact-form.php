<?php
$map_height = isset($parameters['map_height']) ? $parameters['map_height'] : 362;

$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';

$lat = (isset($settings['map_lat'])) ? $settings['map_lat'] : '41.333966';
$lng = (isset($settings['map_lng'])) ? $settings['map_lng'] : '19.805357';
$tooltip = (isset($settings['map_tooltip'])) ? $settings['map_tooltip'] : '';
?>
<style>
    #map {
        width: 100%;
        height: <?= $map_height ?>px;
    }
</style>
<script>
    function initMap() {
        var myLatLng = {lat: <?= $lat ?>, lng: <?= $lng ?>};

        var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '<?= $tooltip ?>'
        });
     
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= CMSSettings::$google_maps_api?>&signed_in=true&callback=initMap"></script>

<div class="contact-page">
    <div class="row contact-form-container">
        <?php if (!isset($parameters['showmap']) || $parameters['showmap'] != '0') {
	?>

            <div class="col-md-6 col-sm-12 map">
                <div class="panel panel-default full-height">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-map-marker"></i> [$where_we_are]</h4>
                    </div>
                    <div class="panel-body">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        <?php
} ?>

        <?php if (!isset($parameters['showmap']) || $parameters['showmap'] != '0') {
		?>
            <div class="col-md-6 col-sm-12 contact-form">
            <?php
	} else {
		?>
                <div class="col-md-12 contact-form">
                <?php
	} ?>
                <div class="panel panel-default full-height">
                    <div class="panel-heading">	
                        <h4 class="panel-title"> <i class="fa fa-envelope"></i> [$contact]</h4>
                    </div>
                    <div class="panel-body">
                        <form role="form" id="feedbackForm" method="post">

                            <?php $this->ComponentErrors->renderHTML(); ?>
                            <?php $this->ComponentMessages->renderHTML(); ?>

                            <?php if (isset($success)) {
		?>
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-check"></i>
                                    <?= $success ?>
                                </div>
                            <?php
	} ?>

                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="[$name_placehoder]" value="<?= $name ?>" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="[$email_placehoder]" value="<?= $email ?>" required>
                            </div>
                            <div class="form-group">
                                <textarea style="max-width:100%" rows="10" cols="100" class="form-control" id="message" name="message" placeholder="[$message_placehoder]" required><?= $message ?></textarea>
                            </div>
                            <button type="submit" id="feedbackSubmit" name="send" class="btn btn-primary send-button">[$send_request]</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 contact-info">
                <div class="panel panel-default text-center">
                    <div class="panel-body">
                        <address>
                              <?php if (CMSSettings::$company_name != '') {
		?>
                            <p class="company-name"><?= CMSSettings::$company_name ?></p>
                            <?
	}?>
                            <?php if (CMSSettings::$company_address != '') {
		?>
                            <p class="company-address"><i class="fa fa-map-marker"></i>&nbsp;<?= CMSSettings::$company_address ?></p>
                            <?
	}?>
                            <p class="company-mail"><i class="fa fa-envelope"></i>&nbsp;<?= CMSSettings::$company_mail ?></p>
                            <?php if (CMSSettings::$company_tel != '') {
		?>
                                <p class="company-tel"><i class="fa fa-phone"></i>&nbsp;<?= CMSSettings::$company_tel ?></p>
                            <?php
	} ?>
                            <?php if (CMSSettings::$company_cel != '') {
		?>
                                <p class="company-cel"><i class="fa fa-mobile"></i>&nbsp;<?= CMSSettings::$company_cel ?></p>
                            <?php
	} ?>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
