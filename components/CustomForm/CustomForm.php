<?php

require_once LIBS_PATH . 'Email.php';

class CustomForm_Component extends BaseComponent
{
	private $mail_view;
	private $mail_subject;
	private $mail_recipient;

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$parameters = WebPage::getParameters();
		$this->mail_view = (isset($parameters['mail_view'])) ? $parameters['mail_view'] : 'customform';
		$this->mail_subject = (isset($parameters['mail_subject'])) ? $parameters['mail_subject'] : '';
		$this->mail_recipient = (isset($parameters['mail_recipient'])) ? $parameters['mail_recipient'] : CMSSettings::$EMAIL_ADMIN;
	}

	public function preload_sendForm()
	{
		//BreadCrumb
		WebPage::$breadcrumb->addDir($this->view->getTerm('Contact'), Utils::getComponentUrl('ContactForm/contact'));
	}

	public function sendForm()
	{
		if (isset($_POST['send'])) {
			if ($this->send($_POST)) {
				$this->AddNotice('[$msg_sent]');
				Utils::RedirectTo($this->getActionUrl('sendForm'));
			} else {
				$this->AddError('[$msg_failed]');
				Utils::RedirectTo($this->getActionUrl('sendForm'));
			}
		}
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		$this->view->set('form_action', Utils::getComponentUrl('CustomForm/sendForm'));
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-custom-form';
		$this->view->render($view);
	}

	public function ajax_SendForm()
	{

		// Temporaneamente valorizzo i parametri del Form in maniera statica ricevendoli via POST
		$this->mail_view = (isset($_POST['mail_view']) && $_POST['mail_view'] != '') ? $_POST['mail_view'] : $this->mail_view;
		$this->mail_subject = (isset($_POST['mail_subject']) && $_POST['mail_subject'] != '') ? $_POST['mail_subject'] : $this->mail_subject;
		$this->mail_recipient = (isset($_POST['mail_recipient']) && $_POST['mail_recipient'] != '') ? $_POST['mail_recipient'] : $this->mail_recipient;

		if ($this->send($_POST)) {
			echo Utils::JsonEncodeSuccessMessage(true, $this->view->getTerm('msg_sent'));
			return;
		}
		echo Utils::JsonEncodeSuccessMessage(false, $this->view->getTerm('msg_failed'), $this->getErrors());
		$this->cleanErrors();
		return false;
	}

	private function send($post)
	{
		if (!isset($post['send'])) {
			$this->AddError('Input with -send- name is not setted!');
			return false;
		}

			($post['cf_firstname'] == '') ? $this->AddError('FirstName is required') : '';
		($post['cf_lastname'] == '') ? $this->AddError('Lastname is required') : '';
		($post['cf_phone'] == '') ? $this->AddError('Phone is required') : '';

		$data = array();
		foreach ($post as $k => $v) {
			if (strstr($k, 'cf_')) {
				$name = substr($k, 3);
				$data[$k] = $v;
			}
		}

		if (count($this->getErrors()) == 0) {
			$from = CMSSettings::$sender_mail;
			$from_name = CMSSettings::$webdomain;
			$to = $this->mail_recipient;
			$subject = ($this->mail_subject != '') ? $this->mail_subject : $this->view->getTerm('New_Contact_Request');
			$sent = $this->sendMail($to, $from_name, $from, $subject, $data);
			if ($sent) {
				return true;
			}
			return false;
		}
		return false;
	}

	public function sendMail($to, $from_name, $from, $subject, $data)
	{
		$message = new BaseView();
		$message->setLangPath(COMPONENTS_PATH . 'CustomForm' . DS . 'lang' . DS);
		$message->setViewPath(COMPONENTS_PATH . 'CustomForm' . DS . 'views' . DS . 'mails' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('data', $data);

		if ($txt = $message->render($this->mail_view . '-mail', true)) {
			$sended = Email::sendMail($to, $from_name, $from, $subject, $txt);
			if ($sended) {
				return true;
			}
			return false;
		}
		$this->AddError("Unable to make mail message using a view $this->mail_view");
		return false;
	}
}
