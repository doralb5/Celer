<?php
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 2;
$targa = (isset($parameters['targa'])) ? $parameters['targa'] : '';
$col_size = 12 / $cols;
?>

<div class="col-md-<?= $col_size ?>" >
    <div class="table-c">
        <div class="desc"><span></span>
            <h3>&nbsp;</h3>

            <?php Messages::showComponentErrors('CustomForm') ?>
            <?php Messages::showComponentMsg('CustomForm') ?>
            <?php if (isset($success)) {
	?>
                <div class="alert alert-success" role="alert">
                    <i class="fa fa-check"></i>
                    <?= $success ?>
                </div>
            <?php
} ?>
            <div class="animate-box fadeInUp animated-fast">
                <h4><strong>[$RichiediPreventivo]</strong></h4>

                <form action="<?= $form_action ?>" method="post">
                    <div class="row form-group">
                        <div class="col-md-12"><!-- <label for="fname">First Name</label> --><input name="cf_firstname" class="form-control" id="fname" placeholder="[$Name]" type="text"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12"><!-- <label for="lname">Last Name</label> --><input name="cf_lastname" class="form-control" id="lname" placeholder="[$Surname]" type="text"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12"><!-- <label for="email">Email</label> --><input name="cf_email" class="form-control" id="email" placeholder="[$Email]" type="email"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12"><!-- <label for="subject">Targa</label> --><input name="cf_targa" class="form-control" id="targa" value="<?= $targa?>" placeholder="[$LicensePlate]" type="text"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12"><!-- <label for="subject">Phone</label> --><input name="cf_phone" class="form-control" id="phone" placeholder="[$Telephone]" type="text"></div>
                    </div>

                    <div class="row form-group">&nbsp;</div>

                    <div class="form-group">
                        <p>[$Privacy] <input id="privacycheck" name="cf_privacy" type="checkbox" value="1" required=""> <label for="privacycheck">[$Accept]</label></p>

                        <p><strong>[$PrivacyPolicy]</strong></p>

                        <p>(Privacy policy ai sensi dell’art. 13 del d.lgs. 196/2003)</p>

                        <p><input class="btn btn-primary" name="send" type="submit" value="Richiedi"></p>
                    </div>
                </form>
            </div>

            <p>&nbsp;</p>
        </div>
    </div>
</div>