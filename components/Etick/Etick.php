<?php

require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Email.php';

class Etick_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->cleanPendingReservations_timing();

		$this->view->set('url_events_list', $this->getActionUrl('eventsList'));
		$this->view->set('url_my_reservations', $this->getActionUrl('list_reservation_user'));
	}

	public function eventsList()
	{
		$elements_per_page = 10;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$filter = ' end_date > now()';
		$filter_old_events = ' end_date < now() ';

		$sorting = ' start_date DESC ';

		$offset = ($page - 1) * $elements_per_page;
		$events = $this->model->getEvents($elements_per_page, $offset, $filter, $sorting);
		$old_events = $this->model->getEvents(4, 0, $filter_old_events, $sorting);

		$totalElements = $this->model->getLastCounter();

		WebPage::$breadcrumb->addDir($this->view->getTerm('event_list'), $this->getActionUrl('eventsList'));

		$this->view->set('old_events', $old_events);
		$this->view->set('events', $events);
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->render('events-list');
	}

	//***************FINISHED***********//

	public function eventDetails($id = '')
	{
		if (!is_null($id)) {
			$event = $this->model->getEvent($id);
			if (count($event) == 0) {
				$this->AddError('[$event]' . $id . '[$not_found]');
				$this->view->renderError('404');
				return;
			}
		} else {
			$this->AddError('[$event]' . $id . '[$not_found]');
			$this->view->renderError('404');
			return;
		}
		WebPage::$breadcrumb->addDir($this->view->getTerm('event_list'), $this->getActionUrl('eventsList'));
		WebPage::$breadcrumb->addDir($event->name, $this->getActionUrl('eventDetails') . '/' . $id);
		$this->view->set('event', $event);
		$this->view->render('event-details');
	}

	//**********FINISHED*********//

	public function addReservation($id = '')
	{
		//Shows the place map
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . Utils::getComponentUrl('Etick/addreservation/' . $id));
		}
		$user_reservations = $this->model->getReservations(-1, 0, ' user.id = ' . $_SESSION['user_auth']['id'] . ' AND eve.id = ' . $id . ' ');
		$count = count($user_reservations);
		$event = $this->model->getEvent($id);
		if ($count >= $event->maxReservations) {
			$this->addNotice('[$max_reservations_reached]');
		}
		if (strtotime($event->start_date) < time()) {
			$this->addError('[$event_expired]');
			Utils::RedirectTo($this->getActionUrl('eventDetails') . DS . $id);
			return;
		}
		$zoneFilter = ' id_place = ' . $event->id_place;
		$zones = $this->model->getZones('', '', $zoneFilter);
		$user_session = UserAuth::getLoginSession();
		$seats = $this->model->getReservations(-1, 0, ' eve.id = ' . $id . ' ', '', $user_session['id_category']);

		WebPage::$breadcrumb->addDir($this->view->getTerm('event_list'), $this->getActionUrl('eventsList'));
		WebPage::$breadcrumb->addDir($event->name, $this->getActionUrl('eventDetails') . '/' . $id);
		WebPage::$breadcrumb->addDir($this->view->getTerm('reserve_map'), $this->getActionUrl('addReservation') . '/' . $id);

		$this->view->set('url_event_details', $this->getActionUrl("eventDetails/$id"));
		$this->view->set('event', $event);
		$this->view->set('seats', $seats);
		$this->view->set('zones', $zones);
		$this->view->set('myreservations', $user_reservations);
		$this->view->set('id', $id);
		$this->view->render('event_map');
	}

	public function showMyEventReservation($id = '')
	{
		$id = preg_replace('/[^0-9]/', '', $id);
		//Shows the place map
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . $this->getActionUrl('list_reservation_user'));
		}
		$user_session = UserAuth::getLoginSession();
		$user_reservations = $this->model->getReservations(-1, 0, ' res.id_event = ' . $id . '  AND res.id_user = ' . $user_session['id'] . ' ', '', $user_session['id_category']);
		if (!count($user_reservations)) {
			$this->addError('[$reservation_not_exist]');
			Utils::RedirectTo($this->getActionUrl('list_reservation_user'));
			return;
		}

		WebPage::$breadcrumb->addDir($this->view->getTerm('my_reservations'), $this->getActionUrl('list_reservation_user'));

		$this->view->set('url_event_details', $this->getActionUrl("eventDetails/$id"));
		$event = $this->model->getEvent($user_reservations[0]->id_event);
		$this->view->set('event', $event);
		$this->view->set('seats', $user_reservations);
		$this->view->set('myreservations', $user_reservations);
		$this->view->set('id', $id);
		$this->view->render('event_map');
	}

	public function sendReservationsByEmail($id = '')
	{
		$id = preg_replace('/[^0-9]/', '', $id);
		//Shows the place map
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . Utils::getComponentUrl('Etick/addreservation/' . $id));
		}
		$user_session = UserAuth::getLoginSession();
		$user_reservations = $this->model->getReservations(-1, 0, ' res.id_event = ' . $id . '  AND res.id_user = ' . $user_session['id'] . ' ', '', $user_session['id_category']);
		if (!count($user_reservations)) {
			$this->addError('[$reservation_not_exist]');
			Utils::RedirectTo($this->getActionUrl('list_reservation_user'));
			return;
		}
		$event = $this->model->getEvent($user_reservations[0]->id_event);
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH . 'Etick' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Etick' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_PHONE')->setVal(CMSSettings::$company_tel);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->set('event', $event);
		$message->set('reservation', $user_reservations);
		$message->set('id', $id);
		$message->renderTemplate(false);
		$testo = $message->render('mails/mail_myreservations', true);
		$to = $user_reservations[0]->email;
		$from = CMSSettings::$sender_mail;
		$from_name = CMSSettings::$website_title;
		$subject = $this->view->getTerm('reservation_info');
		$sended = Email::sendMail($to, $from_name, $from, $subject, $testo);
		if ($sended) {
			$this->AddNotice($this->view->getTerm('reservation_info_sent_by_email'));
		} else {
			$this->AddError($this->view->getTerm('reservation_info_sending_failed'));
		}
		Utils::RedirectTo($this->getActionUrl('list_reservation_user'));
	}

	//********* FINISHED *********//

	public function list_reservation_user()
	{
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . Utils::curPageUrl());
		}

		$user = UserAuth::getLoginSession();
		$filter = ' user.id = ' . $user['id'];
		$filter .= ' AND (res.confirmed = "0" OR res.confirmed is not null) ';  //PERCHÈ NON UTILIZZI IL CAMPO DINAMICO ? libero, occupato, impegnato ? Doralb dice: Apunto!
		$reservations = $this->model->getReservations(-1, 0, $filter, ' eve.start_date DESC , res.creation_date ASC ', $user['id_category']);

		require_once DOCROOT . ENTITIES_PATH . 'Etick/EtickEvent.php';
		$eventres = array();
		$i = -1;
		foreach ($reservations as $res) {
			if (!Utils::in_ObjectArray($eventres, 'id', $res->id_event)) {
				$i++;
				$eve = new EtickEvent_Entity();
				$eve->id = $res->id_event;
				$eve->name = $res->event_name;
				$eve->id_place = $res->place_id;
				$eve->placeName = $res->place_name;
				$eve->description = $res->event_description;
				$eve->image = $res->event_image;
				$eve->start_date = $res->event_start_date;
				$eve->end_date = $res->event_end_date;
				$eve->reservatinos = array();
				$eventres[$i] = $eve;
			}
			$eventres[$i]->reservations[] = $res;
		}

		WebPage::$breadcrumb->addDir($this->view->getTerm('my_reservations'), $this->getActionUrl('list_reservation_user'));

		$this->view->set('events', $eventres);
		$this->view->render('list-res-usr');
	}

	//***********FINISHED*********//

	public function printUserReservations($id_event = null)
	{
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . Utils::curPageUrl());
		}
		if (!is_null($id_event)) {
			$id_event = intval($id_event);
			$user = UserAuth::getLoginSession();
			$filter = ' user.id = ' . $user['id'];
			$filter .= ' AND eve.id = ' . $id_event . ' ';
			$reservations = $this->model->getReservations(-1, 0, $filter, ' eve.start_date DESC , res.creation_date ASC ', $user['id_category']);
			$this->view->set('reservations', $reservations);
			$this->view->render('printable');
		}
	}

	public function delete_reservation($id = null)
	{
		$this->view->setTitle('Etick');

		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . Utils::curPageUrl());
		}

		$reservation = $this->model->getReservation($id);

		if ($reservation != null) {
			if ($reservation->id_user == $_SESSION['user_auth']['id']) {
				if ($reservation->confirmed == '0') {
					if (count($reservation) > 0) {
						$result = $this->model->deleteReservation($id);
						if ($result !== false) {
							$this->addNotice('[$Success]');
						} else {
							$this->addError('[$Failed]');
						}
					}
				} else {
					$this->addError('[$can_not_delete]');
				}
			} else {
				$this->addError('[$do_not_have_permisson]');
			}
		} else {
			$this->addError('[$reservation_do_not_exist]');
		}

		Utils::RedirectTo($this->getActionUrl('list_reservation_user'));
		
		// $this->view->render('delete-reservation');
	}

	//***********FINISHED*********//

	private function saveReservation($user_id, $user_category, $event_id, $seat)
	{
		$user_reservations = $this->model->getReservations(-1, 0, ' user.id = ' . $user_id . ' AND eve.id = ' . $event_id . ' ', '', $user_category);

		$count = count($user_reservations);

		$event = $this->model->getEvent($event_id);

		if ($count >= $event->maxReservations) {
			echo $this->view->getTerm('max_number_reached');
			return;
		}// max resvations control

		if (!isset($_SESSION['auth'])) { // if admin is loged bypass reserve timing rules
			if (strtotime($event->start_date) < time()) {
				echo $this->view->getTerm('event_expired');
				return;
			}// check if event expired
			//
			//
			//
			//
			//check if the user category can reserve
			$timeRuleFilter = ' cms_etick_reservtime.id_event = ' . $event->id;
			$reservation_time_rules = $this->model->getReserveTimeRules(-1, 0, $timeRuleFilter);

			if (!count($reservation_time_rules)) {
				$default = true;
				$reservation_end_time = $event->start_date;
			}//if are not configured the reservation time rules , the default end date of reservation will be the event start date ;
			else {
				$default = false;
				$user_category_reservation_time = $reservation_time_rules = $this->model->getReserveTimeRules(-1, 0, $timeRuleFilter . ' AND cms_etick_reservtime.id_user_category = ' . $user_category);
				if (count($user_category_reservation_time)) {
					$reservation_start_time = $user_category_reservation_time[0]->start_date;
					$reservation_end_time = $user_category_reservation_time[0]->end_date;
				} else { //if there is no rule only for that user category
					if (!isset($_SESSION['auth'])) {
					}
					echo $this->view->getTerm('this_user_category_cant_reserve');
					return;
				}
			}

			if ($default) {
				if (strtotime($reservation_end_time) < time()) { // if event start date is smaller than actual time ( time now is always greatter than 1 second ago LOL)
					echo $this->view->getTerm('reservation_time_finished');
					return;
				}
			} else {
				if (strtotime($reservation_start_time) > time() || strtotime($reservation_end_time) < time()) {
					echo $this->view->getTerm('this_user_category_can_reserve_between') . date(' d M Y H:i ', strtotime($reservation_start_time)) . ' - ' . date(' d M Y H:i ', strtotime($reservation_end_time)) . date(' d M Y H:i ', time());
					return;
				}
			}
		}

		$filter_get_price_rule = ' id_user_category = ' . $user_category . ' AND id_event = ' . $event->id . ' AND cms_etick_price.id_zona = ' . $seat->id_zona . ' ';
		$user_price_rule = $this->model->getPrices(-1, 0, $filter_get_price_rule);
		if (!count($user_price_rule)) { //if there is no price rule for that user category in that zone of the event
			echo $this->view->getTerm('this_user_category_cant_reserve_in_this_zone');
			return;
		}

		if ($seat->stato == 'LIBERO') {
			require_once DOCROOT . ENTITIES_PATH . 'Etick/EtickReservation.php';
			$reservation = new EtickReservation_Entity;
			$reservation->id_user = $user_id;
			$reservation->id_event = $event_id;
			$reservation->id_seat = $seat->seat_id;
			$reservation->confirmed = '0';
			$reservation->price = $seat->final_price;
			$inserted_id = $this->model->saveReservation($reservation);
			echo $this->view->getTerm('reservation_success') . '.<br/>Codice Prenotazione: <b>' . $inserted_id . '</b>';
			$newReservation = $this->model->getReservation($inserted_id);
			$this->sendEmail($newReservation, 'new');
			return $inserted_id;
		}
		echo $this->view->getTerm('seat_busy');
		return false;
		//if the seat is free , make the reservation
	}

	//************************* AJAX FUNCTIONS *******************//

	public function ajx_Reserve()
	{
		if (isset($_POST['elem_id'], $_POST['event_id'])) {
			$seat_name = $_POST['elem_id'];
			$id_event = $_POST['event_id'];
			$user = UserAuth::getLoginSession();
			$user_id_category = $user['id_category'];
			$seats = $this->model->getReservations(-1, 0, " eve.id = '$id_event' AND  seat.name = '$seat_name' ", '', $user_id_category);
			if (count($seats)) {
				$seat = $seats[0];
			} else {
				echo '[$seat_not_found]';
				return;
			}
			$reserv = $this->saveReservation($user['id'], $user['id_category'], $id_event, $seat);
		}
	}

	//***********FINISHED*********//
//
//
//

	public function deletePendingReservations()
	{
		$events = $this->model->getEvents(-1);
		//echo "<pre>";
		foreach ($events as $event) {
			$pending_time_limit = $event->pending_time_limit;
			$time_limit = date('Y-m-d H:i', strtotime('-' . $pending_time_limit . ' day', time()));

			$filter = " eve.id = '" . $event->id . "'";
			$filter .= " AND res.creation_date < '" . $time_limit . "' AND res.confirmed = '0'";

			$reservations = $this->model->getReservations(-1, 0, $filter);
			foreach ($reservations as $reservation) {
				$deleted = $this->model->ArchiveReservation($reservation);
				if ($deleted) {
					$this->sendEmail($reservation, 'expired');
					$log = 'Reservation deleted for passing pending time limit : ' . $reservation->id;
					$this->LogsManager->registerLog('Etick', 'delete', $log, $reservation->id);
				} else {
					$log = 'Reservation  delete failed : ' . $reservation->id;
					$this->LogsManager->registerLog('Etick', 'delete', $log, $reservation->id);
				}
				echo $log . "\n";
			}
		}
		//echo "</pre>";
	}

	//************************* COMENTED FUNCTIONS *******************//
	//    public function ajx_getSeats() {
	//        $zone = $_POST['zone'];
	//        $event = $_POST['event'];
	//        $place = $_POST['place'];
	//        $filter = '  seat.id_zona = ' . $zone;
	//        $seats = $this->model->getReservations($limit = 9999, $offset = 0, $filter);
	//        $i = 1;
	//        //echo '<div class = "row">';
	//        foreach ($seats as $seat) {
	//            if ($seat->stato == 'LIBERO') {
	//                echo '<a title="' . $seat->final_price . '" href="' . $this->getActionUrl('register_reservation') . '/' . $seat->seat_id . '?id_event=' . $_POST['event_id'] . '"> <div  class=" col-md-1 ' . $seat->stato . '">' . $seat->seatName . '</div></a>';
	//            } else {
	//                echo '<div  class=" col-md-1 ' . $seat->stato . '">' . $seat->seatName . '</div>';
	//            }
//
	//            if ($i == 12) {
	//                echo '</div> <div>';
	//                $i = 0;
	//            }
	//            $i++;
	//        }
	//        echo '</div>';
	//    }
	//    public function ajx_getPrice() {
	//        if (isset($_POST['price'])) {
	//            $seat = $_POST['seat'];
	//            $filter = ' seat.id = ' . $seat;
	//            $price = $this->model->getReservations($limit = 1, $offset = 0, $filter);
	//            echo $price[0]->final_price;
	//        }
	//    }
	//    public function register_reservation($id_seat = '') {
	//        if (!UserAuth::checkLoginSession())
	//            Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . Utils::curPageUrl());
	//        if ($id_seat != '') {
	//            require_once DOCROOT . ENTITIES_PATH . 'Etick/EtickReservation.php';
	//            $reservation = new EtickReservation_Entity;
	//            $reservation->id_user = $_SESSION['user_auth']['id'];
	//            $reservation->id_event = $_GET['id_event'];
	//            $reservation->id_seat = $id_seat;
	//            $reservation->confirmed = '0';
	//            $reservation->price = 10;
	//            $inserted_id = $this->model->saveReservation($reservation);
	//            if (!is_array($inserted_id)) {
	//                $sended = Email::sendMail($_SESSION['user_auth']['email'], CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, 'New reservation', 'Your reservation was successfully');
	//                $filter = ' seat.id = ' . $id_seat;
	//                $seat = $this->model->getReservations(1, 0, $filter);
	//                $this->view->addNotice('[$reservation_success]');
	//                $this->view->set('reservation', $reservation);
	//                $this->view->set('inserted_id', $inserted_id);
	//                $this->view->set('seat_zone', $seat[0]->zone_name);
	//                $this->view->set('seat_name', $seat[0]->seatName);
	//                $this->view->set('place_name', $seat[0]->place_name);
	//            } else {
	//                $this->view->addError('[$reservation_failed]');
	//            }
	//        } else {
	//            $this->view->addError('[$no_seat_selected]');
	//        }
	//        $this->view->render('register-reservation');
	//    }
	//    public function ajx_AddSeatToDb(){
//
//
	//       require_once ENTITIES_PATH."Etick/EtickSeat.php";
	//       $seat = new EtickSeat_entity();
//
	//       $element_id = $_POST['elem_id']  ;
	//       $seat->id_zona = 9;
	//       $seat->element_id = $element_id;
	//       $seat->name = $element_id;
//
	//       $result = $this->model->saveSeat($seat);
//
	//       print_r($result);
//
//
	//    }

	public function ajx_AddSeatPriceToDb()
	{
		//        require_once ENTITIES_PATH . "Etick/EtickPrice.php";
//        $price_rule = new EtickPrice_entity();
//        $price_rule->id_zona = $_POST['seat']['id_zona'];
//        $price_rule->id_event = 9;
//        $price_rule->price = $_POST['price'];
//        $price_rule->id_seat = $_POST['seat']['seat_id'];
//        $price_rule->id_user_category = 1;
//        $res = 0;
//        $result = $this->model->savePrice($price_rule);
//        if (!is_array($result)) {
//            $res = 1;
//        }
//        $price_rule->id_user_category = 2;
//        $result = $this->model->savePrice($price_rule);
//        if (!is_array($result)) {
//            $res = $res + 1;
//        }
//        echo $res;
	}

	/**
	 * Invia Email al cliente
	 * @param EtickReservation_Entity $reservation  Oggetto Reservation
	 * @param string $notify_reason ("expired" | "new" | "confirmed")
	 * @return bool Esito dell'invio mail
	 */
	private function sendEmail($reservation, $notify_reason)
	{
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH . 'Etick' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Etick' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_PHONE')->setVal(CMSSettings::$company_tel);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->set('reservation', $reservation);
		$message->renderTemplate(false);
		$to = $reservation->email;
		$from = $this->view->getTerm('reservation_info') . ' - ' . CMSSettings::$website_title;
		$from_name = 'noreply@' . CMSSettings::$webdomain;
		if ($notify_reason = 'expired') {
			$subject = $this->view->getTerm('reservation_expired') . 'ID : ' . $reservation->id;
			$testo = $message->render('mails/expired_reservation', true);
		}
		if ($notify_reason = 'new') {
			$subject = $this->view->getTerm('new_reservation_info') . 'ID : ' . $reservation->id;
			$testo = $message->render('mails/new_reservation', true);
		}
		if ($notify_reason = 'confirmed') {
			$subject = $this->view->getTerm('reservation_confirmed_info') . 'ID : ' . $reservation->id;
			$testo = $message->render('mails/confirmed_reservation', true);
		}
		return Email::sendMail($to, $from_name, $from, $subject, $testo);
	}

	private function cleanPendingReservations_timing()
	{
		if (file_exists(dirname(__FILE__) . '/cleanPending_lastupdate.php')) {
			include dirname(__FILE__) . '/cleanPending_lastupdate.php';
		} else {
			$cron_file = fopen(dirname(__FILE__) . '/cleanPending_lastupdate.php', 'w');
			fclose($cron_file);
		}
		if (!isset($cron_delete_peding)) {
			$cron_delete_peding = time();
			$var_str = var_export($cron_delete_peding, true);
			$var = "<?php\n\n\$cron_delete_peding = $var_str;\n\n";
			file_put_contents(dirname(__FILE__) . '/cleanPending_lastupdate.php', $var);
		} else {
			if ($cron_delete_peding + 60 * 30 < time()) {
				$this->deletePendingReservations();
				$cron_delete_peding = time();
				$var_str = var_export($cron_delete_peding, true);
				$var = "<?php\n\n\$cron_delete_peding = $var_str;\n\n";
				file_put_contents(dirname(__FILE__) . '/cleanPending_lastupdate.php', $var);
			}
		}
	}
}
