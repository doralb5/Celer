<style>
    .OCCUPATO {
        background: red;
        margin: 1px;
        padding: 35px;
    }

    .LIBERO {
        background: green;
        padding: 35px;
        margin: 1px;
    }

    .IMPEGNATO {
        background: greenyellow;
        margin: 1px;
        padding: 35px;
    }
</style>
<div id="new-service-div">
    <div class="col-md-12">
        <form id="reservation-form">
            <input type ='hidden' name='event_id' value="<?= $id?>">
            <div class="form-group">
                <label class="col-md-1 control-label">[$zone]</label>
                <div class="col-md-2">
                    <select class="form-control update" id="zone" name="zone">
                        <?php foreach ($zones as $id => $zone) {
	?>
                            <option value="<?= $zone->id ?>"><?= $zone->name ?></option>
                        <?php
} ?>
                    </select>
                </div>
            </div>
           
        </form>
    </div>
</div>

<div id="show_seats">
</div>

<script>

    $("#zone").change(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "<?= Utils::getComponentUrl('Etick/ajx_getSeats') ?>",
            data: $("#reservation-form").serialize(),
            //dataType: "JSON",
            success: function (data) {
                $('#show_seats').replaceWith($('#show_seats').html(data));
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }

        });
    });


    $(".update-seat").click(function (e) {
        e.preventDefault();
        var id_seat = this.id;
        $.ajax({
            type: 'POST',
            url: "<?= Utils::getComponentUrl('Etick/ajx_getPrice') ?>",
            data: { id_seat: id_seat },
            //dataType: "JSON",
            success: function (data) {
                $('#price').val(data);
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }

        });
    });

//    function addTmpService(zone, servicefull, id_zone, userCategory, seat, price) {
//        var count = $('#tblServicesList tr').length + 1;
//        $('#tblServicesList').append('<tr data-id="' + count + '"><td>'
//                + '<input type="hidden" name="zona[' + count + ']" value="' + id_zone + '" />'
//                + '<input type="hidden" name="userCategory[' + count + ']" value="' + userCategory + '" />'
//                + '<input type="hidden" name="seat[' + count + ']" value="' + seat + '" />'
//                + '<input type="hidden" name="price[' + count + ']" value="' + price + '" />'
//                + servicefull
//                + '</td><td><button class="btn-small" onclick="deleteService(' + count + ')">Rimuovi</button></td></tr>');
//
//        if ($('#tblServicesList').is(':hidden'))
//            $('#tblServicesList').show();
//    }
//    function deleteService(rowId) {
//        $('#tblServicesList').find('[data-id="' + rowId + '"]').remove();
//        if ($('#tblServicesList tr[data-id]').length == 0)
//            $('#tblServicesList').hide();
//    }
//    $('#btnAddService').click(function (e) {
//        var id_zone = $('#zone').val();
//        var userCategory = $('#userCategory').val();
//        var seat = $('#seat').val();
//        var price = $('#price').val();
//        if (id_zone !== '' && price !== '') {
//            var zone = $('#zone option:selected').text();
//            var servicefull = ' <b> Zona </b> : ' + zone;
//            if (userCategory !== '') {
//                var userCategoryLabel = $('#userCategory option:selected').text();
//                servicefull = servicefull + ' » ' + ' <b> Categoria Utente </b> : ' + userCategoryLabel;
//            }
//            if (seat !== '') {
//                var seatLabel = $('#seat option:selected').text();
//                servicefull = servicefull + ' » ' + ' <b>Sedia</b> : ' + seatLabel;
//            }
//            servicefull = servicefull + ' » ' + ' <b>Prezzo</b> : ' + price;
//            addTmpService(zone, servicefull, id_zone, userCategory, seat, price);
//        } else {
//            alert('Zone and price required');
//        }
//    });
</script>
