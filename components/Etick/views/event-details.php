<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/event-details.css'); ?>
<?php HeadHTML::addMeta('keywords', $event->name . ' , ' . $event->placeName . '  , ' . CMSSettings::$website_title . ' , ' . $event->address . ' , ' . $event->city); ?>
<?php HeadHTML::addMeta('description', StringUtils::CutString(strip_tags($event->description), 200)); ?>
<?php HeadHTML::addMetaProperty('og:title', $event->name . ' | ' . CMSSettings::$website_title); ?>
<?php HeadHTML::addMetaProperty('og:type', 'article'); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl("etick/$event->image", 800, 300, array('far' => '1', 'bg' => 'FFFFFF'))); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($event->description), 200)); ?>

<?php
$start_date = date('d M Y', strtotime($event->start_date));
$end_date = date('d M Y', strtotime($event->end_date));
$start_time = date('h:i a', strtotime($event->start_date));
$end_time = date('h:i a', strtotime($event->end_date));
?>

<script type="text/javascript">
    'function' != typeof loadGsLib && (loadGsLib = function () {
        var e = document.createElement("script");
        e.type = "text/javascript", e.async = !0, e.src = '//api.at.getsocial.io/widget/v1/gs_async.js?id=080ddc';
        var t = document.getElementsByTagName("script")[0];
        t.parentNode.insertBefore(e, t)
    })();
</script>

<div class="article_std_container">

    <?php
	Messages::showComponentMsg('Etick');
	Messages::showComponentErrors('Etick');
	?>

    <div class="row">
        <div class="col-md-12 article-img">   

            <?php if (!is_null($event->image)) {
		?>
                <div class="article-image">

                    <a class=""
                       href="<?= Utils::genThumbnailUrl("etick/$event->image", 1000, 0) ?>"
                       data-lightbox="image-1">
                        <img class="image"
                             src="<?= Utils::genThumbnailUrl("etick/$event->image", 800, 200, array('zc' => 1)) ?>"
                             width="100%"/>
                    </a>
                </div>
            <?php
	} ?>
            <div class="card__date">
                <span class="card__date__day">
                    <?php echo date('d', strtotime($event->start_date)); ?>
                </span>
                <span class="card__date__month">

                    <?php echo date('M', strtotime($event->start_date)); ?>
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">         
            <div class="article-body">         


                <div class="row">
                    <div class="col-md-4">
                        <div class="article-title">
                            <h3 class="">[$information]</h3>
                        </div>
                        <div class="content">
                            
                            
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>
                                        [$place_name]
                                    </td>
                                    <td>
                                        <?= $event->placeName ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        [$address]
                                    </td>
                                    <td>
                                        <?= $event->address ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        [$zip_code]
                                    </td>
                                    <td>
                                        <?= $event->zip_code ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        [$city]
                                    </td>
                                    <td>
                                        <?= $event->city ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        [$province]
                                    </td>
                                    <td>
                                        <?= $event->province ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <div class="article-footer">  
                            <?php if (strtotime($event->start_date) > time()) {
		?>
                                <a class="btn btn-lg btn-block" href="<?= Utils::getComponentUrl('Etick/addReservation/' . $event->id) ?>" role="button">[$SeatReservation]</a>
                            <?php
	} ?>
                        </div>
                    </div>
                    <div class="col-md-8">         

                        <div class="article-title">
                            <h3 class=""> <?= $event->name ?></h3>
                        </div>
                        <div class="content">
                            <?= $event->description ?>
                        </div>
                    </div>
                </div>

                <?php if ($start_date == $end_date) {
		?>

                    <div class="card__subtitle">
                        <div class="row event-detail">
                            <div class="col-md-6 col-sm-6 col-xs-12 left">
                                <p class="title">[$event_start]</p>
                                <span class="detail"><?= $start_date ?> - <?= $start_time ?></span>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 right">
                                <p class="title">[$event_end]</p>
                                <span class="detail"><?= $end_time ?> </span>
                            </div>

                        </div>
                    </div>
                <?php
	} else {
		?>

                    <div class="card__subtitle">
                        <div class="row event-detail">
                            <div class="col-md-6 col-sm-6 col-xs-12 left">
                                <p class="title">[$event_start]</p>
                                <span class="detail"><?= $start_date ?> - <?= $start_time ?></span>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 right">
                                <p class="title">[$event_end]</p>
                                <span class="detail"><?= $end_date ?> - <?= $end_time ?> </span>
                            </div>
                        </div>
                    </div>

                <?php
	} ?>
            </div>

            <div class="social-button">
                <div class="share-button">
                    <div class="row ">
                        <!-- Event Social Share -->
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="getsocial gs-inline-group"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>