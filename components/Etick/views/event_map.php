<?php
setlocale('LC_ALL', 'it_IT');
$start_date = date('d-m-Y', strtotime($event->start_date));
$end_date = date('d-m-Y', strtotime($event->end_date));
$start_time = date('H:i', strtotime($event->start_date));
$end_time = date('H:i', strtotime($event->end_date));
?>

<div class="header-title">
    <div class="row">
        <div class="col-md-12">
            <h3 class="new-events"><a href="<?= $url_event_details ?>"><?= $event->name ?></a></h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <h5>[$event_start] <hr class="hr-sublevel"> </h5>
            <h4><?= $start_date ?> - <?= $start_time ?></h4>
        </div>
        <div class="col-md-3">
            <h5>[$event_end] <hr class="hr-sublevel"></h5>
            <h4><?= $end_date ?> - <?= $end_time ?></h4>
        </div>
        <div class="col-md-3">
            <h5>[$place_name] <hr class="hr-sublevel"></h5>
            <h4><?= $event->placeName ?></h4>
        </div>
        <?php if (count($myreservations) > 0) {
	?>
            <div class="col-md-3 text-center">
                <h4>[$MyReservations] </h4> <a href="<?= Utils::getComponentUrl('Etick/list_reservation_user') ?>"><span class="seat-nr"><?= count($myreservations) ?></span></a>
            </div>
        <?php
} ?>
    </div>
</div>



<?php
Messages::showComponentMsg('Etick');
Messages::showComponentErrors('Etick');
?>


<div class="row" >
    <div class="col-md-12 col-lg-12 col-xs-12" style=" height:100vh;" >
        <?php if (isset($event->svg_map) && ($event->svg_map != null || $event->svg_map != '')) {
	echo $event->svg_map;
} ?>
    </div>
</div>


<style>
    .header-title {
        background: white;
        padding: 15px 15px;
        box-shadow: 0 1px 3px rgba(0,0,0,0.1);
        margin-bottom: 0px;
        margin: 0px;
    }
    hr.hr-sublevel {
        margin-top: 5px;
        margin-bottom: 10px;
    }
    .seat-nr {
        padding: 3px 0px;
        width: 100%;
        display: table;
        font-size: 20px;
        background: #ec008c;
        color: white;
        text-align: center;
    }
    .first-anchor {
        background: #ec008c;
        padding: 15px 15px;
        margin-bottom: 20px;
        color: white;
        font-size: 18px;
        border-radius: 0;
        overflow: hidden;
        text-transform: uppercase;
        box-shadow: 0px 1px 3px 0px rgba(0,0,0,0.1);
    }
    .first-anchor:hover{
        color: white;
    }
    #svg-id {
        background: #ffffff;
        margin-top: 20px;
    }
    #svg-id #map text {
        fill: #08291c !important;
    }
    .label{
        font-size: 200%;
    }
    .seat{
    }
    .seat.available:hover   {
        fill: green;
        cursor: pointer;
    }
    .seat.available:hover rect {
        fill: #ff23a5;
    }
    .seat.available rect {
        fill: #00b878;
    }
    .seat.busy rect {
        fill: #e92e49;
        cursor: not-allowed;
    }
    .seat.pending rect {
        fill: yellow;
        cursor: help;
    }




    .shadow-z-1 {
        -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
        -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
    }
    /* -- Material Design Table style -------------- */
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 2rem;
        background-color: #fff;
    }
    .table > thead > tr,
    .table > tbody > tr,
    .table > tfoot > tr {
        -webkit-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        transition: all 0.3s ease;
        border-bottom: 1px solid #f5f5f5;
    }
    .table > thead > tr > th,
    .table > tbody > tr > th,
    .table > tfoot > tr > th,
    .table > thead > tr > td,
    .table > tbody > tr > td{
        text-align: left;
        padding: 0;
        vertical-align: top;
        border-top: 0;
        -webkit-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        transition: all 0.3s ease;
    }
    .table > thead > tr > th {
        font-weight: bold;
        color: #ffffff;
        vertical-align: bottom;
        border-bottom: 1px solid rgba(0, 0, 0, 0.12);
        font-size: 16px;
    }


    .table .table {
        background-color: #fff;
    }
    .table .no-border {
        border: 0;
    }

    .table-bordered {
        border: 0;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td {
        border: 0;
        border-bottom: 1px solid #e0e0e0;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > thead > tr > td {
        border-bottom-width: 2px;
    }
    .table-striped > tbody > tr:nth-child(odd) > td,
    .table-striped > tbody > tr:nth-child(odd) > th {
        background-color: #f5f5f5;
    }
    .table-hover > tbody > tr:hover > td,
    .table-hover > tbody > tr:hover > th {
        background-color: rgba(0, 0, 0, 0.12);
    }


    thead{
        background-color: #ec008c;
    }
    thead td{
        font-weight: bold;
    }

    tfoot{
        background-color: #fbfbfb;
        border-top:1px solid gainsboro;
    }
    tfoot a{
        color: #000;
    }
    tfoot a:hover{
        color: #000 !important;
    }
    tfoot td:first-child{
        border-right:1px solid gainsboro;

    }

    tfoot a i{
        color: #333;
        font-size: 20px !important;
    }

</style>

<style>
    .modal{
        color:black;   
    }
    .modal-header {
        padding-bottom: 5px;
    }
    .modal-footer {
        padding: 0;
    }
    .modal-footer .btn-group button {
        height:40px;
        border-top-left-radius : 0;
        border-top-right-radius : 0;
        border: none;
        border-right: 1px solid #ddd;
    }
    .modal-footer .btn-group:last-child > button {
        border-right: 0;
    }
</style>

<div id="order" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
                <h4 class="modal-title" id="myModalLabel">[$selected_seat]</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" role="button">[$close]</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default btn-reservation" role="button">[$Reserve_this_seat]</button>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>



<div id="order-response" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
                <h4 class="modal-title" id="myModalLabel">[$confirm_reservation]</h4>
            </div>
            <div class="modal-body response">
            </div>
            <div class="modal-footer">
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group" role="group">
                        <button class="btn btn-primary" onClick="location.href = '<?= $url_my_reservations ?>'" role="button">[$my_reservations]</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default" onClick="window.location.reload();" data-dismiss="modal">[$close]</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!--<div class = "row">
    <input class = "col-md-2 col-md-offset-5" id="price_input">
</div>-->



<script src="https://ariutta.github.io/svg-pan-zoom/dist/svg-pan-zoom.js"></script> 
<script>
                            $(function () {
                                panZoomInstance = svgPanZoom('#svg-id', {
                                    zoomEnabled: true,
                                    controlIconsEnabled: true,
                                    fit: true,
                                    center: true,
                                    minZoom: 1.0
                                });

                                // zoom out
                                panZoomInstance.zoom(0.2)

                                $("#move").on("click", function () {
                                    // Pan by any values from -80 to 80
                                    panZoomInstance.panBy({x: Math.round(Math.random() * 160 - 80), y: Math.round(Math.random() * 160 - 80)})
                                });
                            })
</script>
<script>


    var seats = {
<?php
$i = 0;
$j = count($seats);
foreach ($seats as $seat) {
	echo "\n'" . $i . "':{";
	echo "'seat_id' : '" . $seat->seat_id . "',";
	echo "'elem_id' : '" . $seat->element_id . "',";
	echo "'id_zona' : '" . $seat->id_zona . "',";
	echo "'prezzo_finale' : '" . $seat->final_price . "',";
	echo "'nome_zona' : '" . $seat->zone_name . "',";
	echo "'stato' : '" . $seat->stato . "'";
	if ($i == $j - 1) {
		echo '}';
	} else {
		echo '},';
	}
	$i++;
}
?>

    };


    Object.size = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key))
                size++;
        }
        return size;
    };


    var size = Object.size(seats);
    for (var i = 0; i < size; i++) {
        var d = document.getElementById(seats[i]['elem_id']);
        d.classList.add(seats[i]['elem_id']);
        d.setAttribute('data-id', i);
        var myObj = $("#" + seats[i]['elem_id']);
        var myObjTitle = myObj.children("title");
        myObjTitle.html(
                'Posto ' + seats[i]['stato']);

        if (seats[i]['stato'] === "LIBERO") {
            d.classList.add('available');
        } else if (seats[i]['stato'] === "OCCUPATO") {
            d.classList.add('busy');
        } else {
            d.classList.add('pending');
        }
    }

    $(document).ready(function () {
        var event_id = "<?= $id ?>";
        $(".available").click(function () {
            $(".available").prop("disabled", true);
            var id = $(this).data("id");
            var seat = seats[id];
            var elem_id = seat['elem_id'];
            $('.modal-body').html('<table class="table table-mc-light-blue"><tr><td style="width:100px"><b>Nr. Posto:</b></td><td> ' + elem_id + '</td></tr><tr><td><b> Zona: </b></td> <td>' + seat['nome_zona'] + '</td></tr><tr><td><b> Stato: </b></td><td> ' + seat['stato'] + '</td></tr><tr><td><b> Prezzo: </b> </td><td>&euro; ' + seat['prezzo_finale'] + '</td></tr></table>');
            $('#order').modal('toggle');
            $('.btn-reservation').click(function () {
                $(".btn-reservation").prop("disabled", true);
                $.ajax({
                    type: 'POST',
                    url: "<?= Utils::getComponentUrl('Etick/ajx_Reserve') ?>",
                    data: {elem_id: elem_id, event_id: event_id},
                    //dataType: "JSON",
                    success: function (data) {
                        $('#order').modal('toggle');
                        $('.response').html(data);
                        $(".btn-reservation").prop("disabled", false);
                        $('#order-response').modal('toggle');
                        $(".available").prop("disabled", false);

                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            });
        });
    });


//    $(".seat").click(function () {
//        var elem_id = $(this).attr('id');
//        var id = $(this).data("id");
//        var seat = seats[id];
//        var price = $("#price_input").val();
//        var curr = $(this);
//        //$(this).html('<title class="seat-title"></title><rect x="0" y="0" width="30" height="30" rx="3" ry="3"  fill="blue"></rect><rect x="0" y="30" width="30" height="10" rx="3" ry="3"  fill="#999999"></rect><text x="15" y="20" transform="matrix(-1,0,0,-1,25.75,27.2813)" ></text>');
//        $.ajax({
//            type: 'POST',
//            url: "<?= Utils::getComponentUrl('Etick/ajx_AddSeatPriceToDb') ?>",
//            data: {seat: seat, price: price},
//            success: function (data) {
//                if (data === "2") {
//                    curr.find(".butt").css('fill', '#eaa32b');
//                    curr.find(".back").css('fill', '#eaa32b');
//                } else {
//                    curr.find(".butt").css('fill', 'red');
//                    curr.find(".back").css('fill', 'red');
//                }
//
//                console.log(data);
//
//            },
//            error: function (data) {
//                curr.find(".butt").css('fill', 'red');
//                curr.find(".back").css('fill', 'red');
//                console.log(data);
//            }
//        });
//    });


</script>


