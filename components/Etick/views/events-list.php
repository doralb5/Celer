<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/event-list.css'); ?>


<?php
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 400;

$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 350;
$button = (isset($parameters['button'])) ? $parameters['button'] : 'primary';
$cols_new_event = (isset($parameters['cols_new_event'])) ? $parameters['cols_new_event'] : 2;
$cols_old_event = (isset($parameters['cols_old_event'])) ? $parameters['cols_old_event'] : 3;
$col_size_new_event = 12 / $cols_new_event;
$col_size_old_event = 12 / $cols_old_event;

function fix_txt_length($string, $chars)
{
	$length = strlen(utf8_decode($string));
	if ($length > $chars) {
		return StringUtils::CutString(utf8_decode($string), $chars);
	}
	return $string;
}
?>


<style>
    .btn-group-sm>.btn, .btn-sm {
        padding: 5px 10px;
        font-size: 12px;
        /* line-height: 4; */
        border-radius: 3px;
        padding: 22px;
        text-align: center;
    }

    .thumbnail {
        background: aquamarine;
    }

</style>



<div id="news-list" class="row news_list">
    <div class="col-md-12 main-content">
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-header header-title new-events">[$upcoming_events]</h3>
            </div>

        </div>


        <div class="row">

            <?php
			$i = 0;
			foreach ($events as $event) {
				$start_date = date('d M Y', strtotime($event->start_date));
				$end_date = date('d M Y', strtotime($event->end_date));
				$start_time = date('h:i a', strtotime($event->start_date));
				$end_time = date('h:i a', strtotime($event->end_date)); ?>

                <?php if ($i % $cols_new_event == 0 && $i != 0) {
					?>
                    <div class="clearfix"></div>
                <?php
				} ?>

                <div class="col-md-<?= $col_size_new_event ?> news-item">
                    <article class="card">
                        <!--/* --------------- Image ---------------*/-->
                        <header class="card__thumb">
                            <?php if ($event->image != '') {
					?>
                                <a href="<?= Utils::getComponentUrl('Etick/eventDetails/' . $event->id) ?>">
                                    <img src="<?= Utils::genThumbnailUrl('etick/' . $event->image, $w, $h, array('zc' => 1)) ?>" >
                                </a>
                            <?php
				} ?>
                        </header>

                        <div class="card__date">
                            <span class="card__date__day">
                                <?php echo date('d', strtotime($event->start_date)); ?>
                            </span>
                            <span class="card__date__month">

                                <?php echo date('M', strtotime($event->start_date)); ?>
                            </span>
                        </div>
                        <!--/* --------------- Content ---------------*/-->
                        <div class="card__body">
                            <div class="card__category">
                                <?= $event->placeName ?>
                            </div>

                            <div class="card__title">
                                <a href="<?= Utils::getComponentUrl('Etick/eventDetails/' . $event->id) ?>">
                                    <?= fix_txt_length($event->name, 40); ?>
                                </a>
                            </div>
                            <?php if ($start_date == $end_date) {
					?>

                                <div class="card__subtitle">
                                    <div class="row event-detail">
                                        <div class="col-md-6 col-sm-6 col-xs-12 left">
                                            <p class="title">[$Date] [$and] [$time] [$event_start]</p>
                                            <span class="detail"><?= $start_date ?> - <?= $start_time ?></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 right">
                                            <p class="title">[$Time] [$event_end]</p>
                                            <span class="detail"><?= $end_time ?> </span>
                                        </div>

                                    </div>
                                </div>
                            <?php
				} else {
					?>

                                <div class="card__subtitle">
                                    <div class="row event-detail">
                                        <div class="col-md-6 col-sm-6 col-xs-12 left">
                                            <p class="title">[$event_start]</p>
                                            <span class="detail"><?= $start_date ?> - <?= $start_time ?></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 right">
                                            <p class="title">[$event_end]</p>
                                            <span class="detail"><?= $end_date ?> - <?= $end_time ?> </span>
                                        </div>
                                    </div>
                                </div>

                            <?php
				} ?>


                            <div class="card__description"><?= fix_txt_length($event->description, 300); ?></div>
                        </div>
                        <!--/* --------------- Tags ---------------*/-->
                        <footer class="card__footer">
                            <div class="row">
                                <?php if (strtotime($event->start_date) > time()) {
					?>
                                    <div class="col-md-6 col-sm-6 col-xs-6" style="border-right: 1px solid #eee;"> 
                                        <a href="<?= Utils::getComponentUrl('Etick/eventDetails/' . $event->id) ?>" class="btn btn-lg btn-block">[$show_details]</a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="reserve-event">
                                        <a href="<?= Utils::getComponentUrl('Etick/addReservation/' . $event->id) ?>" class="btn btn-lg btn-block ">[$reserve_ticket]</a>
                                        </div>
                                    </div>
                                <?php
				} else {
					?>
                                    <div class="col-md-6 col-sm-6 col-xs-6" style="border-right: 1px solid #eee;"> 
                                        <a href="<?= Utils::getComponentUrl('Etick/eventDetails/' . $event->id) ?>" class="btn btn-lg btn-block">[$show_details]</a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">  
                                        <div class="progress-event">
                                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                [$in_progress]
                                            </div>
                                        </div>
                                    </div>
                                <?php
				} ?>

                            </div>

                        </footer>

                    </article>

                </div>

                <?php
				$i++;
			}
			?>
        </div>


        <!--********************************************************************************************************************-->
<?php if (count($old_events)) {
				?>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-header header-title old-events">[$old_events]</h3>
            </div>

        </div>

        <div class="row">

            <?php
			$i = 0;
				foreach ($old_events as $event) {
					$start_date = date('d M Y', strtotime($event->start_date));
					$end_date = date('d M Y', strtotime($event->end_date));
					$start_time = date('h:i a', strtotime($event->start_date));
					$end_time = date('h:i a', strtotime($event->end_date)); ?>

                <?php if ($i % $cols_old_event == 0 && $i != 0) {
						?>
                    <div class="clearfix"></div>
                <?php
					} ?>

                <div class="col-md-<?= $col_size_old_event ?> news-item">
                    <article class="card">
                        <!--/* --------------- Image ---------------*/-->
                        <header class="card__thumb">
                            <?php if ($event->image != '') {
						?>
                                <a href="<?= Utils::getComponentUrl('Etick/eventDetails/' . $event->id) ?>">
                                    <img src="<?= Utils::genThumbnailUrl('etick/' . $event->image, $w, $h, array('zc' => 1)) ?>" >
                                </a>
                            <?php
					} ?>
                        </header>

                        <div class="card__date">
                            <span class="card__date__day">
                                <?php echo date('d', strtotime($event->start_date)); ?>
                            </span>
                            <span class="card__date__month">

                                <?php echo date('M', strtotime($event->start_date)); ?>
                            </span>
                        </div>
                        <!--/* --------------- Content ---------------*/-->
                        <div class="card__body">
                            <div class="card__category">
                                <?= $event->placeName ?>
                            </div>

                            <div class="card__title">
                                <a href="<?= Utils::getComponentUrl('Etick/eventDetails/' . $event->id) ?>">
                                    <?= fix_txt_length($event->name, 40); ?>
                                </a>
                            </div>
                            <?php if ($start_date == $end_date) {
						?>

                                <div class="card__subtitle">
                                    <div class="row event-detail">
                                        <div class="col-md-6 col-sm-6 col-xs-12 left">
                                            <p class="title">[$event_start]</p>
                                            <span class="detail"><?= $start_date ?> - <?= $start_time ?></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 right">
                                            <p class="title">[$event_end]</p>
                                            <span class="detail"><?= $end_time ?> </span>
                                        </div>

                                    </div>
                                </div>
                            <?php
					} else {
						?>

                                <div class="card__subtitle">
                                    <div class="row event-detail">
                                        <div class="col-md-6 col-sm-6 col-xs-12 left">
                                            <p class="title">[$event_start]</p>
                                            <span class="detail"><?= $start_date ?> - <?= $start_time ?></span>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12 right">
                                            <p class="title">[$event_end]</p>
                                            <span class="detail"><?= $end_date ?> - <?= $end_time ?> </span>
                                        </div>
                                    </div>
                                </div>

                            <?php
					} ?>


                            <div class="card__description"><?= fix_txt_length($event->description, 180); ?></div>
                        </div>
                        <!--/* --------------- Tags ---------------*/-->
                        <footer class="card__footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="<?= Utils::getComponentUrl('Etick/eventDetails/' . $event->id) ?>" class="btn btn-lg btn-block">[$show_details]</a>
                                </div>
                            </div>

                        </footer>

                    </article>

                </div>

                <?php
				$i++;
				} ?>
        </div>
<?php
			}?>






        <div class="clearfix visible-sm"></div>

        <!--/* --------------- Pagination ---------------*/-->
        <div class="col-md-12 pagination">
            <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
        </div>
    </div>
</div>



