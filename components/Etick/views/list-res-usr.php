<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/reservations-list.css'); ?>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

</script>

<style>
    #accordion {margin: 0; padding: 0;}
    .event-title { margin: 0;}
    .event-header {
        background: white;
        padding: 15px 15px;
        box-shadow: 0 1px 3px rgba(0,0,0,0.1);
        margin-bottom: 0px;
        margin: 0px;
        display: block;
        position: relative;
        overflow: hidden;
    }
    .event-header a i, table a i {
        color: #333;
        font-size: 20px !important;
    }
    .event-header .buttons { text-align: right; }
    .event-header .buttons a {
        margin-left: 5px;
    }
    .new-events {
        padding-bottom: 10px;
        margin: 10px 0px 10px;
        border-bottom: 1px solid #eee;
    }
    .btn.btn-cancel:hover i{color: #b00000;}
</style>


<div id="main-nano-wrapper" class="nano">
    <div class="nano-content">
        
        
        <?php $this->showComponentErrors(); ?>
        <?php $this->showComponentMessages(); ?>
        
        <?php
			if (count($events) == 0) {
				?>

        <div class="col-md-6 col-md-offset-3 text-center">
            [$NoReservation]
        </div>
        <div style="clear:both;"><br/><br/></div>
        <div class="text-center">
            <a href="<?=$url_events_list?>" class="btn btn-default">[$Go_to_events]</a>
        </div>

        <?php
			}
		?>
        
        <ul id="accordion" class="message-list">
             
            <?php

			$msg = 0;
			foreach ($events as $eve) {
				?>
            <iframe src="<?=  Utils::getComponentUrl('Etick/printUserReservations') . '/' . $eve->id?>" style="display:none;" name="frame_<?=$msg?>"></iframe>

                <div class="panel">
                    <li class="" id="<?= $eve->id ?>">
                        <div class="row">

                            <div class="event-header">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#msg<?= $msg ?>"><h3 class="event-title"><?= $eve->name ?></h3></a>
                                            <?= $eve->placeName ?> - <?= date('d/m/Y - H:i', strtotime($eve->start_date)) ?>
                                        </div>
                                        <div class="col-md-3 buttons">
                                            <a class="btn" href="<?= Utils::getComponentUrl('Etick/showMyEventReservation/' . $eve->id) ?>"  data-toggle="tooltip" data-placement="top" title="Mostra i miei posti sulla mappa"><i class="fa fa-eye" ></i></a>
                                            <a class="btn" href="#" onclick="frames['frame_<?=$msg?>'].print();return false;" data-toggle="tooltip" data-placement="top" title="Stampa"><i class="fa fa-print" aria-hidden="true"></i></a>
                                            <a class="btn" href="<?=  Utils::getComponentUrl('Etick/sendReservationsByEmail') . '/' . $eve->id?>" data-toggle="tooltip" data-placement="top" title="Invia Mail con riepilogo"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </li>


                    <div id="msg<?= $msg ?>" class="collapse">
                        <div  class="message-container">

                            <div class="received">

                                <div id="res_<?= $eve->id ?>" class="table-responsive-vertical shadow-z-1">
                                    <!-- Table starts here -->
                                    <table id="table" class="table table-hover table-mc-light-blue">

                                        <thead>
                                            <tr>
                                                <th>[$id]</th>
                                                <th>[$reservation_time]</th>
                                                <th>[$seat]</th>
                                                <th>[$zone]</th>
                                                <th>[$price]</th>
                                                <th>[$confirmed]</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($eve->reservations as $res) {
					?>
                                            <tr>
                                                <td data-title="<?= $res->event_name ?>"><?= $res->id ?></td>
                                                <td data-title="<?= $res->event_name ?>"><?= date('d/m/Y - H:i', strtotime($res->creation_date)) ?></td>
                                                <td data-title="<?= $res->event_name ?>"><?= $res->seat_name ?></td>
                                                <td data-title="<?= $res->event_name ?>"><?= $res->zone_name ?></td>
                                                <td data-title="<?= $res->event_name ?>"><?= Utils::price_format($res->price, 'EUR'); ?></td>
                                                <td data-title="<?= $res->event_name ?>">
                                                    <?php
													if ($res->confirmed) {
														echo '[$yes_option]';
													} else {
														echo '[$no_option]'; ?> 

                                                        <div class="pull-right">
                                                            <a class="btn btn-block btn-cancel" href="#" data-toggle="modal" data-target="#confirm-delete-<?= $res->id ?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                        </div>
                                                        
                                                        
                                                        <!-- Modal Delete Confirmation-->
                                                        <div class="modal fade" id="confirm-delete-<?= $res->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        [$delete_title]
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        [$delete_question]<br/><br/>
                                                                        
                                                                        <table class="table">
                                                                            <tr><td style="width:100px"><b>Evento:</b></td><td><?= $eve->name ?></td></tr>
                                                                            <tr><td><b>Id:</b></td><td><?= $res->id ?></td></tr>
                                                                            <tr><td><b>Nr. Posto:</b></td><td><?= $res->seat_name ?></td></tr>
                                                                            <tr><td><b> Zona: </b></td> <td><?= $res->zone_name ?></td></tr>
                                                                        </table>

                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-primary" data-dismiss="modal">[$cancel]</button>
                                                                        <a class="btn btn-danger btn-ok" href="<?= Utils::getComponentUrl('Etick/delete_reservation/' . $res->id) ?>">[$delete]</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                    
                                                    <?php
													} ?>
                                                </td>
                                            </tr>
                                            <?
				} ?>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            
            
            
                <?php
				$msg++; ?>




            <?php
			}
			?> 
        </ul>
    </div>
</div>


<script>
$(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip();
});
</script>