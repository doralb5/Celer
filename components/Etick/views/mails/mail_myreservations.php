<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
    <style>
      /* -------------------------------------
          GLOBAL RESETS
      ------------------------------------- */
      img {
        border: none;
        -ms-interpolation-mode: bicubic;
        max-width: 100%; }
      body {
        /*background-color: #f6f6f6;*/
        background-color: #3c3c3c;
        font-family: sans-serif;
        -webkit-font-smoothing: antialiased;
        font-size: 14px;
        line-height: 1.4;
        margin: 0;
        padding: 0; 
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%; }
      table {
        border-collapse: separate;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        width: 100%; }
        table td {
          font-family: sans-serif;
          font-size: 14px;
          vertical-align: top; }
      /* -------------------------------------
          BODY & CONTAINER
      ------------------------------------- */
      .body {
        background-color: #f6f6f6;
        width: 100%; }
      /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
      .container {
        display: block;
        Margin: 0 auto !important;
        /* makes it centered */
        max-width: 580px;
        padding: 10px;
        width: 580px; }
      /* This should also be a block element, so that it will fill 100% of the .container */
      .content {
        box-sizing: border-box;
        display: block;
        Margin: 0 auto;
        max-width: 580px;
        padding: 10px; }
      /* -------------------------------------
          HEADER, FOOTER, MAIN
      ------------------------------------- */
      .main {
        background: #fff;
        border-radius: 3px;
        width: 100%; }
      .wrapper {
        box-sizing: border-box;
        padding: 20px; }
      .footer {
        clear: both;
        padding-top: 10px;
        text-align: center;
        width: 100%; }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
          color: #999999;
          font-size: 12px;
          text-align: center; }
      /* -------------------------------------
          TYPOGRAPHY
      ------------------------------------- */
      h1,
      h2,
      h3,
      h4 {
        color: #000000;
        font-family: sans-serif;
        font-weight: 400;
        line-height: 1.4;
        margin: 0;
        Margin-bottom: 30px; }
      h1 {
        font-size: 35px;
        font-weight: 300;
        text-align: center;
        text-transform: capitalize; }
      p,
      ul,
      ol {
        font-family: sans-serif;
        font-size: 14px;
        font-weight: normal;
        margin: 0;
        Margin-bottom: 15px; }
        p li,
        ul li,
        ol li {
          list-style-position: inside;
          margin-left: 5px; }
      a {
        color: #3498db;
        text-decoration: underline; }
      /* -------------------------------------
          BUTTONS
      ------------------------------------- */
      .btn {
        box-sizing: border-box;
        width: 100%; }
        .btn > tbody > tr > td {
          padding-bottom: 15px; }
        .btn table {
          width: auto; }
        .btn table td {
          background-color: #ffffff;
          border-radius: 5px;
          text-align: center; }
        .btn a {
          background-color: #ffffff;
          border: solid 1px #3498db;
          border-radius: 5px;
          box-sizing: border-box;
          color: #3498db;
          cursor: pointer;
          display: inline-block;
          font-size: 14px;
          font-weight: bold;
          margin: 0;
          padding: 12px 25px;
          text-decoration: none;
          text-transform: capitalize; }
      .btn-primary table td {
        background-color: #3498db; }
      .btn-primary a {
        background-color: #3498db;
        border-color: #3498db;
        color: #ffffff; }
      /* -------------------------------------
          OTHER STYLES THAT MIGHT BE USEFUL
      ------------------------------------- */
      .last {
        margin-bottom: 0; }
      .first {
        margin-top: 0; }
      .align-center {
        text-align: center; }
      .align-right {
        text-align: right; }
      .align-left {
        text-align: left; }
      .clear {
        clear: both; }
      .mt0 {
        margin-top: 0; }
      .mb0 {
        margin-bottom: 0; }
      .preheader {
        color: transparent;
        display: none;
        height: 0;
        max-height: 0;
        max-width: 0;
        opacity: 0;
        overflow: hidden;
        mso-hide: all;
        visibility: hidden;
        width: 0; }
      .powered-by a {
        text-decoration: none; }
      hr {
        border: 0;
        border-bottom: 1px solid #f6f6f6;
        Margin: 20px 0; }
      /* -------------------------------------
          RESPONSIVE AND MOBILE FRIENDLY STYLES
      ------------------------------------- */
      @media only screen and (max-width: 620px) {
        table[class=body] h1 {
          font-size: 28px !important;
          margin-bottom: 10px !important; }
        table[class=body] p,
        table[class=body] ul,
        table[class=body] ol,
        table[class=body] td,
        table[class=body] span,
        table[class=body] a {
          font-size: 16px !important; }
        table[class=body] .wrapper,
        table[class=body] .article {
          padding: 10px !important; }
        table[class=body] .content {
          padding: 0 !important; }
        table[class=body] .container {
          padding: 0 !important;
          width: 100% !important; }
        table[class=body] .main {
          border-left-width: 0 !important;
          border-radius: 0 !important;
          border-right-width: 0 !important; }
        table[class=body] .btn table {
          width: 100% !important; }
        table[class=body] .btn a {
          width: 100% !important; }
        table[class=body] .img-responsive {
          height: auto !important;
          max-width: 100% !important;
          width: auto !important; }}
      /* -------------------------------------
          PRESERVE THESE STYLES IN THE HEAD
      ------------------------------------- */
      @media all {
        .ExternalClass {
          width: 100%; }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
          line-height: 100%; }
        .apple-link a {
          color: inherit !important;
          font-family: inherit !important;
          font-size: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
          text-decoration: none !important; } 
        .btn-primary table td:hover {
          background-color: #34495e !important; }
        .btn-primary a:hover {
          background-color: #34495e !important;
          border-color: #34495e !important; } }
    </style>
  </head>
  <body class="">
      
    <table border="0" cellpadding="30" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <a href="http://{URL_WEBSITE}">
                        <img src="{URL_LOGO}" width="150" class="flexibleImage" style="max-width:150px;width:100%;display:block;" alt="{URL_WEBSITE}" title="{URL_WEBSITE}">
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
      
    <table border="0" cellpadding="0" cellspacing="0" class="body">
      <tr>
        <td>&nbsp;</td>
        <td class="container">
          <div class="content">

            <!-- START CENTERED WHITE CONTAINER -->
            <span class="preheader">Preview</span>
            <table class="main">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper">
                    
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>
                        <p>Ciao <?=$reservation[0]->firstname?>,</p>
                        <p>Questo è il riepilogo della tua prenotazione effettuata online.</p>
                        <p>
                            
                        </p>
                        <p>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td><b>[$EventName]</b></td> <td><?= $event->name ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>[$Location]</b></td> <td><?= $event->placeName ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>[$event_start_date]</b></td> <td><?= date('d', strtotime($event->start_date)) . ' ' . Utils::WordMonth(date('m', strtotime($event->start_date)), FCRequest::getLang()) . ' ' . date('Y H:i', strtotime($event->start_date)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>[$event_end_date]</b></td> <td> <?= date('d', strtotime($event->end_date)) . ' ' . Utils::WordMonth(date('m', strtotime($event->end_date)), FCRequest::getLang()) . ' ' . date('Y H:i', strtotime($event->end_date)); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </p>
                        <p>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <th>[$id]</th>
                                        <th>[$ReservationDate]</th>
                                        <th>[$Zone]</th>
                                        <th>[$Seat]</th>
                                        <th>[$Price]</th>
                                        <th>[$State]</th>
                                    </tr>
                                    <?php foreach ($reservation as $res) {
	?>
                                        <tr>
                                            <td><?= $res->id ?></td>
                                            <td><?= date('d', strtotime($res->creation_date)) . ' ' . Utils::WordMonth(date('m', strtotime($res->creation_date)), FCRequest::getLang()) . ' ' . date('Y H:i', strtotime($res->creation_date)); ?></td>
                                            <td><?= $res->zone_name ?></td>
                                            <td><?= $res->seat_name ?></td>
                                            <td><?= $res->final_price ?></td>
                                            <td><?= $res->stato ?></td>
                                        </tr>
                                    <?php
} ?>
                                </tbody>
                            </table>
                        </p>
                        <p>
                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                          <tbody>
                            <tr>
                              <td align="left">
                                <table border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                    <tr>
                                        <td style="text-align:center" align="center"> <a href="<?= Utils::getComponentUrl('Etick/list_reservation_user')?>" target="_blank">Gestisci le prenotazioni</a> </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        </p>
                        <p>Ti ricordiamo che le prenotazioni IMPEGNATE devono essere confermate recandosi presso la nostra sede. Qualora non venissero confermate entro <?=$event->pending_time_limit?> giorni dalla data di prenotazione saranno automaticamente cancellate ed i posti risulteranno nuovamente disponibili per nuove prenotazioni.</p>
                        <p>Per qualsiasi informazione contatta il nostro servizio clienti al numero <?= CMSSettings::$company_tel?> o scrivi una mail a <?= CMSSettings::$company_mail?>.</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>

            <!-- START FOOTER -->
            <div class="footer">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="content-block">
                      <span class="apple-link"><?= CMSSettings::$company_name?>, <?= CMSSettings::$company_address?></span>
                      <br>Questo messaggio è stato generato automaticamente dal sistema di prenotazioni <a href="http://<?= CMSSettings::$webdomain?>"><?= CMSSettings::$webdomain?></a>
                  </td>
                </tr>
                <tr>
                  <td class="content-block powered-by">
                      Powered by <a href="http://www.bluehat.al">BlueHAT</a>.
                  </td>
                </tr>
              </table>
            </div>
            <!-- END FOOTER -->
            
          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </body>
</html>