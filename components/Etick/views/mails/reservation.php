<style>
    .shadow-z-1 {
        -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
        -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
    }
    /* -- Material Design Table style -------------- */
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 2rem;
        background-color: #fff;
    }
    .table > thead > tr,
    .table > tbody > tr,
    .table > tfoot > tr {
        -webkit-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        transition: all 0.3s ease;
    }
    .table > thead > tr > th,
    .table > tbody > tr > th,
    .table > tfoot > tr > th,
    .table > thead > tr > td,
    .table > tbody > tr > td{
        text-align: left;
        padding: 1.5rem;
        vertical-align: top;
        border-top: 0;
        -webkit-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        transition: all 0.3s ease;
    }
    .table > thead > tr > th {
        font-weight: bold;
        color: #ffffff;
        vertical-align: bottom;
        border-bottom: 1px solid rgba(0, 0, 0, 0.12);
        font-size: 16px;
    }


    .table .table {
        background-color: #fff;
    }
    .table .no-border {
        border: 0;
    }

    .table-bordered {
        border: 0;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td {
        border: 0;
        border-bottom: 1px solid #e0e0e0;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > thead > tr > td {
        border-bottom-width: 2px;
    }
    .table-striped > tbody > tr:nth-child(odd) > td,
    .table-striped > tbody > tr:nth-child(odd) > th {
        background-color: #f5f5f5;
    }
    .table-hover > tbody > tr:hover > td,
    .table-hover > tbody > tr:hover > th {
        background-color: rgba(0, 0, 0, 0.12);
    }


    thead{
        background-color: #ec008c;
    }
    thead td{
        font-weight: bold;
    }

    tfoot{
        background-color: #fbfbfb;
        border-top:1px solid gainsboro;
    }
    tfoot a{
        color: #000;
    }
    tfoot a:hover{
        color: #000 !important;
    }
    tfoot td:first-child{
        border-right:1px solid gainsboro;

    }

    tfoot a i{
        color: #333;
        font-size: 20px !important;
    }


</style>

<table id="table" class="table table-hover table-mc-light-blue">
    <thead>
        <tr>
            <th>[$event_name]</th>
            <th><?= $res->event_name ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-title="[$event_name]">[$id_reservation]</td>
            <td data-title="<?= $res->event_name ?>"><?= $res->id ?></td>
        </tr>
        <tr>
            <td data-title="[$event_name]">[$place]</td>
            <td data-title="<?= $res->event_name ?>"><?= $res->place_name ?></td>
        </tr>
        <tr>
            <td data-title="[$event_name]">[$event_start_date]</td>
            <td data-title="<?= $res->event_name ?>"><?= date(' d M Y', strtotime($res->event_start_date)) ?></td>
        </tr>
        <tr>
            <td data-title="[$event_name]">[$event_start_time]</td>
            <td data-title="<?= $res->event_name ?>"><?= date(' H:i a', strtotime($res->event_start_date)) ?></td>
        </tr>
        <tr>
            <td data-title="[$event_name]">[$seat]</td>
            <td data-title="<?= $res->event_name ?>"><?= $res->seat_name ?></td>
        </tr>
        <tr>
            <td data-title="[$event_name]">[$zone]</td>
            <td data-title="<?= $res->event_name ?>"><?= $res->zone_name ?></td>
        </tr>
        <tr>
            <td data-title="[$event_name]">[$price]</td>
            <td data-title="<?= $res->event_name ?>"><?= $res->price ?></td>
        </tr>
        <tr>
            <td data-title="[$event_name]">[$reservation_time]</td>
            <td data-title="<?= $res->event_name ?>"><?= $res->creation_date ?></td>
        </tr>
        <tr>
            <td data-title="[$event_name]">[$confirmed]</td>
            <td data-title="<?= $res->event_name ?>"><?php
				if ($res->confirmed) {
					echo '[$yes_option]';
				} else {
					echo '[$no_option]';
				}
				?></td>
        </tr>
    </tbody>
</table>