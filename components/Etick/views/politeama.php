
<a class="btn btn-block"href="<?= Utils::getComponentUrl('Etick/list_reservation_user')?>"> [$my?_reservations] </a>

<div id="container"  class="col-md-12 col-lg-12 col-xs-12" style=" height:100vh;" >
    <?if (isset($event->svg_map) && ($event->svg_map != null || $event->svg_map != '')) {
	echo  $event->svg_map;
} ?>
</div>



<style>
    .label{
        font-size: 200%;
    }

    .seat{

    }
    .seat.available:hover   {
        fill: green;
        cursor: pointer;
    }
    .seat.available:hover rect {
        fill: #9A8899;
    }

    .seat.available rect {
        fill: green;
    }

    .seat.busy rect {
        fill: #FF0000;
        cursor: not-allowed;
    }
    .seat.pending rect {
        fill: yellow;
        cursor: help;
    }


</style>



<div id="order" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>



<div id="order-response" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body response">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="window.location.reload();" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script src="https://ariutta.github.io/svg-pan-zoom/dist/svg-pan-zoom.js"></script> 
<script>
    $(function () {
        panZoomInstance = svgPanZoom('#svg-id', {
            zoomEnabled: true,
            controlIconsEnabled: true,
            fit: true,
            center: true,
            minZoom: 1.0
        });

        // zoom out
        panZoomInstance.zoom(0.2)

        $("#move").on("click", function () {
            // Pan by any values from -80 to 80
            panZoomInstance.panBy({x: Math.round(Math.random() * 160 - 80), y: Math.round(Math.random() * 160 - 80)})
        });
    })
</script>
<script>


    var seats = {
<?php
$i = 0;
$j = count($seats);
foreach ($seats as $seat) {
	echo "\n'" . $i . "':{";
	echo "'seat_id' : '" . $seat->seat_id . "',";
	echo "'elem_id' : '" . $seat->element_id . "',";
	echo "'id_zona' : '" . $seat->id_zona . "',";
	echo "'prezzo_finale' : '" . $seat->final_price . "',";
	echo "'nome_zona' : '" . $seat->zone_name . "',";
	echo "'stato' : '" . $seat->stato . "'";
	if ($i == $j - 1) {
		echo '}';
	} else {
		echo '},';
	}
	$i++;
}
?>

    };


    Object.size = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key))
                size++;
        }
        return size;
    };


    var size = Object.size(seats);
    for (var i = 0; i < size; i++) {
        var d = document.getElementById(seats[i]['elem_id']);
        d.classList.add(seats[i]['elem_id']);
        d.setAttribute('data-id', i);
        var myObj = $("#" + seats[i]['elem_id']);
        var myObjTitle = myObj.children("title");
        myObjTitle.html(
                '<b>Stato:</b> ' + seats[i]['stato'] +
                '<br>' +
                '<b>Prezzo:</b> ' + seats[i]['prezzo_finale']

                );

        if (seats[i]['stato'] === "LIBERO") {
            d.classList.add('available');
        } else if (seats[i]['stato'] === "OCCUPATO") {
            d.classList.add('busy');
        } else {
            d.classList.add('pending');
        }


    }

    $(document).ready(function () {
        var event_id = "<?= $id ?>";
        $(".available").click(function () {
            var id = $(this).data("id");
            var seat = seats[id];
            var elem_id = seat['elem_id'];
            $('.modal-body').html('<br><b> Zona: </b> ' + seat['nome_zona'] + '<br> <b> Posto: </b> ' + elem_id + '<br><b> Stato: </b> ' + seat['stato'] + '<br><b> Prezzo: </b> ' + seat['prezzo_finale']  );
            $('#order').modal('toggle');
            $('.btn-primary').click(function () {
                $.ajax({
                    type: 'POST',
                    url: "<?= Utils::getComponentUrl('Etick/ajx_addSeat') ?>",
                    data: {elem_id: elem_id, event_id: event_id},
                    //dataType: "JSON",
                    success: function (data) {
                        $('#order').modal('toggle');
                            $('.response').html(data);
                            $('#order-response').modal('toggle');
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });



            });






        });
    });


</script>


