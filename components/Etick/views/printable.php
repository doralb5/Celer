<!DOCTYPE html>
<html>
    <head>
        <title>Dettagli Prenotazione</title>
    </head>
    
    <body>
        
        

<?php
if (count($reservations)) {
	$reservation = $reservations[0]; ?>

    <style>
        body th{
            text-align: center;
        }

        th
        {
            font-size: 14px;
            font-weight: normal;
            color: #039;
            padding: 10px 8px;
            border-bottom: 2px solid #6678b1;
        }
        td
        {
            color: #669;
            padding: 9px 8px 0px 8px;
        }
        tbody tr:hover td
        {
            color: #009;
        }
    </style>
    <table>
        <tbody>
            <tr>
                <td><h2><b>[$UserInfo]</b></h2></td>
            </tr>

            <tr>
                <td><b>[$FirstName]</b></td> <td><?= $reservation->firstname ?></td>
            </tr> <tr>
                <td><b>[$Lastname]</b></td> <td><?= $reservation->lastname ?></td>
            </tr> <tr>
                <td><b>[$Email]</b></td> <td><?= $reservation->email ?></td>
            </tr>
        </tbody>
    </table>


    <br><br>
    <table>
        <tbody>
            <tr>
                <td><h2><b>[$EventInfo]</b></h2></td>
            </tr>

            <tr>
                <td><b>[$EventName]</b></td> <td><?= $reservation->event_name ?></td>
            </tr>
            <tr>
                <td><b>[$Location]</b></td> <td><?= $reservation->place_name ?></td>
            </tr>
            <tr>
                <td><b>[$event_start_date]</b></td> <td><?= date('d', strtotime($reservation->event_start_date)) . ' ' . Utils::WordMonth(date('m', strtotime($reservation->event_start_date)), FCRequest::getLang()) . ' ' . date('Y H:i', strtotime($reservation->event_start_date)); ?></td>
            </tr>
            <tr>
                <td><b>[$event_end_date]</b></td> <td> <?= date('d', strtotime($reservation->event_end_date)) . ' ' . Utils::WordMonth(date('m', strtotime($reservation->event_end_date)), FCRequest::getLang()) . ' ' . date('Y H:i', strtotime($reservation->event_end_date)); ?></td>
            </tr>

        </tbody>
    </table>

    <br><br>
    <table>
        <tbody> 
            <tr>
                <td><h2><b>[$Reservations]</b></h2></td>
            </tr>
            <tr>
                <th>[$id]</th>
                <th>[$ReservationDate]</th>
                <th>[$Zone]</th>
                <th>[$Seat]</th>
                <th>[$Price]</th>
                <th>[$State]</th>
            </tr>
            <?php foreach ($reservations as $reservation) {
		?>
                <tr>
                    <td><?= $reservation->id ?></td>
                    <td><?= date('d', strtotime($reservation->creation_date)) . ' ' . Utils::WordMonth(date('m', strtotime($reservation->creation_date)), FCRequest::getLang()) . ' ' . date('Y H:i', strtotime($reservation->creation_date)); ?></td>
                    <td><?= $reservation->zone_name ?></td>
                    <td><?= $reservation->seat_name ?></td>
                    <td><?= $reservation->final_price ?></td>
                    <td><?= $reservation->stato ?></td>
                </tr>
            <?php
	} ?>
        </tbody></table>

    <?php
}?>

        </body>
</html>