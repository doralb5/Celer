<?php

require_once LIBS_PATH . 'StringUtils.php';

class Events_Component extends BaseComponent
{
	public function add_event()
	{
		$event = new Event_Entity();

		if (isset($_POST['save'])) {
			($_POST['title'] == '') ? $this->AddError('Title is required!') : '';
			($_POST['description'] == '') ? $this->AddError('Description is required!') : '';
			($_POST['id_category'] == '') ? $this->AddError('Category is required!') : '';
			($_POST['start_date'] == '') ? $this->AddError('Start Date is required!') : '';
			($_POST['pay'] == '') ? $this->AddError('Pay is required!') : '';

			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('Please upload an image!') : '';
			}

			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time() . '_' . $_FILES['image']['name'];
				} else {
					$_POST['image'] = $event->image;
				}

				if (strlen($_FILES['file']['tmp_name'])) {
					$_POST['file'] = time() . '_' . $_FILES['file']['name'];
				} else {
					$_POST['file'] = $event->file;
				}

				$event->title = $_POST['title'];
				$event->description = $_POST['description'];
				$event->image = $_POST['image'];
				$event->id_category = $_POST['id_category'];
				$event->pay = $_POST['pay'];
				$event->cost = $_POST['cost'];
				$event->currency = $_POST['currency'];
				$event->start_date = $_POST['start_date'];
				$event->end_date = ($_POST['end_date'] != '') ? $_POST['end_date'] : $event->end_date;
				$event->organizer = $_POST['organizer'];
				$event->location = $_POST['location'];
				$event->address = $_POST['address'];
				$event->phone = $_POST['phone'];
				$event->email = $_POST['email'];
				$event->website = $_POST['website'];
				$event->file = $_POST['file'];
				$event->coordinates = ltrim(rtrim($_POST['coordinates'], ')'), '(');
				$event->featured = $_POST['featured'];

				$inserted_id = $this->model->saveEvent($event);

				if (!is_array($inserted_id)) {
					$this->AddNotice('Event has been saved successfully.');
					$this->NewEventNotification($event);

					if (strlen($_FILES['image']['tmp_name'])) {
						$target = DOCROOT . MEDIA_ROOT . DS . 'events' . DS . $event->id_category;
						Utils::createDirectory($target);
						$filename = $target . DS . $_POST['image'];
						move_uploaded_file($_FILES['image']['tmp_name'], $filename);
					}
					if (strlen($_FILES['file']['tmp_name'])) {
						$target = DOCROOT . MEDIA_ROOT . DS . 'events' . DS . $event->id_category . DS . 'files';
						Utils::createDirectory($target);
						$filename = $target . DS . $_POST['file'];
						move_uploaded_file($_FILES['file']['tmp_name'], $filename);
					}

					//Shtojme imazhet shtese
					if (isset($_FILES['images']['tmp_name']) && count($_FILES['images']['tmp_name'])) {
						$failures = 0;
						for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
							$tmp_name = time() . '_' . $_FILES['images']['name'][$i];

							$addImage = new EventImage_Entity();

							$addImage->id_event = $inserted_id;
							$addImage->image = $tmp_name;
							$addImage->size = $_FILES['images']['size'][$i];
							$res = $this->model->saveEventImage($addImage);

							if (!is_array($res)) {
								$target = DOCROOT . MEDIA_ROOT . 'events' . DS . $event->id_category . DS . 'additional';
								Utils::createDirectory($target);
								$filename = $target . DS . $tmp_name;
								move_uploaded_file($_FILES['images']['tmp_name'][$i], $filename);
							} else {
								$failures++;
							}
						}
						if ($failures > 0) {
							$this->AddError('There was ' . $failures . ' failures during image upload!');
						} else {
							$this->view->AddNotice('Image has been uploaded successfully.');
						}
					}

					if (!is_bool($inserted_id)) {
						$this->LogsManager->registerLog('Event', 'insert', 'Event inserted with id : ' . $inserted_id, $inserted_id);
						Utils::RedirectTo($this->getActionUrl('event_edit') . "/$inserted_id");
					}
				} else {
					$this->AddError('Saving failed!');
				}
			}
		}

		$categories = $this->model->getCategories(50);
		$this->view->set('categories', $categories);

		if (isset($this->ComponentSettings['currency']) && $this->ComponentSettings['currency'] != '') {
			$this->view->set('Currencies', explode(';', $this->ComponentSettings['currency']));
		} else {
			$this->view->set('Currencies', array('EUR'));
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'events');

		//BreadCrumb
		WebPage::$breadcrumb->addDir('[$Events]', $this->getActionUrl('events_list'));
		WebPage::$breadcrumb->addDir('[$NewEvent]', $this->getActionUrl('add_event'));

		//Title tag
		HeadHTML::setTitleTag('New Event' . ' | ' . CMSSettings::$website_title);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-event-add';
		$this->view->render($view);
	}

	public function show_event($id = null)
	{
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		} elseif (is_null($id)) {
			Utils::RedirectTo(Utils::getComponentUrl('Webpage/render_error/404'));
		}

		$event = $this->model->getEvent($id);
		if (is_null($event)) {
			$this->view->AddError('Event not found');
			Utils::RedirectTo(Utils::getComponentUrl('Events/events_list'));
			return;
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'events' . ((!is_null($event->id_category)) ? "/{$event->id_category}" : ''));

		//BreadCrumb
		WebPage::$breadcrumb->addDir($event->category, Utils::getComponentUrl('Events/events_list/' . $event->id_category));
		WebPage::$breadcrumb->addDir(StringUtils::CutString($event->title, 50), Utils::getComponentUrl('Events/show_event/' . Utils::url_slug($event->title) . '-' . $id));

		//Title tag
		HeadHTML::setTitleTag(StringUtils::CutString($event->title, 50) . ' | ' . CMSSettings::$website_title);

		$category = $this->model->getCategory($event->id_category);
		$this->view->set('category', $category);

		$this->view->set('event', $event);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-event-show';
		$this->view->render($view);
	}

	public function events_list($id_category = null)
	{
		HeadHTML::setTitleTag('Events' . ' | ' . CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = 'creation_date DESC';
		$filter = '1';

		if (!is_null($id_category)) {
			$filter .= "AND id_category = {$id_category}";
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => 'Event.id', 'peso' => 100),
				array('field' => 'Event.title', 'peso' => 90),
				array('field' => 'EventCategory.category', 'peso' => 80),
			);
			$events = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$events = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('events', $events);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'events' . ((!is_null($id_category)) ? "/$id_category" : ''));

		//BreadCrumb
		if (!is_null($id_category)) {
			$category = $this->model->getCategory($id_category);
			$this->view->set('category', $category);

			WebPage::$breadcrumb->addDir($category->category, Utils::getComponentUrl('Events/events_list/' . $id_category));
		//HeadHTML::setTitleTag($category->category . ' | ' . CMSSettings::$website_title);
		} else {
			WebPage::$breadcrumb->addDir('[$Events]', Utils::getComponentUrl('Events/events_list'));
			//HeadHTML::setTitleTag('[$Events]' . ' | ' . CMSSettings::$website_title);
		}

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'events-list';
		$this->view->render($view);
	}

	public function categories_list()
	{
		HeadHTML::setTitleTag('Event Categories' . ' | ' . CMSSettings::$website_title);
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$filter = '';

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		$categories = $this->model->getCategories($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'events/categories');

		//BreadCrumb
		WebPage::$breadcrumb->addDir('[$Categories]', Utils::getComponentUrl('Events/categories_list'));
		HeadHTML::setTitleTag('[$Categories]' . ' | ' . CMSSettings::$website_title);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-categories-list';
		$this->view->render($view);
	}

	//Mails
	private function NewEventNotification($event)
	{
		$message = new BaseView();
		$message->setViewPath(VIEWS_PATH . 'Events' . DS);
		$message->setLangPath(LANGS_PATH . 'Events' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal('http://' . $_SERVER['HTTP_HOST'] . substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')) . MEDIA_ROOT . CMSSettings::$logo);
		$message->placeholder('MEDIA_PATH')->setVal('http://' . $_SERVER['HTTP_HOST'] . substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')) . MEDIA_ROOT);

		$subject = $message->getTerm('NewEventInserted') . ' - ' . CMSSettings::$website_title;
		$message->renderTemplate(false);
		$message->set('event', $event);
		$txt = $message->render('mails/email_new_event', true);
		Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, 'noreply@' . CMSSettings::$webdomain, $subject, $txt);
	}

	public function ajx_calendar()
	{
		$mod = Loader::loadModule('EventsCalendar');
		$mod->renderCalendar();
	}

	//Event List AJAX created by Doralb Kurti 13/10/2016

	public function events_list_by_start_date()
	{
		$totalElements = $this->model->getLastCounter();
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$mod = Loader::loadModule('Events/LastEvents');
		$mod->renderEventsByDate();
	}
}
