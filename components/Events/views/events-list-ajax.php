<div class="page-event">

    <?php
	if (count($events)) {
		foreach ($events as $event) {
			?>
            <?php
			$date = new DateTime($event->start_date);
			$end_date = new DateTime($event->end_date);
			$categoryPath = ((!is_null($event->id_category)) ? "/{$event->id_category}" : ''); ?>
            <div class="corniz">  
                <div class="row">

                    <div class="col-xs-12 col-sm-4 col-lg-4 col-md-4 picture-event">			
                        <a href="<?= Utils::getComponentUrl('Events/show_event/' . Utils::url_slug($event->title) . '-' . $event->id) ?>"  class="event-titull">	
                            <?php if ($event->image != '') {
				?>
                                <img  src="<?= Utils::genThumbnailUrl("events{$categoryPath}/{$event->image}", $w, $h, array('zc' => 1)) ?>" class="img-responsive" >	<?php
			} ?>
                        </a>
                        
                    </div>


                    <div class="col-xs-12 col-sm-8 col-lg-8 col-md-8"> 
                        
                        <div class="content-event"> 

                            <h1 class="event-title">
                                <a href="<?= Utils::getComponentUrl('Events/show_event/' . Utils::url_slug($event->title) . '-' . $event->id) ?>"  class="event-titull">
                                    <?= $event->title ?>
                                </a> 
                            </h1>
                            <hr class="break-line">
                            <div class="social">

                                <!--                            <div class="business-social-share">
                                                                    <div class="getsocial gs-inline-group"></div>
                                                                </div>-->

                                <span class="paragrafi"><i class="fa fa-map-marker"></i> &nbsp;<?= $event->location ?></span>
                                <div class="paragrafi"><i class="fa fa-clock-o"></i> &nbsp;[$<?= strtolower($date->format('F')); ?>]&nbsp;<?= $date->format('H:i') ?> - <?= $end_date->format('H:i') ?></div>


                            </div>
                            <hr class="break-line">
                            <p class="paragrafi">
                                <?= StringUtils::CutString(strip_tags($event->description), $taglia_content) ?>
                            </p>
                           
                        </div>
                        <p class="calendar"><?= $date->format('d'); ?><em>[$<?= strtolower($date->format('F')); ?>]</em></p>
                    </div>

                </div>
            </div>
        <?php
		} ?> 
        <?php if ($totalElements > $elements_per_page) {
			?>
            <div class="col-md-12 pagination">
                <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
            </div>
        <?php
		} ?>
    <?php
	} else {
		?>
        <p class="text-center">[$text_description]</p>
    <?php
	} ?>
</div>
