<?php $categoryPath = ((!is_null($event->id_category)) ? "/{$event->id_category}" : ''); ?>


<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php HeadHTML::addMetaProperty('og:title', $event->title); ?>
<?php HeadHTML::addMetaProperty('og:type', 'event'); ?>
<?php HeadHTML::addMetaProperty('og:start_time', $event->start_date); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('events/' . $categoryPath . '/' . str_replace(' ', '%20', $event->image), 960, 600, array('zc' => 1), true)); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($event->description), 255)); ?>

<?php setlocale(LC_ALL, 'it_IT'); ?>
 <?$date = new DateTime($event->start_date);
			$end_date = new DateTime($event->end_date);
			$categoryPath = ((!is_null($event->id_category)) ? "/{$event->id_category}" : ''); ?>
<script type="text/javascript">
    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = '//api.at.getsocial.io/widget/v1/gs_async.js?id=2c7a12';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>


<!-- Title -->
<div class="row header-event-detail">
    <div class="col-md-12">
        <div class="table-responsive"> 
            <table class="table">  
                <tbody>
                    <!-- Event Header -->
                    <tr> 
                        <th scope="row">
                            <div class="date-number"><?= $date->format('d') ?></div>
                            <div class="date-text"><?=strftime('%B', $date->getTimestamp())?></div>
                        </th>     
                        <td colspan=""> <div class="event-content">
                                <h1 class="title-content"><?= $event['title'] ?></h1>
                                <h5 class="category-content"><i class="fa fa-hashtag" aria-hidden="true"></i>&nbsp;<?= $event->category ?> </h5>
                                <h6 class="hour-content"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;[$From] <?= strftime('%d %B %Y (%H:%M)', $date->getTimestamp()) ?> [$to] <?= strftime('%d %B %Y (%H:%M)', $end_date->getTimestamp()); ?></h6>

                            </div></td> 
                    </tr> 
                </tbody> 
            </table> 
        </div>
    </div>
</div>
<!-- Main Content -->
<div class="row single-event-detail">

    <!-- Main Image -->
    <div class="col-md-12 col-xs-12 col-sm-12">
        <a data-toggle="lightbox" data-gallery="multiimages" data-title="" href="<?= Utils::genThumbnailUrl('events/' . $event->image, 0, 0) ?>">			
            <?php if ($event->image != '') {
				?>
                <img  src="<?= Utils::genThumbnailUrl('events/' . $event->image, 1600, 600, array('zc' => 1)) ?>" class="" style="width: 100%"> 
            <?php
			} ?> 
        </a>	
    </div>
    <!-- Event Details -->
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="table-responsive"> 
            <table class="table">  
                <tbody>
                    <!-- Event Decription -->
                    <tr> 
                        <th scope="row"><i class="fa fa-navicon fa-style"></i></th>     
                        <td colspan="2"> <div class="event-content"><h1 class="page-header">[$event_detail]</h1><?= $event->description?> </div></td> 
                    </tr> 
                    <!-- Event Start Date -->
                    <tr> 
                        <th scope="row"><i class="fa fa-calendar-plus-o fa-style"></i></th>     
                        <td><h2 class="titlecateg">[$start_date]</h2></td> 
                        <td> <span class="date1"> <?= $date->format('d, F Y - H:i') ?> </span></td> 
                    </tr> 
                    <!-- Event End Date  -->
                    <tr> 
                        <th scope="row"><i class="fa fa-calendar-minus-o fa-style"></i></th>     
                        <td><h2 class="titlecateg">[end_date]</h2></td> 
                        <td> 
                            <?php if (!is_null($end_date)) {
				?>
                                <span class="date1">  <?= $end_date->format('d, F Y - H:i') ?> </span> 
                            <?php
			} ?> </span>
                        </td> 
                    </tr>              
                    <!-- Event Entry -->
                    <tr> 
                        <th scope="row"><i class="fa fa-money fa-style"></i></th>     
                        <td><h2 class="titlecateg">[$entry]</h2></td> 
                        <td><span class="date1">   <?= ($event->pay == 0) ? '[$free]' : '[$cost]' ?> &nbsp; <?php if ($event->pay && $event->cost != 0) {
				?><?= $event->cost ?> <?= $event->currency ?><?php
			} ?></span></td> 
                    </tr> 
                    <!-- Event Organizer -->
                    <?php if ($event->organizer != '') {
				?>
                        <tr> 
                            <th scope="row"><i class="fa fa-user fa-style"></i></th>     
                            <td><h2 class="titlecateg">[$organizer]</h2></td> 
                            <td><span class="date1"> <?= $event->organizer ?> </span></td> 
                        </tr> 
                    <?php
			} ?>
                    <!-- Event Phone -->
                    <?php if ($event['tel'] != '') {
				?>
                        <tr> 
                            <th scope="row"><i class="fa fa-phone fa-style"></i></th>     
                            <td><h2 class="titlecateg">[$telephone]</h2></td> 
                            <td><span class="date1">  <?= $event->phone ?> </span></td> 
                        </tr> 
                    <?php
			} ?>
                    <!-- Event Location -->
                    <tr> 
                        <th scope="row"><i class="fa fa-home fa-style"></i></th>     
                        <td><h2 class="titlecateg">[$location] </h2></td> 
                        <td><span class="date1"> <?= $event->location ?> </span></td> 
                    </tr> 
                    <!-- Event Email -->
                    <?php if ($event['email'] != '' && false) {
				?>
                        <tr> 
                            <th scope="row"><i class="fa fa-envelope fa-style"></i></th>     
                            <td><h2 class="titlecateg"> [$email] </h2></td> 
                            <td><span class="date1">  <?= $event->email ?> </span></td> 
                        </tr> 
                    <?php
			} ?>
                    <!-- Event Website -->
                    <?php if ($event->email != '' && false) {
				?>
                        <tr> 
                            <th scope="row"><i class="fa fa-globe fa-style"></i></th>     
                            <td><h2 class="titlecateg">  [$website] </h2></td> 
                            <td><span class="date1">  <a href="http://<?= $event->website ?>"><?= $event->website ?> </a></span></td> 
                        </tr> 
                    <?php
			} ?>
                </tbody> 
            </table> 
        </div>
    </div>  

    <!-- Event Social Share -->
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="getsocial gs-inline-group"></div>
    </div>


    <!-- Event File PDF -->
    <?php if (!is_null($event->file)) {
				?>
        <div class="col-md-12 col-xs-12 col-sm-12">
            <hr class="firsthr">
            <h3 class="textarea1">Per scaricare un file pdf per questo evento <a href="<?= WEBROOT . MEDIA_ROOT . 'events/files/' . $event->file ?>" download>clicca qui</a>.</h3>
        </div>
    <?php
			} ?>



    <!-- Event Map -->
    <div class="col-md-12 col-xs-12 col-sm-12 klas-margin">
        <hr class="firsthr">
        <h2 class="mapa_cls">  <b> [$map] </b> </h2>
        <div id="map" style="width: 100%; height: 350px;"></div>
    </div>

</div>


<?php
list($lat, $lng) = explode(',', $event->coordinates);
?>

<script>
    function initMap() {
        var myLatLng = {lat: <?= $lat ?>, lng: <?= $lng ?>};

        var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: ''
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRs6TUKT0XI_21c8V_ETOiDtBccf_h2_k&signed_in=true&callback=initMap"></script>

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
