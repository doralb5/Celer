<?php

require_once LIBS_PATH . 'StringUtils.php';

class Gallery_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_images_list', $this->getActionUrl('images_list'));
	}

	public function preload_videos_list($id_category = null)
	{
		if (!is_numeric($id_category) && !is_null($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}

		$parameters = WebPage::getParameters();

		//BreadCrumb
		if (!is_null($id_category)) {
			$category = $this->model->getCategory($id_category);
			$this->view->set('category', $category);
			$Page_Title = $category->category;

			$Categories_Title = isset($parameters['categories_title']) ? $parameters['categories_title'] : '';
			if ($Categories_Title != '') {
				WebPage::$breadcrumb->addDir($Categories_Title, Utils::getComponentUrl('Gallery/categories_list'));
			}
			WebPage::$breadcrumb->addDir($Page_Title, Utils::getComponentUrl('Gallery/videos_list') . '/' . Utils::url_slug($Page_Title) . "-$id_category");

			HeadHTML::setTitleTag($Page_Title . ' | ' . CMSSettings::$website_title);
		} else {
			$Page_Title = isset($parameters['page_title']) ? $parameters['page_title'] : '[$Gallery]';
			WebPage::$breadcrumb->addDir($Page_Title, Utils::getComponentUrl('Gallery/videos_list'));
			HeadHTML::setTitleTag('Gallery' . ' | ' . CMSSettings::$website_title);
		}

		$this->view->set('Page_Title', $Page_Title);
	}

	public function videos_list($id_category = null)
	{
		if (!is_numeric($id_category) && !is_null($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}

		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;

		$GalleryItem_tbl = TABLE_PREFIX . 'GalleryItem';
		$sorting = '';
		$filter = "$GalleryItem_tbl.type='video' OR $GalleryItem_tbl.type='youtube' ";

		if (!is_null($id_category)) {
			$category = $this->model->getCategory($id_category);
			$this->view->set('category', $category);
			$filter .= "AND id_category = {$id_category} ";
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				array('field' => "$GalleryItem_tbl.id", 'peso' => 100),
				array('field' => "$GalleryItem_tbl.title", 'peso' => 90),
				array('field' => "$GalleryItem_tbl.description", 'peso' => 90),
			);
			$items = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$items = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('items', $items);

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'gallery' . ((!is_null($id_category)) ? "/$id_category" : ''));

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-images-list';
		$this->view->render($view);
	}

	public function preload_show_video($id)
	{
		if (!is_numeric($id) && !is_null($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		}
		$parameters = WebPage::getParameters();

		$item = $this->model->getItem($id);

		$this->view->set('item', $item);

		//BreadCrumb
		if (!is_null($item->id_category)) {
			$category = $this->model->getCategory($item->id_category);
			$Categories_Title = isset($parameters['categories_title']) ? $parameters['categories_title'] : '';
			if ($Categories_Title != '') {
				WebPage::$breadcrumb->addDir($Categories_Title, Utils::getComponentUrl('Gallery/categories_list'));
			}
			WebPage::$breadcrumb->addDir($category->category, Utils::getComponentUrl('Gallery/videos_list') . '/' . Utils::url_slug($category->category) . "-$category->id");
			WebPage::$breadcrumb->addDir(StringUtils::CutString($item->title, 60), Utils::getComponentUrl('Gallery/show_video') . '/' . Utils::url_slug($item->title) . "-$item->id");
		} else {
			$Categories_Title = isset($parameters['categories_title']) ? $parameters['categories_title'] : '';
			if ($Categories_Title != '') {
				WebPage::$breadcrumb->addDir($Categories_Title, Utils::getComponentUrl('Gallery/categories_list'));
			}
			WebPage::$breadcrumb->addDir('[$Gallery]', Utils::getComponentUrl('Gallery/videos_list'));
			WebPage::$breadcrumb->addDir(StringUtils::CutString($item->title, 60), Utils::getComponentUrl('Gallery/show_video') . '/' . Utils::url_slug($item->title) . "-$item->id");
		}

		HeadHTML::setTitleTag($item->title . ' | ' . CMSSettings::$website_title);

		$result['item'] = $item;
		return $result;
	}

	public function show_video($id)
	{
		if (!is_numeric($id) && !is_null($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		}

		$item = $this->preload_result['item'];
		$this->model->incrementCounter($id);

		////$GalleryItem_tbl = TABLE_PREFIX . "GalleryItem";
		//$sorting = "";
		//$filter = "$GalleryItem_tbl.type='video' ";
		//Webpage Parameters and Component settings

		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'single-video';
		$this->view->render($view);
	}

	public function images_list($id_category = null)
	{
		if (!is_numeric($id_category) && !is_null($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}

		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;

		$GalleryItem_tbl = TABLE_PREFIX . 'GalleryItem';
		$sorting = '';
		$filter = "$GalleryItem_tbl.enabled='1' AND ($GalleryItem_tbl.type='image' OR $GalleryItem_tbl.type='youtube') ";

		if (!is_null($id_category)) {
			$category = $this->model->getCategory($id_category);
			$this->view->set('category', $category);
			$filter .= "AND id_category = {$id_category}";
		}

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$searchFields = array(
				//array('field' => "$GalleryItem_tbl.id", 'peso' => 100),
				array('field' => "$GalleryItem_tbl.title", 'peso' => 90),
				array('field' => "$GalleryItem_tbl.description", 'peso' => 90),
			);
			$items = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$items = $this->model->getList($elements_per_page, $offset, $filter);
		}

		$totalElements = $this->model->getLastCounter();
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('items', $items);

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'gallery' . ((!is_null($id_category)) ? "/$id_category" : ''));

		//BreadCrumb
		WebPage::$breadcrumb->addDir($this->view->getTerm('Gallery'), Utils::getComponentUrl('Gallery/images_list/' . $id_category));

		//Title tag
		if (!is_null($id_category)) {
			$category = $this->model->getCategory($id_category);
			$this->view->set('category', $category);
			$Page_Title = $category->category;
		//HeadHTML::setTitleTag($Page_Title . ' | ' . CMSSettings::$website_title);
		} else {
			$Page_Title = isset($parameters['page_title']) ? $parameters['page_title'] : '[$Gallery]';
			HeadHTML::setTitleTag('Gallery' . ' | ' . CMSSettings::$website_title);
		}

		$this->view->set('Page_Title', $Page_Title);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-images-list';
		$this->view->render($view);
	}

	public function preload_categories_list()
	{
		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		$title = isset($parameters['title']) ? $parameters['title'] : '';
		//BreadCrumb
		if ($title != '') {
			WebPage::$breadcrumb->addDir($title, Utils::getComponentUrl('Gallery/categories_list'));
		}
	}

	public function categories_list()
	{
		$parameters = WebPage::getParameters();

		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$sorting = '';
		$GalleryItem_Table = TABLE_PREFIX . GalleryItem_Entity::TABLE_NAME;
		$GalleryCategory_Table = TABLE_PREFIX . GalleryCategory_Entity::TABLE_NAME;
		$filter = "(SELECT COUNT(*) FROM $GalleryItem_Table WHERE $GalleryItem_Table.id_category = $GalleryCategory_Table.id AND ($GalleryItem_Table.type='youtube') > 0 OR ($GalleryItem_Table.type='image') > 0)";

		//$albums_to_show = isset($parameters['albums_to_show']) ? json_decode($parameters['albums_to_show']) : array();
		$albums_to_show = isset($parameters['albums_to_show']) ? $parameters['albums_to_show'] : array();
		$show_all = false;
		$ids = '';
		if (count($albums_to_show)) {
			foreach ($albums_to_show as $id_album) {
				if ($id_album === '') {
					$show_all = true;
					break;
				}
				$ids .= "$id_album,";
			}
			$ids = rtrim($ids, ',');
			if (!$show_all) {
				$filter .= " AND id IN ($ids)";
			}
		}

		$categories = $this->model->getCategories($elements_per_page, $offset, $filter);
		$totalElements = $this->model->getLastCounter();

		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('categories', $categories);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'gallery/categories');

		//Title tag
		// HeadHTML::setTitleTag('Albums' . ' | ' . CMSSettings::$website_title);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-albums';
		$this->view->render($view);
	}
}
