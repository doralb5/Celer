<link rel="stylesheet" href="http://labs.juan.me/scss/mixins.scss">

<script src="http://labs.juan.me/gallery/json/media.js" async=""></script>


<style>
    @font-face {
  font-family: 'din-light';
  src: url("http://labs.juan.me/gallery/css/fonts/din-light-webfont.eot");
  src: url("http://labs.juan.me/gallery/css/fonts/din-light-webfont.eot?#iefix") format("embedded-opentype"), url("http://labs.juan.me/gallery/css/fonts/din-light-webfont.woff") format("woff"), url("http://labs.juan.me/gallery/css/fonts/din-light-webfont.ttf") format("truetype"), url("http://labs.juan.me/gallery/css/fonts/din-light-webfont.svg#din-bold-webfont") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'din-medium';
  src: url("http://labs.juan.me/gallery/css/fonts/din-medium-webfont.eot");
  src: url("http://labs.juan.me/gallery/css/fonts/din-medium-webfont.eot?#iefix") format("embedded-opentype"), url("http://labs.juan.me/gallery/css/fonts/din-medium-webfont.woff") format("woff"), url("http://labs.juan.me/gallery/css/fonts/din-medium-webfont.ttf") format("truetype"), url("http://labs.juan.me/gallery/css/fonts/din-medium-webfont.svg#din-bold-webfont") format("svg");
  font-weight: normal;
  font-style: normal;
}
#preloader {
  position: absolute;
  width: 24px;
  height: 24px;
  left: 50%;
  top: 50%;
  margin: -12px 0 0 -12px;
  background: url(http://labs.juan.me/gallery/images/loading-24.gif) no-repeat center center;
  -moz-transition: 600ms, opacity, ease-out;
  -o-transition: 600ms, opacity, ease-out;
  -webkit-transition: 600ms, opacity, ease-out;
  transition: 600ms, opacity, ease-out;
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
#preloader.o {
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
}

#veil {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 0;
  background: rgba(0, 0, 0, 0.84);
  z-index: 7;
  -moz-transition-property: opacity, height;
  -o-transition-property: opacity, height;
  -webkit-transition-property: opacity, height;
  transition-property: opacity, height;
  -moz-transition-duration: 900ms, 0s;
  -o-transition-duration: 900ms, 0s;
  -webkit-transition-duration: 900ms, 0s;
  transition-duration: 900ms, 0s;
  -moz-transition-timing-function: ease-out;
  -o-transition-timing-function: ease-out;
  -webkit-transition-timing-function: ease-out;
  transition-timing-function: ease-out;
  -moz-transition-delay: 0s, 900ms;
  -o-transition-delay: 0s, 900ms;
  -webkit-transition-delay: 0s, 900ms;
  transition-delay: 0s, 900ms;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
}
#veil.o {
  -moz-transition-delay: 0s, 0s;
  -o-transition-delay: 0s, 0s;
  -webkit-transition-delay: 0s, 0s;
  transition-delay: 0s, 0s;
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
  height: 100%;
}

#grid {
  width: 100%;
  height: 100%;
  -moz-transition: 500ms, opacity, ease-out;
  -o-transition: 500ms, opacity, ease-out;
  -webkit-transition: 500ms, opacity, ease-out;
  transition: 500ms, opacity, ease-out;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  font-size: 20px;
}
#grid.loaded {
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
#grid .cell {
  position: absolute;
  width: 256px;
  height: 256px;
  overflow: hidden;
  background: url(http://labs.juan.me/gallery/images/loading-10.gif) no-repeat center center;
}
#grid .cell.ov {
  overflow: visible;
}
#grid .cell .detail {
  position: absolute;
  top: 0;
  left: 0;
  width: 200%;
  height: 200%;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
}
#grid .cell .detail.l {
  left: -100%;
}
#grid .cell .detail.t {
  top: -100%;
}
#grid .cell .detail .close {
  position: absolute;
  top: 3%;
  right: 3%;
  width: 35px;
  height: 35px;
  z-index: 9;
  background: black;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  -moz-transition: 500ms, opacity, ease-out, 1400ms;
  -o-transition: 500ms, opacity, ease-out, 1400ms;
  -webkit-transition: 500ms, opacity, ease-out, 1400ms;
  transition: 500ms, opacity, ease-out, 1400ms;
}
#grid .cell .detail .close span {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: url(http://labs.juan.me/gallery/images/icon-close.png) no-repeat center center;
  -moz-transition: 150ms, all, ease-out;
  -o-transition: 150ms, all, ease-out;
  -webkit-transition: 150ms, all, ease-out;
  transition: 150ms, all, ease-out;
}
#grid .cell .detail .close span:hover {
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg);
}
#grid .cell .detail .image-detail {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: white;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  z-index: 8;
  -moz-transition: 600ms, opacity, ease-out, 900ms;
  -o-transition: 600ms, opacity, ease-out, 900ms;
  -webkit-transition: 600ms, opacity, ease-out, 900ms;
  transition: 600ms, opacity, ease-out, 900ms;
}
#grid .cell .detail .image-detail img {
  width: 100%;
}
#grid .cell .detail.o {
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
#grid .cell .detail.open {
  z-index: 7;
  -moz-perspective: 500;
  -webkit-perspective: 500;
  perspective: 500;
  perspective-origin: 25% 0%;
  -webkit-perspective-origin: 25% 0%;
  /* Safari and Chrome */
}
#grid .cell .detail.open .close {
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=50);
  opacity: 0.5;
}
#grid .cell .detail.open .tl {
  -moz-transform: rotateX(0deg);
  -ms-transform: rotateX(0deg);
  -webkit-transform: rotateX(0deg);
  transform: rotateX(0deg);
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
#grid .cell .detail.open .bl {
  -moz-transform: rotateX(0deg);
  -ms-transform: rotateX(0deg);
  -webkit-transform: rotateX(0deg);
  transform: rotateX(0deg);
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
#grid .cell .detail.open .tr {
  -moz-transform: rotateY(0deg);
  -ms-transform: rotateY(0deg);
  -webkit-transform: rotateY(0deg);
  transform: rotateY(0deg);
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
#grid .cell .detail.open .image-detail {
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
#grid .cell .detail.close .close {
  -moz-transition: 600ms, opacity, ease-out, 0s;
  -o-transition: 600ms, opacity, ease-out, 0s;
  -webkit-transition: 600ms, opacity, ease-out, 0s;
  transition: 600ms, opacity, ease-out, 0s;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
}
#grid .cell .detail.close .image-detail {
  -moz-transition: 600ms, opacity, ease-out, 0s;
  -o-transition: 600ms, opacity, ease-out, 0s;
  -webkit-transition: 600ms, opacity, ease-out, 0s;
  transition: 600ms, opacity, ease-out, 0s;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
}
#grid .cell .detail.close .tr {
  -moz-transition: 300ms, all, linear, 600ms;
  -o-transition: 300ms, all, linear, 600ms;
  -webkit-transition: 300ms, all, linear, 600ms;
  transition: 300ms, all, linear, 600ms;
  -moz-transform: rotateY(90deg);
  -ms-transform: rotateY(90deg);
  -webkit-transform: rotateY(90deg);
  transform: rotateY(90deg);
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
}
#grid .cell .detail.close .bl {
  -moz-transition: 300ms, all, linear, 900ms;
  -o-transition: 300ms, all, linear, 900ms;
  -webkit-transition: 300ms, all, linear, 900ms;
  transition: 300ms, all, linear, 900ms;
  -moz-transform: rotateX(-90deg);
  -ms-transform: rotateX(-90deg);
  -webkit-transform: rotateX(-90deg);
  transform: rotateX(-90deg);
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
}
#grid .cell .detail.close .tl {
  -moz-transition: 300ms, all, linear, 1200ms;
  -o-transition: 300ms, all, linear, 1200ms;
  -webkit-transition: 300ms, all, linear, 1200ms;
  transition: 300ms, all, linear, 1200ms;
  -moz-transform: rotateX(-90deg);
  -ms-transform: rotateX(-90deg);
  -webkit-transform: rotateX(-90deg);
  transform: rotateX(-90deg);
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
}
#grid .cell .detail .tl {
  position: absolute;
  top: 0;
  left: 0;
  width: 50%;
  height: 50%;
  background: white;
  -moz-transition: 300ms, all, linear;
  -o-transition: 300ms, all, linear;
  -webkit-transition: 300ms, all, linear;
  transition: 300ms, all, linear;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  -moz-transform-origin: 0% 0%;
  -ms-transform-origin: 0% 0%;
  -webkit-transform-origin: 0% 0%;
  transform-origin: 0% 0%;
  -moz-transform: rotateX(-90deg);
  -ms-transform: rotateX(-90deg);
  -webkit-transform: rotateX(-90deg);
  transform: rotateX(-90deg);
  z-index: 6;
}
#grid .cell .detail .bl {
  position: absolute;
  top: 50%;
  left: 0;
  width: 50%;
  height: 50%;
  background: white;
  -moz-transition: 300ms, all, linear, 300ms;
  -o-transition: 300ms, all, linear, 300ms;
  -webkit-transition: 300ms, all, linear, 300ms;
  transition: 300ms, all, linear, 300ms;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  -moz-transform-origin: 0% 0%;
  -ms-transform-origin: 0% 0%;
  -webkit-transform-origin: 0% 0%;
  transform-origin: 0% 0%;
  -moz-transform: rotateX(-90deg);
  -ms-transform: rotateX(-90deg);
  -webkit-transform: rotateX(-90deg);
  transform: rotateX(-90deg);
  z-index: 5;
}
#grid .cell .detail .tr {
  position: absolute;
  top: 0;
  left: 50%;
  width: 50%;
  height: 100%;
  background: white;
  z-index: 4;
  -moz-transition: 300ms, all, linear, 600ms;
  -o-transition: 300ms, all, linear, 600ms;
  -webkit-transition: 300ms, all, linear, 600ms;
  transition: 300ms, all, linear, 600ms;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  -moz-transform-origin: 0% 0%;
  -ms-transform-origin: 0% 0%;
  -webkit-transform-origin: 0% 0%;
  transform-origin: 0% 0%;
  -moz-transform: rotateY(90deg);
  -ms-transform: rotateY(90deg);
  -webkit-transform: rotateY(90deg);
  transform: rotateY(90deg);
}
#grid .cell.no-bg {
  background: none;
}
#grid .cell.loaded .image img {
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
#grid .cell.loaded .info {
  display: table;
}
#grid .cell .info {
  display: none;
  position: absolute;
  height: inherit;
  width: inherit;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 2;
  background: rgba(255, 255, 255, 0.8);
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  -moz-transition: 600ms, opacity, ease-out;
  -o-transition: 600ms, opacity, ease-out;
  -webkit-transition: 600ms, opacity, ease-out;
  transition: 600ms, opacity, ease-out;
}
#grid .cell .info:hover {
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
#grid .cell .info .w {
  display: table-cell;
  vertical-align: middle;
}
#grid .cell .info .w h2 {
  color: #333;
  background: none;
  text-align: center;
  font-family: "din-light";
  font-size: 100%;
  line-height: 100%;
  letter-spacing: 0;
  padding: 0 5% 0 5%;
}
#grid .cell .image {
  position: absolute;
  height: inherit;
  width: inherit;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1;
}
#grid .cell .image img {
  width: 100%;
  -moz-transition: 600ms, opacity, ease-out;
  -o-transition: 600ms, opacity, ease-out;
  -webkit-transition: 600ms, opacity, ease-out;
  transition: 600ms, opacity, ease-out;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
}


</style>

<div id='veil'></div>
<div id='preloader'></div>
<div id='grid'></div>


<script>
    function App() {
	var self = this;

	// this.feedUrl			= "json/media.json";
	this.debug 				= true;
	this.cellElement		= null;
	this.window				= $(window);
	this.grid				= $("#grid");
	this.fadeOutMs			= 500;
	this.unFoldMs			= 300;
	this.preloader			= $("#preloader");
	this.veil				= $("#veil");
	this.currentMedia		= null;

	// initialize
	this._init = function() {

		// load media feed
		self.loadFeed(function(d) {

			// add elements to the DOM
			self.addToDOM(d);

			self.cellElement = $(".cell");
			self.window.on('resize', self.resizeHandler).resize();

			// hide loader and show grid
			self.preloader.addClass('o');
			window.setTimeout(function() {
				self.grid.addClass('loaded');
			}, self.fadeOutMs);
		});

		// prepare pop state to load medias
		window.onpopstate = function(e) {
			if (e.state) self.loadMedia(e.state);
		}

		// click on veil will close the current media
		self.veil.on('click', function(e) {
			e.preventDefault();
			if (self.currentMedia) self.closeMedia(self.currentMedia);
		});

		// hotkeys
		$(document).on('keydown', function(event) {
			switch (event.keyCode) {
				// ESC
				case 27:
					// close the current media
					if (self.currentMedia != null) self.closeMedia(self.currentMedia);
				return false;
			}
		});
	}


	// load media.json
	this.loadFeed = function(callback) {
		callback(media);
		// $.ajax({
		// 	url: self.feedUrl,
		// 	type: 'GET',
		// 	dataType: 'json', 
		// 	complete: function(xhr, textStatus) {
		// 	},
		// 	success: function(data, textStatus, xhr) {
		// 		callback(data);
		// 	},
		// 	error: function(xhr, textStatus, errorThrown) {
		// 	}
		// });
	}

	// add media elements to the DOM
	this.addToDOM = function(d) {
		var htmlImage = '<a href="#" class="cell image" data-id-media="{id-media}"><div class="detail"><div class="close"><span></span></div><div class="image-detail"></div><div class="tl"></div><div class="bl"></div><div class="tr"></div></div><div class="info"><div class="w"><h2>{title}</h2></div></div><div class="image"><img src="{img-src}"></div></a>', 
			nroMedia = d.media.length;

		$.each(d.media, function(i, v) {
			self.grid.append(htmlImage.replace('{img-src}', v.url).replace('{title}', v.title).replace('{id-media}', v.id));

			// moving the detail view if media element is at the end of the grid
			if ((i + 1) % 5 == 0) $("a[data-id-media='" + v.id + "']").find('.detail:first').addClass('l');
			if (i >= (nroMedia - 5)) $("a[data-id-media='" + v.id + "']").find('.detail:first').addClass('t');

			// event to load media
			$("a[data-id-media='" + v.id + "']").on('click', function(e) {
				e.preventDefault();
				console.log(v);
				self.loadMedia(v);
			});

			// doesn't show the image until it's fully loaded, and then will fade in.
			self.grid.find("img:last").on('load', function() {
				$(this).closest('a.cell').addClass("loaded");
				window.setTimeout(function() {
					$(this).closest('a.cell').addClass('no-bg');
				}, self.fadeOutMs);
			})
		});

	}

	// load media and set push state
	this.loadMedia = function(m) {
		window.history.pushState(m, m.title + m.id, "/" + m.id + ".html");
		if (m.id != self.currentMedia) {
			self.openMedia(m);
		} else {
			if (self.currentMedia != null) self.closeMedia(self.currentMedia);
		}
	}

	// open a media element
	this.openMedia = function(m) {
		var $cellMedia = $("a[data-id-media='" + m.id + "']"), 
			$detail = $cellMedia.find('.detail'), 
			htmlImage = '<img src="{img-src}" />', 
			waitForIt = 0;

		// if a media element is open will close it first
		if (self.currentMedia != null) {
			self.closeMedia(self.currentMedia);
			waitForIt = (self.unFoldMs * 2) + (self.fadeOutMs * 2);
		}

		window.setTimeout(function() {
			// if press back button in browser it will scroll to the position of the media element
			$('body, html').animate({scrollTop:$cellMedia.offset().top - 100}, 600);

			$detail.find('.image-detail:first').append(htmlImage.replace('{img-src}', m.url));
			$cellMedia.addClass('ov');
			$detail.addClass('open o');
			self.veil.addClass('o');
			self.currentMedia = m.id;
		}, waitForIt);
	}

	// close a media element
	this.closeMedia = function(m) {
		var $cellMedia = $("a[data-id-media='" + m + "']"), 
			$detail = $cellMedia.find('.detail');

		$detail.addClass('close');
		window.setTimeout(function() {
			$detail.removeClass('open close o');
			$detail.find('.image-detail:first').html('');
			self.currentMedia = null;
		}, (self.unFoldMs * 2) + (self.fadeOutMs * 2));

		window.setTimeout(function() {
			self.veil.removeClass('o');
		}, self.fadeOutMs);
	}

	// resize handler for fluid grid
	this.resizeHandler = function() {
		var c = 0, 
			r = 0, 
			widthCell = Math.ceil(self.window.width() / 5);

		$.each(self.cellElement, function(i, v) {
			$(v).css({'top': (r * widthCell), 'left': (c * widthCell), 'width': widthCell, 'height': widthCell});
			c++;
			if (c % 5 == 0) {
				c = 0;
				r++;
			}
		});
	}

	self._init();
}

var Gallery;

$(window).load(function() {
	Gallery = new App();
});
    </script>