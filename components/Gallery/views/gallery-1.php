<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-media.js"></script>


<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/gallery-style.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . COMMONS_PATH . 'css/jquery.fancybox.css'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/gallery-script.js'); ?>
<?php HeadHTML::AddJS(WEBROOT . COMMONS_PATH . 'js/jquery.fancybox.pack.js'); ?>

<?php
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';

$w = (isset($parameters['w'])) ? $parameters['w'] : 300;
$h = (isset($parameters['h'])) ? $parameters['h'] : 0;
$show_title = (isset($parameters['show_title'])) ? $parameters['show_title'] : 0;
$show_description = (isset($parameters['show_description'])) ? $parameters['show_description'] : 0;
$show_gallery_button = (isset($parameters['show_gallery_button'])) ? $parameters['show_gallery_button'] : 0;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$albums_to_show = (isset($parameters['albums_to_show'])) ? $parameters['albums_to_show'] : 'id';
?>

<?php if ($title != '') {
	?>
    <div class="main-title-description">
        <h1 class="text-center"><?= $title ?></h1>
    </div>
<?php
} ?>

<div class="">
    <div class="row gallery-1">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <?php if ($show_title) {
		?>
                        <h2 class="page-title"><?= $Page_Title ?>
                            <span class="pull-right"><a href="<?= Utils::getComponentUrl('Gallery/categories_list') ?>">
                                    <?php if ($show_gallery_button) {
			?>
                                        <button type="button" class="btn btn-default">[$back]</button>
                                    <?php
		} ?>
                                </a></span>
                        </h2>
                    <?php
	} ?>
                </div>
                <div class="col-md-12">
                    <?php if ($show_description && isset($category)) {
		?>
                        <p class="page-description"><?= $category->description ?></p>
                    <?php
	} ?>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <section id="pinBoot">
                        <?php foreach ($items as $image) {
		?>
                            <article class="white-panel">
                                <?php if ($image->type == 'youtube') {
			?>
                                    <a class="fancybox-media"
                                       href="<?= $image->item_name ?>" >
                                        <img class="img-responsive" src="<?= $image->thumbnail ?>" alt=""/>
                                    </a>
                                <?php
		} else {
			?>
                                    <a class=""
                                       href="<?= Utils::genThumbnailUrl("gallery/$image->id_category/" . $image->item_name, 800, 530) ?>"
                                       data-lightbox="image-1">
                                        <img class="img-responsive"
                                             src="<?= Utils::genThumbnailUrl("gallery/$image->id_category/" . $image->item_name, $w) ?>"
                                             alt=""/>
                                    </a>
                                <?php
		} ?>
                            </article>
                        <?php
	} ?>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.fancybox-media').fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            helpers: {
                media: {}
            }
        });
    });
</script>

<!--<hr> <script type='text/javascript' src='//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56e7ebc18cfcffd9'></script> <div class='addthis_sharing_toolbox'></div>-->
