<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/mpg.css'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/mpg.js'); ?>

    <?php
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';

$w = (isset($parameters['w'])) ? $parameters['w'] : 1000;
$h = (isset($parameters['h'])) ? $parameters['h'] : 0;
$show_title = (isset($parameters['show_title'])) ? $parameters['show_title'] : 0;
$show_description = (isset($parameters['show_description'])) ? $parameters['show_description'] : 0;
$show_gallery_button = (isset($parameters['show_gallery_button'])) ? $parameters['show_gallery_button'] : 0;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$albums_to_show = (isset($parameters['albums_to_show'])) ? $parameters['albums_to_show'] : 'id';
?>

<?php if ($title != '') {
	?>
    <div class="main-title-description">
        <h1 class="text-center"><?= $title ?></h1>
    </div>
<?php
} ?>

<div class="m-p-g">
	<div class="m-p-g__thumbs" data-google-image-layout data-max-height="350">
             <?php foreach ($items as $image) {
		?>
		<img src="<?= Utils::genThumbnailUrl("gallery/$image->id_category/" . $image->item_name, $w) ?>" data-full="<?= Utils::genThumbnailUrl("gallery/$image->id_category/" . $image->item_name, $w) ?>" class="m-p-g__thumbs-img" />
		 <?php
	} ?>
	</div>

	<div class="m-p-g__fullscreen"></div>
</div>

<div class="row">
                <div class="col-md-12">
                    <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
                </div>
            </div>
<script>
	var elem = document.querySelector('.m-p-g');

	document.addEventListener('DOMContentLoaded', function() {
		var gallery = new MaterialPhotoGallery(elem);
	});
</script>