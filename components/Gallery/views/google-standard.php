<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/google-standard.css'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/google-standard.js'); ?>

    <?php
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';

$w = (isset($parameters['w'])) ? $parameters['w'] : 1000;
$h = (isset($parameters['h'])) ? $parameters['h'] : 0;
$show_title = (isset($parameters['show_title'])) ? $parameters['show_title'] : 0;
$show_description = (isset($parameters['show_description'])) ? $parameters['show_description'] : 0;
$show_gallery_button = (isset($parameters['show_gallery_button'])) ? $parameters['show_gallery_button'] : 0;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$albums_to_show = (isset($parameters['albums_to_show'])) ? $parameters['albums_to_show'] : 'id';
?>


<div id ="google-gallery">
  <div id="boximage" class="boximage">
    <a title="Space art-img1" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img1.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img1.jpg" />
    </a>
    <a title="Space art-img2" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img2.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img2.jpg" />
    </a>
    <a title="Space art-img3" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img3.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img3.jpg" />
    </a>
    <a title="Space art-img5" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img5.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img5.jpg" />
    </a>
    <a title="Space art-img6" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img6.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img6.jpg" />
    </a>
    <a title="Space art-img93" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img93.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img93.jpg" />
    </a>
    <a title="Space art-img177" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img177.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img177.jpg" />
    </a>
    <a title="Space art-img170" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img170.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img170.jpg" />
    </a>
    <a title="Space art-img95" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img95.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img95.jpg" />
    </a>
    <a title="Space art-img173" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img173.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img173.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-244" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-244.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-244.jpg" />
    </a>
    <a title="Space art-img26" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img26.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img26.jpg" />
    </a>
    <a title="Space art-img170" href="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/normal_Space_art-1900x1200-img170.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/2011y/04/21/1/7/thumb_Space_art-1900x1200-img170.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-258" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-258.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-258.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-206" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-206.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-206.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-258" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-258.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-258.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-259" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-259.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-259.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-260" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-260.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-260.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-261" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-261.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-261.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-262" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-262.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-262.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-214" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-214.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-214.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-215" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-215.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-215.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-216" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-216.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-216.jpg" />
    </a>
    <a title="Sci-Fi-Space-Art-ws-217" href="http://universe-beauty.com/albums/userpics/1/7/normal_Sci-Fi-Space-Art-1900x1200-ws-217.jpg">
      <img alt="" src="http://universe-beauty.com/albums/userpics/1/7/thumb_Sci-Fi-Space-Art-1900x1200-ws-217.jpg" />
    </a>
    <div class="clear"></div>
  </div>

  <script src="js/javascript.js"></script>
</div>