<?php HeadHTML::addMetaProperty('og:title', $item->title . ' | ' . CMSSettings::$website_title); ?>
<?php HeadHTML::addMetaProperty('og:type', 'image'); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('gallery/thumbs/' . $item->thumbnail, 1000, 0, array(), true)); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($item->description), 200)); ?>


<link rel="stylesheet" href="//releases.flowplayer.org/6.0.5/skin/functional.css">
<script src="//releases.flowplayer.org/6.0.5/flowplayer.min.js"></script>


<div class="main-title-description">
    <h1 class="page-header header-title"><?= $item->title ?></h1>
</div>


<?php if ($item->path_type == 'local') {
	?>
    <div data-ratio="0.6" class="flowplayer">
        <video data-title="<?= $item->title ?>" autoplay>
            <source type="video/flash" src="http://<?= CMSSettings::$webdomain . WEBROOT . MEDIA_ROOT . 'gallery' . '/' . $item->id_category . '/' . $item->item_name ?>">
        </video>
    </div>

<?php
} elseif ($item->type == 'youtube') {
		if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $item->item_name, $match)) {
			$video_id = $match[1];
		} ?>

    <iframe width="100%" height="450px"  src="https://www.youtube.com/embed/<?= $video_id ?>" frameborder="0" allowfullscreen></iframe>


<?php
	} ?>


<div class="publish-date"><?= date('j M  Y', strtotime($item->publish_date)) ?></div>
<div class="item-social-share float-right">
    <div class="getsocial gs-inline-group"></div>
</div>
<p class="description-video"> <?= $item->description ?></p>
<script type="text/javascript">
    'function' != typeof loadGsLib && (loadGsLib = function () {
        var e = document.createElement("script");
        e.type = "text/javascript", e.async = !0, e.src = '//api.at.getsocial.io/widget/v1/gs_async.js?id=080ddc';
        var t = document.getElementsByTagName("script")[0];
        t.parentNode.insertBefore(e, t)
    })();
</script>
