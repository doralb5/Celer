<?php HeadHTML::AddStylesheet($this->view_path . 'css/gallery-style.css'); ?>

<?php require_once LIBS_PATH . 'Pager.php'; ?>

<?php
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 4;
$col_size = 12 / $cols;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$show_total = (isset($parameters['show_total'])) ? $parameters['show_total'] : true;
?>

<?php if ($title != '') {
	?>
    <div class="main-title-description">
        <h1 class="page-header text-center"><?= $title ?></h1>
    </div>
<?php
} ?>
<div class="row">
    <div class="col-md-12 album-categories">
        <div class="row">
            <?php $c = 1; ?>
            <?php foreach ($categories as $categorie) {
		?>
                <div class="col-md-<?= $col_size ?> col-sm-6 cols-xs-12 text-center">
                    <?php if ($categorie->image != '') {
			if ($categorie->category == 'Video') {
				$get_function = 'videos_list';
				$text = 'video';
			} else {
				$get_function = 'images_list';
				$text = '[$foto]';
			} ?>
                        
                        <a class="hover-img"
                           href="<?= Utils::getComponentUrl('Gallery/' . $get_function) . '/' . Utils::url_slug($categorie->category) . "-$categorie->id" ?>">
                            <img class="img-responsive image-caption" alt="CATEGORY IMAGE"
                                 src="<?= Utils::genThumbnailUrl('gallery' . DS . 'categories' . DS . $categorie->image, 200, 0, array('zc' => 1)) ?>"
                                 width="100%"/>
                        </a>
                    <?php
		} ?>
                    <div class="photo-caption">
                        <a class="hover-img"
                           href="<?= Utils::getComponentUrl('Gallery/images_list') . '/' . Utils::url_slug($categorie->category) . "-$categorie->id" ?>">
                            <p><strong> <?= $categorie->category ?></strong></p>
                        </a>
                        <?php if ($show_total) {
			?>
                            <p style="color: gray"><?= $categorie->total_images ?> <?= $text ?></p>
                        <?php
		} ?>
                    </div>
                </div>
                <?=($c % $cols == 0) ? '<div class="clearfix"></div>' : '';
		$c++; ?>
            <?php
	} ?>
        </div>
    </div>
</div>
<?php if ($totalElements > $elements_per_page) {
		?>
    <div class="row">
        <div class="col-md-12 pagination">
            <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
        </div>
    </div>
<?php
	} ?>

