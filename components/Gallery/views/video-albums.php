<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/gallery-style.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/video-albums.css'); ?>

<?php
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 4;
$col_size = 12 / $cols;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$show_total = (isset($parameters['show_total'])) ? $parameters['show_total'] : true;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30;
?>

<?php if ($title != '') {
	?>
    <div class="main-title-description">
        <h1 class="page-header  header-title"><?= $title ?></h1>
    </div>
<?php
} ?>
<div id="video-albums">
    <div class="row">
        <div class="col-md-12">


            <div class="row">
                <?php
				$i = 0;
				foreach ($categories as $categorie) {
					?>
                    <?php if ($i % $cols == 0 && $i != 0) {
						?>
                        <div class="clearfix"></div>
                    <?php
					} ?>



                    <div class="col-md-<?= $col_size ?> col-sm-6 cols-xs-12 text-center">
                        <div class=''>
                            <div class='card card1'>
                                <?php if ($categorie->image != '') {
						?>
                                    <div class='image'>
                                        <a class="hover-img" href="<?= Utils::getComponentUrl('Gallery/images_list') . '/' . Utils::url_slug($categorie->category) . "-$categorie->id" ?>">
                                            <img class="img-responsive" alt="CATEGORY IMAGE" src="<?= Utils::genThumbnailUrl('gallery' . DS . 'categories' . DS . $categorie->image, 500, 0, array('zc' => 1)) ?>">
                                        </a>
                                    </div>
                                <?php
					} ?>

                                <div class='details'>
                                    <a class="hover-img" href="<?= Utils::getComponentUrl('Gallery/images_list') . '/' . Utils::url_slug($categorie->category) . "-$categorie->id" ?>">
                                        <span> <?= StringUtils::CutString(strip_tags($categorie->category), $taglia_title) ?> </span>
                                        <?php if ($show_total) {
						?>
                                            <p class="img-count"><?= $categorie->total_images ?> [$foto]</p>
                                        <?php
					} ?>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>

                    <?php
					$i++;
				}
				?>

            </div>
        </div>
    </div>
</div>
