<?php HeadHTML::AddStylesheet($this->view_path . 'css/gallery-style.css'); ?>


<?php
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';

$w = (isset($parameters['w'])) ? $parameters['w'] : 300;
$h = (isset($parameters['h'])) ? $parameters['h'] : 0;
$show_title = (isset($parameters['show_title'])) ? $parameters['show_title'] : 0;
$show_back_button = (isset($parameters['show_back_button'])) ? $parameters['show_back_button'] : 0;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 350;
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 2;
$col_size = 12 / $cols;
?>








<?php if ($show_title && $title != '') {
	?>
    <div class="main-title-description">
        <h1 class="page-header text-center"><?= $title ?></h1>
    </div>
<?php
} ?>
<div class="row">
    <div class="col-md-12 videos-list">
        <div class="row">
            
            <?php $c = 1; ?>
            <?php foreach ($items as $item) {
		?>
                <div class="col-md-<?= $col_size ?> col-sm-6 cols-xs-12 text-center">
                    
                    <?php if ($item->thumbnail != '' && $item->path_type == 'local') {
			?>
                        <!--/* --------------- Image ---------------*/-->
                            <a class="hover-img" href="<?= Utils::getComponentUrl('Gallery/show_video') . '/' . Utils::url_slug($item->title . '-' . $item->id) ?>">
                                <img class="img-responsive image-caption" alt="" src="<?= Utils::genThumbnailUrl('gallery/thumbs/' . $item->thumbnail, $w, $h, array('zc' => 1)) ?>"/>
                            </a>

                    <?php
		} elseif ($item->thumbnail == '' && $item->path_type != 'local') {
			?>
                            <a class="hover-img" href="<?= Utils::getComponentUrl('Gallery/show_video') . '/' . Utils::url_slug($item->title . '-' . $item->id) ?>">
                                <img class="img-responsive image-caption" alt="" src="<?='http://' . CMSSettings::$webdomain . '/' . MEDIA_ROOT . 'gallery/no.jpg' ?>"/>
                            </a>

                    <?php
		} else {
			?>
                        <!--/* --------------- Image ---------------*/-->
                            <a class="hover-img" href="<?= Utils::getComponentUrl('Gallery/show_video') . '/' . Utils::url_slug($item->title . '-' . $item->id) ?>">
                                <img class="img-responsive image-caption" alt="" src="<?= $item->thumbnail ?>"/>
                            </a>
                    <?php
		} ?>
                    
                    
                    <div class="photo-caption">
                        <a class="hover-img"
                           href="<?= Utils::getComponentUrl('Gallery/show_video') . '/' . Utils::url_slug($item->title . '-' . $item->id) ?>">
                            <p><strong><?= strip_tags($item->title); ?></strong></p>
                        </a>
                    </div>
                </div>
                <?=($c % $cols == 0) ? '<div class="clearfix"></div>' : '';
		$c++; ?>
            <?php
	} ?>
        </div>
    </div>
</div>
<?php if ($totalElements > $elements_per_page) {
		?>
    <div class="row">
        <div class="col-md-12 pagination">
            <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
        </div>
    </div>
<?php
	} ?>