<?php

require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Email.php';

class Dashboard_Component extends BaseComponent
{
	public function __construct($name = 'Dashboard', $package = 'Neshqiperi')
	{
		parent::__construct($name, $package);
	}

	public function UserDashboard()
	{
		if (isset($_SESSION['user_auth'])) {
			$business_model = Loader::getModel('Businesses');
			$businesses = $business_model->getList(-1, 0, ' cms_Business.id_user = ' . $_SESSION['user_auth']['id']);
			$businesses_count = $business_model->getLastCounter();

			$announces_model = Loader::getModel('Announces');
			$announces = $announces_model->getAnnounces(-1, 0, ' cms_Announces.user_id = ' . $_SESSION['user_auth']['id']);
			$announces_count = $announces_model->getLastCounter();

			$this->view->set('businesses_count', $businesses_count);
			$this->view->set('announces_count', $announces_count);
			$this->view->render('dashboard');
		} else {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}
	}
}
