<?php

class Subscribers_Component extends BaseComponent
{
	public function subscribe()
	{
		if (isset($_POST['subscribe'])) {
			($_POST['email'] == '') ? $this->view->AddError('Email is required') : '';
			(strlen($_POST['email']) > 100) ? $this->view->AddError('Email is too long') : '';
			if (count($this->view->getErrors()) == 0) {
				$subscriber = new Subscriber_Entity();
				$subscriber->email = $_POST['email'];
				$inserted_id = $this->model->saveSubscriber($subscriber);

				if (!is_array($inserted_id)) {
					$this->view->AddNotice('Your subscription has been saved successfully.');

					if (isset($_POST['id_category'])) {
						if (is_array($_POST['id_category'])) {
							foreach ($_POST['id_category'] as $id_category) {
								$subsCateg = new SubscriberSCateg_Entity();
								$subsCateg->id_subscriber = $inserted_id;
								$subsCateg->id_category = $id_category;
								$this->model->saveSubsCateg($subsCateg);
							}
						} else {
							$subsCateg = new SubscriberSCateg_Entity();
							$subsCateg->id_subscriber = $inserted_id;
							$subsCateg->id_category = $_POST['id_category'];
							$this->model->saveSubsCateg($subsCateg);
						}
					}
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
			Utils::RedirectTo(Utils::getComponentUrl('Newsletter/Subscribers/subscribe'));
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		//BreadCrumb
		WebPage::$breadcrumb->addDir('[$Subscribe]', Utils::getComponentUrl('Newsletter/Subscribers/subscribe'));

		//Title tag
		HeadHTML::setTitleTag('[$Subscribe]' . ' | ' . CMSSettings::$website_title);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-subscribe';
		$this->view->render($view);
	}

	public function unsubscribe()
	{
		if (isset($_POST['unsubscribe'])) {
			($_POST['email'] == '') ? $this->view->AddError('Email is required') : '';
			(strlen($_POST['email']) > 100) ? $this->view->AddError('Email is too long') : '';
			if (count($this->view->getErrors()) == 0) {
				$subscriber = $this->model->getList(1, 0, "email = '{$_POST['email']}'");
				if (count($subscriber)) {
					$sent = $this->mail_unsubscribe($subscriber[0]);
					if ($sent) {
						$this->view->AddNotice('We have sent you a mail to confirm your unsubscription.<br/>
                        Please check your mail.');
					} else {
						$this->view->AddError('Something went wrong.');
					}
				} else {
					$this->view->AddError("This mail doesn't exist in our subscriptions.");
				}
				Utils::RedirectTo(Utils::getComponentUrl('Newsletter/Subscribers/unsubscribe'));
			}
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		//BreadCrumb
		WebPage::$breadcrumb->addDir('[$Subscribe]', Utils::getComponentUrl('Newsletter/Subscribers/subscribe'));

		//Title tag
		HeadHTML::setTitleTag('[$Subscribe]' . ' | ' . CMSSettings::$website_title);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-unsubscribe';
		$this->view->render($view);
	}

	public function unsubscribeConfirm($secretkey)
	{
		if (!isset($_GET['email'])) {
			$this->view->AddError('Something went wrong');
			Utils::RedirectTo(Utils::getComponentUrl('Newsletter/Subscribers/unsubscribe'));
		}

		$subscriber = $this->model->getList(1, 0, "email = '{$_POST['email']}'");

		if (count($subscriber)) {
			if ($secretkey === md5($subscriber->email . $subscriber->creation_date)) {

				//Make unsubscription
				$subscriber->deleted = 1;
				$result = $this->model->saveSubscriber($subscriber);
				if (!is_array($result)) {
					$this->view->AddNotice('You have been unsubscribed successfully!');
				} else {
					$this->view->AddError('Something went wrong!');
				}
			}
		} else {
			$this->view->AddError("This email doesn't exist in our subscriptions!");
		}
		Utils::RedirectTo(Utils::getComponentUrl('Newsletter/Subscribers/unsubscribe'));
	}

	public function updateProfile()
	{
	}

	//Mails
	private function mail_unsubscribe($subscriber)
	{
		$message = new BaseView();
		$message->setViewPath(VIEWS_PATH . 'Newsletter' . DS);
		$message->setLangPath(LANGS_PATH . 'Newsletter' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal('http://' . $_SERVER['HTTP_HOST'] . substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')) . MEDIA_ROOT . CMSSettings::$logo);
		$message->placeholder('MEDIA_PATH')->setVal('http://' . $_SERVER['HTTP_HOST'] . substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')) . MEDIA_ROOT);

		$subject = 'Confirm unsubscription' . ' - ' . CMSSettings::$website_title;
		$message->renderTemplate(false);
		$message->set('subscriber', $subscriber);
		$message->set('SecretKey', md5($subscriber->email . $subscriber->creation_date));
		$txt = $message->render('mails/email_unsubscribe', true);
		$result = Email::sendMail($subscriber->email, CMSSettings::$website_title, 'noreply@' . CMSSettings::$webdomain, $subject, $txt);
		return $result;
	}

	public function ajx_subscribe($email)
	{
		$response = array();

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$response['status'] = 0;
			$response['message'] = $this->view->getTerm('email_invalid');

			header('Content-Type: application/json');
			echo json_encode($response);
			return;
		}

		$id_general_categ = isset($this->ComponentSettings['id_category']) ? $this->ComponentSettings['id_category'] : null;

		$subsc = $this->model->getList(1, 0, "email = '$email'");

		$sub_exist = false;
		$sub_updated = false;
		$sub_created = false;

		if (count($subsc)) {
			$subscriber = $subsc[0];
			if ($subscriber->deleted == 1 || $subscriber->enabled == 0) {
				$subscriber->deleted = 0;
				$subscriber->enabled = 1;
				$result = $this->model->saveSubscriber($subscriber);
				if (!is_array($result)) {
					$sub_updated = true;
					$this->sendNotify($subscriber, 'REACTIVATED');
				}
			} else {
				$sub_exist = true;
			}
		} else {
			$sub = new Subscriber_Entity();

			$sub->email = $email;
			$result = $this->model->saveSubscriber($sub);
			if (!is_array($result)) {
				$sub_created = true;
				$this->sendNotify($sub, 'NEW');

				if (!is_null($id_general_categ)) {
					$subCateg = new SubscriberSCateg_Entity();
					$subCateg->id_category = $id_general_categ;
					$subCateg->id_subscriber = $result;
					$this->model->saveSubsCateg($subCateg);
				}
			}
		}

		if ($sub_exist) {
			$response['status'] = 0;
			$response['message'] = $this->view->getTerm('email_exist');
		} elseif ($sub_updated) {
			$response['status'] = 1;
			$response['message'] = $this->view->getTerm('email_updated');
		} elseif ($sub_created) {
			$response['status'] = 1;
			$response['message'] = $this->view->getTerm('email_created');
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	private function sendNotify($subscriber, $status)
	{
		require_once LIBS_PATH . 'Email.php';

		switch ($status) {
			case 'NEW':
			case 'REACTIVATE':
				$subject = $this->view->getTerm('Newsletter_New_Subscription');
				break;
			default: return false;
		}

		$message = new BaseView();
		$message->setLangPath(COMPONENTS_PATH . 'Newsletter' . DS . 'lang' . DS);
		$message->setViewPath(COMPONENTS_PATH . 'Newsletter' . DS . 'views' . DS . 'mails' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('subscriber_email', $subscriber->email);
		$message->set('status', $status);

		$body = $message->render('newsletter-subscription', true);

		return Email::sendMail(CMSSettings::$EMAIL_ADMIN, CMSSettings::$webdomain, CMSSettings::$sender_mail, $subject, $body);
	}
}
