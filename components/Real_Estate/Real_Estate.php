<?php

require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Email.php';

class Real_Estate_Component extends BaseComponent
{
	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
	}

	public function RequestRealEstate($req_type = '')
	{
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//BreadCrumb
		new WebPage;
		// WebPage::$breadcrumb->addDir($this->view->getTerm('RealEstate'), $this->getActionUrl('RequestRealEstate'));
		HeadHTML::setTitleTag('RealEstate' . ' | ' . CMSSettings::$website_title);
		if ($req_type == 'request') {
			WebPage::$breadcrumb->addDir($this->view->getTerm('request_real_estate'), Utils::getComponentUrl('Real_Estate/RequestRealEstate') . '/' . $req_type);
			HeadHTML::setTitleTag('RealEstate' . ' | ' . CMSSettings::$website_title);
		} else {
			WebPage::$breadcrumb->addDir($this->view->getTerm('propose_real_estate'), Utils::getComponentUrl('Real_Estate/RequestRealEstate') . '/' . $req_type);
			HeadHTML::setTitleTag('RealEstate' . ' | ' . CMSSettings::$website_title);
		}

		$typologies = $this->model->getTypologies(-1);
		$this->view->set('typologies', $typologies);

		$contract = $this->model->getContracts(-1);
		$this->view->set('contract', $contract);

		$floors = $this->model->getFloors();
		$this->view->set('floors', $floors);

		$buildings = $this->model->getBuildings();
		$this->view->set('buildings', $buildings);

		$heatings = $this->model->getHeating();
		$this->view->set('heatings', $heatings);

		if (isset($_POST['save'])) {
			($_POST['name'] == '') ? $this->AddError('[$name_required]') : $_POST['name'];
			($_POST['surname'] == '') ? $this->AddError('[$surname_required]') : $_POST['surname'];
			//($_POST['cel'] == '' || $_POST['tel']== '') ? $this->AddError('[$cel_required]') : $_POST['cel'];
			($_POST['email'] == '') ? $this->AddError('[$email_required]') : $_POST['email'];
			($_POST['id_typology'] == '') ? $this->AddError('[$id_typology_required]') : $_POST['id_typology'];
			($_POST['id_contract'] == '') ? $this->AddError('[$id_contract_required]') : $_POST['id_contract'];
			($_POST['privacy'] == '') ? $this->AddError('[$privacy_required]') : $_POST['privacy'];
			if (count($this->view->getErrors()) == 0) {
				if ($req_type == 'request') {
					require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstateRequest.php';
					$request = new RealEstateRequest_Entity();
					$request->firstname = $_POST['name'];
					$request->lastname = $_POST['surname'];
					$request->tel = $_POST['phone'];
					$request->cel = $_POST['cel'];
					$request->email = $_POST['email'];
					$request->id_contract = (isset($_POST['id_contract']) && $_POST['id_contract'] != '') ? $_POST['id_contract'] : null;
					$request->id_floor = (isset($_POST['id_floor']) && $_POST['id_floor'] != '') ? $_POST['id_floor'] : null;
					$request->id_heating = (isset($_POST['id_heating']) && $_POST['id_heating'] != '') ? $_POST['id_heating'] : null;
					$request->id_building = (isset($_POST['id_building']) && $_POST['id_building'] != '') ? $_POST['id_building'] : null;
					$request->id_typology = (isset($_POST['id_typology']) && $_POST['id_typology'] != '') ? $_POST['id_typology'] : null;
					$request->surface = $_POST['surface'];
					$request->city = $_POST['city'];
					$request->bathrooms = $_POST['bathrooms'];
					(isset($_POST['energetic_class'])) ? $request->energetic_class = $_POST['energetic_class'] : $request->energetic_class = '';
					$request->price = str_replace('.', '', $_POST['price']);
					$request->location = $_POST['location'];
					$request->description = $_POST['description'];
					$request->rooms = $_POST['rooms'];
					$request->creation_date = date('Y-m-d H:i:s');
					(isset($_POST['garden'])) ? $request->garden = '1' : '0';
					(isset($_POST['terrace'])) ? $request->terrace = '1' : '0';
					(isset($_POST['furnished'])) ? $request->furnished = '1' : '0';
					(isset($_POST['balcony'])) ? $request->balcony = '1' : '0';
					$inserted_id = $this->model->saveRealEstateRequest($request);
				} else {
					$inserted_id = '1';
				}
				if (!is_array($inserted_id)) {
					$this->LogsManager->registerLog('Request', 'insert', 'Request inserted with id : ' . $inserted_id, $inserted_id);
					//I dergojme email
					$this->RequestMailRe($request, $req_type);
					Utils::RedirectTo($this->getActionUrl('RequestRealEstate/' . $req_type));
				} else {
					$this->view->AddError('Problem while inserting the request.');
				}
			}
		}
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'request';
		$this->view->placeholder('WEBSITE')->setVal(CMSSettings::$webdomain);
		$this->view->placeholder('COMPANY_NAME')->setVal(CMSSettings::$company_name);
		$this->view->placeholder('COMPANY_ADDRESS')->setVal(CMSSettings::$company_address);
		$this->view->placeholder('COMPANY_EMAIL')->setVal(CMSSettings::$company_mail);
		$this->view->render($view);
	}

	public function RealEstateList($id_typology = null)
	{
		if (!is_null($id_typology) && !is_numeric($id_typology)) {
			$id_typology = substr($id_typology, strripos($id_typology, '-') + 1);
		}
		$elements_per_page = 20;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$RealEstate_tbl = TABLE_PREFIX . 'RealEstate';
		$RealEstateTypology_tbl = TABLE_PREFIX . 'RealEstateTypology';
		$RealEstateContract_tbl = TABLE_PREFIX . 'RealEstateContract';
		$RealEstateCurrency_tbl = TABLE_PREFIX . 'RealEstateCurrency';
		$sorting = "$RealEstate_tbl.featured DESC ";
		$sorting .= ",  $RealEstate_tbl.title";
		$filter = "1 AND $RealEstate_tbl.enabled = '1' AND $RealEstate_tbl.date < NOW() ";
		if (!is_null($id_typology)) {
			$filter .= "AND ($RealEstate_tbl.id_typology = {$id_typology} ) ";
		}
		$realestates = $this->model->getList($elements_per_page, $offset, $filter, $sorting);
		$totalElements = $this->model->getLastCounter();
		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		if (!is_null($id_typology)) {
			$typology = $this->model->getTypology($id_typology);
			$this->view->set('typology', $typology);
		}
		HeadHTML::setTitleTag('RealEstate' . ' | ' . CMSSettings::$website_title);

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'events' . ((!is_null($id_typology)) ? "/$id_typology" : ''));

		//BreadCrumb
		if (!is_null($id_typology)) {
			$typology = $this->model->getTypology($id_typology);
			$this->view->set('typology', $typology);

			$this->addTypologiesToBreadCrumb($typology->id);
			HeadHTML::setTitleTag($typology->type . ' | ' . CMSSettings::$website_title);
		} else {
			new WebPage;
			WebPage::$breadcrumb->addDir($this->view->getTerm('RealEstate'), $this->getActionUrl('RealEstateList') . $id_typology);
			HeadHTML::setTitleTag('RealEstate' . ' | ' . CMSSettings::$website_title);
		}
		$this->view->set('realestates', $realestates);
		//nset($_SESSION['locations']);
		$_SESSION['locations'] = $realestates;
		$view = (isset($parameters['view'])) ? $parameters['view'] : 're_list';
		$this->view->render($view);
	}

	private function addTypologiesToBreadCrumb($id_typology)
	{
		$typology = $this->model->getTypology($id_typology);
		new WebPage;
		WebPage::$breadcrumb->addDir($typology->type, Utils::getComponentUrl('RealEstate/realestate_list/' . $id_typology));
	}

	public function RealEstateShow($id = '')
	{
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		}
		if (!$id) {
			$this->view->renderError('404');
		}
		$RealEstate_Table = TABLE_PREFIX . 'RealEstate';
		$realestate = $this->model->getRealEstate($id);

		if ($realestate == null || $realestate->id === null) {
			$this->view->renderError('404');
			return;
		}

		if (isset($_POST['save-info'])) {
			$this->notifyRealEstate($realestate);
		}
		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Title tag
		HeadHTML::setTitleTag(StringUtils::CutString($realestate->title, 50) . ' | ' . CMSSettings::$website_title);
		WebPage::$breadcrumb->removeLast();
		WebPage::$breadcrumb->addDir($realestate->typology . ' ' . $this->view->getTerm('AT') . ' ' . $realestate->contract_type, $this->getActionUrl('ListRealEstate') . '?contract=' . $realestate->id_contract . '&typology=' . $realestate->id_typology);
		WebPage::$breadcrumb->addDir(ucfirst(strtolower($realestate->title)), $this->getActionUrl('RealEstateShow') . DS . Utils::url_slug($realestate->title . "-{$realestate->id}"));

		$this->view->set('realestate', $realestate);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'realestate-detail';
		$this->view->render($view);
	}

	public function InsertRealEstate()
	{
		$contract = $this->model->getContracts(-1);
		$this->view->set('contrat', $contrat);
		$typology = $this->model->getTypologies(-1);
		$this->view->set('typology', $typology);
		$currency = $this->model->getCurrency(-1);
		$this->view->set('currency', $currency);
		if (isset($_POST['save'])) {
			($_POST['address'] == '') ? $this->AddError('[$address_required]') : '';
			($_POST['id_contract'] == '') ? $this->AddError('[$id_contract_required]') : '';
			($_POST['id_typology'] == '') ? $this->AddError('[$id_typology_required]') : '';
			($_POST['price'] == '') ? $this->AddError('[$price_required]') : '';
			($_POST['id_currency'] == '') ? $this->AddError('[$id_currency_required]') : '';
			if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
				(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('[$invalid_image_format]') : '';
			}
			if (count($this->view->getErrors()) == 0) {
				if (strlen($_FILES['image']['tmp_name'])) {
					$_POST['image'] = time() . '_' . $_FILES['image']['name'];
				} else {
					$_POST['image'] = null;
				}
				$users_md = Loader::getModel('Users');
				$Users_tbl = TABLE_PREFIX . 'User';
				$usr = $users_md->getList(1, 0, "$Users_tbl.email = '{$_POST['email']}'");
				if (count($usr) > 0) {
					$id_user = $usr[0]->id;
				} else {
					$usr = new User_Entity();
					$usr->email = $_POST['email'];
					$usr->username = $_POST['email'];
					$usr->firstname = $_POST['firstname'];
					$usr->lastname = $_POST['lastname'];

					$password = Utils::GenerateRandomPassword(8);
					$usr->password = md5($password);
					$usr->plain_password = $password;
					$id_user = $users_md->saveUser($usr);
				}
				if (!is_array($id_user)) {
					$realestate = new RealEstate_Entity();
					$realestate->address = $_POST['address'];
					$realestate->title = $_POST['title'];
					$realestate->coordinates = ltrim(rtrim($_POST['coordinates'], ')'), '(');
					$realestate->id_contract = $_POST['id_contract'];
					$realestate->id_typology = $_POST['id_typology'];
					$realestate->id_currency = $_POST['id_currency'];
					$realestate->description = $_POST['description'];
					$realestate->date = date('Y-m-d H:i:s');
					$realestate->price = $_POST['price'];
					$realestate->square_meter = $_POST['square_meter'];
					$realestate->id_user = $id_user;
					$realestate->email = $_POST['email'];
					$realestate->image = $_POST['image'];
				}
			}
		}
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'test';
		$this->view->render($view);
	}

	public function ListRealEstate()
	{
		$elements_per_page = 10;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;
		$filter = " cms_RealEstate.enabled = '1' ";

		$contract = $this->model->getContract($_GET['contract']);
		$typology = $this->model->getTypology($_GET['typology']);

		$sorting = 'featured DESC';
		if (isset($_GET['order'])) {
			$sorting .= ', ' . $_GET['order'] . ' ';
		} else {
			$sorting .= ', publish_date DESC ';
		}

		if (isset($_GET['garden'])) {
			$filter .= " AND cms_RealEstate.garden = '1' ";
		}

		if (isset($_GET['furnished'])) {
			$filter .= " AND cms_RealEstate.furnished = '1' ";
		}

		if (isset($_GET['terrace'])) {
			$filter .= " AND cms_RealEstate.terrace = '1' ";
		}

		if (isset($_GET['balcony'])) {
			$filter .= " AND cms_RealEstate.balcony = '1' ";
		}

		if (isset($_GET['locality']) && $_GET['locality'] != '') {
			$filter .= " AND cms_RealEstate.locality = '" . $_GET['locality'] . "' ";
		}

		if (isset($_GET['energetic_class']) && $_GET['energetic_class'] != '') {
			$ec = $_GET['energetic_class'];

			if ($ec == 'A') {
				$filter .= " AND  cms_RealEstate.energetic_class = 'A' ";
			} elseif ($ec == 'BCD') {
				$filter .= " AND  (cms_RealEstate.energetic_class = 'B' or cms_RealEstate.energetic_class = 'C' or cms_RealEstate.energetic_class = 'D')  ";
			} elseif ($ec == 'EFG') {
				$filter .= " AND  (cms_RealEstate.energetic_class = 'E' or cms_RealEstate.energetic_class = 'F' or cms_RealEstate.energetic_class = 'G')  ";
			} else {
				$filter .= " AND  cms_RealEstate.energetic_class = '" . $_GET['energetic_class'] . "' ";
			}
		}

		if (isset($_GET['id_heating']) && $_GET['id_heating'] != '') {
			$filter .= " AND cms_RealEstate.id_heating = '" . $_GET['id_heating'] . "' ";
		}
		if (isset($_GET['id_floor']) && $_GET['id_floor'] != '') {
			$filter .= " AND cms_RealEstate.id_floor = '" . $_GET['id_floor'] . "' ";
		}
		if (isset($_GET['id_building']) && $_GET['id_building'] != '') {
			$filter .= " AND cms_RealEstate.id_building_state = '" . $_GET['id_building'] . "' ";
		}

		if (isset($_GET['city']) && $_GET['city'] != '') {
			$filter .= " AND cms_RealEstate.location LIKE '%" . $_GET['city'] . "%' ";
		}
		if (isset($_GET['contract']) && $_GET['contract'] != '') {
			$filter .= " AND cms_RealEstate.id_contract = '" . $_GET['contract'] . "' ";
		}
		if (isset($_GET['typology']) && $_GET['typology'] != '') {
			$filter .= " AND cms_RealEstate.id_typology = '" . $_GET['typology'] . "' ";
		}
		if (isset($_GET['square_meter']) && $_GET['square_meter'] != '') {
			$filter .= " AND cms_RealEstate.square_meter >= '" . $_GET['square_meter'] . "' ";
		}
		if (isset($_GET['price']) && $_GET['price'] != '') {
			$filter .= " AND cms_RealEstate.price <= '" . $_GET['price'] . "' ";
		}
		if (isset($_GET['rooms']) && $_GET['rooms'] != '') {
			if ($_GET['rooms'] == '6+') {
				$filter .= " AND cms_RealEstate.rooms > '6' ";
			} else {
				$filter .= " AND cms_RealEstate.rooms = '" . $_GET['rooms'] . "' ";
			}
		}

		if (isset($_GET['bathrooms']) && $_GET['bathrooms'] != '') {
			if ($_GET['bathrooms'] == '3+') {
				$filter .= " AND cms_RealEstate.bathrooms > '3' ";
			} else {
				$filter .= " AND cms_RealEstate.bathrooms = '" . $_GET['bathrooms'] . "' ";
			}
		}

		$actual_link = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		WebPage::$breadcrumb->addDir($this->view->getTerm('search_results'), $actual_link);
		$realestates = $this->model->getList($elements_per_page, $offset, $filter, $sorting);
		$locations = $this->model->getList(-1, $offset, $filter, $sorting);
		$totalElements = $this->model->getLastCounter();
		$this->view->set('page', $page);
		$this->view->set('contract', $contract);
		$this->view->set('typology', $typology);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		HeadHTML::setTitleTag('RealEstate' . ' | ' . CMSSettings::$website_title);
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		$this->view->set('realestates', $realestates);
		$this->view->set('locations', $locations);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 're_list';
		$this->view->render($view);
	}

	public function ajx_getLocalities()
	{
		if (isset($_POST['city'])) {
			$city = $_POST['city'];
			$id_contract = $_POST['id'];
			$localities = $this->model->getLocalities($city, $id_contract);
			$localities_array = array();
			foreach ($localities as $row) {
				foreach ($row as $key => $value) {
					array_push($localities_array, $value);
				}
			}
			$string = " <option value = ''> " . $this->view->getTerm('All') . ' </option> ';
			if (isset($_POST['get_locality'])) {
				$l = $_POST['get_locality'];
				foreach ($localities_array as $locality) {
					if ($locality != '') {
						if ($l == $locality) {
							$string .= " <option value = '" . $locality . "' selected > " . ucfirst(strtolower($locality)) . ' </option> ';
						} else {
							$string .= " <option value = '" . $locality . "' > " . ucfirst(strtolower($locality)) . ' </option> ';
						}
					}
				}
			} else {
				foreach ($localities_array as $locality) {
					if ($locality != '') {
						$string .= " <option value = '" . $locality . "'> " . ucfirst(strtolower($locality)) . ' </option> ';
					}
				}
			}

			echo $string;
			return;
		}
		return false;
	}

	public function ajx_getTypologies()
	{
		if (isset($_POST['id_contract'])) {
			$id_contract = $_POST['id_contract'];
			$typologies = $this->model->getTypologiesByContract($id_contract);

			$typologies_array = array();
			$typologies_arr = array();
			foreach ($typologies as $row) {
				foreach ($row as $key => $value) {
					$typologies_arr[$key] = $value;
				}
				array_push($typologies_array, $typologies_arr);
			}
			$string = " <option value = ''> " . $this->view->getTerm('select_a_typology') . ' </option> ';
			if (isset($_POST['get_typology'])) {
				$t = $_POST['get_typology'];
				foreach ($typologies_array as $typology) {
					if ($t != '') {
						if ($t == $typology['id_typology']) {
							$string .= " <option value = '" . $typology['id_typology'] . "' selected > " . ucfirst(strtolower($typology['type'])) . ' </option> ';
						} else {
							$string .= " <option value = '" . $typology['id_typology'] . "'> " . ucfirst(strtolower($typology['type'])) . ' </option> ';
						}
					} else {
						$string .= " <option value = '" . $typology['id_typology'] . "'> " . ucfirst(strtolower($typology['type'])) . ' </option> ';
					}
				}
			} else {
				foreach ($typologies_array as $typology) {
					$string .= " <option value = '" . $typology['id_typology'] . "'> " . ucfirst(strtolower($typology['type'])) . ' </option> ';
				}
			}

			echo $string;
			return;
		}
		return false;
	}

	public function ajx_ListRealEstate()
	{
		//        $get = json_decode($_POST['get']);
//        $_GET =  (array) $get;
//
//        $elements_per_page = 10;
//        $page = (isset($_GET['page'])) ? $_GET['page'] : 1;
//        $offset = ($page - 1) * $elements_per_page;
//        $filter = " cms_RealEstate.enabled = '1' ";
//
//        $sorting = '';
//        if (isset($_POST['order'])) {
//            $sorting .= ' ' . $_POST['order'] . ' ';
//        } else {
//            $sorting = " featured DESC ";
//        }
//
//        if (isset($_GET['city']) && $_GET['city'] != '') {
//            $filter .= " AND cms_RealEstate.location LIKE '%" . $_GET['city'] . "%' ";
//        }
//        if (isset($_GET['contract']) && $_GET['contract'] != '') {
//            $filter .= " AND cms_RealEstate.id_contract = '" . $_GET['contract'] . "' ";
//        }
//        if (isset($_GET['typology']) && $_GET['typology'] != '') {
//            $filter .= " AND cms_RealEstate.id_typology = '" . $_GET['typology'] . "' ";
//        }
//        if (isset($_GET['square_meter']) && $_GET['square_meter'] != '') {
//            $filter .= " AND cms_RealEstate.square_meter >= '" . $_GET['square_meter'] . "' ";
//        }
//        if (isset($_GET['price']) && $_GET['price'] != '') {
//            $filter .= " AND cms_RealEstate.price <= '" . $_GET['price'] . "' ";
//        }
//        if (isset($_GET['rooms']) && $_GET['rooms'] != '') {
//            $filter .= " AND cms_RealEstate.rooms >= '" . $_GET['rooms'] . "' ";
//        }
//        $realestates = $this->model->getList($elements_per_page, $offset, $filter, $sorting);
//        $totalElements = $this->model->getLastCounter();
//        $this->view->set('page', $page);
//        $this->view->set('totalElements', $totalElements);
//        $this->view->set('elements_per_page', $elements_per_page);
//        HeadHTML::setTitleTag('RealEstate' . ' | ' . CMSSettings::$website_title);
//        $parameters = WebPage::getParameters();
//        $this->view->set('parameters', $parameters);
//        $this->view->set('settings', $this->ComponentSettings);
//        $this->view->set('realestates', $realestates);
//        $view = (isset($parameters['view'])) ? $parameters['view'] : 'ajx_re_list';
//        $this->view->render($view);
	}

	private function notifyRealEstate($realestate)
	{
		$message = new BaseView();
		$link = Utils::getComponentUrl('Real_Estate/RealEstateShow/' . $realestate->id);
		$message->setViewPath(COMPONENTS_PATH . 'Real_Estate' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Real_Estate' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_PHONE')->setVal(CMSSettings::$company_tel);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('realestate', $realestate);
		$message->set('link', $link);
		$txt = $message->render('mails/richiedi-info', true);

		$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, $_POST['name'], $_POST['email'], $this->view->getTerm('Richiesta di informazioni su') . CMSSettings::$webdomain, $txt);
		if ($sended) {
			$this->view->AddNotice('[$email_sent]');
		} else {
			$this->view->AddError('[$email_sending_failed]');
		}
	}

	private function RequestMailRe($request, $req_type)
	{
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH . 'Real_Estate' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Real_Estate' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_PHONE')->setVal(CMSSettings::$company_tel);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('request', $request);
		$typology = $this->model->getTypology($_POST['id_typology']);
		$message->set('typology', $typology);

		$contract = $this->model->getContract($_POST['id_contract']);
		$message->set('contract', $contract);

		$floor = $this->model->getFloor($_POST['id_floor']);
		$message->set('floor', $floor);

		$building = $this->model->getBuilding($_POST['id_building']);
		$message->set('building', $building);

		$heating = $this->model->getHeat($_POST['id_heating']);
		$message->set('heating', $heating);

		$txt = $message->render('mails/request-mail', true);
		if ($req_type == 'sell') {
			$txt = $message->render('mails/req-sale-mail', true);
		}

		$subject = ($req_type == 'sell') ? 'Nuova proposta immobiliare | ' . CMSSettings::$webdomain : 'Nuova richiesta immobiliare | ' . CMSSettings::$webdomain;
		$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, $_POST['name'], $_POST['email'], $subject, $txt);
		if ($sended) {
			$this->AddNotice('[$request_sent]');
		} else {
			$this->AddError('[$request_not_sent]');
		}
	}
}
