<?php

$w = (isset($parameters['w'])) ? $parameters['w'] : 600;
$h = (isset($parameters['h'])) ? $parameters['h'] : 450;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 150;

?>


<div id="all-content">


<div id="map"  style="height: 400px;"></div>

<!----- Search result alert ----->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="search-result">
                <div class="alert alert-grey">

                    <div class="row">
                        <div class="col-md-9" style="line-height: 30px">
                            <?php if (isset($_GET['query']) && $_GET['query'] != '') {
	?>
                                <i class="fa fa-search" aria-hidden="true">&nbsp&nbsp</i>[$result_for]: <span
                                    class="result"><?= $_GET['query'] ?></span>
                                <?php
} ?>
                            <?php if (isset($category)) {
		?>
                                <i class="fa fa-search" aria-hidden="true">&nbsp&nbsp</i>[$result_for]: <span
                                    class="result"><?= $category->category ?></span>
                                <?php
	} ?>
                        </div>
                        <div class="col-md-3">
                            <?php
							$order = isset($_POST['order']) ? $_POST['order'] : 'relevance desc';
							?>
                            <form method="POST">
                                <div class="input-group">
                                    <div class="input-group-addon btn btn-default btn-sm" style="padding: 11px 15px 12px 15px;">
                                        <span class="fa fa-sort"></span>
                                    </div>
                                    <select class="form-control input-sm" id="order-select"
                                            onchange="load_ajax_re_list(this.value)" name="order">
                                        <option
                                            value="price asc" <?= ($order == 'price asc') ? 'selected' : '' ?>
                                            data-icon="fa fa-sort-amount-asc">&nbsp;[$price]
                                        </option>
                                        <option
                                            value="price desc" <?= ($order == 'price desc') ? 'selected' : '' ?>
                                            data-icon="fa fa-sort-amount-desc">&nbsp;[$price]
                                        </option>
                                        <option
                                            value="square_meter asc" <?= ($order == 'square_meter asc') ? 'selected' : '' ?>
                                            data-icon="fa fa-sort-amount-asc">&nbsp;[$square_meter]
                                        </option>
                                        <option
                                            value="square_meter desc" <?= ($order == 'square_meter desc') ? 'selected' : '' ?>
                                            data-icon="fa fa-sort-amount-desc">&nbsp;[$square_meter]
                                        </option>
                                        <option
                                            value="rooms asc" <?= ($order == 'rooms asc') ? 'selected' : '' ?>
                                            data-icon="fa fa-sort-amount-asc">&nbsp;[$rooms]
                                        </option>
                                        <option
                                            value="rooms desc" <?= ($order == 'rooms desc') ? 'selected' : '' ?>
                                            data-icon="fa fa-sort-amount-desc">&nbsp;[$rooms]
                                        </option>
                                        <option
                                            value="bathrooms asc" <?= ($order == 'bathrooms asc') ? 'selected' : '' ?>
                                            data-icon="fa fa-sort-amount-asc">&nbsp;[$bathrooms]
                                        </option>
                                        <option
                                            value="bathrooms desc" <?= ($order == 'bathrooms desc') ? 'selected' : '' ?>
                                            data-icon="fa fa-sort-amount-desc">&nbsp;[$bathrooms]
                                        </option>
                                        <option
                                            value="publish_date desc" <?= ($order == 'publish_date desc') ? 'selected' : '' ?>
                                            data-icon="fa fa-sort-amount-desc">&nbsp;[$date]
                                        </option>
                                        <option
                                            value="publish_date asc" <?= ($order == 'publish_date asc') ? 'selected' : '' ?>
                                            data-icon="fa fa-sort-amount-asc">&nbsp;[$date]
                                        </option>

                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--****************************************************************************************************************-->


<!-- Cards-->
<div id="ajax_cards">
    <?php foreach ($realestates as $re) {
								?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="real-estate-card">
                        <div class="card card--small">
                            <div class="card__image" style="background-image: url(http://<?= CMSSettings::$webdomain . Utils::genThumbnailUrl('realestates/' . $re->image, $w, $h, array('zc' => 1)) ?>)">&nbsp;</div>
                            <span class="card__title"> <?= $re->title ?> </span>
                            <span class="card__subtitle"> <?= StringUtils::CutString(strip_tags($re->description), $taglia_content) ?> </span>


                            <p class="card__text">
        <!--                            <span class="was-price"></span><br />-->
                                <span class="price">&euro; <?= $re->price ?> k</span><br />
                                <span class="save"><i class="fa fa-map-marker" aria-hidden="true"></i><?= $re->address ?></span>
                            </p>

                            <div class="card__action-bar">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 col-xs-3 xs-visible">
                                        <span class="specifics card__button"  style="border-left: 0px;"><i class="fa fa-bed" aria-hidden="true"></i><?= $re->rooms ?></span>
                                    </div>

                                    <div class="col-md-3 col-sm-3 col-xs-3 xs-visible">
                                        <span class="specifics card__button"><i class="fa fa-bath" aria-hidden="true"></i><?= $re->bathrooms ?></span>
                                    </div>

                                    <div class="col-md-3 col-sm-3 col-xs-3 xs-visible">
                                        <span class="specifics card__button"><?= $re->square_meter ?>m²</span>
                                    </div>

                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <a href="<?= Utils::getComponentUrl('Real_Estate/RealEstateShow/' . Utils::url_slug($re->title . '-' . $re->id)) ?>">
                                            <button class="card__button btn btn-block btn-default">Di piu</button>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!----- Paggination ----->
            <div class="col-md-12 pagination">
                <?php
				require_once DOCROOT . LIBS_PATH . 'Pager.php';
								new Pager();
								Pager::printPager($page, $totalElements, $elements_per_page); ?>
            </div>
        </div>
    <?php
							} ?>

    <script>
        function changeUrl(response , urlPath) {
            window.history.pushState({"html": response.html, "pageTitle": response.pageTitle}, "", urlPath);
        }

        function load_ajax_re_list(order_value) {
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Real_Estate/ajx_ListRealEstate') ?>",
                data: {'get': '<?= json_encode($_GET) ?>'},
                success: function (data) {
                    changeUrl(data, window.location.href + '?order=' + order_value);
                    $('#all-content').html(data);
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }
    </script>


</div>

<script>






//card
    (function () {

        $(".controller").click(function () {
            id = $(this).attr("id");

            $(".controller-container").find(".is_current").removeClass("is_current");
            $(this).addClass("is_current");
            $(".card").attr('class', 'card card--' + id);
            $("html").attr('class', 'bg--' + id);

        });

    })();

    $('.card').on('click', function () {
        if ($(this).hasClass('card--big')) {
            $(this).addClass('card--small').removeClass('card--big');
        } else if ($(this).hasClass('card--small')) {
            $('.card--big').removeClass('card--big').addClass('card--small');
            $(this).addClass('card--big').removeClass('card--small');
        }
    });

// Ripple function
    (function () {
        "use strict";

        var colour = "#FF1744";
        var opacity = 0.1;
        var ripple_within_elements = ['input', 'button', 'a'];
        var ripple_without_diameter = 0;

        var overlays = {
            items: [],
            get: function () {
                var $element;
                for (var i = 0; i < overlays.items.length; i++) {
                    $element = overlays.items[i];
                    if ($element.transition_phase === false) {
                        $element.transition_phase = 0;
                        return $element;
                    }
                }
                $element = document.createElement("div");
                $element.style.position = "absolute";
                $element.style.opacity = opacity;
                //$element.style.outline = "10px solid red";
                $element.style.pointerEvents = "none";
                $element.style.background = "-webkit-radial-gradient(" + colour + " 64%, rgba(0,0,0,0) 65%) no-repeat";
                $element.style.background = "radial-gradient(" + colour + " 64%, rgba(0,0,0,0) 65%) no-repeat";
                $element.style.transform = "translateZ(0)";
                $element.transition_phase = 0;
                $element.rid = overlays.items.length;
                $element.next_transition = overlays.next_transition_generator($element);
                document.body.appendChild($element);
                overlays.items.push($element);
                return $element;
            },
            next_transition_generator: function ($element) {
                return function () {
                    $element.transition_phase++;
                    switch ($element.transition_phase) {
                        case 1:
                            $element.style[transition] = "all 500ms cubic-bezier(0.165, 0.840, 0.440, 1.000)";
                            $element.style.backgroundSize = $element.ripple_backgroundSize;
                            $element.style.backgroundPosition = $element.ripple_backgroundPosition;
                            setTimeout($element.next_transition, 0.2 * 1000); //now I know transitionend is better but it fires multiple times when multiple properties are animated, so this is simpler code and (imo) worth tiny delays
                            break;
                        case 2:
                            $element.style[transition] = "opacity 0.15s ease-in-out";
                            $element.style.opacity = 0;
                            setTimeout($element.next_transition, 0.15 * 1000);
                            break;
                        case 3:
                            overlays.recycle($element);
                            break;
                    }
                };
            },
            recycle: function ($element) {
                $element.style.display = "none";
                $element.style[transition] = "none";
                if ($element.timer)
                    clearTimeout($element.timer);
                $element.transition_phase = false;
            }
        };

        var transition = function () {
            var i,
                    el = document.createElement('div'),
                    transitions = {
                        'WebkitTransition': 'webkitTransition',
                        'transition': 'transition',
                        'OTransition': 'otransition',
                        'MozTransition': 'transition'
                    };
            for (i in transitions) {
                if (transitions.hasOwnProperty(i) && el.style[i] !== undefined) {
                    return transitions[i];
                }
            }
        }();

        var click = function (event) {
            var $element = overlays.get(),
                    touch,
                    x,
                    y;

            touch = event.touches ? event.touches[0] : event;

            $element.style[transition] = "none";
            $element.style.backgroundSize = "3px 3px";
            $element.style.opacity = opacity;
            if (ripple_within_elements.indexOf(touch.target.nodeName.toLowerCase()) > -1) {
                x = touch.offsetX;
                y = touch.offsetY;

                var dimensions = touch.target.getBoundingClientRect();
                if (!x || !y) {
                    x = (touch.clientX || touch.x) - dimensions.left;
                    y = (touch.clientY || touch.y) - dimensions.top;
                }
                $element.style.backgroundPosition = x + "px " + y + "px";
                $element.style.width = dimensions.width + "px";
                $element.style.height = dimensions.height + "px";
                $element.style.left = (dimensions.left) + "px";
                $element.style.top = (dimensions.top + document.body.scrollTop + document.documentElement.scrollTop) + "px";
                var computed_style = window.getComputedStyle(event.target);
                for (var key in computed_style) {
                    if (key.toString().indexOf("adius") > -1) {
                        if (computed_style[key]) {
                            $element.style[key] = computed_style[key];
                        }
                    } else if (parseInt(key, 10).toString() === key && computed_style[key].indexOf("adius") > -1) {
                        $element.style[computed_style[key]] = computed_style[computed_style[key]];
                    }
                }
                $element.style.backgroundPosition = x + "px " + y + "px";
                $element.ripple_backgroundPosition = (x - dimensions.width) + "px " + (y - dimensions.width) + "px";
                $element.ripple_backgroundSize = (dimensions.width * 2) + "px " + (dimensions.width * 2) + "px";
            } else { //click was outside of ripple element
                x = touch.clientX || touch.x || touch.pageX;
                y = touch.clientY || touch.y || touch.pageY;

                $element.style.borderRadius = "0px";
                $element.style.left = (x - ripple_without_diameter / 2) + "px";
                $element.style.top = (document.body.scrollTop + document.documentElement.scrollTop + y - ripple_without_diameter / 2) + "px";
                $element.ripple_backgroundSize = ripple_without_diameter + "px " + ripple_without_diameter + "px";
                $element.style.width = ripple_without_diameter + "px";
                $element.style.height = ripple_without_diameter + "px";
                $element.style.backgroundPosition = "center center";
                $element.ripple_backgroundPosition = "center center";
                $element.ripple_backgroundSize = ripple_without_diameter + "px " + ripple_without_diameter + "px";
            }
            $element.ripple_x = x;
            $element.ripple_y = y;
            $element.style.display = "block";
            setTimeout($element.next_transition, 20);
        };

        if ('ontouchstart' in window || 'onmsgesturechange' in window) {
            document.addEventListener("touchstart", click, false);
        } else {
            document.addEventListener("click", click, false);
        }
    }());

</script>

<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php if (count($realestates) > 1 || (count($realestates) == 1 && $realestates[0]->coordinates != '')) {
								?>

    <?php HeadHtml::linkJS('markerwithlabel.js'); ?>


    <script type="text/javascript">
        var locations = [
    <?php
	$counter = 1;
								foreach ($realestates as $loc) {
									$loc->title = str_replace("'", "\'", $loc->title);
									$loc->address = str_replace("'", "\'", $loc->address);
									$loc->price = str_replace("'", "\'", $loc->price);
									$price = '&euro;' . Utils::numberFormatToK($loc->price);

									if ($loc->image != '') {
										$fullImage = Utils::genThumbnailUrl('realestates' . DS . $loc->image, 200, 100, array('zc' => 1));
									} else {
										$fullImage = WEBROOT . $this->view_path . 'img/default.jpg';
									}

									if ($loc->coordinates != '') {
										list($lat, $lng) = explode(',', $loc->coordinates);
										$id_biz = $loc->id;
										echo "[ '{$loc->title}' , {$lat} , {$lng} , {$counter} , '{$loc->address}', '{$id_biz}' , '{$fullImage}', '{$price}' ],\n";

										$counter++;
									}
								} ?>
        ];

    <?php
	if (count($realestates) >= 1) {
		$map_center = $realestates[0]->coordinates;
	} ?>


        //  new map
        var map = new google.maps.Map(document.getElementById('map'), {
            //zoom: 14,
            center: new google.maps.LatLng(<?= $map_center ?>),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            draggable: true
        });

        //  var infowindow = new google.maps.InfoWindow();
        var infowindow = new google.maps.InfoWindow({
            maxWidth: 400
        });

        var marker, i;
        var markerImage = 'marker.png';
        var contentString = [];

        var bounds = new google.maps.LatLngBounds();

        for (i = 0; i < locations.length; i++) {


            marker = new MarkerWithLabel({
                // marker position
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                draggable: false,
                raiseOnDrag: true,
                labelContent: (locations[i][7]),
                labelAnchor: new google.maps.Point(20, 50),
                labelClass: "labels", // the CSS class for the label
                labelInBackground: false,
                icon: 'http://www.livingcasa.eu/data/livingcasa.eu/media/marker2.png'
            });

            contentString[i] = '<div id="iw-container">' +
                    '<div class="iw-content">' +
                    '<img src="' + locations[i][6] + '" alt="neTirane.al" height="100" width="200">' +
                    '<div class="iw-subTitle">' + locations[i][0] + '</div>' +
                    '<p><i class="fa fa-map-marker" style="padding-right: 5px;"></i>' + locations[i][4] + '<br>' +
                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                    '</div>';

            bounds.extend(marker.position);

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(contentString[i]);
                    infowindow.open(map, marker);
                }
            })


                    (marker, i));

        }
        map.fitBounds(bounds);
        google.maps.event.addListener(infowindow, 'domready', function () {


            //var bounds = new google.maps.LatLngBounds();
            // Begin loop
            //        bounds.extend(new google.maps.LatLng(<?= $map_center ?>));
            // End loop
            //map.fitBounds(bounds);

            // Reference to the DIV that wraps the bottom of infowindow
            var iwOuter = $('.gm-style-iw');

            /* Since this div is in a position prior to .gm-div style-iw.
             * We use jQuery and create a iwBackground variable,
             * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
             */
            var iwBackground = iwOuter.prev();

            // Removes background shadow DIV
            iwBackground.children(':nth-child(2)').css({'display': 'none'});

            // Removes white background DIV
            iwBackground.children(':nth-child(4)').css({'display': 'none'});

            // Moves the infowindow 45px to the right.
            iwOuter.parent().parent().css({left: '25px'});

            // Moves the shadow of the arrow 76px to the left margin.
            iwBackground.children(':nth-child(1)').attr('style', function (i, s) {
                return s + 'left: 90px !important;'
            });

            // Moves the arrow 76px to the left margin.
            iwBackground.children(':nth-child(3)').attr('style', function (i, s) {
                return s + 'left: 90px !important;'
            });

            // Changes the desired tail shadow color.
            iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0) 0px 1px 6px', 'z-index': '1'});

            // Reference to the div that groups the close button elements.
            var iwCloseBtn = iwOuter.next();

            // Apply the desired effect to the close button
            iwCloseBtn.css({width: '25px', height: '25px', opacity: '1', right: '45px', top: '3px', border: '6px solid #ffffff', 'border-radius': '13px', 'box-shadow': '0 0 5px #9c9c9c'});

            // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
            //    if($('.iw-content').height() < 140){
            //      $('.iw-bottom-gradient').css({display: 'none'});
            //    }

            // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
            iwCloseBtn.mouseout(function () {
                $(this).css({opacity: '1'});
            });
        });


        //  map style
        var styles = [{"stylers": [{"saturation": -100}]}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#0099dd"}]}, {"elementType": "labels", "stylers": [{"visibility": "off"}]}, {"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"color": "#aadd55"}]}, {"featureType": "road.highway", "elementType": "labels", "stylers": [{"visibility": "on"}]}, {"featureType": "road.arterial", "elementType": "labels.text", "stylers": [{"visibility": "on"}]}, {"featureType": "road.local", "elementType": "labels.text", "stylers": [{"visibility": "on"}]}, {}];
        map.set('styles', styles);


        google.maps.event.addDomListener(window, 'load');


    </script>

<?php
							} ?>

<script>
    $('#order-select').selectpicker({
        style: 'btn-default btn-sm'
    });
</script>
</div>