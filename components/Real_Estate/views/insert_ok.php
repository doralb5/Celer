<div id="insert_ok" class="alert-grey">

    <div class="row">
        <div class="col-md-12 text-center">

            <?php if ($realestate->image != '' && file_exists(DOCROOT . MEDIA_ROOT . 'realestates' . DS . $realestate->id_contract . DS . $realestate->image)) {
	?>
                <img  class="img-responsive" style="display: inline-block;"
                     src="<?= Utils::genThumbnailUrl("realestates/$realestate->id_contract/" . $realestate->image, 200, 200, array()) ?>">
            <?php
} else {
		?>
                <img class="little-logo"
                     src="<?= WEBROOT . $this->view_path ?>img/default.jpg" width="200px" alt=""/>
            <?php
	} ?>
            <h1 style="font-weight: bold;">URIME!</h1>

            <h3>
                Ne kete moment ju krijuat publicitetin tuaj ne kete portal.
            </h3>
            <h5>
                <br/> Brenda 24 oreve ne vazhdim objekti juaj do te jete i reklamuar online.
                <br/> Ju do te merrni nje email ne momentin qe objekti juaj do te aktivizohet.
            </h5>
            <br/>
            <br/>
            <a href="/" class="btn btn-md btn-default">Kthehu ne faqen kryesore</a>
            <p><br/></p>


        </div>
    </div>
</div>