

<?php $lat = (isset($settings['map_lat'])) ? $settings['map_lat'] : '41.3280456' ?>
<?php $lng = (isset($settings['map_lng'])) ? $settings['map_lng'] : '19.8119486' ?>

<?php HeadHtml::linkJS('https://maps.googleapis.com/maps/api/js'); ?>
<script type="text/javascript" src="//www.google.fr/jsapi"></script>


<?php
$address = (isset($_POST['address'])) ? $_POST['address'] : '';
$id_contract = (isset($_POST['id_contract'])) ? $_POST['id_contract'] : '';
$id_typology = (isset($_POST['id_typology'])) ? $_POST['id_typology'] : '';
$id_currency = (isset($_POST['id_currency'])) ? $_POST['id_currency'] : '';
$price = (isset($_POST['price'])) ? $_POST['price'] : '';
$date = (isset($_POST['date'])) ? $_POST['date'] : '';
$firstname = (isset($_POST['firstname'])) ? $_POST['firstname'] : '';
$lastname = (isset($_POST['lastname'])) ? $_POST['lastname'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
$coordinates = (isset($_POST['coordinates'])) ? $_POST['coordinates'] : '';

?>

<style>
    .header_bg{
        padding: 1em 0 12em 0px;
        /* margin: -16px -16px 0px -16px; */
        background-color: #81c3cf;
        -webkit-background-size: cover !important;
        -moz-background-size: cover !important;
        -ms-background-size: cover !important;
        -o-background-size: cover !important;
        background-size: contain;
        background-position: center bottom;
        background-repeat: no-repeat;
        text-align: center;
        width: 100%;
    }
    @media (max-width:992px) {
        .resizable_bg {
            margin-right: -15px;
            margin-left: -15px;;
        }
    }
    @media (min-width:767px) {
        .resizable_bg {
            margin-right: 0px;
            margin-left: 0px;;
        }
    }
</style>

<div class="resizable_bg">
    <div class="header_bg">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <h2 class="" style="color: white;text-shadow: 0px 2px 2px rgba(33, 31, 31, 0.65);">[$data_insert]</h2>

                    <h4 class="" style="color: white;text-shadow: 0px 2px 2px rgba(33, 31, 31, 0.3);">[$your_announcement] <span style="color: #c90000;"><?= CMSSettings::$webdomain ?></span></h4>
                </div>
            </div>
        </div>
    </div>
</div>
<form id="insert-annuncement" method="post" action="" enctype="multipart/form-data" class="alert-grey">

    <div class="row">

        <div class="col-md-12 border-left">

            <?php Messages::showErrors(); ?>
            <?php Messages::showInfoMsg(); ?>


            <?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
	?>
                <div class="row">
                    <div class="col-xs-12">
                        <?php $this->ComponentErrors->renderHTML(); ?>
                        <?php $this->ComponentMessages->renderHTML(); ?>
                    </div>
                </div>
            <?php
} ?>

            
            
             <div class="row">
                <div class="col-md-6">
                    <h5 class="titlecatege">[$name_p]* </h5>

                    <div class="form-group formgroup">
                        <input type="text" required="" class="form-control" id="" name="firstname"
                               value="<?= $firstname ?>" placeholder="Emri juaj">
                    </div>
                </div>
                <div class="col-md-6">
                    <h5 class="titlecatege">[$l_name]* </h5>

                    <div class="form-group formgroup">
                        <input type="text" required="" class="form-control" id="" name="lastname"
                               value="<?= $lastname ?>" placeholder="Mbiemri juaj">
                    </div>
                </div>
            </div>
            
            
            <div class="row">
                
                <div class="col-md-6">
                    <h5 class="titlecatege">[$image]</h5>
                    <span class="file-input"><div class="file-preview ">
                            <div class="file-preview-thumbnails">
                                <div class="file-preview-frame">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="file-preview-status text-center text-success"></div>
                            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                        </div>
                    </span>

                    <div class="upload-btn">
                        <input type="file" name="image">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h5 class="titlecatege">[$Contract]* </h5>
                    <div class="form-group formgroup">
                        <select class="form-control" id="id_category" name="id_contract">
                            <option value=""></option>
                            <?php foreach ($contract as $cont) {
		?>
                                <option
                                    value="<?= $cont->id ?>" <?= ($cont->id == $id_contract) ? 'selected' : '' ?>><?= $cont->type ?></option>
                                <?php
	} ?>
                        </select>
                    </div>
                </div>
                 <div class="col-md-6">
                    <h5 class="titlecatege">[$Currency]* </h5>
                    <div class="form-group formgroup">
                        <select class="form-control" id="id_category" name="id_currency">
                            <option value=""></option>
                            <?php foreach ($currency as $cu) {
		?>
                                <option
                                    value="<?= $cu->id ?>" <?= ($cu->id == $id_currency) ? 'selected' : '' ?>><?= $cu->type ?></option>
                                <?php
	} ?>
                        </select>
                    </div>
                </div>
            </div>
            
            
            <div class="row">
                <div class="col-md-6">
                    <h5 class="titlecatege">[$Typology]* </h5>
                    <div class="form-group formgroup">
                        <select class="form-control" id="id_typology" name="id_typology">
                            <option value=""></option>
                            <?php foreach ($typology as $tp) {
		?>
                                <option
                                    value="<?= $tp->id ?>" <?= ($tp->id == $id_typology) ? 'selected' : '' ?>><?= $tp->type ?></option>
                                <?php
	} ?>
                        </select>
                    </div>
                </div>
                
                 <div class="col-md-6">
                    <h5 class="titlecatege">[$email]*</h5>

                    <div class="input-group form-group">
                        <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                        <input type="text" required="" class="form-control" id="" name="email" value="<?= $email ?>" placeholder="username@example.com">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h5 class="titlecatege">[$description]* </h5>

                    <div class="form-group">
                        <textarea class="form-control" rows="6" id="descrizione" name="description" placeholder=""
                                  required=""><?= $description ?></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <h5 class="titlecatege">[$address]* </h5>

                    <div class=" formgroup">
                        <div class="input-group">
                            <div class="input-group-addon gps"><a onclick="gps()"><i class="fa fa-compass" aria-hidden="true"></i></a></div>
                            <input type="text" class="form-control" id="address" name="address" value="<?= $address ?>"
                                   placeholder="[$address_placeholder]" onblur="setMarker();" value="">
                            <input type="hidden" id="coordinates" name="coordinates" value="<?= $coordinates ?>">
                        </div>
                    </div>
                    <label class="well-label">[$drag_marker]</label>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 marg-bottm">
                    <h5 class="titlecatege">[$map]</h5>

                    <div id="map" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
            <br>
      

            


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center finish-button">
                    <button type="submit" id="feedbackSubmit" name="save" class="btn btn-md btn-default">[$continue]
                    </button>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="warning" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title">[$warning]</h5>
                        </div>
                        <div class="modal-body alert-warning">
                            <p>[$insert_valid_input]</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">[$close]</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<script>
    $(document).ready(function () {
        fileInput();
    });
</script>
<script>
    // Function to replace inputs

    function fileInput(fi_container_class, fi_button_class, fi_filename_class, fi_button_text) {

        // Arguments
        fi_container_class = fi_container_class || 'fileUpload'; // Classname of the wrapper that contains the button & filename.
        fi_button_class = fi_button_class || 'fileBtn'; // Classname for the button
        fi_filename_class = fi_filename_class || 'fileName'; // Name of the text element's class
        fi_button_text = fi_button_text || '[$upload_image]'; // Text inside the button

        // Variables
        var fi_file = $('input[type=file]'); // Type of input to look for

        // Hide file inputs
        fi_file.css('display', 'none');

        // String to append
        var fi_str = '<div class="' + fi_container_class + '"><div class="' + fi_button_class + '">' + fi_button_text + '</div><div class="' + fi_filename_class + '"></div></div>';
        // Append "fake input" after the original input (which have been hidden)
        fi_file.after(fi_str);

        // Count amount of inputs
        var fi_count = fi_file.length;
        // Loop while "count" is greater than or equal to "i".
        for (var i = 1; i <= fi_count; i++) {
            // Get original input-name
            var fi_file_name = fi_file.eq(i - 1).attr('name');
            // Assign the name to the equivalent "fake input".
            $('.' + fi_container_class).eq(i - 1).attr('data-name', fi_file_name);
        }

        // Button: action
        $('.' + fi_button_class).on('click', function () {
            // Get the name of the clicked "fake-input"
            var fi_active_input = $(this).parent().data('name');
            // Trigger "real input" with the equivalent input-name
            $('input[name=' + fi_active_input + ']').trigger('click');
        });

        // When the value of input changes
        fi_file.on('change', function () {
            // Variables
            var fi_file_name = $(this).val(); // Get the name and path of the chosen file
            var fi_real_name = $(this).attr('name'); // Get the name of changed input

            // Remove path from file-name
            var fi_array = fi_file_name.split('\\'); // Split on backslash (and escape it)
            var fi_last_row = fi_array.length - 1; // Deduct 1 due to 0-based index
            fi_file_name = fi_array[fi_last_row]; //

            // Loop through each "fake input container"
            $('.' + fi_container_class).each(function () {
                // Name of "this" fake-input
                var fi_fake_name = $(this).data('name');
                // If changed "fake button" is equal to the changed input-name
                if (fi_real_name == fi_fake_name) {
                    // Add chosen file-name to the "fake input's label"
                    $('.' + fi_container_class + '[data-name=' + fi_real_name + '] .' + fi_filename_class).html(fi_file_name);
                }
            });
        });
    }
</script>
<script type="text/javascript">
    var lat = <?= $lat ?>;
    var lng = <?= $lng ?>;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: {lat: lat, lng: lng}
    });
    var geocoder = new google.maps.Geocoder();

    var marker;

    geocodeAddress("Tirane", geocoder, map);
    function setMarker() {

        if (marker != null)
            marker.setMap(null);

        var address = document.getElementById('address').value + ',Tirane';

        geocodeAddress(address, geocoder, map);

        if (document.getElementById('address').value != '')
            map.setZoom(15);
        else
            map.setZoom(15);

    }


    function geocodeAddress(address, geocoder, resultsMap) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                });


                google.maps.event.addListener(marker, 'dragend', function () {
                    geocodePosition(marker.getPosition());
                });


                $('#coordinates').val(results[0].geometry.location);
                console.log($('#coordinates').val());
            } else {
                $("#warning").modal();
            }
        });
    }


    function geocodePosition(pos) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode
                ({
                    latLng: pos
                },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                $("#address").val(results[0].formatted_address);
                                $('#coordinates').val(results[0].geometry.location.toString());
                            } else {
                                $("#warning").modal();
                                //$("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
                            }
                        }
                );
    }

    var checkeventcount = 1, prevTarget;
    $('.modal').on('show.bs.modal', function (e) {
        if (typeof prevTarget == 'undefined' || (checkeventcount == 1 && e.target != prevTarget)) {
            prevTarget = e.target;
            checkeventcount++;
            e.preventDefault();
            $(e.target).appendTo('body').modal('show');
        } else if (e.target == prevTarget && checkeventcount == 2) {
            checkeventcount--;
        }
    });


    //auto gps
    function gps() {
        navigator.geolocation.getCurrentPosition(function (location) {
            console.log(location.coords.latitude);
            console.log(location.coords.longitude);
            console.log(location.coords.accuracy);
        });
    }
</script>


<script>
//auto gps
    /*
     function gps(){
     navigator.geolocation.getCurrentPosition(function(location) {
     $("#coordinates").value = location.coords.latitude+","+location.coords.longitude;
     geocodeAddress(location.coords.latitude+","+location.coords.longitude, geocoder, map);
     setMarker();
     });
     }
     */
</script>
