<?php

function printColor($energetic_class)
{
	$string = null;
	switch ($energetic_class) {
		case 'A':
			$string = 'style="background: #7fb800; color:white; font-weight: bold;"';
			break;
		case 'B':
			$string = 'style="background: #91d100; color:white; font-weight: bold;"';
			break;
		case 'C':
			$string = 'style="background: #ebc400; color:white; font-weight: bold;"';
			break;
		case 'D':
			$string = 'style="background: #eb9d00; color:white; font-weight: bold;"';
			break;
		case 'E':
			$string = 'style="background: #e67300; color:white; font-weight: bold;"';
			break;
		case 'F':
			$string = 'style="background: #d22300; color:white; font-weight: bold;"';
			break;
		case 'G':
			$string = 'style="background: #b80000; color:white; font-weight: bold;"';
	}
	return $string;
}
?>

<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/realestate-style.css');
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/map-style.css');
HeadHTML::AddStylesheet('bootstrap-select.min.css');
HeadHTML::AddJS('noframework.waypoints.min.js');
HeadHTML::AddJS('bootstrap-select.min.js');

$w = (isset($parameters['w'])) ? $parameters['w'] : 600;
$h = (isset($parameters['h'])) ? $parameters['h'] : 450;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 150;
HeadHtml::linkJS(Utils::googleMapsJsLink());
?>



<div id="all-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 map-section">
                <?php if ($totalElements != 0) {
	?>
                    <div id="map"  style="height: 100vh; position: fixed;"></div>
                <?php
} ?>
            </div>

            <?php if ($totalElements != 0) {
		?>
                <div class="col-md-6 list-section">
                    <span class="open-map glyphicon glyphicon-chevron-right"></span>
                    <!----- Search result alert ----->
                    <div class="row">
                        <div class="col-md-12">
                            <div id="search-result">
                                <div class="alert alert-grey">
                                    <div class="result">
                                        <h5 class="page-header">[$result_for] [$for_parameters] : </h5>

                                        <?php
										$search = Loader::loadModule('RealEstate/RealEstateSearch');
		$search->setView('advanced-filters');
		$search->setIdContract($_GET['contract']);
		$search->advancedFilters(); ?>

                                    </div>


                                    <div class="row">
                                        <div class="col-md-12" style="line-height: 30px">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12 no-gap left-button">
                                                    <span id="advanced-search" class="btn btn-default btn-sm btn-block search-input search-select ">
                                                        <i class="glyphicon glyphicon-cog" aria-hidden="true"></i>  
                                                        <span class="text-hidden">[$advanced]&nbsp;&nbsp;&nbsp;</span>
                                                    </span>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 no-gap right-button">
                                                    <div class="list-filters">
                                                        <?php
														$order = isset($_GET['order']) ? $_GET['order'] : 'publish_date desc'; ?>

                                                        <form method="GET">
                                                            <?php
															if (count($_GET)) {
																foreach ($_GET as $k => $v) {
																	if ($k != 'order' && $k != 'req') {
																		echo("<input type='hidden' name='$k' value='" . $v . "'/>\n");
																	}
																}
															} ?>
                                                            <div class="input-group btn-block">
                                                                <select class="form-control input-sm" id="order-select"
                                                                        onchange="this.form.submit();" name="order">
                                                                    <option
                                                                        value="price asc" <?= ($order == 'price asc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-asc">[$price]
                                                                    </option>

                                                                    <option
                                                                        value="price desc" <?= ($order == 'price desc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-desc">[$price]
                                                                    </option>

                                                                    <option
                                                                        value="square_meter asc" <?= ($order == 'square_meter asc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-asc">[$square_meter]
                                                                    </option>

                                                                    <option
                                                                        value="square_meter desc" <?= ($order == 'square_meter desc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-desc">[$square_meter]
                                                                    </option>

                                                                    <option
                                                                        value="publish_date desc" <?= ($order == 'publish_date desc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-desc">[$date]
                                                                    </option>

                                                                    <option
                                                                        value="publish_date asc" <?= ($order == 'publish_date asc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-asc">[$date]
                                                                    </option>

                                                                </select>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <!--****************************************************************************************************************-->

                    <!-- Cards list-->
                    <div id="ajax_cards">

                        <div class="row">
                            <div class="col-md-12">
                                <div id="real-estate-card">
                                    <?php
									$i = 0;
		foreach ($realestates as $re) {
			?>
                                        <div class="card card--small" data-markerid="<?php
										if ($re->coordinates != '') {
											echo $i;
											$i++;
										} ?>">

                                            <?php if ($re->featured == '1') {
											?>
                                                <div class="ribbon-wrapper-green">
                                                    <div class="ribbon-green">CONSIGLIATO</div>
                                                </div>
                                            <?php
										} ?>
                                            <div class="card__image" style="background-image: url(<?=Utils::genThumbnailUrl('realestates/' . urlencode($re->image), $w, $h, array('zc' => 1)) ?>)">&nbsp;</div>
                                            <span class="card__title"> <?= $re->title ?> </span>

                                            <span class="card__subtitle"> <?= $re->typology ?> [$in] <?= $re->contract_type ?> </span>

                                            <?php if ($re->hide_price == 0 && $re->price != '0') {
											?>
                                                <span class="price">&euro; <?= number_format($re->price, 0, ',', '.') ?></span>
                                            <?php
										} else {
											?>
                                                <span class="price"><?= '[$price_hidden]' ?> </span>
                                            <?php
										} ?>

                                            <p class="card__text">
                                                <?php if ($re->hide_price == 0 && $re->price != '0') {
											?>
                                                    <span class="price">&euro; <?= number_format($re->price, 0, ',', '.') ?></span>
                                                <?php
										} else {
											?>
                                                    <span class="price"><?= '[$price_hidden]' ?> </span>
                                                <?php
										} ?><br />
                                                <span class="save" style="text-transform:capitalize;"><i class="fa fa-map-marker" aria-hidden="true"></i><?= $re->location ?> - <?= $re->locality ?> - <?= $re->address ?> </span>
                                            </p>

                                            <div class="card__action-bar">
                                                <div class="row">
                                                    <div class="col-md-8 col-sm-6 hidden-xs">
                                                        <?php if ($re->energetic_class == 'A' || $re->energetic_class == 'B' || $re->energetic_class == 'C' || $re->energetic_class == 'D' || $re->energetic_class == 'E' || $re->energetic_class == 'F' || $re->energetic_class == 'G') {
											?>
                                                            <span class="energetic_class" <?= printColor($re->energetic_class) ?>>
                                                                <?= $re->energetic_class ?>
                                                            </span>
                                                        <?php
										} ?>

                                                        <?php if ($re->rooms != 0 and $re->rooms != '' and $re->rooms != null) {
											?>
                                                            <span class="specifics card__button"  style="border-left: 0px;"><i class="fa fa-bed" aria-hidden="true"></i><?= $re->rooms ?></span>
                                                        <?php
										} ?>
                                                        <?php if ($re->bathrooms != 0 and $re->bathrooms != '' and $re->bathrooms != null) {
											?>
                                                            <span class="specifics card__button"><i class="fa fa-bath" aria-hidden="true"></i><?= $re->bathrooms ?></span>
                                                        <?php
										} ?>
                                                        <?php if ($re->square_meter != 0) {
											?>
                                                            <span class="specifics card__button"><?= $re->square_meter ?>m²</span>
                                                        <?php
										} ?>
                                                    </div>

                                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                                        <a href="<?= Utils::getComponentUrl('Real_Estate/RealEstateShow/' . Utils::url_slug($re->title . '-' . $re->id)) ?>">
                                                            <button class="card__button btn btn-block btn-default">[$read_more]</button>
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    <?php
		} ?>
                                </div>
                            </div>
                        </div>
                        <!----- Paggination ----->
                        <div class="col-md-12 pagination">
                            <?php
							require_once DOCROOT . LIBS_PATH . 'Pager.php';
		new Pager();
		Pager::printPager($page, $totalElements, $elements_per_page); ?>
                        </div>

                    </div>
                </div>

            <?php
	} else {
		?>

                <div class="container">
                    <!----- Search result alert ----->
                    <div class="row">
                        <div class="col-md-12">
                            <div id="search-result">
                                <div class="alert alert-grey">
                                    <div class="result">
                                        <h5 class="page-header">[$no_result] : </h5>

                                        <?php
										$search = Loader::loadModule('RealEstate/RealEstateSearch');
		$search->setView('advanced-filters');
		$search->setIdContract($_GET['contract']);
		$search->advancedFilters(); ?>

                                    </div>


                                    <div class="row">
                                        <div class="col-md-12" style="line-height: 30px">
                                            <div class="row">
                                                <div class="col-md-2 col-sm-6 col-xs-12 no-gap left-button">
                                                    <span id="advanced-search" class="btn btn-default btn-sm search-input search-select ">
                                                        <i class="glyphicon glyphicon-cog" aria-hidden="true"></i>  
                                                        <span class="text-hidden">[$advanced]&nbsp;&nbsp;&nbsp;</span>
                                                    </span>
                                                </div>

                                                <div class="col-md-3 col-sm-6 col-xs-12 no-gap right-button">
                                                    <div class="list-filters">
                                                        <?php
														$order = isset($_POST['order']) ? $_POST['order'] : 'relevance desc'; ?>

                                                        <form method="POST">
                                                            <div class="input-group btn-block">
                                                                <select class="form-control input-sm" id="order-select"
                                                                        onchange="this.form.submit();" name="order">
                                                                    <option
                                                                        value="price asc" <?= ($order == 'price asc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-asc">&nbsp;[$price]
                                                                    </option>

                                                                    <option
                                                                        value="price desc" <?= ($order == 'price desc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-desc">&nbsp;[$price]
                                                                    </option>

                                                                    <option
                                                                        value="square_meter asc" <?= ($order == 'square_meter asc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-asc">&nbsp;[$square_meter]
                                                                    </option>

                                                                    <option
                                                                        value="square_meter desc" <?= ($order == 'square_meter desc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-desc">&nbsp;[$square_meter]
                                                                    </option>

                                                                    <option
                                                                        value="publish_date desc" <?= ($order == 'publish_date desc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-desc">&nbsp;[$date]
                                                                    </option>

                                                                    <option
                                                                        value="publish_date asc" <?= ($order == 'publish_date asc') ? 'selected' : '' ?>
                                                                        data-icon="fa fa-sort-amount-asc">&nbsp;[$date]
                                                                    </option>

                                                                </select>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
	} ?>  
        </div>
    </div>


    <script>



        //card
        (function () {

            $(".controller").click(function () {
                id = $(this).attr("id");
                $(".controller-container").find(".is_current").removeClass("is_current");
                $(this).addClass("is_current");
                $(".card").attr('class', 'card card--' + id);
                $("html").attr('class', 'bg--' + id);
            });
        })();
        $('.card').on('click', function () {
            if ($(this).hasClass('card--big')) {
                $(this).addClass('card--small').removeClass('card--big');
            } else if ($(this).hasClass('card--small')) {
                $('.card--big').removeClass('card--big').addClass('card--small');
                $(this).addClass('card--big').removeClass('card--small');
            }
        });
        // Ripple function
        (function () {
            "use strict";
            var colour = "#FF1744";
            var opacity = 0.1;
            var ripple_within_elements = ['input', 'button', 'a'];
            var ripple_without_diameter = 0;
            var overlays = {
                items: [],
                get: function () {
                    var $element;
                    for (var i = 0; i < overlays.items.length; i++) {
                        $element = overlays.items[i];
                        if ($element.transition_phase === false) {
                            $element.transition_phase = 0;
                            return $element;
                        }
                    }
                    $element = document.createElement("div");
                    $element.style.position = "absolute";
                    $element.style.opacity = opacity;
                    //$element.style.outline = "10px solid red";
                    $element.style.pointerEvents = "none";
                    $element.style.background = "-webkit-radial-gradient(" + colour + " 64%, rgba(0,0,0,0) 65%) no-repeat";
                    $element.style.background = "radial-gradient(" + colour + " 64%, rgba(0,0,0,0) 65%) no-repeat";
                    $element.style.transform = "translateZ(0)";
                    $element.transition_phase = 0;
                    $element.rid = overlays.items.length;
                    $element.next_transition = overlays.next_transition_generator($element);
                    document.body.appendChild($element);
                    overlays.items.push($element);
                    return $element;
                },
                next_transition_generator: function ($element) {
                    return function () {
                        $element.transition_phase++;
                        switch ($element.transition_phase) {
                            case 1:
                                $element.style[transition] = "all 500ms cubic-bezier(0.165, 0.840, 0.440, 1.000)";
                                $element.style.backgroundSize = $element.ripple_backgroundSize;
                                $element.style.backgroundPosition = $element.ripple_backgroundPosition;
                                setTimeout($element.next_transition, 0.2 * 1000); //now I know transitionend is better but it fires multiple times when multiple properties are animated, so this is simpler code and (imo) worth tiny delays
                                break;
                            case 2:
                                $element.style[transition] = "opacity 0.15s ease-in-out";
                                $element.style.opacity = 0;
                                setTimeout($element.next_transition, 0.15 * 1000);
                                break;
                            case 3:
                                overlays.recycle($element);
                                break;
                        }
                    };
                },
                recycle: function ($element) {
                    $element.style.display = "none";
                    $element.style[transition] = "none";
                    if ($element.timer)
                        clearTimeout($element.timer);
                    $element.transition_phase = false;
                }
            };
            var transition = function () {
                var i,
                        el = document.createElement('div'),
                        transitions = {
                            'WebkitTransition': 'webkitTransition',
                            'transition': 'transition',
                            'OTransition': 'otransition',
                            'MozTransition': 'transition'
                        };
                for (i in transitions) {
                    if (transitions.hasOwnProperty(i) && el.style[i] !== undefined) {
                        return transitions[i];
                    }
                }
            }();
            var click = function (event) {
                var $element = overlays.get(),
                        touch,
                        x,
                        y;
                touch = event.touches ? event.touches[0] : event;
                $element.style[transition] = "none";
                $element.style.backgroundSize = "3px 3px";
                $element.style.opacity = opacity;
                if (ripple_within_elements.indexOf(touch.target.nodeName.toLowerCase()) > -1) {
                    x = touch.offsetX;
                    y = touch.offsetY;
                    var dimensions = touch.target.getBoundingClientRect();
                    if (!x || !y) {
                        x = (touch.clientX || touch.x) - dimensions.left;
                        y = (touch.clientY || touch.y) - dimensions.top;
                    }
                    $element.style.backgroundPosition = x + "px " + y + "px";
                    $element.style.width = dimensions.width + "px";
                    $element.style.height = dimensions.height + "px";
                    $element.style.left = (dimensions.left) + "px";
                    $element.style.top = (dimensions.top + document.body.scrollTop + document.documentElement.scrollTop) + "px";
                    var computed_style = window.getComputedStyle(event.target);
                    for (var key in computed_style) {
                        if (key.toString().indexOf("adius") > -1) {
                            if (computed_style[key]) {
                                $element.style[key] = computed_style[key];
                            }
                        } else if (parseInt(key, 10).toString() === key && computed_style[key].indexOf("adius") > -1) {
                            $element.style[computed_style[key]] = computed_style[computed_style[key]];
                        }
                    }
                    $element.style.backgroundPosition = x + "px " + y + "px";
                    $element.ripple_backgroundPosition = (x - dimensions.width) + "px " + (y - dimensions.width) + "px";
                    $element.ripple_backgroundSize = (dimensions.width * 2) + "px " + (dimensions.width * 2) + "px";
                } else { //click was outside of ripple element
                    x = touch.clientX || touch.x || touch.pageX;
                    y = touch.clientY || touch.y || touch.pageY;
                    $element.style.borderRadius = "0px";
                    $element.style.left = (x - ripple_without_diameter / 2) + "px";
                    $element.style.top = (document.body.scrollTop + document.documentElement.scrollTop + y - ripple_without_diameter / 2) + "px";
                    $element.ripple_backgroundSize = ripple_without_diameter + "px " + ripple_without_diameter + "px";
                    $element.style.width = ripple_without_diameter + "px";
                    $element.style.height = ripple_without_diameter + "px";
                    $element.style.backgroundPosition = "center center";
                    $element.ripple_backgroundPosition = "center center";
                    $element.ripple_backgroundSize = ripple_without_diameter + "px " + ripple_without_diameter + "px";
                }
                $element.ripple_x = x;
                $element.ripple_y = y;
                $element.style.display = "block";
                setTimeout($element.next_transition, 20);
            };
            if ('ontouchstart' in window || 'onmsgesturechange' in window) {
                document.addEventListener("touchstart", click, false);
            } else {
                document.addEventListener("click", click, false);
            }
        }());
    </script>

    <?php require_once LIBS_PATH . 'StringUtils.php'; ?>
    <?php if (count($locations) > 1 || (count($locations) == 1 && $locations[0]->coordinates != '')) {
		?>

        <?php HeadHtml::linkJS('markerwithlabel.js'); ?>


        <script type="text/javascript">
            var locations = [
    <?php
	$counter = 1;
		foreach ($locations as $loc) {
			$loc->title = str_replace("'", "\'", $loc->title);
			$loc->address = str_replace("'", "\'", $loc->address);
			$loc->price = str_replace("'", "\'", $loc->price);
			//$price = "&euro;" . Utils::numberFormatToK($loc->price);
			$price = ($loc->hide_price == '1') ? 'ND' : Utils::numberFormatToK($loc->price);
			$id = $loc->id;

			if ($loc->image != '') {
				$fullImage = Utils::genThumbnailUrl('realestates' . DS . $loc->image, 200, 100, array('zc' => 1));
			} else {
				$fullImage = WEBROOT . $this->view_path . 'img/default.jpg';
			}

			if ($loc->coordinates != '') {
				list($lat, $lng) = explode(',', $loc->coordinates);
				$id = $loc->id;
				echo "[ '{$loc->title}' , {$lat} , {$lng} , {$counter} , '{$loc->address}', '{$id}' , '" . str_replace("'", "\'", $fullImage) . "', '{$price}' ],\n";

				$counter++;
			}
		} ?>
            ];
    <?php
	if (count($locations) >= 1) {
		$map_center = $locations[0]->coordinates;
	} ?>
            //  new map
            var map = new google.maps.Map(document.getElementById('map'), {
                //zoom: 14,
                center: new google.maps.LatLng(<?= $map_center ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: true,
                draggable: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                zoomControl: true,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                }
            }
            );
            //  var infowindow = new google.maps.InfoWindow();
            var infowindow = new google.maps.InfoWindow({
                maxWidth: 400
            });
            var marker, i;
            var markerImage = 'marker.png';
            var contentString = [];
            var bounds = new google.maps.LatLngBounds();
            var markers = [];
            for (i = 0; i < locations.length; i++) {


                var map_price = 'ND';
                if (locations[i][7] != '0' && locations[i][7] != '' && locations[i][7] != 'ND') {
                    map_price = "&euro; " + locations[i][7];
                }


                marker = new MarkerWithLabel({
                    // marker position
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    draggable: false,
                    raiseOnDrag: true,
                    labelContent: map_price,
                    labelAnchor: new google.maps.Point(20, 50),
                    labelClass: "labels", // the CSS class for the label
                    labelId: (locations[i][5]),
                    labelInBackground: false,
                    icon: 'http://www.livingcasa.eu/data/livingcasa.eu/media/marker2.png'
                });
                contentString[i] = '<div class="iw-container" data-markerid="' + locations[i][5] + '">' +
                        '<div class="iw-content">' +
                        '<img src="' + locations[i][6] + '" alt="<?= CMSSettings::$webdomain ?> - ' + locations[i][0] + ' Real Estate id - ' + locations[i][5] + '" height="100" width="200">' +
                        ' <a href= " <?= Utils::getComponentUrl('Real_Estate/RealEstateShow') . '/' ?>' + locations[i][5] + '"><div class="iw-subTitle">' + locations[i][0] + '</div>' +
                        '</div>' +
                        '<div class="iw-bottom-gradient"></div>' +
                        '</div>';
                bounds.extend(marker.position);
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(contentString[i]);
                        infowindow.open(map, marker);
                    };
                })(marker, i));
                markers.push(marker);
            }
            map.fitBounds(bounds);
            google.maps.event.addListener(infowindow, 'domready', function () {


                //var bounds = new google.maps.LatLngBounds();
                // Begin loop
                //        bounds.extend(new google.maps.LatLng(<?= $map_center ?>));
                // End loop
                //map.fitBounds(bounds);

                // Reference to the DIV that wraps the bottom of infowindow
                var iwOuter = $('.gm-style-iw');
                /* Since this div is in a position prior to .gm-div style-iw.
                 * We use jQuery and create a iwBackground variable,
                 * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
                 */
                var iwBackground = iwOuter.prev();
                // Removes background shadow DIV
                iwBackground.children(':nth-child(2)').css({'display': 'none'});
                // Removes white background DIV
                iwBackground.children(':nth-child(4)').css({'display': 'none'});
                // Moves the infowindow 45px to the right.
                iwOuter.parent().parent().css({left: '25px'});
                // Moves the shadow of the arrow 76px to the left margin.
                iwBackground.children(':nth-child(1)').attr('style', function (i, s) {
                    return s + 'left: 90px !important;'
                });
                // Moves the arrow 76px to the left margin.
                iwBackground.children(':nth-child(3)').attr('style', function (i, s) {
                    return s + 'left: 90px !important;'
                });
                // Changes the desired tail shadow color.
                iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0) 0px 1px 6px', 'z-index': '1'});
                // Reference to the div that groups the close button elements.
                var iwCloseBtn = iwOuter.next();
                // Apply the desired effect to the close button
                iwCloseBtn.css({width: '25px', height: '25px', opacity: '1', right: '45px', top: '3px', border: '6px solid #ffffff', 'border-radius': '13px', 'box-shadow': '0 0 5px #9c9c9c'});
                // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
                //    if($('.iw-content').height() < 140){
                //      $('.iw-bottom-gradient').css({display: 'none'});
                //    }

                // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
                iwCloseBtn.mouseout(function () {
                    $(this).css({opacity: '1'});
                });
            });
            //map listeners 
            $('.card').on('mouseover', function () {
                google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
                map.panTo(new google.maps.LatLng(locations[$(this).data('markerid')][1], locations[$(this).data('markerid')][2]));
            });
            //  map style
            var styles = [{"stylers": [{"saturation": -100}]}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#0099dd"}]}, {"elementType": "labels", "stylers": [{"visibility": "off"}]}, {"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"color": "#aadd55"}]}, {"featureType": "road.highway", "elementType": "labels", "stylers": [{"visibility": "on"}]}, {"featureType": "road.arterial", "elementType": "labels.text", "stylers": [{"visibility": "on"}]}, {"featureType": "road.local", "elementType": "labels.text", "stylers": [{"visibility": "on"}]}, {}];
            map.set('styles', styles);
            google.maps.event.addDomListener(window, 'load');
            $('.open-map').click(function () {
                var currCenter = map.getCenter();
                $('.map-section').toggleClass('col-md-6');
                $('.map-section').toggleClass('col-md-10');
                $('.list-section').toggleClass('col-md-6');
                $('.list-section').toggleClass('col-md-2');
                $('.list-section').toggleClass('transform');
                $('.open-map').toggleClass('glyphicon-chevron-right');
                $('.open-map').toggleClass('glyphicon-chevron-left');
                google.maps.event.trigger(map, 'resize');
                map.setCenter(currCenter);
            });


        </script>

    <?php
	} ?>

    <script>
        $('#order-select').selectpicker({
            style: 'btn-default btn-sm'
        });
    </script>
    <script>
        $("#advanced-search").click(
                function () {
                    $("#advanced-search").toggleClass("advanced-black");
                    $("#advanced-form").toggle("slow");
                });
    </script>
</div>
