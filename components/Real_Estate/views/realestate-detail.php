<?php HeadHTML::addMeta('keywords', $realestate->title . ' , ' . $realestate->typology . '  , ' . $realestate->contract_type . '  , ' . CMSSettings::$website_title . ' , ' . $realestate->location . ' , ' . $realestate->locality); ?>
<?php HeadHTML::addMeta('description', StringUtils::CutString(strip_tags($realestate->description), 200)); ?>
<?php HeadHTML::addMetaProperty('og:title', $realestate->title . ' | ' . CMSSettings::$website_title); ?>
<?php HeadHTML::addMetaProperty('og:type', 'article'); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($realestate->description), 200)); ?>



<?php

function printColor($energetic_class)
{
	$string = null;
	switch ($energetic_class) {
		case 'A':
			$string = 'style="background: #7fb800; color:white; font-weight: bold;"';
			break;
		case 'B':
			$string = 'style="background: #91d100; color:white; font-weight: bold;"';
			break;
		case 'C':
			$string = 'style="background: #ebc400; color:white; font-weight: bold;"';
			break;
		case 'D':
			$string = 'style="background: #eb9d00; color:white; font-weight: bold;"';
			break;
		case 'E':
			$string = 'style="background: #e67300; color:white; font-weight: bold;"';
			break;
		case 'F':
			$string = 'style="background: #d22300; color:white; font-weight: bold;"';
			break;
		case 'G':
			$string = 'style="background: #b80000; color:white; font-weight: bold;"';
	}
	return $string;
}
?>


<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/realestate-detail.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/map-style.css'); ?>

<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 200;
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 300;
$currency = isset($realestate->currency) ? $realestate->currency : 'EUR';
?>

<script type="text/javascript">
    'function' != typeof loadGsLib && (loadGsLib = function () {
        var e = document.createElement("script");
        e.type = "text/javascript", e.async = !0, e.src = '//api.at.getsocial.io/widget/v1/gs_async.js?id=080ddc';
        var t = document.getElementsByTagName("script")[0];
        t.parentNode.insertBefore(e, t);
    })();</script>

<div id="announce-detail">
    <div class="row">


        <?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
	?>
            <div class="col-xs-12">
                <?php $this->ComponentErrors->renderHTML(); ?>
                <?php $this->ComponentMessages->renderHTML(); ?>
            </div>
        <?php
} ?>

        <div class="col-md-8">
            <!--RealEstate Image-->
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-title">
                                <?= $realestate->title ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--RealEstate Image-->
                <div class="row">
                    <div class="col-md-12">
                        <?php
						if ($realestate->image != '') {
							HeadHTML::addMetaProperty('og:image', 'http://' . strtolower(CMSSettings::$webdomain) . Utils::genThumbnailUrl('realestates/' . str_replace(' ', '%20', $realestate->image), 1000, 700, array('zc' => 1))); ?>
                            <a class="" href="<?= Utils::genThumbnailUrl('realestates/' . $realestate->image, 800, 500, array('far' => '1', 'bg' => 'FFFFFF')) ?>" data-lightbox="image-1">
                                <img class="group list-group-image" src="<?= Utils::genThumbnailUrl('realestates/' . $realestate->image, $w, $h, array('zc' => 1)) ?>" width="100%">
                            </a>
                        <?php
						} ?>
                    </div>
                </div>
                <!--Announce Additional Image-->
                <div class="row additional-images">
                    <div class="col-md-12">

                        <?php
						if (count($realestate->images)) {
							foreach ($realestate->images as $image) {
								?>

                                <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 0px;">
                                    <div class="item">
                                        <a class="" href="http://<?= CMSSettings::$webdomain . Utils::genThumbnailUrl('realestates' . DS . 'additional/' . $image->image) ?>" data-lightbox="image-1">
                                            <img class="lazyOwl" src="<?= Utils::genThumbnailUrl('realestates' . DS . 'additional/' . $image->image, 200, 200, array('zc' => '1')) ?>" alt="Immagine" width="100%" style="padding: 3px 3px 0px 0px;">
                                        </a>
                                    </div>
                                </div>
                                <?php
							}
						}
						?>

                    </div>
                </div>



                <div class="card-inner">
                    <hr>

                    <div class="card-main">
                      <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12 share-in-social-area medium-margin-bottom ">
                            <div class="sharepost">
                                <ul class="shareSocial">
                                    <li class="share-title small-font-size xs-margin-top">[$share_in_social_media] </li>
                                    <li class="getsocial"><a href="https://www.facebook.com/sharer/sharer.php?u=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'FacebookShare',600,250); return false;" target="_blank"> <i class="fa fa-facebook-official" aria-hidden="true"> </i> </a></li>
                                    <li class="getsocial"><a href="https://twitter.com/home?status=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'LinkedinShare',600,250); return false;" target="_blank"> <i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                                    <li class="getsocial"><a href="https://plus.google.com/share?url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'GooglePlusShare',600,250); return false;" target="_blank"> <i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                                    <li class="getsocial"><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'LinkedinShare',600,250); return false;"  target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                          </div>
                      </row>
                        <div class="getsocial gs-inline-group float-right">
                           
                        </div>
                        <h4 class="page-header title-header">[$real_estate_description]</h4>
                        <?= nl2br($realestate->description) ?>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 marg-bottm">
                                    <div id="wrap-map" class="wrap-map" style="width:100%; height: 300px;"></div>

                                    <?php HeadHtml::linkJS(Utils::googleMapsJsLink()); ?>
                                    <?php HeadHtml::linkJS('markerwithlabel.js'); ?>
                                    <script type="text/javascript">
                                        google.maps.event.addDomListener(window, 'load', init);
                                        function init() {
                                            // Basic options for a simple Google Map
                                            var featureOpts = [{"stylers": [{"saturation": -100}]}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#0099dd"}]}, {"elementType": "labels", "stylers": [{"visibility": "off"}]}, {"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"color": "#aadd55"}]}, {"featureType": "road.highway", "elementType": "labels", "stylers": [{"visibility": "on"}]}, {"featureType": "road.arterial", "elementType": "labels.text", "stylers": [{"visibility": "on"}]}, {"featureType": "road.local", "elementType": "labels.text", "stylers": [{"visibility": "on"}]}, {}];
                                            var markerPosition = new google.maps.LatLng(<?= $realestate->coordinates ?>);
                                            var mapOptions = {
                                                zoom: 15,
                                                scrollwheel: false,
                                                // The latitude and longitude to center the map (always required)
                                                center: markerPosition,
                                                // How you would like to style the map. 
                                                styles: featureOpts
                                            };
                                            var mapElement = document.getElementById('wrap-map');
                                            // Create the Google Map using our element and options defined above
                                            var map = new google.maps.Map(mapElement, mapOptions);

<?php
if ($realestate->price != '0' && $realestate->price != '') {
							$map_price = '&euro;' . Utils::numberFormatToK($realestate->price);
						} else {
							$map_price = 'ND';
						}
?>
                                            marker = new MarkerWithLabel({
                                                // marker position
                                                position: markerPosition,
                                                map: map,
                                                draggable: false,
                                                raiseOnDrag: true,
                                                labelContent: (<?= "'$map_price'" ?>),
                                                labelAnchor: new google.maps.Point(20, 50),
                                                labelClass: "labels", // the CSS class for the label
                                                labelInBackground: false,
                                                icon: 'http://www.livingcasa.eu/data/livingcasa.eu/media/marker2.png'
                                            });


                                            var oldCenter = map.getCenter();
                                            // Center change
                                            google.maps.event.addListener(map, 'dragend', function () {
                                                oldCenter = map.getCenter();
                                            });
                                            //Center map
                                            google.maps.event.addDomListener(window, 'resize', function () {
                                                map.setCenter(oldCenter);
                                                oldCenter = map.getCenter();
                                            });
                                        }

                                    </script>


                                    <script>
                                        $(document).ready(function () {


                                            function checkContact(id = '', tipi = '') {
                                                $.ajax({
                                                    type: 'POST',
                                                    data: {ajx_request: 'new'},
                                                    url: "<?= Utils::getComponentUrl('Announces/ajx_showContactInfo') . '/' . $realestate->id ?>",
                                                    dataType: 'JSON',
                                                    success: function (result) {
                                                        var data = result.toString();
                                                        var res = data.split(',');
                                                        if (tipi == 'email') {
                                                            id.html(res[1]);
                                                        } else {
                                                            id.html(res[0]);
                                                        }


                                                    },
                                                    error: function (result) {
                                                        console.log(result);
                                                    }

                                                });
                                            }

                                            $('#show-tel').click(function (event) {
                                                event.preventDefault();
                                                checkContact($('#tel'), 'tel');
                                            });
                                            $('#show-email').click(function (event) {
                                                //alert('Hello');
                                                event.preventDefault();
                                                checkContact($('#email'), 'email');
                                            });
                                        });


                                    </script>


                                </div>
                            </div>
                        </div>

                    </div>
                    <hr/>
                    

                </div>
            </div>
        </div>
    </div>

        <div class="col-md-4">
            <div class="alert alert-grey ">
                <h4 class="text-center title-header">[$announce_information]</h4>
                <table class="table table-striped">
                    <tbody>

<?php if ($realestate->publish_date != '') {
	?>
                            <tr>
                                <td class="re-features">[$date]</td>
                                <td><?= date('d ', strtotime($realestate->publish_date)) . ' ' . Utils::ItalianWordMonth(date('n', strtotime($realestate->publish_date))) . ' ' . date('Y', strtotime($realestate->publish_date)); ?></td>
                            </tr>
                        <?php
} ?>

<?php if ($realestate->location != '') {
		?>
                            <tr>
                                <td class="re-features">[$location]</td>
                                <td><?= $realestate->location ?></td>
                            </tr>
<?php
	} ?>




<?php if ($realestate->contract_type != '') {
		?>
                            <tr>
                                <td class="re-features">[$contract_type]</td>
                                <td><?= $realestate->contract_type ?></td>
                            </tr>
                        <?php
	} ?>

<?php if ($realestate->typology != '') {
		?>
                            <tr>
                                <td class="re-features">[$typology]</td>
                                <td><?= $realestate->typology ?></td>
                            </tr>
<?php
	} ?>




<?php if ($realestate->price != '' && $realestate->price != '0' && $realestate->hide_price == '0') {
		?>
                            <tr>
                                <td class="re-features">[$price]</td>


                                <td>
                                    <?php if ($realestate->hide_price == 0) {
			?>
                                        <?= (number_format($realestate->price, 0, ',', '.') . ' ' . $currency) ?>
                                    <?php
		} else {
			?>
                                        <?= '[$price_hidden]' ?>
    <?php
		} ?>
                                </td>

                            </tr>
<?php
	} else {
		?>
                            <tr>
                                <td class="re-features">[$price]</td>
                                <td>[$price_hidden]</td>

                            </tr>
<?php
	} ?>


<?php if ($realestate->address != '') {
		?>
                            <tr>
                                <td class="re-features">[$address]</td>
                                <td><?= $realestate->address ?></td>
                            </tr>
<?php
	} ?>



                    </tbody>
                </table>
            </div>

            <div class="alert alert-grey ">
                <h4 class="text-center title-header">[$real_estate_charachteristics]</h4>
                <table class="table table-striped">
                    <tbody>

<?php if ($realestate->square_meter != '' && $realestate->square_meter != '0') {
		?>
                            <tr>
                                <td class="re-features">[$square_meter]</td>
                                <td><?= $realestate->square_meter ?> m²</td>
                            </tr>
                        <?php
	} ?>

<?php if ($realestate->rooms != '') {
		?>
                            <tr>
                                <td class="re-features">[$rooms]</td>
                                <td><?= $realestate->rooms ?></td>
                            </tr>
                        <?php
	} ?>

<?php if ($realestate->bathrooms != '') {
		?>
                            <tr>
                                <td class="re-features">[$bathrooms]</td>
                                <td><?= $realestate->bathrooms ?></td>
                            </tr>
                        <?php
	} ?>

<?php if ($realestate->garden != '' && $realestate->garden == '1') {
		?>
                            <tr>
                                <td class="re-features">[$garden]</td>
                                <td>Si</td>
                            </tr>
<?php
	} else {
		?>
                            <tr>
                                <td class="re-features">[$garden]</td>
                                <td>No</td>
                            </tr>
                        <?php
	} ?>

<?php if ($realestate->furnished != '' && $realestate->furnished == '1') {
		?>
                            <tr>
                                <td class="re-features">[$furnished]</td>
                                <td>Si</td>
                            </tr>
<?php
	} else {
		?>
                            <tr>
                                <td class="re-features">[$furnished]</td>
                                <td>No</td>
                            </tr>
                        <?php
	} ?>

<?php if ($realestate->balcony != '' && $realestate->balcony == '1') {
		?>
                            <tr>
                                <td class="re-features">[$balcony]</td>
                                <td>Si</td>
                            </tr>
<?php
	} else {
		?>
                            <tr>
                                <td class="re-features">[$balcony]</td>
                                <td>No</td
                            </tr>
                        <?php
	} ?>

<?php if ($realestate->terrace != '' && $realestate->terrace == '1') {
		?>
                            <tr>
                                <td class="re-features">[$terrace]</td>
                                <td>Si</td>
                            </tr>
<?php
	} else {
		?>
                            <tr>
                                <td class="re-features">[$terrace]</td>
                                <td>No</td>
                            </tr>
                        <?php
	} ?>

<?php if ($realestate->floor != '') {
		?>
                            <tr>
                                <td class="re-features">[$floor]</td>
                                <td><?= $realestate->floor ?></td>
                            </tr>
                        <?php
	} ?>

<?php if ($realestate->heating != '') {
		?>
                            <tr>
                                <td class="re-features">[$heating]</td>
                                <td><?= $realestate->heating ?></td>
                            </tr>
                        <?php
	} ?>

<?php if ($realestate->building != '') {
		?>
                            <tr>
                                <td class="re-features">[$building]</td>
                                <td><?= $realestate->building ?></td>
                            </tr>
                        <?php
	} ?>

<?php if (isset($realestate->energetic_class) && $realestate->energetic_class != '') {
		switch ($realestate->energetic_class) {
			case 'Y': $enclass = '[$requesting]'; break;
			case 'X': $enclass = '[$missing]'; break;
			default: $enclass = $realestate->energetic_class;
		} ?>
                            <tr <?= printColor($realestate->energetic_class) ?> >
                                <td class="re-features">[$energetic_class]</td>
                                <td ><?= $enclass; ?></td>
                            </tr>
<?php
	} ?>

                    </tbody>
                </table>
            </div>

            <div class="alert alert-grey">
                <div class="requestRe">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">                        
                            <h4 class="page-header title-header">[$RequestInformations]</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form id="contact-form" class="form" action="#" method="POST" role="form">
                                <div class="form-group">
                                    <label class="form-label" for="name">[$name]</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="[$your_name]" tabindex="1" required>
                                </div>                            
                                <div class="form-group">
                                    <label class="form-label" for="email">[$email]</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="[$your_email]" tabindex="2" required>
                                </div>                            

                                <div class="form-group">
                                    <label class="form-label" for="message">[$message]</label>
                                    <textarea rows="5" cols="50" name="message" class="form-control" id="message" placeholder="[$your_message]..." tabindex="4" required></textarea>                                 
                                </div>
                                <div class="text-center">
                                    <button type="submit" name="save-info" class="btn btn-success">[$save]</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



