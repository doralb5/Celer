<script src="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.min.js"></script>
<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.indigo-pink.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<?php
$name = (isset($_POST['name'])) ? $_POST['name'] : '';
$surname = (isset($_POST['surname'])) ? $_POST['surname'] : '';
$phone = (isset($_POST['phone'])) ? $_POST['phone'] : '';
$cel = (isset($_POST['cel'])) ? $_POST['cel'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
$location = (isset($_POST['location'])) ? $_POST['location'] : '';
$city = (isset($_POST['city'])) ? $_POST['city'] : '';
$zip_code = (isset($_POST['zip_code'])) ? $_POST['zip_code'] : '';
$street = (isset($_POST['street'])) ? $_POST['street'] : '';
$address = (isset($_POST['address'])) ? $_POST['address'] : '';
$surface = (isset($_POST['surface'])) ? $_POST['surface'] : '';
$rooms = (isset($_POST['rooms'])) ? $_POST['rooms'] : '';
$bathrooms = (isset($_POST['bathrooms'])) ? $_POST['bathrooms'] : '';
$description = (isset($_POST['description'])) ? $_POST['description'] : '';
$garden = isset($_POST['garden']) ? $_POST['garden'] : '';
$furnished = isset($_POST['furnished']) ? $_POST['furnished'] : '';
$terrace = isset($_POST['terrace']) ? $_POST['terrace'] : '';

$balcony = isset($_POST['balcony']) ? $_POST['balcony'] : '';
$energetic_class = isset($_POST['energetic_class']) ? $_POST['energetic_class'] : '';
$price = isset($_POST['price']) ? $_POST['price'] : '';
?>
<div class="row">

    <div class="col-md-12">
        
        <?php Messages::showComponentErrors('Real_Estate'); ?>
        <?php Messages::showComponentMsg('Real_Estate'); ?>
        
        <form   method = "post" autocomplete = "on" class = "clienteform" >
            <div id = "internal_main_container">

                <!--************************************************************************************-->
                <h3 class="page-header">[$personal_info]</h3>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$name]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" placeholder="[$name]" name="name" value="<?= $name ?>" required="">
                        </div>
                    </div>
                </div>
                <div class="form-group">   
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$surname]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" placeholder="[$surname]" name="surname" value="<?= $surname ?>" required="">
                        </div>
                    </div>
                </div>
                <div class="form-group">   
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$phone]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="number" class="form-control" placeholder="[$phone]" name="phone" value="<?= $phone ?>"required="" >
                        </div>
                    </div>
                </div>
                <div class="form-group">   
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$cel]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="number" class="form-control" placeholder="[$cel]" name="cel" value="<?= $cel ?>" >
                        </div>
                    </div>
                </div>
                <div class="form-group"> 
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$email]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="email" class="form-control" placeholder="[$email]" name="email" value="<?= $email ?>" required="">
                        </div>
                    </div>
                </div>
                <!--************************************************************************************-->
                <h3 class="page-header">[$request_zone]</h3>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$city]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" placeholder="[$city]" name="city" value="<?= $city ?>" required="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$location]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" placeholder="[$location]" name="location" value="<?= $location ?>" >
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$zip_code]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="number" class="form-control" placeholder="[$zip_code]" name="zip_code" value="<?= $zip_code ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$street]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" placeholder="[$street]" name="street" value="<?= $street ?>" >
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$address]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" placeholder="[$address]" name="address" value="<?= $address ?>" >
                        </div>
                    </div>
                </div>

                <!--************************************************************************************-->

                <h3 class="page-header">[$house_location]</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class=" control-label">[$contract]</label>
                            <div class="">
                                <select class="form-control" name="id_contract" placeholder="[$contract]" required="">
                                    <option value="">[$select_contract]</option>
                                    <?php foreach ($contract as $cnt) {
	?>
                                        <option value="<?= $cnt->id ?>"><?= $cnt->type ?></option>                    
                                    <?php
} ?>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">[$typology]</label>
                            <div class="">
                                <select class="form-control" name="id_typology" placeholder="[$typology]" required="">
                                    <option value="">[$select_typology]</option>
                                    <?php foreach ($typologies as $typology) {
		?>
                                        <option value="<?= $typology->id ?>"><?= $typology->type ?></option>                    
                                    <?php
	} ?>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">   
                            <label class="control-label">[$surface]</label>
                            <div class="">
                                <input type="number" class="form-control" placeholder="[$surface]" name="surface" value="<?= $surface ?>" required="">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">   
                            <label class="control-label">[$rooms]</label>
                            <div class="">
                                <input type="number" class="form-control" placeholder="[$rooms]" name="rooms" value="<?= $rooms ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">   
                            <label class=" control-label">[$bathrooms]</label>
                            <div class="">
                                <input type="number" class="form-control" placeholder="[$bathrooms]" name="bathrooms" value="<?= $bathrooms ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table">
                    <tr>
                        <td>
                            <label class="mdl-checkbox mdl-js-checkbox" for="garden">
                                <input name="garden" type="checkbox" id="garden" class="mdl-checkbox__input"  <?= $garden ?> >
                                <span class="mdl-checkbox__label">[$garden]</span>
                            </label>
                        </td>

                        <td>
                            <label class="mdl-checkbox mdl-js-checkbox" for="furnished">
                                <input  name="furnished" type="checkbox" id="furnished" class="mdl-checkbox__input"  <?= $furnished ?> >
                                <span class="mdl-checkbox__label">[$furnished]</span>
                            </label>
                        </td>

                        <td>
                            <label class="mdl-checkbox mdl-js-checkbox" for="balcony">
                                <input  name="balcony" type="checkbox" id="balcony" class="mdl-checkbox__input"  <?= $balcony ?> >
                                <span class="mdl-checkbox__label">[$balcony]</span>
                            </label>
                        </td>

                        <td>
                            <label class="mdl-checkbox mdl-js-checkbox" for="terrace">
                                <input name='terrace' type="checkbox" id="terrace" class="mdl-checkbox__input" <?= $terrace ?> >
                                <span class="mdl-checkbox__label">[$terrace]</span>
                            </label>
                        </td>
                    </tr>
                </table>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">[$floor]</label>
                            <div class="">
                                <select class="form-control" name="id_floor" placeholder="[$floor]">
                                    <option value="">[$select_floor]</option>
                                    <?php foreach ($floors as $floor) {
		?>
                                        <option value="<?= $floor->id ?>"><?= $floor->name ?></option>                    
                                    <?php
	} ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">[$building]</label>
                            <div class="">
                                <select class="form-control" name="id_building" placeholder="[$building]">
                                    <select<option value="">[$select_building]</option>
                                        <?php foreach ($buildings as $building) {
		?>
                                            <option value="<?= $building->id ?>"><?= $building->type ?></option>                    
                                        <?php
	} ?>
                                    </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">[$heating]</label>
                            <div class="">
                                <select class="form-control" name="id_heating" placeholder="[$heating]">
                                    <option value="">[$select_heating]</option>
                                    <?php foreach ($heatings as $heating) {
		?>
                                        <option value="<?= $heating->id ?>"><?= $heating->name ?></option>                    
                                    <?php
	} ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="">[$energetic_class]</label>

                            <div class="">
                                <select class="form-control" id="" name="energetic_class">
                                    <option <?php if ($energetic_class == '') {
		echo 'selected';
	} ?> value="">In fase di richiesta</option>
                                    <option <?php if ($energetic_class == 'X') {
		echo 'selected';
	} ?> value="X">Esente</option>
                                    <option <?php if ($energetic_class == 'A') {
		echo 'selected';
	} ?> value="A">A</option>
                                    <option <?php if ($energetic_class == 'B') {
		echo 'selected';
	} ?> value="B">B</option>
                                    <option <?php if ($energetic_class == 'C') {
		echo 'selected';
	} ?> value="C">C</option>
                                    <option <?php if ($energetic_class == 'D') {
		echo 'selected';
	} ?> value="D">D</option>
                                    <option <?php if ($energetic_class == 'E') {
		echo 'selected';
	} ?> value="E">E</option>
                                    <option <?php if ($energetic_class == 'F') {
		echo 'selected';
	} ?> value="F">F</option>
                                    <option <?php if ($energetic_class == 'G') {
		echo 'selected';
	} ?> value="G">G</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <hr>
                <br>

                <div class="form-group">   
                    <div class="row">
                        <label class="col-md-4 col-sm-4 control-label">[$price]</label>
                        <div class="col-md-8 col-sm-8">
                            <input type="number" class="form-control" placeholder="[$price]" name="price" value="<?= $price ?>"required="" >
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-md-4 control-label">[$description]</label>
                        <div class="col-md-8">
                            <textarea maxlength="400" class="form-control" rows="6" id="descrizione" name="description" placeholder="[$description]" value="<?= $description ?>"></textarea>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div style = "margin-top: 10px;">[$privacy]</div>
                            <div class="trattamento" style="height: 150px; background-color: #fff; border: 1px solid #ccc; padding: 3px; font-size: 12px; overflow: auto;"> 
                                [$terms]
                            </div>
                        </div>
                    </div>
                </div>
                <p>
                    <input type = "radio" name = "privacy" value = "SI" > [$accept]
                </p>

                <!--************************************************************************************-->






                <div id = "errorsinform"></div>

                <div class="g-recaptcha" data-sitekey="6LdFNBgUAAAAAARY0qLZoydos4NZbDhh_eS57Ixa" style="margin: 0 auto; display: table;padding-bottom: 1em;" required></div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button class = "form_submit btn btn-md btn-default" name="save" id = "submitform">[$send]</button>
                        </div>
                    </div>
                </div>


            </div><!--# internalcontainer -->

        </form>
    </div>
</div>
<script>





    $(document).ready(function () {
        $('.checkbox-ripple').rkmd_checkboxRipple();
        change_checkbox_color();
    });

    (function ($) {

        $.fn.rkmd_checkboxRipple = function () {
            var self, checkbox, ripple, size, rippleX, rippleY, eWidth, eHeight;
            self = this;
            checkbox = self.find('.input-checkbox');

            checkbox.on('mousedown', function (e) {
                if (e.button === 2) {
                    return false;
                }

                if ($(this).find('.ripple').length === 0) {
                    $(this).append('<span class="ripple"></span>');
                }
                ripple = $(this).find('.ripple');

                eWidth = $(this).outerWidth();
                eHeight = $(this).outerHeight();
                size = Math.max(eWidth, eHeight);
                ripple.css({'width': size, 'height': size});
                ripple.addClass('animated');

                $(this).on('mouseup', function () {
                    setTimeout(function () {
                        ripple.removeClass('animated');
                    }, 200);
                });

            });
        }

    }(jQuery));

    function change_checkbox_color() {
        $('.color-box .show-box').on('click', function () {
            $(".color-box").toggleClass("open");
        });

        $('.colors-list a').on('click', function () {
            var curr_color = $('main').data('checkbox-color');
            var color = $(this).data('checkbox-color');
            var new_colot = 'checkbox-' + color;

            $('.rkmd-checkbox .input-checkbox').each(function (i, v) {
                var findColor = $(this).hasClass(curr_color);

                if (findColor) {
                    $(this).removeClass(curr_color);
                    $(this).addClass(new_colot);
                }

                $('main').data('checkbox-color', new_colot);

            });
        });
    }









</script>