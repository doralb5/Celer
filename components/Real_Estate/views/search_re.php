<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/search-style.css'); ?>

<div class="banner-right">

    <div class="resizable_bg" style="margin-top:20px;">
        <div class="header_bg">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <h3><span>Search For Home</span></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">


        <div class="col-md-12 border-left">

            <?php Messages::showErrors(); ?>
            <?php Messages::showInfoMsg(); ?>


            <?php if (count($this->ComponentMessages->getAll()) || count($this->ComponentErrors->getAll())) {
	?>
                <div class="row">
                    <div class="col-xs-12">
                        <?php $this->ComponentErrors->renderHTML(); ?>
                        <?php $this->ComponentMessages->renderHTML(); ?>
                    </div>
                </div>
            <?php
} ?>






            <div class="row">

                <div class="col-md-6">
                    <h5 class="titlecatege">[$annonuce_category]* </h5>
                    <div class="form-group formgroup">
                        <select class="form-control" id="id_category" name="category_id">
                            <option value=""></option>
                            <?php foreach ($categories as $categ) {
		?>
                                <option
                                    value="<?= $categ->id ?>" <?= ($categ->id == $id_category) ? 'selected' : '' ?>><?= $categ->category ?></option>
                                <?php
	} ?>
                        </select>
                        <label class="well-label">[$announce_example]</label>
                    </div>
                </div>



                <div class="col-md-6">
                    <h5 class="titlecatege">[$announce_user]* </h5>

                    <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
		?>
                        <div class="form-group formgroup">
                            <input type="text" required="" class="form-control" id="" name="name_a"
                                   value="<?= $_SESSION['user_auth']['firstname'] ?>" placeholder="Emri/Mbiemri" readonly="true">
                        </div>
                    <?php
	} else {
		?>
                        <div class="form-group formgroup">
                            <input type="text" required="" class="form-control" id="" name="name_a"
                                   value="<?= $name ?>" placeholder="Emri/Mbiemri">
                        </div>

                    <?php
	} ?>

                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h5 class="titlecatege">[$announce_title]* </h5>

                    <div class="form-group formgroup">
                        <input type="text" required="" class="form-control"  name="title"
                               value="<?= $title ?>" placeholder="Titulli i njoftimit">
                    </div>
                </div>
                <div class="col-md-6">
                    <h5 class="titlecatege">[$announce_image]</h5>
                    <span class="file-input"><div class="file-preview ">
                            <div class="file-preview-thumbnails">
                                <div class="file-preview-frame">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="file-preview-status text-center text-success"></div>
                            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                        </div>
                    </span>

                    <div class="upload-btn">
                        <input type="file" name="image">
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h5 class="titlecatege">[$announce_description]* </h5>

                    <div class="form-group">
                        <textarea class="form-control" rows="6"  name="description" placeholder=""
                                  required=""><?= $description ?></textarea>
                    </div>
                </div>
            </div>


            <br>
            <div class="row">
                <div class="col-md-6">
                    <h5 class="titlecatege">[$announce_email]*</h5>

                    <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
		?>
                        <div class="input-group form-group">
                            <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                            <input type="text" required="" class="form-control" id="" name="email" value="<?= $_SESSION['user_auth']['email'] ?>" placeholder="username@example.com" readonly="true">
                        </div>
                    <?php
	} else {
		?>
                        <div class="input-group form-group">
                            <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                            <input type="text" required="" class="form-control" id="" name="email" value="<?= $email ?>" placeholder="username@example.com">
                        </div>
                    <?php
	} ?>
                </div>

                <div class="col-md-6">
                    <h5 class="titlecatege">[$announce_phone]*</h5>

                    <div class="input-group form-group">
                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                        <input type="text" required="" class="form-control" id="" name="tel" value="<?= $tel ?>"placeholder="+355690000000">
                    </div>

                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center finish-button">
                    <button type="submit" id="feedbackSubmit" name="save" class="btn btn-md btn-default">[$continue]
                    </button>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="warning" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title">[$warning]</h5>
                        </div>
                        <div class="modal-body alert-warning">
                            <p>[$insert_valid_input]</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">[$close]</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>