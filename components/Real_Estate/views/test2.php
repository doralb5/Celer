<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/test.css'); ?>



<!--****Progress Step Bar****-->

<div class="container">
    <div class="row">
        <section>
            <div class="wizard">
                <div class="wizard-inner">
                    <div class="connecting-line"></div>
                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="active">
                            <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Zgjidhni paketen tuaj">
                                <span class="round-tab">
                                    <i class="fa fa-hand-pointer-o"></i>
                                </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Rregjistroni te dhenat e biznesit tuaj">
                                <span class="round-tab">
                                    <i class="fa fa-briefcase"></i>
                                </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Skip">
                                <span class="round-tab">
                                    <i class="fa fa-info" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Urime!">
                                <span class="round-tab">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>

                <form role="form" id="form" data-toggle="validator">
                    <div class="tab-content alert alert-grey">
                        <div class="tab-pane fade in active" role="tabpanel" id="step1">
                            <div class="myData">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Step 1</h3>
                                        <!--****Pricing Table****-->
                                        <div class="pricing-list">
                                            <!-- PRICING-TABLE CONTAINER -->
                                            <div class="pricing-table group"><div class="row">
                                                    <div class="col-md-12">

                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="block personal fl free">
                                                            <h2 class="title">FALAS</h2>
                                                            <!-- CONTENT -->
                                                            <div class="content">
                                                                <div class="price">
                                                                    <span>0</span>
                                                                    <sub>Leke</sub>
                                                                </div>
                                                                <p class="hint">Perfitoni kete pakete FALAS</p>
                                                            </div>
                                                            <!-- /CONTENT -->
                                                            <!-- FEATURES -->
                                                            <ul class="features">
                                                                <li>Website i dedikuar per biznesin tuaj  me 1GB Mail te personalizuar</li>
                                                                <hr><li>Reklamim ne portal me:
                                                                    <ol class="sub-features">

                                                                        <li>Logon e biznesit tuaj</li>
                                                                        <li>Pershkrimin e biznesit</li>
                                                                        <li>Imazhe HD (10 imazhe)</li>
                                                                        <li>Oferta (5 produkte)</li>
                                                                        <li>Rrjetet Sociale</li>
                                                                        <li>Pozicionimin ne harte</li>
                                                                    </ol>
                                                                </li>
                                                                <hr>
                                                                <li>Sistem Vleresimi me pershtypje nga klientet dhe modul kontakti online</li>
                                                            </ul>
                                                            <!-- /FEATURES -->
                                                            <!-- PT-FOOTER -->
                                                            <div class="pt-footer">
                                                                <a href="">Vazhdo</a>
                                                            </div>
                                                            <!-- /PT-FOOTER -->
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="block professional fl premium">
                                                            <div class="ribbon-wrapper-green ribbon-mobile">
                                                                <div class="ribbon-green">PREMIUM</div>
                                                            </div>
                                                            <h2 class="title">PREMIUM</h2>
                                                            <!-- CONTENT -->
                                                            <div class="content">
                                                                <div class="row">


                                                                    <div class="col-md-4">
                                                                        <div class="price">
                                                                            <h4 class="month">3 Muaj</h4>
                                                                            <span>8900</span>
                                                                            <sub>Leke</sub>

                                                                        </div>
                                                                        <p class="hint">Pakete e vlefshme per 3 muaj</p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="price">
                                                                            <h4 class="month">6 Muaj</h4>
                                                                            <span>14900</span>
                                                                            <sub>Leke</sub>

                                                                        </div>
                                                                        <p class="hint">Pakete e vlefshme per 6 muaj</p>
                                                                    </div><div class="col-md-4">
                                                                        <div class="price">
                                                                            <h4 class="month">12 Muaj</h4>
                                                                            <span>24900</span>
                                                                            <sub>Leke</sub>

                                                                        </div>
                                                                        <p class="hint">Pakete e vlefshme per 12 muaj</p>
                                                                    </div></div>
                                                            </div>
                                                            <!-- /CONTENT -->
                                                            <!-- FEATURES -->
                                                            <ul class="features">

                                                                <li>Ne pakten PREMIUM pervec paketes FALAS do te perfitoni edhe:<ol class="sub-features">

                                                                        <li>Logon e biznesit ne harte</li>
                                                                        <li>Video (5 video max)</li>
                                                                        <li>Imazhe HD (pa limit)</li>
                                                                        <li>Adezivi PREMIUM per vitrinen tuaj</li>
                                                                        <li>Renditje kryesore ne listim</li>
                                                                        <li>Vendosje e yjeve ne liste</li>
                                                                        <li>Shfaqja e Websitit</li><li>Reklamimi ne faqen kryesore</li><li>Mosshfaqja e bizneseve konkurente</li><li>Statistikat e biznesit tuaj</li><li>Shperndarje Automatike ne Rrjetet Sociale</li><li>Website pa reklama (ads)</li><li>Sistem vleresimi me kredite</li><li>Chat online me klientet</li></ol>
                                                                </li>


                                                            </ul>
                                                            <!-- /FEATURES -->
                                                            <!-- PT-FOOTER -->
                                                            <div class="pt-footer">
                                                                <a href="">Vazhdo></a>
                                                            </div>
                                                            <!-- /PT-FOOTER -->
                                                        </div>
                                                    </div>
                                                </div>




                                            </div>
                                            <!-- /PRICING-TABLE -->
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 pull-right">
                                        <button type="button" class="btn btn-default next-step">Vazhdo</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="step2">
                            <div class="container-fluid myData">
                                <div class="row col-md-12">
                                    <div class="col-md-8">

                                        <h3>Step 2</h3>
                                    </div>
                                </div>

                                <div class="row col-md-12">
                                    <ul class="list-inline pull-right">
                                        <li>
                                            <button type="button" class="btn btn-default prev-step">Kthehu</button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn btn-default next-step">Vazhdo</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="step3">
                            <div class="container-fluid myData">
                                <div class="row col-md-12">
                                    <div class="col-md-8">
                                        <h3>Step 3</h3>
                                    </div>
                                </div>



                                <div class="row col-md-12">
                                    <ul class="list-inline pull-right">
                                        <li>
                                            <button type="button" class="btn btn-default prev-step">Kthehu</button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn btn-default next-step">Tejkalo</button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn btn-default next-step">Vazhdo</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="complete">
                            <div class="container-fluid myData">
                                <div class="row col-md-12">
                                    <div class="center-block text-center">
                                        <h2><span style="color:#f48260;" class="glyphicon glyphicon-heart"></span></h2>
                                        <input type="submit" class="btn btn-success" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </section>
    </div>
</div>


<script>
            $(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

    var $target = $(e.target);
    if ($target.parent().hasClass('disabled')) {
    return false;
    }
    })

            $(".next-step").click(function (e) {

    var $active = $('.wizard .nav-tabs li.active');
    $active.next().removeClass('disabled');
    nextTab($active);
    });
    $(".prev-step").click(function (e) {

    var $active = $('.wizard .nav-tabs li.active');
    prevTab($active);
    });
    $(".next-step").click(function (e) {
    $('#form').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
    // handle the invalid form...
    alert("Failed");
    } else {
    // everything looks good!
    alert("Successful!");
    }
    });
    });
    //End of Doc Ready
    });
    function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
    }
</script>