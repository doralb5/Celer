<?php

require_once LIBS_PATH . 'Email.php';

class Reservation_Component extends BaseComponent
{
	public function preload_reservation()
	{
		//BreadCrumb
		WebPage::$breadcrumb->addDir($this->view->getTerm('Reservation'), Utils::getComponentUrl('Reservation/reservation'));
	}

	public function reservation()
	{
		if (isset($_POST['send'])) {
			($_POST['name'] == '') ? $this->view->AddError('Name is required') : '';
			(strlen($_POST['name']) > 60) ? $this->view->AddError('Name is too long') : '';
			($_POST['email'] == '') ? $this->view->AddError('Email is required') : '';
			(strlen($_POST['email']) > 60) ? $this->view->AddError('Email is too long') : '';
			($_POST['message'] == '') ? $this->view->AddError('Message is required') : '';
			($_POST['reservation_nr'] == '') ? $this->view->AddError('Numbers of persons are required') : '';
			($_POST['reservation_date'] == '') ? $this->view->AddError('Date is required') : '';
			($_POST['phone'] == '') ? $this->view->AddError('Phone is required') : '';

			if (count($this->view->getErrors()) == 0) {
				$sent = $this->sendMail($_POST);
				if ($sent) {
					$this->view->AddNotice('[$msg_sent]');
					Utils::RedirectTo($this->getActionUrl('reservation'));
					return;
				}
				$this->view->AddNotice('[$msg_failed]');
				Utils::RedirectTo($this->getActionUrl('reservation'));
				return;
			}
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		//Title tag
		HeadHTML::setTitleTag($this->view->getTerm('Reservation') . ' | ' . CMSSettings::$website_title);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-reservation';
		$this->view->render($view);
	}

	public function sendMail($post)
	{
		$message = new BaseView();
		$message->setViewPath(COMPONENTS_PATH . 'Reservation' . DS . 'views' . DS);
		$message->setLangPath(COMPONENTS_PATH . 'Reservation' . DS . 'lang' . DS);
		$message->setLang($this->getLang());
		$message->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$message->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$message->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
		$message->renderTemplate(false);
		$message->set('post', $post);

		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		$txt = $message->render('mails/email_format', true);
		if (CMSSettings::$webdomain == 'bostonlobster.al') {
			$sended = Email::sendMail($this->ComponentSettings['email'], $_POST['name'], $_POST['email'], $this->view->getTerm('NewMessageFrom') . CMSSettings::$webdomain, $txt);
			if ($sended) {
				return true;
			}
			return false;
		}
		$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, $_POST['name'], $_POST['email'], $this->view->getTerm('NewMessageFrom') . CMSSettings::$webdomain, $txt);
		if ($sended) {
			return true;
		}
		return false;
	}
}
