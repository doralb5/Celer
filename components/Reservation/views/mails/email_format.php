<title>{SITENAME}</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<style>

    .body-info{
        padding: 5%;
    }
</style>
<!-- begin page body -->
<table class="body-main" style="background: white;">
    <tr>
        <td>
    <center>
        <!-- begin six columns -->
        <table>
            <tr>
                <td> <a href="http://{URL_WEBSITE}"><img src="{URL_LOGO}"/></a> </td>
            </tr>
        </table>
        <!-- end six columns -->
    </center>

    <table>
        <tr>
            <td> [$date]: </td>
            <td> <?= date('Y/m/d') ?> </td>
        </tr>

        <tr>
            <td> [$hour]: </td>
            <td> <?= date('H:i') ?> </td>
        </tr>

        <tr>
            <td> [$name_placehoder]: </td>
            <td>  <?= $post['name'] ?> </td>
        </tr>

        <tr>
            <td> [$email_placehoder]: </td>
            <td> <?= $post['email'] ?> </td>
        </tr>
        <tr>
            <td> [$phone_placehoder]: </td>
            <td> <?= $post['phone'] ?> </td>
        </tr>

        <?php if (isset($post['reservation_nr'])) {
	?>
            <tr>
                <td> [$number]: </td>
                <td> <?= $post['reservation_nr'] ?> </td>
            </tr>
        <?php
} ?>

        <?php if (isset($post['reservation_date'])) {
		?>
            <tr>
                <td> [$reservation_date]: </td>
                <td> <?= $post['reservation_date'] ?> </td>
            </tr>
        <?php
	} ?>

        <tr>
            <td> [$message_placehoder]: </td>
            <td> <?= $post['message'] ?> </td>
        </tr>

    </table>

</table>




