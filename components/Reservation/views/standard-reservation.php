<?php
$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';
$phone = isset($_POST['phone']) ? $_POST['phone'] : '';
?>

<?php HeadHTML::linkJS('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment.js'); ?>

<?php HeadHtml::linkStylesheet('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css'); ?>
<?php HeadHTML::linkJS('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js'); ?>


<style>

    .counter {
        font-family: 'Open Sans', sans-serif;
        font-size: 60px;
        font-weight: 600;
        color: #5e606a;
        /* margin: 0 200px; */
        width: 100%;
    }
    .minus,
    .plus {
        font-size: 40px;
        padding-top: 20px;
        cursor: pointer;
    }
    .minus {
        float: left;
    }
    .plus {
        float: right;
    }
    h2.reservation-number {
    color: #5e606a;
    text-align: left;
    font-size: 24px;
}
.bootstrap-datetimepicker-widget table td.active, .bootstrap-datetimepicker-widget table td.active:hover {
    background-color: #db4437 !important;
}
.bootstrap-datetimepicker-widget .picker-switch td span {
    font-size: 2em !important;
    color: #db4437 !important;
}

</style>



<div class="reservation-page alert-grey">
    <div class="row">

        <div class="col-md-12 contact-form">

            <div class="">
                <div class="">	
                    <h3 class="lobster-style">[$reservation]</h3>
                </div>
                <div class="">
                    <form role="form" id="feedbackForm" method="post">

                        <?php $this->ComponentErrors->renderHTML(); ?>
                        <?php $this->ComponentMessages->renderHTML(); ?>

                        <?php if (isset($success)) {
	?>
                            <div class="alert alert-success" role="alert">
                                <i class="fa fa-check"></i>
                                <?= $success ?>
                            </div>
                        <?php
} ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h2 class="reservation-number">Emri juaj:</h2>
                                    <input type="text" class="form-control form-bg-color" id="name" name="name" placeholder="[$name_placehoder]" value="<?= $name ?>" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <h2 class="reservation-number">Emaili juaj:</h2>
                                    <input type="email" class="form-control form-bg-color" id="email" name="email" placeholder="[$email_placehoder]" value="<?= $email ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <h2 class="reservation-number">Nr.telefonit:</h2>
                                    <input type="text" class="form-control" name="phone" id="phone"
                                           placeholder="Nr.tel" value="<?= $phone ?>" required="">
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <h2 class="reservation-number">Sa persona:</h2>
                                    <input type="number" class="form-control" name="reservation_nr" placeholder="Sa Persona?">
<!--                                    <p class="counter"><span class="minus" onclick="minNumber()">-</span><span id="number">2</span><span class="plus" onclick="addNumber()">+</span></p>-->
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <h2 class="reservation-number">Data e rezervimit:</h2>
                                    <input type="text" class="form-control" name="reservation_date" id="datetimepicker1"
                                           placeholder="Rezervoni Daten" value="<?= $publish_date ?>" required="">
                                </div>
                            </div>
                            
                        </div>
                </div>

                <div class="form-group">
                    <h2 class="reservation-number">Lini nje mesazh:</h2>
                    <textarea style="max-width:100%" rows="10" cols="100" class="form-control form-bg-color" id="message" name="message" placeholder="[$message_placehoder]" required><?= $message ?></textarea>
                </div>

                <div class="text-center">
                    <button type="submit" id="feedbackSubmit" name="send" class="btn btn-default send-button">[$send_request]</button>
                </div>  

                </form>
            </div>
        </div>
    </div>
</div>

</div>
</div>


<script>
    var counter = document.getElementById('number'), num = counter.innerHTML;

    function addNumber() {
        num++
        counter.innerHTML = num;
    }
    ;

    function  minNumber() {
        num--
        if (num >= 1) {
            counter.innerHTML = num;
        } else {
            alert("Numri i rezervimit nuk mund të jetë më pak se 1");
        }
    }
    ;
</script>
<script>
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>
