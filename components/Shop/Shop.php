<?php

require_once LIBS_PATH . 'Email.php';
require_once LIBS_PATH . 'StringUtils.php';

class Shop_Component extends BaseComponent
{
	public static $currProduct;
	public static $currProductObject;

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->view->set('url_products_list', $this->getActionUrl('products_list'));
		$this->view->set('url_request_info', $this->getActionUrl('ajax_requestinfo'));
	}

	public function ajax_requestinfo()
	{
		$ajax = new AjaxResponse();
		$errors = array();
		if (isset($_POST['product_id'])) {
			$prod = $this->model->getProduct($_POST['product_id']);
			($_POST['name'] == '') ? array_push($errors, $this->view->getTerm('Name_required')) : '';
			($_POST['email'] == '') ? array_push($errors, $this->view->getTerm('Email_required')) : '';
			($_POST['phone'] == '') ? array_push($errors, $this->view->getTerm('Phone_required')) : '';
			($_POST['message'] == '') ? array_push($errors, $this->view->getTerm('Message_required')) : '';
		} else {
			array_push($errors, 'Product not defined!');
		}

		if (count($errors)) {
			$ajax->setErrors($errors);
			$ajax->setData('0');
		} else {
			$message_text = $this->view->getTerm('Request_info_for') . ' ' . $prod->name . '</br> ' . $this->view->getTerm('From') . ' ' . $_POST['name'] . "(Phone: {$_POST['phone']})";
			$message_text .= '</br> ' . $this->view->getTerm('Message') . ' : <strong>' . $_POST['message'] . ' </strong>';
			$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, $_POST['name'], $_POST['email'], $this->view->getTerm('New_message_from') . ' ' . $_POST['name'], $message_text);
			if ($sended == 1) {
				$ajax->setMessage($this->view->getTerm('Message_sent'));
				$ajax->setData('1');
			} else {
				$ajax->setMessage($this->view->getTerm('Message_not_sent'));
				$ajax->setData('0');
			}
		}

		$ajax->printResponse();
	}

	public function send_email_request()
	{
		if (isset($_POST['send'])) {
			($_POST['name'] == '') ? $this->AddError('Name is required') : '';
			($_POST['lastname'] == '') ? $this->AddError('Lastname is required') : '';
			($_POST['email'] == '') ? $this->AddError('Email is required') : '';
			($_POST['tel'] == '') ? $this->AddError('Telephone number is required') : '';

			$prod = $this->model->getProduct($_POST['productid']);

			if (count($this->view->getErrors()) == 0) {
				$message_text = $this->view->getTerm('Request_info_about') . $prod->name . '</br> Da ' . $_POST['name'];
				$message_text .= "</br> .$this->view->getTerm('Client_posted_this_message'). : <strong>" . $_POST['message'] . ' </strong>';
				$sended = Email::sendMail(CMSSettings::$EMAIL_ADMIN, $_POST['name'], $_POST['email'], $this->view->getTerm('New_message_from') . $_POST['email'], $message_text);
				if ($sended == 1) {
					//print_r($sended); echo "Runs"; exit;
					//$this->AddNotice("Il messaggio e' stato inviato correttamente.");
					$_SESSION['email-sent'] = $this->view->getTerm('Message_sent_successfully');
				} else {
					// $this->AddError("Errore durante l'invio del messaggio!");
					$_SESSION['email-not-sent'] = $this->view->getTerm('Error_during_message_sent');
				}
			}
		}

		Utils::RedirectTo($this->getActionUrl('show_product/' . $prod->id));
	}

	public function preload_show_product($id = null)
	{
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		} elseif (is_null($id)) {
			Utils::RedirectTo(Utils::getComponentUrl('Webpage/render_error/404'));
		}

		$Product_Table = TABLE_PREFIX . 'shop_Product';
		$product = $this->model->getProduct($id);
		if (is_null($product)) {
			$this->view->AddError . $this->view->getTerm('Product_not_found');
			Utils::RedirectTo(Utils::getComponentUrl('Shop/products_list'));
			return;
		}

		self::$currProduct = $product->id;
		self::$currProductObject = $product;

		//BreadCrumb
		WebPage::$breadcrumb->addDir($product->category, Utils::getComponentUrl('Products/products_list/' . $product->id_category));
		WebPage::$breadcrumb->addDir(StringUtils::CutString($product->name, 50), Utils::getComponentUrl('Products/show_product/' . Utils::url_slug($product->name) . '-' . $id));

		$result['product'] = $product;
		return $result;
	}

	public function show_product($id = null)
	{
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		} elseif (is_null($id)) {
			Utils::RedirectTo(Utils::getComponentUrl('Webpage/render_error/404'));
		}

		$product = $this->model->getProduct($id);

		$ProductImage_Table = TABLE_PREFIX . 'shop_ProductImage';
		$product->images = $this->model->getProductImages(30, 0, "{$ProductImage_Table}.id_product = {$product->id} ");
		$features = $this->model->getFeatures(50, 0, "id_type = {$product->id_type}");
		foreach ($features as $feat) {
			$prodFeat = $this->model->getProdFeatures(20, 0, "id_product = $id AND id_feature = {$feat->id}");
			if (count($prodFeat)) {
				$feat->value = $prodFeat[0]->value;
			} else {
				$feat->value = null;
			}
		}
		$this->view->set('features', $features);

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$parameters['show_share_buttons'] = (isset($parameters['show_share_buttons'])) ? $parameters['show_share_buttons'] : 1;
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'products');

		//Title tag
		HeadHTML::setTitleTag(StringUtils::CutString($product->name, 50) . ' | ' . CMSSettings::$website_title);

		$category = $this->model->getCategory($product->id_category);
		$this->view->set('category', $category);
		//$this->view->set('similar_products', $similar_products);
		$this->view->set('product', $product);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-product-show';
		$this->view->render($view);
	}

	public function products_list($id_category = null)
	{

//       $all = get_defined_vars();
		//       print_r ($all);

		if (!is_null($id_category) && !is_numeric($id_category)) {
			$id_category = substr($id_category, strripos($id_category, '-') + 1);
		}

		$elements_per_page = 6;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$offset = ($page - 1) * $elements_per_page;

		$Product_Table = TABLE_PREFIX . 'shop_Product';
		$ProdFeat_Table = TABLE_PREFIX . 'shop_ProductFeature';
		$Brand_Table = TABLE_PREFIX . 'shop_Brand';
		$ProductCategory_Table = TABLE_PREFIX . 'shop_Category';

		$sorting = 'publish_date desc';
		$filter = "{$Product_Table}.enabled = '1' ";

		if (!is_null($id_category)) {
			$filter .= "AND {$Product_Table}.id_category = {$id_category} ";
		}

		$id_brand = isset($_GET['id_brand']) ? $_GET['id_brand'] : '';
		$id_model = isset($_GET['id_model']) ? $_GET['id_model'] : '';
		$status = isset($_GET['status']) ? $_GET['status'] : '';

		if ($id_brand != '') {
			$filter .= "AND {$Brand_Table}.id = {$id_brand} ";
		}
		if ($id_model != '') {
			$filter .= "AND {$Product_Table}.id_model = {$id_model} ";
		}
		if ($status != '') {
			$filter .= "AND {$Product_Table}.status = '{$status}' ";
		}

		$i = 0;
		$features = $this->model->getFeatures();
		$ft_filter = '';
		foreach ($features as $ft) {
			if (isset($_GET['ft_' . $ft->feature]) && $_GET['ft_' . $ft->feature] != '') {
				$value = $_GET['ft_' . $ft->feature];

				if ($i != 0) {
					$ft_filter .= 'OR';
				}

				$ft_filter .= " ($ProdFeat_Table.id_feature = {$ft->id} AND $ProdFeat_Table.value='$value') ";
				$i++;
			}
		}

		if ($ft_filter != '') {
			$filter .= "AND ( $ft_filter )";
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();

		$this->view->set('parameters', $parameters);

		$this->view->set('settings', $this->ComponentSettings);

		$categories = $this->model->getCategories(100, 0, '', ' category DESC');
		$this->view->set('categories', $categories);

		if (isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$list_title = $this->view->getTerm('Results_for') . ' ' . $_REQUEST['query'];
			$searchFields = array(
				array('field' => "{$Product_Table}.name", 'peso' => 100),
				array('field' => "{$ProductCategory_Table}.category", 'peso' => 40),
			);
			$products = $this->model->search($_REQUEST['query'], $searchFields, $filter, $sorting, $elements_per_page, $offset);
		} else {
			$products = $this->model->getList($elements_per_page, $offset, $filter, $sorting);
		}

		$totalElements = $this->model->getLastCounter();

		if ($totalElements == 0) {
			$noitem = 0;
			$this->view->set('noitem', $noitem);
		}

		$this->view->set('page', $page);
		$this->view->set('totalElements', $totalElements);
		$this->view->set('elements_per_page', $elements_per_page);
		$this->view->set('products', $products);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . 'products' . ((!is_null($id_category)) ? "/$id_category" : ''));

		$category_cover = '';
		//BreadCrumb and CategoryCover
		if (!is_null($id_category)) {
			$category = $this->model->getCategory($id_category);
			$this->view->set('category', $category);
			if ($category->image != '') {
				$category_cover = WEBROOT . DATA_DIR . 'media/products/categories/' . $category->image;
			}
			$list_title = $category->category;

			WebPage::$breadcrumb->addDir($category->category, Utils::getComponentUrl('Products/products_list/' . $id_category));
			HeadHTML::setTitleTag($category->category . ' | ' . CMSSettings::$website_title);
		} else {
			WebPage::$breadcrumb->addDir('[$Products]', Utils::getComponentUrl('Products/products_list'));
			//            HeadHTML::setTitleTag('Products' . ' | ' . CMSSettings::$website_title);
		}

		if (!isset($list_title) || $list_title == '') {
			$list_title = $this->view->getTerm('Product_list');
		}
		$this->view->set('list_title', $list_title);
		$this->view->set('category_cover', $category_cover);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-products-list';
		$this->view->render($view);
	}

	public function categories_list()
	{
	}

	public function ajx_getModels($id_brand)
	{
		$models = $this->model->getModels(50, 0, "id_brand = $id_brand");

		$options = array();
		foreach ($models as $model) {
			array_push($options, array('text' => $model->model, 'value' => $model->id));
		}
		echo json_encode(array('options' => $options));
	}
}
