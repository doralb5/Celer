<?php HeadHTML::AddStylesheet(WEBROOT . DATA_DIR . $this->view_path . 'css/icon-font.css'); ?>
<?php
HeadHTML::AddStylesheet(WEBROOT . DATA_DIR . $this->view_path . 'css/shop-style.css');

require_once LIBS_PATH . 'Pager.php';
?>


<style>

    @media (min-width: 767px) {
        .lastcompany .thumbnail {
        }
    }

    .lastcompany .thumbnail {
        padding: 0px;
        margin-bottom: 0px;
        border: 0px solid white;
        border-radius: 0px;
    }

    .lastcompany #companyCarousel {
        margin-right: 0px;
    }

    ul.thumbnails li {
        margin-bottom: 0px;
    }

    @media (max-width: 768px) {
        .lastcompany ul.thumbnails {
            padding-left: 0px;
        }

        .lastcompany #companyCarousel {
            margin-right: 0px;
        }

        .lastcompany .well {
            padding: 0px;
        }
    }

    .carousel-control {
        background: none !important;
        color: #CACACA;
        font-size: 2.3em;
        text-shadow: none;
        margin-top: 0px;
        width: 5%;
    }

    .carousel-control.left i {
        margin-left: -65px;
    }

    .carousel-control.right i {
        margin-right: -65px;
    }

    @media (max-width: 768px) {
        .carousel-control.left i, .carousel-control.right i {
            display: none;
        }
    }

    ul.pager {
        display: none;
    }

    ul {
        list-style-type: none;
        -webkit-padding-start: 0px;
    }

    .button_align {
        text-align: center;
    }

    .fff {
        margin: 30px 0;
    }

    .product-list {
        box-shadow: 0 0px 1px 0px rgba(158, 36, 48, 0.85);
        overflow: hidden;
        display: block;
        position: relative;
        z-index: 2;
        margin: 8px;
        border: 1px solid #ab888c;
        border-radius: 2px;
    }
</style>


<!--NEW-->

<?php $w = (isset($parameters['w'])) ? $parameters['w'] : 600; ?>
<?php $h = (isset($parameters['h'])) ? $parameters['h'] : 400; ?>
<?php $cols = (isset($parameters['cols'])) ? $parameters['cols'] : 4; ?>
<?php $label_product = (isset($parameters['label_product'])) ? $parameters['label_product'] : '[$Product_list]'; ?>

<div class="row">
    <div class="col-md-12">
        <div class="category-list">
            <h4 style="product-list-title" ><?= $label_product?></h4>
        </div> 
    </div>
</div>
   
 
<div class="last-products">
    <div class="row">
        <ul>
            <?php if (isset($noitem)) {
	?>
                <div class="col-md-12">
                    <div class="alert alert-danger">[$no_item]</div>
                </div>
            <?php
} ?>

            <?php
			foreach ($products as $pro) {
				?>
                <li class="col-md-12 ">
                    <div class="row">
                        <div class="product-list">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <a href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>"title="">                                                                                                      <?if ($pro->image != '') {
					?>
                                    <img src="<?= Utils::genThumbnailUrl('products/' . $pro->image, $w, $h, array('zc' => '1')) ?>" alt="" width="100%">
                                </a>
                            <?
				} else {
					?>
                                <img src="<?= Utils::genThumbnailUrl('products/' . 'no-thumb.png', $w, $h, array('zc' => '1')) ?>" alt="" width="100%">
                            <?
				} ?>

                            </div>

                            <div class="col-md-5 col-sm-12 col-xs-12">
                                <div class="main-title1">
                                    <a href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>" title="">
                                        <b>   <?= $pro->name ?> </b>
                                    </a>

                                    <br>
                                    <br>
                                    <?php
//                                    for ($j = 0; $j < count($pro->id_category); $j++) {
//                                        if ($j > 0) {
//                                            break;
//                                        }
										?>

                                        <a href="<?= Utils::getComponentUrl('Shop/show_product/' . '' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>">
                                            <?php
											if (!is_null($pro->category) && $pro->category != '') {
												echo $pro->category;
											} ?>
                                        </a>

                                    <?php // }?>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12"> 
                                <br>
                                <div class="dettagli-btn">
                                    <a class="btn btn-sm btn-danger btn-readall" href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>">
                                        Maggiori dettagli</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php
			} ?>
        </ul>
    </div>
</div>


<!--/* --------------- Pagination ---------------*/-->
<div class="col-md-12 pagination">
    <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
</div>

