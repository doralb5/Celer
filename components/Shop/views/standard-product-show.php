
<?php $w = (isset($parameters['w'])) ? $parameters['w'] : 800; ?>
<?php $h = (isset($parameters['h'])) ? $parameters['h'] : 500; ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/product-details.css'); ?>
<?php HeadHTML::addMetaProperty('og:title', $product->name); ?>
<?php HeadHTML::addMetaProperty('og:type', 'product'); ?>
<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('products/' . $product->image, $w, $h)); ?>
<?php HeadHTML::addMetaProperty('og:url', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<?php HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($product->description), 200)); ?>
<?php $this->ComponentMessages->renderHTML() ?>
<?php $this->ComponentErrors->renderHTML() ?>

<div class="product-details">
    <div class="row">
        <div class="col-md-12">
            <?php
			if (isset($_SESSION['email-sent'])) {
				print "<div class=\"alert alert-info\"> {$_SESSION['email-sent']} </div>";
			} elseif (isset($_SESSION['email-not-sent'])) {
				print "<div class=\"alert alert-danger\"> {$_SESSION['email-not-sent'] }</div>";
			}
				
			unset($_SESSION['email-sent']);
			unset($_SESSION['email-not-sent']);
			?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-xs-12">
            <!--Main Image-->
            <div class="product-image">
                <?php if ($product->image != '') {
				?>
                    <a class="block-item" href="<?= Utils::genThumbnailUrl('products/' . $product->image, $w, $h, array('zc' => 0)) ?>"  data-lightbox="image-1">
                        <img class="img-responsive product-list-thumbnail img-thumbnail" src="<?= Utils::genThumbnailUrl('products/' . $product->image, $w, $h, array('zc' => 0)) ?>" style="width:100%;">
                    </a>
                <?php
			} else {
				?>
                    <img class="img-responsive product-list-thumbnail img-thumbnail" src="<?= WEBROOT . $this->view_path ?>img/defaultImage.png" style="width:100%;">
                <?php
			} ?>
            </div>
            <!--Additionals Image-->
            <div class="additional-photo-thumbnails">
                <?php if (count($product->images) > 0) {
				?>
                    <?php foreach ($product->images as $img) {
					?>
                        <div class="col-md-2 col-xs-6 additional-image">
                            <div class="item">
                                <a class="block-item" href="<?= Utils::genThumbnailUrl('products/additional/' . $img->image, 800, 500, array('far' => '1', 'bg' => 'FFFFFF')) ?>" data-lightbox="image-1">
                                    <img class="lazyOwl" src="<?= Utils::genThumbnailUrl('products/additional/' . $img->image, 205, 205, array('zc' => 1)) ?>" alt="Immagine" width="100%" style="padding: 3px 3px 0px 0px;">
                                </a>
                            </div>
                        </div>
                    <?php
				} ?>
                <?php
			} ?>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <!--Main Info-->
            <div class="product-title">
                <span><?= $product->name ?></span>
            </div>

            <div class="product-details-items xxxlarge-margin-bottom-1">
                <div class="small-margin-bottom">
                    <span><?= $product->category ?></span>
                </div>
                <div class="product-final-prize">
                    <b>
                        <?php
						if ($product->final_price != 0) {
							echo Utils::price_format($product->final_price, 'EUR', true);
						}
						?>
                    </b>
                </div>
            </div>
            <?php if ($parameters['show_share_buttons'] == 1) {
							?>
            <script>
                function PopupCenter(url, title, w, h) {
                    // Fixes dual-screen position                         Most browsers      Firefox
                    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                    var top = ((height / 2) - (h / 2)) + dualScreenTop
                    var newWindow = window.open(url, title, 'toolbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

                    // Puts focus on the newWindow
                    if (window.focus) {
                        newWindow.focus();
                    }
                }
            </script>
            <div class="xxxlarge-margin-bottom-1">
                <ul class="product-social-icons">
                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'FacebookShare',600,250); return false;" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://plus.google.com/share?url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'GooglePlusShare',600,250); return false;" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'LinkedinShare',600,250); return false;"  target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="https://twitter.com/home?status=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'LinkedinShare',600,250); return false;" target="_blank"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <?php
						}?>
            <button class="btn-product-detail" data-toggle="modal" data-target="#product-information-modal">[$RequestInfo]</button>
            <div id="ajax_response"></div>
            <div class="modal fade" id="product-information-modal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">[$Ask_Info_About] <?= $product->name ?></h4>
                        </div>
                        <div class="modal-body">
                            <form id="form-requestinfo" action="" method="POST">
                                <input type="hidden" name="product_id" value="<?=$product->id?>" />
                                <input type="hidden" name="product_name" value="<?=$product->name?>" />
                                <div class="form-group">
                                    <label for="name">[$Name]*:</label>
                                   <input type="text" name="name" class="form-control" id="name">   
                                </div>
                                <div class="form-group">
                                    <label for="email">[$Email]*:</label>
                                    <input type="email" name="email" class="form-control" id="email">  
                                </div>
                                <div class="form-group">
                                    <label for="phone">[$Phone]*:</label>
                                  <input type="text" name="phone" class="form-control" id="phone"> 
                                </div>
                                <div class="form-group">
                                    <label for="message">[$Message]:</label>
                                    <textarea class="form-control" rows="5" id="message" name="message"> </textarea>
                                </div>
                                <div id="ajax_error">
                                    
                                </div>
                                <button type="submit" class="btn btn-default btn-block">[$Submit]</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">[$Close]</button>
                        </div>
                    </div>
                    <script>
                      $(function () {

                        $('#form-requestinfo').on('submit', function (e) {

                          e.preventDefault();
                          $('#ajax_error').html('');
                          $('#ajax_response').html('');
                          $.ajax({
                            type: 'post',
                            url: '<?=$url_request_info?>',
                            data: $('#form-requestinfo').serialize(),
                            success: function (resp) {
                                json = $.parseJSON(resp);
                                console.log(json);
                                if(json.data === '0'){
                                    $('#ajax_error').html('<ul class="list-errors"></ul>');
                                    for (i = 0; i < json.errors.length; i++) { 
                                        error = json.errors[i];
                                        $('.list-errors').append('<li>' + json.errors[i] + '</li>');
                                    }
                                    $('#ajax_error').addClass('alert alert-danger');
                                } else {
                                    $('#ajax_response').html(json.message);
                                    $('#ajax_response').addClass('alert alert-success');
                                    $('#product-information-modal').modal('hide');
                                    $('#form-requestinfo').each(function(){
                                        this.reset();
                                    });
                                }
                            }
                          });

                        });
                    
                        $('#similar-products-details').owlCarousel({
                            loop: true,
                            margin: 15,
                            stagePadding: 50,
                            nav: true,autoplay:true,
                            autoplayTimeout:3000,
                            autoplayHoverPause:true,
                            dots: false,
                            navText: ['<i class="fa fa-long-arrow-left" aria-hidden="true"></i>','<i class="fa fa-long-arrow-right" aria-hidden="true"></i>'],
                            responsive: {
                                0: {
                                    items:1
                                },
                                600: {
                                    items:2
                                },
                                1000: {
                                    items:3
                                }
                            }
                        });
                    });
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="product-description">						 
        <span><?= $product->description ?></span>
    </div>
    
    <?php if (count($features)) {
							?>
        <div class="table-responsive">
            <h4 class="table-title">[$Additional_Informations]</h4>
            <table class="table color">
                <tbody>
                    <?php foreach ($features as $attr) {
								?>
                    <tr>
                        <th scope="row"><?=$attr->feature?></th>
                        <td><?=$attr->value?></td>
                    </tr>
                    <?php
							} ?>
                </tbody>
            </table>
        </div>
    <?php
						}?>

</div>
