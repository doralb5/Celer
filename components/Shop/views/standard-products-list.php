<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/product-list.css'); ?>

<?php $w = (isset($parameters['w'])) ? $parameters['w'] : 250; ?>
<?php $h = (isset($parameters['h'])) ? $parameters['h'] : 250; ?>
<?php $cols = (isset($parameters['cols'])) ? $parameters['cols'] : 2; ?>
<?php $col_size = 12 / $cols; ?>
<?php $list_title = (isset($list_title) && $list_title != '') ? $list_title : '[$Product_list]'; ?>

<script>
    $(document).ready(function () {
        $('#products .item').removeClass('col-lg-6');
        $('#products .item').addClass('col-lg-12');
        $('.thumbnail-image').addClass('col-lg-4');
        $('.caption').addClass('col-lg-8 flex-design-column');
        $('.product-details-container').addClass('flex-design-row');
        $('#list').click(function (event) {
            event.preventDefault();
            $('#products .item').removeClass('col-lg-6');
            $('#products .item').addClass('col-lg-12');
            $('.thumbnail-image').addClass('col-lg-4');
            $('.caption').addClass('col-lg-8 flex-design-column');
            $('.product-details-container').addClass('flex-design-row');
        });
        $('#grid').click(function (event) {
            event.preventDefault();
            $('#products .item').removeClass('col-lg-12');
            $('#products .item').addClass('col-lg-6');
            $('.thumbnail-image').removeClass('col-lg-4');
            $('.caption').removeClass('col-lg-8 flex-design-column');
            $('.product-details-container').removeClass('flex-design-row');
        });
    });
</script>

 <div class="category-list left-padding">
    <?php if ($category_cover != '') {
	?>
        <div class="row">
            <div class="col-md-12">
                <img src="<?=$category_cover?>" class="img-responsive" />
            </div>
        </div>
    <?php
}?>
    <div class="row">
        <div class="col-md-6 title-view">
            <h4 class="product-list-title"><?= $list_title; ?></h4>
        </div>
        <div class="col-md-6 grid-view">
            <div class="btn-group">
                <a id="list" class="btn btn-default btn-md"><span class="glyphicon glyphicon-th-list">
                    </span>[$List]</a> <a id="grid" class="btn btn-default btn-md"><span
                        class="glyphicon glyphicon-th"></span>[$Grid]</a>
            </div>
        </div>
    </div>
</div>
<div class="row category-image" >   
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div>
            <!--      I set this static photo for test      -->
            <img  class="img-responsive" src="">
        </div>
    </div>
</div>
<?php if (count($products) == 0) {
		?>
    <div class="col-md-12">
        <div class="alert alert-danger">[$no_item]</div>
    </div>
<?php
	} ?>


<div id="products" class="row list-group">
    <?php
	$i = 1;
	foreach ($products as $pro) {
		?>
        <div class="item col-md-<?= $col_size ?> col-lg-<?= $col_size ?>">
            <div class="product-details-container">
                <?php if ($pro->image != '') {
			?>
                    <div class="thumbnail-image">
                        <a href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>"> <img class="product-list-thumbnail img-responsive img-thumbnail" src="<?= Utils::genThumbnailUrl('products/' . $pro->image, $w, $h, array()) ?>" alt="" > </a>
                    </div>
                    
                <?php
		} else {
			?>
                    <div class="thumbnail-image">
                        <img class="product-list-thumbnail img-responsive img-thumbnail" src="<?= Utils::genThumbnailUrl('products/' . 'no-thumb.png', $w, $h, array()) ?>" alt="" >
                    </div>
                    </a>
                <?php
		} ?>
                <div class="caption">
                    <h4 class="group inner list-group-item-heading"><a href='<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>'><?= $pro->name ?></a></h4>
                    <p class="group inner list-group-item-text"><?= StringUtils::CutString(strip_tags($pro->description), 100); ?></p>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <?php if ($pro->price != 0) {
			?>
                            <p class="lead"><?= Utils::price_format($pro->price); ?></p>
                            <?php
		} ?>
                        </div>
                        <div class="col-xs-12 col-md-6 right-align">
                            <a class="btn btn-success btn-product-detail" href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>">[$Details]</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
		if ($i % $cols == 0) {
			echo "<div class=\"clearfix\"></div>\n";
		}
		$i++;
	}
	?>
</div>
<?php require_once LIBS_PATH . 'Pager.php'; ?>

<!--/* --------------- Pagination ---------------*/-->
<div class="col-md-12 pagination">
    <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
</div>

