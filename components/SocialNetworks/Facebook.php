<?php

require_once DOCROOT . LIBS_PATH . 'facebook-sdk-v4' . DS . 'src' . DS . 'Facebook' . DS . 'autoload.php';

class Facebook_Component extends BaseComponent
{
	public function facebook_callback()
	{

		//Component settings
		$settings = $this->ComponentSettings;

		$app_id = isset($settings['app_id']) ? $settings['app_id'] : '1726438044257214';
		$app_secret = isset($settings['app_secret']) ? $settings['app_secret'] : '1a0291be854de5f79a2cf865f70d1e5c';

		$fb = new Facebook\Facebook(array(
			'app_id' => "$app_id",
			'app_secret' => "$app_secret",
			'default_graph_version' => 'v2.2',
		));

		$helper = $fb->getRedirectLoginHelper();

		try {
			$accessToken = $helper->getAccessToken();
		} catch (Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch (Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}

		if (!isset($accessToken)) {
			if ($helper->getError()) {
				header('HTTP/1.0 401 Unauthorized');
				echo 'Error: ' . $helper->getError() . "\n";
				echo 'Error Code: ' . $helper->getErrorCode() . "\n";
				echo 'Error Reason: ' . $helper->getErrorReason() . "\n";
				echo 'Error Description: ' . $helper->getErrorDescription() . "\n";
			} else {
				header('HTTP/1.0 400 Bad Request');
				echo 'Bad request';
			}
			exit;
		}

		// Logged in
		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);

		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId("$app_id");
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();

		if (!$accessToken->isLongLived()) {
			// Exchanges a short-lived access token for a long-lived one
			try {
				$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
			} catch (Facebook\Exceptions\FacebookSDKException $e) {
				echo '<p>Error getting long-lived access token: ' . $helper->getMessage() . "</p>\n\n";
				exit;
			}
		}

		$_SESSION['fb_access_token'] = (string) $accessToken;

		// User is logged in with a long-lived access token.
		// You can redirect them to a members-only page.
		//header('Location: https://example.com/members.php');

		$users_md = Loader::getModel('Users');

		$fb->setDefaultAccessToken($accessToken);
		try {
			$response = $fb->get('/me?fields=email,first_name, last_name,picture.type(large)');
			$userNode = $response->getGraphUser();

			$data = $userNode->asArray();

			if (!empty($data)) {

				//Perpunojme te dhenat e perdoruesit
				if ($this->model->existFbId($data['id'])) {
					$user = $this->model->getFbUser($data['id']);
				} elseif ($users_md->existEmail($data['email'])) {
					$user = $users_md->getList(1, 0, "email = '{$data['email']}'");
					if (count($user)) {
						UserAuth::setLoginSession(Utils::ObjectToArray($user['0']));
					}
				} else {
					$user = new User_Entity();
					$user->email = $data['email'];
					$user->firstname = $data['first_name'];
					$user->lastname = $data['last_name'];
					$user->facebook_id = $data['id'];
					$user->enabled = 1;

					//Ruajme imazhin
					$pic = file_get_contents($data['picture']);
					$target = DOCROOT . MEDIA_ROOT . DS . 'users';
					$fileName = time() . '_' . $data['first_name'];
					$file = fopen($target . DS . $fileName, 'w+');
					$result = fputs($file, $pic);
					fclose($file);

					if ($result) {
						$user->image = $fileName;
					}

					$inserted_id = $users_md->saveUser($user);

					if (!is_array($inserted_id)) {
						$user = $users_md->getUser($inserted_id);
						UserAuth::setLoginSession(Utils::ObjectToArray($user));
					}
				}

				Utils::RedirectTo(Utils::getComponentUrl('home'));
			} else {
				throw new Exception('Facebook returns empty resonse, Please contact admin.');
			}
		} catch (Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch (Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
	}

	public function facebook_logout()
	{
		UserAuth::unsetLoginSession();
		Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
	}
}
