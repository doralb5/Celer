<?php

class StaticBlocks_Component extends BaseComponent
{
	public function preload_printBlock($id = null)
	{
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		}

		if (!is_null($id)) {
			$currentLang = Utils::getLang();

			$sb = $this->model->getList(1, 0, "id = $id AND enabled = '1'");

			if (count($sb) == 0) {
				$content = '';
			} else {
				$filter = "id_static_block = {$id} AND lang = '{$currentLang}'";
				$blockContents = $this->model->getStaticBlockContents(1, 0, $filter);

				if (count($blockContents)) {
					$content = $blockContents[0]->content;

				//WebPage::$breadcrumb->addDir($blockContents[0]->title, Utils::getComponentUrl("StaticBlocks/printBlock/{$blockContents[0]->id_static_block}"));
				} else {
					$content = '';
				}
			}
		} else {
			$content = '';
		}
		//$this->view->set('content', $content);
		$this->view->setContent($content);
		$this->view->set('id_static_block', $id);
	}

	public function printBlock($id = null)
	{
		if (!is_numeric($id)) {
			$id = substr($id, strripos($id, '-') + 1);
		}

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-static-block';
		$this->view->render($view);
	}
}
