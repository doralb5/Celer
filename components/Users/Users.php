<?php

require_once DOCROOT . LIBS_PATH . 'Email.php';

class Users_Component extends BaseComponent
{
	private $fb_app_id;
	private $user_logged;
	private $message;

	public function __construct($name = '', $package = '')
	{
		parent::__construct($name, $package);
		$this->user_logged = UserAuth::getLoginSession();
		$this->view->set('url_login', $this->getActionUrl('login'));
		require VIEWS_PATH . 'MailMessage.php';
		$this->message = new MailMessage();
		$this->message->setViewPath(COMPONENTS_PATH . 'Users' . DS . 'views/mails' . DS);
		$this->message->setLangPath(COMPONENTS_PATH . 'Users' . DS . 'lang' . DS);
		$this->message->set('url_login', $this->getActionUrl('login'));
	}

	public function register()
	{
		UserAuth::unsetLoginSession();
		$user_categories = $this->model->getUserCategories();
		$default_category = Utils::in_ObjectArray($user_categories, 'default', '1');
		$this->view->set('user_categories', $user_categories);
		$this->view->set('default_category', $default_category->id);

		if (isset($_POST['register'])) {
			$password_min_length = 6;
			$password_max_length = 20;

			$id_user_category = ($default_category) ? $default_category->id : '';

			if (count($user_categories) > 1) {
				(!isset($_POST['id_category'])) ? $this->AddError('[$must_select_user_category]') : $id_user_category = $_POST['id_category'];
			}

			(!isset($_POST['agree'])) ? $this->AddError('[$must_agree_policy]') : '';
			(!isset($_POST['firstname'])) ? $this->AddError('[$firstname_required]') : '';
			(!isset($_POST['lastname'])) ? $this->AddError('[$lastname_required]') : '';
			if (isset($this->ComponentSettings['can_set_username']) && $this->ComponentSettings['can_set_username'] == 0) {
				$_POST['username'] = strtolower($_POST['email']);
			} else {
				(!isset($_POST['username'])) ? $this->AddError('[$username_required]') : '';
			}
			(!isset($_POST['email'])) ? $this->AddError('[$email_required]') : '';
			($this->model->existEmail($_POST['email'])) ? $this->AddError('[$email_exist]') : '';
			(!isset($_POST['password'])) ? $this->AddError('[$password_required]') : '';
			(strlen($_POST['password']) > $password_max_length || strlen($_POST['password']) < $password_min_length) ? $this->AddError('[$password_length]' . ' ' . $password_min_length . ' [$to] ' . $password_max_length . ' [$characters]') : '';
			($_POST['password'] != $_POST['rpassword']) ? $this->AddError('[$password_dont_match]') : '';
			if ($this->model->existUsername($_POST['username'])) {
				if (isset($this->ComponentSettings['can_set_username']) && $this->ComponentSettings['can_set_username'] == 0) {
					$this->AddError('[$user_exist]');
				} else {
					$this->AddError('[$username_exist]');
				}
			}

			if (count($this->view->getErrors()) == 0) {
				$registered_user = $this->registerNewUser($_POST['username'], $_POST['email'], $_POST['firstname'], $_POST['lastname'], $_POST['password'], $id_user_category);

				if ($registered_user) {
					Utils::RedirectTo($this->getActionUrl('login'));
					return true;
				}
			}
		}

		$parameters = WebPage::getParameters();
		//Webpage Parameters and Component settings
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		// Facebook Login Button
		$fb = $this->FacebookConnect();
		$helper = $fb->getRedirectLoginHelper();
		$loginUrl = $helper->getLoginUrl($this->getActionUrl('loginByFacebook'), array('email'));
		$this->view->set('FBLoginUrl', $loginUrl);
		//

		//BreadCrumb
		WebPage::$breadcrumb->addDir($this->view->getTerm('Register'), $this->getActionUrl('register'));
		//Title tag
		HeadHTML::setTitleTag($this->view->getTerm('Register') . ' - ' . CMSSettings::$website_title);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-register';
		$this->view->render($view);
		$this->view->cleanErrors();
	}

	private function completeRegistration($user)
	{
		if ($user->enabled) {
			return true;
		}

		$categ = $this->model->getUserCategory($user->id_category);
		if ($categ->enabled_on_register) {
			$user->enabled = '1';
			$this->message->set('user', $user);
			if ($this->model->saveUser($user)) {
				$subject = $this->view->getTerm('registration_completed');
				$sended = $this->message->send($user->email, $subject, 'register-successfully');
				//$sended_admin = Email::sendMail(CMSSettings::$EMAIL_ADMIN, $from_name, $from, $subject, $testo_admin);
				$this->AddNotice('[$register_success]');
				return true;
			}
			$this->AddError('Error during User Activation!');
			return false;
		}
		$this->message->set('user', $user);
		$subject = $this->view->getTerm('registration_in_progress');
		$sended = $this->message->send($user->email, $subject, 'wait-for-confirmation');
		//$sended_admin = Email::sendMail(CMSSettings::$EMAIL_ADMIN, $from_name, $from, $subject, $testo_admin);
		$this->AddNotice('[$wait_for_comfirmation]');
		
		return true;
	}

	public function login()
	{
		if ($this->user_logged) {
			Utils::RedirectTo($this->getActionUrl('dashboard'));
			return;
		}

		if (isset($_GET['redir'])) {
			$redir_url = $_GET['redir'];
			$_SESSION['redir_url'] = $redir_url;
		}

		HeadHTML::setTitleTag('Login' . ' - ' . CMSSettings::$webdomain);

		$this->model->setDatabase(Database::getInstance());
		if (isset($_POST['login'])) {
			$user = $this->model->getUserByUsername($_POST['username']);

			if ($user === false) {
				$this->AddError('[$UserNotExist]');
			} else {
				if ($this->checkAuthorization($user)) {
					$logged = UserAuth::checkAuthentication($_POST['username'], $_POST['password'], $arr_user);

					if ($logged) {
						UserAuth::setLoginSession($arr_user);
						//Vendosim last_access
						$user->last_access = date('Y-m-d H:i:s');
						$this->model->saveUser($user);
						$this->LogsManager->registerLog('User', 'login', 'User logged in with id : ' . $user->id, $user->id);

						if (isset($redir_url)) {
							Utils::RedirectTo(urldecode($redir_url));
						} else {
							Utils::RedirectTo($this->getActionUrl('dashboard'));
						}

						return;
					}
					$this->LogsManager->registerLog('User', 'login', 'User tried to log in with username : ' . $_POST['username'] . ' and password : ' . $_POST['password']);
					$this->AddError('[$LoginFailed]');
				}
			}
		}

		// Facebook Login Button
		$fb = $this->FacebookConnect();
		$helper = $fb->getRedirectLoginHelper();
		$loginUrl = $helper->getLoginUrl($this->getActionUrl('loginByFacebook'), array('email'));
		$this->view->set('FBLoginUrl', $loginUrl);
		//

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);
		//BreadCrumb
		WebPage::$breadcrumb->addDir($this->view->getTerm('Login'), $this->getActionUrl('login'));
		//Title tag
		HeadHTML::setTitleTag($this->view->getTerm('Login') . ' - ' . CMSSettings::$website_title);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-login';
		$this->view->render($view);
	}

	private function checkAuthorization($user)
	{
		if (isset($user->id)) {
			if ($user->enabled == 0) {
				$this->AddError('[$UserNotEnabled]');
				return false;
			}
			$categ_selected = $this->model->getUserCategory($user->id_category);
			if ($user->verified == 0 && (!$categ_selected || is_object($categ_selected) && $categ_selected->required_verify == '1')) {
				$user->hash = $this->generateNewUserToken();
				$this->model->saveUser($user);
				$this->sendVerifyMail($user);
				$this->AddError('[$Email_not_verified]');
				return false;
			}
			return true;
		}
		return false;
	}

	private function FacebookConnect()
	{
		require_once DOCROOT . LIBS_PATH . 'facebook-sdk-v4/src/Facebook/autoload.php';
		$appid = CMSSettings::$facebook_api;
		$appsecret = CMSSettings::$facebook_secret;
		$this->fb_app_id = $appid;

		$fb = new Facebook\Facebook(array(
			'app_id' => $appid,
			'app_secret' => $appsecret,
			'default_graph_version' => 'v2.2',
		));

		return $fb;
	}

	public function loginByFacebook()
	{
		if ($this->user_logged) {
			UserAuth::unsetLoginSession();
		}

		if (isset($_POST['complete-profile'])) {
			return $this->completeProfile();
		}

		if (isset($_GET['redir'])) {
			$_SESSION['redir_url'] = $_GET['redir'];
		}

		$fb = $this->FacebookConnect();
		$helper = $fb->getRedirectLoginHelper();
		try {
			$accessToken = $helper->getAccessToken();
		} catch (Facebook\Exceptions\FacebookResponseException $e) {
			$this->AddError('Graph returned an error: ' . $e->getMessage());
			Utils::RedirectTo($this->getActionUrl('login'));
		} catch (Facebook\Exceptions\FacebookSDKException $e) {
			$this->AddError('Facebook SDK returned an error: ' . $e->getMessage());
			Utils::RedirectTo($this->getActionUrl('login'));
		}
		if (!isset($accessToken)) {
			if ($helper->getError()) {
				header('HTTP/1.0 401 Unauthorized');
				echo 'Error: ' . $helper->getError() . "\n";
				echo 'Error Code: ' . $helper->getErrorCode() . "\n";
				echo 'Error Reason: ' . $helper->getErrorReason() . "\n";
				echo 'Error Description: ' . $helper->getErrorDescription() . "\n";
			} else {
				header('HTTP/1.0 400 Bad Request');
				echo 'Bad request';
			}
			exit;
		}
		$oAuth2Client = $fb->getOAuth2Client();
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
		$tokenMetadata->validateAppId($this->fb_app_id);
		$tokenMetadata->validateExpiration();
		if (!$accessToken->isLongLived()) {
			try {
				$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
			} catch (Facebook\Exceptions\FacebookSDKException $e) {
				exit;
			}
		}
		try {
			$response = $fb->get('/me?fields=id,name,email,birthday,first_name,last_name,picture.width(300)', $accessToken->getValue());
		} catch (Facebook\Exceptions\FacebookResponseException $e) {
			$this->AddError('Graph returned an error: ' . $e->getMessage());
			Utils::RedirectTo($this->getActionUrl('login'));
		} catch (Facebook\Exceptions\FacebookSDKException $e) {
			$this->AddError('Facebook SDK returned an error: ' . $e->getMessage());
			Utils::RedirectTo($this->getActionUrl('login'));
		}
		$fbuser = $response->getGraphUser();

		$facebook_id = $fbuser->getId();

		$user = UserAuth::getUserByFb($facebook_id);

		if ($this->checkAuthorization($this->model->getUser($user['id']))) {
			/** Utente registrato ed autorizzato ad accedere * */
			UserAuth::setLoginSession($user);
			$this->goToDashboard();
			return;
		} elseif ($user != false) {
			/** Utente registrato senza autorizzazioni ad accedere * */
			Utils::RedirectTo($this->getActionUrl('Login'));
			return;
		} elseif (!$user && $fbuser->getId() != '') {

			// Utente non registrato

			$userarray = array();
			$userarray['firstname'] = $fbuser->getFirstName();
			$userarray['lastname'] = $fbuser->getLastName();
			$userarray['email'] = $fbuser->getEmail();
			$userarray['facebook_id'] = $fbuser->getId();
			$image = $fbuser->getPicture();
			$userarray['profile_image_url'] = $image->getUrl();

			$user_categories = $this->model->getUserCategories();
			if (count($user_categories) <= 1 && $userarray['email'] != null && !$this->model->existEmail($userarray['email'])) {
				$registered = $this->registerNewFbUser($userarray, true);
				if ($registered) {
					$loginUrl = $helper->getLoginUrl($this->getActionUrl('loginByFacebook'), array('email'));
					Utils::RedirectTo($loginUrl);
					return;
				}
			} else {
				Session::set('fbuser_to_register', $userarray);
				if (!$this->model->existEmail($userarray['email'])) {
					$this->completeProfile();
				} else {
					$this->completeProfile();
				}
			}
		}
	}

	private function completeProfile()
	{
		$userarray = Session::get('fbuser_to_register');
		if ($userarray == null) {
			echo $this->view->getTerm('[$error_occurred]');
			exit;
		}

		if (isset($_POST['complete-profile'])) {
			if ($this->model->existEmail($_POST['email'])) {
				$this->view->AddError('[$email_exist]');
			} else {
				$newuser = $userarray;
				$newuser['email'] = $_POST['email'];
				$newuser['id_category'] = (isset($_POST['id_category'])) ? $_POST['id_category'] : '';
				$verified = ($newuser['email'] == $userarray['email']) ? true : false;
				if ($this->registerNewFbUser($newuser, $verified)) {
					Session::clear('fbuser_to_register');

					if ($registered->enabled == 1) {
						$fb = $this->FacebookConnect();
						$helper = $fb->getRedirectLoginHelper();
						$loginUrl = $helper->getLoginUrl($this->getActionUrl('loginByFacebook'), array('email'));
						Utils::RedirectTo($loginUrl);
						return;
					}
					Utils::RedirectTo($this->getActionUrl('login'));
					return;
				}
			}
		}

		$user = new User_Entity();
		$user->firstname = $userarray['firstname'];
		$user->lastname = $userarray['lastname'];
		$user->email = $userarray['email'];
		$user->id_category = '';
		$user_categories = $this->model->getUserCategories();

		$this->view->set('user', $user);
		$this->view->set('user_categories', $user_categories);

		HeadHTML::setTitleTag($this->view->getTerm('complete_registration') . ' - ' . CMSSettings::$website_title);

		$this->view->render('complete-registration');
	}

	private function registerNewFbUser($userarray, $verified = false)
	{
		return $this->registerNewUser(
						'fb_' . $userarray['facebook_id'], $userarray['email'], $userarray['firstname'], $userarray['lastname'], Utils::GenerateRandomPassword(8), $userarray['id_category'], $this->importFacebookProfileImage($userarray['facebook_id'], $userarray['profile_image_url']), $userarray['facebook_id'], 0, $verified);
	}

	private function registerNewUser($username, $email, $firstname, $lastname, $password, $id_user_category = '', $photo = null, $facebook_id = null, $is_admin = 0, $verified = false)
	{
		$user = new User_Entity();
		$user->enabled = '0';
		$user->id_category = null;

		$mail_verification_required = true;

		$categ_selected = $this->model->getUserCategory($id_user_category);
		if ($categ_selected) {
			$user->id_category = $id_user_category;
			if ($categ_selected->required_verify == '0') {
				$mail_verification_required = false;
			}
		}

		if ($email == '' || $email == null || strlen($email) < 3) {
			$this->AddError($this->view->getTerm('email_required'));
			return false;
		}
		if ($username == '' || $username == null || strlen($username) < 3) {
			$this->AddError($this->view->getTerm('username_required'));
			return false;
		}

		$user->username = $username;
		$user->email = $email;
		$user->firstname = $firstname;
		$user->lastname = $lastname;
		$user->password = md5($password);
		$user->plain_password = $password;
		$user->image = $photo;
		$user->facebook_id = $facebook_id;
		$user->admin = $is_admin;
		$user->hash = $this->generateNewUserToken();
		$user->verified = ($verified) ? '1' : '0';
		$user->lang = FCRequest::getLang();
		$inserted_id = $this->model->saveUser($user);
		$user->id = $inserted_id;
		if (!is_array($inserted_id)) {
			$this->LogsManager->registerLog('User', 'register', "User registered with id : $inserted_id", $inserted_id);
			if ($mail_verification_required && !$verified) {
				$this->sendVerifyMail($user);
				$this->AddNotice('[$email_verification_sent]');
				Utils::RedirectTo($this->getActionUrl('login'));
			} else {
				$this->completeRegistration($user);
			}
			return $user;
		}
		$this->AddError('[$error_occurred]');
		
		return false;
	}

	//    private function registerUserLogedByFacebook($user_data) {
	//        //$user_data is the data returned by facebook open graph
	//        $registred_user = $this->registerNewUser($user_data->getEmail(), $user_data->getEmail(), $user_data->getFirstName(), $user_data->getLastName(), Utils::GenerateRandomPassword(8), $user_data->getId());
	//        $this->afterRegisterSuccess($registred_user);
	//    }

	private function goToDashboard()
	{
		if (isset($this->ComponentSettings['dashboard_url']) && $this->ComponentSettings['dashboard_url'] != '') {
			Utils::RedirectTo($this->ComponentSettings['dashboard_url']);
		} else {
			if (isset($_SESSION['redir_url'])) {
				$redir_url = $_SESSION['redir_url'];
				unset($_SESSION['redir_url']);
				Utils::RedirectTo(urldecode($redir_url));
				return;
			}
			Utils::RedirectTo($this->getActionUrl('edit_account'));
		}
	}

	private function importFacebookProfileImage($fbuserid, $fbimageurl)
	{
		$filename = 'fb_user_' . $fbuserid . '.jpg';
		$target = MEDIA_ROOT . 'users' . DS . $filename;
		if (copy($fbimageurl, $target)) {
			return $filename;
		}
		return false;
	}

	public function logout()
	{
		UserAuth::unsetLoginSession();
		Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
	}

	public function user_profile()
	{
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
		}

		$logged_user = UserAuth::getLoginSession();

		$user = $this->model->getUser($logged_user['id']);
		$this->view->set('user', $user);

		//Webpage Parameters and Component settings
		$parameters = WebPage::getParameters();
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		//BreadCrumb
		WebPage::$breadcrumb->addDir('[$Profile]', Utils::getComponentUrl('Users/user_profile'));

		//Title tag
		HeadHTML::setTitleTag('[$UserProfile]' . ' | ' . CMSSettings::$website_title);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-user-profile';
		$this->view->render($view);
	}

	public function edit_account()
	{
		if (!isset($_SESSION['user_auth'])) {
			Utils::RedirectTo(Utils::getComponentUrl('Users/login') . '?redir=' . $this->getActionUrl('edit_account'));
		}
		if (isset($_SESSION['user_auth']['id'])) {
			$id = $_SESSION['user_auth']['id'];
			if (!isset($_SESSION['user_auth'])) {
				Utils::RedirectTo(Utils::getComponentUrl('Users/login'));
			}
			$logged_user = UserAuth::getLoginSession();

			if (!is_null($id)) {
				$user = $this->model->getUser($id);

				if (is_null($user)) {
					Utils::RedirectTo(Utils::getComponentUrl('Businesses/business_panel'));
				}
			} else {
				$user = $this->model->getUser($id);
				//HeadHTML::setTitleTag('New User' . ' - ' . CMSSettings::$website_title);
			}

			if (isset($_POST['save'])) {
				($_POST['username'] == '') ? $this->view->AddError('[$usename_required]') : '';
				($_POST['email'] == '') ? $this->view->AddError('[$email_required]') : '';
				($_POST['firstname'] == '') ? $this->view->AddError('[$firstname_required]') : '';
				($_POST['lastname'] == '') ? $this->view->AddError('[$lastname_required]') : '';

				if (isset($_FILES['image']['tmp_name']) && strlen($_FILES['image']['tmp_name'])) {
					(!Utils::allowedFileType($_FILES['image']['name'])) ? $this->view->AddError('[$file_must_be_an_image]') : '';
				}

				if (is_null($id) || $_POST['username'] != $user->username) {
					($this->model->existUsername($_POST['username'])) ? $this->view->AddError('[$username_exist]') : '';
				}
				if (is_null($id) || $_POST['email'] != $user->email) {
					($this->model->existEmail($_POST['email'])) ? $this->view->AddError('[$email_exist]') : '';
				}

				if (count($this->view->getErrors()) == 0) {
					if (strlen($_FILES['image']['tmp_name'])) {
						$_POST['image'] = time() . '_' . $_FILES['image']['name'];
						$old_image = $user->image;
					} else {
						$_POST['image'] = $user->image;
					}
					$user->username = $_POST['username'];
					$old_email = $user->email;
					$user->email = $_POST['email'];
					if ($old_email != $_POST['email']) {
						$user->verified = '0';
					}
					$user->hash = $this->generateNewUserToken();
					if (is_null($user->id)) {
						$new_password = Utils::GenerateRandomPassword(8);
						$user->password = md5($new_password);
					}
					$user->firstname = $_POST['firstname'];
					$user->lastname = $_POST['lastname'];
					$user->image = $_POST['image'];
					$user->admin = 1;
					$inserted_id = $this->model->saveUser($user);
					if (!is_array($inserted_id)) {
						$this->view->AddNotice('[$modifications_saved]');
						if (strlen($_FILES['image']['tmp_name'])) {
							$target = DOCROOT . MEDIA_ROOT . DS . 'users';
							Utils::createDirectory($target);
							$filename = $target . DS . $_POST['image'];
							move_uploaded_file($_FILES['image']['tmp_name'], $filename);
							if (!is_null($id)) {
								unlink($target . DS . $old_image);
							}
						}
						$_SESSION['user_auth']['firstname'] = $_POST['firstname'];
						$_SESSION['user_auth']['lastname'] = $_POST['lastname'];
						$_SESSION['user_auth']['email'] = $_POST['email'];
						$_SESSION['user_auth']['username'] = $_POST['username'];
						$_SESSION['user_auth']['image'] = $_POST['image'];
						if (is_null($id)) {
							Email::sendMail($user->email, CMSSettings::$website_title, CMSSettings::$sender_mail, '[$your_new_password_is]' . CMSSettings::$website_title, $user->password);
						}

						if (!is_bool($inserted_id)) {
							$this->LogsManager->registerLog('User', 'insert', 'User inserted with id : ' . $inserted_id, $inserted_id);
							Utils::RedirectTo(Utils::getComponentUrl('Users/edit_account'));
						} else {
							if ($old_email != $_POST['email']) {
								$this->view->AddNotice('[$email_verification_sent]');
								$this->sendVerifyMail($user);
							}

							$this->LogsManager->registerLog('User', 'update', 'User updated with id : ' . $id, $id);
							Utils::RedirectTo(Utils::getComponentUrl('Users/edit_account'));
						}
					} else {
						$this->view->AddError('[$modifications_saving_failure]');
					}
				}
			} elseif (isset($_POST['save_password'])) {
				$password_min_length = 6;
				$password_max_length = 20;
				($_POST['password'] != $_POST['rpassword']) ? $this->view->AddError('[$password_dont_match]') : '';
				(strlen($_POST['password']) > $password_max_length || strlen($_POST['password']) < $password_min_length) ? $this->view->AddError('[$password_length]' . ' ' . $password_min_length . ' [$to] ' . $password_max_length . ' [$characters]') : '';
				if (count($this->view->getErrors()) === 0) {
					$user->plain_password = $_POST['password'];
					$user->password = md5($_POST['password']);
					//I Dergojme email clientit me passwordin e resetuar.
					Email::sendMail($user->email, CMSSettings::$website_title, CMSSettings::$sender_mail, '[$your_new_password_is] ' . CMSSettings::$website_title, 'Password: ' . $user->password);
					$res = $this->model->saveUser($user);

					if ($res) {
						$this->view->AddNotice('[$password_saved_success]');
						$this->LogsManager->registerLog('User', 'update', 'Password changed for client with id : ' . $user->id, $user->id);
					} else {
						$this->view->AddError('[$modifications_saving_failure]');
					}
					Utils::RedirectTo(Utils::getComponentUrl('Users/edit_account'));
				}
			}
		}
		$this->view->set('user', $user);
		$this->view->render('edit-profile');
	}

	public function messages_list()
	{
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'messages-list';
		$this->view->render($view);
	}

	public function reviews_list()
	{
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'reviews-list';
		$this->view->render($view);
	}

	private function sendVerifyMail($user)
	{
		$subject = $this->view->getTerm('Verify_email');
		$this->message->set('user', $user);
		$sended = $this->message->send($user->email, $subject, 'send-verify-mail');
		return $sended;
	}

	private function sendMailAfterVerify($email)
	{
		$subject = $this->view->getTerm('email_verified');
		$this->message->set('to', $email);

		$sended = $this->message->send($email, $subject, 'send-email-after-verified');
		return $sended;
	}

	public function verifyEmail()
	{
		$this->view->set('email_verified', false);

		$email = mysql_escape_string($_GET['email']);
		$hash = mysql_escape_string($_GET['hash']);

		$user = $this->model->getUserByEmail($email);

		if ($user) {
			if ($user->verified == '0') {
				if ($user->hash == $hash) {
					$user->verified = '1';
					if ($this->model->saveUser($user)) {
						//$this->AddNotice('[$email_verification_success]');
						$this->view->set('email_verified', true);
						$this->sendMailAfterVerify($email);
						$this->completeRegistration($user);
					} else {
						$this->AddError('[$email_verification_failed]');
					}
				} else {
					$this->AddError('[$wrong_verification_code]');
				}
			} else {
				$this->AddNotice('[$email_was_verified]');
			}
		} else {
			$this->AddError('[$email_not_found]');
		}
		$this->view->render('email-confirmation');
	}

	private function generateNewUserToken($key = '')
	{
		return md5($key . rand(0, 1000));
	}

	public function resetPassword()
	{
		$reset_password_sent = false;
		$password_changed = false;
		if (isset($_GET['reset-request'])) {
			if (isset($_GET['email'], $_GET['token'])) {
				$email = mysql_escape_string($_GET['email']);
				$token = mysql_escape_string($_GET['token']);
				$user = $this->model->getUserByEmail($email);
				if ($user) {
					$this->view->set('user', $user);

					if (isset($_POST['reset-password'])) {
						if ($user->hash == $token) {
							(!isset($_POST['password'])) ? $this->AddError('[$password_required]') : '';
							($_POST['password'] != $_POST['rpassword']) ? $this->AddError('[$password_dont_match]') : '';
							if ($this->validatePassword($_POST['reset-password']) && count($this->view->getErrors()) == 0) {
								$user->plain_password = $_POST['password'];
								$user->hash = $this->generateNewUserToken();
								if (!is_array($this->model->saveUser($user))) {
									$this->sendMailPasswordChanged($user);
									$password_changed = true;
								} else {
									$this->AddError('[$error_occurred]');
								}
							}
						} else {
							$this->AddError('[$email_or_token_invalid]');
							Utils::RedirectTo($this->getActionUrl('resetPassword'));
							return;
						}
					}
				} else {
					$this->AddError('[$email_not_found]');
				}
			} else {
				$this->AddError('[$email_or_token_invalid]');
				Utils::RedirectTo($this->getActionUrl('resetPassword'));
				return;
			}
			$this->view->set('password_changed', $password_changed);
			$this->view->render('choose-new-password');
			return;
		}
		if (isset($_POST['submit-email'])) {
			$email = mysql_escape_string($_POST['email']);
			$user = $this->model->getUserByEmail($email);
			if ($user) {
				$user->hash = $this->generateNewUserToken();

				if ($this->model->saveUser($user)) {
					if ($this->sendMailResetPassword($user)) {
						//$this->AddNotice('[$email_reset_password_sent]');
						$reset_password_sent = true;
					} else {
						$this->AddError('[$email_reset_password_sending_failed]');
					}
				} else {
					$this->AddError('[$error_occurred]');
				}
			} else {
				$this->AddError('[$email_not_exist]');
			}
		}
		$this->view->set('reset_password_sent', $reset_password_sent);
		$this->view->render('reset-password-submit');
	}

	private function validatePassword($password)
	{
		$password_min_length = 6;
		$password_max_length = 30;
		if (strlen($password) > $password_max_length || strlen($password) < $password_min_length) {
			$this->AddError('[$password_length]' . ' ' . $password_min_length . ' [$to] ' . $password_max_length . ' [$characters]');
			return false;
		}
		return true;
	}

	private function sendMailPasswordChanged($user)
	{
		$subject = $this->view->getTerm('Password_changed');

		$this->message->set('user', $user);

		return $this->message->send($user->email, $subject, 'password-changed');
	}

	private function sendMailResetPassword($user)
	{
		$subject = $this->view->getTerm('reset_password');

		$this->message->set('user', $user);
		$reset_url = Utils::getComponentUrl('Users/resetPassword') . '?email=' . $user->email . '&token=' . $user->hash . '&reset-request=true';
		$this->message->set('reset_url', $reset_url);

		return $this->message->send($user->email, $subject, 'password-reset-mail');
	}

	public function dashboard()
	{
		if (isset($this->ComponentSettings['dashboard_url']) && $this->ComponentSettings['dashboard_url'] != '') {
			Utils::RedirectTo($this->ComponentSettings['dashboard_url']);
		} else {
			if (isset($_SESSION['redir_url'])) {
				$redir_url = $_SESSION['redir_url'];
				unset($_SESSION['redir_url']);
				Utils::RedirectTo(urldecode($redir_url));
				return;
			}
			$view = (isset($parameters['view'])) ? $parameters['view'] : 'dashboard';
			$this->view->render($view);
		}
	}
}
