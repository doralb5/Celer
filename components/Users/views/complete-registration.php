<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/md-login.css'); ?>


<div id="login-page">
    <div class="row">
        <div class="col-md-12">
            
            <div class="card">
                <div class="login">

                    <?php $this->showComponentErrors(); ?>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-login text-center">
                                <h3>[$Hi] <?=$user->firstname . ' ' . $user->lastname?></h3>
                                <?php //if(!$reg_completed) {?>
                                    <h1>[$please_complete_your_profile]</h1>
                                <?php //} else {?>
<!--                                    <h1>[$email_verification_sent]</h1>-->
                                <?php //}?>
                                <br/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->showComponentMessages(); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="login-content">
                                <form method="POST" class="margin-bottom-0">

                                    <div class="login-email">
                                        <fieldset>
                                            <input type="text" class="email" id="email" name="email" value="<?=$user->email?>" required="true" autofocus=""/>
                                            <label for="email">[$email]</label>
                                            <div class="underline"></div>
                                        </fieldset>
                                    </div>

                                    <?php if (count($user_categories) > 1) {
	?>
                                    
                                    <div class="register-select">
                                        <fieldset>
                                            <label for="user_category">[$user_category]</label>
                                            <select type="text" class="" id="user_category"  name="id_category" required="">
                                                <?php foreach ($user_categories as $categ) {
		if ($categ->id == $selected_categ) {
			$selected = 'selected';
		} else {
			$selected = '';
		} ?>
                                                    <option value="<?= $categ->id ?>" <?=$selected?>><?= $categ->category ?></option>
                                                <?php
	} ?>
                                            </select>
                                            <div class="underline"></div>
                                        </fieldset>
                                    </div>
                                    
                                    <?php
} ?>


                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="login-btn">
                                                <button type="submit" name="complete-profile" value="[$register]" class="btn btn-primary btn-block btn-sm">[$complete_registration]</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div>
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>



<script>
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function (e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("[$field_required]");
            }
            if (!e.target.validity.valueMissing) {
                e.target.setCustomValidity("[$field_invalid]");
            }
        };
        elements[i].oninput = function (e) {
            e.target.setCustomValidity("");
        };
    }
</script>
