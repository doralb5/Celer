<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/dashboard.css'); ?>


<div id="frontend-dashboard">
    <div class="row">
        <div class="col-md-6">
            <div class="box first">
                <span class="icon-cont"><i class="glyphicon glyphicon-briefcase"></i></span>
                <h3>[$businesses]</h3>
                <div class="row footer-btn">
                    <div class="col-md-6 left-btn">
                        <a class="" href="<?= Utils::getComponentUrl('Businesses/business_list_u/') ?>">[$my_businesses]</a>
                    </div>
                    <div class="col-md-6 right-btn">
                        <a class="" href="<?= Utils::getComponentUrl('Businesses/insert_business/') ?>">[$InsertBusiness]</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box third">
                <span class="icon-cont"><i class="glyphicon glyphicon-shopping-cart"></i></span>
                <h3>[$offers]</h3>
                <div class="row footer-btn">
                    <div class="col-md-6 left-btn">
                        <a class="" href="<?= Utils::getComponentUrl('Businesses/ListOffertsUser/') ?>">[$my_offers]</a>
                    </div>
                    <div class="col-md-6 right-btn">
                        <a class="" href="<?= Utils::getComponentUrl('Businesses/edit_offer/') ?>">[$add_offer]</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box second">
                <span class="icon-cont"><i class="glyphicon glyphicon-flag"></i></span>
                <h3>[$notification]</h3>
                <div class="row footer-btn">
                    <div class="col-md-6 left-btn">
                        <a class="" href="<?= Utils::getComponentUrl('Announces/listAnnouncesUser/') ?>">[$my_notification]</a>
                    </div>
                    <div class="col-md-6 right-btn">
                        <a class="" href="<?= Utils::getComponentUrl('Announces/insertAnnounce/') ?>">[$add_notification]</a>
                    </div>
                </div>
            </div>
        </div>
        
<!--        <div class="col-md-6">
            <div class="box fourth">
                <span class="icon-cont"><i class="glyphicon glyphicon-home"></i></span>
                <h3>[$propertys]</h3>
                <div class="row footer-btn">
                    <div class="col-md-6 left-btn">
                        <a class="" href="">[$my_propertys]</a>
                    </div>
                    <div class="col-md-6 right-btn">
                        <a class="" href="">[$add_property]</a>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</div>
