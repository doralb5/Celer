<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/edit-profile.css'); ?>
<?php
$username = (isset($_SESSION['user_auth']['username'])) ? $_SESSION['user_auth']['username'] : '';
$email = (isset($_SESSION['user_auth']['email'])) ? $_SESSION['user_auth']['email'] : '';
$firstname = (isset($_SESSION['user_auth']['firstname'])) ? $_SESSION['user_auth']['firstname'] : '';
$lastname = (isset($_SESSION['user_auth']['lastname'])) ? $_SESSION['user_auth']['lastname'] : '';
$w = 205;
$h = 205;
?>


<div id="edit-profile">
    <form method="post" class="form-horizontal" enctype="multipart/form-data" role="form">
        <div class="alert alert-grey">

            <h1 class="page-header">[$edit_profile]</h1>
            <div class="row">
                <!-- left column -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="text-center">
                        <?php if (isset($_SESSION['user_auth']['image']) && $_SESSION['user_auth']['image'] != '') {
	?>
                            <img src = "<?= Utils::genThumbnailUrl('users/' . $user->image, $w, $h, array('zc' => 1)) ?>" class="avatar img-circle img-thumbnail" alt="avatar">

                        <?php
} else {
		?>

                            <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar img-circle img-thumbnail" alt="avatar">

                        <?php
	} ?>
                        <h6>[$upload_profile_picture]...</h6>
                        <span class="file-input"><div class="file-preview ">
                                <div class="file-preview-thumbnails">
                                    <div class="file-preview-frame">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="file-preview-status text-center text-success"></div>
                                <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                            </div>
                        </span>

                        <div class="upload-btn">
                            <input type="file" name="image">
                        </div>
                    </div>
                </div>
                <!-- edit form column -->
                <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
                    <?php Messages::showComponentErrors('Users'); ?>
                    <?php Messages::showComponentMsg('Users'); ?>
                    
                    <?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
		?>


                        <div class="edit-profile">
                            <fieldset>
                                <input type="text" class="firstname" id="firstname"  name="firstname" value="<?= $firstname ?>"/>
                                <label for="firstname">[$first_name]</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>

                        <div class="edit-profile">
                            <fieldset>
                                <input type="text" class="lastname" id="lastname"  name="lastname" value="<?= $lastname ?>" />
                                <label for="lastname">[$last_name]</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>


                        <div class="edit-profile">
                            <fieldset>
                                <input type="email" class="email" id="email"  name="email" value="<?= $email ?>" />
                                <label for="email">[$email]</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>

                        <div class="edit-profile">
                            <fieldset>
                                <input type="text" class="username" id="username"  name="username" value="<?= $username ?>"/>
                                <label for="username">[$username]</label>
                                <div class="underline"></div>
                            </fieldset>
                        </div>

                    <?php
	} ?>

                    <div class="form-group">
                        <div class="col-md-6">
                            <input class="btn btn-primary" name="save" value="[$save_your_data]" type="submit">
                        </div>
                        <div class="col-md-6">
                            <input class="btn btn-default" value="[$cancell]" type="reset">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </form>

    <form method="POST">

        <div class="alert alert-grey password-reset">
            <div class="row">
                <div class="col-md-9">


                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="col-md-4 control-label">[$change_password]:</label>

                        <div class="col-md-4">
                            <div class="edit-profile">
                                <fieldset>
                                    <input type="password" class="password" id="password"  name="password"/>
                                    <label for="password">[$new_password]</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="edit-profile">
                                <fieldset>
                                    <input type="rpassword" class="rpassword" id="rpassword"  name="rpassword"/>
                                    <label for="rpassword">[$retype_new_password]</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 pull-right">
                    <input class="btn btn-primary btn-block" name="save_password" value="[$save_password]" type="submit">
                </div>
            </div>
        </div>

    </form>
</div>



<script>
    $(document).ready(function () {
        fileInput();
    });
</script>
<script>
    // Function to replace inputs

    function fileInput(fi_container_class, fi_button_class, fi_filename_class, fi_button_text) {

        // Arguments
        fi_container_class = fi_container_class || 'fileUpload'; // Classname of the wrapper that contains the button & filename.
        fi_button_class = fi_button_class || 'fileBtn'; // Classname for the button
        fi_filename_class = fi_filename_class || 'fileName'; // Name of the text element's class
        fi_button_text = fi_button_text || '[$upload_image]'; // Text inside the button

        // Variables
        var fi_file = $('input[type=file]'); // Type of input to look for

        // Hide file inputs
        fi_file.css('display', 'none');

        // String to append
        var fi_str = '<div class="' + fi_container_class + '"><div class="' + fi_button_class + '">' + '<i class="fa fa-camera" aria-hidden="true"></i>' + '</div><div class="' + fi_filename_class + '"></div></div>';
        // Append "fake input" after the original input (which have been hidden)
        fi_file.after(fi_str);

        // Count amount of inputs
        var fi_count = fi_file.length;
        // Loop while "count" is greater than or equal to "i".
        for (var i = 1; i <= fi_count; i++) {
            // Get original input-name
            var fi_file_name = fi_file.eq(i - 1).attr('name');
            // Assign the name to the equivalent "fake input".
            $('.' + fi_container_class).eq(i - 1).attr('data-name', fi_file_name);
        }

        // Button: action
        $('.' + fi_button_class).on('click', function () {
            // Get the name of the clicked "fake-input"
            var fi_active_input = $(this).parent().data('name');
            // Trigger "real input" with the equivalent input-name
            $('input[name=' + fi_active_input + ']').trigger('click');
        });

        // When the value of input changes
        fi_file.on('change', function () {
            // Variables
            var fi_file_name = $(this).val(); // Get the name and path of the chosen file
            var fi_real_name = $(this).attr('name'); // Get the name of changed input

            // Remove path from file-name
            var fi_array = fi_file_name.split('\\'); // Split on backslash (and escape it)
            var fi_last_row = fi_array.length - 1; // Deduct 1 due to 0-based index
            fi_file_name = fi_array[fi_last_row]; //

            // Loop through each "fake input container"
            $('.' + fi_container_class).each(function () {
                // Name of "this" fake-input
                var fi_fake_name = $(this).data('name');
                // If changed "fake button" is equal to the changed input-name
                if (fi_real_name == fi_fake_name) {
                    // Add chosen file-name to the "fake input's label"
                    $('.' + fi_container_class + '[data-name=' + fi_real_name + '] .' + fi_filename_class).html(fi_file_name);
                }
            });
        });
    }
</script>

<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });
        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });
    }).call(this);
    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>