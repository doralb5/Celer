<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/md-login.css'); ?>


<div id="login-page">
    <div class="row">
        <div class="col-md-12">
            <?php $this->showComponentErrors(); ?>
            
            <div class="card">
                <div class="login">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-login text-center"><br>
                                <h1 class="login-main-title">[$Verify_email]</h1>
                                <h1 class="login-main-subtitle">
                                <?php if ($email_verified) {
	echo '[$email_verification_success]';
	echo '<br/><br/><p><a href="' . $url_login . '" class="btn btn-login">[$Login]</a></p>';
} else {
	echo '[$email_verification_failed]';
}
								?>
                                </h1>
                                <br><br><br>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->showComponentMessages(); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>