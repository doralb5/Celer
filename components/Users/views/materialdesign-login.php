<?php  /* @var $this ComponentView */ ?>

<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/md-login.css'); ?>

<?php
$username = (isset($_POST['username'])) ? $_POST['username'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
?>


<div id="login-page">
    <div class="row">
        <div class="col-md-12">
            <?php $this->showComponentErrors(); ?>
            <div class="card">
                <div class="login">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-login text-center">
                                <h1 class="login-main-title">[$login]</h1>
                                <h1 class="login-main-subtitle">[$new_to] <?= CMSSettings::$webdomain ?>?&nbsp;<a href="<?= Utils::getComponentUrl('Users/register') ?>">[$sign_up]</a></h1><br>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                           <?php $this->showComponentMessages(); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 left-side">
                            <div class="login-content">
                                <form method="POST" class="margin-bottom-0">

                                    <div class="login-email">
                                        <fieldset>
                                            <input type="text" class="username" id="username"  name="username" value="<?= $username ?>"required="" autofocus=""/>
                                            <label for="username">[$username]<?=(!isset($settings['can_set_username']) || $settings['can_set_username'] == 0) ? '/[$Email]' : ''?></label>
                                            <div class="underline"></div>
                                        </fieldset>
                                    </div>
                                    <div class="login-password">
                                        <fieldset>
                                            <input type="password" class="password" id="password"  name="password" required=""/>
                                            <label for="password">[$password]</label>
                                            <div class="underline"></div>
                                        </fieldset>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="login-btn">
                                                <button type="submit" name="login" value="[$login]" class="btn btn-primary btn-block btn-sm">[$login]</button>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div> 
                                        <p class="no-account">
                                            [$dont_have_account] <span class="register-buttons" ><a href="<?= Utils::getComponentUrl('Users/register') ?>">[$Register]</a></span>
                                        </p>
                                        <p class="forgot-pasword">
                                            <span class="register-buttons" ><a href="<?= Utils::getComponentUrl('Users/resetPassword') ?>">[$forgot_password]</a></span>
                                        </p>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="col-md-6 right-side">
                            <div class="login-content">
                                <div class="login-btn">
                                    <div class="register-buttons">
                                        <?php
										echo '<a  class="login-with-fb btn btn-block btn-xs" href="' . htmlspecialchars($FBLoginUrl) . '"><span class="icon fa fa-facebook"></span>[$login_with] Facebook</a>';
										?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                </div>
            </div>
        </div>

    </div>
</div>


<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });

        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });

    }).call(this);



    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>