<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/md-login.css'); ?>

<?php
$code = (isset($_POST['code'])) ? $_POST['code'] : '';
$firstname = (isset($_POST['firstname'])) ? $_POST['firstname'] : '';
$lastname = (isset($_POST['lastname'])) ? $_POST['lastname'] : '';
$username = (isset($_POST['username'])) ? $_POST['username'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
$selected_categ = (isset($_POST['id_category'])) ? $_POST['id_category'] : ((isset($default_category)) ? $default_category : '');
?>


<div id="login-page">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="login">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-login text-center">
                                <h1 class="login-main-title">[$NewRegistration]</h1>
                                <h1 class="login-main-subtitle">[$already_have_account]&nbsp;<a href="<?= Utils::getComponentUrl('Users/login') ?>">[$login]</a></h1><br>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->showComponentMessages(); ?>
                            <?php $this->showComponentErrors(); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 left-side">
                            <div class="register-content">
                                <form method="POST" class="margin-bottom-0">


                                    <?php if (count($user_categories) > 1) {
	?>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="register-select">
                                                <fieldset>
                                                    <label for="user_category">[$user_category]</label>
                                                    <select type="text" class="" id="user_category"  name="id_category" required="">
                                                        <?php foreach ($user_categories as $categ) {
		if ($categ->id == $selected_categ) {
			$selected = 'selected';
		} else {
			$selected = '';
		} ?>
                                                            <option value="<?= $categ->id ?>" <?=$selected?>><?= $categ->category ?></option>
                                                        <?php
	} ?>
                                                    </select>
                                                    <div class="underline"></div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                       

                                    <?php
} ?>
                                        
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="login-name">
                                                <fieldset>
                                                    <input type="text" class="firstname" id="firstname"  name="firstname" value="<?= $firstname ?>"required="" autofocus=""/>
                                                    <label for="firstname">[$first_name]</label>
                                                    <div class="underline"></div>
                                                </fieldset>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="login-name">
                                                <fieldset>
                                                    <input type="text" class="lastname" id="lastname"  name="lastname" value="<?= $lastname ?>"required=""/>
                                                    <label for="[$last_name]">[$last_name]</label>
                                                    <div class="underline"></div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <?php if (!isset($settings['can_set_username']) || $settings['can_set_username'] != 0) {
		?> 
                                        <div class="col-md-12">
                                            <div class="login-username">
                                                <fieldset>
                                                    <input type="text" class="username" id="username"  name="username" value="<?= $username ?>"required=""/>
                                                    <label for="[$username]">[$username]</label>
                                                    <div class="underline"></div>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <?php
	} ?>
                                        <div class="col-md-12">
                                            <div class="login-username">
                                                <fieldset>
                                                    <input type="text" class="email" id="email"  name="email" value="<?= $email ?>"required=""/>
                                                    <label for="[$email]">[$email]</label>
                                                    <div class="underline"></div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="login-password">
                                                <fieldset>
                                                    <input type="password" class="password" id="password"  name="password" required=""/>
                                                    <label for="[$password]">[$password]</label>
                                                    <div class="underline"></div>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="login-username">
                                                <fieldset>
                                                    <input type="password" class="rpassword" id="rpassword"  name="rpassword" required=""/>
                                                    <label for="[$retype_password]">[$retype_password]</label>
                                                    <div class="underline"></div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="checkbox checkbox-danger">
                                                <input id="checkbox6" type="checkbox" name="agree" required="">
                                                <label for="checkbox6">
                                                    [$accept_terms_and_conditions]
                                                </label>
                                            </div>


                                        </div>
                                    </div>


                                    <div class="login-btn">
                                        <div class="register-buttons">
                                            <button type="submit" name="register" class="btn btn-primary btn-block btn-sm">[$Register]</button>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="m-t-20 m-b-40 p-b-40 text-center">
                                        [$registred_before_question] &nbsp; &nbsp; | &nbsp; &nbsp;[$click] <a href="<?= Utils::getComponentUrl('Users/login') ?>">[$here]</a> [$to_login].
                                    </div>

                                </form>
                            </div>
                        </div>

                        <div class="col-md-6 right-side">
                            <div class="register-content">
                                <div class="login-btn">
                                    <div class="register-buttons">
                                        <?php
										//if ($_SERVER['REMOTE_ADDR']=='46.252.33.104')
										echo '<a  class="login-with-fb btn btn-block btn-xs" href="' . htmlspecialchars($FBLoginUrl) . '"><span class="icon fa fa-facebook"></span>[$login_with] Facebook</a>';
										?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                  
                </div>
            </div>




        </div>
    </div>

</div>


<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });

        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });

    }).call(this);



    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>