<style>
    
    /* Reviews list comment*/
#reviews-list ul#comments {
    list-style-type: none;
    font-family: "Helvetica Neue", arial, sans-serif;
    font-size: 15px;
    color: #a5b2b9;
}

#reviews-list ul#comments li {
    margin-bottom: 15px;
    position: relative;
}

#reviews-list ul#comments li a {
    text-decoration: none;
    color: rgb(255, 255, 255);
}

#reviews-list .avatar-container {
    width: 60px;
    box-sizing: border-box;
}

#reviews-list .avatar {
    width: 50px;
    height: 50px;
    border-radius: 2px;
    float: left;
    margin: 5px;
    background: white;
    border: 2px solid white;
}

#reviews-list .post-container {
    padding-bottom: 15px;
    margin-left: 75px;
    box-sizing: border-box;
    border-bottom: 1px solid #383838;
}

#reviews-list .post-container p {
    color: #f6f6f6;
    font-size: 12px;
    letter-spacing: .02em;
    font-weight: lighter;
}

#reviews-list .author_name {
    font-size: 14px;
    text-transform: uppercase;
    color: white;
}

#reviews-list .comment-stars {
    font-size: 12px;
}

#reviews-list .timeago,
#reviews-list .posted {
    font-weight: lighter;
    font-size: 11px;
    color: #db5252;
}
#reviews-list .timeago span
{
    font-weight: lighter;
    font-size: 11px;
    color: #e2e7ea;
}

#reviews-list .bullet {
    padding: 0 0px 0 5px;
    font-size: 12px;
    color: #ccc;
    line-height: 1.4;
}
</style>




<section id="reviews-list">
    <div class="row">
        <div class="col-md-12">

            <h4 id="row4" class="page-header" style="text-transform: uppercase;"> Reçensionet</h4>
            <ul id="comments">
                <li>
                    <div class="avatar-container">
                        <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar" alt="avatar">
                    </div>
                    <div class="post-container">
                        <div>
                            <span class="author_name">Erald Halili                                                            </span>

                            <span class="bullet" aria-hidden="true">•</span>

                            <span class="comment-stars">
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                            </span>

                            <p></p>

                            <p>The result is what you are exploring right now.</p>

                            <p></p>

                            <span class="timeago">Postuar ne:  <span>14   Dhjetor 2016</span></span>

                        </div>
                    </div>

                </li>

            </ul>
            <ul id="comments">
                <li>
                    <div class="avatar-container">
                        <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar" alt="avatar">
                    </div>
                    <div class="post-container">
                        <div>
                            <span class="author_name">Erald 1 Halili                                                            </span>

                            <span class="bullet" aria-hidden="true">•</span>

                            <span class="comment-stars">
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                            </span>

                            <p></p>

                            <p>The result is what you are exploring right now.</p>

                            <p></p>

                            <span class="timeago">Postuar ne:  <span>14   Dhjetor 2016</span></span>

                        </div>
                    </div>

                </li>

            </ul>
            <ul id="comments">
                <li>
                    <div class="avatar-container">
                        <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar" alt="avatar">
                    </div>
                    <div class="post-container">
                        <div>
                            <span class="author_name">Erald 1 Halili                                                            </span>

                            <span class="bullet" aria-hidden="true">•</span>

                            <span class="comment-stars">
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                            </span>

                            <p></p>

                            <p>The result is what you are exploring right now.</p>

                            <p></p>

                            <span class="timeago">Postuar ne:  <span>14   Dhjetor 2016</span></span>

                        </div>
                    </div>

                </li>

            </ul>
            <ul id="comments">
                <li>
                    <div class="avatar-container">
                        <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar" alt="avatar">
                    </div>
                    <div class="post-container">
                        <div>
                            <span class="author_name">Erald 1 Halili                                                            </span>

                            <span class="bullet" aria-hidden="true">•</span>

                            <span class="comment-stars">
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                            </span>

                            <p></p>

                            <p>The result is what you are exploring right now.</p>

                            <p></p>

                            <span class="timeago">Postuar ne:  <span>14   Dhjetor 2016</span></span>

                        </div>
                    </div>

                </li>

            </ul>
            <ul id="comments">
                <li>
                    <div class="avatar-container">
                        <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar" alt="avatar">
                    </div>
                    <div class="post-container">
                        <div>
                            <span class="author_name">Erald 1 Halili                                                            </span>

                            <span class="bullet" aria-hidden="true">•</span>

                            <span class="comment-stars">
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                            </span>

                            <p></p>

                            <p>test</p>

                            <p></p>

                            <span class="timeago">Postuar ne:  <span>14   Dhjetor 2016</span></span>

                        </div>
                    </div>

                </li>

            </ul>
            <ul id="comments">
                <li>
                    <div class="avatar-container">
                        <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar" alt="avatar">
                    </div>
                    <div class="post-container">
                        <div>
                            <span class="author_name">Erald 1 Halili                                                            </span>

                            <span class="bullet" aria-hidden="true">•</span>

                            <span class="comment-stars">
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                            </span>

                            <p></p>

                            <p>The result is what you are exploring right now.</p>

                            <p></p>

                            <span class="timeago">Postuar ne:  <span>14   Dhjetor 2016</span></span>

                        </div>
                    </div>

                </li>

            </ul>
            <ul id="comments">
                <li>
                    <div class="avatar-container">
                        <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar" alt="avatar">
                    </div>
                    <div class="post-container">
                        <div>
                            <span class="author_name">silva                                                            </span>

                            <span class="bullet" aria-hidden="true">•</span>

                            <span class="comment-stars">
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>
                            </span>

                            <p></p>

                            <p>super fare</p>

                            <p></p>

                            <span class="timeago">Postuar ne:  <span>06   Dhjetor 2016</span></span>

                        </div>
                    </div>

                </li>

            </ul>
            <ul id="comments">
                <li>
                    <div class="avatar-container">
                        <img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar" alt="avatar">
                    </div>
                    <div class="post-container">
                        <div>
                            <span class="author_name">Erald Halili                                                            </span>

                            <span class="bullet" aria-hidden="true">•</span>

                            <span class="comment-stars">
                                <i class="fa fa-star star-on-png "></i>
                                <i class="fa fa-star star-on-png "></i>

                                <i class="fa fa-star-o star-on-png "></i>


                                <i class="fa fa-star-o star-on-png "></i>


                                <i class="fa fa-star-o star-on-png "></i>

                            </span>

                            <p></p>

                            <p>A typical novice strategy for searching the Internet is to type the topic into the address bar. For example, if you are researching Christopher Columbus, you naturally would look first at www.columbus.com. Unfortunately, as you will see if you click on this link, that is not helpful. Neither is www.columbus.org, which takes you to the Columbus, OH, Chamber of Commerce.

                                There are many less benign examples of site names that do not relate to the topic they appear to be about. While we could use many of the existing sites that make this point for us, we did not want to run the risk of finding out in a year or two that the site had new, undesirable content. The only way we could guarantee this would be to create our own site in which we could control the content ourselves. The result is what you are exploring right now.</p>

                            <p></p>

                            <span class="timeago">Postuar ne:  <span>03   Nëntor 2016</span></span>

                        </div>
                    </div>

                </li>

            </ul>
        </div>
    </div>
</section>