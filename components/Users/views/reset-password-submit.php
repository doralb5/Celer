<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/md-login.css'); ?>


<div id="login-page">
    <div class="row">
        <div class="col-md-12">
            <?php $this->showComponentErrors(); ?>
            
            <div class="card">
                <div class="login">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-login text-center"><br>
                                <h1 class="login-main-title">[$reset_password_title]</h1>
                                <h1 class="login-main-subtitle">
                                <?php if ($reset_password_sent) {
	echo '[$reset_password_sent]';
} else {
	echo '[$reset_password_subtitle]';
}
								?>
                                </h1>
                                <br><br><br>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->showComponentMessages(); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="login-content">
                                
                                
                                <?php
								if (!$reset_password_sent) {
									?>
                                
                                <form method="POST" class="margin-bottom-0">

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="login-email">
                                                        <fieldset>
                                                            <input type="email" class="email" id="email" name="email" required="" autofocus=""/>
                                                            <label for="email">[$email]</label>
                                                            <div class="underline"></div>
                                                        </fieldset>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="login-btn">
                                                        <button type="submit" name="submit-email" value="[$request_password]" class="btn btn-primary btn-block btn-sm">[$request_password]</button>
                                                    <br><br><br>
                                                    </div>
                                                </div>
                                                

                                            </div>
                                        </div>
                                    </div>

                                </form>
                                
                                <?php
								}
								?>
                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>

    </div>
</div>


<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });

        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });

    }).call(this);



    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>
