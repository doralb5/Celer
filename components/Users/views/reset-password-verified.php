<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/md-login.css'); ?>


<div id="login-page">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="login">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-login text-center">
                                <h1 class="login-main-title">[$new_password_title]</h1>
                                <h1 class="login-main-subtitle">[$new_password_subtitle] <?= CMSSettings::$webdomain ?></h1><br>
                                <h3>Hi, <?=$username?></h3>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?php Messages::showComponentErrors('Users'); ?>
                            <?php Messages::showComponentMsg('Users'); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="login-content">
                                <form method="POST" class="margin-bottom-0">

                                    <div class="login-email">
                                        <fieldset>
                                            <input type="password" class="username" id="password"  name="password" value=""required="true" autofocus=""/>
                                            <label for="password">[$new_password]</label>
                                            <div class="underline"></div>
                                        </fieldset>
                                    </div>

                                    <div class="login-email">
                                        <fieldset>
                                            <input type="password" class="username" id="r-password"  name="rpassword" value=""required="true" autofocus=""/>
                                            <label for="r-password">[$retype_new_password]</label>
                                            <div class="underline"></div>
                                        </fieldset>
                                    </div>


                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="login-btn">
                                                <button type="submit" name="reset-password" value="[$save_password]" class="btn btn-primary btn-block btn-sm">[$save_password]</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <hr />
                            <p class="text-inverse">
                                &copy; <?= CMSSettings::$website_title ?> <?= date('Y') ?>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>


<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });

        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });

    }).call(this);



    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>
