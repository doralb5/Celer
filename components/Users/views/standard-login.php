<?php
$username = (isset($_POST['username'])) ? $_POST['username'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
?>

<style>
    .or {
        display: block;
        width: 100%;
        height: 1px;
        border-bottom: 1px solid #dee3e4;
        position: relative;
        margin: 3em 0;
    }

    .or:before {
        width: 30px;
        height: 30px;
        position: absolute;
        top: -15px;
        right: calc(50% - 20px);
        background-color: #fff;
        text-align: center;
        color: #555;
        border-radius: 50%;
        line-height: 30px;
        font-size: 15px;
    }

    .login-with-fb
    {
        width: 100%;
        display: block;
        margin: 15px 0;
        color: #fff;
        text-align: center;
        line-height: 40px;
        font-size: 14px;
        opacity: .95;
        border-radius:         3px;
        -moz-border-radius:    3px;
        -webkit-border-radius: 3px;
    }
    .login-with-fb:hover{
        opacity: 1;
        color:white;
    }
    .login-with-fb{
        background: #527EBF
    }
    .login-with-fb .icon
    {
        float: left;
        font-size: 21px;
        width: 50px;
        height: 26px;
        margin: 7px;
        padding: 2px;
        text-align: center;
        border-right: 1px solid #fff;
    }

</style>

<div class="work-jarallax work-parallax">
    <div class="paralaxMask">&nbsp;</div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                <div id="login-page">

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="login-header">
                                <img src="http://<?= CMSSettings::$webdomain . '/' . MEDIA_ROOT . CMSSettings::$logo ?>" width="100%">
                            </div>
                        </div>
                    </div>

                    <div class="login-content">
                        <form method="POST" class="margin-bottom-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php Messages::showComponentErrors('Users'); ?>
                                    <?php Messages::showComponentMsg('Users'); ?>
                                </div>
                            </div>
                            <div class="login-email">
                                <label class="control-label">[$username]</label>
                                <div class="row m-b-15">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" placeholder="[$username]" name="username" value="<?= $username ?>"required=""/>
                                    </div>
                                </div>
                            </div>

                            <div class="login-password">
                                <label class="control-label">[$password]</label>
                                <div class="row m-b-15">
                                    <div class="col-md-12">
                                        <input type="password" class="form-control" placeholder="[$password]" name="password" required=""/>
                                    </div>
                                </div>
                            </div>

                            <div> 

                                <p>
                                    [$dont_have_account] <span class="register-buttons" ><a href="<?= Utils::getComponentUrl('Users/register') ?>">[$Register]</a></span>
                                </p>
                            </div>

                            <div class="login-btn">
                                <div class="register-buttons">
                                    <button type="submit" name="login" value="[$login]" class="btn btn-primary btn-block btn-md">[$login]</button>

                                    <div class="or">[$or]</div>

                                    <?php
									echo '<a  class="login-with-fb btn btn-block btn-xs" href="' . htmlspecialchars($FBLoginUrl) . '"><span class="icon fa fa-facebook"></span>[$login_with] Facebook</a>';
									?>

                                </div>
                            </div>

                            <hr />
                            <p class="text-center text-inverse">
                                &copy; <?= CMSSettings::$website_title ?> <?= date('Y') ?>
                            </p>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
