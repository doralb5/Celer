<?php
$code = (isset($_POST['code'])) ? $_POST['code'] : '';
$firstname = (isset($_POST['firstname'])) ? $_POST['firstname'] : '';
$lastname = (isset($_POST['lastname'])) ? $_POST['lastname'] : '';
$username = (isset($_POST['username'])) ? $_POST['username'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
?>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                <div id="register-page">

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="login-header">
                                <img src="http://<?= CMSSettings::$webdomain . '/' . MEDIA_ROOT . CMSSettings::$logo ?>" width="100%">
                            </div>
                        </div>
                    </div>

                    <div class="register-content">
                        <form method="POST" class="margin-bottom-0">
                            <div class="row m-b-15">
                                <div class="col-md-12">
                                    <?php Messages::showComponentErrors('Users'); ?>
                                    <?php Messages::showComponentMsg('Users'); ?>
                                </div>
                            </div>

                            <div class="login-name">
                                <label class="control-label">[$first_name]</label>
                                <div class="row row-space-10">
                                    <div class="col-md-6 m-b-15">
                                        <input type="text" class="form-control" placeholder="[$first_name]" name="firstname" value="<?= $firstname ?>"required=""/>
                                    </div>
                                    <div class="col-md-6 m-b-15">
                                        <input type="text" class="form-control" placeholder="[$last_name]" name="lastname" value="<?= $lastname ?>"required=""/>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($settings['can_set_username']) && $settings['can_set_username'] != 0) {
	?> 
                            <div class="login-username">
                                <label class="control-label">[$username]</label>
                                <div class="row m-b-15">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" placeholder="[$username]" name="username" value="<?= $username ?>"required=""/>
                                    </div>
                                </div>
                            </div>
                            <?php
} ?>
                            <?php if (count($user_categories) > 1) {
		?>
                                <br/>
                                <div class="login-email">
                                        <label class="control-label">[$user_category]</label>
                                        <div class="col-md-9 col-sm-9">
                                            <select class="form-control" name="id_category">
                                                <?php foreach ($user_categories as $categ) {
			?>
                                                    <option
                                                        value="<?= $categ->id ?>" ><?= $categ->category ?></option>
                                                    <?php
		} ?>
                                            </select>
                                        </div>
                                </div>

                            <?php
	} ?>


                            <div class="login-email">
                                <label class="control-label">Email</label>
                                <div class="row m-b-15">
                                    <div class="col-md-12">
                                        <input type="email" class="form-control" placeholder="[$email]" name="email" value="<?= $email ?>"required=""/>
                                    </div>
                                </div>
                            </div>

                            <div class="login-password">
                                <label class="control-label">[$password]</label>
                                <div class="row m-b-15">
                                    <div class="col-md-12">
                                        <input type="password" class="form-control" placeholder="[$password]" name="password" required=""/>
                                    </div>
                                </div>
                            </div>

                            <div class="login-password">
                                <label class="control-label">[$confirm_pass]</label>
                                <div class="row m-b-15">
                                    <div class="col-md-12">
                                        <input type="password" class="form-control" placeholder="[$retype_password]" name="rpassword" required=""/>
                                    </div>
                                </div>
                            </div>


                            <div class="checkbox m-b-30">
                                <label>
                                    <input type="checkbox" name="agree"/> [$accept_terms_and_conditions]
                                </label>
                            </div>

                            <div class="login-btn">
                                <div class="register-buttons">
                                    <button type="submit" name="register" class="btn btn-primary btn-block btn-lg">[$register]</button>
                                </div>
                            </div>

                            <div class="m-t-20 m-b-40 p-b-40 text-center">
                                [$registred_before_question]<br>[$click] <a href="<?= Utils::getComponentUrl('Users/login') ?>">[$here]</a> [$to_login].
                            </div>
                            <hr />
                            <p class="text-center text-inverse">
                                &copy;  <?= CMSSettings::$webdomain . ' ' . date('Y') ?>
                            </p>
                        </form>
                    </div>

                </div>
            </div>
        </div>

<script>
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function (e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("[$field_required]");
            }
            if (!e.target.validity.valueMissing) {
                e.target.setCustomValidity("[$field_invalid]");
            }
        };
        elements[i].oninput = function (e) {
            e.target.setCustomValidity("");
        };
    }
</script>
