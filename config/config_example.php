<?php

define('DOMAIN', 'www.exampledomain.com');

define('DB_TYPE', 'mysql');
define('DB_HOST', 'dbhost');
define('DB_NAME', 'db_name');
define('DB_USER', 'db_user');
define('DB_PASS', 'db_pass');
define('DATA_DIR', 'data' . DS);
define('MEDIA_ROOT', DATA_DIR . 'media' . DS);
define('CACHE_PATH', MEDIA_ROOT . DS . 'thumbs' . DS);
define('BRAND_LOGO', WEBROOT . 'commons/images/poweredby/poweredbybh.png');
define('BRAND_URL', '//www.bluehat.al');
define('BRAND_FAVICON', WEBROOT . 'commons/images/bluehat.ico');
define('FTP_HOST', 'ftp_host');
define('FTP_USER', 'ftp_user');
define('FTP_PASS', 'ftp_pass');

define('FB_APPID', '1726438044257214');
//define('CMS_LOGO', '/data/media/cmslogo.png');
define('DEFAULT_DATA_DIR', 'data' . DS . 'cmsv2.bluehat.al' . DS);
define('DEFAULT_MEDIA_ROOT', 'data' . DS . 'cmsv2.bluehat.al' . DS . 'media' . DS);

define('TABLE_PREFIX', 'cms_');

//define('MAX_PAGES', ($site['max_pages'] != 0) ? $site['max_pages'] : 1000);
//define('MAX_SUBMENUS', $site['max_submenus']);
//define('QUOTA_WEB', $site['quota_web']);
