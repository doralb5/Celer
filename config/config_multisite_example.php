<?php

define('DEVELOPMENT_ENVIRONMENT', false);
define('DEVELOPMENT_MAIL', 'developers@bluehat.al');
define('SHOW_ERRORS', false);

define('MAIN_DB_TYPE', 'mysql');
define('MAIN_DB_HOST', 'mysql1.bluehat.it');
define('MAIN_DB_NAME', 'serv_db');
define('MAIN_DB_USER', 'serv_user');
define('MAIN_DB_PASS', 'pruLkSG#79');

$wwwdomain = $_SERVER['HTTP_HOST'];
$domain = (strpos($wwwdomain, 'www.') === 0) ? substr($wwwdomain, 4) : $wwwdomain;

if (strpos($domain, '.netirane.al') && $domain != 'www.netirane.al') {
	$domain = 'site.neshqiperi.al';
}
if (strpos($domain, '.neshkoder.al') && $domain != 'www.neshkoder.al') {
	$domain = 'site.neshqiperi.al';
}

$db = mysql_connect(MAIN_DB_HOST, MAIN_DB_USER, MAIN_DB_PASS);
mysql_select_db(MAIN_DB_NAME, $db) or die('Errore durante la connessione al Main DB');

$qsel = "SELECT
                    `Website`.`domain`,
                    `Website`.`db_host`,
                    `Website`.`db_user`,
                    `Website`.`db_pass`,
                    `Website`.`db_name`,
                    `Website`.`ftp_host`,
                    `Website`.`ftp_user`,
                    `Website`.`ftp_pass`,
                    `Website`.`data_dir`,
                    `Website`.`dev_mode`,
                    `Website`.`expire_date`,
                    `Website`.`enabled`,
                    `Profile`.`max_pages`,
                    `Profile`.`max_submenus`,
                    `Profile`.`quota_web`,
                    `Client`.`id_brand`
                FROM `serv_db`.`Website`
                LEFT JOIN `serv_db`.`Profile` ON(`Profile`.`id` = `Website`.`id_profile`)
                LEFT JOIN `serv_db`.`Client` ON(`Client`.`id` = `Website`.`id_client`)
                WHERE `Website`.`domain` LIKE '$wwwdomain' OR `Website`.`domain` LIKE '" . $domain . "' LIMIT 1;
                    ";

$res = mysql_query($qsel);

if (mysql_num_rows($res)) {
	$site = mysql_fetch_array($res);

	if (strtotime($site['expire_date']) < strtotime('now') && $site['expire_date'] != '0000-00-00') {
		echo 'SERVIZIO SCADUTO!';
		exit;
	} elseif (!$site['enabled']) {
		echo 'SERVIZIO SOSPESO';
		exit;
	}
	define('DOMAIN', $site['domain']);

	define('DB_TYPE', 'mysql');
	define('DB_HOST', $site['db_host']);
	define('DB_NAME', $site['db_name']);
	define('DB_USER', $site['db_user']);
	define('DB_PASS', $site['db_pass']);
	define('MEDIA_ROOT', rtrim($site['data_dir'], DS) . DS . 'media' . DS);
	define('CACHE_PATH', MEDIA_ROOT . DS . 'thumbs' . DS);
	define('DATA_DIR', rtrim($site['data_dir'], DS) . DS);

	switch ($site['id_brand']) {
			case 1:
				define('BRAND_LOGO', WEBROOT . 'commons/images/poweredby/WeWeb-PoweredBy.png');
				define('BRAND_URL', '//www.weweb.al');
				define('BRAND_FAVICON', WEBROOT . 'commons/images/weweb.ico');
				break;
			case 3:
				define('BRAND_LOGO', WEBROOT . 'commons/images/poweredby/poweredbybh.png');
				define('BRAND_URL', '//www.bluehat.al');
				define('BRAND_FAVICON', WEBROOT . 'commons/images/bluehat.ico');
				break;
			default:
				define('BRAND_LOGO', WEBROOT . 'commons/images/poweredby/WeWeb-PoweredBy.png');
				define('BRAND_URL', '//www.weweb.al');
				define('BRAND_FAVICON', WEBROOT . 'commons/images/weweb.ico');
				break;
		}

	define('FTP_HOST', $site['ftp_host']);
	define('FTP_USER', $site['ftp_user']);
	define('FTP_PASS', $site['ftp_pass']);
	//define('DEVELOPMENT_ENVIRONMENT', ($site['dev_mode'] == 1) ? true : false);

	define('MAX_PAGES', ($site['max_pages'] != 0) ? $site['max_pages'] : 1000);
	define('MAX_SUBMENUS', $site['max_submenus']);
	define('QUOTA_WEB', $site['quota_web']);

	define('FB_APPID', '1726438044257214');
	//define('CMS_LOGO', '/data/media/cmslogo.png');
	define('DEFAULT_DATA_DIR', 'data' . DS . 'cmsv2.bluehat.al' . DS);
	define('DEFAULT_MEDIA_ROOT', 'data' . DS . 'cmsv2.bluehat.al' . DS . 'media' . DS);

	define('TABLE_PREFIX', 'cms_');
} else {
	echo 'ATTENZIONE!!! A questo dominio non &egrave; associato nessun servizio!';
	exit;
}
