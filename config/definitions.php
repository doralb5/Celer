<?php

define('DS', DIRECTORY_SEPARATOR);
define('DOCROOT', dirname(dirname(__FILE__)) . DS);

$site_scheme = (!isset($_SERVER['HTTPS']) || empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off') ? 'http' : 'https';
define('SITE_SCHEME', $site_scheme);

define('CLASSES_PATH', 'classes' . DS);
define('CONTROLLERS_PATH', 'controllers' . DS);
define('COMPONENTS_PATH', 'components' . DS);
define('MODELS_PATH', 'models' . DS);
define('ENTITIES_PATH', 'entities' . DS);
define('VIEWS_PATH', 'views' . DS);
define('MODULES_PATH', 'modules' . DS);
define('TEMPLATES_PATH', 'templates' . DS);
define('LANGS_PATH', 'lang' . DS);
define('LIBS_PATH', 'libs' . DS);
define('TEMP_PATH', 'tmp' . DS);
define('COMMONS_PATH', 'commons' . DS);

define('CSS_PATH', 'commons' . DS . 'css' . DS);
define('JS_PATH', 'commons' . DS . 'js' . DS);
define('MEDIA_PATH', 'commons' . DS . 'media' . DS);
define('CMLIBS_PATH', 'commons' . DS . 'libs' . DS);

define('POST_MAX_SIZE', '20M');
define('MEMORY_LIMIT', '256M');
