<?php

class ImageManager extends BaseController
{
	public function thumb($filename, $size)
	{
		$filename = ltrim($filename, '/');
		/**
		 * Create a thumbnail
		 *
		 */
		// define allowed image sizes
		//        $sizes = array(
		//            '75x50',
		//            '576x300',
		//            '210x120',
		//            '250x200',
		//            '768x300',
		//            '128x100',
		//            '182x150'
		//        );
		// get the thumbnail from the URL
		$thumb = htmlspecialchars(DOCROOT . MEDIA_ROOT . $filename);

		// get the image and size
		list($width, $height) = explode('x', $size);

		// ensure the size is valid
		//        if (!in_array($size, $sizes)) {
		//            error('invalid size');
		//        }
		// ensure the image file exists
		if (!file_exists(MEDIA_ROOT . $filename)) {
			$this->error('No source image!');
		}

		// generate the thumbnail
		require(LIBS_PATH . 'phpThumb/phpthumb.class.php');
		$phpThumb = new phpThumb();
		$phpThumb->config_allow_src_above_docroot = true;   //per far funzionare le datafolder esterne alla documentroot
		$phpThumb->setSourceFilename($thumb);
		if ($width > 0) {
			$phpThumb->setParameter('w', $width);
		}
		if ($height > 0) {
			$phpThumb->setParameter('h', $height);
		}
		$phpThumb->setParameter('f', substr($thumb, -3, 3)); // set the output format
		$phpThumb->setParameter('far', 'C'); // scale outside
		$phpThumb->setParameter('bg', 'ffffff'); // background

		foreach ($_GET as $p => $v) {
			if ($p != 'url') {
				$phpThumb->setParameter($p, $v);
			}
		}

		//$phpThumb->setParameter('bg','FFFFFF'); // scale outside

		if (!$phpThumb->GenerateThumbnail()) {
			echo '<pre>';
			print_r($phpThumb->debugmessages);
			echo '</pre>';
			$this->error('cannot generate thumbnail');
		}
		//echo $thumb . '<br/>';
		// make the directory to put the image
		//echo 'thumbs/' . rtrim(ltrim(dirname($filename), '.'),'/') . '/' . $size . '/' . $filename;
		if (!$this->mkpath('thumbs/' . rtrim(ltrim(dirname($filename), '.'), '/') . '/' . $size . '/' . $filename, true)) {
			echo '<pre>';
			print_r($phpThumb->debugmessages);
			echo '</pre>';
			$this->error('Can not create the directory!');
		}

		$array_path = explode('/', $filename);
		$filename = array_pop($array_path);
		$path = implode('/', $array_path);
		$new_file = MEDIA_ROOT . 'thumbs/' . (($path != '') ? ($path . '/') : '') . $size . '/' . $filename;

		// write the file
		if (!$phpThumb->RenderToFile(DOCROOT . $new_file)) {
			echo '<pre>';
			print_r($phpThumb->debugmessages);
			echo '</pre>';
			$this->error('cannot save thumbnail');
		}

		// redirect to the thumb
		// note: you need the '?new' or IE wont do a redirect

		header('Location: ' . WEBROOT . $new_file . '?new');
	}

	public function cleanCache()
	{
		error_reporting(0);
		if (!UserAuth::checkLoginSession()) {
			Utils::RedirectTo(Utils::getComponentUrl('home'));
		}

		$path = DOCROOT . MEDIA_ROOT . 'thumbs';
		$this->cleanDirectory($path);

		if (count(scandir($path)) > 1) {
			$this->cleanCache();
		}
	}

	// basic error handling
	public function error($error)
	{
		header('HTTP/1.0 404 Not Found');
		echo '<h1>Not Found</h1>';
		echo '<p>The image you requested could not be found.</p>';
		echo "<p>An error was triggered: <b>$error</b></p>";
		exit();
	}

	//recursive dir function
	public function mkpath($path, $mode)
	{
		//echo "Check if is dir : " . DOCROOT . MEDIA_ROOT . dirname($path);
		//echo ' ... or create ' . DOCROOT . MEDIA_ROOT . dirname($path);
		is_dir(DOCROOT . MEDIA_ROOT . dirname($path)) || $this->mkpath(dirname($path), $mode);

		//        if(is_dir(DOCROOT . MEDIA_ROOT . dirname($path)))
		//                return true;
		//        else
		//            if(mkdir(DOCROOT . MEDIA_ROOT . dirname($path), 0777, $mode))
		//                    return true;
		//            else {
		//                echo 'FOLDER NOT CREATED!';
		//                return false;
		//            }

		return is_dir(DOCROOT . MEDIA_ROOT . dirname($path)) || @mkdir(DOCROOT . MEDIA_ROOT . dirname($path), 0777, $mode);
	}

	public function cleanDirectory($path)
	{
		$files = glob($path . '/*');
		if ($files !== false) {
			foreach ($files as $file) {
				if (is_dir($file)) {
					$isDirEmpty = (count(glob("$file/*")) === 0) ? true : false;
					if ($isDirEmpty) {
						rmdir($file);
					} else {
						$this->cleanDirectory($file);
					}
				} else {
					unlink($file);
				}
			}
		} else {
			rmdir($path);
		}
	}
}
