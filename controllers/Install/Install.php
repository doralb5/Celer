<?php

require_once LIBS_PATH . 'Helpers.php';

class Install extends BaseController
{
	private $simple_data = 'simple_001';

	public function __construct()
	{
		parent::__construct();
		if ($this->isInstalled()) {
			echo 'Installation not possible!';
			exit;
		}
	}

	public function InstallDataDir()
	{
		if (defined('DATA_DIR')) {
			if (!$this->makeDataDir(DATA_DIR)) {
				echo 'Setup Failed!';
				return false;
			}
		} else {
			echo 'DATA_DIR not defined!';
			return false;
		}

		$res = $this->installSimpleData($this->simple_data);

		if ($res) {
			echo 'Setup finished successfully!';
			Utils::RedirectTo('/');
			return true;
		}
		echo 'Something went wrong during installation of the main folder!';
		return false;
	}

	private function makeDataDir($name)
	{
		if (!is_dir(DOCROOT . $name)) {
			if (!mkdir(DOCROOT . $name)) {
				echo 'Error during creation of ' . DOCROOT . $name;
				return false;
			}
		}

		if (!is_dir(DOCROOT . MEDIA_ROOT)) {
			if (!mkdir(DOCROOT . MEDIA_ROOT)) {
				echo 'Error during creation of ' . DOCROOT . MEDIA_ROOT;
				return false;
			}
		}

		return true;
	}

	private function isInstalled()
	{
		if (is_dir(DOCROOT . DATA_DIR)) {
			return true;
		}
		return false;
	}

	public function installSimpleData($name)
	{
		if (is_dir(DOCROOT . 'data' . DS . $name)) {
			Helpers::CopyContent(DOCROOT . 'data' . DS . $name, DOCROOT . DATA_DIR);
			return true;
		}
	}
}
