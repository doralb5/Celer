<?php

class Maintenance extends WebPage
{
	public function index()
	{
		$this->view->renderTemplate(false);
		//$this->view->setLayout('');
		$this->view->render('maintenance');
	}
}
