<?php

require_once DOCROOT . LIBS_PATH . 'Breadcrumb_class.php';

class WebPage extends BaseController
{
	private $id;
	private $page;
	private $package;
	private $component;
	private $action;
	private $action_params;
	private static $parameters;
	private $layout;
	private static $page_request;
	public static $breadcrumb;
	private $error;
	private static $show_content_heading = true;
	private static $content_heading = '';

	public function __construct()
	{
		parent::__construct();
		self::$breadcrumb = new Breadcrumb_class();
	}

	public function render($request = '')
	{
		//echo 'Request is :' . $request;
		self::$page_request = $request;
		$this->routePage($request);

		//echo "run: ". $this->component . " / " . $this->action;

		$this->dispatchPage();
	}

	private function routePage($request = '')
	{
		$request = explode('/', $request);

		// @var $page Page
		if ($request[0] == '') {
			$page = $this->model->getHomePage($this->getLang());
		} else {
			$page = $this->model->getPageByAlias($request[0], $this->getLang());
			array_shift($request);
		}

		if (is_null($page)) {
			$this->error = 404;
			return;
		}

		$this->component = $page->getComponent();
		if (strpos($this->component, '/')) {
			$this->package = substr($this->component, 0, strpos($this->component, '/'));
			$this->component = substr($this->component, strpos($this->component, '/') + 1);
		} else {
			$this->package = $this->component;
		}
		$this->action = $page->getAction();
		$this->layout = $page->getLayout();

		if (count($page->getActionParams()) > 0) {
			$this->action_params = implode('/', $page->getActionParams());
		} else {
			$this->action_params = $request;
		}
		self::$parameters = $page->getParameters();

		//Set Page title.
		$meta_title = ($page->getMeta_title() != '') ? $page->getMeta_title() : $page->getName();
		HeadHTML::setTitleTag($meta_title . ' - ' . CMSSettings::$webdomain);

		HeadHTML::AddMeta('robots', $page->getRobots());
		HeadHTML::AddMeta('keywords', $page->getMeta_keywords());
		HeadHTML::AddMeta('description', $page->getMeta_description());

		self::$breadcrumb->setHomepage('Home');
		if ($page->isHome()) {
			self::$breadcrumb->clean();
		} else {
			if ($page->getContentHeading() != '') {
				self::$breadcrumb->addDir($page->getContentHeading(), Utils::genUrl($page->getAlias()));
			} else {
				self::$breadcrumb->addDir($page->getName(), Utils::genUrl($page->getAlias()));
			}
		}
		self::setContentHeading($page->getContentHeading());
		self::setShowContentHeading($page->showContentHeading());
		$this->page = $page;
		$this->id = $page->getId();
	}

	private function dispatchPage()
	{
		if (!is_null($this->error)) {
			$this->render_error($this->error);
			return;
		}
		$_SESSION['currentPage'] = $this->id;
		$this->setLang($this->getLang());
		$this->view->setViewPath(TEMPLATES_PATH . CMSSettings::$template . DS);
		$this->view->setIdPage($this->id);

		//HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl(str_replace('%2F', '/', urlencode(CMSSettings::$logo)), 960, 600, array('zc' => '0'), true));

		if ($this->component != '') {
			$c = Loader::loadComponent($this->component, $this->package);
			if ($this->action_params == array('')) {
				$this->action_params = array();
			}

			$c->setLang($this->getLang());
			$c->view->setIdPage($this->id);
			$c->setTemplate(CMSSettings::$template);
			$c->before_action($this->action, $this->action_params);

			ob_start();
			Loader::runController($c, $this->action, $this->action_params);
			$ComponentOutput = ob_get_contents();
			ob_end_clean();

			$this->view->setContent($ComponentOutput);
		} else {
			$this->view->setContent(BaseView::replaceWidgets($this->page->getContent(), $this->view->getTemplate()));
		}
		$this->view->setPositions($this->model->getPositions($this->id, $this->view->getTemplate()));

		// Setting Meta OpenGraph Properties if not are setted by any Component
		$meta = HeadHTML::$arr_meta_property;
		$meta_og = array();
		foreach ($meta as $m) {
			$meta_name = key($m);
			if (substr($meta_name, 0, 3) == 'og:') {
				$meta_og[$meta_name] = $m[$meta_name];
			}
		}
		if (!isset($meta_og['og:type'])) {
			HeadHTML::addMetaProperty('og:type', 'website');
		}
		if (!isset($meta_og['og:title'])) {
			HeadHTML::addMetaProperty('og:title', $this->page->getMeta_title());
		}
		if (!isset($meta_og['og:description'])) {
			HeadHTML::addMetaProperty('og:description', htmlentities($this->page->getMeta_description()));
		}
		if (!isset($meta_og['og:url'])) {
			HeadHTML::addMetaProperty('og:url', Utils::genUrl('', true));
		}

		//

		$this->view->render($this->layout);
	}

	public static function setContentHeading($text)
	{
		self::$content_heading = $text;
	}

	public static function getContentHeading()
	{
		return self::$content_heading;
	}

	private static function setShowContentHeading($show)
	{
		self::$show_content_heading = $show;
	}

	public static function showContentHeading()
	{
		return self::$show_content_heading;
	}

	public function render_error($num)
	{
		$this->view->renderTemplate(false);
		if ($num == 404) {
			$this->view->render('Error/404');
			//print_r($this->view);
		}
	}

	public static function getParameters()
	{
		return self::$parameters;
	}

	public static function getRequest()
	{
		return self::$page_request;
	}

	public static function currentUrl()
	{
		return Utils::genUrl(self::$page_request, true);
	}
}
