<?php

class WebPageView extends BaseView
{
	private static $contentHeading;
	private static $show_content_heading;

	public static function getContentHeading()
	{
		return self::$contentHeading;
	}

	public static function showContentHeading()
	{
		return self::$show_content_heading;
	}

	public static function setContentHeading($contentHeading)
	{
		self::$contentHeading = $contentHeading;
	}

	public static function setShowContentHeading($show_content_heading)
	{
		self::$show_content_heading = $show_content_heading;
	}
}
