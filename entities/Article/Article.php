<?php

require_once DOCROOT . ENTITIES_PATH . 'Article/ArticleCategory.php';
require_once DOCROOT . ENTITIES_PATH . 'Article/ArticleImage.php';
require_once DOCROOT . ENTITIES_PATH . 'Article/ArticleRole.php';
require_once DOCROOT . ENTITIES_PATH . 'Article/ArticleDetail.php';
require_once DOCROOT . ENTITIES_PATH . 'User/User.php';

class Article_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Article';
	const INDEX = 'id';
	const FK_ArticleCategory = 'Article.id_category';
	const FK_ArticleSection = 'Article.id_section';
	const FK_User = 'Article.id_user';

	public $id;
	public $image;
	public $id_category;
	public $id_section;
	public $id_user;
	public $author;
	public $creation_date;
	public $publish_date;
	public $expire_date;
	//public $tags;
	public $featured = '0';
	public $featured_until;
	public $featured_now;
	public $enabled = '1';
	public $show_title = '0';
	public $show_subtitle = '0';
	public $show_date = '0';
	public $show_author = '0';
	public $show_share = '0';
	public $show_comments = '0';
	public $user_fullname;
	public $category;
	public $section;
	//Details
	public $title;
	public $subtitle;
	public $content;
	public $lang;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'image' => 'image',
			'id_category' => 'id_category',
			'id_section' => 'id_section',
			'id_user' => 'id_user',
			'author' => 'author',
			'creation_date' => 'creation_date',
			'publish_date' => 'publish_date',
			'expire_date' => 'expire_date',
			//'tags' => 'tags',
			'featured' => 'featured',
			'featured_until' => 'featured_until',
			'enabled' => 'enabled',
			'show_title' => 'show_title',
			'show_subtitle' => 'show_subtitle',
			'show_date' => 'show_date',
			'show_author' => 'show_author',
			'show_share' => 'show_share',
			'show_comments' => 'show_comments',
		);
		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$Article_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$ArticleCategory_Table = $TABLE_PREFIX . ArticleCategory_Entity::TABLE_NAME;
		$ArticleSection_Table = $TABLE_PREFIX . ArticleSection_Entity::TABLE_NAME;
		$ArticleDetail_Table = $TABLE_PREFIX . ArticleDetail_Entity::TABLE_NAME;
		$User_Table = $TABLE_PREFIX . User_Entity::TABLE_NAME;
		$TagArticle_Table = $TABLE_PREFIX . 'TagArticle';

		$FK_ArticleCategory = $TABLE_PREFIX . self::FK_ArticleCategory;
		//        $FK_ArticleSection = $TABLE_PREFIX . ArticleCategory_Entity::FK_ArticleSection;
		$FK_ArticleSection = $TABLE_PREFIX . self::FK_ArticleSection;
		$FK_User = $TABLE_PREFIX . self::FK_User;
		$FK_ArticleDetail = $TABLE_PREFIX . ArticleDetail_Entity::FK_Article;
		$FK_TagArticle = "$TagArticle_Table.article_id";

		$fpdo = new FluentPDO($pdo);

		$join_category = $ArticleCategory_Table . ' ON ' . $ArticleCategory_Table . '.' . ArticleCategory_Entity::INDEX . ' = ' . $FK_ArticleCategory;
		$join_section = $ArticleSection_Table . ' ON ' . $ArticleSection_Table . '.' . ArticleSection_Entity::INDEX . ' = ' . $FK_ArticleSection;
		$join_user = $User_Table . ' ON ' . $User_Table . '.' . User_Entity::INDEX . ' = ' . $FK_User;
		$join_detail = $ArticleDetail_Table . ' ON ' . $Article_Table . '.' . self::INDEX . ' = ' . $FK_ArticleDetail;
		$join_tags = $TagArticle_Table . ' ON ' . $Article_Table . '.' . self::INDEX . ' = ' . $FK_TagArticle;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_category)
			->leftJoin($join_section)
			->leftJoin($join_user)
			->leftJoin($join_detail)
			->leftJoin($join_tags)
			->select("CASE WHEN(featured = '2' OR (featured = '1' AND featured_until > NOW())) THEN '1' ELSE '0' END AS featured_now")
			->select("{$TABLE_PREFIX}ArticleCategory.category")
			->select("{$TABLE_PREFIX}ArticleSection.section")
			->select("{$TABLE_PREFIX}ArticleDetail.title, {$TABLE_PREFIX}ArticleDetail.subtitle, {$TABLE_PREFIX}ArticleDetail.content, {$TABLE_PREFIX}ArticleDetail.lang")
			->select("CONCAT({$TABLE_PREFIX}User.firstname, ' ', {$TABLE_PREFIX}User.lastname) AS user_fullname");
		return $query;
	}
}
