<?php

require_once DOCROOT . ENTITIES_PATH . 'Article/ArticleSection.php';

class ArticleCategory_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ArticleCategory';
	const INDEX = 'id';
	const FK_ArticleSection = 'ArticleCategory.id_section';

	public $id;
	public $category;
	public $description;
	public $image;
	public $parent_id;
	public $id_section;
	//public $view;
	//public $section;
	public $url;
	public $total_articles;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'category' => 'category',
			'description' => 'description',
			'image' => 'image',
			'parent_id' => 'parent_id',
			'id_section' => 'id_section',
			'view' => 'view',
		);
	}

	//    public static function getSelectQueryObj($pdo)
//    {
//        $TABLE_PREFIX = self::$table_prefix;
//
//        $ArticleSection_Table = $TABLE_PREFIX . ArticleSection_Entity::TABLE_NAME;
//        $FK_ArticleSection = $TABLE_PREFIX . self::FK_ArticleSection;
//
//        $fpdo = new FluentPDO($pdo);
//
//        $join_section = $ArticleSection_Table . " ON " . $ArticleSection_Table . '.' . ArticleSection_Entity::INDEX . " = " . $FK_ArticleSection;
//
//        $query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
//            ->leftJoin($join_section)
//            ->select("{$TABLE_PREFIX}ArticleSection.section");
//        return $query;
//    }
}
