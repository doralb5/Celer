<?php

class ArticleComment_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ArticleComment';
	const INDEX = 'id';

	public $id;
	public $article_id;
	public $comment_id;
	public $content;
	public $user;
	public $creation_date;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'article_id' => 'article_id',
			'comment_id' => 'comment_id',
			'content' => 'content',
			'user' => 'user',
			'creation_date' => 'creation_date',
		);
		$this->creation_date = date('Y-m-d H:i:s');
	}
}
