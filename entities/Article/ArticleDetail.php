<?php

class ArticleDetail_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ArticleDetail';
	const INDEX = 'id';
	const FK_Article = 'ArticleDetail.id_article';

	public $id;
	public $id_article;
	public $title;
	public $subtitle;
	public $content;
	public $lang;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_article' => 'id_article',
			'title' => 'title',
			'subtitle' => 'subtitle',
			'content' => 'content',
			'lang' => 'lang',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$Article_Table = $TABLE_PREFIX . Article_Entity::TABLE_NAME;
		$FK_Article = $TABLE_PREFIX . self::FK_Article;

		$fpdo = new FluentPDO($pdo);
		$join_article = $Article_Table . ' ON ' . $Article_Table . '.' . Article_Entity::INDEX . ' = ' . $FK_Article;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_article);
		return $query;
	}
}
