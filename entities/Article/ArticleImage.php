<?php

class ArticleImage_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ArticleImage';
	const INDEX = 'id';
	const FK_Article = 'ArticleImage.id_article';

	public $id;
	public $id_article;
	public $image;
	public $size;
	public $id_category;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_article' => 'id_article',
			'image' => 'image',
			'size' => 'size',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$Article_Table = $TABLE_PREFIX . Article_Entity::TABLE_NAME;
		$FK_Article = $TABLE_PREFIX . self::FK_Article;

		$fpdo = new FluentPDO($pdo);
		$join_article = $Article_Table . ' ON ' . $Article_Table . '.' . Article_Entity::INDEX . ' = ' . $FK_Article;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_article)
			->select("{$TABLE_PREFIX}Article.id_category");
		return $query;
	}
}
