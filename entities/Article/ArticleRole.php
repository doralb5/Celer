<?php

class ArticleRole_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ArticleRole';
	const INDEX = 'id';
	const FK_User = 'ArticleRole.id_user';

	public $id;
	public $id_user;
	public $role = 'author';
	public $user_fullname;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$logged_user = UserAuth::getLoginSession();
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_user' => 'id_user',
			'role' => 'role',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$User_Table = $TABLE_PREFIX . User_Entity::TABLE_NAME;
		$FK_User = $TABLE_PREFIX . self::FK_User;

		$fpdo = new FluentPDO($pdo);
		$join_user = $User_Table . ' ON ' . $User_Table . '.' . User_Entity::INDEX . ' = ' . $FK_User;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_user)
			->select("CONCAT({$TABLE_PREFIX}User.firstname, ' ', {$TABLE_PREFIX}User.lastname) AS user_fullname");
		return $query;
	}
}
