<?php

class ArticleSection_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ArticleSection';
	const INDEX = 'id';

	public $id;
	public $section;
	public $view;

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
