<?php

require_once DOCROOT . ENTITIES_PATH . 'Banner/BannerCategory.php';

class Banner_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Banner';
	const INDEX = 'id';
	const FK_BannerCategory = 'Banner.id_category';

	public $id;
	public $name;
	public $filename;
	public $target_url;
	public $id_category;
	public $publish_date;
	public $expire_date;
	public $click_counter;
	public $enabled = 1;
	public $text1;
	public $text2;
	public $text3;
	public $category;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'filename' => 'filename',
			'target_url' => 'target_url',
			'id_category' => 'id_category',
			'publish_date' => 'publish_date',
			'expire_date' => 'expire_date',
			'click_counter' => 'click_counter',
			'enabled' => 'enabled',
			'text1' => 'text1',
			'text2' => 'text2',
			'text3' => 'text3',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$BannerCategory_Table = $TABLE_PREFIX . BannerCategory_Entity::TABLE_NAME;
		$FK_BannerCategory = $TABLE_PREFIX . self::FK_BannerCategory;

		$fpdo = new FluentPDO($pdo);

		$join_category = $BannerCategory_Table . ' ON ' . $BannerCategory_Table . '.' . BannerCategory_Entity::INDEX . ' = ' . $FK_BannerCategory;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_category)
			->select("{$TABLE_PREFIX}BannerCategory.category");
		return $query;
	}
}
