<?php

require_once DOCROOT . ENTITIES_PATH . 'Banner/Banner.php';

class BannerCategory_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BannerCategory';
	const INDEX = 'id';

	public $id;
	public $category;

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
