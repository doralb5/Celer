<?php

class ProdCategory_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ProdCategory';
	const INDEX = 'id';

	public $id;
	public $category;
	public $description;
	public $image;
	public $id_parent;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'category' => 'category',
			'description' => 'description',
			'image' => 'image',
			'id_parent' => 'id_parent',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
