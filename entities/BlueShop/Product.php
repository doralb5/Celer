<?php

require_once DOCROOT . ENTITIES_PATH . 'BlueShop/ProductCategory.php';
require_once DOCROOT . ENTITIES_PATH . 'BlueShop/ProductImage.php';

class Product_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'prodotti';
	const INDEX = 'id';

	public $id;
	public $codice;
	public $titolo_it;
	public $titolo_en;
	public $titolo_fr;
	public $titolo_pr;
	public $descrizione_html_it;
	public $descrizione_html_en;
	public $descrizione_html_fr;
	public $descrizione_txt_it;
	public $descrizione_txt_en;
	public $descrizione_txt_fr;
	public $descrizione_txt_pr;
	public $descrizione_html_pr;
	public $note;
	public $id_categoria;
	public $id_marca;
	public $offerta = '0';
	public $id_disponibilita;
	public $novita = '0';
	public $primo_piano = '0';
	public $quantita_disponibile = '0';
	public $peso;
	public $volume = '0.00000';
	public $iva;
	public $id_unita_misura;
	public $prezzi_ivati;
	public $listino1;
	public $listino2;
	public $listino3;
	public $listino4;
	public $listino5;
	public $listino6;
	public $listino7;
	public $listino8;
	public $listino9;
	public $listino10;
	public $listino1_sc = '0.00000';
	public $listino2_sc = '0.00000';
	public $listino3_sc = '0.00000';
	public $listino4_sc = '0.00000';
	public $listino5_sc = '0.00000';
	public $listino6_sc = '0.00000';
	public $listino7_sc = '0.00000';
	public $listino8_sc = '0.00000';
	public $listino9_sc = '0.00000';
	public $listino10_sc = '0.00000';
	public $listino1_conf_min = '0';
	public $listino2_conf_min = '0';
	public $listino3_conf_min = '0';
	public $listino4_conf_min = '0';
	public $listino5_conf_min = '0';
	public $listino6_conf_min = '0';
	public $listino7_conf_min = '0';
	public $listino8_conf_min = '0';
	public $listino9_conf_min = '0';
	public $listino10_conf_min = '0';
	public $visibile = '1';
	public $tipo_vis_varianti = '0';
	public $prezzo_variabile = '0';
	public $tag_description_it;
	public $tag_description_en;
	public $tag_description_fr;
	public $tag_description_pr;
	public $tag_keywords_it;
	public $tag_keywords_en;
	public $tag_keywords_fr;
	public $tag_keywords_pr;
	public $ext_key_id = '0';
	public $application_id = '0';
	public $tag_prodotto;
	public $id_master = '0';
	public $serie = '0';
	public $sorting = '100';
	public $csv_code;
	//additional

	public $img_id;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'codice' => 'codice',
			'titolo_it' => 'titolo_it',
			'titolo_en' => 'titolo_en',
			'titolo_fr' => 'titolo_fr',
			'titolo_pr' => 'titolo_pr',
			'descrizione_html_it' => 'descrizione_html_it',
			'descrizione_html_en' => 'descrizione_html_en',
			'descrizione_html_fr' => 'descrizione_html_fr',
			'descrizione_txt_it' => 'descrizione_txt_it',
			'descrizione_txt_en' => 'descrizione_txt_en',
			'descrizione_txt_fr' => 'descrizione_txt_fr',
			'descrizione_txt_pr' => 'descrizione_txt_pr',
			'descrizione_html_pr' => 'descrizione_html_pr',
			'note' => 'note',
			'id_categoria' => 'id_categoria',
			'id_marca' => 'id_marca',
			'offerta' => 'offerta',
			'id_disponibilita' => 'id_disponibilita',
			'novita' => 'novita',
			'primo_piano' => 'primo_piano',
			'quantita_disponibile' => 'quantita_disponibile',
			'peso' => 'peso',
			'volume' => 'volume',
			'iva' => 'iva',
			'id_unita_misura' => 'id_unita_misura',
			'prezzi_ivati' => 'prezzi_ivati',
			'listino1' => 'listino1',
			'listino2' => 'listino2',
			'listino3' => 'listino3',
			'listino4' => 'listino4',
			'listino5' => 'listino5',
			'listino6' => 'listino6',
			'listino7' => 'listino7',
			'listino8' => 'listino8',
			'listino9' => 'listino9',
			'listino10' => 'listino10',
			'listino1_sc' => 'listino1_sc',
			'listino2_sc' => 'listino2_sc',
			'listino3_sc' => 'listino3_sc',
			'listino4_sc' => 'listino4_sc',
			'listino5_sc' => 'listino5_sc',
			'listino6_sc' => 'listino6_sc',
			'listino7_sc' => 'listino7_sc',
			'listino8_sc' => 'listino8_sc',
			'listino9_sc' => 'listino9_sc',
			'listino10_sc' => 'listino10_sc',
			'listino1_conf_min' => 'listino1_conf_min',
			'listino2_conf_min' => 'listino2_conf_min',
			'listino3_conf_min' => 'listino3_conf_min',
			'listino4_conf_min' => 'listino4_conf_min',
			'listino5_conf_min' => 'listino5_conf_min',
			'listino6_conf_min' => 'listino6_conf_min',
			'listino7_conf_min' => 'listino7_conf_min',
			'listino8_conf_min' => 'listino8_conf_min',
			'listino9_conf_min' => 'listino9_conf_min',
			'listino10_conf_min' => 'listino10_conf_min',
			'visibile' => 'visibile',
			'tipo_vis_varianti' => 'tipo_vis_varianti',
			'prezzo_variabile' => 'prezzo_variabile',
			'tag_description_it' => 'tag_description_it',
			'tag_description_en' => 'tag_description_en',
			'tag_description_fr' => 'tag_description_fr',
			'tag_description_pr' => 'tag_description_pr',
			'tag_keywords_it' => 'tag_keywords_it',
			'tag_keywords_en' => 'tag_keywords_en',
			'tag_keywords_fr' => 'tag_keywords_fr',
			'tag_keywords_pr' => 'tag_keywords_pr',
			'ext_key_id' => 'ext_key_id',
			'application_id' => 'application_id',
			'tag_prodotto' => 'tag_prodotto',
			'id_master' => 'id_master',
			'serie' => 'serie',
			'sorting' => 'sorting',
			'csv_code' => 'csv_code',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$join_image = 'img_prodotti on prodotti.id = img_prodotti.id_prodotto';
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from(self::TABLE_NAME)
			->leftJoin($join_image)
			->select('img_prodotti.id as img_id');
		'<!--' . $query->getQuery() . '-->';
		return $query;
	}
}
