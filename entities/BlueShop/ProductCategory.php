<?php

class ProductCategory_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'categorie';
	const INDEX = 'id';

	public $id;
	public $categoria_it;
	public $categoria_en;
	public $categoria_fr;
	public $categoria_pr;
	public $id_top_cat;
	public $img_mini;
	public $img_big;
	public $reversed;
	public $ordine;
	public $abilitata;
	public $descrizione_it;
	public $descrizione_en;
	public $descrizione_fr;
	public $descrizione_pr;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'codice' => 'codice',
			'categoria_it' => 'categoria_en',
			'categoria_en' => 'categoria_en',
			'categoria_fr' => 'categoria_fr',
			'categoria_pr' => 'categoria_pr',
			'id_top_cat' => 'id_top_cat',
			'img_mini' => 'img_mini',
			'img_big' => 'img_big',
			'reversed' => 'reversed',
			'ordine' => 'ordine',
			'abilitata' => 'abilitata',
			'descrizione_it' => 'descrizione_it',
			'descrizione_en' => 'descrizione_en',
			'descrizione_fr' => 'descrizione_fr',
			'descrizione_pr' => 'descrizione_pr',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from(self::TABLE_NAME);
		return $query;
	}
}
