<?php

class ProductImage_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'img_prodotti';
	const INDEX = 'id';

	public $id;
	public $id_prodotto;
	public $img_big;
	public $img_mini;
	public $sorting;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'id_prodotto' => 'id_prodotto',
			'img_big' => 'img_big',
			'img_mini' => 'img_mini',
			'sorting' => 'sorting',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from(self::TABLE_NAME)
			->disableSmartJoin();
		return $query;
	}
}
