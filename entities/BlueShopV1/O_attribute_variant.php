<?php

class O_attribute_variant
{
	public $id;
	public $id_varianteprodotto;
	public $id_attributo;
	public $valore;
	public $nome;
	public $background;
	private static $pre_query = "SELECT va.*,
                                    CASE WHEN (a.nome:user_lang != '') THEN a.nome:user_lang ELSE a.nome:default_lang END AS nome
                                FROM varianti_attributi AS va
                                INNER JOIN tab_attributi AS a ON a.id=va.id_attributo";
	private static $attributes = array(
		':id' => 'va.id',
		':id_varianteprodotto' => 'va.id_varianteprodotto',
		':id_attributo' => 'va.id_attributo',
		':valore' => 'va.valore',
		':nome' => 'nome',
		':background' => 'background'
	);

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function __construct()
	{
	}
}
