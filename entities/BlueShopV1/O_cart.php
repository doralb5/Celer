<?php

class O_cart
{
	public $id;
	public $id_cliente;
	public $id_sessione;
	public $qta;
	public $spese_spedizione;
	public $coupon_code;
	public $discount;
	public $imponibile;
	public $imposta;
	public $totale;
	public $details = array();
	public $payment_methods;
	public $shipping_method;
	public $peso_totale;
	public $TABLE_NAME = 'ordini';
	private $em;

	/*
	  private static $pre_query = "SELECT o.id as id, o.id_cliente, o.id_sessione,
	  ROUND (SUM(do.quantita_ar) ,2) AS qta,
	  ROUND (o.spese_spedizione ,2) AS spese_spedizione,
	  ROUND (SUM(do.prezzo_ar*quantita_ar) ,2) AS imponibile,
	  ROUND ( SUM((do.prezzo_ar*quantita_ar*do.iva_ar)/100) ,2) AS imposta,
	  ROUND ( ( SUM(do.prezzo_ar*quantita_ar) + SUM((do.prezzo_ar*quantita_ar*do.iva_ar)/100)) ,2) AS totale,
	  (SELECT ROUND(SUM(peso_ar*quantita_ar),2)
	  FROM dettagli_ordine
	  INNER JOIN ordini ON dettagli_ordine.id_ordine=ordini.id
	  WHERE ordini.id=o.id) AS peso_totale
	  FROM ordini AS o
	  LEFT JOIN  dettagli_ordine AS do ON o.id=do.id_ordine";
	 */

	/*
	  private static $attributes = array(

	  ':id'=>'o.id',
	  ':id_cliente' => "o.id_cliente",
	  ':id_sessione' => "o.id_sessione",
	  ':qta' => "ROUND (SUM(do.quantita_ar) ,2)",
	  ':spese_spedizione' => "ROUND (o.spese_spedizione ,2)",
	  ':imponibile' => "ROUND (SUM(do.prezzo_ar*quantita_ar) ,2)",
	  ':imposta' => "ROUND ( SUM((do.prezzo_ar*quantita_ar*do.iva_ar)/100) ,2)",
	  ':totale' => "ROUND ( ( SUM(do.prezzo_ar*quantita_ar) + SUM((do.prezzo_ar*quantita_ar*do.iva_ar)/100)) ,2)",
	  ':peso_totale'=>"(SELECT ROUND(SUM(peso_ar*quantita_ar),2)
	  FROM dettagli_ordine
	  INNER JOIN ordini ON dettagli_ordine.id_ordine=ordini.id
	  WHERE ordini.id=o.id) ",
	  );

	 */
	private static $pre_query = 'SELECT o.id as id, o.id_cliente, o.id_sessione,
                                    ROUND (SUM(do.quantita_ar) ,2) AS qta,
                                    0 AS spese_spedizione,
                                    ROUND (SUM(do.prezzo_ar*quantita_ar) ,2) AS imponibile,
                                    ROUND ( SUM((do.prezzo_ar*quantita_ar*do.iva_ar)/100) ,2) AS imposta,
                                    ROUND (
                                        ( SUM(do.prezzo_ar*quantita_ar) + SUM((do.prezzo_ar*quantita_ar*do.iva_ar)/100))
                                        - o.discount
                                    ,2) AS totale,
                                    (SELECT ROUND(SUM(peso_ar*quantita_ar),2)
                                                FROM dettagli_ordine
                                               INNER JOIN ordini ON dettagli_ordine.id_ordine=ordini.id
                                               WHERE ordini.id=o.id) AS peso_totale,
                                    o.discount,
                                    o.coupon_code
                                FROM ordini AS o
                                LEFT JOIN  dettagli_ordine AS do ON o.id=do.id_ordine';
	private static $attributes = array(
		':id' => 'o.id',
		':id_cliente' => 'o.id_cliente',
		':id_sessione' => 'o.id_sessione',
		':qta' => 'ROUND (SUM(do.quantita_ar) ,2)',
		':spese_spedizione' => 0,
		':discount' => 'o.discount',
		':coupon_code' => 'o.coupon_code',
		':imponibile' => 'ROUND (SUM(do.prezzo_ar*quantita_ar) ,2)',
		':imposta' => 'ROUND ( SUM((do.prezzo_ar*quantita_ar*do.iva_ar)/100) ,2)',
		':totale' => 'ROUND (
                                        ( SUM(do.prezzo_ar*quantita_ar) + SUM((do.prezzo_ar*quantita_ar*do.iva_ar)/100))
                                        - o.discount
                                    ,2)',
		':peso_totale' => '(SELECT ROUND(SUM(peso_ar*quantita_ar),2)
                                                            FROM dettagli_ordine
                                                           INNER JOIN ordini ON dettagli_ordine.id_ordine=ordini.id
                                                           WHERE ordini.id=o.id) ',
	);

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function __construct()
	{
		$this->em = ORM::getInstance();
	}

	/**
	 * Ritorna i dettagli del carrello
	 * @return O_order_details
	 */
	public function getDetails()
	{
		$this->em->cleanAll();
		/*     if ($this->id_cliente != '') {
		  $this->em->setWhere("  :id_cliente='$this->id_cliente' AND numero IS NULL");
		  } else {
		  $this->em->setWhere("  :id_sessione='$this->id_sessione' AND o.id_cliente IS NULL AND numero IS NULL");
		  } */
		$this->em->setWhere(" :id_ordine='{$this->id}'");
		$this->em->setWhere(" :id_ordine='{$this->id}'");

		// $this->em->setGroup_by(" :id_ordine");
		$this->em->setRepository('O_order_details');
		$this->details = $this->em->find();
		return $this->details;
	}

	/**
	 * Inserisce l'oggetto dettaglio al db iserendo l'id dell'ordine e poi riaggiorna l'oggetto cart
	 * @param obj O_order_details $detail
	 */
	public function addDetail($detail)
	{
		$detail->id_ordine = $this->id;
		$detail->setNew();

		return $this->em->save($detail);
		//$this->update();
	}

	/**
	 * Elimina l'oggetto dettaglio dall'ordine, elimandolo dal db, e poi riaggiorna l'oggetto cart
	 * @param int $id_detail
	 */
	public function deleteDetail($id_detail)
	{
		$this->em->cleanAll();
		$this->em->setWhere(" :id='{$id_detail}'");
		$this->em->setLimit(1);
		$this->em->setRepository('O_order_details');
		$detail = $this->em->find();

		$this->em->cleanAll();
		return $this->em->delete($detail[0]);
	}

	/**
	 * Modifica la quantità dell'oggetto dettaglio di questo carrello e fa un update sul db, infine riaggiorna l'oggetto cart
	 * @param int $id_detail
	 * @param int $quantita
	 */
	public function changeAmountDetail($id_detail, $quantita)
	{
		$this->em->cleanAll();
		$this->em->setWhere(" :id='{$id_detail}'");
		$this->em->setLimit(1);
		$this->em->setRepository('O_order_details');
		$detail = $this->em->find();

		$detail[0]->quantita = $quantita;
		$detail[0]->setNew();

		return $this->em->save($detail[0]);
	}

	/**
	 * Aggiorna l'oggetto cart
	 * @return boolean
	 */
	public function update()
	{
		/* $this->em->cleanAll();
		  $this->em->setWhere(" :id='{$this->id}'");
		  $this->em->setLimit(1); */
		$outcome = $this->em->updatesObj($this);
		if ($outcome) {
			$this->getDetails();
			return true;
		}
		return false;
	}

	/**
	 * Return payment_method se l'id del metodo di pagamento non è settato nell'oggeto restituisce tutti i metodi di pagamento attivi
	 * @return O_payment_method
	 */
	public function getPayment_methods()
	{
		$this->em->cleanAll();

		$this->em->setWhere(" attiva='1'");
		$this->em->setOrder_by(' :id');

		$this->em->setRepository('O_payment_method');
		$this->payment_methods = $this->em->find();
		return $this->payment_methods;
	}
}
