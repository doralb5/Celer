<?php

/**
 *
 * @author Antonio Rotundo
 */
class O_listini
{
	public $id;
	public $descrizione;
	public $campo_prodotto;
	public $attivo;
	public $prezzi_ivati;
	public $ordine_min = 0;
	private static $attributes = array(
		':id' => 'id',
		':descrizione' => 'descrizione',
		':campo_prodotto' => 'campo_prodotto',
		':attivo' => 'attivo',
		':prezzi_ivati' => 'prezzi_ivati',
		':ordine_min' => 'ordine_min',
	);
	private static $pre_query = 'SELECT * FROM listini';

	public function __construct($row = '')
	{
		if (is_array($row)) {
			parent::_loadByRow($row);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":id='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}
}
