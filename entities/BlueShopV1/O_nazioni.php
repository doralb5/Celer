<?php

/**
 * Description of T_category
 *
 * @author Antonio Mazza
 */
class O_nazioni
{
	public $id;
	public $suffisso;
	public $stato;
	public $UE;
	public $attiva;
	private static $attributes = array(
		':id' => 'id',
		':suffisso' => 'suffisso',
		':stato' => 'stato',
		':UE' => 'UE',
		':attiva' => 'attiva'
	);
	private static $pre_query = 'SELECT id, suffisso, stato, UE, attiva 
                                 FROM nazioni';

	public function __construct($row = '')
	{
		if (is_array($row)) {
			parent::_loadByRow($row);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":id='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}
}
