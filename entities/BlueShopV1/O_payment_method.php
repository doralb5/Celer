<?php

class O_payment_method
{
	public $id;
	public $descrizione;
	public $valore;
	public $istruzioni;
	public $attiva;
	public $costo_aggiuntivo;
	private static $pre_query = 'SELECT id,descrizione,valore,istruzioni,attiva,costo_aggiuntivo
                                 FROM modalita_pagamento ';
	private static $attributes = array(
		':id' => 'id',
		':descrizione' => 'descrizione',
		':valore' => 'valore',
		':istruzioni' => 'istruzioni',
		':attiva' => 'attiva',
		':costo_aggiuntivo' => 'costo_aggiuntivo'
	);

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function __construct()
	{
	}
}
