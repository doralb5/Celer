<?php

/**
 * Description of T_product
 *
 * @author alessandro
 */
class O_product
{
	private $em;
	public $id;
	public $codice;
	public $titolo;
	public $descrizione;
	public $descrizione_html;
	public $metadescription;
	public $metakeywords;
	public $note;
	public $id_categoria;
	public $categoria;
	public $iva;
	public $id_unita_misura;
	public $marca;
	public $marca_img = '0';
	public $id_marca;
	public $novita;
	public $primo_piano;
	public $quantita_disponibile;
	public $conf_min;
	public $multiple;
	public $disponibilita;
	public $serie;
	public $sorting = 100;
	public $prezzo_listino;
	public $prezzo_vendita;
	public $prezzo_listino_raw; //Importo non arrotondato, per come è scritto sul db
	public $prezzo_vendita_raw; //Importo non arrotondato, per come è scritto sul db
	public $prezzo_listino_ivato;
	public $prezzo_vendita_ivato;
	public $risparmio;
	public $risparmio_ivato;
	public $sconto;
	public $listino_variante_min;
	public $listino_variante_min_ivato;
	public $prezzo_variante_min_ivato;
	public $prezzo_variante_min;
	public $withphoto;
	public $prezzi_ivati;
	public $offerta;
	public $img;
	//public $img_big;
	//public $img_mini;
	public $count_img;
	public $titolo_it;
	public $titolo_en;
	public $titolo_fr;
	public $titolo_pr;
	public $descrizione_html_it;
	public $descrizione_html_en;
	public $descrizione_html_fr;
	public $descrizione_html_pr;
	public $descrizione_txt_it;
	public $descrizione_txt_en;
	public $descrizione_txt_fr;
	public $descrizione_txt_pr;
	public $prezzo_variante_max;
	public $prezzo_variante_max_ivato;
	public $peso;
	public $listino1;
	public $listino2;
	public $listino3;
	public $listino4;
	public $listino5;
	public $listino6;
	public $listino7;
	public $listino8;
	public $listino9;
	public $listino10;
	public $listino1_sc;
	public $listino2_sc;
	public $listino3_sc;
	public $listino4_sc;
	public $listino5_sc;
	public $listino6_sc;
	public $listino7_sc;
	public $listino8_sc;
	public $listino9_sc;
	public $listino10_sc;
	public $listino1_conf_min;
	public $listino2_conf_min;
	public $listino3_conf_min;
	public $listino4_conf_min;
	public $listino5_conf_min;
	public $listino6_conf_min;
	public $listino7_conf_min;
	public $listino8_conf_min;
	public $listino9_conf_min;
	public $listino10_conf_min;
	public $visibile;
	public $tipo_vis_varianti;
	public $prezzo_variabile;
	public $tag_description_it;
	public $tag_description_en;
	public $tag_description_fr;
	public $tag_description_pr;
	public $tag_keywords_it;
	public $tag_keywords_en;
	public $tag_keywords_fr;
	public $tag_keywords_pr;
	public $tag_prodotto;
	public $id_disponibilita;
	public $id_variante_def;
	// ARRAY DI OGGETTI/OGGETTO CORRELATI
	public $imgs;
	public $related_product;
	public $variant_product;
	public $car_product;
	public $new = array();

	/**
	 * @return O_variant_product
	 */
	/*
	  public function getVariant_product(){

	  $this->em->cleanAll();
	  $this->em->setWhere(" :id_prodotto= '{$this->id}'");
	  $this->em->setOrder_by(" l:nlistino, :codice ");
	  $this->em->setRepository("O_variant_product");
	  $this->variant_product=$this->em->find();
	  return $this->variant_product;

	  }
	 */

	public function geVariantById($id_variante)
	{
		$this->em->cleanAll();
		$this->em->setWhere(" :id='{$id_variante}'");
		$this->em->setRepository('O_variant_product');
		return $this->em->find();
	}

	public function geAttribite_variant($id_variante)
	{
		$this->em->cleanAll();
		$this->em->setWhere(" :id_varianteprodotto='{$id_variante}'");
		$this->em->setRepository('O_attribute_variant');
		return $this->em->find();
	}

	public function getVariant_product($array_attributes = null)
	{
		$this->em->cleanAll();
		if (is_null($array_attributes)) {
			$this->em->setWhere(" :id_prodotto= '{$this->id}'");
		} else {
			$where = " :id_prodotto= '{$this->id}' AND :id IN ";
			$subquery = '(SELECT DISTINCT id_varianteprodotto AS id FROM varianti_attributi WHERE ';

			$attribute_names = array_keys($array_attributes);

			/*
			 * ripulisco l'array degli attributi da eventuali attributi vuoti
			 * la query fallisce quando un attributo ha valore = ""
			 */

			for ($i = 0; $i < count($attribute_names); $i++) {
				if ($array_attributes[$attribute_names[$i]] == '') {
					unset($array_attributes[$attribute_names[$i]]);
				}
			}
			$attribute_names = array_keys($array_attributes);
			if (count($attribute_names) > 0) {
				/*
				  $subquery.=" id_attributo= '".addslashes($attribute_names[0])."'  AND valore = '".addslashes($array_attributes[$attribute_names[0]])."' ";
				  for($i=1;$i<count($attribute_names);$i++)
				  {

				  $subquery.=" OR id_attributo= '".addslashes($attribute_names[$i])."'  AND valore = '".addslashes($array_attributes[$attribute_names[$i]])."' ";

				  }
				 *
				 */

				$subquery .= " id_attributo= '" . addslashes($attribute_names[0]) . "'";
				for ($i = 1; $i < count($attribute_names); $i++) {
					$subquery .= " OR id_attributo= '" . addslashes($attribute_names[$i]) . "'  ";
				}
				$where .= $subquery . ')';
				$this->em->setWhere($where);
			}
		}
		$this->em->setOrder_by(' l:nlistino, :codice ');
		$this->em->setRepository('O_variant_product');
		$this->variant_product = $this->em->find();
		return $this->variant_product;
	}

	/**
	 * @return O_img_product
	 */
	public function getImagesId()
	{
		$this->em->cleanAll();

		$q = "SELECT id FROM img_prodotti WHERE id_prodotto = '" . $this->id . "' ORDER BY sorting";

		$this->imgs = $this->em->findByQuery($q);
		return $this->imgs;
	}

	/**
	 * @return O_img_product
	 */
	public function getImages()
	{
		$this->em->cleanAll();
		$this->em->setRepository('O_img_product');
		$q = "SELECT id, filename FROM img_prodotti WHERE id_prodotto = '" . $this->id . "' ORDER BY sorting";
		$this->imgs = $this->em->findByQuery($q);
		return $this->imgs;
	}

	public function getCaratteristiche()
	{
		$this->em->cleanAll();

		$q = 'SELECT
              CASE WHEN (t.nome' . $this->em->getUser_lang() . "!='') THEN t.nome" . $this->em->getUser_lang() . ' ELSE t.nome' . $this->em->getDefault_lang() . ' END AS nome,
              CASE WHEN (c.valore' . $this->em->getUser_lang() . "!='') THEN c.valore" . $this->em->getUser_lang() . ' ELSE c.valore' . $this->em->getDefault_lang() . ' END AS valore
              FROM tab_caratteristiche t INNER JOIN caratteristiche_prod c ON t.id=c.id_caratteristica
              WHERE c.id_prodotto = ' . $this->id . '';

		$this->car_product = $this->em->findByQuery($q);
		return $this->car_product;
	}

	/**
	 * @return O_related_product
	 */
	public function getRelated_product()
	{
		$this->em->cleanAll();
		$this->em->setWhere(" :id_prodotto= '{$this->id}'");
		$this->em->setOrder_by(':sorting, :titolo');
		$this->em->setRepository('O_related_product');
		$this->related_product = $this->em->find();
		return $this->related_product;
	}

	private static $attributes = array(
		':id' => 'p.id',
		':codice' => 'p.codice',
		':titolo' => 'titolo_it',
		':descrizione' => 'descrizione',
		':descrizione_html' => 'descrizione_html',
		':metadescription' => 'metadescription',
		':metakeywords' => 'metakeywords',
		':note' => 'p.note',
		':serie' => 'serie',
		':sorting' => 'sorting',
		':id_categoria' => 'p.id_categoria',
		':categoria' => 'categoria',
		':ordine_cat' => 'c.ordine',
		':iva' => 'p.iva',
		':id_marca' => 'p.id_marca',
		':id_unita_misura' => 'p.id_unita_di_misura',
		':marca' => 'm.marca',
		':marca_img' => 'CASE WHEN(m.img_big NOT IS NULL) THEN 1 ELSE 0 END',
		':offerta' => 'p.offerta',
		':id_disponibilita' => 'p.id_disponibilita',
		':disponibilita' => 'd.descrizione',
		':novita' => 'p.novita',
		':primo_piano' => 'p.primo_piano',
		':quantita_disponibile' => 'p.quantita_disponibile',
		':peso' => 'p.peso',
		':conf_min' => 'p.listino:nlistino_conf_min',
		':prezzo_listino' => 'ROUND(p.listino:nlistino,2)',
		':prezzo_vendita' => 'ROUND(p.listino:nlistino_sc,2)',
		':prezzo_listino_raw' => 'p.listino:nlistino',
		':prezzo_vendita_raw' => 'p.listino:nlistino_sc',
		':prezzo_listino_ivato' => 'ROUND(p.listino:nlistino + (p.listino:nlistino * p.iva / 100) ,2)',
		':prezzo_vendita_ivato' => 'ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100) ,2)',
		':risparmio' => 'ROUND( (p.listino:nlistino - p.listino:nlistino_sc) ,2)',
		':risparmio_ivato' => 'ROUND( (p.listino:nlistino - p.listino:nlistino_sc) + ((p.listino:nlistino - p.listino:nlistino_sc) * p.iva / 100) ,2)',
		':prezzo_variante_min' => '(SELECT ROUND(MIN(vp.l:nlistino_sc),2) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)',
		':sconto' => 'CASE WHEN (p.listino:nlistino_sc=p.listino:nlistino) THEN ROUND(0,2) ELSE ROUND(100-(listino:nlistino_sc * 100)/listino:nlistino,2)',
		':count_img' => 'count(DISTINCT(img.id))',
		':listino_variante_min_ivato' => '(SELECT ROUND( (vp.l:nlistino + (vp.l:nlistino * p.iva / 100)) , 2) AS plistino
                                                                FROM varianti_prodotti vp
                                                                INNER JOIN (SELECT vp2.id, MIN(vp2.l:nlistino_sc) FROM varianti_prodotti vp2 GROUP BY vp2.id) AS jvar ON vp.id = jvar.id 
                                                                WHERE vp.id_prodotto = p.id LIMIT 1)',
		':prezzo_variante_min_ivato' => '(SELECT ROUND( MIN(vp.l:nlistino_sc) + ( MIN(vp.l:nlistino_sc) * p.iva / 100 ), 2 ) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)',
		':prezzo_variante_max' => '(SELECT ROUND( MAX(vp.l:nlistino_sc), 2 ) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)',
		':prezzo_variante_max_ivato' => '(SELECT ROUND( MAX(vp.l:nlistino_sc) + ( MAX(vp.l:nlistino_sc) * p.iva / 100 ), 2 ) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)',
		':id_variante_def' => '(SELECT vp.id FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id AND vp.l:nlistino_sc IN  (SELECT MIN(vp.l:nlistino_sc) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id) LIMIT 1)'
	);
	private static $pre_query = "SELECT
    p.id,
    p.codice,
    CASE
        WHEN (p.titolo:user_lang != '') THEN p.titolo:user_lang
        ELSE p.titolo:default_lang
    END AS titolo,
    CASE
        WHEN (p.descrizione_txt:user_lang != '') THEN p.descrizione_txt:user_lang
        ELSE p.descrizione_txt:default_lang
    END AS descrizione,
    CASE
        WHEN (p.descrizione_html:user_lang != '') THEN p.descrizione_html:user_lang
        ELSE p.descrizione_html:default_lang
    END AS descrizione_html,
    CASE
        WHEN (p.tag_description:user_lang != '') THEN p.tag_description:user_lang
        ELSE p.tag_description:default_lang
    END AS metadescription,
    CASE
        WHEN (p.tag_keywords:user_lang != '') THEN p.tag_keywords:user_lang
        ELSE p.tag_keywords:default_lang
    END AS metakeywords,
    CASE
        WHEN (c.categoria:user_lang != '') THEN c.categoria:user_lang
        ELSE c.categoria:default_lang
    END AS categoria,
    p.note,
    p.id_categoria,
    p.iva,
    serie,
    sorting,
    p.id_marca,
    p.id_unita_misura,
    p.offerta,
    p.id_disponibilita,
    d.descrizione as disponibilita,
    p.novita,
    p.primo_piano,
    p.quantita_disponibile,
    p.peso,
    p.visibile,
    p.tipo_vis_varianti,
    p.listino:nlistino_conf_min AS conf_min,
    ROUND(p.listino:nlistino, 2) AS prezzo_listino,
    ROUND(p.listino:nlistino_sc, 2) AS prezzo_vendita,
    p.listino:nlistino AS prezzo_listino_raw,
    p.listino:nlistino_sc AS prezzo_vendita_raw,
    ROUND(p.listino:nlistino + (p.listino:nlistino * p.iva / 100),
            2) AS prezzo_listino_ivato,
    ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100),
            2) AS prezzo_vendita_ivato,
    ROUND((p.listino:nlistino - p.listino:nlistino_sc), 2) AS risparmio,
    1 as withphoto,
    m.marca,
    CASE WHEN(m.img_big IS NOT NULL) THEN 1 ELSE 0 END AS marca_img,
    (SELECT i.id FROM img_prodotti as i WHERE i.id_prodotto=p.id ORDER BY i.sorting LIMIT 1) as img,

    MIN(vp.l:nlistino_sc) AS prezzo_variante_min,
    MAX(vp.l:nlistino_sc) AS prezzo_variante_max,
    MIN(vp.l:nlistino) AS listino_variante_min,
    MAX(vp.l:nlistino) AS listino_variante_max,
    1 AS prezzi_ivati,
    MIN(vp.id) AS id_variante_def,
    
    (SELECT ROUND( (vp.l:nlistino + (vp.l:nlistino * p.iva / 100)) , 2) AS plistino
                                                                FROM varianti_prodotti vp
                                                                INNER JOIN (SELECT vp2.id, MIN(vp2.l:nlistino_sc) FROM varianti_prodotti vp2 GROUP BY vp2.id) AS jvar ON vp.id = jvar.id 
                                                                WHERE vp.id_prodotto = p.id LIMIT 1) AS listino_variante_min_ivato,
    (SELECT ROUND( MIN(vp.l:nlistino_sc) + ( MIN(vp.l:nlistino_sc) * p.iva / 100 ), 2 ) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id) AS prezzo_variante_min_ivato,
    
    CASE WHEN (p.listino:nlistino_sc=p.listino:nlistino) THEN ROUND(0,2) ELSE ROUND(100-(listino:nlistino_sc * 100)/listino:nlistino,2) END AS sconto

    FROM
        prodotti p
            INNER JOIN
        disponibilita AS d ON p.id_disponibilita = d.id
            INNER JOIN
        categorie AS c ON p.id_categoria = c.id
            LEFT JOIN
        varianti_prodotti AS vp ON (vp.id_prodotto = p.id)
            LEFT JOIN
        marche AS m ON m.id = p.id_marca
            LEFT JOIN
        categorie_prodotti AS cp ON cp.id_prodotti = p.id ";

	public function __construct()
	{
		$this->em = ORM::getInstance();
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function setNew()
	{
		$this->new = array(
			'id' => $this->id,
			'codice' => $this->codice,
			'titolo_it' => $this->titolo_it,
			'titolo_en' => $this->titolo_en,
			'titolo_fr' => $this->titolo_en,
			'titolo_pr' => $this->titolo_pr,
			'descrizione_html_it' => $this->descrizione_html_it,
			'descrizione_html_en' => $this->descrizione_html_en,
			'descrizione_html_fr' => $this->descrizione_html_fr,
			'descrizione_txt_it' => $this->descrizione_txt_it,
			'descrizione_txt_en' => $this->descrizione_txt_en,
			'descrizione_txt_fr' => $this->descrizione_txt_fr,
			'descrizione_txt_pr' => $this->descrizione_txt_pr,
			'descrizione_html_pr' => $this->descrizione_html_pr,
			'note' => $this->note,
			'id_categoria' => $this->id_categoria,
			'categoria' => $this->categoria,
			'id_marca' => $this->id_marca,
			'offerta' => $this->offerta,
			'img' => $this->img,
			'id_disponibilita' => $this->id_disponibilita,
			'novita' => $this->novita,
			'primo_piano' => $this->primo_piano,
			'quantita_disponibile' => $this->quantita_disponibile,
			'peso' => $this->peso,
			'iva' => $this->iva,
			'id_unita_misura' => $this->id_unita_misura,
			'prezzi_ivati' => $this->prezzi_ivati,
			'listino1' => $this->listino1,
			'listino2' => $this->listino2,
			'listino3' => $this->listino3,
			'listino4' => $this->listino4,
			'listino5' => $this->listino5,
			'listino6' => $this->listino6,
			'listino7' => $this->listino7,
			'listino8' => $this->listino8,
			'listino9' => $this->listino9,
			'listino10' => $this->listino10,
			'listino1_sc' => $this->listino1_sc,
			'listino2_sc' => $this->listino2_sc,
			'listino3_sc' => $this->listino3_sc,
			'listino4_sc' => $this->listino4_sc,
			'listino5_sc' => $this->listino5_sc,
			'listino6_sc' => $this->listino6_sc,
			'listino7_sc' => $this->listino7_sc,
			'listino8_sc' => $this->listino8_sc,
			'listino9_sc' => $this->listino9_sc,
			'listino10_sc' => $this->listino10_sc,
			'listino1_conf_min' => $this->listino1_conf_min,
			'listino2_conf_min' => $this->listino2_conf_min,
			'listino3_conf_min' => $this->listino3_conf_min,
			'listino4_conf_min' => $this->listino4_conf_min,
			'listino5_conf_min' => $this->listino5_conf_min,
			'listino6_conf_min' => $this->listino6_conf_min,
			'listino7_conf_min' => $this->listino7_conf_min,
			'listino8_conf_min' => $this->listino8_conf_min,
			'listino9_conf_min' => $this->listino9_conf_min,
			'listino10_conf_min' => $this->listino10_conf_min,
			'visibile' => $this->visibile,
			'tipo_vis_varianti' => $this->tipo_vis_varianti,
			'prezzo_variabile' => $this->prezzo_variabile,
			'tag_description_it' => $this->tag_description_it,
			'tag_description_en' => $this->tag_description_en,
			'tag_description_fr' => $this->tag_description_fr,
			'tag_description_pr' => $this->tag_description_pr,
			'tag_keywords_it' => $this->tag_keywords_it,
			'tag_keywords_en' => $this->tag_keywords_en,
			'tag_keywords_fr' => $this->tag_description_fr,
			'tag_keywords_pr' => $this->tag_keywords_pr,
			'ext_key_id' => '',
			'application_id' => '',
			'tag_prodotto' => $this->tag_prodotto,
			'id_master' => '',
			'serie' => $this->serie,
			'sorting' => $this->sorting
		);
	}

	public function loadMultiple($nlistino = 1)
	{
		$this->multiple = 1;
		$nlistino = sprintf('%02d', $nlistino);
		$note = trim($this->note);

		if ($note != '') {
			$params = explode(';', $note);
			foreach ($params as $par) {
				$pkey = substr($par, 0, strpos($par, '='));
				$pval = substr($par, strpos($par, '=') + 1);
				if ($pkey == 'ML' . $nlistino) {
					$this->multiple = intval($pval);
				}
			}
		}
	}

	public function getSeoUrl()
	{
		return UrlUtils::url_slug($this->titolo) . '-' . $this->id;
	}
}
