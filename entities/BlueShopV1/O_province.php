<?php

/**
 * Description of T_category
 *
 * @author Antonio Mazza
 */
class O_province
{
	public $id;
	public $cap;
	public $comune;
	public $prefisso;
	public $provincia;
	public $sigla_prov;
	public $regione;
	public $regione_id;
	private static $attributes = array(
		':provincia' => 'provincia',
		':sigla_prov' => 'sigla_prov'
	);
	private static $pre_query = 'SELECT DISTINCT provincia, sigla_prov 
                                 FROM comuni';

	public function __construct($row = '')
	{
		if (is_array($row)) {
			parent::_loadByRow($row);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":id='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}
}
