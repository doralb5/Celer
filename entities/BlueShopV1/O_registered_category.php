<?php

/**
 * Description of T_category
 *
 * @author Antonio Mazza
 */
class O_registered_category
{
	public $id;
	public $descrizione;
	public $id_listino;
	public $attivazione_utente;
	public $tipo_utente;
	public $predef;
	private static $attributes = array(
		':id' => 'id',
		':descrizione' => 'descrizione',
		':id_listino' => 'id_listino',
		':attivazione_utente' => 'attivazione_utente',
		':tipo_utente' => 'tipo_utente',
		':predef' => 'predef'
	);
	private static $pre_query = 'SELECT id, descrizione, attivazione_utente, tipo_utente, predef,id_listino 
                                 FROM categorie_registrati';

	public function __construct($row = '')
	{
		if (is_array($row)) {
			parent::_loadByRow($row);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":id='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}
}
