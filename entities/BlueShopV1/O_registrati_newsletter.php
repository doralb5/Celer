<?php

/**
 * Description of T_category
 *
 * @author Antonio Mazza
 */
class O_registrati_newsletter
{
	public $id;
	public $email;
	public $iscritto;
	public $data_iscrizione;
	public $remove_url;
	public $TABLE_NAME = 'registrati_newsletter';
	private static $attributes = array(
		':id' => 'id',
		':email' => 'email',
		':iscritto' => 'iscritto',
		':data_iscrizione' => 'data_iscrizione',
		':remove_url' => 'remove_url'
	);
	private static $pre_query = 'SELECT id, email, iscritto,data_iscrizione,remove_url 
                                 FROM registrati_newsletter';

	public function __construct($row = '')
	{
		if (is_array($row)) {
			parent::_loadByRow($row);
		}
		if (is_numeric($row)) {
			$this->cleanAll();
			$where = ":id='{$row}'";
			$this->setWhere($where);
			return $this->find();
		}
	}

	public static function getPre_query()
	{
		return self::$pre_query;
	}

	public static function getAttributes()
	{
		return self::$attributes;
	}

	public function setNew()
	{
		$this->new = array(
			'email' => $this->email,
			'data_iscrizione' => $this->data_iscrizione,
			'iscritto' => $this->iscritto,
			'remove_url' => $this->remove_url
		);
	}

	public function deleteEmail($id_mail)
	{
		$this->em->cleanAll();
		$this->em->setWhere(" :id='{$id_mail}'");
		$this->em->setLimit(1);
		$detail = $this->em->find();
		$this->em->cleanAll();
		return $this->em->delete($detail[0]);
	}
}
