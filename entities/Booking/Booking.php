<?php

class Booking_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Booking';
	const INDEX = 'id';
	const FK_User = 'Booking.user_id';

	public $id;
	public $user_id;
	public $room_type_id;
	public $room_id;
	public $created_time;
	public $reservation_start;
	public $reservation_end;
	public $guests_qty;
	public $children_qty;
	public $note;
	public $guest_firstname;
	public $guest_lastname;
	public $guest_address;
	public $guest_city;
	public $guest_zipcode;
	public $guest_country;
	public $guest_phone;
	public $guest_email;
	public $guest_birthday;
	public $guest_personalid;
	public $final_price;
	public $status_id;
	public $status_name;
	public $status_available;
	public $status_editable;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'user_id' => 'user_id',
			'room_type_id' => 'room_type_id',
			'room_id' => 'room_id',
			'created_time' => 'created_time',
			'reservation_start' => 'reservation_start',
			'reservation_end' => 'reservation_end',
			'guests_qty' => 'guests_qty',
			'children_qty' => 'children_qty',
			'note' => 'note',
			'guest_firstname' => 'guest_firstname',
			'guest_lastname' => 'guest_lastname',
			'guest_address' => 'guest_address',
			'guest_city' => 'guest_city',
			'guest_zipcode' => 'guest_zipcode',
			'guest_country' => 'guest_country',
			'guest_phone' => 'guest_phone',
			'guest_email' => 'guest_email',
			'guest_birthday' => 'guest_birthday',
			'guest_personalid' => 'guest_personalid',
			'final_price' => 'final_price',
			'status_id' => 'status_id'
		);
	}
}
