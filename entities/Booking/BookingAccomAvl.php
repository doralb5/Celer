<?php

require_once DOCROOT . ENTITIES_PATH . 'Booking/BookingAccomodation.php';

class BookingAccomAvl_Entity extends BookingAccomodation_Entity
{
	public $total_price;
	public $total_additional_price;
	public $final_price;
	public $available;
}
