<?php

class BookingAccomPrice_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BookingAccomPrice';
	const INDEX = 'id';
	const FK_AccomId = 'BookingAccomPrice.accomodation_id';

	public $id;
	public $accomodation_id;
	public $date_from;
	public $date_end;
	public $price_per_day;
	public $price_per_additional_guest;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'accomodation_id' => 'accomodation_id',
			'date_from' => 'date_from',
			'date_end' => 'date_end',
			'price_per_day' => 'price_per_day',
			'price_per_additional_guesy' => 'price_per_additional_guesy'
		);
	}
}
