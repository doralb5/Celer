<?php

require_once DOCROOT . ENTITIES_PATH . 'Booking/BookingAccomType.php';
require_once DOCROOT . ENTITIES_PATH . 'Booking/BookingPlace.php';

class BookingAccomodation_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BookingAccomodation';
	const INDEX = 'id';
	const FK_PlaceId = 'BookingAccomodation.place_id';
	const FK_TypeId = 'BookingAccomodation.type_id';

	public $id;
	public $place_id;
	public $type_id;
	public $name;
	public $descripton;
	public $image;
	public $qty;
	public $max_guests;
	public $place;
	public $type;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'place_id' => 'place_id',
			'type_id' => 'type_id',
			'name' => 'name',
			'descripton' => 'descripton',
			'image' => 'image',
			'qty' => 'qty',
			'max_guests' => 'max_guests'
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$Type_Table = TABLE_PREFIX . BookingAccomType_Entity::TABLE_NAME;
		$Place_Table = TABLE_PREFIX . BookingPlace_Entity::TABLE_NAME;

		$fpdo = new FluentPDO($pdo);

		$query = $fpdo->from(TABLE_PREFIX . self::TABLE_NAME)
			->innerJoin("$Place_Table ON $Place_Table.id = " . TABLE_PREFIX . self::FK_PlaceId)
			->leftJoin("$Type_Table ON $Type_Table.id = " . TABLE_PREFIX . self::FK_TypeId)
			->select("$Place_Table.name AS place, $Type_Table.name AS type");
		return $query;
	}
}
