<?php

require_once DOCROOT . ENTITIES_PATH . 'Booking/BookingRoomType.php';

class BookingAvailRoomPrice_Entity extends BookingRoomType_Entity
{
	public $checkin;
	public $checkout;
	public $guests;
	public $children;
	public $nights;
	public $total_price;
	public $total_additional_price;
	public $total_childrens_price;
	public $final_price;
	public $no_price;
	public $qty;
	public $images;
	public $room_type_id;
	public $avail_qty;  //Loaded by another model function
}
