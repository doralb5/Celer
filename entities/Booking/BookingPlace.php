<?php

class BookingPlace_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BookingPlace';
	const INDEX = 'id';

	public $id;
	public $name;
	public $address;
	public $city;
	public $zipcode;
	public $country;
	public $phone;
	public $email;
	public $www;
	public $accomodation_type_id;
	public $enabled;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'address' => 'address',
			'city' => 'city',
			'zipcode' => 'zipcode',
			'country' => 'country',
			'phone' => 'phone',
			'email' => 'email',
			'www' => 'www',
			'accomodation_type_id' => 'accomodation_type_id',
			'enabled' => 'enabled'
		);
	}
}
