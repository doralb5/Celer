<?php

class BookingRoom_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BookingRoom';
	const INDEX = 'id';

	public $id;
	public $name;
	public $room_type_id;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'room_type_id' => 'room_type_id'
		);
	}
}
