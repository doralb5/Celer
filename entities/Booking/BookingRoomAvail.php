<?php

require_once DOCROOT . ENTITIES_PATH . 'Booking/BookingRoomType.php';

class BookingRoomAvail_Entity extends BookingRoomType_Entity
{
	public $tot_qty;
	public $busy_qty;
	public $avail_qty;

	public function __construct($index = '')
	{
		parent::__construct($index);
	}
}
