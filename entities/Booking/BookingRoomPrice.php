<?php

class BookingRoomPrice_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BookingRoomPrice';
	const INDEX = 'id';

	public $id;
	public $room_type_id;
	public $date_from;
	public $date_end;
	public $price_per_day;
	public $price_per_additional_guest;
	public $price_per_child;
	public $type_name;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'room_type_id' => 'room_type_id',
			'date_from' => 'date_from',
			'date_end' => 'date_end',
			'price_per_day' => 'price_per_day',
			'price_per_additional_guest' => 'price_per_additional_guest',
			'price_per_child' => 'price_per_child'
		);
	}
}
