<?php

class BookingRoomType_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BookingRoomType';
	const INDEX = 'id';

	public $id;
	public $name;
	public $description;
	public $place_id;
	public $bedrooms;
	public $sleeps;
	public $free_wifi;
	public $sorting;

	public function __construct($index = '')
	{
		parent::__construct($index);
	}
}
