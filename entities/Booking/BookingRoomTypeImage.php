<?php

class BookingRoomTypeImage_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BookingRoomTypeImage';
	const INDEX = 'id';

	public $id;
	public $room_type_id;
	public $filename;
	public $is_default;
	public $sorting;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'room_type_id' => 'room_type_id',
			'filename' => 'filename',
			'is_default' => 'is_default',
			'sorting' => 'sorting'
		);
	}
}
