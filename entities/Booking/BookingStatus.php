<?php

class BookingStatus_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BookingStatus';
	const INDEX = 'id';

	public $id;
	public $name;
	public $available;
	public $editable;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'available' => 'available',
			'editable' => 'editable',
		);
	}
}
