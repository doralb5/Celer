<?php

class BookingEngine_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Reservation';
	const INDEX = 'id';
	const FK_Guest = 'Reservation.guest_id';

	public $id;
	public $date_in;
	public $date_out;
	public $made_by;
	public $guest_id;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'date_in' => 'date_in',
			'date_out' => 'date_out',
			'made_by' => 'made_by',
			'guest_id' => 'guest_id',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
	}
}
