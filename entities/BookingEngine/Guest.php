<?php

class Guest_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'guest';
	const INDEX = 'id';

	public $id;
	public $first_name;
	public $last_name;
	public $member_since;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'first_name' => 'first_name',
			'last_name' => 'last_name',
			'member_since' => 'member_since',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
