<?php

require_once DOCROOT . ENTITIES_PATH . 'BookingEngine/Reservation.php';

class ReservedRoom_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ReservedRoom';
	const INDEX = 'id';
	const FK_Reservation = 'ReservedRoom.reservation_id';

	public $id;
	public $number_of_rooms;
	public $reservation_id;
	public $status;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'number_of_rooms' => 'number_of_rooms',
			'reservation_id' => 'reservation_id',
			'status' => 'status',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$ReservedRoom_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$Reservation_Table = $TABLE_PREFIX . Reservation_Entity::TABLE_NAME;

		$FK_Reservation = $TABLE_PREFIX . self::FK_Reservation;
	}
}
