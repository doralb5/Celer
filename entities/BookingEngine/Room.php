<?php

class Guest_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Room';
	const INDEX = 'id';

	public $id;
	public $number;
	public $name;
	public $status;
	public $smoke;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'number' => 'number',
			'name' => 'name',
			'status' => 'status',
			'smoke' => 'smoke',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
