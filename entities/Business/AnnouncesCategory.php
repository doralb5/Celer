<?php

class AnnouncesCategory_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'AnnouncesCategory';
	const INDEX = 'id';

	public $id;
	public $category;
	public $description;
	public $class;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'category' => 'category',
			'description' => 'description',
			'class' => 'class',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
