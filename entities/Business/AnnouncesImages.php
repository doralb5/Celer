<?php

class AnnouncesImages_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'AnnounceImage';
	const INDEX = 'id';
	const FK_Announce = 'AnnounceImage.id_announce';

	public $id;
	public $id_announce;
	public $image;
	public $size;

	//Additional

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_announce' => 'id_announce',
			'image' => 'image',
			'size' => 'size',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Announce_Table = $TABLE_PREFIX . BusinessAnnounces_Entity::TABLE_NAME;
		$FK_Announce = $TABLE_PREFIX . self::FK_Announce;

		$fpdo = new FluentPDO($pdo);
		$join_announces = $Announce_Table . ' ON ' . $Announce_Table . '.' . BusinessAnnounces_Entity::INDEX . ' = ' . $FK_Announce;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_announces);
		return $query;
	}
}
