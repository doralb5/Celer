<?php

require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessCategory.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessImage.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessCateg.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessProfile.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessRating.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessStatistic.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessStatistics.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessSticker.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessMessage.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessImported.php';
require_once DOCROOT . ENTITIES_PATH . 'User/User.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/TempImage.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessOpenHours.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessPaymentsMethod.php';

class Business_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Business';
	const INDEX = 'id';
	const FK_BusinessCategory = 'Business.id_category';
	const FK_BusinessProfile = 'Business.id_profile';
	const FK_User = 'Business.id_user';

	public $id;
	public $company_name;
	public $description;
	public $image;
	public $id_category;
	public $location;
	public $address;
	public $phone;
	public $mobile;
	public $email;
	public $website;
	public $coordinates;
	public $rates;
	public $creation_date;
	public $publish_date;
	public $expire_date;
	public $featured = 0;
	public $enabled = 1;
	public $subdomain;
	public $business_mail;
	public $facebook;
	public $instagram;
	public $twitter;
	public $google;
	public $linkedin;
	public $notified = 0;
	public $id_user;
	public $tags = '';
	public $id_profile;
	public $category;
	public $profile;
	public $relevance;
	public $payment = array();
	public $images = array();
	public $open_hours = array();
	public $user_fullname;
	public $rating_count;
	public $total_rating;
	public $rating;
	public $panorama;
	public $youtube_link;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'company_name' => 'company_name',
			'description' => 'description',
			'image' => 'image',
			'id_category' => 'id_category',
			'location' => 'location',
			'address' => 'address',
			'phone' => 'phone',
			'mobile' => 'mobile',
			'email' => 'email',
			'website' => 'website',
			'coordinates' => 'coordinates',
			'rates' => 'rates',
			'creation_date' => 'creation_date',
			'publish_date' => 'publish_date',
			'expire_date' => 'expire_date',
			'featured' => 'featured',
			'enabled' => 'enabled',
			'subdomain' => 'subdomain',
			'business_mail' => 'business_mail',
			'facebook' => 'facebook',
			'instagram' => 'instagram',
			'twitter' => 'twitter',
			'google' => 'google',
			'linkedin' => 'linkedin',
			'notified' => 'notified',
			'id_user' => 'id_user',
			'tags' => 'tags',
			'id_profile' => 'id_profile',
			'youtube_link' => 'youtube_link',
		);
		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Business_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$BusinessCategory_Table = $TABLE_PREFIX . BusinessCategory_Entity::TABLE_NAME;
		$BusinessProf_Table = $TABLE_PREFIX . BusinessProfile_Entity::TABLE_NAME;
		$User_Table = $TABLE_PREFIX . User_Entity::TABLE_NAME;
		$BC_Table = $TABLE_PREFIX . BusinessCateg_Entity::TABLE_NAME;
		$BusRating_Table = $TABLE_PREFIX . BusinessRating_Entity::TABLE_NAME;
		$BusOpenHours_Table = $TABLE_PREFIX . BusinessOpenHours_Entity::TABLE_NAME;
		$BusPaymentMethods_Table = $TABLE_PREFIX . BusinessPaymentsMethod_Entity::TABLE_NAME;

		$FK_BusinessCategory = $TABLE_PREFIX . self::FK_BusinessCategory;
		$FK_BusinessProf = $TABLE_PREFIX . self::FK_BusinessProfile;
		$FK_User = $TABLE_PREFIX . self::FK_User;
		$FK_BC = $TABLE_PREFIX . BusinessCateg_Entity::FK_Business;
		$FK_Business = $TABLE_PREFIX . BusinessRating_Entity::FK_Business;
		$FK_BusinessOpenHours = $TABLE_PREFIX . BusinessOpenHours_Entity::FK_Business;
		$fpdo = new FluentPDO($pdo);

		$join_bc = $BC_Table . ' ON ' . $FK_BC . ' = ' . $Business_Table . '.' . self::INDEX;
		$join_category = $BusinessCategory_Table . ' ON ' . $BusinessCategory_Table . '.' . BusinessCategory_Entity::INDEX . ' = ' . $FK_BusinessCategory;
		$join_profile = $BusinessProf_Table . ' ON ' . $BusinessProf_Table . '.' . BusinessProfile_Entity::INDEX . ' = ' . $FK_BusinessProf;
		$join_user = $User_Table . ' ON ' . $User_Table . '.' . User_Entity::INDEX . ' = ' . $FK_User;
		$join_rating = $BusRating_Table . ' ON ' . $FK_Business . ' = ' . $Business_Table . '.' . self::INDEX;
		//$join_openhours = $BusOpenHours_Table . " ON " . $FK_BusinessOpenHours . " = " . $Business_Table . '.' . self::INDEX;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_bc)
			->leftJoin($join_category)
			->leftJoin($join_user)
			->leftJoin($join_profile)
			->leftJoin($join_rating)
			->select("(SELECT COUNT(id_biz) FROM $BusRating_Table WHERE $BusRating_Table.id_biz = $Business_Table.id) as rating_count")
			->select("(SELECT SUM(rating) FROM $BusRating_Table WHERE $BusRating_Table.id_biz = $Business_Table.id) as total_rating")
			->select(" (SELECT SUM(rating) FROM $BusRating_Table WHERE $BusRating_Table.id_biz = $Business_Table.id) / (SELECT COUNT(id_biz) FROM $BusRating_Table WHERE $BusRating_Table.id_biz = $Business_Table.id) as rating")
			->select("CONCAT($User_Table.firstname, ' ', $User_Table.lastname) AS user_fullname")
			->select("{$TABLE_PREFIX}BusinessCategory.category")
			->select("$BusinessProf_Table.profile, $BusinessProf_Table.relevance, $BusinessProf_Table.payment")
			->groupBy(self::INDEX);
		//echo '<!--' . $query->getQuery() . '-->';
		return $query;
	}
}
