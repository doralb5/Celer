<?php

//require_once DOCROOT . ENTITIES_PATH . 'Business/AnnouncesCategory.php';
//
//class BusinessAnnounces_Entity extends Entity {
//
//    const DATASOURCE = 'db';
//    const TABLE_NAME = 'Announces';
//    const INDEX = 'id';
//    const FK_Category = 'Announces_Category.category_id';
//    const FK_Categoryname = 'Announces_Category.category';
//
//
//    public $id;
//    public $publish_date;
//    public $name;
//    public $title;
//    public $description;
//    public $category_id;
//    public $expiration_date;
//    public $views;
//    public $email;
//    public $tel;
//    public $user_id;
//    public $enabled = 1;
//    public $notified = 0;
//    public $image;
//
//
//
//    public function __construct($index = '') {
//
//        parent::__construct($index);
//        $this->entity_fields = array(
//            'id' => self::$table_prefix . self::TABLE_NAME . '.id',
//            'publish_date' => 'publish_date',
//            'name' => 'name',
//            'title' => 'title',
//            'description' => 'description',
//            'category_id' => 'category_id',
//            'expiration_date' => 'expiration_date',
//            'views' => 'views',
//            'email' => 'email',
//            'tel' => 'tel',
//            'user_id' => 'user_id',
//            'enabled' => 'enabled',
//            'notified' => 'notified',
//            'image' => 'image',
//
//        );
//        $this->publish_date = date('Y-m-d H:i:s');
//    }
//
//
//    public static function getSelectQueryObj($pdo) {
//        $TABLE_PREFIX = self::$table_prefix;
//        $BusinessAnnounces_Table = $TABLE_PREFIX . self::TABLE_NAME;
//        $AnnouncesCategory_Table = $TABLE_PREFIX . AnnouncesCategory_Entity::TABLE_NAME;
//        $FK_AnnouncesCategory = $TABLE_PREFIX . self::FK_Categoryname;
//        $fpdo = new FluentPDO($pdo);
//        $join_category = $BusinessAnnounces_Table . " ON " . $AnnouncesCategory_Table . '.' . AnnouncesCategory_Entity::INDEX . " = " . $FK_AnnouncesCategory;
//
//
//        $query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
//                ->leftJoin($join_category);
//
//        return $query;
//    }
//}

require_once DOCROOT . ENTITIES_PATH . 'Business/AnnouncesCategory.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/AnnouncesImages.php';

class BusinessAnnounces_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Announces';
	const INDEX = 'id';
	const FK_AnnouncesCategory = 'AnnouncesCategory.category_id';

	public $id;
	public $publish_date;
	public $name;
	public $title;
	public $description;
	public $category_id;
	public $expiration_date;
	public $views;
	public $email;
	public $tel;
	public $user_id;
	public $enabled = 1;
	public $notified = 0;
	public $image;
	public $view_contact;
	public $location;
	public $category;
	public $class;
	public $category_description;
	public $category_image;
	public $images = array();

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'publish_date' => 'publish_date',
			'name' => 'name',
			'title' => 'title',
			'description' => 'description',
			'category_id' => 'category_id',
			'expiration_date' => 'expiration_date',
			'views' => 'views',
			'email' => 'email',
			'tel' => 'tel',
			'user_id' => 'user_id',
			'enabled' => 'enabled',
			'notified' => 'notified',
			'image' => 'image',
			'view_contact' => 'view_contact',
			'location' => 'location',
		);
		$this->publish_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$BusinessAnnounces_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$AnnouncesCategory_Table = $TABLE_PREFIX . AnnouncesCategory_Entity::TABLE_NAME;

		$fpdo = new FluentPDO($pdo);
		$join_category = $AnnouncesCategory_Table . ' ON cms_AnnouncesCategory.id = cms_Announces.category_id'; //. $BusinessAnnounces_Table . "." . self::INDEX . "=" . self::FK_AnnouncesCategory  ;
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_category)
			->select("{$TABLE_PREFIX}Announces.id as id")
			->select("{$TABLE_PREFIX}Announces.publish_date as publish_date ")
			->select("{$TABLE_PREFIX}Announces.title as title ")
			->select("{$TABLE_PREFIX}Announces.email as email")
			->select("{$TABLE_PREFIX}Announces.description as description ")
			->select("{$TABLE_PREFIX}Announces.category_id as category_id")
			->select("{$TABLE_PREFIX}Announces.expiration_date as expiration_date")
			->select("{$TABLE_PREFIX}Announces.views as views")
			->select("{$TABLE_PREFIX}Announces.tel as tel")
			->select("{$TABLE_PREFIX}Announces.user_id as user_id")
			->select("{$TABLE_PREFIX}Announces.enabled as enabled")
			->select("{$TABLE_PREFIX}Announces.notified as notified")
			->select("{$TABLE_PREFIX}Announces.image as image")
			->select("{$TABLE_PREFIX}Announces.view_contact as view_contact")
			->select("{$TABLE_PREFIX}AnnouncesCategory.category as category")
			->select("{$TABLE_PREFIX}AnnouncesCategory.class as class")
			->select("{$TABLE_PREFIX}AnnouncesCategory.description as category_description")
			->select("{$TABLE_PREFIX}AnnouncesCategory.image as category_image");
		//echo '<!--' . $query->getQuery() . '-->';

		return $query;
	}
}
