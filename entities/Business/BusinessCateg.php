<?php

class BusinessCateg_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessCateg';
	const INDEX = 'id';
	const FK_Business = 'BusinessCateg.id_business';
	const FK_Category = 'BusinessCateg.id_category';

	public $id;
	public $id_business;
	public $id_category;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_business' => 'id_business',
			'id_category' => 'id_category',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
