<?php

require_once DOCROOT . ENTITIES_PATH . 'Business/Business.php';

class BusinessCategory_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessCategory';
	const INDEX = 'id';

	public $id;
	public $category;
	public $description;
	public $image;
	public $sorting;
	public $parent_id;
	public $tags = '';
	public $total_businesses;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'category' => 'category',
			'description' => 'description',
			'image' => 'image',
			'sorting' => 'sorting',
			'parent_id' => 'parent_id',
			'tags' => 'tags',
		);
	}

	public static function old_______getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$Category_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$Business_Table = $TABLE_PREFIX . Business_Entity::TABLE_NAME;
		$BusinessCateg_Table = $TABLE_PREFIX . BusinessCateg_Entity::TABLE_NAME;

		$fpdo = new FluentPDO($pdo);

		//$join_business = $BusinessCategory_Table . " ON " . $BusinessCategory_Table . '.' . BusinessCategory_Entity::INDEX . " = " . $FK_BusinessCategory;
		//        $total_businesses = "((SELECT DISTINCT COUNT(DISTINCT id) FROM $Business_Table WHERE $Business_Table.id_category = $Category_Table.id AND $Business_Table.enabled = '1' AND $Business_Table.expire_date > NOW() AND $Business_Table.publish_date < NOW())
		//        + (SELECT DISTINCT COUNT(DISTINCT id_business) AS TOT FROM $BusinessCateg_Table
		//        LEFT JOIN $Business_Table ON $Business_Table.id = $BusinessCateg_Table.id_business
		//        WHERE $BusinessCateg_Table.id_category = $Category_Table.id AND $Business_Table.enabled = '1' AND $Business_Table.expire_date > NOW() AND $Business_Table.publish_date < NOW())) AS total_businesses";

		$leftJoinBusiness = "(
SELECT t.id, t.enabled, t.publish_date, t.expire_date, t.location, cms_BusinessCateg.id_category
FROM cms_Business AS t
INNER JOIN cms_BusinessCateg ON cms_BusinessCateg.id_business = t.id

UNION DISTINCT

SELECT t.id, t.enabled, t.publish_date, t.expire_date, t.location, t.id_category
FROM cms_Business AS t
)AS $Business_Table 
ON cms_BusinessCategory.id = $Business_Table.id_category";

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($leftJoinBusiness)
			->select("COUNT(DISTINCT $Business_Table.id) AS total_businesses")
			->groupBy("$Category_Table.id")
			->disableSmartJoin();
		return $query;
	}
}
