<?php

class BusinessImported_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessImported';
	const INDEX = 'id';

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'company_name' => 'company_name',
			'address' => 'address',
			'phone1' => 'phone1',
			'phone2' => 'phone2',
			'phone3' => 'phone3',
			'mobile1' => 'mobile1',
			'mobile2' => 'mobile2',
			'mobile3' => 'mobile3',
			'fax' => 'fax',
			'website' => 'website',
			'establishment_year' => 'establishment_year',
			'employes' => 'employes',
			'registration_code' => 'registration_code',
			'vat_registration' => 'vat_registration',
			'company_manager' => 'company_manager',
			'description' => 'description',
			'image' => 'image'
		);
	}
}
