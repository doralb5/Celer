<?php

class BusinessMessage_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessMessage';
	const INDEX = 'id';
	const FK_Business = 'Business.id_business';

	public $id;
	public $id_business;
	public $creation_date;
	public $useragent;
	public $fromip;
	public $name;
	public $email;
	public $tel;
	public $message;
	public $readed = 0;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_business' => 'id_business',
			'useragent' => 'useragent',
			'fromip' => 'fromip',
			'name' => 'name',
			'email' => 'email',
			'tel' => 'tel',
			'message' => 'message',
			'creation_date' => 'creation_date',
			'readed' => 'readed'
		);
		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
