<?php

class BusinessOffer_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessOffer';
	const INDEX = 'id';
	const INDEX_B = 'id_biz';
	const FK_Images = 'id_offer';
	const FK_BusinessUser = 'id';

	public $id;
	public $id_biz;
	public $title;
	public $description;
	public $creation_date;
	public $expiration_date;
	public $price;
	public $final_price;
	public $currency;
	public $offer_image;
	public $business_name;
	public $allimages;
	public $images = array();

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_biz' => 'id_biz',
			'description' => 'description',
			'title' => 'title',
			'expiration_date' => 'expiration_date',
			'price' => 'price',
			'final_price' => 'final_price',
			'currency' => 'currency',
			'offer_image' => 'offer_image',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$Images_Table = self::$table_prefix . 'BusinessOfferImages';
		$Business_Table = self::$table_prefix . 'Business';
		$BusinessCateg_Table = self::$table_prefix . 'BusinessCateg';
		$joinImages = $Images_Table . ' ON ' . self::$table_prefix . self::TABLE_NAME . '.' . self::INDEX . ' = ' . $Images_Table . '.' . self::FK_Images;
		$joinBusiness = $Business_Table . ' ON ' . self::$table_prefix . self::TABLE_NAME . '.' . self::INDEX_B . ' = ' . $Business_Table . '.' . self::FK_BusinessUser;
		//$joinBusinessCateg = $BusinessCateg_Table . ' ON ' . self::$table_prefix. self::TABLE_NAME . '.' .self::INDEX_B . ' = ' . $BusinessCateg_Table .'.id_business';
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from(self::$table_prefix . self::TABLE_NAME)
			->innerJoin($joinBusiness)
			->leftJoin($joinImages)
			->select("$Business_Table.id_user, $Business_Table.company_name AS business_name, GROUP_CONCAT($Images_Table.image,',',$Images_Table.id) AS allimages")
			->groupBy(self::$table_prefix . self::TABLE_NAME . '.' . self::INDEX);

		return $query;
	}
}
