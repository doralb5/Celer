<?php

class BusinessOfferImages_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessOfferImages';
	const INDEX = 'id';

	public $id;
	public $id_offer;
	public $image;
	public $alt_tags;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_offer' => 'id_offer',
			'image' => 'image',
			'alt_tags' => 'alt_tags',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from(self::$table_prefix . self::TABLE_NAME);
		return $query;
	}
}
