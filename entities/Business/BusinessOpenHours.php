<?php

class BusinessOpenHours_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessOpenHours';
	const INDEX = 'id';
	const FK_Business = 'BusinessOpenHours.business_id';

	public $id;
	public $business_id;
	public $day;
	public $open_start;
	public $open_end;
	public $pause_start;
	public $pause_end;
	public $closed = 0;
	public $hours24 = 0;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'business_id' => 'business_id',
			'day' => 'day',
			'open_start' => 'open_start',
			'open_end' => 'open_end',
			'pause_start' => 'pause_start',
			'pause_end' => 'pause_end',
			'closed' => 'closed',
			'hours24' => 'hours24',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from(self::$table_prefix . self::TABLE_NAME);
		return $query;
	}
}
