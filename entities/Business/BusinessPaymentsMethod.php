<?php

class BusinessPaymentsMethod_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessPaymentsMethod';
	const INDEX = 'id';

	public $id;
	public $id_business;
	public $id_payment;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_business' => 'id_business',
			'id_payment' => 'id_payment',
		);
	}
}
