<?php

require_once DOCROOT . ENTITIES_PATH . 'Business/Business.php';

class BusinessProfile_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessProfile';
	const INDEX = 'id';

	public $id;
	public $profile;
	public $description;
	public $relevance;
	public $payment = 0;
	public $max_photo;
	public $max_video;
	public $max_offers;
	public $show_stats = 0;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'profile' => 'profile',
			'description' => 'description',
			'relevance' => 'relevance',
			'payment' => 'payment',
			'max_photo' => 'max_photo',
			'max_video' => 'max_video',
			'show_stats' => 'show_stats',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		//$Category_Table = $TABLE_PREFIX . self::TABLE_NAME;
		//$Business_Table = $TABLE_PREFIX . Business_Entity::TABLE_NAME;

		$fpdo = new FluentPDO($pdo);

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
				//->select("(SELECT count(*) FROM $Business_Table WHERE $Business_Table.id_category = $Category_Table.id AND enabled='1') AS total_businesses")
			->disableSmartJoin();
		return $query;
	}
}
