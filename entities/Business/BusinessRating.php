<?php

class BusinessRating_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessRating';
	const INDEX = 'id';
	const FK_Business = 'BusinessRating.id_biz';

	public $id;
	public $id_biz;
	public $rating;
	public $ip_client;
	public $comment;
	public $email;
	public $name;
	public $tel;
	public $enabled = 1;
	public $publish_date;
	//Additional
	public $company_name = '';
	public $image = '';

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => 'id',
			'id_biz' => 'id_biz',
			'rating' => 'rating',
			'ip_client' => 'ip_client',
			'comment' => 'comment',
			'email' => 'email',
			'name' => 'name',
			'tel' => 'tel',
			'enabled' => 'enabled',
			'publish_date' => 'publish_date',
		);

		$this->publish_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		//        $TABLE_PREFIX = self::$table_prefix;
		//        $fpdo = new FluentPDO($pdo);
		//        $query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
//
		//        return $query;
//

		$TABLE_PREFIX = self::$table_prefix;
		$Business_Table = $TABLE_PREFIX . Business_Entity::TABLE_NAME;
		$FK_Business = $TABLE_PREFIX . self::FK_Business;

		$fpdo = new FluentPDO($pdo);
		$join_business = $Business_Table . ' ON ' . $Business_Table . '.' . Business_Entity::INDEX . ' = ' . $FK_Business;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_business)
			->select("{$TABLE_PREFIX}Business.company_name,{$TABLE_PREFIX}Business.image");
		return $query;
	}
}
