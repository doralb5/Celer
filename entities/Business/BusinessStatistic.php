<?php

class BusinessStatistic_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessStatistic';
	const INDEX = 'id';
	const FK_Business = 'BusinessStatistic.id_business';

	public $id;
	public $id_business;
	public $type;
	public $creation_date;
	public $ip_client;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_business' => 'id_business',
			'type' => 'type',
			'creation_date' => 'creation_date',
			'ip_client' => 'ip_client',
		);

		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
