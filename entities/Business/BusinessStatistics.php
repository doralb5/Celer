<?php

class BusinessStatistics_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessStatistics';
	const INDEX = 'id';
	const FK_Business = 'BusinessStatistics.id_business';

	public $id;
	public $id_business;
	public $impressions = 0;
	public $views = 0;
	public $subdomain_views = 0;
	public $messages = 0;
	public $subdomain_messages = 0;
	public $website_clicks = 0;
	public $facebook_clicks = 0;
	public $linkedin_clicks = 0;
	public $instagram_clicks = 0;
	public $featured_impressions = 0;
	public $featured_clicks = 0;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_business' => 'id_business',
			'impressions' => 'impressions',
			'views' => 'views',
			'subdomain_views' => 'subdomain_views',
			'messages' => 'messages',
			'subdomain_messages' => 'subdomain_messages',
			'website_clicks' => 'website_clicks',
			'facebook_clicks' => 'facebook_clicks',
			'linkedin_clicks' => 'linkedin_clicks',
			'instagram_clicks' => 'instagram_clicks',
			'featured_impressions' => 'featured_impressions',
			'featured_clicks' => 'featured_clicks',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
