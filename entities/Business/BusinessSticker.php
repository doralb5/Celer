<?php

class BusinessSticker_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'BusinessSticker';
	const INDEX = 'id';
	const FK_Business = 'BusinessSticker.id_business';

	public $id;
	public $id_business;
	public $token;
	public $required = 0;
	public $done = 0;
	//Additional
	public $company_name = '';

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_business' => 'id_business',
			'token' => 'token',
			'required' => 'required',
			'done' => 'done',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Business_Table = $TABLE_PREFIX . Business_Entity::TABLE_NAME;
		$FK_Business = $TABLE_PREFIX . self::FK_Business;

		$fpdo = new FluentPDO($pdo);
		$join_business = $Business_Table . ' ON ' . $Business_Table . '.' . Business_Entity::INDEX . ' = ' . $FK_Business;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_business)
			->select("{$TABLE_PREFIX}Business.company_name");
		return $query;
	}
}
