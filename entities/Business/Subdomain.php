<?php

require_once DOCROOT . ENTITIES_PATH . 'Business/Business.php';

class Subdomain_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Subdomain';
	const INDEX = 'id';

	public $id;
	public $subdomain;
	public $target_id;
	public $meta_keywords;
	public $meta_description;
	public $type;
	public $target_url;
	public $cover_image;
	public $company_name;
	public $category;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'subdomain' => 'subdomain',
			'target_id' => 'target_id',
			'meta_keywords' => 'meta_keywords',
			'meta_description' => 'meta_description',
			'type' => 'type',
			'target_url' => 'target_url',
			'cover_image' => 'cover_image',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);

		$Business_Tbl = $TABLE_PREFIX . 'Business';
		$BusinessCateg_Tbl = $TABLE_PREFIX . 'BusinessCategory';
		$Subdomain_Tbl = $TABLE_PREFIX . 'Subdomain';

		$FK_target = $Subdomain_Tbl . '.target_id';

		$join_business = $Business_Tbl . ' ON ' . $Business_Tbl . '.' . Business_Entity::INDEX . ' = ' . $FK_target;
		$join_category = $BusinessCateg_Tbl . ' ON ' . $BusinessCateg_Tbl . '.' . BusinessCategory_Entity::INDEX . ' = ' . $FK_target;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_business)
			->leftJoin($join_category)
			->select("$Business_Tbl.company_name")
			->select("$BusinessCateg_Tbl.category");
		return $query;
	}
}
