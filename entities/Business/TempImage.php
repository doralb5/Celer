<?php

class TempImage_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'TempImages';
	const INDEX = 'id';

	public $id;
	public $image;
	public $filename;
	public $path;
	public $delete_action;

	//    public static function getSelectQueryObj($pdo) {
//        $TABLE_PREFIX = self::$table_prefix;
//        $fpdo = new FluentPDO($pdo);
//        $query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
//        return $query;
//    }
}
