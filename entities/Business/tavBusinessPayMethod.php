<?php

class tabBusinessPayMethod_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'tabBusinessPayMethod';
	const INDEX = 'id';

	public $id;
	public $name;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
		);
	}
}
