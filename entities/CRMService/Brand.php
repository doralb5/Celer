<?php

class Brand_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Brand';
	const INDEX = 'id';

	public $id;
	public $name;
	public $logo;
	public $company_name;
	public $support_email;
	public $support_url;
	public $phone;
	public $host;
}
