<?php

require_once DOCROOT . ENTITIES_PATH . 'CRMService/Profile.php';

class License_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'License';
	const INDEX = 'id';
	const FK_Profile = 'License.id_profile';

	public $id;
	public $code;
	public $activation_date;
	public $expiration_date;
	public $db_host;
	public $db_user;
	public $db_pass;
	public $db_name;
	public $enabled = 1;
	public $type = 0;
	public $id_profile = 0;
	public $profile_name;
	public $price;
	public $id_brand;
	public $brand_name;
	public $logo;
	public $company_name;
	public $support_email;
	public $support_url;
	public $phone;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'code' => 'username',
			'activation_date' => 'activation_date',
			'expiration_date' => 'expiration_date',
			'db_host' => 'db_host',
			'db_user' => 'db_user',
			'db_pass' => 'db_pass',
			'db_name' => 'db_name',
			'enabled' => 'enabled',
			'type' => 'type',
			'id_profile' => 'id_profile',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);

		$join_profile = Profile_Entity::TABLE_NAME . ' ON ' . Profile_Entity::TABLE_NAME . '. ' . Profile_Entity::INDEX . ' = ' . self::FK_Profile;
		$join_brand = Brand_Entity::TABLE_NAME . ' ON ' . Brand_Entity::TABLE_NAME . '. ' . Brand_Entity::INDEX . ' = ' . Profile_Entity::FK_Brand;

		$query = $fpdo->from(self::TABLE_NAME)
			->innerJoin($join_profile)
			->innerJoin($join_brand)
			->select('Profile.name AS profile_name, Profile.price, Profile.id_brand')
			->select('Brand.name AS brand_name, Brand.logo, Brand.company_name, Brand.support_email, Brand.support_url, Brand.phone');
		return $query;
	}
}
