<?php

require_once DOCROOT . ENTITIES_PATH . 'CRMService/Brand.php';

class Profile_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Profile';
	const INDEX = 'id';
	const FK_Brand = 'Profile.id_brand';

	public $id;
	public $name;
	public $price;
	public $id_brand;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'name' => 'name',
			'price' => 'price',
			'id_brand' => 'id_brand',
		);
	}
}
