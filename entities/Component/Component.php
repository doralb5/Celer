<?php

class Component_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Component';
	const INDEX = 'id';

	public $id;
	public $name;
	public $description;
	public $enabled = '0';
	public $settings;
	public $type = '0';
	public $sorting = '10';
	public $featured = '1';

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
