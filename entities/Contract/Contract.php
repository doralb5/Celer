<?php

require_once DOCROOT . ENTITIES_PATH . 'Registries/Company.php';
require_once DOCROOT . ENTITIES_PATH . 'Registries/Contact.php';
require_once DOCROOT . ENTITIES_PATH . 'Contract/Service.php';
require_once DOCROOT . ENTITIES_PATH . 'Contract/ContractService.php';
require_once DOCROOT . ENTITIES_PATH . 'Contract/ContractFile.php';
require_once DOCROOT . ENTITIES_PATH . 'Contract/ContractServiceData.php';
require_once DOCROOT . ENTITIES_PATH . 'User/User.php';

class Contract_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Contract';
	const INDEX = 'id';
	const FK_Company = 'Contract.id_company';
	const FK_Contact = 'Contract.id_contact';
	const FK_User = 'Contract.id_user';
	const FK_User_ReferredBy = 'Contract.id_referred_by';

	public $id;
	public $number;
	public $id_company;
	public $id_contact;
	public $date;
	public $description;
	public $id_user;
	public $id_referred_by;
	public $status;
	public $creation_date;
	public $expiration_date;
	public $company_name;
	public $contact_fullname;
	public $user_fullname;
	public $refby_fullname;
	public $max_num;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'number' => 'number',
			'id_company' => 'id_company',
			'id_contact' => 'id_contact',
			'date' => 'date',
			'description' => 'description',
			'id_user' => 'id_user',
			'id_referred_by' => 'id_referred_by',
			'status' => 'status',
			'creation_date' => 'creation_date',
			'expiration_date' => 'expiration_date',
		);
		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);

		$join_company = Company_Entity::TABLE_NAME . ' ON ' . Company_Entity::TABLE_NAME . '. ' . Company_Entity::INDEX . ' = ' . self::FK_Company;
		$join_contact = Contact_Entity::TABLE_NAME . ' ON ' . Contact_Entity::TABLE_NAME . '. ' . Contact_Entity::INDEX . ' = ' . self::FK_Contact;
		$join_user = User_Entity::TABLE_NAME . '  ON ' . User_Entity::TABLE_NAME . '. ' . User_Entity::INDEX . ' = ' . self::FK_User;
		$join_referredby = User_Entity::TABLE_NAME . ' ReferredBy  ON ReferredBy.' . User_Entity::INDEX . ' = ' . self::FK_User_ReferredBy;

		$query = $fpdo->from(self::TABLE_NAME)
			->leftJoin($join_company)
			->leftJoin($join_contact)
			->leftJoin($join_user)
			->leftJoin($join_referredby)
			->select('Company.name AS company_name')
			->select('CONCAT(Contact.firstname, " ", Contact.lastname) AS contact_fullname')
			->select('CONCAT(User.firstname, " ", User.lastname) AS user_fullname')
			->select('CONCAT(ReferredBy.firstname, " ", ReferredBy.lastname) AS refby_fullname');
		return $query;
	}
}
