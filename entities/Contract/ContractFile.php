<?php

class ContractFile_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ContractFile';
	const INDEX = 'id';
	const FK_Contract = 'ContractFile.id_contract';

	public $id;
	public $id_contract;
	public $type;
	public $filename;
	public $size;
}
