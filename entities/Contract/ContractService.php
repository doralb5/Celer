<?php

require_once DOCROOT . ENTITIES_PATH . 'Contract/Service.php';
require_once DOCROOT . ENTITIES_PATH . 'Contract/Contract.php';
require_once DOCROOT . ENTITIES_PATH . 'User/User.php';

class ContractService_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ContractService';
	const INDEX = 'id';
	const FK_Contract = 'ContractService.id_contract';
	const FK_Service = 'ContractService.id_service';
	const FK_User = 'ContractService.id_user';

	public $id;
	public $id_contract;
	public $id_service;
	public $id_user;
	public $status;
	public $creation_date;
	public $activation_date;
	public $suspended_date;
	public $cancelled_date;
	public $expiration_date;
	public $description;
	public $service_name;
	public $id_model;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'id_contract' => 'id_contract',
			'id_service' => 'id_service',
			'id_user' => 'id_user',
			'status' => 'status',
			'creation_date' => 'creation_date',
			'activation_date' => 'activation_date',
			'suspended_date' => 'suspended_date',
			'cancelled_date' => 'cancelled_date',
			'expiration_date' => 'expiration_date',
			'description' => 'description',
		);
		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);

		$join_service = Service_Entity::TABLE_NAME . ' ON ' . Service_Entity::TABLE_NAME . '. ' . Service_Entity::INDEX . ' = ' . self::FK_Service;

		$query = $fpdo->from(self::TABLE_NAME)
			->innerJoin($join_service)
			->select('Service.id_model')
			->select('Service.name AS service_name');
		return $query;
	}
}
