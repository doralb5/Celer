<?php

class ContractServiceData_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ContractServiceData';
	const INDEX = 'id';
	const FK_ContractService = 'ContractServiceData.id_contract_service';
	const FK_ServiceModelField = 'ContractServiceData.id_field';

	public $id;
	public $id_contract_service;
	public $id_field;
	public $value;
}
