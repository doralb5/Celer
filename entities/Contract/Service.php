<?php

require_once DOCROOT . ENTITIES_PATH . 'Contract/ServiceCategory.php';

class Service_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Service';
	const INDEX = 'id';
	const FK_ServiceCategory = 'Service.id_category';
	const FK_ServiceModel = 'Service.id_model';

	public $id;
	public $name;
	public $description;
	public $id_category;
	public $id_model;
	public $category;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'name' => 'name',
			'description' => 'description',
			'id_category' => 'id_category',
			'id_model' => 'id_model',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);

		$join_category = ServiceCategory_Entity::TABLE_NAME . ' ON ' . ServiceCategory_Entity::TABLE_NAME . '. ' . ServiceCategory_Entity::INDEX . ' = ' . self::FK_ServiceCategory;

		$query = $fpdo->from(self::TABLE_NAME)
			->innerJoin($join_category)
			->select('ServiceCategory.category');

		return $query;
	}
}
