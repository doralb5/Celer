<?php

class ServiceCategory_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ServiceCategory';
	const INDEX = 'id';

	public $id;
	public $category;
}
