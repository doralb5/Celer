<?php

class ServiceModel_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ServiceModel';
	const INDEX = 'id';

	public $id;
	public $name;
	public $filename = ''; //The View of the model ( Optional )

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from(self::TABLE_NAME);
		return $query;
	}
}
