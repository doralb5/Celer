<?php

class ServiceModelField_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'ServiceModelField';
	const INDEX = 'id';
	const FK_ServiceModel = 'ServiceModelField.id_model';

	public $id;
	public $id_model;
	public $name;
	public $type;  //text, select, number, radio ect...
	public $required = '0';
}
