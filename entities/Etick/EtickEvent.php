<?php

require_once DOCROOT . ENTITIES_PATH . 'Etick/EtickPlace.php';

class EtickEvent_Entity extends Entity
{
	const DATASOURCE = 'db';
	const INDEX = 'id';
	const TABLE_NAME = 'etick_event';
	const FK_Place = 'etick_place.id';

	public $id;
	public $name;
	public $description;
	public $start_date;
	public $end_date;
	public $id_place;
	public $image;
	public $enabled = '0';
	public $maxReservations;
	public $placeName;
	public $svg_map;
	public $city;
	public $address;
	public $zip_code;
	public $province;
	public $website;
	public $pending_time_limit;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => 'id',
			'name' => 'name',
			'description' => 'description',
			'start_date' => 'start_date',
			'end_date' => 'end_date',
			'id_place' => 'id_place',
			'image' => 'image',
			'enabled' => 'enabled',
			'maxReservations' => 'max_reservations',
			'pending_time_limit' => 'pending_time_limit',
			'website' => 'website'
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$prefix = self::$table_prefix;
		$placeTable = $prefix . 'etick_place';
		$fpdo = new FluentPDO($pdo);

		$join_place = $placeTable . ' ON ' . $prefix . self::FK_Place . ' = ' . $prefix . self::TABLE_NAME . '.' . 'id_place';
		$query = $fpdo->from($prefix . self::TABLE_NAME)
			->innerJoin($join_place)
			->select("{$placeTable}.name as placeName")
			->select("{$placeTable}.svg_map as svg_map")
			->select("{$placeTable}.zip_code as zip_code")
			->select("{$placeTable}.address as address")
			->select("{$placeTable}.city as city")
			->select("{$placeTable}.province as province");
		//echo '<!--' . $query->getQuery() . 'findme -->' ;exit;
		return $query;
	}
}
