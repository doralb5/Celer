<?php

class EtickPlace_entity extends Entity
{
	const INDEX = 'id';
	const TABLE_NAME = 'etick_place';
	const FK_Zona = 'etick_zona.id';

	public $id;
	public $name;
	public $image_map;
	public $enabled = '0';
	public $description;
	public $svg_map;
	public $city;
	public $zip_code;
	public $province;
	public $address;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'description' => 'description',
			'image_map' => 'image_map',
			'enabled' => 'enabled',
			'city' => 'city',
			'zip_code' => 'zip_code',
			'province' => 'province',
			'svg_map' => 'svg_map',
			'address' => 'address'
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$prefix = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($prefix . self::TABLE_NAME);
		return $query;
	}
}
