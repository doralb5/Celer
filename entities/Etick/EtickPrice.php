<?php

class EtickPrice_entity extends Entity
{
	const INDEX = 'id';
	const TABLE_NAME = 'etick_price';
	const FK_Zone = 'etick_zona.id';
	const FK_User = 'UserCategory.id';
	const FK_Event = 'etick_event.id';
	const FK_Seat = 'etick_seat.id';

	public $id;
	public $id_zona;
	public $id_event;
	public $price;
	public $id_seat;
	public $id_user_category;
	public $userCategory;
	public $eventName;
	public $seatName;
	public $zoneName;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_zona' => 'id_zona',
			'id_event' => 'id_event',
			'price' => 'price',
			'id_seat' => 'id_seat',
			'id_user_category' => 'id_user_category'
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$prefix = self::$table_prefix;
		$userTable = $prefix . 'UserCategory';
		$zoneTable = $prefix . 'etick_zona';
		$eventTable = $prefix . 'etick_event';
		$seatTable = $prefix . 'etick_seat';

		$joinUser = $userTable . ' ON ' . $prefix . self::FK_User . ' = ' . $prefix . self::TABLE_NAME . '.id_user_category';
		$joinEvent = $eventTable . ' ON ' . $prefix . self::FK_Event . ' = ' . $prefix . self::TABLE_NAME . '.id_event';
		$joinSeat = $seatTable . ' ON ' . $prefix . self::FK_Seat . ' = ' . $prefix . self::TABLE_NAME . '.id_seat';
		$joinZone = $zoneTable . ' ON ' . $prefix . self::FK_Zone . ' = ' . $prefix . self::TABLE_NAME . '.id_zona';

		$prefix = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($prefix . self::TABLE_NAME)
			->innerJoin($joinUser)
			->innerJoin($joinEvent)
			->innerJoin($joinZone)
			->leftJoin($joinSeat)
			->select("{$userTable}.category as userCategory")
			->select("{$zoneTable}.name as zoneName")
			->select("{$seatTable}.name as seatName")
			->select("{$eventTable}.name as eventName");
		return $query;
	}
}
