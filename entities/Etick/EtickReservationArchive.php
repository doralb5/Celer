<?php

class EtickReservationArchive_entity extends Entity
{
	const INDEX = 'id';
	const TABLE_NAME = 'etick_reservation_archive';
	const TABLE_ALIAS = 'res';
	const FK_Zone = 'etick_zona.id';
	const FK_User = 'User.id';
	const FK_Event = 'etick_event.id';
	const FK_Seat = 'etick_seat.id';
	const FK_Place = 'etick_zona.id_place';

	public $id;
	public $id_user;
	public $id_event;
	public $id_seat;
	public $confirmed;
	public $creation_date;
	public $price;
	public $username;
	public $firstname;
	public $lastname;
	public $email;
	public $eventName;
	public $seatName;
	public $note;
	public $delete_date;
	public $reservation_id;
	public $seat_id;
	public $seat_name;
	public $seat_description;
	public $seat_group;
	public $id_zona;
	public $zone_name;
	public $zone_description;
	public $zone_map;
	public $zone_id_place;
	public $zone_id_parent;
	public $place_id;
	public $place_name;
	public $place_description;
	public $place_image;
	public $place_address;
	public $event_name;
	public $event_description;
	public $event_start_date;
	public $event_end_date;
	public $event_image;
	public $maxreservations;
	public $final_price;
	public $id_evento;
	public $stato;
	public $element_id;
	public $fullname;
	protected static $user_category_id = null;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_user' => 'id_user',
			'id_event' => 'id_event',
			'id_seat' => 'id_seat',
			'confirmed' => 'confirmed',
			'note' => 'note',
			'creation_date' => 'creation_date',
			'delete_date' => 'delete_date',
			'reservation_id' => 'reservation_id',
			'price' => 'price'
		);
		$this->creation_date = date('Y-m-d H:i:s', time());
	}

	// $ucat_id integer UserCategoryID

	public static function setUserCategory($ucat_id)
	{
		self::$user_category_id = $ucat_id;
	}

	public static function getSelectQueryObj($pdo)
	{
		$prefix = self::$table_prefix;
		$resTable = $prefix . self::TABLE_NAME;
		$userTable = $prefix . 'User';
		$eventTable = $prefix . 'etick_event';
		$seatTable = $prefix . 'etick_seat';
		$zonaTable = $prefix . 'etick_zona';
		$placeTable = $prefix . 'etick_place';
		$priceTable = $prefix . 'etick_price';

		//$joinEventRes = $eventTable . ' AS eventres ON eventres.id = '.self::TABLE_ALIAS.'.id_event';
		//$joinUser = $userTable .  ' AS  ON ' .$prefix.  self::FK_User. ' = '.  $prefix.self::TABLE_NAME. '.id_user';
		$joinUser = $userTable . ' AS user ON res.id_user = user.id';
		//$joinSeat = $seatTable . ' AS seat ON seat.id = res.id_seat';
		$joinZone = $zonaTable . ' AS zona ON seat.id_zona = zona.id';
		$joinPlace = $placeTable . ' AS place ON zona.id_place = place.id';
		$joinEvent = $eventTable . ' AS eve ON eve.id_place = zona.id_place ';
		$joinReserv = $resTable . ' AS res ON res.id_event = eve.id AND seat.id = res.id_seat';
		if (!is_null(self::$user_category_id)) {
			$joinPr_seat = $priceTable . ' AS pr_seat ON pr_seat.id_seat = seat.id AND pr_seat.id_event = eve.id AND pr_seat.id_user_category = ' . self::$user_category_id;
		} else {
			$joinPr_seat = $priceTable . ' AS pr_seat ON pr_seat.id_seat = seat.id AND pr_seat.id_event = eve.id';
		}
		if (!is_null(self::$user_category_id)) {
			$joinPr_zona = $priceTable . ' AS pr_zona ON pr_zona.id_zona = seat.id_zona AND pr_zona.id_event = eve.id AND pr_zona.id_user_category = ' . self::$user_category_id . ' AND pr_zona.id_seat IS NULL';
		} else {
			$joinPr_zona = $priceTable . ' AS pr_zona ON pr_zona.id_zona = seat.id_zona AND pr_zona.id_event = eve.id AND pr_zona.id_seat IS NULL';
		}
		$joinPr_seat_other = $priceTable . ' AS pr_seat_others ON pr_seat_others.id_seat = seat.id AND pr_seat_others.id_event = eve.id AND pr_seat_others.id_user_category IS NULL';
		$joinPr_zona_other = $priceTable . ' AS pr_zona_others ON pr_zona_others.id_zona = seat.id_zona AND pr_zona_others.id_event = eve.id AND pr_zona_others.id_user_category IS NULL AND pr_zona_others.id_seat IS NULL';
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($seatTable . ' AS seat')	//$prefix.self::TABLE_NAME . " as " . self::TABLE_ALIAS)
				//->rightJoin($joinSeat)
			->innerJoin($joinZone)
			->innerJoin($joinPlace)
			->innerJoin($joinEvent)
			->leftJoin($joinReserv)
			->leftJoin($joinUser)
			->leftJoin($joinPr_seat)
			->leftJoin($joinPr_zona)
			->leftJoin($joinPr_seat_other)
			->leftJoin($joinPr_zona_other)
			->groupBy('seat.id')
			->select(null)
			->select(" res.*, seat.id as seat_id,
    seat.name as seat_name,
    seat.description as seat_description,
    seat.id_group as seat_group,
    seat.id_zona,
    seat.element_id,
    zona.name as zone_name,
    zona.description as zone_description,
    zona.image_map as zone_map,
    zona.id_place as zona_id_place,
    zona.id_parent as zona_id_parent,
    place.id as place_id,
    place.name as place_name,
    place.description as place_description, 
    place.image_map as place_image,
    place.address as place_address,
    eve.id as id_evento,
    eve.name as event_name,
    eve.description as event_description,
    eve.start_date as event_start_date ,
    eve.end_date as event_end_date,
    eve.image as event_image,
    user.username as username,
    CONCAT(user.firstname, ' ', user.lastname ) as fullname,
    user.email as email,
    user.firstname as firstname,
    user.lastname as lastname")
			->select('CASE
        WHEN
            ((pr_zona.id_user_category IS NULL AND pr_zona_others.id_user_category IS NULL AND pr_seat.id_user_category IS NULL AND pr_seat_others.id_user_category IS NULL) )
        THEN
            (CASE
                WHEN (pr_seat_others.price IS NOT NULL) THEN pr_seat_others.price
                ELSE (CASE
                    WHEN (pr_zona_others.price IS NOT NULL) THEN pr_zona_others.price
                    ELSE 0
                END)
            END)
        ELSE (CASE
            WHEN (pr_seat.price IS NOT NULL) THEN pr_seat.price
            ELSE (CASE
                WHEN (pr_zona.price IS NOT NULL) THEN pr_zona.price
                ELSE 0
            END)
        END)
    END AS final_price')
			->select("CASE
        WHEN (res.id IS NULL) THEN 'LIBERO'
        ELSE (CASE
            WHEN (res.confirmed = '1') THEN 'OCCUPATO'
            ELSE 'IMPEGNATO'
        END)
    END AS stato");

		return $query;
	}
}
