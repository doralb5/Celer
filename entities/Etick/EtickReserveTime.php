<?php

class EtickReserveTime_entity extends Entity
{
	const INDEX = 'id';
	const TABLE_NAME = 'etick_reservtime';

	public $id;
	public $start_date;
	public $end_date;
	public $id_user_category;
	public $id_event;
	public $user_category;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'start_date' => 'start_date',
			'end_date' => 'end_date',
			'id_user_category' => 'id_user_category',
			'id_event' => 'id_event');
	}

	public static function getSelectQueryObj($pdo)
	{
		$prefix = self::$table_prefix;
		$userCategoryTable = $prefix . 'UserCategory';
		$joinUserCategory = $userCategoryTable . ' AS userCat ON cms_etick_reservtime.id_user_category = userCat.id';

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($prefix . self::TABLE_NAME)
			->leftJoin($joinUserCategory)
			->select('userCat.category as user_category');
		return $query;
	}
}
