<?php

class EtickSeat_entity extends Entity
{
	const INDEX = 'id';
	const TABLE_NAME = 'etick_seat';
	const FK_Zona = 'etick_zona.id';

	public $id;
	public $name;
	public $description;
	public $id_zona;
	public $zoneName;
	public $element_id;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'element_id' => 'element_id',
			'description' => 'description',
			'id_zona' => 'id_zona');
	}

	public static function getSelectQueryObj($pdo)
	{
		$prefix = self::$table_prefix;
		$zoneTable = $prefix . 'etick_zona';

		$joinZone = $zoneTable . ' ON ' . $prefix . self::FK_Zona . ' = ' . $prefix . self::TABLE_NAME . '.' . 'id_zona';

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($prefix . self::TABLE_NAME)
			->innerJoin($joinZone)
			->select("{$zoneTable}.name as zoneName");

		return $query;
	}
}
