<?php

class EtickZona_entity extends Entity
{
	const INDEX = 'id';
	const TABLE_NAME = 'etick_zona';
	const FK_Place = 'etick_place.id';

	public $id;
	public $name;
	public $description;
	public $image_map;
	public $id_place;
	public $id_parent;

	//  public $placeName;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'description' => 'description',
			'image_map' => 'image_map',
			'id_place' => 'id_place',
			'id_parent' => 'id_parent'
				);
	}

	public static function getSelectQueryObj($pdo)
	{
		$prefix = self::$table_prefix;
		$placeTable = $prefix . 'etick_place';

		$fpdo = new FluentPDO($pdo);

		// $join_place = $placeTable . ' ON ' . self::FK_Place . ' = ' . $prefix . self::TABLE_NAME . '.' . 'id_place';
		$query = $fpdo->from($prefix . self::TABLE_NAME)
		// ->innerJoin($join_place)
		//->select("{$placeTable}.name as placeName")
;
		return $query;
	}
}
