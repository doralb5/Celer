<?php

require_once DOCROOT . ENTITIES_PATH . 'Event/EventCategory.php';
require_once DOCROOT . ENTITIES_PATH . 'Event/EventImage.php';
require_once DOCROOT . ENTITIES_PATH . 'User/User.php';

class Event_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Event';
	const INDEX = 'id';
	const FK_EventCategory = 'Event.id_category';
	const FK_User = 'Event.id_user';

	public $id;
	public $title;
	public $description;
	public $image;
	public $id_category;
	public $pay;
	public $cost;
	public $currency;
	public $creation_date;
	public $start_date;
	public $end_date;
	public $organizer;
	public $location;
	public $address;
	public $phone;
	public $email;
	public $website;
	public $file;
	public $coordinates;
	public $featured = 0;
	public $enabled = 1;
	public $id_user;
	public $user_fullname;
	public $category;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$logged_user = UserAuth::getLoginSession();
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'title' => 'title',
			'description' => 'description',
			'image' => 'image',
			'id_category' => 'id_category',
			'pay' => 'pay',
			'cost' => 'cost',
			'currency' => 'currency',
			'creation_date' => 'creation_date',
			'start_date' => 'start_date',
			'end_date' => 'end_date',
			'organizer' => 'organizer',
			'location' => 'location',
			'address' => 'address',
			'phone' => 'phone',
			'email' => 'email',
			'website' => 'website',
			'file' => 'file',
			'coordinates' => 'coordinates',
			'featured' => 'featured',
			'enabled' => 'enabled',
			'id_user' => 'id_user',
		);
		$this->creation_date = date('Y-m-d H:i:s');
		$this->id_user = $logged_user['id'];
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$EventCategory_Table = $TABLE_PREFIX . EventCategory_Entity::TABLE_NAME;
		$User_Table = $TABLE_PREFIX . User_Entity::TABLE_NAME;
		$FK_EventCategory = $TABLE_PREFIX . self::FK_EventCategory;
		$FK_User = $TABLE_PREFIX . self::FK_User;

		$fpdo = new FluentPDO($pdo);

		$join_category = $EventCategory_Table . ' ON ' . $EventCategory_Table . '.' . EventCategory_Entity::INDEX . ' = ' . $FK_EventCategory;
		$join_user = $User_Table . ' ON ' . $User_Table . '.' . User_Entity::INDEX . ' = ' . $FK_User;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_category)
			->leftJoin($join_user)
			->select("{$TABLE_PREFIX}EventCategory.category")
			->select("CONCAT({$TABLE_PREFIX}User.firstname, ' ', {$TABLE_PREFIX}User.lastname) AS user_fullname");
		return $query;
	}
}
