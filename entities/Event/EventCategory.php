<?php

class EventCategory_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'EventCategory';
	const INDEX = 'id';

	public $id;
	public $category;
	public $description;
	public $image;
	public $sorting;
	public $parent_id;
	public $total_events;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'category' => 'category',
			'description' => 'description',
			'image' => 'image',
			'sorting' => 'sorting',
			'parent_id' => 'parent_id',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
