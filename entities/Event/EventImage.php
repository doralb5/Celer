<?php

class EventImage_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'EventImage';
	const INDEX = 'id';
	const FK_Event = 'EventImage.id_event';

	public $id;
	public $id_event;
	public $image;
	public $size;
	//Additional
	public $id_category;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_event' => 'id_event',
			'image' => 'image',
			'size' => 'size',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Event_Table = $TABLE_PREFIX . Event_Entity::TABLE_NAME;
		$FK_Event = $TABLE_PREFIX . self::FK_Event;

		$fpdo = new FluentPDO($pdo);
		$join_event = $Event_Table . ' ON ' . $Event_Table . '.' . Event_Entity::INDEX . ' = ' . $FK_Event;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_event)
			->select("{$TABLE_PREFIX}Event.id_category");
		return $query;
	}
}
