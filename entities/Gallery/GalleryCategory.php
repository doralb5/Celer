<?php

class GalleryCategory_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'GalleryCategory';
	const INDEX = 'id';

	public $id;
	public $category;
	public $description;
	public $image;
	public $enabled;
	public $total_images;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'category' => 'category',
			'description' => 'description',
			'image' => 'image',
			'enabled' => 'enabled',
		);
	}
}
