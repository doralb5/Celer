<?php

require_once DOCROOT . ENTITIES_PATH . 'Gallery/GalleryCategory.php';

class GalleryItem_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'GalleryItem';
	const INDEX = 'id';
	const FK_GalleryCategory = 'GalleryItem.id_category';

	public $id;
	public $title;
	public $description;
	public $item_name;
	public $id_category;
	public $publish_date;
	public $enabled = '1';
	public $size;
	public $type = 'image';
	public $path_type = 'local';
	public $thumbnail;
	public $counter_views = 0;
	public $category;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'title' => 'title',
			'description' => 'description',
			'item_name' => 'item_name',
			'id_category' => 'id_category',
			'publish_date' => 'publish_date',
			'enabled' => 'enabled',
			'size' => 'size',
			'type' => 'type',
			'path_type' => 'path_type',
			'thumbnail' => 'thumbnail',
			'counter_views' => 'counter_views',
		);
		$this->publish_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);
		$GalleryCategory_Table = $TABLE_PREFIX . GalleryCategory_Entity::TABLE_NAME;
		$FK_GalleryCategory = $TABLE_PREFIX . self::FK_GalleryCategory;

		$join_category = $GalleryCategory_Table . ' ON ' . $GalleryCategory_Table . '.' . GalleryCategory_Entity::INDEX . ' = ' . $FK_GalleryCategory;
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_category)
			->select("{$TABLE_PREFIX}GalleryCategory.category");
		return $query;
	}
}
