<?php

class City_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'City';
	const INDEX = 'id';
	const FK_Country = 'City.id_country';

	public $id;
	public $id_country;
	public $city;
	public $country;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'city' => 'city',
			'id_country' => 'id_country',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Country_Table = $TABLE_PREFIX . Country_Entity::TABLE_NAME;
		$FK_Country = $TABLE_PREFIX . self::FK_Country;

		$join_country = $Country_Table . ' ON ' . $Country_Table . '.' . Country_Entity::INDEX . ' = ' . $FK_Country;

		$fpdo = new FluentPDO($pdo);

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->innerJoin($join_country)
			->select("{$TABLE_PREFIX}Country.country");
		return $query;
	}
}
