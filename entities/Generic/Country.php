<?php

require_once DOCROOT . ENTITIES_PATH . 'Generic/City.php';

class Country_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Country';
	const INDEX = 'id';

	public $id;
	public $country;
}
