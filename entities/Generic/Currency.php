<?php

class Currency_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Currency';
	const INDEX = 'id';

	public $id;
	public $currency;
	public $symbol;
}
