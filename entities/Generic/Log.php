<?php

class Log_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Log';
	const INDEX = 'id';

	public $id;
	public $id_user;
	public $username;
	public $date;
	public $object;
	public $id_object;
	public $action;
	public $description;
	public $ip;
	public $user_agent;
	public $session_id;

	public function __construct($index = '')
	{
		parent::__construct($index);

		$loggeduser = UserAuth::getLoginSession();

		if (!is_null($loggeduser)) {
			$this->id_user = $loggeduser['id'];
			$this->username = $loggeduser['username'];
		} else {
			$this->id_user = null;
			$this->username = '';
		}

		$this->date = date('Y-m-d H:i:s');
		//$this->ip = Utils::get_client_ip();
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$this->user_agent = $_SERVER['HTTP_USER_AGENT'];
		$this->session_id = session_id();
	}
}
