<?php

class Setting_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Setting';
	const INDEX = 'id';

	public $id;
	public $field;
	public $value;
}
