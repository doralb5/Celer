<?php

require_once DOCROOT . ENTITIES_PATH . 'Registries/Lead.php';
require_once DOCROOT . ENTITIES_PATH . 'User/User.php';

class InstantCall_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'InstantCall';
	const INDEX = 'id';
	const FK_User = 'InstantCall.id_user';
	const FK_User_Operator = 'InstantCall.id_operator';
	const FK_Lead = 'InstantCall.id_lead';

	public $id;
	public $id_user;
	public $creation_date;
	public $id_lead;
	public $result;
	public $id_operator;
	public $note;
	public $lead_fullname;
	public $user_fullname;
	public $operator_fullname;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'id_user' => 'id_user',
			'creation_date' => 'creation_date',
			'id_lead' => 'id_lead',
			'result' => 'result',
			'id_operator' => 'id_operator',
			'note' => 'note',
		);
		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);

		$join_user = User_Entity::TABLE_NAME . ' ON ' . User_Entity::TABLE_NAME . '. ' . User_Entity::INDEX . ' = ' . self::FK_User;
		$join_operator = User_Entity::TABLE_NAME . '  Operator  ON Operator. ' . User_Entity::INDEX . ' = ' . self::FK_User_Operator;
		$join_lead = Lead_Entity::TABLE_NAME . ' ON ' . Lead_Entity::TABLE_NAME . '. ' . Lead_Entity::INDEX . ' = ' . self::FK_Lead;

		$query = $fpdo->from(self::TABLE_NAME)
			->leftJoin($join_user)
			->leftJoin($join_operator)
			->leftJoin($join_lead)
			->select('CONCAT(User.firstname, " ", User.lastname) AS user_fullname')
			->select('CONCAT(Operator.firstname, " ", Operator.lastname) AS operator_fullname')
			->select('CONCAT(Lead.firstname, " ", Lead.lastname) AS lead_fullname');
		return $query;
	}
}
