<?php

require_once DOCROOT . ENTITIES_PATH . 'Menu/MenuItem.php';
require_once DOCROOT . ENTITIES_PATH . 'Menu/MenuDetail.php';

class Menu_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Menu';
	const INDEX = 'id';

	public $id;
	public $name;
	public $main;
	public $title;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'main' => 'main',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$MenuDetail_Table = $TABLE_PREFIX . MenuDetail_Entity::TABLE_NAME;
		$Menu_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$FK_Menu = $TABLE_PREFIX . MenuDetail_Entity::FK_Menu;

		$fpdo = new FluentPDO($pdo);

		$joinDetail = $MenuDetail_Table . ' ON ' . $Menu_Table . '.' . self::INDEX . ' = ' . $FK_Menu;
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($joinDetail)
			->select("{$TABLE_PREFIX}MenuDetail.title");
		return $query;
	}
}
