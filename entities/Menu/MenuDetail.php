<?php

require_once DOCROOT . ENTITIES_PATH . 'Menu/Menu.php';

class MenuDetail_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'MenuDetail';
	const INDEX = 'id';
	const FK_Menu = 'MenuDetail.id_menu';

	public $id;
	public $id_menu;
	public $title;
	public $lang;
	//Additional
	public $menu;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_menu' => 'id_menu',
			'title' => 'title',
			'lang' => 'lang',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Menu_Table = $TABLE_PREFIX . Menu_Entity::TABLE_NAME;
		$FK_Menu = $TABLE_PREFIX . self::FK_Menu;

		$fpdo = new FluentPDO($pdo);

		$joinMenu = $Menu_Table . ' ON ' . $Menu_Table . '.' . Menu_Entity::INDEX . ' = ' . $FK_Menu;
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($joinMenu)
			->select("{$TABLE_PREFIX}Menu.name AS menu");
		return $query;
	}
}
