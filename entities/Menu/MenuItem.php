<?php

require_once DOCROOT . ENTITIES_PATH . 'Menu/Menu.php';

class MenuItem_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'MenuItem';
	const INDEX = 'id';
	const FK_Menu = 'MenuItem.id_menu';

	public $id;
	public $id_menu;
	public $item;
	public $lang;
	public $type;
	public $target;
	public $parameters;
	public $parent_id;
	public $sorting;
	public $enabled = 1;
	public $image;
	//Additional
	public $menu;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_menu' => 'id_menu',
			'item' => 'item',
			'lang' => 'lang',
			'type' => 'type',
			'target' => 'target',
			'parameters' => 'parameters',
			'parent_id' => 'parent_id',
			'sorting' => 'sorting',
			'enabled' => 'enabled',
			'image' => 'image',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Menu_Table = $TABLE_PREFIX . Menu_Entity::TABLE_NAME;
		$FK_Menu = $TABLE_PREFIX . self::FK_Menu;

		$fpdo = new FluentPDO($pdo);

		$joinMenu = $Menu_Table . ' ON ' . $Menu_Table . '.' . Menu_Entity::INDEX . ' = ' . $FK_Menu;
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($joinMenu)
			->select("{$TABLE_PREFIX}Menu.name AS menu");
		return $query;
	}
}
