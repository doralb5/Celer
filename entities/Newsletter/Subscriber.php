<?php

require_once DOCROOT . ENTITIES_PATH . 'Newsletter/SubscriberCategory.php';
require_once DOCROOT . ENTITIES_PATH . 'Newsletter/SubscriberSCateg.php';

class Subscriber_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Subscriber';
	const INDEX = 'id';

	public $id;
	public $email;
	public $enabled = 1;
	public $deleted = 0;
	public $creation_date;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'email' => 'email',
			'enabled' => 'enabled',
			'deleted' => 'deleted',
			'creation_date' => 'creation_date',
		);

		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->disableSmartJoin();
		return $query;
	}
}
