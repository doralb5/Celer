<?php

class SubscriberSCateg_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'SubscriberSCateg';
	const INDEX = 'id';
	const FK_Subscriber = 'SubscriberSCateg.id_subscriber';
	const FK_SubscriberCategory = 'SubscriberSCateg.id_category';

	public $id;
	public $id_subscriber;
	public $id_category;
	//Additional
	public $category;
	public $email;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_subscriber' => 'id_subscriber',
			'id_category' => 'id_category',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Subscriber_Table = $TABLE_PREFIX . Subscriber_Entity::TABLE_NAME;
		$SubscriberCategory = $TABLE_PREFIX . SubscriberCategory_Entity::TABLE_NAME;
		$FK_Subscriber = $TABLE_PREFIX . self::FK_Subscriber;
		$FK_SubscriberCategory = $TABLE_PREFIX . self::FK_SubscriberCategory;

		$fpdo = new FluentPDO($pdo);

		$join_subscriber = $Subscriber_Table . ' ON ' . $Subscriber_Table . '.' . Subscriber_Entity::INDEX . ' = ' . $FK_Subscriber;
		$join_subsCategory = $SubscriberCategory . ' ON ' . $SubscriberCategory . '.' . SubscriberCategory_Entity::INDEX . ' = ' . $FK_SubscriberCategory;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->innerJoin($join_subscriber)
			->innerJoin($join_subsCategory)
			->select("{$TABLE_PREFIX}SubscriberCategory.category")
			->select("{$TABLE_PREFIX}Subscriber.email");
		return $query;
	}
}
