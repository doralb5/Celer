<?php

require_once DOCROOT . ENTITIES_PATH . 'Page/Module.php';
require_once DOCROOT . ENTITIES_PATH . 'Page/PageBlock.php';
require_once DOCROOT . ENTITIES_PATH . 'Page/Page.php';

class Block_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Block';
	const INDEX = 'id';
	const FK_Module = 'Block.id_module';

	public $id;
	public $id_module;
	public $position;
	public $parameters;
	public $class;
	public $sorting = 10;
	public $enabled = '1';
	public $always = '0';
	public $module_name;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_module' => 'id_module',
			'position' => 'position',
			'parameters' => 'parameters',
			'class' => 'class',
			'sorting' => 'sorting',
			'enabled' => 'enabled',
			'always' => 'always',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Block_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$Module_Table = $TABLE_PREFIX . Module_Entity::TABLE_NAME;
		$PageBlock_Table = $TABLE_PREFIX . PageBlock_Entity::TABLE_NAME;
		$Page_Table = $TABLE_PREFIX . Page_Entity::TABLE_NAME;
		$FK_Module = $TABLE_PREFIX . self::FK_Module;
		$FK_Block = $TABLE_PREFIX . PageBlock_Entity::FK_Block;
		$FK_Page = $Page_Table . '.' . Page_Entity::INDEX;

		$fpdo = new FluentPDO($pdo);

		$join_module = $Module_Table . ' ON ' . $Module_Table . '.' . Module_Entity::INDEX . ' = ' . $FK_Module;
		$join_pageblock = $PageBlock_Table . ' ON ' . $Block_Table . '.' . self::INDEX . ' = ' . $FK_Block;
		$join_page = $Page_Table . ' ON ' . $TABLE_PREFIX . PageBlock_Entity::FK_Page . ' = ' . $FK_Page;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_module)
			->leftJoin($join_pageblock)
			->leftJoin($join_page)
			->select("{$TABLE_PREFIX}Module.name AS module_name");

		//echo $query->getQuery();
		return $query;
	}
}
