<?php

class Module_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Module';
	const INDEX = 'id';

	public $id;
	public $name;
	public $description;
	public $enabled = '0';

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
