<?php

require_once DOCROOT . ENTITIES_PATH . 'Page/PageContent.php';
require_once DOCROOT . ENTITIES_PATH . 'Page/PageAlias.php';
require_once DOCROOT . ENTITIES_PATH . 'Component/Component.php';

class Page_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Page';
	const INDEX = 'id';
	const FK_Component = 'Page.id_component';

	public $id;
	public $name;
	public $private = '0';
	public $home = '0';
	public $robots = 'INDEX,FOLLOW';
	public $id_component;
	public $action;
	public $action_params;
	public $parameters;
	public $layout = 'default';
	public $enabled = '1';
	public $type = '1';
	public $creation_date = '';
	public $update_date;
	public $show_content_heading = '1';
	public $alias;
	public $title;
	public $lang;
	public $meta_title;
	public $meta_description;
	public $meta_keywords;
	public $component_name;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'private' => 'private',
			'home' => 'home',
			'robots' => 'robots',
			'id_component' => 'id_component',
			'action' => 'action',
			'action_params' => 'action_params',
			'parameters' => 'parameters',
			'layout' => 'layout',
			'enabled' => 'enabled',
			'type' => 'type',
			'creation_date' => 'creation_date',
			'update_date' => 'update_date',
			'show_content_heading' => 'show_content_heading',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Page_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$PageContent_Table = $TABLE_PREFIX . PageContent_Entity::TABLE_NAME;
		$Component_Table = $TABLE_PREFIX . Component_Entity::TABLE_NAME;
		$FK_Page = $TABLE_PREFIX . PageContent_Entity::FK_Page;
		$FK_Component = $TABLE_PREFIX . self::FK_Component;

		$fpdo = new FluentPDO($pdo);

		$join_pagecontent = $PageContent_Table . ' ON ' . $FK_Page . ' = ' . $Page_Table . '.' . self::INDEX;
		$join_component = $Component_Table . ' ON ' . $Component_Table . '.' . Component_Entity::INDEX . ' = ' . $FK_Component;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_component)
			->leftJoin($join_pagecontent)
			->select("{$TABLE_PREFIX}PageContent.alias, {$TABLE_PREFIX}PageContent.title, {$TABLE_PREFIX}PageContent.lang, {$TABLE_PREFIX}PageContent.meta_title, {$TABLE_PREFIX}PageContent.meta_description, {$TABLE_PREFIX}PageContent.meta_keywords")
			->select("{$TABLE_PREFIX}Component.name AS component_name");

		return $query;
	}
}
