<?php

class PageAlias_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'PageAlias';
	const INDEX = 'id';

	public $id;
	public $alias;
	public $destination;
	public $id_page;
	public $lang;

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->disableSmartJoin();
		return $query;
	}
}
