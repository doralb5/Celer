<?php

class PageBlock_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'PageBlock';
	const INDEX = 'id';
	const FK_Page = 'PageBlock.id_page';
	const FK_Block = 'PageBlock.id_block';

	public $id;
	public $id_page;
	public $id_block;
}
