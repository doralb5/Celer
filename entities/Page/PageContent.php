<?php

class PageContent_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'PageContent';
	const INDEX = 'id';
	const FK_Page = 'PageContent.id_page';

	public $id;
	public $id_page;
	public $alias = '';
	public $lang = 'en';
	public $title = '';
	public $meta_title;
	public $meta_description;
	public $meta_keywords;
	public $content;
}
