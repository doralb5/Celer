<?php

require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstateContract.php';
require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstateTypology.php';
require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstateCurrency.php';
require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstateImage.php';
require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstateFloor.php';
require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstateHeating.php';
require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstateBuildingState.php';
require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstatePdfFlyer.php';
require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstatePdfFlyerItems.php';

class RealEstate_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'RealEstate';
	const INDEX = 'id';
	const FK_RealEstateContract = 'RealEstate.id_contract';
	const FK_RealEstateTypology = 'RealEstate.id_typology';
	const FK_RealEstateCurrency = 'RealEstate.id_currency';
	const FK_RealEstateFloor = 'RealEstate.id_floor';
	const FK_RealEstateHeating = 'RealEstate.id_heating';
	const FK_RealEstateBuildingState = 'RealEstate.id_building_state';
	const FK_User = 'RealEstate.id_user';

	public $id;
	public $title;
	public $address;
	public $id_contract;
	public $id_typology;
	public $price;
	public $id_currency;
	public $description;
	public $publish_date;
	public $image;
	public $id_user;
	public $coordinates;
	public $enabled = 1;
	public $featured = 0;
	public $notified = 0;
	public $hide_price = 0;
	public $location;
	public $email;
	public $square_meter;
	public $rooms;
	public $bathrooms;
	public $garden;
	public $furnished;
	public $balcony;
	public $terrace;
	public $id_heating;
	public $id_floor;
	public $id_building_state;
	public $zip_code;
	public $locality;
	public $min_price;
	public $max_price;
	public $ref_code;
	public $street;
	public $energetic_class;
	public $images = array();
	public $contract_type;
	public $typology;
	public $currency;
	public $floor;
	public $heating;
	public $building;
	public $fullAddress;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'title' => 'title',
			'address' => 'address',
			'id_contract' => 'id_contract',
			'id_typology' => 'id_typology',
			'price' => 'price',
			'id_currency' => 'id_currency',
			'publish_date' => 'publish_date',
			'description' => 'description',
			'image' => 'image',
			'id_user' => 'id_user',
			'coordinates' => 'coordinates',
			'enabled' => 'enabled',
			'featured' => 'featured',
			'location' => 'location',
			'square_meter' => 'square_meter',
			'rooms' => 'rooms',
			'notified' => 'notified',
			'bathrooms' => 'bathrooms',
			'garden' => 'garden',
			'furnished' => 'furnished',
			'balcony' => 'balcony',
			'terrace' => 'terrace',
			'id_heating' => 'id_heating',
			'id_floor' => 'id_floor',
			'id_building_state' => 'id_building_state',
			'hide_price' => 'hide_price',
			'zip_code' => 'zip_code',
			'ref_code' => 'ref_code',
			'locality' => 'locality',
			'street' => 'street',
			'energetic_class' => 'energetic_class'
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$RealEstate_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$RealEstateContract_Table = $TABLE_PREFIX . RealEstateContract_Entity::TABLE_NAME;
		$RealEstateTypology_Table = $TABLE_PREFIX . RealEstateTypology_Entity::TABLE_NAME;
		$RealEstateCurrency_Table = $TABLE_PREFIX . RealEstateCurrency_Entity::TABLE_NAME;
		$RealEstateFloor_Table = $TABLE_PREFIX . RealEstateFloor_Entity::TABLE_NAME;
		$RealEstateHeating_Table = $TABLE_PREFIX . RealEstateHeating_Entity::TABLE_NAME;
		$RealEstateBuildingState_Table = $TABLE_PREFIX . RealEstateBuildingState_Entity::TABLE_NAME;
		$User_Table = $TABLE_PREFIX . 'User';
		$FK_RealEstateContract = $TABLE_PREFIX . self::FK_RealEstateContract;
		$FK_RealEstateTypology = $TABLE_PREFIX . self::FK_RealEstateTypology;
		$FK_RealEstateCurrency = $TABLE_PREFIX . self::FK_RealEstateCurrency;
		$FK_RealEstateFloor = $TABLE_PREFIX . self::FK_RealEstateFloor;
		$FK_RealEstateHeating = $TABLE_PREFIX . self::FK_RealEstateHeating;
		$FK_RealEstateBuildingState = $TABLE_PREFIX . self::FK_RealEstateBuildingState;
		$FK_User = $TABLE_PREFIX . self::FK_User;
		$fpdo = new FluentPDO($pdo);
		$join_contract = $RealEstateContract_Table . ' ON ' . $RealEstateContract_Table . '.' . RealEstateContract_Entity::INDEX . ' = ' . $FK_RealEstateContract;
		$join_typology = $RealEstateTypology_Table . ' ON ' . $RealEstateTypology_Table . '.' . RealEstateTypology_Entity::INDEX . ' = ' . $FK_RealEstateTypology;
		$join_currency = $RealEstateCurrency_Table . ' ON ' . $RealEstateCurrency_Table . '.' . RealEstateCurrency_Entity::INDEX . ' = ' . $FK_RealEstateCurrency;
		$join_floor = $RealEstateFloor_Table . ' ON ' . $RealEstateFloor_Table . '.' . RealEstateFloor_Entity::INDEX . ' = ' . $FK_RealEstateFloor;
		$join_heating = $RealEstateHeating_Table . ' ON ' . $RealEstateHeating_Table . '.' . RealEstateHeating_Entity::INDEX . ' = ' . $FK_RealEstateHeating;
		$join_building = $RealEstateBuildingState_Table . ' ON ' . $RealEstateBuildingState_Table . '.' . RealEstateBuildingState_Entity::INDEX . ' = ' . $FK_RealEstateBuildingState;
		$join_user = $User_Table . ' ON ' . $User_Table . '.' . 'id' . ' = ' . $FK_User;
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_contract)
			->leftJoin($join_typology)
			->leftJoin($join_currency)
			->leftJoin($join_user)
			->leftJoin($join_floor)
			->leftJoin($join_heating)
			->leftJoin($join_building)
			->select("{$TABLE_PREFIX}RealEstateContract.type as contract_type")
			->select("MAX({$TABLE_PREFIX}RealEstate.price) AS max_price")
			->select("MIN({$TABLE_PREFIX}RealEstate.price) AS min_price")
			->select("{$TABLE_PREFIX}RealEstateTypology.type as typology")
			->select("{$TABLE_PREFIX}RealEstateCurrency.type as currency")
			->select("{$TABLE_PREFIX}RealEstateFloor.name as floor")
			->select("{$TABLE_PREFIX}RealEstateHeating.name as heating")
			->select("{$TABLE_PREFIX}RealEstateBuildingState.type as building")
			->select("CONCAT({$RealEstate_Table}.location,', ',{$RealEstate_Table}.zip_code,', ',{$RealEstate_Table}.locality,', ',{$RealEstate_Table}.address) AS fullAddress")
			->select("CONCAT($User_Table.firstname, ' ', $User_Table.lastname) AS user_fullname");
		//echo '<!--' . $query->getQuery() . '-->';
		return $query;
	}
}
