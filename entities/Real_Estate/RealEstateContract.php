<?php

class RealEstateContract_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'RealEstateContract';
	const INDEX = 'id';

	public $id;
	public $type;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'type' => 'type',
		);
	}
}
