<?php

class RealEstateImage_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'RealEstateIMage';
	const INDEX = 'id';
	const FK_RealEstate = 'RealEstateIMage.id_realestate';

	public $id;
	public $id_realestate;
	public $image;
	public $size;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_realestate' => 'id_realestate',
			'image' => 'image',
			'size' => 'size',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$RealEstate_Table = $TABLE_PREFIX . RealEstate_Entity::TABLE_NAME;
		$FK_RealEstate = $TABLE_PREFIX . self::FK_RealEstate;

		$fpdo = new FluentPDO($pdo);
		$join_realestate = $RealEstate_Table . ' ON ' . $RealEstate_Table . '.' . RealEstate_Entity::INDEX . ' = ' . $FK_RealEstate;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_realestate)
			->select("{$TABLE_PREFIX}RealEstate.id_contract");
		return $query;
	}
}
