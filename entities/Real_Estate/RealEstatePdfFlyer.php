<?php

class RealEstatePdfFlyer_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'RealEstatePdfFlyer';
	const INDEX = 'id';

	public $id;
	public $name;
	public $creation_date;
	public $last_modification;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'creation_date' => 'creation_date',
			'last_modification' => 'last_modification'
		);
		$this->last_modification = date('Y-m-d H:i:s', time());
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
