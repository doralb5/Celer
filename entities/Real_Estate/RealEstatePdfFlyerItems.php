<?php

class RealEstatePdfFlyerItems_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'RealEstatePdfFlyerItems';
	const INDEX = 'id';

	public $id;
	public $id_flyer;
	public $id_real_estate;
	public $short_description;
	public $image;
	public $title;
	public $address;
	public $location;
	public $square_meter;
	public $rooms;
	public $locality;
	public $energetic_class;
	public $price;
	public $description;
	public $ref_code;
	public $typology;
	public $contract;
	public $hide_price;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_flyer' => 'id_flyer',
			'short_description' => 'short_description',
			'id_real_estate' => 'id_real_estate'
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);

		$joinRealEstate = ' cms_RealEstate ON cms_RealEstatePdfFlyerItems.id_real_estate = cms_RealEstate.id';
		$joinRealEstateContract = ' cms_RealEstateContract ON cms_RealEstateContract.id = cms_RealEstate.id_contract';
		$joinRealEstateTypology = ' cms_RealEstateTypology ON cms_RealEstateTypology.id = cms_RealEstate.id_typology';
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($joinRealEstate)
			->leftJoin($joinRealEstateContract)
			->leftJoin($joinRealEstateTypology)
			->select('cms_RealEstate.image as image , cms_RealEstate.title as title ')
			->select('cms_RealEstate.address as address')
			->select('cms_RealEstate.description as description')
			->select('cms_RealEstate.ref_code as ref_code')
			->select('cms_RealEstate.location as location')
			->select('cms_RealEstate.square_meter as square_meter')
			->select('cms_RealEstate.rooms as rooms')
			->select('cms_RealEstate.locality as locality')
			->select('cms_RealEstate.energetic_class as energetic_class')
			->select('cms_RealEstate.price as price')
			->select('cms_RealEstateContract.type as contract')
			->select('cms_RealEstateTypology.type as typology')
			->select('cms_RealEstate.hide_price as hide_price');
		return $query;
	}
}
