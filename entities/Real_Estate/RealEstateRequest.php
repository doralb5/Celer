<?php

class RealEstateRequest_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'RealEstate_Request';
	const INDEX = 'id';

	public $id;
	public $firstname;
	public $lastname;
	public $tel;
	public $cel;
	public $email;
	public $location;
	public $id_contract;
	public $id_typology;
	public $surface;
	public $description;
	public $creation_date;
	public $rooms;
	public $garden;
	public $furnished;
	public $balcony;
	public $terrace;
	public $city;
	public $zip_code;
	public $street;
	public $address;
	public $bathrooms;
	public $id_floor;
	public $id_building;
	public $id_heating;
	public $price;
	public $energetic_class;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'firstname' => 'firstname',
			'lastname' => 'lastname',
			'tel' => 'tel',
			'cel' => 'cel',
			'email' => 'email',
			'location' => 'location',
			'id_contract' => 'id_contract',
			'id_typology' => 'id_typology',
			'surface' => 'surface',
			'creation_date' => 'creation_date',
			'description' => 'description',
			'rooms' => 'rooms',
			'garden' => 'garden',
			'furnished' => 'furnished',
			'balcony' => 'balcony',
			'terrace' => 'terrace',
			'city' => 'city',
			'zip_code' => 'zip_code',
			'street' => 'street',
			'address' => 'address',
			'bathrooms' => 'bathrooms',
			'id_floor' => 'id_floor',
			'id_building' => 'id_building',
			'id_heating' => 'id_heating',
			'price' => 'price',
			'energetic_class' => 'energetic_class',
		);
		$this->creation_date = date('Y-m-d H:i:s', time());
	}
}
