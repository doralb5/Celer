<?php

class RealEstateTypology_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'RealEstateTypology';
	const INDEX = 'id';

	public $id;
	public $type;
	public $enabled;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'type' => 'type',
			'enabled' => 'enabled',
		);
	}
}
