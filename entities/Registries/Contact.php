<?php

require_once DOCROOT . ENTITIES_PATH . 'Registries/LeadSource.php';
require_once DOCROOT . ENTITIES_PATH . 'Registries/Company.php';

class Contact_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'Contact';
	const INDEX = 'id';
	const FK_Company = 'Contact.id_company';
	const FK_User = 'Contact.id_user_assigned';
	const FK_LeadSource = 'Contact.id_source';
	const FK_Campaign = 'Contact.id_campaign';

	public $id;
	public $code;
	public $id_company;
	public $department;
	public $title;
	public $firstname;
	public $lastname;
	public $birthday;
	public $tax_code;
	public $state;
	public $country;
	public $city;
	public $zip;
	public $address;
	public $phone;
	public $mobile;
	public $fax;
	public $website;
	public $email;
	public $description;
	public $creation_date;
	public $id_user_assigned;
	public $id_source;
	public $id_campaign;
	public $name;
	public $company_name;
	public $source;
	public $user_fullname;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'code' => 'code',
			'id_company' => 'id_company',
			'title' => 'title',
			'department' => 'department',
			'firstname' => 'firstname',
			'lastname' => 'lastname',
			'birthday' => 'birthday',
			'tax_code' => 'tax_code',
			'state' => 'state',
			'country' => 'country',
			'city' => 'city',
			'zip' => 'zip',
			'address' => 'address',
			'phone' => 'phone',
			'mobile' => 'mobile',
			'fax' => 'fax',
			'website' => 'website',
			'email' => 'email',
			'description' => 'description',
			'creation_date' => 'creation_date',
			'id_user_assigned' => 'id_user_assigned',
			'id_source' => 'id_source',
			'id_campaign' => 'id_campaign',
		);

		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);

		$join_company = Company_Entity::TABLE_NAME . ' ON ' . Company_Entity::TABLE_NAME . '. ' . Company_Entity::INDEX . ' = ' . self::FK_Company;
		$join_source = LeadSource_Entity::TABLE_NAME . ' ON ' . LeadSource_Entity::TABLE_NAME . '. ' . LeadSource_Entity::INDEX . ' = ' . self::FK_LeadSource;
		$join_user = User_Entity::TABLE_NAME . ' ON ' . User_Entity::TABLE_NAME . '. ' . User_Entity::INDEX . ' = ' . self::FK_User;

		$query = $fpdo->from(self::TABLE_NAME)
			->leftJoin($join_company)
			->leftJoin($join_source)
			->leftJoin($join_user)
			->select("CONCAT(Contact.firstname,' ', Contact.lastname) AS name")
			->select('Company.name AS company_name')
			->select('CONCAT(User.firstname, " ", User.lastname) AS user_fullname')
			->select('LeadSource.source AS source');
		return $query;
	}
}
