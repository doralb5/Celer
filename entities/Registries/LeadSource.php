<?php

class LeadSource_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'LeadSource';
	const INDEX = 'id';

	public $id;
	public $source;
}
