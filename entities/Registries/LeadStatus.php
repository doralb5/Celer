<?php

class LeadStatus_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'LeadStatus';
	const INDEX = 'id';

	public $id;
	public $status;
}
