<?php

class shop_Brand_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'shop_Brand';
	const INDEX = 'id';

	public $id;
	public $brand;
	public $description;
	public $image;
	public $total_products;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'brand' => 'brand',
			'description' => 'description',
			'image' => 'image',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
