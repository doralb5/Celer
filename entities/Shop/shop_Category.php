<?php

class shop_Category_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'shop_Category';
	const INDEX = 'id';

	public $id;
	public $category;
	public $description;
	public $image;
	public $parent_id;
	public $sorting;
	public $total_products;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->parent_id = null;
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'category' => 'category',
			'description' => 'description',
			'image' => 'image',
			'parent_id' => 'parent_id',
			'sorting' => 'sorting'
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$Product_tbl = $TABLE_PREFIX . 'shop_Product';
		$Category_tbl = $TABLE_PREFIX . 'shop_Category';

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->select("(SELECT COUNT(*) FROM $Product_tbl WHERE $Product_tbl.id_category = $Category_tbl.id) AS total_products")
			->disableSmartJoin();
		return $query;
	}
}
