<?php

class shop_Feature_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'shop_Feature';
	const INDEX = 'id';
	const FK_shop_ProductType = 'shop_Feature.id_type';

	public $id;
	public $feature;
	public $id_type;
	public $type;
	public $value;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'feature' => 'feature',
			'id_type' => 'id_type',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$Feature_tbl = $TABLE_PREFIX . self::TABLE_NAME;
		$Type_tbl = $TABLE_PREFIX . shop_ProductType_Entity::TABLE_NAME;
		$FeatVal_tbl = $TABLE_PREFIX . shop_ProductFeature_Entity::TABLE_NAME;
		$FK_Type = $TABLE_PREFIX . self::FK_shop_ProductType;
		$FK_Feat = $TABLE_PREFIX . shop_ProductFeature_Entity::FK_shop_Feature;

		$join_type = $Type_tbl . ' ON ' . $Type_tbl . '.' . shop_ProductType_Entity::INDEX . ' = ' . $FK_Type;
		//$join_featval = $FeatVal_tbl . " ON " . $FK_Feat . " = " . $Feature_tbl . '.' . self::INDEX;

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_type)
				//->leftJoin($join_featval)
				//->select("$FeatVal_tbl.value")
			->select("$Type_tbl.type")
			->disableSmartJoin();
		return $query;
	}
}
