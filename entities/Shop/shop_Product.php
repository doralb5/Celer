<?php

require_once DOCROOT . ENTITIES_PATH . 'Shop/shop_Brand.php';
require_once DOCROOT . ENTITIES_PATH . 'Shop/shop_Category.php';
require_once DOCROOT . ENTITIES_PATH . 'Shop/shop_ProductType.php';
require_once DOCROOT . ENTITIES_PATH . 'Shop/shop_ProductImage.php';
require_once DOCROOT . ENTITIES_PATH . 'Shop/shop_Feature.php';
require_once DOCROOT . ENTITIES_PATH . 'Shop/shop_ProductFeature.php';
require_once DOCROOT . ENTITIES_PATH . 'Shop/shop_ProductModel.php';

class shop_Product_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'shop_Product';
	const INDEX = 'id';
	const FK_shop_Category = 'shop_Product.id_category';
	const FK_shop_ProductModel = 'shop_Product.id_model';
	const FK_shop_ProductType = 'shop_Product.id_type';

	public $id;
	public $name;
	public $description;
	public $status;
	public $image;
	public $price;
	public $final_price;
	public $id_category;
	public $id_model;
	public $id_type;
	public $id_brand;
	public $creation_date;
	public $publish_date;
	public $featured = 0;
	public $enabled = 1;
	public $category;
	public $type;
	public $model;
	public $brand;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'description' => 'description',
			'status' => 'status',
			'image' => 'image',
			'price' => 'price',
			'final_price' => 'final_price',
			'id_category' => 'id_category',
			'id_model' => 'id_model',
			'id_type' => 'id_type',
			'id_brand' => 'id_brand',
			'creation_date' => 'creation_date',
			'publish_date' => 'publish_date',
			'featured' => 'featured',
			'enabled' => 'enabled',
		);
		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$Product_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$Category_Table = $TABLE_PREFIX . shop_Category_Entity::TABLE_NAME;
		$Type_Table = $TABLE_PREFIX . shop_ProductType_Entity::TABLE_NAME;
		$Brand_Table = $TABLE_PREFIX . shop_Brand_Entity::TABLE_NAME;
		$Model_Table = $TABLE_PREFIX . shop_ProductModel_Entity::TABLE_NAME;
		$ProdFeat_Table = $TABLE_PREFIX . shop_ProductFeature_Entity::TABLE_NAME;
		$Feature_Table = $TABLE_PREFIX . shop_Feature_Entity::TABLE_NAME;

		$FK_Category = $TABLE_PREFIX . self::FK_shop_Category;
		$FK_Model = $TABLE_PREFIX . self::FK_shop_ProductModel;
		$FK_Type = $TABLE_PREFIX . self::FK_shop_ProductType;
		$FK_Brand = $TABLE_PREFIX . shop_ProductModel_Entity::FK_Brand;
		$FK_Product = $TABLE_PREFIX . shop_ProductFeature_Entity::FK_shop_Product;
		$FK_Feature = $TABLE_PREFIX . shop_ProductFeature_Entity::FK_shop_Feature;

		$fpdo = new FluentPDO($pdo);

		$join_category = $Category_Table . ' ON ' . $Category_Table . '.' . shop_Category_Entity::INDEX . ' = ' . $FK_Category;
		$join_model = $Model_Table . ' ON ' . $Model_Table . '.' . shop_ProductModel_Entity::INDEX . ' = ' . $FK_Model;
		$join_brand = $Brand_Table . ' ON ' . $Brand_Table . '.' . shop_Brand_Entity::INDEX . ' = ' . $FK_Brand;
		$join_type = $Type_Table . ' ON ' . $Type_Table . '.' . shop_ProductType_Entity::INDEX . ' = ' . $FK_Type;
		$join_prodFeat = $ProdFeat_Table . ' ON ' . $Product_Table . '.' . self::INDEX . ' = ' . $FK_Product;
		$join_feature = $Feature_Table . ' ON ' . $Feature_Table . '.' . shop_Feature_Entity::INDEX . ' = ' . $FK_Feature;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_category)
			->leftJoin($join_model)
			->leftJoin($join_brand)
			->leftJoin($join_type)
			->leftJoin($join_prodFeat)
			->leftJoin($join_feature)
			->select("$Category_Table.category")
			->select("$Type_Table.type")
				//->select("$Model_Table.model, $Model_Table.id_brand")
			->select("$Brand_Table.brand")
			->disableSmartJoin();
		return $query;
	}
}
