<?php

class shop_ProductFeature_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'shop_ProductFeature';
	const INDEX = 'id';
	const FK_shop_Product = 'shop_ProductFeature.id_product';
	const FK_shop_Feature = 'shop_ProductFeature.id_feature';

	public $id;
	public $id_product;
	public $id_feature;
	public $value;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_product' => 'id_product',
			'id_feature' => 'id_feature',
			'value' => 'value',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
