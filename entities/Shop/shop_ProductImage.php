<?php

class shop_ProductImage_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'shop_ProductImage';
	const INDEX = 'id';

	public $id;
	public $id_product;
	public $image;
	public $size;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_product' => 'id_product',
			'image' => 'image',
			'size' => 'size',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
