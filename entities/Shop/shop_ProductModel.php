<?php

class shop_ProductModel_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'shop_ProductModel';
	const INDEX = 'id';
	const FK_Brand = 'shop_ProductModel.id_brand';

	public $id;
	public $model;
	public $id_brand;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'model' => 'model',
			'id_brand' => 'id_brand',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->disableSmartJoin();
		return $query;
	}
}
