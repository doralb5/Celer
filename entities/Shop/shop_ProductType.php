<?php

class shop_ProductType_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'shop_ProductType';
	const INDEX = 'id';

	public $id;
	public $type;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'type' => 'type',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
