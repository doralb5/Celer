<?php

require_once DOCROOT . ENTITIES_PATH . 'StaticBlock/StaticBlockContent.php';

class StaticBlock_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'StaticBlock';
	const INDEX = 'id';

	public $id;
	public $name;
	public $enabled = '1';
	public $creation_date;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'name' => 'name',
			'enabled' => 'enabled',
			'creation_date' => 'creation_date',
		);

		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;

		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME);
		return $query;
	}
}
