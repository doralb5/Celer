<?php

require_once DOCROOT . ENTITIES_PATH . 'StaticBlock/StaticBlock.php';

class StaticBlockContent_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'StaticBlockContent';
	const INDEX = 'id';
	const FK_StaticBlock = 'StaticBlockContent.id_static_block';

	public $id;
	public $id_static_block;
	public $lang;
	public $title;
	public $content;
	public $name;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_static_block' => 'id_static_block',
			'lang' => 'lang',
			'title' => 'title',
			'content' => 'content',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$StaticBlock_Table = $TABLE_PREFIX . StaticBlock_Entity::TABLE_NAME;
		$FK_StaticBlock = $TABLE_PREFIX . self::FK_StaticBlock;

		$fpdo = new FluentPDO($pdo);

		$join_staticblock = $StaticBlock_Table . ' ON ' . $StaticBlock_Table . '.' . StaticBlock_Entity::INDEX . ' = ' . $FK_StaticBlock;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_staticblock)
			->select("{$TABLE_PREFIX}StaticBlock.name");
		return $query;
	}
}
