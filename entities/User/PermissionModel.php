<?php

class PermissionModel_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'PermissionModel';
	const INDEX = 'id';
	const FK_Model = 'PermissionModel.id_model';
	const FK_Component = 'PermissionModel.id_component';

	public $id;
	public $id_model;
	public $id_component;
	public $component_name;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'id_model' => 'id_model',
			'id_component' => 'id_component',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$Component_Table = $TABLE_PREFIX . Component_Entity::TABLE_NAME;
		$FK_Component = $TABLE_PREFIX . self::FK_Component;

		$fpdo = new FluentPDO($pdo);

		$join_component = "$Component_Table ON $Component_Table." . Component_Entity::INDEX . " = $FK_Component";

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_component)
			->select("$Component_Table.name AS component_name");
		return $query;
	}
}
