<?php

require_once DOCROOT . ENTITIES_PATH . 'User/RemoteUserPermission.php';

class RemoteUser_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'RemoteUser';
	const INDEX = 'id';

	public $id;
	public $username;
	public $password;
	public $creation_date;
	public $enabled = 0;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'username' => 'username',
			'password' => 'password',
			'creation_date' => 'creation_date',
			'enabled' => 'enabled',
		);
		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from(self::TABLE_NAME);
		return $query;
	}
}
