<?php

class RemoteUserPermission_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'RemoteUserPermission';
	const INDEX = 'id';
	const FK_User = 'RemoteUserPermission.id_user';
	const FK_Component = 'RemoteUserPermission.id_component';

	public $id;
	public $id_user;
	public $id_component;
	public $action;
	public $component_name;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'id_user' => 'id_user',
			'id_component' => 'id_component',
			'action' => 'action',
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);

		$join_comp = Component_Entity::TABLE_NAME . ' ON ' . Component_Entity::TABLE_NAME . '. ' . Component_Entity::INDEX . ' = ' . self::FK_Component;

		$query = $fpdo->from(self::TABLE_NAME)
			->leftJoin($join_comp)
			->select('Component.name AS component_name');
		return $query;
	}
}
