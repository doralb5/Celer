<?php

class RemoteUserSession_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'RemoteUserSession';
	const INDEX = 'id';
	const FK_User = 'RemoteUserSession.id_user';

	public $id;
	public $id_user;
	public $start_time;
	public $token;

	public static function getSelectQueryObj($pdo)
	{
		$fpdo = new FluentPDO($pdo);
		$query = $fpdo->from(self::TABLE_NAME);
		return $query;
	}
}
