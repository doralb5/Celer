<?php

require_once DOCROOT . ENTITIES_PATH . 'User/UserDetail.php';
require_once DOCROOT . ENTITIES_PATH . 'User/UserPermission.php';
require_once DOCROOT . ENTITIES_PATH . 'User/UserCategory.php';

class User_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'User';
	const INDEX = 'id';

	public $id;
	public $username;
	public $email;
	public $password;
	public $plain_password;
	public $creation_date;
	public $firstname;
	public $lastname;
	public $last_access;
	public $super = 0;
	public $admin = 0;
	public $enabled = 0;
	public $image;
	public $facebook_id;
	public $id_category = 0;
	public $verified = '0';
	public $hash = '0';
	public $lang;
	//Additional
	public $phone;
	public $fullname;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'username' => 'username',
			'email' => 'email',
			'password' => 'password',
			'plain_password' => 'plain_password',
			'creation_date' => 'creation_date',
			'firstname' => 'firstname',
			'lastname' => 'lastname',
			'last_access' => 'last_access',
			'super' => 'super',
			'admin' => 'admin',
			'enabled' => 'enabled',
			'image' => 'image',
			'id_category' => 'id_category',
			'hash' => 'hash',
			'verified' => 'verified',
			'facebook_id' => 'facebook_id',
			'lang' => 'lang'
		);

		$this->creation_date = date('Y-m-d H:i:s');
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$User_Table = $TABLE_PREFIX . self::TABLE_NAME;
		$UserDetail_Table = $TABLE_PREFIX . UserDetail_Entity::TABLE_NAME;
		$fpdo = new FluentPDO($pdo);

		$join_detail = $UserDetail_Table . ' ON ' . $UserDetail_Table . '. ' . UserDetail_Entity::INDEX . ' = ' . $User_Table . '.' . self::INDEX;

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_detail)
			->select("CONCAT(firstname, ' ', lastname) AS fullname")
			->disableSmartJoin()
			->select("{$TABLE_PREFIX}UserDetail.phone");
		return $query;
	}
}
