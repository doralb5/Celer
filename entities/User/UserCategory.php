<?php

require_once DOCROOT . ENTITIES_PATH . 'User/UserPermModel.php';
require_once DOCROOT . ENTITIES_PATH . 'User/PermissionModel.php';

class UserCategory_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'UserCategory';
	const INDEX = 'id';
	const FK_PermissionModel = 'UserCategory.id_permission_model';

	public $id;
	public $category;
	public $id_permission_model;
	public $default;
	public $enabled_on_register;
	public $required_verify;
	public $model;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::TABLE_NAME . '.id',
			'category' => 'category',
			'id_permission_model' => 'id_permission_model',
			'default' => 'default',
			'required_verify' => 'required_verify',
			'enabled_on_register' => 'enabled_on_register'
		);
	}

	public static function getSelectQueryObj($pdo)
	{
		$TABLE_PREFIX = self::$table_prefix;
		$UserPermModel_Table = $TABLE_PREFIX . UserPermModel_Entity::TABLE_NAME;
		$FK_PermissionModel = $TABLE_PREFIX . self::FK_PermissionModel;

		$fpdo = new FluentPDO($pdo);
		$join_model = "$UserPermModel_Table ON $UserPermModel_Table." . UserPermModel_Entity::INDEX . " = $FK_PermissionModel";

		$query = $fpdo->from($TABLE_PREFIX . self::TABLE_NAME)
			->leftJoin($join_model)
			->select("$UserPermModel_Table.name AS model");
		return $query;
	}
}
