<?php

class UserDetail_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'UserDetail';
	const INDEX = 'id_user';

	public $id_user;
	public $phone;
}
