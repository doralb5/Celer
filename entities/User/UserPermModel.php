<?php

class UserPermModel_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'UserPermModel';
	const INDEX = 'id';

	public $id;
	public $name;
	public $description;
}
