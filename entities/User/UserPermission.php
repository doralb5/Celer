<?php

class UserPermission_Entity extends Entity
{
	const DATASOURCE = 'db';
	const TABLE_NAME = 'UserPermission';
	const INDEX = 'id';
	const FK_User = 'UserPermission.id_user';
	const FK_Component = 'UserPermission.id_component';

	public $id;
	public $id_user;
	public $id_component;
	public $disabled_functions;
	public $comp_name;
	public $name;
	public $email;

	public function __construct($index = '')
	{
		parent::__construct($index);
		$this->entity_fields = array(
			'id' => self::$table_prefix . self::TABLE_NAME . '.id',
			'id_user' => 'id_user',
			'id_component' => 'id_component',
			'disabled_functions' => 'disabled_functions',
		);
	}
}
