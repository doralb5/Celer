<?php

if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
	ob_start("ob_gzhandler");
else
	ob_start();

/*
 * Front Controller
 */
define('WEBROOT_ADMIN', 'admin/');
define('WEBROOT_FRONT', '/');

define('WEBROOT', WEBROOT_FRONT);
define('ADMIN', false);

include "functions.php";
include "./config/definitions.php";
include "./config/config.php";

if (SHOW_ERRORS)
	ini_set("display_errors", "1");

ini_set('post_max_size', POST_MAX_SIZE);
ini_set('memory_limit', MEMORY_LIMIT);

function __autoload($class) {
	if (is_file(CLASSES_PATH . $class . '_class.php'))
		require_once CLASSES_PATH . $class . '_class.php';
}

spl_autoload_register("__autoload");

include DOCROOT . LIBS_PATH . "Loader.php";


include DOCROOT . LIBS_PATH . "FrontController.php";

$fc = new FrontController();

$fc->bootstrap();
