<?php

class BaseComponent extends BaseController
{
	protected $ComponentID;
	protected $component_name;
	protected $ComponentSettings;

	/** @var ComponentView */
	public $view;

	/** @var BaseModel */
	protected $model;

	public function __construct($name = '', $package = '')
	{
		if ($name == '') {
			$name = get_class($this);
		}
		$cmod = Loader::getModel('Components');

		$cp_nam = str_replace('_Component', '', $name);
		$this->component_name = ($package == '' || $package == $cp_nam) ? $cp_nam : $package.'/'.$cp_nam;
		$comp = $cmod->getComponentByName($this->component_name);

		if (!is_null($comp)) {
			$this->ComponentID = $comp->id;
			$this->ComponentSettings = json_decode($comp->settings, true);
		}

		$this->model = Loader::getModel($name, $package);
		$this->view = new ComponentView($name, $package);

		$this->LogsManager = new LogsManager();
		$this->LogsManagerMain = new LogsManager();

		if (ADMIN && !UserAuth::checkComponentPerms($this->component_name)) {
			echo 'PERMISSION DENIED!';
			Utils::RedirectTo(Utils::getComponentUrl('Dashboard'));
		}
	}

	public static function getComponentSettings($name, $package = '')
	{
		$cmod = Loader::getModel('Components');
		$compname = str_replace('_Component', '', $name);
		$comp = $cmod->getComponentByName(($package != '' && $package !== $compname) ? $package.'/'.$compname : $compname);
		return json_decode($comp->settings, true);
	}

	protected function AddNotice($msg)
	{
		$this->view->AddNotice($msg);
	}

	protected function AddError($msg)
	{
		$this->view->AddError($msg);
	}

	protected function getErrors()
	{
		return $this->view->getErrors();
	}

	protected function getNotices()
	{
		return $this->view->getNotices();
	}

	protected function cleanErrors()
	{
		$this->view->cleanErrors();
	}

	protected function getActionUrl($action)
	{
		return Utils::getComponentUrl($this->component_name.'/'.$action);
	}

	public function checkActionPerms($action)
	{
		if (!UserAuth::checkActionPerms($this->component_name, $action)) {
			Utils::RedirectTo(Utils::getComponentUrl('Errors/permission_denied'));
		}
	}

	public function checkRemotePermission($action)
	{
		if (UserAuth::checkRemoteSession()) {
			if (UserAuth::checkLoginSession()) {
				$user = UserAuth::getLoginSession();
				$remoteu_md = Loader::getModel('RemoteUsers');
				if ($remoteu_md->existPermission($user['id'], $this->ComponentID, $action)) {
					return true;
				}
			}
			return false;
		} else {
			return true;
		}
	}
}

class ComponentMessages
{
	private $id;
	private $messages = array();

	public function __construct($CompID)
	{
		$this->id = $CompID;
		if (isset($_SESSION['ComponentMessages'][$this->id])) {
			$this->messages = $_SESSION['ComponentMessages'][$this->id];
		}
	}

	public function createNew($text)
	{
		array_push($this->messages, $text);
		$this->saveSession();
	}

	public function getAll()
	{
		return $this->messages;
	}

	public function renderHTML($return = false, $clean = true)
	{
		$output = '';
		foreach ($this->messages as $m) {
			$output .= "<div class=\"alert alert-info alert-costumized \" role=\"alert\">\n";
			$output .= $m."\n";
			$output .= "</div>\n";
		}
		if ($return) {
			return $output;
		} else {
			echo $output;
		}

		if ($clean) {
			$this->clean();
		}
	}

	private function saveSession()
	{
		$_SESSION['ComponentMessages'][$this->id] = $this->messages;
	}

	public function clean()
	{
		$this->messages = array();
		$this->saveSession();
	}
}

class ComponentErrors
{
	private $id;
	private $errors = array();

	public function __construct($CompID)
	{
		$this->id = $CompID;
		if (isset($_SESSION['ComponentErrors'][$this->id])) {
			$this->errors = $_SESSION['ComponentErrors'][$this->id];
		}
	}

	public function createNew($text)
	{
		array_push($this->errors, $text);
		$this->saveSession();
	}

	public function getAll()
	{
		return $this->errors;
	}

	public function renderHTML($return = false, $clean = true)
	{
		$output = '';
		foreach ($this->errors as $m) {
			$output .= "<div class=\"alert alert-danger\" role=\"alert\">\n";
			$output .= $m."\n";
			$output .= "</div>\n";
		}
		if ($return) {
			return $output;
		} else {
			echo $output;
		}

		if ($clean) {
			$this->clean();
		}
	}

	private function saveSession()
	{
		$_SESSION['ComponentErrors'][$this->id] = $this->errors;
	}

	public function clean()
	{
		$this->errors = array();
		$this->saveSession();
	}
}
