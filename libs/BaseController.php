<?php

require_once DOCROOT.CLASSES_PATH.'EntityManager.php';
require_once DOCROOT.CLASSES_PATH.'Entity.php';
require_once DOCROOT.ENTITIES_PATH.'Generic/Log.php';

class BaseController
{
	protected $model;
	protected $view;
	private $lang;
	public $LogsManager;
	public $LogsManagerMain;
	protected $ControllerID;
	protected $controller_name;
	protected $ControllerSettings;
	protected $preload_result;

	public function __construct()
	{
		$this->model = Loader::getModel(get_class($this));
		if (!isset($this->view)) {
			if (defined('ADMIN') && ADMIN) {
				$this->view = new ComponentView(get_class($this));
			} else {
				$this->view = new BaseView();
			}
		}
		$this->view->setViewPath(VIEWS_PATH.get_class($this).DS);
		$this->view->setLangPath(LANGS_PATH.get_class($this).DS);

		if (ADMIN) {
			$cmod = Loader::getModel('Components');
			$this->component_name = get_class($this);
			$comp = $cmod->getComponentByName($this->component_name);
			if (!is_null($comp)) {
				$this->ControllerID = $comp->id;
				$this->ControllerSettings = json_decode($comp->settings, true);
			}
		}

		$this->LogsManager = new LogsManager();
		$this->LogsManagerMain = new LogsManager();
		$this->LogsManagerMain->setDatabase(new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS));
	}

	public function setTemplate($templ, $params = array())
	{
		if (is_string($templ)) {
			$template_model = Loader::getModel('WebPage');
			$template_object = $template_model->getTemplateByName($templ);
			if ($template_object !== false) {
				return $this->view->setTemplate($template_object, $params);
			}
		} elseif (is_object($templ)) {
			return $this->view->setTemplate($templ, $params);
		}

		return false;
	}

	public function setAdminTemplate($templ_name)
	{
		$this->view->setAdminTemplate($templ_name);
	}

	public function setLang($lang)
	{
		$this->lang = $lang;
		$this->view->setLang($lang);
	}

	public function getLang()
	{
		return $this->lang;
	}

	public function checkRemotePermission($action)
	{
		if (UserAuth::checkRemoteSession()) {
			if (UserAuth::checkLoginSession()) {
				$user = UserAuth::getLoginSession();
				$remoteu_md = Loader::getModel('RemoteUsers');
				if ($remoteu_md->existPermission($user['id'], $this->ControllerID, $action)) {
					return true;
				}
			}
			return false;
		} else {
			return true;
		}
	}

	public function before_action($action, $action_params)
	{
		if (method_exists($this, 'preload_'.$action)) {
			$this->preload_result = Loader::runController($this, 'preload_'.$action, $action_params);
			return;
		}
	}
}

class LogsManager
{
	private $db;
	private $em;

	public function __construct()
	{
		$this->em = new EntityManager();
		$this->setDatabase();
	}

	public function setDatabase($db = null)
	{
		if (is_null($db)) {
			$this->db = Database::getInstance();
		} else {
			$this->db = $db;
		}

		$res = $this->em->setDatabase($this->db);
	}

	public function registerLog($object, $action, $description, $id_object = 0)
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Log');

		$log = new Log_Entity();

		$log->object = $object;
		$log->id_object = $id_object;
		$log->action = $action;
		$log->description = $description;
		$res = $this->em->persist($log);
		return $res;
	}

	public function getLogs($limit, $offset, &$counter, $object = '', $action = '', $order = '', $id_object = '')
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Log');

		if ($order == '') {
			$order = 'Log.date DESC, Log.id DESC';
		}

		$filter = '';
		$bindArray = array();

		if ($object != '') {
			$filter .= ' object = :object ';
			$bindArray[':object'] = $object;
		}

		if ($action != '') {
			if ($filter != '') {
				$filter .= ' AND ';
			}
			$filter .= ' action = :action ';
			$bindArray[':action'] = $action;
		}

		if ($id_object != '') {
			if ($filter != '') {
				$filter .= ' AND ';
			}
			$filter .= ' id_object = :id_obj ';
			$bindArray[':id_obj'] = $id_object;
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order, $bindArray);
		$res = $this->em->getEntities();

		$tot_count = $this->db->select('SELECT COUNT(*) AS tot FROM '.Log_Entity::TABLE_NAME.' WHERE 1 '.(($filter != '') ? "AND $filter" : ''), $bindArray);
		$counter = $tot_count[0]['tot'];
		return $res;
	}
}
