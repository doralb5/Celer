<?php

require_once DOCROOT.CLASSES_PATH.'EntityManager.php';
require_once DOCROOT.CLASSES_PATH.'Entity.php';
require_once DOCROOT.LIBS_PATH.'FluentPDO'.DS.'FluentPDO.php';
require_once DOCROOT.LIBS_PATH.'Database.php';

class BaseModel
{

	/** @var Database $db */
	protected $db;

	/** @var EntityManager $em */
	protected $em;

	public function __construct()
	{
		$this->db = Database::getInstance();
		$this->em = new EntityManager();
		$this->em->setDatabase($this->db);
	}

	public function setDatabase($db)
	{
		$this->db = $db;
		$this->em->setDatabase($this->db);
	}

	public function startTransaction()
	{
		$this->db->beginTransaction();
		$this->db->execute('SET FOREIGN_KEY_CHECKS = 0;');
	}

	public function commit()
	{
		$this->db->execute('SET FOREIGN_KEY_CHECKS = 1;');
		$this->db->commit();
	}

	public function rollback()
	{
		$this->db->execute('SET FOREIGN_KEY_CHECKS = 1;');
		$this->db->rollBack();
	}
}
