<?php

class BaseModule extends BaseController {

	protected $model;
	protected $view;
	protected $position;
	protected $block_id;

	function __construct($block_id = '', $position = '', $package = '') {

		if ($package == '') {
			$package = get_class($this);
		}

		$this->block_id = $block_id;
		$this->position = $position;
		$this->model = Loader::getModel(get_class($this), $package);
		$this->view = new ModuleView($package);

		$this->view->renderTemplate(false);
		$this->view->set('block_id', $block_id);

		$this->LogsManager = new LogsManager();
		$this->LogsManagerMain = new LogsManager();
	}

}
