<?php

class BasicSearchEngine
{
	private $section = 'Generic';
	private $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function excludeSections($section)
	{
		$this->section = $section;
	}

	public function search($keywords, $limit = 10, $offset = 0, $onlyEnabled = true, &$num_rows = 0)
	{
		$pertinenceField = $this->getPertinenceSQL($keywords, 10, 8, 4, 0);

		$query = "SELECT news.*,{$pertinenceField} AS pertinence,CONCAT(users.firstname, ' ', users.lastname) AS author "
				.'FROM news INNER JOIN categories ON(news.category_id=categories.id) '
				.'INNER JOIN section ON(categories.section_id=section.id) '
				.'LEFT JOIN users ON(users.id = news.author_id) ';

		$where = " WHERE section.name NOT IN(:section) AND news.visible = '1' AND {$pertinenceField} > 0";

		$groupby = 'GROUP BY news.id';

		$orderby = 'ORDER BY pertinence DESC, news.publish_date DESC';

		$qcount = "SELECT COUNT(*) AS numrows FROM ($query $where $groupby) AS tab";
		$resqcount = $this->db->select($qcount, array(':section' => $this->section));
		$num_rows = (isset($resqcount[0]['numrows']) ? $resqcount[0]['numrows'] : 0);

		$sqllimit = '';
		if ($limit != 0) {
			$sqllimit = ' LIMIT '.$limit;
		}
		if ($offset != '') {
			$sqllimit .= ' OFFSET '.$offset;
		}

		$query = $query.' '.$where.' '.$groupby.' '.$orderby.' '.$sqllimit; //echo $query;

		$articles = $this->db->select($query, array(':section' => $this->section));

		return $articles;
	}

	// CreaQueryRicerca: Creo la query di ricerca.
	// peso title se non specificato=5, peso testo se non specificato=3
	// searchlevel -> 1 o 0. default 1. Se 0 trova parole non complete. Es. cerchi osso?ok anche ossobuco. Se 1 non succede.
	public function getPertinenceSQL($queryvar, $pesotitle = 5, $pesoSubtitle = 4, $pesotesto = 3, $searchlevel = 1)
	{
		$titleField = 'news.title';
		$subtitleField = 'news.subtitle';
		$descrizioneField = 'news.content';

		//$pesokeywords = $pesotesto;
		// trasformo la stringa in un array di parole da cercare
		$arrayToFind = $this->QueryToArray($queryvar);
		// numero elementi da cercare
		$words = count($arrayToFind);
		// punteggio massimo raggiungibile
		$maxPoint = $words * $pesotitle +
				$words * $pesoSubtitle +
				$words * $pesotesto;

		if ($words == 0) {
			return '';
		} else {
			$query = 'ROUND((';
			$sqlwhere = '';
			// ciclo per ogni parola trovata ($Valore)
			foreach ($arrayToFind as $Indice => $Valore) {
				// se $Valore è presente in title instr(title, '$Valore') restituirà 1 altrimenti 0
				// moltiplico il valore restituito (1 o 0) per il peso della parola (5 per il title, 3 per testo)
				if ($searchlevel == 1) {
					// regexp: uso espressioni regolari. [[:<:]] equivale a \b per separare parole
					$query .= "(($titleField REGEXP '[[:<:]]".$Valore."[[:>:]]')>0)*$pesotitle+";
					$query .= "(($subtitleField REGEXP '[[:<:]]".$Valore."[[:>:]]')>0)*$pesoSubtitle+";
					$query .= "(($descrizioneField REGEXP '[[:<:]]".$Valore."[[:>:]]')>0)*$pesotesto+";

				//                                $sqlwhere.="$titleField REGEXP '[[:<:]]".$Valore."[[:>:]]' OR ";
//                                $sqlwhere.="$descrizioneField REGEXP '[[:<:]]".$Valore."[[:>:]]' OR ";
					//$sqlwhere.="p.title_it REGEXP '[[:<:]]".$Valore."[[:>:]]' OR p.content_it REGEXP '[[:<:]]".$Valore."[[:>:]]' OR ";
				} else {
					$query .= "(instr($titleField, '$Valore')>0)*$pesotitle+";
					$query .= "(instr($subtitleField, '$Valore')>0)*$pesoSubtitle+";
					$query .= "(instr($descrizioneField, '$Valore')>0)*$pesotesto+";

					//                $sqlwhere.="$titleField like '%$Valore%' OR $descrizioneField like '%$Valore%' OR ";
				}
			}
			//        $sqlwhere=substr($sqlwhere, 0, strlen($sqlwhere)-4);
			// calcolo la percentuale di rilevanza  --> rilevanza*100/$maxPoint
			$query .= "0)*100/$maxPoint,2)";
			return $query;
		}
	}

	// QueryToArray: Restituisce array delle parole chiave da cercare
	public function QueryToArray($queryvar)
	{
		// pulisco query da parole accessorie e caratteri non alfanumerici
		$querypulita = $this->cleanQuery($queryvar);
		// costruisco l'array contenente tutte le parole da cercare
		$arraySearch = explode(' ', $querypulita);
		// elimino doppioni dall'array
		$arraySearchUnique = array_unique($arraySearch);
		// elimino valori array vuoti o con solo spazi
		$arrayVuoto = array('', ' ');
		$arrayToReturn = array_diff($arraySearchUnique, $arrayVuoto);
		return $arrayToReturn;
	}

	// PulisciQuery: Restituisce la query pulita da porole inutili(congiunzioni etc.)
	public function cleanQuery($queryvar)
	{
		// array parole di cui non tener conto nelle ricerche
		$arrayBadWord = array('lo', 'l', 'il', 'la', 'i', 'gli', 'le', 'uno', 'un', 'una', 'un', 'su', 'sul', 'sulla', 'sullo', 'sull', 'in', 'nel', 'nello', 'nella', 'nell', 'con', 'di', 'da', 'dei', 'd', 'della', 'dello', 'del', 'dell', 'che', 'a', 'dal', 'è', 'e', 'per', 'non', 'si', 'al', 'ai', 'allo', 'all', 'al', 'o');
		$queryclean = strtolower($queryvar);
		for ($a = 0; $a < count($arrayBadWord); $a++) {
			// sostituisco bad words con espressioni regolari \b ->solo se parole singole, non facenti parti di altre
			$queryclean = preg_replace("/\b".$arrayBadWord[$a]."\b/", '', $queryclean);
		}
		// elimino tutti caratteri non alfanumerici sostituendeli con uno spazio
		$queryclean = preg_replace("/\W/", ' ', $queryclean);
		return $queryclean;
	}
}
