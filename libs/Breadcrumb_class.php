<?php

class Breadcrumb_class
{
	private $breadcrumb;
	private $breadcrumbString;
	private $symbol = ' > ';
	private $cssClass = 'breadcrumb';
	private $target = '_self';
	private $homepage = 'Home';
	private $rootIndexPage = '/';
	private $showCurrentLink = true;
	private $format = 'ucfirst';
	private $imagedir;
	private $useImages = false;
	private $withSymbol = false;

	public function __construct()
	{
		//$this->create();
	}

	public function getArray()
	{
		return $this->breadcrumb;
	}

	public function setSymbol($symbol)
	{
		$this->symbol = $symbol;
	}

	public function setWithSymbol($bool)
	{
		$this->withSymbol = $bool;
	}

	public function setCssClass($cssClass)
	{
		$this->cssClass = $cssClass;
	}

	public function setTarget($target)
	{
		$this->target = $target;
	}

	/**
	 * Setta il nome da visualizzare per il link HomePage
	 * Se viene passata una stringa vuota il link HomePage non viene inserito nel Breadcrumb
	 * @param string $homepage
	 */
	public function setHomepage($homepage)
	{
		$this->homepage = $homepage;
	}

	public function setRootIndexPage($rootIndexPage)
	{
		$this->rootIndexPage = $rootIndexPage;
	}

	public function unlinkCurrent($unlink)
	{
		if (is_bool($unlink)) {
			$this->showCurrentLink = !$unlink;
		}
	}

	public function setFormat($format)
	{
		$formatAccepted = array('titlecase', 'ucfirst', 'ucwords', 'uppercase', 'lowercase');
		if (in_array($format, $formatAccepted)) {
			$this->format = $format;
		}
	}

	public function setImagedir($imagedir)
	{
		$this->imagedir = $imagedir;
	}

	public function useImages($useImages)
	{
		if (is_bool($useImages)) {
			$this->useImages = $useImages;
		} else {
			throw new Exception('useImages accept only boolean value!');
		}
	}

	public function removeDirs($names)
	{
		for ($i = 0; $i < count($this->breadcrumb); $i++) {
			if (is_string($names)) {
				if ($this->breadcrumb[$i] == $names) {
					unset($this->breadcrumb[$i]);
				}
			} elseif (is_array($names)) {
				foreach ($names as $name) {
					if ($this->breadcrumb[$i] == $name) {
						unset($this->breadcrumb[$i]);
					}
				}
			}
		}
	}

	public function addDir($voce, $link)
	{
		//array_push($this->breadcrumb,array($voce => $link));
		$this->breadcrumb[$voce] = $link;
	}

	public function clean()
	{
		$this->breadcrumb = null;
	}

	public function removeLast()
	{
		if (is_array($this->breadcrumb)) {
			array_pop($this->breadcrumb);
		}
	}

	public function getLast()
	{
		if (is_array($this->breadcrumb) && count($this->breadcrumb)) {
			end($this->breadcrumb);
			return key($this->breadcrumb);
		}
	}

	public function create($arr = null)
	{	 //INCOMPLETA..... e NON TESTATA
		if (!is_null($arr)) {
			$this->breadcrumb = $arr;
		}

		if (!is_null($this->breadcrumb)) {
			if (!$this->showCurrentLink) {
				array_splice($this->breadcrumb, -1);
			}

			$this->breadcrumbString = "<ol itemscope itemtype='http://schema.org/BreadcrumbList' class=\"".$this->cssClass."\">\n";
			$first = true;
			$total_elements = count($this->breadcrumb);
			$i = 0;
			if ($this->homepage != '') {
				$i++;
				$total_elements = count($this->breadcrumb) + 1;
				$this->breadcrumbString .= "<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem' class=\"crumb\">"."<a  itemprop='item' href=\"$this->rootIndexPage\"><span itemprop='name' >$this->homepage</span></a>"."<meta itemprop='position' content='$i' /> </li>";
				$first = false;
			}
			$i++;
			foreach ($this->breadcrumb as $voce => $link) {
				$active_class = ($i == $total_elements) ? 'active' : '';

				if (!$first && $this->withSymbol) {
					$this->breadcrumbString .= "<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem' class=\"crumb_next\">".$this->symbol.'</li>';
				}
				$this->breadcrumbString .= "<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem' class=\"crumb $active_class\">"."<a itemprop='item' href=\"$link\"><span itemprop='name' >$voce</span></a>"." <meta itemprop='position' content='$i' /> </li>";
				$first = false;
				$i++;
			}
			$this->breadcrumbString .= "</ol>\n";
			return $this->breadcrumbString;
		} else {
			return '';
		}
	}

	public function render()
	{
		$this->create();
		echo $this->breadcrumbString;
	}

	public function get()
	{
		$this->create();
		return $this->breadcrumbString;
	}
}
