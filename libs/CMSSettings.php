<?php

require_once DOCROOT.LIBS_PATH.'Utils.php';

class CMSSettings
{
	const CLASS_NAME = 'CMSSettings';
	const TABLE_NAME = 'Setting';

	private static $db;
	public static $theme_id = '';
	public static $available_langs = array('en');
	public static $default_lang = 'en';
	public static $website_title = '';
	public static $website_template = '';
	public static $webdomain = '';
	public static $sender_mail = '';
	public static $offline = '';
	public static $SMTP = 1;
	public static $SMTP_HOST = 'smtp.weweb.al';
	public static $SMTP_USERNAME = 'sender@weweb.al';
	public static $SMTP_PASSWORD = 'pwrMqgqKL1YE@';
	public static $EMAIL_ADMIN = '';
	public static $logo = '';
	public static $logo_inverse = '';
	public static $template = '';
	public static $template_options = array();
	public static $layout = '';
	public static $admin_template = '';
	public static $admin_layout = '';
	public static $maintenance = '';
	public static $company_name = '';
	public static $company_address = '';
	public static $company_mail = '';
	public static $company_tel = '';
	public static $company_cel = '';
	public static $facebook_api = '';
	public static $facebook_secret = '';
	public static $google_key = '';
	public static $google_recaptcha_key = '';
	public static $google_maps_api = 'AIzaSyBEiE-_79eSxQl4b1tdrEichm8vTLw4VC0';
	public static $marker_icon = '';
	public static $facebook = '';
	public static $alternativ_email = '';
	public static $CURRENCY = 'EUR';
	public static $country = '';
	public static $favicon = 'favicon.ico';
	public static $force_https = '0';

	public static function loadSettings()
	{
		self::$db = Database::getInstance();

		$Setting_Table = TABLE_PREFIX.'Setting';
		$result = self::$db->select("SELECT * FROM $Setting_Table");

		$vars = get_class_vars(self::CLASS_NAME);

		foreach ($result as $record) {
			$key = $record['field'];
			if (key_exists($key, $vars)) {
				if ($key == 'available_langs') {
					self::$$key = explode(';', $record['value']);
					self::$$key = array_filter(self::$$key);
				} else {
					self::$$key = $record['value'];
				}
			}
		}

		$template = self::getCurrentTemplate();
		self::$template = $template['name'];
		self::$template_options = json_decode($template['options'], true);
	}

	private static function getCurrentTemplate()
	{
		self::$db = Database::getInstance();

		$Template_Table = TABLE_PREFIX.'Template';
		$sql = "SELECT name, options FROM $Template_Table WHERE enabled = '1'";
		$result = self::$db->select($sql);
		if (count($result)) {
			return $result[0];
		} else {
			return false;
		}
	}

	public static function getEnabledComponents()
	{
		self::$db = Database::getInstance();
		$sql = "SELECT * FROM Component WHERE enabled = '1'";
		$result = self::$db->select($sql);
		return $result;
	}

	public static function loadUserPermissions($component_name)
	{
		self::$db = Database::getInstance();

		$UserPermision_Table = TABLE_PREFIX.'UserPermission';
		$Component_Table = TABLE_PREFIX.'Component';

		$sql = "SELECT * FROM $UserPermision_Table AS up "
				."LEFT JOIN $Component_Table AS cmp ON (cmp.id = up.id_component) "
				.'WHERE cmp.name = :comp_name';
		$result = self::$db->select($sql, array(':comp_name' => $component_name));
		if (count($result)) {
			return $result[0];
		} else {
			return null;
		}
	}
}
