<?php

class ComponentView extends BaseView
{
	private $title;
	private $subtitle;
	private $layout = 'Component_view';
	protected $ComponentMessages;
	protected $ComponentErrors;
	protected $action_buttons = array();
	protected $action_filters = array();
	public $BreadCrumb;

	public function __construct($name, $package = '')
	{
		parent::__construct();
		$component_name = substr($name, 0, strpos($name, '_Component'));
		if ($package == '') {
			$this->setLangPath(COMPONENTS_PATH.$component_name.DS.'lang'.DS);
			$this->setViewPath(COMPONENTS_PATH.$component_name.DS.'views'.DS);
		} else {
			$this->setLangPath(COMPONENTS_PATH.$package.DS.'lang'.DS);
			$this->setViewPath(COMPONENTS_PATH.$package.DS.'views'.DS);
		}

		if (ADMIN) {
			$this->setLayout('default');
		}

		$this->setTitle($name);
		$this->setSubTitle('');

		$this->ComponentMessages = new ComponentMessages($name);
		$this->ComponentErrors = new ComponentErrors($name);
		$this->BreadCrumb = new Breadcrumb_class();
	}

	public function AddJS($name)
	{
		HeadHTML::AddJS($this->view_path.'js/'.$name);
	}

	public function AddStylesheet($name)
	{
		HeadHTML::AddStylesheet($this->view_path.'css/'.$name);
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function getSubTitle()
	{
		return $this->subtitle;
	}

	public function setTitle($title)
	{
		$this->title = $this->replace_vars($title);
	}

	public function setSubTitle($subtitle)
	{
		$this->subtitle = $this->replace_vars($subtitle);
	}

	public function setViewFile($view)
	{
		return $this->setComponentLayout($view);
	}

	public function setComponentLayout($layout)
	{
		$this->layout = $layout;
	}

	public function addButton($btn)
	{
		array_push($this->action_buttons, $btn);
	}

	public function addSelectFilter($select)
	{
		array_push($this->action_filters, $select);
	}

	//    public function render($view, $return_output = false) {
	//        if(ADMIN)
	//            return $this->backend_render ($view, $return_output);
	//        else
	//            parent::render($view, $return_output);
	//    }
//
	//    public function backend_render($view, $return_output = false)
	//    {
	//        $content = parent::render($view, true);
	//        parent::setContent($content);
	//        parent::setLayout('default');
	//        return parent::render($this->layout);
	//    }

	public function AddNotice($msg)
	{
		$this->ComponentMessages->createNew($msg);
	}

	public function AddError($msg)
	{
		$this->ComponentErrors->createNew($msg);
	}

	public function getErrors()
	{
		return $this->ComponentErrors->getAll();
	}

	public function getNotices()
	{
		return $this->ComponentMessages->getAll();
	}

	public function cleanMessages()
	{
		return $this->ComponentMessages->clean();
	}

	public function cleanErrors()
	{
		return $this->ComponentErrors->clean();
	}

	public function showComponentErrors()
	{
		if ($errors = $this->getErrors()) {
			$this->cleanErrors();
			print '<div class="alert alert-danger fade in alert-costumized">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="fa fa-exclamation-circle"></i>
            </button>';
			foreach ($errors as $error) {
				print '&bull; '.$error.'<br/>';
			}
			print '</div>';
		}
	}

	public function showComponentMessages()
	{
		if ($notices = $this->getNotices()) {
			$this->cleanMessages();
			print '<div class="alert alert-info fade in alert-costumized">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="fa fa-exclamation-circle"></i>
            </button>';
			foreach ($notices as $notice) {
				print '&bull; '.$notice.'<br/>';
			}
			print '</div>';
		}
	}
}
