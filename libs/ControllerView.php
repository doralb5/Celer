<?php

class ControllerView extends BaseView {

	function __construct($name, $package = '') {
		parent::__construct();
		if ($package == '') {
			$this->setLangPath(LANGS_PATH . $name . DS);
			$this->setViewPath(VIEWS_PATH . $name . DS);
		} else {
			$this->setLangPath(LANGS_PATH . $package . DS);
			$this->setViewPath(VIEWS_PATH . $package . DS);
		}
	}

	public function AddJS($name) {
		HeadHTML::AddJS($this->view_path . 'js/' . $name);
	}

	public function AddStylesheet($name) {
		HeadHTML::AddStylesheet($this->view_path . 'css/' . $name);
	}

}
