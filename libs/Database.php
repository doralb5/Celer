<?php

class Database extends PDO
{
	private static $instance = null;
	public $DB_NAME;

	public function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS)
	{
		$options = null;
		if ($DB_TYPE == 'mysql') {
			$options = array(
				parent::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
			);
		}
		parent::__construct($DB_TYPE.':host='.$DB_HOST.';dbname='.$DB_NAME, $DB_USER, $DB_PASS, $options);
		$this->DB_NAME = $DB_NAME;
	}

	/**
	 *
	 * @return Database
	 */
	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
		}
		return self::$instance;
	}

	public static function setInstance($db)
	{
		self::$instance = $db;
	}

	public function execute($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC)
	{
		if (!is_array($array)) {
			throw new Exception('The parameter passed not is an Array!');
		}
		$sth = $this->prepare($sql);
		foreach ($array as $key => $value) {
			$sth->bindValue("$key", $value);
		}

		$res = $sth->execute();

		if ($res) {
			return $sth->rowCount();
		} else {
			return $sth->errorInfo();
		}
	}

	/**
	 * SELECT
	 * @param string $sql
	 * @param array $array
	 * @param constant $fetchMode
	 * @return mixed
	 */
	public function select($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC)
	{
		if (!is_array($array)) {
			throw new Exception('The parameter passed not is an Array!');
		}
		$sth = $this->prepare($sql);
		foreach ($array as $key => $value) {
			$sth->bindValue("$key", $value);
		}

		$sth->execute();

		return $sth->fetchAll($fetchMode);
	}

	/**
	 * INSERT
	 * @param string $table Nome della tabella
	 * @param string $data Array associativo
	 */
	public function insert($table, $data, $ignore_duplicate = false)
	{
		ksort($data);

		$fieldNames = implode('`,`', array_keys($data));
		$fieldValues = ':'.implode(', :', array_keys($data));

		$sth = $this->prepare('INSERT '.($ignore_duplicate ? 'IGNORE' : '')." INTO $table (`$fieldNames`) VALUES ($fieldValues)");

		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}
		//echo "<!-- $sth->queryString -->";
		$res = $sth->execute();

		if ($res) {
			$id = $this->lastInsertId();
			return $id;
		} else {
			$err = $sth->errorInfo();
			return $err;
		}
	}

	/**
	 * INSERT
	 * @param string $table Nome della tabella
	 * @param string $data Array associativo
	 */
	public function replace($table, $data)
	{
		ksort($data);

		$fieldNames = implode('`,`', array_keys($data));
		$fieldValues = ':'.implode(', :', array_keys($data));

		$sth = $this->prepare("REPLACE INTO $table (`$fieldNames`) VALUES ($fieldValues)");

		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}

		$res = $sth->execute();

		if ($res) {
			return $this->lastInsertId();
		} else {
			return $sth->errorInfo();
		}
	}

	/**
	 * UPDATE
	 * @param string $table Nome della tabella
	 * @param string $data Array associativo
	 * @param string $where Parte Where della query
	 */
	public function update($table, $data, $where)
	{
		ksort($data);

		$fieldDetails = null;
		foreach ($data as $key => $value) {
			$fieldDetails .= "`$key` = :$key,";
		}
		$fieldDetails = rtrim($fieldDetails, ',');

		$sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");

		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}
		return $sth->execute();
	}

	/**
	 * DELETE
	 * @param string $table
	 * @param string $where
	 * @param integer $limit
	 * @return integer Affected Rows
	 */
	public function delete($table, $where, $limit = 1)
	{
		return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
	}
}
