<?php

$use_smtp = (CMSSettings::$SMTP == '1') ? true : false;
if (!defined('USE_SMTP'))
	define('USE_SMTP', $use_smtp);
if (!defined('SMTP_HOST'))
	define('SMTP_HOST', CMSSettings::$SMTP_HOST);
if (!defined('SMTP_USERNAME'))
	define('SMTP_USERNAME', CMSSettings::$SMTP_USERNAME);
if (!defined('SMTP_PASSWORD'))
	define('SMTP_PASSWORD', CMSSettings::$SMTP_PASSWORD);
if (!defined('DEVELOPMENT_MAIL'))
	define('DEVELOPMENT_MAIL', 'a.afmeti@bluehat.al');

require_once DOCROOT . LIBS_PATH . 'PHPMailer/class.phpmailer.php';
require_once DOCROOT . LIBS_PATH . 'PHPMailer/class.smtp.php';

class Email {

	private static $use_smtp = USE_SMTP;
	private static $smtp_host = SMTP_HOST;
	private static $smtp_username = SMTP_USERNAME;
	private static $smtp_password = SMTP_PASSWORD;

	public static function sendMail($to, $from_name, $from, $subject, $testo, $arrAttach = array()) {
		$messaggio = new PHPmailer();

		if (self::$use_smtp) {
			$messaggio->IsSMTP();
			$messaggio->SMTPAuth = true;
			$messaggio->Host = self::$smtp_host;
			$messaggio->Username = self::$smtp_username;
			$messaggio->Password = self::$smtp_password;
			$messaggio->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
		} else {
			$messaggio->IsMail();
		}
		$messaggio->IsHTML(true);

		$messaggio->CharSet = "UTF-8";
		$messaggio->FromName = $from_name;
		$messaggio->From = $from;

		$arr_to = explode(';', $to);
		for ($i = 0; $i < count($arr_to); $i++) {
			if (defined('DEVELOPMENT_ENVIRONMENT') && DEVELOPMENT_ENVIRONMENT === true) {
				$messaggio->AddAddress(DEVELOPMENT_MAIL);
				break;
			}
			$messaggio->AddAddress($arr_to[$i]);
		}

		$messaggio->AddReplyTo($from);
		$messaggio->Subject = $subject;
		$messaggio->Body = $testo;

		if (count($arrAttach)) {
			foreach ($arrAttach as $key => $attach) {
				$messaggio->addAttachment($attach, $key);
			}
		}

		if (!$messaggio->Send()) {
			$messaggio->SmtpClose();
			unset($messaggio);
			return false;
		} else {
			$messaggio->SmtpClose();
			unset($messaggio);
			return true;
		}
	}

	public static function generateMailBody($text) {

		$msg = "<html><head></head><body>";
		$msg .= $text;
		$msg .= "</body></html>";
		return $msg;
	}

}

?>
