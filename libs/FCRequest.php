<?php

class FCRequest {

	private static $url;
	private static $isComponent;
	private static $controller;
	private static $package;
	private static $action;
	private static $parameters;
	private static $lang;

	public static function create() {
		
	}

	public static function getUrl() {
		return self::$url;
	}

	public static function getIsComponent() {
		return self::$isComponent;
	}

	public static function getController() {
		return self::$controller;
	}

	public static function getPackage() {
		return self::$package;
	}

	public static function getAction() {
		return self::$action;
	}

	public static function getParameters() {
		return self::$parameters;
	}

	static function getLang() {
		return self::$lang;
	}

	public static function setUrl($url) {
		self::$url = $url;
	}

	public static function setController($controller) {
		self::$controller = $controller;
	}

	public static function setPackage($package) {
		self::$package = $package;
	}

	public static function setAction($action) {
		self::$action = $action;
	}

	public static function setParameters($parameters) {
		self::$parameters = $parameters;
	}

	static function setLang($lang) {
		self::$lang = $lang;
	}

	public static function setIsComponent($isComponent) {
		self::$isComponent = $isComponent;
	}

}
