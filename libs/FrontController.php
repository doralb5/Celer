<?php

require_once DOCROOT.LIBS_PATH.'Database.php';
require_once DOCROOT.LIBS_PATH.'CMSSettings.php';
require_once DOCROOT.LIBS_PATH.'FCRequest.php';
require_once DOCROOT.LIBS_PATH.'Session.php';
require_once DOCROOT.LIBS_PATH.'UserAuth.php';
require_once DOCROOT.LIBS_PATH.'Messages.php';
require_once DOCROOT.CLASSES_PATH.'Entity.php';
require_once DOCROOT.LIBS_PATH.'BaseModel.php';

class FrontController
{
	private $package = '';
	private $controller = 'Index';
	private $action = 'index';
	private $parameters = array();
	private $isComponent = false;
	private $current_lang;

	public function __construct()
	{
		Session::start();
		Database::getInstance();
		CMSSettings::loadSettings();

		if (CMSSettings::$force_https == 1 && (!isset($_SERVER['HTTPS']) || empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off')) {
			$redirect = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: '.$redirect);
			exit();
		}

		$this->current_lang = CMSSettings::$default_lang;
		Utils::setLang(CMSSettings::$default_lang);
	}

	public function bootstrap()
	{
		$request = (isset($_GET['req'])) ? $_GET['req'] : '';
		FCRequest::create();
		FCRequest::setUrl($request);
		$this->ProcessRequest($request);
		$this->route($request);
		$this->dispatch();
	}

	public function route($request)
	{
		if ($this->checkThumbnail($request)) {
			return;
		}

		$request = self::URItoArray($request);

		if (is_numeric(array_search($request[0], CMSSettings::$available_langs))) {
			$this->current_lang = $request[0];
			array_shift($request);
		}

		$this->ProcessPageAlias($request);

		if (!isset($request[0]) || $request[0] == '') {
			if (!ADMIN) {
				$this->controller = 'WebPage';
				$this->action = 'render';
			}
		} else {
			if ($request[0] == 'com') {
				$this->isComponent = true;
				array_shift($request);
				if (is_dir(DATA_DIR.COMPONENTS_PATH.$request[0]) || is_dir(COMPONENTS_PATH.$request[0])) {
					if (is_file(DATA_DIR.COMPONENTS_PATH.DS.$request[0].DS.$request[1].'.php')) {
						$this->package = array_shift($request);
					} elseif (is_file(COMPONENTS_PATH.DS.$request[0].DS.$request[1].'.php')) {
						$this->package = array_shift($request);
					}
				}
			}

			if ($this->isController($request[0]) || $this->isComponent) {
				$this->controller = array_shift($request);
				if (isset($request[0]) && $request[0] != '') {
					$this->action = array_shift($request);

					if (isset($request[0]) && $request[0] != '') {
						$this->parameters = $request;
					}
				}
			} else {
				$this->controller = 'WebPage';
				$this->action = 'render';
				$this->parameters = array(implode('/', $request));
			}
		}

		if ($this->checkMaintenance() && $this->controller != 'Maintenance' && $this->controller != 'Install'
		) {
			Utils::RedirectTo(Utils::getControllerUrl('Maintenance'));
		}

		FCRequest::setIsComponent($this->isComponent);
		FCRequest::setController($this->controller);
		FCRequest::setPackage($this->package);
		FCRequest::setAction($this->action);
		FCRequest::setParameters($this->parameters);
		FCRequest::setLang($this->current_lang);

		switch ($this->current_lang) {
			case 'en': setlocale(LC_ALL, 'en_US');
				break;
			case 'al': setlocale(LC_ALL, 'Albanian');
				break;
			case 'it': setlocale(LC_ALL, 'it_IT');
				break;
			default: setlocale(LC_ALL, 'it_IT');
		}
	}

	public function dispatch()
	{
		require_once DOCROOT.LIBS_PATH.'BaseController.php';
		require_once DOCROOT.LIBS_PATH.'BaseView.php';
		require_once DOCROOT.LIBS_PATH.'BaseModule.php';
		require_once DOCROOT.LIBS_PATH.'BaseComponent.php';
		require_once DOCROOT.LIBS_PATH.'ComponentView.php';
		require_once DOCROOT.LIBS_PATH.'ControllerView.php';
		require_once DOCROOT.LIBS_PATH.'ModuleView.php';

		require_once DOCROOT.CONTROLLERS_PATH.'WebPage/WebPage.php';

		Utils::setLang($this->current_lang);

		//Load the Controller
		if ($this->isComponent || $this->isController($this->controller)) {
			if ($this->isComponent) {

				//Mbrojme faqet e administratorit
				if (ADMIN && !UserAuth::checkLoginSession()) {
					Utils::RedirectTo(Utils::getControllerUrl('Users/login'));
					return;
				}

				if ($this->package != '') {
					$filename = COMPONENTS_PATH.$this->package.DS.$this->controller.'.php';
					if (file_exists(DATA_DIR.$filename)) {
						require_once DATA_DIR.$filename;
					} else {
						require_once $filename;
					}
					$cName = $this->controller.'_Component';
					$controller = new $cName($this->controller, $this->package);
				} else {
					$filename = COMPONENTS_PATH.$this->controller.DS.$this->controller.'.php';
					if (file_exists(DATA_DIR.$filename)) {
						require_once DATA_DIR.$filename;
					} else {
						require_once $filename;
					}
					$cName = $this->controller.'_Component';
					$controller = new $cName();
				}
			} else {
				$filename = CONTROLLERS_PATH.$this->controller.DS.$this->controller.'.php';
				if (file_exists(DATA_DIR.$filename)) {
					require_once DATA_DIR.$filename;
				} else {
					require_once $filename;
				}
				$cName = $this->controller;
				$controller = new $cName();
			}

			if (ADMIN) {
				$controller->setAdminTemplate(CMSSettings::$admin_template);
			} else {
				$controller->setTemplate(CMSSettings::$template, CMSSettings::$template_options);
			}

			$controller->setLang($this->current_lang);
			Loader::runController($controller, $this->action, $this->parameters);
		} else {
			echo "Controller $this->controller not found!";
		}
	}

	private static function URItoArray($url)
	{
		$url = rtrim($url, '/');
		$url = urldecode($url);
		$url = explode('/', $url);
		return $url;
	}

	public function isController($name)
	{
		$controller_path = ($this->isComponent) ? COMPONENTS_PATH : CONTROLLERS_PATH;
		if (file_exists($controller_path.$name.DS.$name.'.php') || file_exists(DATA_DIR.$controller_path.$name.DS.$name.'.php')) {
			return true;
		} else {
			return false;
		}
	}

	private function checkMaintenance()
	{
		if (CMSSettings::$maintenance && !ADMIN && !UserAuth::checkAdminSession()) {
			return true;
		} else {
			return false;
		}
	}

	private function ProcessRequest(&$request)
	{
		if (!is_dir(DOCROOT.DATA_DIR)) {
			$request = 'Install/InstallDataDir';
		}
	}

	public function ProcessPageAlias(&$request)
	{
		$lang = Utils::getLang();
		$request = implode('/', $request);
		if (!ADMIN) {
			$pages_md = Loader::getModel('WebPage');
			$request_string = $request;
			if (isset($_GET) && count($_GET) > 0) {
				$request_string = $request.'?';
				foreach ($_GET as $k => $v) {
					if ($k != 'req') {
						$request_string .= $k.'='.$v.'&';
					}
				}
				$request_string = rtrim($request_string, '&');
			}
			$alias = $pages_md->getPageAliases(1, 0, "alias = '$request_string' AND (lang='$lang' OR lang IS NULL)");
			if (count($alias) == 1) {
				$alias = $alias[0];

				$request = $alias->destination;

				$request = explode('/', $request);
				return true;

			}
		}
		$request = explode('/', $request);
		return false;
	}

	private function checkThumbnail(&$request)
	{
		if (strstr($request, MEDIA_ROOT.'thumbs/') !== false) {
			$this->controller = 'ImageManager';
			$this->action = 'thumb';
			$request = str_replace(MEDIA_ROOT.'thumbs/', '', $request);
			$request = self::URItoArray($request);
			$size = $request[count($request) - 2];
			$filename = $request[count($request) - 1];
			array_pop($request);
			array_pop($request);
			$filename = (implode('/', $request).'/'.$filename);
			$this->parameters = array($filename, $size);
			return true;
		} else {
			return false;
		}
	}
}
