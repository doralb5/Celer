<?php

/**
 * @version 1.2 09/03/2013
 */
class HeadHTML
{
	public static $css = array();
	public static $js = array();
	public static $controllers_css = array();
	public static $controllers_js = array();
	public static $lang = 'en';
	public static $template = 'default';
	public static $TEMPLATE_PATH;
	public static $title;
	public static $arr_meta = array();
	public static $arr_meta_property = array();
	public static $meta_description;
	public static $meta_keywords;
	public static $meta_author;
	public static $meta_copyright;
	public static $meta_robots = 'INDEX, FOLLOW';
	public static $arr_scripts = array();

	public static function setLang($lang)
	{
		self::$lang = $lang;
	}

	public static function setTemplate($sTemplate)
	{
		self::$template = $sTemplate;
		self::$TEMPLATE_PATH = TEMPLATES_PATH.$sTemplate.'/';
	}

	public static function setTitleTag($title)
	{
		self::$title = $title;
	}

	public static function setMetaTag($name, $cont)
	{
		self::$arr_meta[$name] = $cont;
	}

	public static function addMetaProperty($name, $cont)
	{
		self::$arr_meta_property[][$name] = $cont;
	}

	public static function getHeadersTags()
	{
		$aTags['title'] = '<title>'.self::$title."</title>\n";
		$aTags['favicon'] = '<link rel="shortcut icon" href="'.WEBROOT.DATA_DIR."images/favicon.ico\" />\n";

		$aTags['meta'] = '';
		foreach (self::$arr_meta as $meta => $content) {
			$aTags['meta'] .= "<meta name=\"$meta\" content=\"$content\" />\n";
		}

		$aTags['meta'] .= '<meta http-equiv="content-language" content="'.self::$lang.'" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="ROBOTS" content="'.self::$meta_robots."\" />\n\n";

		return $aTags;
	}

	/**
	 * Scrive la parte HTML per includere un foglio di stile
	 * @param string $name
	 */
	public static function linkStylesheet($name)
	{
		if (file_exists(DOCROOT.$name)) {
			echo '<link href="'.$name."\" rel=\"stylesheet\" type=\"text/css\" />\n";
		} else {
			echo '<link href="'.self::getStyleSheetPath($name)."\" rel=\"stylesheet\" type=\"text/css\" />\n";
		}
	}

	public static function getStyleSheetPath($name, $admin = false)
	{
		$file = ltrim($name, '/');

		//        echo "<link href=\"" . WEBROOT . $file . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
		//        return;

		if (!$admin) {
			$webroot = (ADMIN) ? dirname(WEBROOT) : WEBROOT;
			self::setTemplate(CMSSettings::$template);
		} else {
			$webroot = WEBROOT;
			self::setTemplate(CMSSettings::$admin_template);
		}

		if (file_exists($file)) {
			return $webroot.$file;
		}

		$file = DATA_DIR.self::$TEMPLATE_PATH.'css/'.$name;
		if (file_exists(DOCROOT.$file)) {
			return $webroot.$file;
		} else {
			$file = self::$TEMPLATE_PATH."css/$name";
			if (file_exists($file)) {
				return $webroot.$file;
			} else {
				$file = "commons/css/$name";
				if (file_exists($file)) {
					return $webroot.$file;
				} else {
					if (Utils::remoteFileExists($name)) {
						return $name;
					}
				}
			}
		}
	}

	public static function linkJS($name, $async = false, $defer = false)
	{
		$additional = '';
		if ($async) {
			$additional .= ' async ';
		}
		if ($defer) {
			$additional .= ' defer ';
		}

		if (file_exists(DOCROOT.$name)) {
			echo '<script type="text/javascript" src="'.$name."\"></script>\n";
			return;
		}

		$file = ltrim($name, '/');

		if (file_exists($file)) {
			echo '<script type="text/javascript" src="'.WEBROOT.$file."\"></script>\n";
			return;
		}

		$file = DATA_DIR.self::$TEMPLATE_PATH."js/$name";
		if (file_exists($file)) {
			echo '<script type="text/javascript" src="'.WEBROOT.$file."\"></script>\n";
		} else {
			$file = self::$TEMPLATE_PATH."js/$name";
			if (file_exists($file)) {
				echo '<script type="text/javascript" src="'.WEBROOT.$file."\"></script>\n";
			} else {
				$file = "commons/js/$name";
				if (file_exists($file)) {
					echo '<script type="text/javascript" src="'.WEBROOT.$file."\"></script>\n";
				} else {
					if (Utils::remoteFileExists($name)) {
						echo '<script '.$additional.' type="text/javascript" src="'.$name."\"></script>\n";
					}
				}
			}
		}
	}

	public static function printStylesheetLinks()
	{
		if (isset(self::$css)) {
			foreach (self::$css as $css) {
				self::linkStylesheet($css);
			}
		}

		// CSS dei Controller
		if (isset(self::$controllers_css)) {
			foreach (self::$controllers_css as $css) {
				$file = VIEWS_PATH.$css;
				if (file_exists(DOCROOT.WEBROOT.DATA_DIR.$file)) {
					echo '<link href="'.WEBROOT.DATA_DIR.$file."\" rel=\"stylesheet\" type=\"text/css\" />\n";
				} elseif (file_exists(DOCROOT.WEBROOT.$file)) {
					echo '<link href="'.WEBROOT.$file."\" rel=\"stylesheet\" type=\"text/css\" />\n";
				}
			}
		}
	}

	public static function printJSLinks()
	{
		if (isset(self::$js)) {
			foreach (self::$js as $js) {
				self::linkJS($js);
			}
		}

		// JS of Controllers
		if (isset(self::$controllers_js)) {
			foreach (self::$controllers_js as $js) {
				$file = VIEWS_PATH.$js;
				if (file_exists(DOCROOT.WEBROOT.DATA_DIR.$file)) {
					echo '<script type="text/javascript" src="'.WEBROOT.DATA_DIR.$file."\"></script>\n";
				} elseif (file_exists(DOCROOT.WEBROOT.$file)) {
					echo '<script type="text/javascript" src="'.WEBROOT.$file."\"></script>\n";
				}
			}
		}
	}

	public static function AddStylesheet($css)
	{
		$c = 0;
		if (is_array($css)) {
			foreach ($css as $v) {
				if (!in_array($v, self::$css)) {
					array_push(self::$css, $v);
					$c++;
				}
			}
		} elseif (is_string($css)) {
			if (!in_array($css, self::$css)) {
				array_push(self::$css, $css);
				$c++;
			}
		}
		return $c;
	}

	public static function AddJS($js)
	{
		$c = 0;
		if (is_array($js)) {
			foreach ($js as $v) {
				if (!in_array($v, self::$js)) {
					array_push(self::$js, $v);
					$c++;
				}
			}
		} elseif (is_string($js)) {
			if (!in_array($js, self::$js)) {
				array_push(self::$js, $js);
				$c++;
			}
		}

		return $c;
	}

	public static function AddControllerCSS($compname, $css)
	{
		$c = 0;
		if (is_array($css)) {
			foreach ($css as $v) {
				$v = $compname.DS.'css'.DS.$v;
				if (!in_array($v, self::$controllers_css)) {
					array_push(self::$controllers_css, $v);
					$c++;
				}
			}
		} elseif (is_string($css)) {
			$css = $compname.DS.'css'.DS.$css;
			if (!in_array($css, self::$controllers_css)) {
				array_push(self::$controllers_css, $css);
				$c++;
			}
		}

		return $c;
	}

	public static function AddControllerJS($compname, $js)
	{
		$c = 0;
		if (is_array($js)) {
			foreach ($js as $v) {
				$v = $compname.DS.'js'.DS.$v;
				if (!in_array($v, self::$controllers_js)) {
					array_push(self::$controllers_js, $v);
					$c++;
				}
			}
		} elseif (is_string($js)) {
			$js = $compname.DS.'js'.DS.$js;
			if (!in_array($js, self::$controllers_js)) {
				array_push(self::$controllers_js, $js);
				$c++;
			}
		}

		return $c;
	}

	public static function AddMeta($name, $content)
	{
		self::setMetaTag($name, $content);
	}

	public static function printMeta()
	{
		foreach (self::$arr_meta as $name => $content) {
			print "<meta name=\"{$name}\" content=\"{$content}\" />\n";
		}
		foreach (self::$arr_meta_property as $mprop) {
			$name = key($mprop);
			$content = $mprop[$name];
			print "<meta property=\"{$name}\" content=\"{$content}\" />\n";
		}
	}

	public static function printFavicon($name)
	{
		print "<link rel=\"shortcut icon\" href=\"$name\" />\n<link rel=\"icon\" href=\"$name\" />\n";
	}

	public static function printTitle()
	{
		print '<title>'.self::$title.CMSSettings::$company_name."</title>\n";
	}

	public static function printDynTags()
	{
		self::printMeta();
		self::printStylesheetLinks();
		self::printJSLinks();
	}

	public static function addScript($code)
	{
		array_push(self::$arr_scripts, $code);
	}

	public static function printScripts()
	{
		foreach (self::$arr_scripts as $script) {
			echo "\n<script type=\"text/javascript\">\n"
			."$script\n"
			."</script>\n\n";
		}
	}
}
