<?php

header('Content-Type: application/json');

// This serves for listing the images of a path (hierarchy mode) all in JSON format and returns image and thumb;

define(DOCROOT, $_GET['docroot']);
define(MEDIA_PATH, $_GET['media_path']);

define(MEDIA_ROOT, DOCROOT.MEDIA_PATH);

$DirImages = new JsonDirImages();
$DirImages->setMediaUrl(MEDIA_PATH);

//Type the directory that you don't want to include
$DirImages->excludeDir(array(''));

echo $DirImages->hierarchyJsonDirImages(MEDIA_ROOT);

class JsonDirImages
{
	private $media_url;
	private $exclude_dirs = array();
	public static $ctrl = 0;
	public static $result = array();

	public function setMediaUrl($url)
	{
		$this->media_url = rtrim($url, '/').'/';
	}

	public function excludeDir($dirs)
	{
		$this->exclude_dirs = $dirs;
	}

	public function hierarchyJsonDirImages($path = '.')
	{
		$path = rtrim($path, '/');

		if ($images = glob("/{$path}/*.{jpg,png,gif,jpeg}", GLOB_BRACE)) {
			foreach ($images as $img) {
				self::$result[self::$ctrl]['image'] = $this->media_url.ltrim(str_replace(MEDIA_ROOT, '', $img), './');
				self::$result[self::$ctrl]['thumb'] = $this->media_url.ltrim(str_replace(MEDIA_ROOT, '', $img), './');
				self::$result[self::$ctrl]['folder'] = (basename($path) == '.') ? 'media' : basename($path);
				self::$ctrl++;
			}
		}
		if (count(array_filter(glob("{$path}/*"), 'is_dir'))) {
			$dirs = array_filter(glob("{$path}/*"), 'is_dir');

			foreach ($dirs as $dir) {
				if (!in_array(basename($dir), $this->exclude_dirs)) {
					$this->hierarchyJsonDirImages($dir);
				}
			}
		}
		return json_encode(self::$result);
	}
}
