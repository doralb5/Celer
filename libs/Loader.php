<?php

class Loader
{
	private static $DATA_DIR;

	public function __construct()
	{
		self::$DATA_DIR = (defined('DATA_DIR')) ? DATA_DIR : DEFAULT_DATA_DIR;
	}

	/**
	 * Return a Model Object
	 * @param string $modelName , $package
	 * @return BaseModel
	 */
	public static function getModel($modelName, $package = '')
	{
		if (strstr($modelName, '_Component')) {
			$modelName = substr($modelName, 0, strpos($modelName, '_Component'));
		}

		if ($package == '') {
			$package = $modelName;
		}
		//Check in the data folder

		$filename = self::$DATA_DIR.MODELS_PATH.$package.DS.$modelName.'_model.php';
		if (file_exists($filename)) {
			require_once $filename;
			$modName = $modelName.'_Model';
			$model = new $modName();
			return $model;
		}

		$filename = MODELS_PATH.$package.DS.$modelName.'_model.php';
		if (file_exists($filename)) {
			require_once $filename;
			$modelName = $modelName.'_Model';
			$model = new $modelName();
			return $model;
		} else {
			return null;
		}
	}

	public static function runController(&$obj, $action = '', $params = '')
	{
		if (!empty($action)) {
			if (method_exists($obj, $action)) {
				if (!empty($params)) {
					if (is_string($params)) {
						$params = explode('/', $params);
					}
					$classMethod = new ReflectionMethod($obj, $action);
					if (count($params) <= $classMethod->getNumberOfParameters() && count($params) >= $classMethod->getNumberOfRequiredParameters()) {
						return call_user_func_array(array($obj, $action), $params);
					} else {
						var_dump($params);
						echo "Errore: Numero di parametri errato!\n";
					}
				} else {
					return call_user_func(array($obj, $action));
				}
			} else {
				echo 'Errore: Action non valida ('.get_class($obj)."/$action)!!!";
			}
		}
	}

	public static function loadModule($name, $block_id = '', $position = '')
	{
		if (strpos($name, '/')) {
			list($package, $name) = explode('/', $name);
		} else {
			$package = $name;
		}

		$filename = DOCROOT.WEBROOT.self::$DATA_DIR.MODULES_PATH.$package.DS.$name.'.php';
		if (file_exists($filename)) {
			require_once $filename;
			return new $name($block_id, $position, $package);
		}

		$filename = DOCROOT.WEBROOT.MODULES_PATH.$package.DS.$name.'.php';
		if (file_exists($filename)) {
			require_once $filename;
			return new $name($block_id, $position, $package);
		} else {
			echo 'MODULE NOT FOUND IN '.$filename;
			return null;
		}
	}

	public static function loadComponent($component, $package = '')
	{
		if (strstr($component, '_Component')) {
			$component = substr($component, 0, strpos($component, '_Component'));
		}
		if ($package == '') {
			$package = $component;
		}

		$filename = self::$DATA_DIR.COMPONENTS_PATH.$package.DS.$component.'.php';
		if (file_exists($filename)) {
			require_once $filename;
			$compName = $component.'_Component';
			$component = new $compName();
			return $component;
		}

		$filename = COMPONENTS_PATH.$package.DS.$component.'.php';
		if (file_exists($filename)) {
			require_once $filename;
			$compName = $component.'_Component';
			$component = new $compName();
			return $component;
		} else {
			return null;
		}
	}
}
