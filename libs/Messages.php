<?php

class Messages
{
	private static $errors = array();
	private static $info_msg = array();
	private static $alerts = array();

	public static function addError($msg, $id = null)
	{
		if (!is_null($id)) {
			if (!isset($_SESSION[$id]['errors'])) {
				$_SESSION[$id]['errors'] = array();
			}

			array_push($_SESSION[$id]['errors'], $msg);
			return;
		}

		if (!isset($_SESSION['errors'])) {
			$_SESSION['errors'] = array();
		}

		array_push($_SESSION['errors'], $msg);
	}

	public static function addInfoMsg($msg, $id = null)
	{
		if (!is_null($id)) {
			if (!isset($_SESSION[$id]['info_msg'])) {
				$_SESSION[$id]['info_msg'] = array();
			}

			array_push($_SESSION[$id]['info_msg'], $msg);
			return;
		}

		if (!isset($_SESSION['info_msg'])) {
			$_SESSION['info_msg'] = array();
		}

		array_push($_SESSION['info_msg'], $msg);
	}

	public static function existErrors($id = null)
	{
		if (!is_null($id)) {
			if (isset($_SESSION[$id]['errors'])) {
				if (count($_SESSION[$id]['errors'])) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		if (isset($_SESSION['errors'])) {
			if (count($_SESSION['errors'])) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static function showErrors($id = null)
	{
		if (!is_null($id)) {
			if (!isset($_SESSION[$id]['errors'])) {
				return;
			}
			$errors = $_SESSION[$id]['errors'];
			unset($_SESSION[$id]['errors']);
		} else {
			if (!isset($_SESSION['errors'])) {
				return;
			}

			$errors = $_SESSION['errors'];
			unset($_SESSION['errors']);
		}

		foreach ($errors as $err) {
			print '
            <div class="alert alert-danger fade in alert-costumized">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-exclamation-circle"></i>
                </button>
                '.$err.'
            </div>';
		}
	}

	public static function showInfoMsg($id = null)
	{
		if (!is_null($id)) {
			if (!isset($_SESSION[$id]['info_msg'])) {
				return;
			}
			$msgs = $_SESSION[$id]['info_msg'];
			unset($_SESSION[$id]['info_msg']);
		} else {
			if (!isset($_SESSION['info_msg'])) {
				return;
			}

			$msgs = $_SESSION['info_msg'];
			unset($_SESSION['info_msg']);
		}

		foreach ($msgs as $msg) {
			print '
            <div class="alert alert-info fade in alert-costumized">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-info-circle"></i>
                </button>
                '.$msg.'
            </div>';
		}
	}

	public static function addAlert($title, $text, $image = '', $sticky = true, $time = '', $class = 'my-sticky-class')
	{
		$alert = array();

		$_SESSION['popup_alerts'] = isset($_SESSION['popup_alerts']) ? $_SESSION['popup_alerts'] : array();

		$alert['title'] = $title;
		$alert['text'] = $text;
		$alert['image'] = $image;
		$alert['sticky'] = $sticky;
		$alert['time'] = $time;
		$alert['class'] = $class;

		array_push($_SESSION['popup_alerts'], $alert);
	}

	public static function showAlerts()
	{
		if (isset($_SESSION['popup_alerts']) && $_SESSION['popup_alerts'] > 0) {
			$alerts = $_SESSION['popup_alerts'];

			foreach ($alerts as $alert) {
				echo "handleDashboardGritterNotification('{$alert['title']}', '{$alert['text']}', '{$alert['image']}', {$alert['sticky']}, '{$alert['time']}', '{$alert['class']}');";
			}

			unset($_SESSION['popup_alerts']);
		}
	}

	public static function showComponentErrors($component_name = '')
	{
		if (isset($_SESSION['ComponentErrors'][$component_name])) {
			$errors = $_SESSION['ComponentErrors'][$component_name];
			unset($_SESSION['ComponentErrors'][$component_name]);
			if (count($errors)) {
				print '<div class="alert alert-danger fade in alert-costumized">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-exclamation-circle"></i>
                </button>';
				foreach ($errors as $error) {
					print '&bull; '.$error.'<br/>';
				}
				print '</div>';
			}
		}
	}

	public static function showComponentMsg($component_name = '')
	{
		if (isset($_SESSION['ComponentMessages'][$component_name])) {
			$messages = $_SESSION['ComponentMessages'][$component_name];
			unset($_SESSION['ComponentMessages'][$component_name]);
			if (count($messages)) {
				print '<div class="alert alert-info fade in alert-costumized">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-info-circle"></i>
                </button>';
				foreach ($messages as $message) {
					print '&bull; '.$message.'<br/>';
				}
				print '</div>';
			}
		}
	}
}
