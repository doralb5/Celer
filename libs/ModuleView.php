<?php

class ModuleView extends BaseView
{
	public function __construct($package = '')
	{
		parent::__construct();
		$this->setLangPath(MODULES_PATH.$package.DS.'lang'.DS);
		$this->setViewPath(MODULES_PATH.$package.DS.'views'.DS);
		$this->setLang(FCRequest::getLang());
	}

	public function AddJS($name)
	{
		HeadHTML::AddJS($this->view_path.'js/'.$name);
	}

	public function AddStylesheet($name)
	{
		HeadHTML::AddStylesheet($this->view_path.'css/'.$name);
	}
}
