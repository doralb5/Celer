<?php

class Pager {

	function __construct() {
		
	}

	public static function printPager($currpage, $num_art, $limit, $url = null, $prev_text = '', $next_text = '') {

		$prev_text = ($prev_text == '') ? "&laquo;" : $prev_text;
		$next_text = ($next_text == '') ? "&raquo;" : $next_text;

		if (ceil($num_art / $limit) > 1) {

			echo "<div class=\"pages\">
                        <nav class=\"text-center\">";


			$num_pages = ceil($num_art / $limit);

			if ($num_pages > 0) {

				echo "<ul class=\"pagination\">\n";

				if ($currpage > 1) {
					$prev_page = $currpage - 1;
					echo "<li class=\"prev\"><a class=\"prev\" href=\"" . self::getPageUrl($url, $prev_page) . "\">$prev_text</a></li>\n";
				}
//                else {
//                    echo "<li class=\"prev\" class=\"disabled\"><a class=\"prev\" href=\"#\">$prev_text</a></li>\n";
//                }

				for ($i = 1; $i <= $num_pages; $i++) {
					if ($i > $currpage - 5 && $i < $currpage + 5) {
						echo '<li';
						echo ($i == $currpage) ? ' class="active"' : '';
						echo '>';
						echo '  <a href="' . self::getPageUrl($url, $i) . '">' . $i . '</a>';
						echo "</li>\n";
					}
				}

				if ($currpage < $num_pages) {
					$next_page = $currpage + 1;
					echo "<li class=\"next\"><a href=\"" . self::getPageUrl($url, $next_page) . "\">$next_text</a></li>\n";
				}
//                else {
//                    echo "<li class=\"next\" class=\"disabled\"><a class=\"next\" href=\"#\">$next_text</a></li>\n";
//                }

				echo "</ul>\n";
			}


			echo "      </nav>
                    </div>";
		}
	}

	private static function getPageUrl($urlstring, $page) {

//        $url = substr($urlstring, 0, strrpos($urlstring, '/') + 1);
//        $url .= $page;

		if (is_null($urlstring)) {
			if (isset($_GET['req'])) {
				unset($_GET['req']);
			}
			if (isset($_GET['page'])) {
				$_GET['page'] = $page;
			}

			$query_str = http_build_query($_GET);

			$base_uri = explode('?', $_SERVER['REQUEST_URI']);

			$urlstring = "http://{$_SERVER['HTTP_HOST']}{$base_uri[0]}" . (($query_str != '') ? "?$query_str" : '');

			if (isset($_GET['page']))
				$url = $urlstring;
			elseif (count($_GET)) {
				$url = $urlstring . '&page=' . $page;
			} else {
				$url = $urlstring . '?page=' . $page;
			}

			return $url;
		} elseif (strpos($urlstring, '?'))
			$url = $urlstring . '&page=' . $page;
		else
			$url = $urlstring . '?page=' . $page;

		return $url;
	}

}
