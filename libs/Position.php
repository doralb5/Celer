<?php

class Position
{
	private $position_name;
	private $id_page;
	private $blocks = array();
	private $db;

	public function __construct($name = '', $id_page = '')
	{
		$this->position_name = $name;
		$this->id_page = $id_page;
		$this->db = Database::getInstance();
	}

	public function printContent($echo = true)
	{
		$blocks = $this->getBlocks();
		foreach ($blocks as $block_id => $b) {
			if ($echo) {
				echo '<div id="pb_'.$block_id.'" class="'.$b['class'].'">';
				echo $b['content'];
				echo '</div>';
			} else {
				return $b['content'];
			}
		}
	}

	public function getBlocks()
	{
		return $this->blocks;
	}

	public function setBlock($id, $content, $class = '')
	{
		$this->blocks[$id]['content'] = $content;
		$this->blocks[$id]['class'] = $class;
	}

	public function countBlocks()
	{
		return count($this->blocks);
	}

	//    public function isEnabledByLayout($layout) {
//        $result = $this->db->select("SELECT * FROM layouts WHERE name = :name",array(':name'=>$layout));
//        if(count($result)){
//            $positions = explode(';', $result[0]['positions']);
//
//            if(in_array($this->position_name, $positions)){
//                return true;
//            }
//        }
//        return false;
//    }
}
