<?php

class QuerySearch
{
	private $db;
	private $select;
	private $from;
	private $where = '';
	private $orderby = '';
	private $limit = 10;
	private $offset = 0;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function select($select)
	{
		$this->select = $select;
	}

	public function from($from)
	{
		$this->from = $from;
	}

	public function where($where)
	{
		$this->where = $where;
	}

	public function orderby($orderby)
	{
		$this->orderby = $orderby;
	}

	public function limit($limit, $offset = 0)
	{
		$this->limit = $limit;
		$this->offset = $offset;
	}

	public function search($keywords, array $fields, &$num_rows = 0)
	{
		if ($this->select == '' || $this->from == '' || count($fields) == 0) {
			return false;
		}

		$pertinenceField = $this->getPertinenceSQL($keywords, $fields, 0);

		$select = $this->select.', '.$pertinenceField.' AS pertinence';
		$from = $this->from;
		$where = $this->where.' AND '.$pertinenceField.'> 0';
		$groupby = '';
		$orderby = 'ORDER BY pertinence DESC'.(($this->orderby != '') ? ','.$this->orderby : '');
		$limit = 'LIMIT '.$this->limit.' OFFSET '.$this->offset;

		$query = $select.' '.$from.' '.$where.' '.$groupby.' '.$orderby;
		$qcount = "SELECT COUNT(*) AS numrows FROM ($query) AS tab";
		$resqcount = $this->db->select($qcount);
		$num_rows = (isset($resqcount[0]['numrows']) ? $resqcount[0]['numrows'] : 0);
		$results = $this->db->select($query.' '.$limit);
		return $results;
	}

	public function getSearchQuery(&$fpdo, $keywords, array $fields, &$num_rows = 0, $searchlevel = 0)
	{
		if ($fpdo->getQuery() == '' || count($fields) == 0) {
			return false;
		}

		$pertinenceField = $this->getPertinenceSQL($keywords, $fields, $searchlevel);

		$fpdo->select($pertinenceField.' AS pertinence');

		$filter = $pertinenceField.'> 0';

		if ($this->where != '') {
			$filter .= "AND {$this->where}";
		}

		$fpdo->where($filter);

		$fpdo->groupBy('id');
		$fpdo->orderBy('pertinence DESC');
		if ($this->orderby != '') {
			$fpdo->orderBy($this->orderby);
		}
		//if ($this->limit != '') {
		//$fpdo->limit($this->limit);
		//}
		if ($this->offset != '') {
			$fpdo->offset($this->offset);
		}
		//echo $query;
		$qcount = 'SELECT COUNT(*) AS numrows FROM ('.$fpdo->getQuery().') AS tab';
		$resqcount = $this->db->select($qcount);
		$num_rows = (isset($resqcount[0]['numrows']) ? $resqcount[0]['numrows'] : 0);
		//echo $fpdo->getQuery();
		return $fpdo;

		//        $results = $this->db->select($fpdo->getQuery());
//        return $results;
	}

	// CreaQueryRicerca: Creo la query di ricerca.
	// peso title se non specificato=5, peso testo se non specificato=3
	// searchlevel -> 1 o 0. default 1. Se 0 trova parole non complete. Es. cerchi osso?ok anche ossobuco. Se 1 non succede.
	public function getPertinenceSQL($queryvar, array $fields, $searchlevel = 1)
	{
		foreach ($fields as $f) {
			$f['peso'] = (isset($f['peso'])) ? $f['peso'] : 50;
		}

		//$pesokeywords = $pesotesto;
		// trasformo la stringa in un array di parole da cercare
		$arrayToFind = $this->QueryToArray($queryvar);
		// numero elementi da cercare
		$words = count($arrayToFind);

		if ($words == 0) {
			return false;
		} else {

			// punteggio massimo raggiungibile
			$maxPoint = 0;
			foreach ($fields as $f) {
				$maxPoint = $maxPoint + $words * $f['peso'];
			}

			$query = 'ROUND((';
			$sqlwhere = '';
			// ciclo per ogni parola trovata ($Valore)
			foreach ($arrayToFind as $Indice => $Valore) {
				// se $Valore è presente in title instr(title, '$Valore') restituirà 1 altrimenti 0
				// moltiplico il valore restituito (1 o 0) per il peso della parola (5 per il title, 3 per testo)
				if ($searchlevel == 1) {
					// regexp: uso espressioni regolari. [[:<:]] equivale a \b per separare parole
					foreach ($fields as $f) {
						$query .= "\n"."(({$f['field']} REGEXP '[[:<:]]".$Valore."[[:>:]]')>0)*{$f['peso']}+";
					}

					//                  $sqlwhere.="$titleField REGEXP '[[:<:]]".$Valore."[[:>:]]' OR ";
//                  $sqlwhere.="$descrizioneField REGEXP '[[:<:]]".$Valore."[[:>:]]' OR ";
					//$sqlwhere.="p.title_it REGEXP '[[:<:]]".$Valore."[[:>:]]' OR p.content_it REGEXP '[[:<:]]".$Valore."[[:>:]]' OR ";
				} else {
					foreach ($fields as $f) {
						$query .= "\n"."(instr({$f['field']}, '$Valore')>0)*{$f['peso']}+";
					}

					//                $sqlwhere.="$titleField like '%$Valore%' OR $descrizioneField like '%$Valore%' OR ";
				}
			}
			//        $sqlwhere=substr($sqlwhere, 0, strlen($sqlwhere)-4);
			// calcolo la percentuale di rilevanza  --> rilevanza*100/$maxPoint
			$query .= "0)*100/$maxPoint,2)";
			return $query;
		}
	}

	// QueryToArray: Restituisce array delle parole chiave da cercare
	public function QueryToArray($queryvar)
	{
		// pulisco query da parole accessorie e caratteri non alfanumerici
		$querypulita = $this->cleanQuery($queryvar);
		// costruisco l'array contenente tutte le parole da cercare
		$arraySearch = explode(' ', $querypulita);
		// elimino doppioni dall'array
		$arraySearchUnique = array_unique($arraySearch);
		// elimino valori array vuoti o con solo spazi
		$arrayVuoto = array('', ' ');
		$arrayToReturn = array_diff($arraySearchUnique, $arrayVuoto);
		return $arrayToReturn;
	}

	// PulisciQuery: Restituisce la query pulita da porole inutili(congiunzioni etc.)
	public function cleanQuery($queryvar)
	{
		// array parole di cui non tener conto nelle ricerche
		$arrayBadWord = array('lo', 'l', 'il', 'la', 'i', 'gli', 'le', 'uno', 'un', 'una', 'un', 'su', 'sul', 'sulla', 'sullo', 'sull', 'in', 'nel', 'nello', 'nella', 'nell', 'con', 'di', 'da', 'dei', 'd', 'della', 'dello', 'del', 'dell', 'che', 'a', 'dal', 'è', 'e', 'per', 'non', 'si', 'al', 'ai', 'allo', 'all', 'al', 'o');
		$queryclean = strtolower($queryvar);
		for ($a = 0; $a < count($arrayBadWord); $a++) {
			// sostituisco bad words con espressioni regolari \b ->solo se parole singole, non facenti parti di altre
			$queryclean = preg_replace("/\b".$arrayBadWord[$a]."\b/", '', $queryclean);
		}
		// elimino tutti caratteri non alfanumerici sostituendeli con uno spazio
		$queryclean = preg_replace("/\W/", ' ', $queryclean);
		return $queryclean;
	}
}
