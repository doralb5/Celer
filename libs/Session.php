<?php

/**
 * Class Session
 * @version 1.0
 * @author Antonio Rotundo <a.rotundo@bluehat.it>
 */
class Session
{
	public static function start()
	{
		@session_start();
	}

	public static function set($key, $value)
	{
		$_SESSION[$key] = $value;
	}

	public static function get($key)
	{
		if (isset($_SESSION[$key])) {
			return $_SESSION[$key];
		} else {
			return null;
		}
	}

	/**
	 * Effettua unset della sessione $key
	 * @param string $key
	 */
	public static function clear($key)
	{
		unset($_SESSION[$key]);
	}

	public static function destroy()
	{
		session_destroy();
	}
}
