<?php

class StringUtils
{
	public static function CutString($string, $max_char, &$altro = 0)
	{
		if (strlen($string) > $max_char) {
			$stringa_tagliata = substr($string, 0, $max_char);
			$last_space = strrpos($stringa_tagliata, ' ');
			$stringa_ok = substr($stringa_tagliata, 0, $last_space);
			$altro = strlen(substr($string, strlen($stringa_ok)));
			return $stringa_ok.'...';
		} else {
			$altro = 0;
			return $string;
		}
	}

	public static function TagliaStringaInArray($stringa, $max_char, &$altro = 0)
	{
		//questa tagliastringa restituisce un array con più info.

		if (strlen($stringa) > $max_char) {
			$stringa_tagliata = substr($stringa, 0, $max_char);
			$last_space = strrpos($stringa_tagliata, ' ');
			$aString['first_part'] = substr($stringa_tagliata, 0, $last_space);
			$aString['second_part'] = strlen(substr($stringa, strlen($aString['first_part'])));
			$aString['complete'] = $stringa;
			$aString['modified'] = true;
			return $aString;
		} else {
			$aString['modified'] = false;
			$aString['complete'] = $stringa;
			return $aString;
		}
	}

	public static function linkify($text)
	{
		$text = preg_replace("/(^|[\n ])([\w]*?)([\w]*?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", '$1$2<a target="_blank" href="$3" >$3</a>', $text);
		$text = preg_replace("/(^|[\n ])([\w]*?)((www)\.[^ \,\"\t\n\r<]*)/is", '$1$2<a target="_blank" href="http://$3" >$3</a>', $text);
		$text = preg_replace("/(^|[\n ])([\w]*?)((ftp)\.[^ \,\"\t\n\r<]*)/is", '$1$2<a target="_blank" href="ftp://$3" >$3</a>', $text);
		$text = preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", '$1<a target="_blank" href="mailto:$2@$3">$2@$3</a>', $text);
		return ($text);
	}

	public function prepareToView($string, $chars)
	{
		$stringArray = StrUtils::TagliaStringaInArray($string, $chars);
		if ($stringArray['modified']) {
			$stringArray['first_part'] = StrUtils::linkify(nl2br(htmlspecialchars($stringArray['first_part']), ENT_COMPAT));
			$stringArray['second_part'] = StrUtils::linkify(nl2br(htmlspecialchars($stringArray['second_part']), ENT_COMPAT));
			$stringArray['complete'] = StrUtils::linkify(nl2br(htmlspecialchars($stringArray['complete']), ENT_COMPAT));
		} else {
			$stringArray['complete'] = StrUtils::linkify(nl2br(htmlspecialchars($stringArray['complete']), ENT_COMPAT));
		}

		return $stringArray;
	}

	public function prepareToViewNoSpecialChar($string, $chars)
	{
		$stringArray = StrUtils::TagliaStringaInArray($string, $chars);
		if ($stringArray['modified']) {
			$stringArray['first_part'] = StrUtils::linkify($stringArray['first_part'], ENT_COMPAT);
			$stringArray['second_part'] = StrUtils::linkify($stringArray['second_part'], ENT_COMPAT);
			$stringArray['complete'] = StrUtils::linkify($stringArray['complete'], ENT_COMPAT);
		} else {
			$stringArray['complete'] = StrUtils::linkify($stringArray['complete'], ENT_COMPAT);
		}

		return $stringArray;
	}

	public function clearStringFromCode($string)
	{
		return \preg_replace('"-/<script\b[^>]*>(.*?)<\/script>/is"', '', strip_tags($string));
	}

	public function fromJsonToArray_old($string)
	{
		$pre = explode('[', $string); // Get the part of the signed_request we need.
		//echo substr($pre[1], 0, -3); exit;

		$aJson = explode('},', substr($pre[1], 0, -3));
		//var_dump($aJson);
		$notificationsTmp = 'response: {';
		foreach ($aJson as $k => $v) {
			if ($k != 0) {
				$notificationsTmp .= ',';
			}
			$notificationsTmp .= $k.':'.$v.'}';
		}
		//$notificationsTmp .= "}";
		echo $notificationsTmp;

		$arrayJson = json_decode($notificationsTmp, true);
		echo '<br />';
		echo '<br />';
		var_dump($arrayJson);
		exit;
		return json_decode('response: '.substr($pre[1], 0, -3), true); // Split the JSON into arrays.
	}

	/**
	 *
	 * @param string $oldName
	 * @return string without every char that aren't letters or numbers
	 */
	public static function GetValidString($oldName)
	{
		$newName = '';
		$name = $oldName;
		for ($i = 0; $i < strlen($name); $i++) {
			$char = substr($name, $i, 1);
			if (!preg_match('/^[a-zA-Z0-9]$/', $char)) {
				$char = '';
			}
			$newName .= $char;
		}
		return $newName;
	}
}
