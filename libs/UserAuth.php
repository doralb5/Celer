<?php

class UserAuth
{
	const PERM_SUPER = '1';

	private static $auth_index = 'auth';
	private static $userauth_index = 'user_auth';
	private static $brand_index = 'brand_info';
	public static $previous_login = 'previous_login';

	public static function checkAuthentication($username, $password, &$user = array())
	{
		$db = Database::getInstance();
		
		$User_Table = TABLE_PREFIX.'User';
		
		$password = md5($password);
		if (ADMIN) {
			$sql = "SELECT * FROM $User_Table WHERE (email=:uname or username=:uname) AND password=:pass AND enabled='1' AND admin='1'";
		} else {
			$sql = "SELECT * FROM $User_Table WHERE (email=:uname or username=:uname) AND password=:pass AND enabled='1'";
		}
		
		$result = $db->select($sql, array(':uname' => $username, ':pass' => $password));
		$num = count($result);

		if ($num == 1) {
			$user = $result[0];
			return true;
		} else {
			return false;
		}
	}

	public static function checkAuthenticationByEmail($email, $password, &$user = array())
	{
		$db = Database::getInstance();

		$User_Table = TABLE_PREFIX.'User';

		$password = md5($password);
		if (ADMIN) {
			$sql = "SELECT * FROM $User_Table WHERE email=:mail AND password=:pass AND enabled='1' AND admin='1'";
		} else {
			$sql = "SELECT * FROM $User_Table WHERE email=:mail AND password=:pass AND enabled='1'";
		}
		$result = $db->select($sql, array(':mail' => $email, ':pass' => $password));
		$num = count($result);

		if ($num == 1) {
			$user = $result[0];
			return true;
		} else {
			return false;
		}
	}

	public static function getUserByFb($facebook_id)
	{
		$db = Database::getInstance();
		$User_Table = TABLE_PREFIX.'User';
		if (ADMIN) {
			$sql = "SELECT * FROM $User_Table WHERE facebook_id=:fb_id AND admin='1'";
		} else {
			$sql = "SELECT * FROM $User_Table WHERE facebook_id=:fb_id ";
		}
		$result = $db->select($sql, array(':fb_id' => $facebook_id));
		$num = count($result);
		if ($num == 1) {
			$user = $result[0];
			return $user;
		} else {
			return false;
		}
	}

	public static function setLoginSession($user)
	{
		if (ADMIN) {
			Session::set(self::$auth_index, $user);
		} else {
			Session::set(self::$userauth_index, $user);
		}
	}

	public static function setLoginSessionFrontend($user)
	{
		Session::set(self::$userauth_index, $user);
	}

	public static function getLoginSession()
	{
		if (ADMIN && self::checkLoginSession()) {
			return Session::get(self::$auth_index);
		}
		if (self::checkFrontLoginSession()) {
			return Session::get(self::$userauth_index);
		}
		return null;
	}

	public static function checkLoginSession()
	{
		if (ADMIN) {
			if (!is_null(Session::get(self::$auth_index))) {
				return true;
			} else {
				return false;
			}
		} else {
			if (!is_null(Session::get(self::$userauth_index))) {
				return true;
			} else {
				return false;
			}
		}
	}

	public static function checkAdminSession()
	{
		if (!is_null(Session::get(self::$auth_index))) {
			return true;
		} else {
			return false;
		}
	}

	public static function checkFrontLoginSession()
	{
		if (!is_null(Session::get(self::$userauth_index))) {
			return true;
		} else {
			return false;
		}
	}

	public static function unsetLoginSession()
	{
		if (ADMIN) {
			Session::clear(self::$auth_index);
		} else {
			Session::clear(self::$userauth_index);
		}
	}

	public static function getAuthSessionIndex()
	{
		return self::$auth_index;
	}

	public static function getUserAuthSessionIndex()
	{
		return self::$userauth_index;
	}

	public static function setBrandInfoSession($brand)
	{
		Session::set(self::$brand_index, $brand);
	}

	public static function getBrandInfoSession()
	{
		return Session::get(self::$brand_index);
	}

	public static function setPreviousLogin($user)
	{
		$_SESSION[self::$previous_login] = $user;
	}

	public static function getPreviousLogin()
	{
		if (self::checkPreviousLogin()) {
			return $_SESSION[self::$previous_login];
		} else {
			return null;
		}
	}

	public static function checkPreviousLogin()
	{
		if (isset($_SESSION[self::$previous_login])) {
			return true;
		} else {
			return false;
		}
	}

	public static function unsetPreviousLogin()
	{
		unset($_SESSION[self::$previous_login]);
	}

	public static function checkComponentPerms($component, &$permission = array())
	{
		$loggeduser = self::getLoginSession();

		if ($loggeduser['super'] == self::PERM_SUPER) {
			return true;
		}

		$UserPermission_Table = TABLE_PREFIX.'UserPermission';
		$User_Table = TABLE_PREFIX.'User';
		$Component_Table = TABLE_PREFIX.'Component';

		$db = Database::getInstance();
		$sql = "SELECT up.id,up.disabled_functions FROM $UserPermission_Table up "
				."INNER JOIN $User_Table u ON u.id = up.id_user "
				."INNER JOIN $Component_Table comp ON comp.id = up.id_component "
				.'WHERE id_user = :iduser AND comp.name = :component';
		$result = $db->select($sql, array(':iduser' => $loggeduser['id'], ':component' => $component));

		if (count($result)) {
			$permission = $result[0];
			return true;
		} else {
			return false;
		}
	}

	public static function checkActionPerms($component, $action)
	{
		$loggeduser = self::getLoginSession();

		if ($loggeduser['type'] == self::PERM_SUPER) {
			return true;
		}

		$permission = array();
		if (!self::checkComponentPerms($component, $permission)) {
			return false;
		}

		if (count($permission)) {
			$disabled_functions = explode(';', $permission['disabled_functions']);
			if (in_array($action, $disabled_functions)) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
}
