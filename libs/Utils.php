<?php

class Utils
{
	private static $lang = 'en';

	public static function setLang($lang)
	{
		self::$lang = $lang;
	}

	public static function getLang()
	{
		return self::$lang;
	}

	public static function get_mimetype($filename)
	{
		if (function_exists('finfo_file')) {
			$mimepath = '/usr/share/misc/magic'; //Cambia in base alla configurazione del server php - ATTENZIONE!!!! Abilitare l'accesso Apache (OpenBaseDir) al percorso...
			if (!file_exists($mimepath) || !is_file($mimepath)) {
				$mimepath = '/usr/share/file/magic.mgc';
				if (!file_exists($mimepath)) {
					throw new Exception('Magic File non trovato!');
				}
			}
			$mime = finfo_open(FILEINFO_MIME, $mimepath);
			if ($mime === false) {
				throw new Exception('Unable to open finfo');
			}
			$filetype = finfo_file($mime, $filename);
			finfo_close($mime);
			if ($filetype === false) {
				throw new Exception('Unable to recognise filetype');
			}
		} else {
			ob_start();
			system("file -i -b {$filename}");
			$output = ob_get_clean();
			$output = explode('; ', $output);
			if (is_array($output)) {
				$output = $output[0];
			}
			$filetype = $output;
		}

		return $filetype;
	}

	public static function get_mimetype_from_buffer($string)
	{
		if (function_exists('finfo_file')) {
			//$mimepath='/usr/share/misc/magic.mgc'; //Cambia in base alla configurazione del server php - ATTENZIONE!!!! Abilitare l'accesso Apache (OpenBaseDir) al percorso...
			$mime = finfo_open(FILEINFO_MIME);
			if ($mime === false) {
				throw new Exception('Unable to open finfo');
			}
			$filetype = finfo_buffer($mime, $string);
			finfo_close($mime);
			if ($filetype === false) {
				throw new Exception('Unable to recognise filetype');
			}
		} else {
			throw new Exception('Unable to recognise filetype');
		}

		return $filetype;
	}

	public static function genPath($filename)
	{
		$WEBROOT = WEBROOT_FRONT;

		if (file_exists($filename)) {
			return $WEBROOT.$filename;
		}

		if (substr($filename, 0, 2) == '//' || substr($filename, 0, 5) == 'http:' || substr($filename, 0, 6) == 'https:') {
			if (Utils::remoteFileExists($filename)) { //echo "founded remote<br/><br/>";
				return $filename;
			}
		}

		$TEMPLATE_PATH = TEMPLATES_PATH.CMSSettings::$template.DS;

		$file = $WEBROOT.DATA_DIR.$TEMPLATE_PATH.$filename; //echo $file."<br/>";
		if (file_exists(DOCROOT.$file)) { //echo "founded 1<br/><br/>";
			return $file;
		} else {
			$file = $WEBROOT.$TEMPLATE_PATH.$filename; //echo $file."<br/>";
			if (file_exists(DOCROOT.$file)) { //echo "founded 2<br/><br/>";
				return $file;
			} else {
				$file = $WEBROOT."commons/$filename"; //echo $file."<br/>";
				if (file_exists(DOCROOT.$file)) { //echo "founded 3<br/><br/>";
					return $file;
				}
			}
		}

		return null;
	}

	public static function thumb($f, $options = array())
	{
		require_once DOCROOT.LIBS_PATH.'phpThumb/phpthumb.class.php';
		$phpThumb = new phpthumb();
		$phpThumb->setSourceFilename($f);

		$checkcache = true;
		if (isset($_GET['nocache']) && $_GET['nocache'] == '1') {
			$checkcache = false;
		}

		$phpThumb->setParameter('q', 98);

		foreach ($_GET as $p => $v) {
			if ($p != 'url') {
				$phpThumb->setParameter($p, $v);
			}
		}
		foreach ($options as $p => $v) {
			$phpThumb->setParameter($p, $v);
		}

		$type = mime_content_type($f);
		if (strpos($type, 'image/png;') !== false) {
			$phpThumb->setParameter('f', 'png');
		}

		//        if($width>0)$phpThumb->setParameter('w', $width);
		//        if($height>0)$phpThumb->setParameter('h', $height);
		//        $phpThumb->setParameter ('aoe', true);
		//        if($s == '1' || $s == true){
		//            $phpThumb->setParameter ('iar', true);
		//        }
//
		//        if(isset($_GET['zc']))
		//            $phpThumb->setParameter ('zc', $_GET['zc']);

		$phpThumb->config_cache_directory = CACHE_PATH;
		$phpThumb->setCacheDirectory();
		$cache_file = $phpThumb->SetCacheFilename();

		if (@is_readable($phpThumb->cache_filename) && $checkcache) {
			$nModified = filemtime($phpThumb->cache_filename);
			header('Last-Modified: '.gmdate('D, d M Y H:i:s', $nModified).' GMT');
			if (@$_SERVER['HTTP_IF_MODIFIED_SINCE'] && ($nModified == strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])) && @$_SERVER['SERVER_PROTOCOL']) {
				header($_SERVER['SERVER_PROTOCOL'].' 304 Not Modified');
				exit;
			}

			if ($getimagesize = @getimagesize($phpThumb->cache_filename)) {
				header('Content-Type: '.phpthumb_functions::ImageTypeToMIMEtype($getimagesize[2]));
			} elseif (preg_match('#\\.ico$#i', $phpThumb->cache_filename)) {
				header('Content-Type: image/x-icon');
			}

			//@readfile($phpThumb->cache_filename);
			//return;
			$cont = file_get_contents($phpThumb->cache_filename);
			return $cont;
		}
		$phpThumb->GenerateThumbnail();
		$res = $phpThumb->RenderToFile($phpThumb->cache_filename);
		//print_r($phpThumb->debugmessages);
		//$phpThumb->OutputThumbnail();
		//return;
		if ($res) {
			$cont = file_get_contents($phpThumb->cache_filename);
		} else {
			ob_start();
			$phpThumb->OutputThumbnail();
			$cont = ob_get_contents();
			ob_end_clean();
		}
		return $cont;
	}

	public static function thumb_old($f)
	{
		$par = '';
		foreach ($_GET as $k => $v) {
			if ($k != 'url') {
				if ($k == 'w' || $k == 'h') {
					$v = (int) $v;
				}
				$par .= "&$k=$v";
			}
		}
		if (preg_match('#(.*)\..png$#i', $f)) {
			$par .= '&f=png';
		}

		unset($_GET['url']);
		$_GET['src'] = WEBROOT.$f;

		//ob_start();
		require(LIBS_PATH.'phpThumb/phpThumb.php');
		//$file_cont = ob_get_contents();
		//ob_end_clean();
		//$file_cont = file_get_contents(((isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS'])?'https://':'http://').$_SERVER['HTTP_HOST'].WEBROOT.LIBS_PATH.'phpThumb/phpThumb.php?src=' . urlencode(WEBROOT.$f).$par);
		//return $file_cont;
	}

	public static function StartsWith($text, $search, $ignoreCase = false)
	{
		$s = substr($text, 0, strlen($search));
		if ($ignoreCase === true) {
			return (strtolower($s) == strtolower($search));
		}
		return ($s == $search);
	}

	public static function EndsWith($text, $search, $ignoreCase = false)
	{
		$s = substr($text, -strlen($search));
		if ($ignoreCase === true) {
			return (strtolower($s) == strtolower($search));
		}
		return ($s == $search);
	}

	public static function ConvertSizeFromBytes($bytes, $to = null)
	{
		$float = floatval($bytes);
		switch ($to) {
			case 'Kb':			// Kilobit
				$float = ($float * 8) / 1024;
				break;
			case 'b':			 // bit
				$float *= 8;
				break;
			case 'GB':			// Gigabyte
				$float /= 1024;
				// no break
			case 'MB':			// Megabyte
				$float /= 1024;
				// no break
			case 'KB':			// Kilobyte
				$float /= 1024;
				// no break
			default:			  // byte
		}
		unset($bytes, $to);
		$float = round($float, 1);
		return ($float);
	}

	public static function ExtractSearchKeywords($searchText)
	{
		$keys = array();
		$testo = preg_replace("/\s{2,}/", ' ', $searchText);
		preg_match_all('|"[^"]+"|', $testo, $multiple);
		foreach ($multiple[0] as $frase) {
			$testo = str_replace($frase, '', $testo);
			$keys[] = str_replace('"', '', $frase);
		}
		$arr_testo = explode(' ', $testo);
		foreach ($arr_testo as $s) {
			if ($s == '' || in_array($s, $keys)) {
				continue;
			}
			$keys[] = $s;
		}
		return $keys;
	}

	public static function CalcPercentage($total, $count)
	{
		if ($total == 0 || $count == 0) {
			return 0;
		}
		$perc = number_format((floatval($count) / floatval($total)) * 100, 2);
		if (substr($perc, -2) == '00') {
			return substr($perc, 0, strlen($perc) - 3);
		}
		if (substr($perc, -1) == '0') {
			return substr($perc, 0, strlen($perc) - 1);
		}
		return $perc;
	}

	public static function GetPageOffset($page, $limit)
	{
		if ($page == 1) {
			return 0;
		}
		return $limit * ($page - 1);
	}

	public static function GetPagesCount($count, $limit)
	{
		return ceil($count / $limit);
	}

	public static function IsUtf8($string)
	{
		//return (mb_detect_encoding($string) == "UTF-8");
		return (mb_detect_encoding($string, mb_detect_order(), true) == 'UTF-8');
	}

	public static function Utf8Encode(&$object, $property = null)
	{
		self::ApplyEncoding($object, $property, 'utf8_encode');
	}

	public static function Utf8Decode(&$object, $property = null)
	{
		self::ApplyEncoding($object, $property, 'utf8_decode');
	}

	public static function ApplyEncoding(&$object, $property, $encodingFunction)
	{
		if (!$object) {
			return;
		}
		if (is_array($object)) {
			if (count($object) == 0) {
				return;
			}
			$isNumericArray = false;
			foreach ($object as $k => $v) {
				$isNumericArray = ($k === 0);
				break;
			}
			if ($isNumericArray) {
				for ($i = 0; $i < count($object); $i++) {
					if (!$property) {
						if (is_array($object[$i])) {
							foreach ($object[$i] as $k => $v) {
								$object[$i][$k] = $encodingFunction($object[$i][$k]);
							}
							continue;
						}
						if (is_object($object[$i])) {
							$props = get_class_vars(get_class($object[$i]));
							foreach ($props as $k => $v) {
								$object[$i]->$k = $encodingFunction($object[$i]->$k);
							}
							continue;
						}
						$object[$i] = $encodingFunction($object[$i]);
						continue;
					}
					if (is_numeric($property) && $property >= 0) {
						$object[$i][$property] = $encodingFunction($object[$i][$property]);
					} else {
						foreach ($object[$i] as $k => $v) {
							$encode = false;
							if (is_array($property)) {
								foreach ($property as $p) {
									if ($k == $p) {
										$encode = true;
									}
								}
							} elseif ($k == $property) {
								$encode = true;
							}
							if ($encode) {
								if (is_array($object[$i])) {
									$object[$i][$k] = $encodingFunction($object[$i][$k]);
								} else {
									$object[$i]->$k = $encodingFunction($object[$i]->$k);
								}
							}
						}
					}
				}
			} else {
				if (!$property) {
					foreach ($object as $k => $v) {
						if (is_array($v)) {
							$vt = array();
							foreach ($v as $vk => $vv) {
								$vt[$vk] = $encodingFunction($vv);
							}
							$object[$k] = $v;
						} else {
							$object[$k] = $encodingFunction($v);
						}
					}
					return;
				}
				if (is_array($property)) {
					foreach ($property as $p) {
						if ($k == $p) {
							$object[$k] = $encodingFunction($object[$k]);
						}
					}
				} else {
					$object[$property] = $encodingFunction($object[$property]);
				}
			}
		} elseif (is_object($object)) {
			$props = get_class_vars(get_class($object));
			foreach ($props as $k => $v) {
				$encode = false;
				if (is_array($property)) {
					foreach ($property as $p) {
						if ($k == $p) {
							$encode = true;
							break;
						}
					}
				} elseif ($k == $property) {
					$encode = true;
				}
				if ($encode) {
					$object->$k = $encodingFunction($object->$k);
				}
			}
		}
	}

	public static function GetValidName($oldName)
	{
		$newName = '';
		$name = $oldName;
		for ($i = 0; $i < strlen($name); $i++) {
			$char = substr($name, $i, 1);
			if (!preg_match('/^[a-zA-Z0-9]$/', $char)) {
				$char = '_';
			}
			$newName .= $char;
		}
		return $newName;
	}

	/**
	 * Get a normalized string to use as filename starting from a specified file path, replacing all the non alphanumeric characters with an underscore
	 *
	 * @param string $oldName String to normalize
	 * @param type $newExt New extension to append to the returned string
	 * @return string A valid filename
	 */
	public static function GetValidFilename($oldName, $newExt = '')
	{
		$newName = '';
		$name = $oldName;
		$ext = Utils::GetFilenameExtension($name);
		if ($ext != '') {
			$name = substr($name, 0, strlen($name) - strlen($ext));
		}
		for ($i = 0; $i < strlen($name); $i++) {
			$char = substr($name, $i, 1);
			if (!preg_match('/^[a-zA-Z0-9._-]$/', $char)) {
				$char = '_';
			}
			$newName .= $char;
		}
		$newName .= ($newExt != '' ? $newExt : $ext);
		return $newName;
	}

	/**
	 * Get the extension of a file path
	 *
	 * @param string $filename The file path to check
	 * @return string The file extension
	 */
	public static function GetFilenameExtension($filename)
	{
		$i = strrpos($filename, '.');
		if ($i === false) {
			return '';
		}
		return substr($filename, $i);
	}

	/**
	 * Generate a random temporary filename with ".tmp" extension
	 *
	 * @return string Temporary file name
	 */
	public static function GetTempname($fileExt = '.tmp')
	{
		$tmp_name = md5(microtime()).$fileExt;
		return $tmp_name;
	}

	/**
	 * Generate a random sequence of characters for password
	 *
	 * @param int $length The length of the password to generate
	 * @return string A random password
	 */
	public static function GenerateRandomPassword($length = '8')
	{
		$password = '';
		$ulettera = '';
		// Generate random characters until the requested length
		for ($i = 0; $i < $length; $i++) {
			$letter = chr(rand(48, 122));
			// If it is not alphanumeric then generate another the character
			//while (!ereg("[a-zA-Z0-9]", $letter)) {
			while (!preg_match('/[a-zA-Z0-9]/', $letter)) {
				// Avoid the last selected character
				if ($letter == $ulettera) {
					continue;
				}
				$letter = chr(rand(48, 90));
			}
			// Add the character to the password
			$password .= $letter;
			// Store the latest generated character
			$ulettera = $letter;
		}
		return $password; // restituisci alla funzione
	}

	public static function JavascriptEncodeString($stringa, $quote_char = "'")
	{
		$result = $stringa;
		$result = str_replace('\\', '\\\\', $result);
		$result = str_replace($quote_char, '\\'.$quote_char, $result);
		$result = str_replace('"', "\'", $result);
		return $result;
	}

	public static function StripTags($string)
	{
		$string = str_replace(array("\r", "\n"), ' ', $string);
		$string = str_replace('<br \>', '', $string);
		$string = str_replace('<br />', '', $string);
		return $string;
	}

	public static function DateDiff($interval, $datefrom, $dateto, $using_timestamps = false)
	{
		/*
		  $interval can be:
		  yyyy - Number of full years
		  q - Number of full quarters
		  m - Number of full months
		  y - Difference between day numbers (eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
		  d - Number of full days
		  w - Number of full weekdays
		  ww - Number of full weeks
		  h - Number of full hours
		  n - Number of full minutes
		  s - Number of full seconds (default)
		 */

		if (!$using_timestamps) {
			$datefrom = self::GetTimestamp($datefrom);
			$dateto = self::GetTimestamp($dateto);
		}

		$difference = $dateto - $datefrom; // Difference in seconds
		switch ($interval) {
			case 'yyyy': // Number of full years
				$years_difference = floor($difference / 31536000);
				if (mktime(date('H', $datefrom), date('i', $datefrom), date('s', $datefrom), date('n', $datefrom), date('j', $datefrom), date('Y', $datefrom) + $years_difference) > $dateto) {
					$years_difference--;
				}
				if (mktime(date('H', $dateto), date('i', $dateto), date('s', $dateto), date('n', $dateto), date('j', $dateto), date('Y', $dateto) - ($years_difference + 1)) > $datefrom) {
					$years_difference++;
				}
				$datediff = $years_difference;
				break;
			case 'q': // Number of full quarters
				$quarters_difference = floor($difference / 8035200);
				while (mktime(date('H', $datefrom), date('i', $datefrom), date('s', $datefrom), date('n', $datefrom) + ($quarters_difference * 3), date('j', $dateto), date('Y', $datefrom)) < $dateto) {
					$months_difference++;
				}
				$quarters_difference--;
				$datediff = $quarters_difference;
				break;
			case 'm': // Number of full months
				$months_difference = floor($difference / 2678400);
				while (mktime(date('H', $datefrom), date('i', $datefrom), date('s', $datefrom), date('n', $datefrom) + ($months_difference), date('j', $dateto), date('Y', $datefrom)) < $dateto) {
					$months_difference++;
				}
				$months_difference--;
				$datediff = $months_difference;
				break;
			case 'y': // Difference between day numbers
				$datediff = date('z', $dateto) - date('z', $datefrom);
				break;
			case 'd': // Number of full days
				$datediff = floor($difference / 86400);
				break;
			case 'w': // Number of full weekdays
				$days_difference = floor($difference / 86400);
				$weeks_difference = floor($days_difference / 7); // Complete weeks
				$first_day = date('w', $datefrom);
				$days_remainder = floor($days_difference % 7);
				$odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
				if ($odd_days > 7) {
					// Sunday
					$days_remainder--;
				}
				if ($odd_days > 6) {
					// Saturday
					$days_remainder--;
				}
				$datediff = ($weeks_difference * 5) + $days_remainder;
				break;
			case 'ww': // Number of full weeks
				$datediff = floor($difference / 604800);
				break;
			case 'h': // Number of full hours
				$datediff = floor($difference / 3600);
				break;
			case 'n': // Number of full minutes
				$datediff = floor($difference / 60);
				break;
			default: // Number of full seconds (default)
				$datediff = $difference;
				break;
		}
		return abs($datediff);
	}

	public static function GetUserFriendlyDate($date, $isTimestamp = false)
	{
		$smarty = new Smarty();
		$smarty->config_dir = CONFIGROOT;
		$smarty->configLoad('App.conf', 'common');
		if (!$isTimestamp) {
			$date = self::GetTimestamp($date);
		}

		$SECOND = 1;
		$MINUTE = 60 * $SECOND;
		$HOUR = 60 * $MINUTE;
		$DAY = 24 * $HOUR;
		$MONTH = 30 * $DAY;

		$delta = time() - $date;

		if ($delta < 1 * $MINUTE) {
			return $delta == 1 ? $smarty->getConfigVariable('ago_second') : $delta.' '.$smarty->getConfigVariable('ago_seconds');
		}
		if ($delta < 2 * $MINUTE) {
			return $smarty->getConfigVariable('ago_minute');
		}
		if ($delta < 45 * $MINUTE) {
			return floor($delta / $MINUTE).' '.$smarty->getConfigVariable('ago_minutes');
		}
		if ($delta < 90 * $MINUTE) {
			return $smarty->getConfigVariable('ago_hour');
		}
		if ($delta < 24 * $HOUR) {
			return floor($delta / $HOUR).' '.$smarty->getConfigVariable('ago_hours');
		}
		if ($delta < 48 * $HOUR) {
			return $smarty->getConfigVariable('ago_day');
		}
		if ($delta < 30 * $DAY) {
			return floor($delta / $DAY).' '.$smarty->getConfigVariable('ago_days');
		}
		if ($delta < 12 * $MONTH) {
			$months = floor($delta / $DAY / 30);
			return $months <= 1 ? $smarty->getConfigVariable('ago_month') : $months.' '.$smarty->getConfigVariable('ago_months');
		} else {
			$years = floor($delta / $DAY / 365);
			return $years <= 1 ? $smarty->getConfigVariable('ago_year') : $years.' '.$smarty->getConfigVariable('ago_years');
		}
		return date('d.m.Y H:i', $date);
	}

	/**
	 * Returns UNIX timestamp from the specified date string
	 *
	 * @param DateTime $data Date to check
	 * @param int $add_days Number of days to add to the date
	 * @return int Date/time UNIX timestamp
	 *
	 */
	public static function GetTimestamp($data, $add_days = 0)
	{
		if (strpos($data, ' ') === false) {
			$a_data = $data;
			$a_time = '00:00';
		} else {
			$a_data = substr($data, 0, strpos($data, ' '));
			$a_time = substr($data, strpos($data, ' ') + 1, 5);
		}
		$a_data = explode('/', str_replace('-', '/', $a_data));
		$a_time = explode(':', str_replace('.', ':', $a_time));
		if (strlen($a_data[0]) == 4) {
			// From ISO format (YYYY/MM/DD)
			$timestamp = mktime($a_time[0], $a_time[1], 0, $a_data[1], $add_days + $a_data[2], $a_data[0]);
		} else {
			// From ITA format (DD/MM/YYYY)
			$timestamp = mktime($a_time[0], $a_time[1], 0, $a_data[1], $add_days + $a_data[0], $a_data[2]);
		}
		return $timestamp;
	}

	public static function IsDateNull($date)
	{
		if ($date == '' || strpos($date, '/') === false || $date == '00/00/0000' || $date == '00/00/0000 00:00') {
			return true;
		} else {
			return false;
		}
	}

	public static function IsDateValid($date, $allowEmpty = false)
	{
		if ($date == '') {
			return $allowEmpty;
		}
		if (strpos($date, '/') === false) {
			return false;
		}
		$d = explode('/', $date);
		return checkdate($d[1], $d[0], $d[2]);
	}

	public static function IsValidEmail($email)
	{
		$result = eregi("^[_a-z0-9+-]+(\.[_a-z0-9+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$", $email);
		if ($result == false) {
			return false;
		} else {
			return true;
		}
	}

	public static function JsonEncodeDbRows($rows, $count)
	{
		$rows = $rows;
		for ($i = 0; $i < count($rows); $i++) {
			foreach ($rows[$i] as $k => $v) {
				if (is_array($v) || is_object($v)) {
					continue;
				}
				if (is_numeric($v) || is_bool($v)) {
					continue;
				}
				if ($v == null || $v == 'NULL') {
					$rows[$i][$k] = '';
				} elseif (substr($v, 0, 10) == '00/00/0000' || substr($v, 0, 10) == '0000-00-00') {
					$rows[$i][$k] = '';
				}
			}
		}
		return json_encode(array('total' => $count, 'results' => $rows));
	}

	public static function JsonEncodeDbRow($row)
	{
		return json_encode(array('success' => $row ? true : false, 'data' => $row));
	}

	public static function JsonEncodeSuccessMessage($success = true, $message = '', $errors = array())
	{
		return json_encode(array('success' => $success, 'message' => $message, 'errors' => $errors));
	}

	public static function FlexgridJsonEncodeDbRows($page, $_rows, $totalCount)
	{
		$rows = $_rows;
		for ($i = 0; $i < count($rows); $i++) {
			foreach ($rows[$i] as $k => $v) {
				if (is_numeric($v) || is_bool($v)) {
					continue;
				}
				if ($v == null || $v == 'NULL') {
					$rows[$i][$k] = '';
				} elseif (substr($v, 0, 10) == '00/00/0000' || substr($v, 0, 10) == '0000-00-00') {
					$rows[$i][$k] = '';
				}
			}
		}
		$recs = array();
		$id = 0;
		foreach ($rows as $row) {
			//$recs[] = $row;
			$newrec = array(
				'id' => ++$id,
				'cell' => array());
			foreach ($row as $k => $v) {
				$newrec['cell'][] = $row[$k];
			}
			$recs[] = $newrec;
		}
		return json_encode(array('page' => $page, 'total' => $totalCount, 'rows' => $recs));
	}

	/**
	 * Redirect the browser to the specified URL
	 *
	 * @param string $url URL to redirect
	 */
	public static function RedirectTo($url)
	{
		ob_clean();
		header('Location: '.$url);
		exit;
	}

	/**
	 * Initialize the object properties from an associative array
	 *
	 * @param mixed $obj Object to initialize
	 * @param array $row Associative array from to load the properties
	 * @param mixed $callbackOnExists (Optional) Callback to call when setting a property, it overrides the common property value assignment
	 */
	public static function FillObjectFromRow(&$obj, $row, $stripSlashes = false, $callbackOnExists = false)
	{
		$props = get_class_vars(get_class($obj));
		foreach ($props as $prop => $value) {
			if (array_key_exists($prop, $row)) {
				if (!$callbackOnExists) {
					$value = ($stripSlashes ? trim(stripslashes($row[$prop])) : $row[$prop]);
					if ($stripSlashes && self::IsUtf8($value)) {
						$value = utf8_decode($value);
					}
					$obj->$prop = $value;
				} else {
					$obj->$callbackOnExists($prop, $row[$prop]);
				}
			}
		}
	}

	/**
	 * Returns an associative array from object properties (the property name will be the key)
	 *
	 * @param mixed $obj Object to convert
	 * @return array Associative array filled with properties names and values
	 *
	 */
	public static function ObjectToArray($obj)
	{
		$array = array();
		if (is_a($obj, 'stdClass')) {
			$variables = get_object_vars($obj);
			$keys = array_keys($variables);
			foreach ($keys as $k) {
				$array[$k] = $obj->$k;
			}
		} else {
			$props = get_object_vars($obj);
			foreach ($props as $prop => $value) {
				$array[$prop] = $obj->$prop;
			}
		}
		return $array;
	}

	public static function GetUrlContent($url)
	{
		$ch = curl_init();
		$timeout = 25; // set to zero for no timeout ( attualmente settato a 60)
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$file_contents = curl_exec($ch);
		curl_close($ch);
		return $file_contents;
	}

	public static function curPageURL($absolute = true)
	{
		if ($absolute) {
			$pageURL = 'http';
			if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
				$pageURL .= 's';
			}
			$pageURL .= '://';
			if ($_SERVER['SERVER_PORT'] != '80') {
				$pageURL .= $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
			} else {
				$pageURL .= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
			}
		} else {
			$pageURL = $_SERVER['REQUEST_URI'];
		}
		return $pageURL;
	}

	public static function DateToWords($date, $withHours = false, $lang = 'it')
	{
		switch ($lang) {
			case 'it':
				$mesi = array(1 => 'gennaio', 'febbraio', 'marzo', 'aprile',
					'maggio', 'giugno', 'luglio', 'agosto',
					'settembre', 'ottobre', 'novembre', 'dicembre');

				$giorni = array('domenica', 'lunedì', 'martedì', 'mercoledì',
					'giovedì', 'venerdì', 'sabato');

				list($sett, $giorno, $mese, $anno, $h, $m) = explode('-', date('w-d-n-Y-H-i', strtotime($date)));

				if ($withHours) {
					return ucfirst($giorni[$sett]).' '.$giorno.' '.ucfirst($mesi[$mese]).' '.$anno.' - '.$h.':'.$m;
				} else {
					return ucfirst($giorni[$sett]).' '.$giorno.' '.ucfirst($mesi[$mese]).' '.$anno;
				}
				break;
			default:
				break;
		}
		return $date;
	}

	public static function print_PagesNumbers($currentPage, $tot, $per_page, $linkpage, $get_PageParam = 'page', $aGET = array())
	{
		$get_params = '';

		foreach ($aGET as $k => $v) {
			if ($k != 'pag') {
				$get_params .= "&$k=$v";
			}
		}
		$paginazione = '';
		$tot_pags = ceil($tot / $per_page);

		if ($currentPage > $tot_pags) {
			$currentPage = $tot_pags;
		}

		$view = new View();
		$view->currPage = $currentPage;
		$view->linkpage = $linkpage;
		$view->get_params = $get_params;
		$view->get_PageParam = $get_PageParam;
		$view->tot_pags = $tot_pags;
		$view->render('paginazione');
	}

	/**
	 * Verifica se esiste un file remoto (senza scaricarlo)
	 * @param type $url
	 * @return boolean
	 */
	public static function remoteFileExists($url)
	{
		$curl = curl_init($url);

		//don't fetch the actual page, you only want to check the connection is ok
		curl_setopt($curl, CURLOPT_NOBODY, true);

		//do request
		$result = curl_exec($curl);

		$ret = false;

		//if request did not fail
		if ($result !== false) {
			//if request was ok, check response code
			$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

			if ($statusCode == 200) {
				$ret = true;
			}
		}

		curl_close($curl);

		return $ret;
	}

	/**
	 * Genera l'url completo del campo lingua
	 * @param type $urlpart
	 * @return string
	 */
	public static function genMediaUrl($urlpart, $absolute = false, $admin = false)
	{
		if (!$admin) {
			$webroot = ((strpos(WEBROOT, 'admin/')) > 0) ? substr(WEBROOT, 0, strpos(WEBROOT, 'admin/')) : WEBROOT;
		} else {
			$webroot = WEBROOT;
		}

		$url = str_replace('//', '/', $webroot.MEDIA_ROOT.'/'.$urlpart);

		if ($absolute) {
			$url = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].$url;
		}
		return $url;
	}

	public static function genUrl($urlpart, $absolute = false, $lang = null)
	{
		if (is_null($lang)) {
			$lang = self::$lang;
		}

		$url = str_replace('//', '/', WEBROOT.$lang.'/'.$urlpart);

		if ($absolute) {
			$url = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].$url;
		}
		return $url;
	}

	public static function getComponentUrl($req, $absolute = true)
	{
		$db = Database::getInstance();
		$request = explode('/', $req);

		if (is_dir(COMPONENTS_PATH.$request[0]) || is_dir(DATA_DIR.COMPONENTS_PATH.$request[0])) {
			if (is_file(COMPONENTS_PATH.$request[0].DS.$request[0].'.php') || is_file(DATA_DIR.COMPONENTS_PATH.$request[0].DS.$request[0].'.php')) {
				$component = $request[0];
				array_shift($request);
				$action = $request[0];
				array_shift($request);
			} else {
				$component = $request[0].'/'.$request[1];
				array_shift($request);
				//array_shift($request);
				$action = $request[0];
				array_shift($request);
			}
			$parameters = implode('/', $request);
		} else {
			return Utils::genUrl($req, $absolute, self::$lang);
		}
		if ($parameters == '') {
			$parameters = null;
		}

		$Page_Table = TABLE_PREFIX.'Page';
		$PageCon_Table = TABLE_PREFIX.'PageContent';
		$Component_Table = TABLE_PREFIX.'Component';

		$sql = "SELECT DISTINCT alias FROM {$PageCon_Table}
                INNER JOIN {$Page_Table} ON({$Page_Table}.id = {$PageCon_Table}.id_page) 
                LEFT JOIN {$Component_Table} ON({$Page_Table}.id_component = {$Component_Table}.id) 
                WHERE {$Component_Table}.name=:c AND {$Page_Table}.action=:a AND {$PageCon_Table}.lang=:l ";

		if ($parameters == '') {
			$where = 'AND action_params IS NULL';
			$result = $db->select($sql.$where, array(':c' => $component, ':a' => $action, ':l' => self::$lang));
			$withParams = false;
		} else {
			$where = "AND action_params = '{$parameters}'";
			$result = $db->select($sql.$where, array(':c' => $component, ':a' => $action, ':l' => self::$lang));
			$withParams = count($result) ? true : false;
		}

		if (count($result) == 0 && $parameters != '') {
			$sql .= 'AND action_params IS NULL';
			$result = $db->select($sql, array(':c' => $component, ':a' => $action, ':l' => self::$lang));
			$withParams = count($result) ? false : false;
		}

		/* if (!count($result)) {
		  $result = $db->select($sql, array(':c' => $component, ':a' => $action, ':l' => 'en'));
		  if (!count($result))
		  $result = $db->select($sql, array(':c' => $component, ':a' => $action, ':l' => 'it'));
		  } */

		if (count($result) && !ADMIN) {
			return Utils::genUrl($result[0]['alias'].((!empty($parameters) && !$withParams) ? '/'.$parameters : ''), $absolute, self::$lang);
		} else {
			return Utils::genUrl('com'.'/'.$req, $absolute, self::$lang);
		}
	}

	public static function getControllerUrl($req)
	{
		$db = Database::getInstance();
		$request = explode('/', $req);

		if (is_dir(CONTROLLERS_PATH.$request[0]) || is_dir(DATA_DIR.CONTROLLERS_PATH.$request[0])) {
			if (is_file(CONTROLLERS_PATH.$request[0].DS.$request[0].'.php') || is_file(DATA_DIR.CONTROLLERS_PATH.$request[0].DS.$request[0].'.php')) {
				$controller = $request[0];
				array_shift($request);
				$action = (isset($request[0])) ? $request[0] : 'index';
				array_shift($request);
			} else {
				$controller = $request[0].'/'.$request[1];
				array_shift($request);
				//array_shift($request);
				$action = (isset($request[0])) ? $request[0] : 'index';
				array_shift($request);
			}
			$parameters = implode('/', $request);
		} else {
			return Utils::genUrl($req, true, self::$lang);
		}

		return Utils::genUrl($req, true, self::$lang);
	}

	public static function clean($string)
	{
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}

	/**
	 * Create a web friendly URL slug from a string.
	 *
	 * Although supported, transliteration is discouraged because
	 *     1) most web browsers support UTF-8 characters in URLs
	 *     2) transliteration causes a loss of information
	 *
	 * @author Sean Murphy <sean@iamseanmurphy.com>
	 * @copyright Copyright 2012 Sean Murphy. All rights reserved.
	 * @license http://creativecommons.org/publicdomain/zero/1.0/
	 *
	 * @param string $str
	 * @param array $options
	 * @return string
	 */
	public static function url_slug($str, $options = array())
	{
		// Make sure string is in UTF-8 and strip invalid UTF-8 characters
		$str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

		$defaults = array(
			'delimiter' => '-',
			'limit' => null,
			'lowercase' => true,
			'replacements' => array(),
			'transliterate' => false,
		);

		// Merge options
		$options = array_merge($defaults, $options);

		$char_map = array(
			// Latin
			'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
			'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
			'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
			'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
			'ß' => 'ss',
			'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
			'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
			'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
			'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
			'ÿ' => 'y',
			// Latin symbols
			'©' => '(c)',
			// Greek
			'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
			'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
			'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
			'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
			'Ϋ' => 'Y',
			'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
			'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
			'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
			'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
			'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
			// Turkish
			'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
			'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
			// Russian
			'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
			'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
			'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
			'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
			'Я' => 'Ya',
			'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
			'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
			'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
			'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
			'я' => 'ya',
			// Ukrainian
			'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
			'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
			// Czech
			'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
			'Ž' => 'Z',
			'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
			'ž' => 'z',
			// Polish
			'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
			'Ż' => 'Z',
			'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
			'ż' => 'z',
			// Latvian
			'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
			'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
			'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
			'š' => 's', 'ū' => 'u', 'ž' => 'z'
		);

		// Make custom replacements
		$str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

		// Transliterate characters to ASCII
		if ($options['transliterate']) {
			$str = str_replace(array_keys($char_map), $char_map, $str);
		}

		// Replace non-alphanumeric characters with our delimiter
		$str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

		// Remove duplicate delimiters
		$str = preg_replace('/('.preg_quote($options['delimiter'], '/').'){2,}/', '$1', $str);

		// Truncate slug to max. characters
		$str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

		// Remove delimiter from ends
		$str = trim($str, $options['delimiter']);

		return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
	}

	public static function genThumbnailUrl($urlpart, $width = 0, $height = 0, $parameters = array(), $absolute = false, $baseurl = '')
	{
		$url_exp = explode('/', $urlpart);
		if (count($url_exp) > 1) {
			$filename = $url_exp[count($url_exp) - 1];
			array_pop($url_exp);
			$pathpart = (count($url_exp) > 1) ? implode('/', $url_exp) : $url_exp[0];
		} else {
			$pathpart = '';
			$filename = $url_exp[0];
		}
		$params = '';
		foreach ($parameters as $key => $value) {
			$params .= "$key=$value&";
		}

		if ($baseurl == '') {
			if (ADMIN) {
				$webroot = substr(WEBROOT, 0, strpos(WEBROOT, 'admin/'));
			} else {
				$webroot = WEBROOT;
			}
			$baseurl = $webroot.MEDIA_ROOT;
		} else {
			$baseurl = rtrim($baseurl, '/');
		}

		$pathpart = str_replace('//', '/', $pathpart);
		$filename = str_replace('//', '/', $filename);

		if ($width == 0 && $height == 0) {
			$url = $baseurl.$pathpart.'/'.$filename;
		} else {
			$url = $baseurl.'/thumbs/'.$pathpart.'/'.$width.'x'.$height.'/'.$filename.'?'.rtrim($params, '&');
		}

		if ($absolute && substr($baseurl, 0, 4) != 'http') {
			$url = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].$url;
		}

		return $url;
	}

	public static function trim_text($input, $length, $ellipses = true, $strip_html = true)
	{
		//strip tags, if desired
		if ($strip_html) {
			$input = strip_tags($input);
		}

		//no need to trim, already shorter than trim length
		if (strlen($input) <= $length) {
			return $input;
		}

		//find last space within length
		$last_space = strrpos(substr($input, 0, $length), ' ');
		$trimmed_text = substr($input, 0, $last_space);

		//add ellipses (...)
		if ($ellipses) {
			$trimmed_text .= '...';
		}

		return $trimmed_text;
	}

	public static function createDirectory($structure)
	{
		if (!is_dir($structure)) {
			if (mkdir($structure, 0777, true)) {
				return true;
			} else {
				return ('Failed to create folders: '.$structure);
			}
		} else {
			return false;
		}
	}

	public static function getCurrencyFormat($val, $currency = 'EUR', $symbolPosition = 'left')
	{
		//        $locale='en-US';
		//        $fmt = new NumberFormatter( $locale."@currency=$currency", NumberFormatter::CURRENCY );
		//        $symbol = $fmt->getSymbol(NumberFormatter::CURRENCY_SYMBOL);
		$symbol = '€';
		$val = number_format($val, 2);
		if ($symbolPosition == 'left') {
			return $symbol.' '.$val;
		} else {
			return $val.' '.$symbol;
		}
	}

	//    public static function setLanguageSession($lang) {
	//        $_SESSION['lang'] = $lang;
	//    }
	//    public static function issetLanguageSession() {
	//        if (isset($_SESSION['lang']))
	//            return true;
	//        else
	//            return false;
	//    }
	//    public static function getLanguageSession() {
	//        if (self::issetLanguageSession())
	//            return $_SESSION['lang'];
	//        else
	//            return null;
	//    }
	//Per kerkime dhe ne array te brendshme
	public static function is_in_array($array, $key, $key_value)
	{
		$within_array = false;
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$within_array = is_in_array($v, $key, $key_value);
				if ($within_array == true) {
					break;
				}
			} else {
				if ($v == $key_value && $k == $key) {
					$within_array = true;
					break;
				}
			}
		}
		return $within_array;
	}

	//By default filter only images
	public static function allowedFileType($filename, $allowed = array('gif', 'png', 'jpg', 'jpeg'))
	{
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if (in_array(strtolower($ext), $allowed)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return Word of Month in Albanian Language
	 * @param int $numeric_month
	 * @return string
	 */
	public static function AlbanianWordMonth($numeric_month, $style = 'std')
	{
		$word = '';
		switch ($numeric_month) {
			case 1:
				$word = 'Janar';
				break;
			case 2:
				$word = 'Shkurt';
				break;
			case 3:
				$word = 'Mars';
				break;
			case 4:
				$word = 'Prill';
				break;
			case 5:
				$word = 'Maj';
				break;
			case 6:
				$word = 'Qershor';
				break;
			case 7:
				$word = 'Korrik';
				break;
			case 8:
				$word = 'Gusht';
				break;
			case 9:
				$word = 'Shtator';
				break;
			case 10:
				$word = 'Tetor';
				break;
			case 11:
				$word = 'Nentor';
				break;
			case 12:
				$word = 'Dhjetor';
				break;
		}

		switch ($style) {
			case 'lowercase':
				return strtolower($word);
				break;
			case 'uppercase':
				return strtoupper($word);
				break;
			case 'ucfirst':
				return ucfirst($word);
				break;
			default:
				return $word;
		}
	}

	/**
	 * Return Word of Month in Italian Language
	 * @param int $numeric_month
	 * @return string
	 */
	public static function ItalianWordMonth($numeric_month, $style = 'std')
	{
		$word = '';
		switch ($numeric_month) {
			case 1:
				$word = 'Gennaio';
				break;
			case 2:
				$word = 'Febbraio';
				break;
			case 3:
				$word = 'Marzo';
				break;
			case 4:
				$word = 'Aprile';
				break;
			case 5:
				$word = 'Maggio';
				break;
			case 6:
				$word = 'Giugno';
				break;
			case 7:
				$word = 'Luglio';
				break;
			case 8:
				$word = 'Agosto';
				break;
			case 9:
				$word = 'Settembre';
				break;
			case 10:
				$word = 'Ottobre';
				break;
			case 11:
				$word = 'Novembre';
				break;
			case 12:
				$word = 'Dicembre';
				break;
		}

		switch ($style) {
			case 'lowercase':
				return strtolower($word);
				break;
			case 'uppercase':
				return strtoupper($word);
				break;
			case 'ucfirst':
				return ucfirst($word);
				break;
			default:
				return $word;
		}
	}

	public static function WordMonth($numeric_month, $lang = 'it', $style = 'std')
	{
		if ($lang == 'it') {
			$word = '';
			switch ($numeric_month) {
				case 1:
					$word = 'Gennaio';
					break;
				case 2:
					$word = 'Febbraio';
					break;
				case 3:
					$word = 'Marzo';
					break;
				case 4:
					$word = 'Aprile';
					break;
				case 5:
					$word = 'Maggio';
					break;
				case 6:
					$word = 'Giugno';
					break;
				case 7:
					$word = 'Luglio';
					break;
				case 8:
					$word = 'Agosto';
					break;
				case 9:
					$word = 'Settembre';
					break;
				case 10:
					$word = 'Ottobre';
					break;
				case 11:
					$word = 'Novembre';
					break;
				case 12:
					$word = 'Dicembre';
					break;
			}
		} else {
			switch ($numeric_month) {
				case 1:
					$word = 'Janar';
					break;
				case 2:
					$word = 'Shkurt';
					break;
				case 3:
					$word = 'Mars';
					break;
				case 4:
					$word = 'Prill';
					break;
				case 5:
					$word = 'Maj';
					break;
				case 6:
					$word = 'Qershor';
					break;
				case 7:
					$word = 'Korrik';
					break;
				case 8:
					$word = 'Gusht';
					break;
				case 9:
					$word = 'Shtator';
					break;
				case 10:
					$word = 'Tetor';
					break;
				case 11:
					$word = 'Nentor';
					break;
				case 12:
					$word = 'Dhjetor';
					break;
			}
		}

		switch ($style) {
			case 'lowercase':
				return strtolower($word);
				break;
			case 'uppercase':
				return strtoupper($word);
				break;
			case 'ucfirst':
				return ucfirst($word);
				break;
			default:
				return $word;
		}
	}

	public static function ArrayToEntities($arr, $entityClass)
	{
		$temp_arr = array();
		if (count($arr)) {
			foreach ($arr as $r) {
				$entity = new $entityClass();
				Utils::FillObjectFromRow($entity, $r);

				array_push($temp_arr, $entity);
			}
		}
		return $temp_arr;
	}

	public static function deleteDirectory($dirPath)
	{
		if (!is_dir($dirPath)) {
			throw new InvalidArgumentException("$dirPath must be a directory");
		}
		if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath.'*', GLOB_MARK);
		foreach ($files as $file) {
			if (is_dir($file)) {
				self::deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirPath);
	}

	public static function formatSizeUnits($bytes)
	{
		if ($bytes >= 1073741824) {
			$bytes = number_format($bytes / 1073741824, 2).' GB';
		} elseif ($bytes >= 1048576) {
			$bytes = number_format($bytes / 1048576, 2).' MB';
		} elseif ($bytes >= 1024) {
			$bytes = number_format($bytes / 1024, 2).' KB';
		} elseif ($bytes > 1) {
			$bytes = $bytes.' bytes';
		} elseif ($bytes == 1) {
			$bytes = $bytes.' byte';
		} else {
			$bytes = '0 bytes';
		}

		return $bytes;
	}

	public static function get_client_ip()
	{
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP'])) {
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		} elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		} elseif (isset($_SERVER['HTTP_FORWARDED'])) {
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		} elseif (isset($_SERVER['REMOTE_ADDR'])) {
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		} else {
			$ipaddress = 'UNKNOWN';
		}
		return $ipaddress;
	}

	public static function genVideoThumbnail($input, $output, $fromdurasec)
	{
		$ffmpegpath = '/usr/bin/ffmpeg';
		if (!file_exists($input)) {
			return false;
		}

		$command = "$ffmpegpath -i $input -an -ss 00:00:$fromdurasec -r 1 -vframes 1 -f mjpeg -y $output";

		exec($command, $ret);

		if (!file_exists($output)) {
			return false;
		}
		if (filesize($output) == 0) {
			return false;
		}
		return true;
	}

	public static function saveCurrentPageUrl()
	{
		if (!isset($_SESSION['page_url_history'])) {
			$_SESSION['page_url_history'] = array();
		}

		if (($key = array_search(Utils::curPageURL(), $_SESSION['page_url_history'])) !== false) {
			$toUnset = array();
			for ($i = 0; $i < count($_SESSION['page_url_history']); $i++) {
				if ($i >= $key) {
					array_push($toUnset, $i);
				}
			}
			foreach ($toUnset as $uns) {
				unset($_SESSION['page_url_history'][$uns]);
			}
			$_SESSION['page_url_history'] = array_values($_SESSION['page_url_history']);
		}
		array_push($_SESSION['page_url_history'], Utils::curPageURL());

		//var_dump($_SESSION['page_url_history']);
	}

	public static function getLastPageHistory($optionalUrl = null, $pop = false)
	{
		if (isset($_SESSION['page_url_history'])) {
			if ($pop) {
				$pageUrl = array_pop($_SESSION['page_url_history']);
			} elseif (count($_SESSION['page_url_history'])) {
				$pageUrl = $_SESSION['page_url_history'][count($_SESSION['page_url_history']) - 1];
				if ($pageUrl == Utils::curPageURL()) {
					if (count($_SESSION['page_url_history']) >= 2) {
						$pageUrl = $_SESSION['page_url_history'][count($_SESSION['page_url_history']) - 2];
					} else {
						$pageUrl = $optionalUrl;
					}
				}
			} else {
				$pageUrl = (is_null($optionalUrl)) ? '' : $optionalUrl;
			}
			return $pageUrl;
		} else {
			return $optionalUrl;
		}
	}

	public static function backRedirect($optionalUrl = '', $history = true)
	{
		$back_url = Utils::getLastPageHistory('', true);
		if ($history && !is_null($back_url)) {
			if ($back_url != Utils::curPageURL()) {
				Utils::RedirectTo($back_url);
				return;
			} else {
				self::backRedirect($optionalUrl, $history);
				return;
			}
		} else {
			Utils::RedirectTo($optionalUrl);
		}
	}

	public static function getEmails($text = '')
	{
		//$text = 'QENDRA EUROPIANE E STUDIMIT, KERKON TE PUNESOJE PART TIME NJE SPECIALIST PER KURSE AUTOKADI (MASHKULL) ME KETO KARAKTERISTIKA: -TE KETE EKSPERIENCE NE MESIMDHENIE NE PROGRAMET E AUTOKADIT. -TE JETE I DISPONUESHEM CDO DITE NGA E HENA NE TE SHTUNE PARADITE. TE INTERESUARIT TE DERG CV ME FOTO NE ADRESEN: E-MAIL:info.esc2008@gmail.com OSE TELEFONONI PA HEZITIM NE NR. CEL.068 20 41 009 CEL.069 72 94 402';
		$pattern = '/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}/i';
		preg_match_all($pattern, $text, $matches);
		$email = $matches[0][0];
		if ($email != '') {
			return $email;
		} else {
			return null;
		}
	}

	public static function getPhone($text = '')
	{
		//$text = 'Përshkrimi LUFRA shpk është një kompani Shqiptare, e krijuar rreth 23 vite më parë, me qendër në Lushnjë dhe produkte prezente në të gjithë Shqipërinë. Ajo është sot kompania më e madhe e bulmetit në vend duke ofruar mbi 40 produkte të ndryshme për konsumatorin. Aktualisht jemi duke kërkuar të rekrutojmë një MANAXHER SHITJESH, i/e cila do të jetë përgjegjës për: 1. Mirëmbajtjen e rrjetit ekzistues të klientëve dhe identifikimin e klientëve të rinj 2. Informimin e klientëve mbi kompaninë, produktet dhe shërbimet e saj, përfitimet, bonuset, skemat e ndryshme dhe çdo lloj komunikimi në lidhje me mbledhjen e informacionit të tregut; 3. Ofrimin e shërbimit të shkëlqyer ndaj klientëve; 4. Ndjekjen e proceseve të punës sic përcaktohet në udhëzimet e shitjeve dhe në përputhje me synimet e caktuara nga kompania; 5. Bashkëpunimin me sektorë të tjerë brenda kompanisë për të mundësuar kryerjen e sukseshme të procesit të shitjes sic përcaktohet në rregulloret dhe politikat e shitjeve; 6. Kryerjen aktivitetet e shitjes me qëllim përmbushjen e nevojave të klientëve arritjen e objektivave të paracaktuara të shitjes. 7. Kryerjen e shitjes së produkteve dhe mbledh edhe informacion nga klientët lidhur me nevojat, shërbimet, performancën dhe imazhin e kompanisë. 8. Kujdesin ndaj pajisjeve dhe mjedisit të punës, duke krijuar një atmosferë të ngrohtë në komunikimin me klientët. 9. Ndjekjen e kërkesave të pazgjidhura të klientëve duke i adresuar ato tek sektorët përkatës në kompani; 10. Njohjen, dokumentimin dhe lajmërimin e kolegëve dhe eprorëve rreth ndodhive dhe cështjeve problematike të ngritura nga klientët ose nga vetë ai; 11. Implementimin e politikave dhe procedurave në fuqi të kompanisë; 12. Rekomandimin e përmirësimeve të proceseve, duke identifikuar problemet dhe rreziqeve; 13. Ofrimin e trajnimit për punonjësit e rinj. Kualifikimet / Kërkesat Kandidati duhet të plotësojë kriteret e mëposhtme: • Diplomë Universitare; • Eksperiencë e mëparshme në fushën e shitjeve përbën avantazh; • Njohuri të mira të gjuhës angleze; • Aftësi të mira në kompjuter dhe aftësi për të mësuar programet/sistemet aplikative të shitjes; • Aftësi të shkëlqyera komunikimi; person energjik dhe i motivuar; • Të jetë i përgjegjshëm dhe i kujdesshëm ndaj detajeve; • Paraqitje profesionale e të jetë i/e orientuar drejt shërbimit të klientit; • I/e gatshëm për të punuar me turne; • Ambicioz/e, i/e besueshëm dhe i aftë për të ndërmarrë iniciativa; • Aftësi për të punuar individualisht si dhe në grup; • Aftësi për të trajnuar punonjësit e rinj rreth punës aktuale. Shënime Aplikoni për të qenë pjesë e Skuadrës së LUFRA shpk, duke dërguar CV-në tuaj së bashku me një përgjigje të argumentuar të pyetjes: “PSE DUHET TË JENI JU MANAXHERI I RI I SHITJEVE në LUFRA shpk?” në adresën e e-mail-it hr@lufra.al . Vetëm kandidatët e përzgjedhur për intervistë do të kontaktohen. LUFRA ju siguron që aplikimi juaj do të trajtohet me konfidencialitet të plotë dhe të dhënat tuaja janë të sigurta. Kontakt: 0696069800 ';
		$pattern = '/[0-9]{3}[\-][0-9]{7}|[0-9]{3}[\s][0-9]{6}|[0-9]{3}[\s][0-9]{3}[\s][0-9]{4}|[0-9]{10}|[0-9]{9}|[0-9]{3}[\-][0-9]{3}[\-][0-9]{4}/';
		preg_match_all($pattern, $text, $matches);
		$phone = $matches[0][0];
		if ($phone != '') {
			return $phone;
		} else {
			return null;
		}
	}

	//converts number greater than 1000 to k format . example : 5410 -> 5.4k
	public static function numberFormatToK($value)
	{
		if ($value < 1000) {
			return $value;
		}
		if ($value >= 1000) {
			if (($value % 1000) === 0) {
				$value = number_format(($value / 1000), 0).'k';
			} else {
				$value = number_format(($value / 1000), 1).'k';
			}
			return $value;
		}
	}

	public static function googleMapsJsLink()
	{
		$link = 'https://maps.googleapis.com/maps/api/js?key='.CMSSettings::$google_maps_api.'&callback=initMap';
		return $link;
	}

	public static function price_format($value, $curr = 'EUR', $show_symbol = true, $symbol_before = true)
	{
		switch ($curr) {
			case 'USD':
				$symbol = '$';
				$decimals = 2;
				$dec_point = '.';
				$thousands_sep = ',';
				break;
			case 'GBP':
				$symbol = '£';
				$decimals = 2;
				$dec_point = ',';
				$thousands_sep = '.';
				break;
			case 'ALL':
				$symbol = 'Lekë';
				$decimals = 0;
				$dec_point = ',';
				$thousands_sep = '.';
				break;
			default:
				$symbol = '€';
				$decimals = 2;
				$dec_point = ',';
				$thousands_sep = '.';
				break;
		}

		$price = number_format($value, $decimals, $dec_point, $thousands_sep);

		if ($show_symbol) {
			if ($symbol_before) {
				$price = $symbol.' '.$price;
			} else {
				$price = $price.' '.$symbol;
			}
		}

		return $price;
	}

	public static function xss_clean($data)
	{
		// Fix &entity\n;
		$data = str_replace(array('&amp;', '&lt;', '&gt;'), array('&amp;amp;', '&amp;lt;', '&amp;gt;'), $data);
		$data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
		$data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
		$data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

		// Remove any attribute starting with "on" or xmlns
		$data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

		// Remove javascript: and vbscript: protocols
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

		// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

		// Remove namespaced elements (we do not need them)
		$data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

		do {
			// Remove really unwanted tags
			$old_data = $data;
			$data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
		} while ($old_data !== $data);

		// we are done...
		return $data;
	}

	public static function numbertoweekdate($num, $lang = 'it')
	{
		if ($lang == 'it') {
			switch ($num) {
				case '0':
					$day = 'Domenica';
					break;
				case '1':
					$day = 'Lunedì';
					break;
				case '2':
					$day = 'Martedì';
					break;
				case '3':
					$day = 'Mercoledì';
					break;
				case '4':
					$day = 'Giovedì';
					break;
				case '5':
					$day = 'Venerdì';
					break;
				case '6':
					$day = 'Sabato';
					break;
			}
			return $day;
		}

		if ($lang == 'en') {
			switch ($num) {
				case '0':
					$day = 'Sunday';
					break;
				case '1':
					$day = 'Monday';
					break;
				case '2':
					$day = 'Tuesday';
					break;
				case '3':
					$day = 'Wednesday';
					break;
				case '4':
					$day = 'Thursday';
					break;
				case '5':
					$day = 'Friday';
					break;
				case '6':
					$day = 'Saturday';
					break;
			}
			return $day;
		}

		if ($lang == 'al') {
			switch ($num) {
				case '0':
					$day = 'E Diel';
					break;
				case '1':
					$day = 'E Hene';
					break;
				case '2':
					$day = 'E Marte';
					break;
				case '3':
					$day = 'E Merkure';
					break;
				case '4':
					$day = 'E Enjte';
					break;
				case '5':
					$day = 'E Premte';
					break;
				case '6':
					$day = 'E Shtune';
					break;
			}
			return $day;
		}
	}

	public static function isActivePage($target)
	{
		$current_Url = Utils::curPageURL(false);
		if ($current_Url == self::removeSpecialChars($target)) {
			return true;
		} else {
			return false;
		}
	}

	public static function MenuPrintActive($target, &$isActive = null)
	{
		$current_Url = Utils::curPageURL();
		$target = self::removeSpecialChars($target);

		if ($current_Url == $target) {
			$isActive = true;
			return 'class="active"';
		} else {
			$isActive = false;
			return '';
		}
	}

	private static function removeSpecialChars($string)
	{
		if (strpos($string, '#') !== false) {
			$string = substr($string, 0, strpos($string, '#'));
		}

		if (substr($string, -1) == '/') {
			$string = rtrim($string, '/');
		}
		return $string;
	}

	public static function in_ObjectArray($objArr, $member, $needle)
	{
		if (!is_array($objArr)) {
			echo 'First parameters must be an Array of Objects!';
			return false;
		}
		foreach ($objArr as $obj) {
			if (!is_object($obj)) {
				echo 'Element of array not is a valid Object!';
				return false;
			}
			if (!property_exists(get_class($obj), $member)) {
				echo "Member $member not exists in Object!";
				return false;
			}
			if ($needle == $obj->$member) {
				return $obj;
			}
		}
		return false;
	}
}
