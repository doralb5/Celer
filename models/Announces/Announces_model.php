<?php

require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessAnnounces.php';

class Announces_Model extends BaseModel
{
	private $lastCounter;
	public static $lastAnnouncesResult;

	public function __construct()
	{
		parent::__construct();

		$this->ComponentSettings = BaseComponent::getComponentSettings('Announces', 'Announces');
		if (isset($this->ComponentSettings['db_host'], $this->ComponentSettings['db_name'], $this->ComponentSettings['db_user'], $this->ComponentSettings['db_pass'])
				 
				) {
			$this->setDatabase(new Database('mysql', $this->ComponentSettings['db_host'], $this->ComponentSettings['db_name'], $this->ComponentSettings['db_user'], $this->ComponentSettings['db_pass']));
		}
	}

	private static function setAnnouncesArray($array)
	{
		self::$lastAnnouncesResult = $array;
	}

	public static function getAnnouncesArray()
	{
		return self::$lastAnnouncesResult;
	}

	public function insertAnnounce($user_id, $name, $title, $email, $tel, $category_id, $expiration_date, $draft)
	{
		require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessAnnounces.php';
		$ann = new BusinessAnnounces_Entity();
		$ann->user_id = $user_id;
		$ann->name = $name;
		$ann->email = $email;
		$ann->tel = $tel;
		$ann->category_id = $category_id;
		$ann->expiration_date = $expiration_date;
		$ann->draft = $draft;
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessAnnounces');
		$res = $this->em->persist($ann);
		return $res;
	}

	public function getAnnounces($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessAnnounces');

		if ($order == '') {
			$order = 'publish_date DESC';
		}

		$Announces_Table = TABLE_PREFIX . BusinessAnnounces_Entity::TABLE_NAME;

		if (isset($this->ComponentSettings['location'])) {
			if ($filter != '') {
				$filter .= ' AND ';
			}

			$filter .= ' ' . $Announces_Table . ".location = '" . $this->ComponentSettings['location'] . "' ";
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Announces_Table
                      WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessAnnounces');

		if ($order == '') {
			$order = 'publish_date DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getAnnounce($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessAnnounces');
		$this->em->loadById($id);
		$announce = $this->em->getEntities();
		if (count($announce)) {
			$announce[0]->images = $this->getAnnounceImages(100, 0, "id_announce = {$announce[0]->id}");
		}
		return (count($announce)) ? $announce[0] : null;
	}

	public function getCategory($id)
	{
		//require_once DOCROOT . ENTITIES_PATH . 'Business/AnnouncesCategory.php';
		$this->em->setRepository('Business');
		$this->em->setEntity('AnnouncesCategory');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function getCategories($limit = 100, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('AnnouncesCategory');
		if ($order == '') {
			$order = 'category ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$AnnouncesCategory_Table = TABLE_PREFIX . AnnouncesCategory_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $AnnouncesCategory_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function saveAnnounce($ann)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessAnnounces');
		$res = $this->em->persist($ann);
		return $res;
	}

	public function deleteAnnounce($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessAnnounces');
		$res = $this->em->delete('BusinessAnnounces', $id);
		return $res;
	}

	////////////////////// kto posht jan te erides

	public function getAnnounceImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('AnnouncesImages');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function saveAnnounceImage($evImage)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('AnnouncesImages');
		$res = $this->em->persist($evImage);
		return $res;
	}

	public function getAnnounceImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('AnnouncesImages');
		$this->em->loadById($id);
		$evImage = $this->em->getEntities();
		return (count($evImage)) ? $evImage[0] : null;
	}

	public function deleteAnnounceImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('AnnouncesImages');
		$res = $this->em->delete('AnnouncesImages', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Bususiness');
		$this->em->setEntity('BusinessAnnounces');

		require_once DOCROOT . LIBS_PATH . 'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = BusinessAnnounces_Entity::getSelectQueryObj($this->db);
		if ($sorting != '') {
			$q->orderBy($sorting);
		}
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		$query->limit($limit);
		$query->offset($offset);
		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		self::setAnnouncesArray($res);
		return $res;
	}

	public function saveTempImage($temp_image)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		$res = $this->em->persist($temp_image);
		return $res;
	}

	public function getTempImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		$this->em->loadById($id);
		$Image = $this->em->getEntities();
		return (count($Image)) ? $Image[0] : null;
	}

	public function deleteTempImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		$res = $this->em->delete('TempImage', $id);
		return $res;
	}

	public function getTempImages($limit = 999, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		if ($order == '') {
			$order = 'id';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}
}
