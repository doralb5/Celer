<?php

require_once DOCROOT . ENTITIES_PATH . 'Article/Article.php';

class Articles_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '', $get_comments = false)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('Article');

		if ($order == '') {
			$order = 'publish_date DESC';
		}

		if ($filter != '') {
			$filter = "publish_date < NOW() AND $filter";
		} else {
			$filter = 'publish_date < NOW()';
		}

		$Article_Table = TABLE_PREFIX . Article_Entity::TABLE_NAME;
		//        $ArticleDetail_Table = TABLE_PREFIX . ArticleDetail_Entity::TABLE_NAME;
		//        $Section_Table = TABLE_PREFIX . ArticleSection_Entity::TABLE_NAME;
		//        $Categ_Table = TABLE_PREFIX . ArticleCategory_Entity::TABLE_NAME;

		$filter .= " AND $Article_Table.enabled='1'";
		$this->em->loadByFilter($filter, $limit, $offset, $order);

		$result = $this->em->getEntities();

		$this->lastCounter = $this->em->countByFilter($filter);

		//        $tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Article_Table
		//                                        LEFT JOIN $ArticleDetail_Table ON ($ArticleDetail_Table.id_article = $Article_Table.id)
		//                                        LEFT JOIN $Categ_Table categ ON(categ.id = $Article_Table.id_category)
		//                                        LEFT JOIN $Section_Table sec ON(sec.id = $Article_Table.id_section)
		//                                        WHERE enabled='1' " . (($filter != '') ? "AND $filter" : ""));
		//        $this->lastCounter = $tot_count[0]['tot'];

		if ($get_comments) {
		}

		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getArticle($id, $lang = null)
	{
		$Article_Table = TABLE_PREFIX . 'Article';
		$ArticleDetail_Table = TABLE_PREFIX . 'ArticleDetail';

		$this->em->setRepository('Article');
		$this->em->setEntity('Article');
		//$this->em->loadById($id);
		$filter = "{$Article_Table}.id = $id AND publish_date < NOW() ";
		if (!is_null($lang)) {
			$filter .= " AND {$ArticleDetail_Table}.lang = '$lang'";
		}
		$this->em->loadByFilter($filter, 1);
		$Article = $this->em->getEntities();
		if (count($Article)) {
			$art = $Article[0];

			$this->em->cleanEntities();
			$this->em->setRepository('Article');
			$this->em->setEntity('ArticleComment');
			$this->em->loadByFilter("article_id = '$art->id'", '', '', 'creation_date ASC');
			$unordered_comments = $this->em->getEntities();
			$comments = array();

			foreach ($unordered_comments as $comm) {
				if ($comm->comment_id == '') {
					array_push($comments, $comm);
				}
			}

			foreach ($comments as &$comm) {
				$comm->replies = array();
				foreach ($unordered_comments as $reply) {
					if ($reply->comment_id == $comm->id) {
						array_push($comm->replies, $reply);
					}
				}
			}

			$art->comments = $comments;
			return $art;
		}
		return null;
	}

	public function saveArticle($Article)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('Article');
		$res = $this->em->persist($Article);
		return $res;
	}

	public function deleteArticle($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('Article');
		$res = $this->em->delete('Article', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('Article');

		require_once DOCROOT . LIBS_PATH . 'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter . ' AND ' . TABLE_PREFIX . "Article.enabled='1'");
		} else {
			$se->where(TABLE_PREFIX . "Article.enabled='1'");
		}

		$q = Article_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleCategory');

		if ($order == '') {
			$order = 'parent_id DESC, id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$ArticleCategory_Table = TABLE_PREFIX . ArticleCategory_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $ArticleCategory_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));

		$this->lastCounter = $tot_count[0]['tot'];

		//Marrim dhe numrin e kategorive
		$Article_Table = TABLE_PREFIX . Article_Entity::TABLE_NAME;
		foreach ($result as &$categ) {
			$tot_ser = $this->db->select("SELECT COUNT(*) AS total FROM $Article_Table WHERE id_category = :categ", array(':categ' => $categ->id));
			$categ->total_articles = $tot_ser[0]['total'];
			$this->hierarchyCategory($categ, $categ->category_full);

			$categ->url_key = Utils::url_slug($categ->category) . '-' . $categ->id;
			$categ->url = Utils::getComponentUrl('Articles/articles_list') . "/$categ->url_key";
		}

		return $result;
	}

	public function getSubCategoriesIds($arr_categories, $include_parent_id = false)
	{
		$arr_ids = array();
		if (!is_array($arr_categories)) {
			$arr_categories = array($arr_categories);
		}
		foreach ($arr_categories as $catid) {
			if ($include_parent_id) {
				array_push($arr_ids, $catid);
			}
			$arr_ids = array_merge($arr_ids, $this->recursiveCategsId($catid));
		}
		return $arr_ids;
	}

	private function recursiveCategsId($idcat, &$arrayIds = array())
	{
		$subcategories = $this->getCategories(-1, 0, "parent_id = '$idcat'");
		foreach ($subcategories as $subcat) {
			array_push($arrayIds, $subcat->id);
			$this->recursiveCategsId($subcat->id, $arrayIds);
		}
		return $arrayIds;
	}

	public function getCategory($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleCategory');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		if (count($category)) {
			$category[0]->url_key = Utils::url_slug($category[0]->category) . '-' . $category[0]->id;
			$category[0]->url = Utils::getComponentUrl('Articles/articles_list') . '/' . $category[0]->url_key;
			return $category[0];
		}
		return null;
	}

	public function saveCategory($category)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleCategory');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleCategory');
		$res = $this->em->delete('ArticleCategory', $id);
		return $res;
	}

	public function getSections($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleSection');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$ArticleSection_Table = TABLE_PREFIX . ArticleSection_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $ArticleSection_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getSection($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleSection');
		$this->em->loadById($id);
		$section = $this->em->getEntities();
		return (count($section)) ? $section[0] : null;
	}

	public function saveSection($section)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleSection');
		$res = $this->em->persist($section);
		return $res;
	}

	public function deleteSection($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleSection');
		$res = $this->em->delete('ArticleSection', $id);
		return $res;
	}

	public function getRoles($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleRole');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$ArticleRole_Table = TABLE_PREFIX . ArticleRole_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $ArticleRole_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];

		return $result;
	}

	public function getRole($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleRole');
		$this->em->loadById($id);
		$role = $this->em->getEntities();
		return (count($role)) ? $role[0] : null;
	}

	public function saveRole($role)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleRole');
		$res = $this->em->persist($role);
		return $res;
	}

	public function deleteRole($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleRole');
		$res = $this->em->delete('ArticleRole', $id);
		return $res;
	}

	public function getArticleImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleImage');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$ArticleImage_Table = TABLE_PREFIX . ArticleImage_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $ArticleImage_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getArticleImage($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleImage');
		$this->em->loadById($id);
		$arImage = $this->em->getEntities();
		return (count($arImage)) ? $arImage[0] : null;
	}

	public function saveArticleImage($arImage)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleImage');
		$res = $this->em->persist($arImage);
		return $res;
	}

	public function deleteArticleImage($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleImage');
		$res = $this->em->delete('ArticleImage', $id);
		return $res;
	}

	public function getArticleDetails($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleDetail');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getArticleDetail($id)
	{
		$this->em->setRepository('Article');
		$this->em->setEntity('ArticleDetail');
		$this->em->loadById($id);
		$detail = $this->em->getEntities();
		return (count($detail)) ? $detail[0] : null;
	}

	public function getPopularTags($limit = 10)
	{
		$TTag = TABLE_PREFIX . 'Tag';
		$TTagArticle = TABLE_PREFIX . 'TagArticle';
		$TagJoin = "$TTag.id = $TTagArticle.tag_id";
		$q = "SELECT DISTINCT($TTag.title), $TTag.id FROM $TTag INNER JOIN $TTagArticle ON $TagJoin ";
		$q .= " GROUP BY $TTag.id ";
		$q .= " ORDER BY COUNT($TTag.id) DESC ";
		$q .= " LIMIT $limit ";

		$tags = $this->db->select($q);
		foreach ($tags as &$tag) {
			$tag['url'] = Utils::getComponentUrl('Articles/articles_list') . '/?tags=' . $tag['id'];
		}
		return $tags;
	}

	private function hierarchyCategory($category, &$category_name)
	{
		if (!is_null($category->parent_id) && $category->parent_id != 0) {
			$categ = $this->getCategory($category->parent_id);
			$category_name = $categ->category . ' <b>&raquo</b> ' . $category_name;
			$this->hierarchyCategory($categ, $category_name);
		} else {
			return;
		}
	}
}
