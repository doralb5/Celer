<?php

require_once BS_OBJECTS_PATH . 'O_cart.php';
require_once BS_OBJECTS_PATH . 'O_order.php';
require_once BS_OBJECTS_PATH . 'O_order_details.php';
require_once BS_OBJECTS_PATH . 'O_payment_method.php';
require_once BS_OBJECTS_PATH . 'O_shipment_method.php';
require_once BS_OBJECTS_PATH . 'O_citta.php';
require_once BS_OBJECTS_PATH . 'O_nazioni.php';
require_once BS_OBJECTS_PATH . 'O_province.php';
require_once BS_OBJECTS_PATH . 'O_impostazioni.php';

//require_once CLASSES_PATH . 'BH/BS_session.php';

class Orders_Model extends Model
{
	private $em;

	public function __construct()
	{
		parent::__construct();
		$this->em = ORM::getInstance();
		$this->em->setDefault_lang('_it');
		$this->em->setUser_lang('_it');
		$this->em->setNlistino(BS_session::get_listino());
	}

	/**
	 * Ritorna il carrello in base all'id_cliente o all'id_sessione
	 * @param int $id_cliente
	 * @param int $id_sessione
	 * @return O_cart
	 */
	public function getCart($id_cliente, $id_sessione, $type = 'cart')
	{
		$this->em->cleanAll();
		if ($id_cliente != '') {
			$this->em->setWhere(" :id_cliente='$id_cliente' AND numero IS NULL");
		} elseif ($id_cliente == '' && $id_sessione != '') {
			$this->em->setWhere(" :id_sessione='$id_sessione' AND :id_cliente IS NULL AND numero IS NULL");
		}

		//  $this->em->setGroup_by("do.id");
		$this->em->setLimit(1);
		if ($type == 'cart') {
			$this->em->setRepository('O_cart');
			$cart = $this->em->find();

			if (!$cart || is_null($cart[0]->id)) {
				return null;
			}
			return $cart[0];
		} elseif ($type == 'order') {
			$this->em->setRepository('O_order');
			$cart = $this->em->find();
			if (!$cart) {
				return false;
			}
			return $cart[0];
		}
		return false;
	}

	public function getCartbyId($id_cart, $type = 'cart')
	{
		$this->em->cleanAll();
		$this->em->setWhere(":id = '{$id_cart}'");
		$this->em->setLimit(1);
		// $this->em->setGroup_by("do.id");
		$this->em->setLimit(1);
		if ($type == 'cart') {
			$this->em->setRepository('O_cart');
			$cart = $this->em->find();
			if (!$cart) {
				return false;
			}
			return $cart[0];
		} elseif ($type == 'order') {
			$this->em->setRepository('O_order');
			$cart = $this->em->find();
			if (!$cart) {
				return false;
			}

			return $cart[0];
		}
		return false;
	}

	/**
	 * Ritorna un array di oggetti order
	 * @param int $id_cliente
	 * @param int $stato_ordini ['0'=>Ordine in carrello, '1'=>ordini ancora aperti, '2'=>ordini evasi, '3'=> tutti gli ordini tranne il carrello] default=>'3'
	 * @return O_order
	 */
	public function getOrders($id_cliente, $stato_ordini = 3, $limit = '')
	{
		$where = " :id_cliente='$id_cliente'";
		switch ($stato_ordini) {
			case '0': $where .= ' AND numero IS NULL '; // Ordini in carrello
				break;
			case '1': $where .= ' AND chiuso=0 AND numero IS NOT NULL '; //ordini ancora aperti
				break;
			case '2': $where .= ' AND chiuso=1 AND numero IS NOT NULL '; //ordini evasi
				break;
			case '3': $where .= ' AND numero IS NOT NULL '; //tutti gli ordini tranne il carrello
				break;
		}
		$this->em->cleanAll();
		$this->em->setWhere($where);
		$this->em->setOrder_by(' :data DESC , :id DESC');
		if ($limit != '') {
			$this->em->setLimit($limit);
		}
		$this->em->setRepository('O_order');
		$orders = $this->em->find();
		return $orders;
	}

	public function getOrderInfoByNum($num)
	{
		$this->em->cleanAll();
		$this->em->setWhere("numero = '$num'");
		$this->em->setLimit(1);
		$this->em->setRepository('O_order');
		$order = $this->em->find();
		if (!$order) {
			return false;
		}
		return $order[0];
	}

	/**
	 * Ritorna l'oggetto Order
	 * @param int $id_cliente
	 * @param int $id_ordine
	 * @return O_order
	 */
	public function getOrderInfo($id_cliente, $id_ordine)
	{
		$this->em->cleanAll();
		if (!is_null($id_cliente)) {
			$where = " :id_cliente='$id_cliente'";
		} else {
			$where = ' 1';
		}
		if ($id_ordine == '') {
			$where .= ' AND numero IS NULL'; //ordini in carrello
		} else {
			$where .= " AND :id='$id_ordine'";
		}
		$this->em->setWhere($where);
		$this->em->setLimit(1);
		$this->em->setRepository('O_order');
		$order = $this->em->find();
		if (!$order) {
			return false;
		}
		return $order[0];
	}

	public function createCart($userid = null)
	{
		$cart = new O_order();

		$this->em->cleanAll();
		$query_qiva = 'SELECT iva_predefinita FROM impostazioni LIMIT 1';
		$res_qiva = $this->em->findByQuery($query_qiva);
		$iva = $res_qiva[0]['iva_predefinita'];

		if ($userid == null && isset($_SESSION['user_id'])) {
			$userid = $_SESSION['user_id'];
		}

		if ($userid != null) { // Se viene passato l'id utente
			$user_model = Loader::loadModel('Users', true, 'BlueShop');
			// @var $user O_user
			$user = $user_model->getUser($userid);

			//$nazUser = $user->nazione;
			//$nazInfo = $user_model->getNazioneInfo($nazUser);
			if ($user->UE != '1' || ($user->UE == '1' && $user->nazione != 'Italia' && $user->business == '1')) {
				$iva = 0;
			}

			if ($user != false) { // controllo un pò eccessivo (se è loggato vuol dire che esiste nel db), creo carrelo con dati utenti
				$cart->data_creazione = date('Y-m-d');
				$cart->id_stato = 1;
				$cart->id_cliente = $user->id;
				$cart->id_sessione = session_id();
				$cart->codice_cl = $user->codice;
				$cart->cognome = $user->cognome;
				$cart->nome = $user->nome;
				$cart->codice_fiscale_cl = $user->codice_fiscale;
				$cart->ragione_sociale = $user->ragione_sociale;
				$cart->partita_iva_cl = $user->partita_iva;
				$cart->indirizzo_cl = $user->indirizzo;
				$cart->provincia_cl = $user->provincia;
				$cart->comune_cl = $user->comune;
				$cart->cap_cl = $user->cap;
				$cart->nazione_cl = $user->nazione;
				$cart->nazione_dest = $user->nazione;
				$cart->aliquota_iva = $iva;
				$cart->id_metodo_spedizione = '';
				$cart->spese_spedizione = 0;
				$cart->indirizzo_dest = '';
				$cart->cap_dest = '';
				$cart->tracking_number = '';
				$cart->comune_dest = '';
				$cart->id_comune_dest = '';
				$cart->pagato = 0;
			} else { // casomai non dovesse esistere l'utente nel db creo il carrelo con l'id di sessione
				$cart->data_creazione = date('Y-m-d');
				$cart->id_stato = 1;
				$cart->id_sessione = session_id();
				$cart->aliquota_iva = $iva;
				$cart->codice_cl = '';
				$cart->id_metodo_spedizione = '';
				$cart->spese_spedizione = 0;
				$cart->ragione_sociale = '';
				$cart->partita_iva_cl = '';
				$cart->indirizzo_dest = '';
				$cart->cap_dest = '';
				$cart->tracking_number = '';
				$cart->comune_dest = '';
				$cart->id_comune_dest = '';
				$cart->pagato = 0;
			}
		} else {// utente non loggato creo carrello con id di sessione
			$cart->data_creazione = date('Y-m-d');
			$cart->id_stato = 1;
			$cart->id_sessione = session_id();
			$cart->aliquota_iva = $iva;
			$cart->codice_cl = '';
			$cart->id_metodo_spedizione = '';
			$cart->spese_spedizione = 0;
			$cart->ragione_sociale = '';
			$cart->partita_iva_cl = '';
			$cart->indirizzo_dest = '';
			$cart->cap_dest = '';
			$cart->tracking_number = '';
			$cart->comune_dest = '';
			$cart->id_comune_dest = '';
			$cart->pagato = 0;
		}

		$cart->setNew();

		$id_cart = $this->em->save($cart);
		if (!$id_cart) {
			return false;
		}
		return $id_cart;
	}

	public function sendOrder($id_order)
	{
		$query = "UPDATE ordini SET data_creazione=NOW(),
            numero = IFNULL(  (   (SELECT x.max_field  FROM (SELECT MAX(t.numero) + 1 AS max_field  FROM ordini AS t )x)),1)
            WHERE id='" . $id_order . "'";
		$res = $this->db->execute($query);
		return true;
	}

	public function payOrder($id_order)
	{
		$query = "UPDATE ordini SET pagato = 1
        WHERE id='" . $id_order . "'";
		$res = $this->em->findByQuery($query);
	}

	public function mergeCart($user, $sessionId)
	{

		// @var $user O_user

		$CartOfUser = $this->getCart($user->id, $sessionId, 'cart');
		$CartInSession = $this->getCart('', $sessionId, 'cart');
		if (is_null($CartInSession)) {
			$this->updateDiscount($CartOfUser);
			return $CartOfUser;
		}

		if (is_null($CartOfUser)) {
			$id_cart = $this->createCart($user->id);
			$CartOfUser = $this->getCart($user->id, '', 'cart');
		}

		$CartInSession->getDetails();
		$CartOfUser->getDetails();

		$user_model = Loader::loadModel('Users', true, 'BlueShop');

		// @var $detail O_order_details

		if ($CartInSession->details && count($CartInSession->details) > 0) { // Se sono presenti dettagli nel carrello di sessione
			$prod_model = Loader::loadModel('Products', true, 'BlueShop');

			foreach ($CartInSession->details as $detail) {

					/** Per soggetti extraUE annullo l'IVA * */
				//$nazUser = $user->nazione;
				//$nazInfo = $user_model->getNazioneInfo($nazUser);
				if ($user->UE != '1' || ($user->UE == '1' && $user->nazione != 'Italia' && $user->business == '1')) {
					$detail->iva_ar = 0;
				}

				//TODO: Aggiornare prezzi in base al listino dell'utente

				// @var $prod_model Products_model
				// @var $prod O_product
				$prod = $prod_model->getProduct($detail->id_prodotto);
				if ($prod->serie == '0') {
					$detail->prezzo = $prod->prezzo_vendita_raw;
				} else {	//se è una variante
					$var = $prod->geVariantById($detail->id_variante);
					$detail->prezzo = $var[0]->prezzo_vendita_raw;
				}

				$alreadyInCart = false;
				if ($CartOfUser->details !== false) {
					for ($jj = 0; $jj < count($CartOfUser->details); $jj++) {
						$det = $CartOfUser->details[$jj];
						if ($det->id_prodotto == $detail->id_prodotto && ($det->id_variante == $detail->id_variante)) {
							$tot_qty = $det->quantita + $detail->quantita;
							$CartOfUser->details[$jj]->quantita = $tot_qty;
							$CartOfUser->details[$jj]->iva_ar = $detail->iva_ar;
							$CartOfUser->details[$jj]->prezzo = $detail->prezzo;
							$this->updateCartDetail($CartOfUser->details[$jj]);
							$res = true;
							$alreadyInCart = true;
							break;
						}
					}
				}

				if (!$alreadyInCart) {
					$res = $CartOfUser->addDetail($detail);
				}

				if (!$res) {
					throw new Exception("Errore durante l'aggiornamento del carrello");
				}
			}

			if (!$CartOfUser->update()) {
				throw new Exception('Error: update cart_user');
			}
		}
		$this->em->cleanAll();
		if (!$this->em->delete($CartInSession)) {
			throw new Exception('Error: delete cart_session');
		}
		
		$this->updateDiscount($CartOfUser);
		return $CartOfUser;
	}

	/**
	 *
	 * ATTENZIONE !!!!!!! ********************************************************************** METODO OBSOLETO..... SOSTITUITO DA mergeCart();
	 *
	 * Controlla l'esistenza in un carrelo in caso esiste unisce i dettagli
	 * @param O_user $user
	 */
	public function checkCart($user, $id_sessione)
	{

		// @var $cart_session O_order
		$cart_session = $this->getCart('', $id_sessione, 'cart'); //Recupero carrello con la sessione

		// @var $cart_user O_order
		$order = $this->getCart($user->id, '', 'order');

		$user_model = Loader::loadModel('Users', true, 'BlueShop');

		//
		// Se non è stato creato nessuno carrello con quella sessione termina la procedura
		if (is_null($cart_session->id)) {
			return true;
		}
		// Se invece esiste un carrello con quella sessione
		 
		// Se non esiste un carrello per il cliente
		if (!$order) {
			//                $cart_session->id_cliente = $user->id;
			//                $this->em->save($cart_session);
			//                $order = $cart_session;
//
			//                var_dump($order);exit;

			$id_order = $this->createCart();

			if (!$id_order) {
				return false;
			}
			$order = $this->getCartbyId($id_order);
		}

		/** Per soggetti extraUE annullo l'IVA * */
		//$nazUser = $user->nazione;
		//$nazInfo = $user_model->getNazioneInfo($nazUser);
		if ($user->UE != '1' || ($user->UE == '1' && $user->nazione != 'Italia' && $user->business == '1')) {
			$order->aliquota_iva = 0;

			$q = "UPDATE ordini SET aliquota_iva = '0' WHERE id = '" . $order->id . "'";
			$this->db->execute($q);

			$q = "UPDATE dettagli_ordine SET iva_ar = '0' WHERE id_ordine = '" . $order->id . "'";
			$this->db->execute($q);

			$order->update();
		}

		//L'ordine esiste ed è salvato su DB
		//Aggiungo i prodotti del carrello di sessione al carrello utente ed elimino il carrelo di sessione
		$cart_session->getDetails();

		// @var $detail O_order_details

		if ($cart_session->details && count($cart_session->details) > 0) { // Se sono presenti dettagli nel carrello di sessione
			foreach ($cart_session->details as $detail) {

					/** Per soggetti extraUE annullo l'IVA * */
				//$nazUser = $user->nazione;
				//$nazInfo = $user_model->getNazioneInfo($nazUser);
				if ($user->UE != '1' || ($user->UE == '1' && $user->nazione != 'Italia' && $user->business == '1')) {
					$detail->iva_ar = 0;
				}

				$res = $order->addDetail($detail);

				if (!$res) {
					throw new Exception("Errore durante l'aggiornamento del carrello");
				}
			}

			if (!$order->update()) {
				throw new Exception('Error: update cart_user');
			}
		}
		$this->em->cleanAll();
		if (!$this->em->delete($cart_session)) {
			throw new Exception('Error: delete cart_session');
		}
		return true;
	}

	public function getDetail($id_detail)
	{
		$this->em->cleanAll();
		$this->em->setWhere(" :id='{$id_detail}'");
		$this->em->setLimit(1);
		$this->em->setRepository('O_order_details');
		$detail = $this->em->find();
		if (!$detail) {
			return false;
		}
		return $detail[0];
	}

	public function setPaymentMethod($idOrder, $idPayment, $prezzo)
	{
		$this->em->cleanAll();
		$query = "UPDATE ordini SET id_modalita_pagamento = '" . $idPayment . "', costo_aggiuntivo = '" . $prezzo . "' WHERE id='" . $idOrder . "'";
		$res = $this->db->execute($query);

		return true;
	}

	public function getComuni($sigla_prov = null)
	{
		$this->em->cleanAll();
		if (isset($sigla_prov)) {
			$this->em->setWhere(":sigla_prov='{$sigla_prov}'");
		}
		$this->em->setRepository('O_citta');
		$this->em->setOrder_by(' :comune');
		$comuni = $this->em->find();
		return $comuni;
	}

	public function getProvince()
	{
		$this->em->cleanAll();
		$this->em->setRepository('O_province');

		$this->em->setOrder_by(' :provincia');
		$province = $this->em->find();
		return $province;
	}

	public function getValuteCode($simbolo)
	{
		$this->em->cleanAll();

		$this->em->setWhere(":simbolo='" . $simbolo . "'");
		$this->em->setRepository('O_valute');

		$valuta = $this->em->find();
		return $valuta[0]->codice;
	}

	public function getNazioni()
	{
		$this->em->cleanAll();

		$this->em->setWhere(":attiva='1'");
		$this->em->setRepository('O_nazioni');
		$this->em->setOrder_by(' :stato');
		$nazioni = $this->em->find();
		return $nazioni;
	}

	public function getImpostazioni()
	{
		$this->em->cleanAll();
		$this->em->setRepository('O_impostazioni');
		$sito = $this->em->find();

		return $sito[0];
	}

	public function updateCartDetail($det)
	{
		$det->setNew();
		return $this->em->save($det);
	}

	public function getDiscount($orderId)
	{
		try {
			$q = 'SELECT discount FROM ordini WHERE id = :id';
			$res = $this->db->select($q, array(':id' => $orderId));
			return $res[0]['discount'];
		} catch (Exception $ex) {
			return 0;
		}
	}

	public function insertCoupon($orderId, $coupon, $val)
	{
		$q = 'UPDATE ordini SET coupon_code = :coupon, discount = :value WHERE id = :id';
		$res = $this->db->execute($q, array(':value' => $val, ':coupon' => $coupon, ':id' => $orderId));

		return $res;
	}

	public function removeCoupon($orderId)
	{
		$q = "UPDATE ordini SET coupon_code = '', discount = '' WHERE id = :id";
		$res = $this->db->execute($q, array(':id' => $orderId));

		return $res;
	}

	public function getCouponByCode($code)
	{
		$q = "SELECT * FROM coupons WHERE coupon_code = :code AND deleted != '1'";
		$res = $this->db->select($q, array(':code' => $code));
		if (count($res)) {
			return $res[0];
		}
		
		return false;
	}

	public function getCountCouponUsed($code, $uid = '')
	{
		$q = 'SELECT COUNT(coupon_code) AS countcoupon FROM ordini WHERE numero IS NOT NULL AND coupon_code = :code ';
		if ($uid != '') {
			$q .= " AND id_cliente = '$uid'";
		}

		$res = $this->db->select($q, array(':code' => $code));

		if (count($res)) {
			return intval($res[0]['countcoupon']);
		}
		
		return 0;
	}

	public function getCouponDiscountVal($code, $val)
	{
		$coupon = $this->getCouponByCode($code);

		$val = floatval($val);

		if ($val >= floatval($coupon['order_min']) || $coupon['order_min'] == 0) {
			if (floatval($coupon['discount_fixed']) > 0) {
				$discount = $coupon['discount_fixed'];
			} else {
				$discountPercVal = $val * floatval($coupon['discount_perc']) / 100;
				if ($discountPercVal <= floatval($coupon['discount_max'])) {
					$discount = $discountPercVal;
				} else {
					$discount = $coupon['discount_max'];
				}
			}

			if (floatval($discount) <= floatval($val)) {
				return $discount;
			}
			return $val;
		}
		return 0;
	}

	public function checkCoupon($code, $uid = '')
	{
		$coupon = $this->getCouponByCode($code);
		if ($coupon !== false) {
			$enddate = strtotime($coupon['enddate']);
			if ($enddate >= strtotime('now')) {
				if (intval($coupon['qty_available']) == 0 || (intval($coupon['qty_available']) > $this->getCountCouponUsed($code, $uid))) {
					return true;
				}
			}
		}

		return false;
	}

	public function updateDiscount($cart)
	{
		if (is_null($cart)) {
			return;
		}

		$coupon = $cart->coupon_code;

		if ($this->checkCoupon($coupon, $cart->id_cliente)) {
			$val = $this->getCouponDiscountVal($coupon, $cart->totale + $cart->discount);
			$q = "UPDATE ordini SET discount = '$val' WHERE id = '$cart->id'";
			$this->db->execute($q);
			$cart = $this->getCartbyId($cart->id);
		} else {
			$this->removeCoupon($cart->id);
		}
	}

	public function getMinOrder($nListino = 1)
	{
		$this->em->cleanAll();
		$this->em->setWhere(" :id='{$nListino}'");
		$this->em->setLimit(1);
		$this->em->setRepository('O_listini');
		$detail = $this->em->find();
		if (!$detail) {
			return false;
		}
		return (float) $detail[0]->ordine_min;
	}
}
