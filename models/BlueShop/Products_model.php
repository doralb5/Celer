<?php

require_once DOCROOT . ENTITIES_PATH . 'BlueShop/Product.php';

class Products_Model extends BaseModel
{
	private $lastCounter;
	public static $lastProductResult;

	public function __construct()
	{
		parent::__construct();
		$db = new Database(DB_TYPE, 'mysql0.bluehat.it', 'c4088_cms0', 'desta_ro', 'afmetipass');
		$this->setDatabase($db);
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('BlueShop');
		$this->em->setEntity('Product');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$Product_Table = Product_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Product_Table
                      WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = (count($tot_count)) ? $tot_count[0]['tot'] : 0;

		return $result;
	}

	public function getProduct($id)
	{
		$this->em->cleanAll();
		$this->em->setWhere(":id = '$id'");
		$this->em->setLimit(1);
		$this->em->setRepository('O_product');
		// @var $products O_product
		$products = $this->em->find();
		if (is_array($products) && count($products) > 0) {

			//   $products[0]->getRelated_product();
			//   $products[0]->getVariant_product();
			//   $products[0]->variant_product[0]->getAttribute_variant();
			$products[0]->getImagesId();
			for ($i = 0; $i < count($products[0]->imgs); $i++) {
				$products[0]->imgs[$i]['url'] = $this->getImgUrl($products[0]->imgs[$i]['id']);
			}

			self::$current_prod = $products[0];
			self::$current_cat = $products[0]->id_categoria;

			return $products[0];
		}
		return null;

		return;

		$this->em->setRepository('BlueShop');
		$this->em->setEntity('Product');
		$this->em->setTablePrefix();
		$this->em->loadById($id);
		$Product = $this->em->getEntities();
		return (count($Product)) ? $Product[0] : null;
	}

	public function getProdCategories($filter = '', $order = '')
	{
		$this->em->setRepository('BlueShop');
		$this->em->setEntity('ProductCategory');
		$this->em->loadByFilter($filter, 500, 0, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getProductImage($id)
	{
		$this->em->setRepository('BlueShop');
		$this->em->setEntity('ProductImage');
		$this->em->setTablePrefix();
		$this->em->loadById($id);
		$img = $this->em->getEntities();
		return (count($img)) ? $img[0] : null;
	}
}
