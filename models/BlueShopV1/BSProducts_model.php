<?php

require_once BS_OBJECTS_PATH . 'O_product.php';
require_once BS_OBJECTS_PATH . 'O_brand.php';
require_once BS_OBJECTS_PATH . 'O_category.php';
require_once BS_OBJECTS_PATH . 'O_img_product.php';
require_once BS_OBJECTS_PATH . 'O_related_product.php';
require_once BS_OBJECTS_PATH . 'O_variant_product.php';
require_once BS_OBJECTS_PATH . 'O_attribute_variant.php';
require_once BS_OBJECTS_PATH . 'O_impostazioni.php';
require_once CLASSES_PATH . 'BlueShopV1/ORM.php';
require_once CLASSES_PATH . 'BlueShopV1/BS_Session.php';

class BSProducts_Model extends BaseModel
{
	private $attributi_varianti;
	private static $current_cat;
	private static $current_prod;
	private static $num_prod = -1;
	private static $limit = 10;
	private static $offset = 0;
	private $settings;

	public static function getCurrentCat()
	{
		return self::$current_cat;
	}

	public static function getLimit()
	{
		return self::$limit;
	}

	public static function getOffset()
	{
		return self::$offset;
	}

	public static function getCurrentProd()
	{
		return self::$current_prod;
	}

	public static function getNumProd()
	{
		return self::$num_prod;
	}

	public function __construct()
	{
		parent::__construct();

		$this->db = new Database(DB_TYPE, BS_DB_HOST, BS_DB_NAME, BS_DB_USER, BS_DB_PASS);

		$this->em = ORM::getInstance();
		$settings = $this->getImpostazioni();
		$this->settings = $settings;

		$this->em->setDefault_lang($settings->lang_default);
		//$this->em->setUser_lang('_'.Lang::getCurrent());
		$this->em->setUser_lang('_' . FCRequest::getLang());
		$this->em->setNlistino(BS_Session::get_listino());
	}

	public function getBlueShopSettings()
	{
		$this->em->cleanAll();
		$this->em->setRepository('O_impostazioni');
		$settings = $this->em->find();
		return $settings[0];
	}

	public function findVarianteByAttributes($array_attributes, $id_prodotto)
	{
		$attribute_names = array_keys($array_attributes);
		$query = 'SELECT varianti.id_varianteprodotto AS id_variante    
                FROM (';

		$query .= "SELECT  vattr . *
                        FROM varianti_attributi as vattr INNER JOIN varianti_prodotti AS vp ON vp.id = vattr.id_varianteprodotto
                        WHERE
                            vp.id_prodotto = '" . $id_prodotto . "'
                            AND id_attributo = '" . $attribute_names[0] . "'
                            AND valore = '" . $array_attributes[$attribute_names[0]] . "'";

		for ($i = 1; $i < count($attribute_names); $i++) {
			$query .= " UNION SELECT  vattr . *
                        FROM varianti_attributi as vattr  INNER JOIN varianti_prodotti AS vp ON vp.id = vattr.id_varianteprodotto
                        WHERE
                            vp.id_prodotto = '" . $id_prodotto . "'
                            AND id_attributo = '" . $attribute_names[$i] . "'
                            AND valore = '" . $array_attributes[$attribute_names[$i]] . "'";
		}

		$query .= ') AS varianti
        GROUP BY id_varianteprodotto
        HAVING count(varianti.id) = ' . count($array_attributes) . '
        LIMIT 1';
		$res = $this->db->select($query);
		if (count($res) > 0) {
			return $res[0]['id_variante'];
		}
		return null;
	}

	public function getListPrices()
	{
		$q = 'SELECT p.id, p.codice, :titolo, '
				. 'listino1, listino1_sc, '
				. 'listino2, listino2_sc, '
				. 'listino3, listino3_sc, '
				. 'listino4, listino4_sc, '
				. 'listino5, listino5_sc, '
				. 'listino6, listino6_sc, '
				. 'listino7, listino7_sc, '
				. 'listino8, listino8_sc, '
				. 'listino9, listino9_sc, '
				. 'listino10, listino10_sc, '
				. 'MIN(varp.l1) AS varl1, MIN(varp.l1_sc) AS varl1_sc, '
				. 'MIN(varp.l2) AS varl2, MIN(varp.l2_sc) AS varl2_sc, '
				. 'MIN(varp.l3) AS varl3, MIN(varp.l3_sc) AS varl3_sc, '
				. 'MIN(varp.l4) AS varl4, MIN(varp.l4_sc) AS varl4_sc, '
				. 'MIN(varp.l5) AS varl5, MIN(varp.l5_sc) AS varl5_sc, '
				. 'MIN(varp.l6) AS varl6, MIN(varp.l6_sc) AS varl6_sc, '
				. 'MIN(varp.l7) AS varl7, MIN(varp.l7_sc) AS varl7_sc, '
				. 'MIN(varp.l8) AS varl8, MIN(varp.l8_sc) AS varl8_sc, '
				. 'MIN(varp.l9) AS varl9, MIN(varp.l9_sc) AS varl9_sc, '
				. 'MIN(varp.l10) AS varl10, MIN(varp.l10_sc) AS varl10_sc,'
				. 'p.serie '
				. 'FROM prodotti AS p '
				. 'LEFT JOIN varianti_prodotti varp ON varp.id_prodotto = p.id '
				. "WHERE visibile = '1' GROUP BY p.id";
		$this->em->cleanAll();
		$this->em->bindAttributes($q, 'O_product');
		$prods = $this->em->findByQuery($q);

		return $prods;
	}

	/**
	 * Ritorna un array di oggetti brand,
	 * se è settata la variabile order== 'rilevant' li ordina per num di prodotti se
	 * altrimenti li ordina in base al nome
	 * @param type $limit
	 * @param type $order
	 * @return  T_brand
	 */
	public function getBrandsList($limit = 0, $order = false)
	{
		$this->em->cleanAll();
		if ($order == 'rilevant') {
			$this->em->setOrder_by(':num_prodotti DESC');
		} elseif ($order == 'latest') {
			$this->em->setOrder_by(':id DESC');
		} else {
			$this->em->setOrder_by(':name ASC');
		}
		if ($limit != 0) {
			$this->em->setLimit($limit);
		}
		$this->em->setRepository('O_brand');
		$this->em->setGroup_by('m.id');
		return $this->em->find();
	}

	public function getBrands()
	{
		return $this->getBrandsList($limit = 0, $order = false);
	}

	/**
	 * Ritorna l'oggetto brand con id=$id
	 * @param type $id
	 * @return \T_brand
	 */
	public function getBrand($id)
	{
		$this->em->cleanAll();
		$this->em->setWhere(":id = {$id}");
		$this->em->setRepository('O_brand');
		$brand = $this->em->find();
		return $brand[0];
	}

	/**
	 * Ritorna un matrice di oggetti categoria
	 * se è settato explode calcola anche se sottocategorie
	 * @param int $id_top
	 * @param bool $explode
	 * @return O_category
	 */
	public function getCategoriesList($id_top = 0, $explode = false, $withNum_Art = false)
	{
		$this->em->cleanAll();
		$this->em->setWhere(":id_top_cat = '$id_top' AND :abilitata = '1'");
		$this->em->setOrder_by(':ordine, name');
		$this->em->setRepository('O_category');
		$categories = $this->em->find();
		if ($categories != false && count($categories) > 0) {
			for ($i = 0; $i < count($categories); ++$i) {
				if ($withNum_Art != false) {
					$categories[$i]->num_art = $this->getNumArt($categories[$i]->id);
				}
				if ($explode > 0) {
					$categories[$i]->subcategory = $this->getCategoriesList($categories[$i]->id, false, $withNum_Art);
				}
			}
			return $categories;
		}
		return false;
	}

	/**
	 * Ritorna la lista dei prodotti ricercati in base ai parametri passati attenzione sono tutti AND
	 * @param int $id_categ
	 * @param int $id_marca
	 * @param int $limit
	 * @param string $order ['prezzo_desc', 'prezzo_asc', 'nome_desc', 'nome_asc', 'novita'] di default li ordina per nome asc
	 * @param string $flag ['vetrina', 'novita', 'offerta']
	 * @param int $offset
	 * @return O_product
	 */
	public function getListProducts($id_categ = '', $id_marca = 0, $limit = 10, $order = null, $flag = null, $offset = 0)
	{
		$this->em->cleanAll();
		if (isset($id_categ)) {
			if (strpos($id_categ, ',') > 0) {
				self::$current_cat = substr($id_categ, 0, strpos($id_categ, ','));
			} else {
				self::$current_cat = $id_categ;
			}
		}
		self::$limit = $limit;
		self::$offset = $offset;

		$where = " 1 AND p.visibile='1'";
		if ($id_marca != 0) {
			$where .= " AND :id_marca='{$id_marca}'";
		}
		if ($id_categ != '') {
			$where .= " AND (:id_categoria IN({$id_categ}) OR id_categorie IN({$id_categ}))";
		}
		switch ($flag) {
			case 'vetrina':
				$where .= " AND :primo_piano='1'";
				break;
			case 'novita':
				$where .= " AND :novita='1'";
				break;
			case 'offerta':
				$where .= " AND :offerta='1'";
				break;
			default:
				break;
		}
		switch ($order) {
			case 'prezzo_desc':
				$this->em->setOrder_by('p.sorting, :ordine_cat, :prezzo_vendita DESC');
				break;
			case 'prezzo_asc':
				$this->em->setOrder_by('p.sorting, :ordine_cat, :prezzo_vendita ASC');
				break;
			case 'nome_desc':
				$this->em->setOrder_by('p.sorting, :ordine_cat, :titolo DESC');
				break;
			case 'nome_asc':
				$this->em->setOrder_by('p.sorting, :ordine_cat, :titolo ASC');
				break;
			case 'novita':
				$this->em->setOrder_by('p.sorting, :ordine_cat, :novita DESC');
				break;
			default:
				$this->em->setOrder_by('p.sorting, :ordine_cat, :prezzo_vendita ASC, :titolo ASC');
				break;
		}
		$this->em->setWhere($where);
		$this->em->setLimit($limit);
		$this->em->setOffset($offset);
		$this->em->setGroup_by(' :id');
		$this->em->setRepository('O_product');
		$products = $this->em->find();

		if (is_array($products)) {
			for ($i = 0; $i < count($products); $i++) {
				$products[$i]->prezzo_variante_min_ivato = $products[$i]->prezzo_variante_min * (1 + $products[$i]->iva / 100);
				$products[$i]->prezzo_variante_max_ivato = $products[$i]->prezzo_variante_max * (1 + $products[$i]->iva / 100);
				$products[$i]->listino_variante_min_ivato = $products[$i]->listino_variante_min * (1 + $products[$i]->iva / 100);
				$products[$i]->seoUrl = Utils::url_slug($products[$i]->titolo) . '-' . $products[$i]->id;
			}
		}
		return $products;
	}

	/**
	 * Ritorna l'oggetto produtc con id=$id
	 * @param type $id
	 * @return O_product
	 */
	public function getProduct($id)
	{
		$this->em->cleanAll();
		$this->em->setWhere(":id = '$id'");
		$this->em->setLimit(1);
		$this->em->setRepository('O_product');
		// @var $products O_product
		$products = $this->em->find();
		if (is_array($products) && count($products) > 0) {

			//   $products[0]->getRelated_product();
			//   $products[0]->getVariant_product();
			//   $products[0]->variant_product[0]->getAttribute_variant();
			$products[0]->getImagesId();
			for ($i = 0; $i < count($products[0]->imgs); $i++) {
				$products[0]->imgs[$i]['url'] = $this->getImgUrl($products[0]->imgs[$i]['id']);
			}

			self::$current_prod = $products[0];
			self::$current_cat = $products[0]->id_categoria;

			return $products[0];
		}
		return null;
	}

	/*    public function getNum_art($id_category) {
	  return $this->CountProdutctOfCategory($id_category);
	  }
	 */

	private function CountProdutctOfCategory($id_cat)
	{
		$q = "SELECT COUNT(id) AS num FROM
              prodotti LEFT JOIN categorie_prodotti AS cp ON cp.id_prodotti=p.id
              WHERE id_categoria='{$id_cat}' OR cp.id_gategorie='{$id_cat}'";
		$res = $this->em->findByQuery($q);

		$num_art = $res[0]['num'] + $this->CountProductOfSubCategory($id_cat);
		return $num_art;
	}

	/*
	  private function CountProductOfSubCategory($id_cat) {
	  $q = "SELECT SUM(num_art) AS num_art
	  FROM(
	  SELECT COUT(prodotti.id) AS num_art
	  FROM prodotti
	  INNER JOIN categorie AS cat ON prodotti.id_categoria=cat.id

	  WHERE cat.id_top_cat='{$id_cat}' AND prodotti.visibile='1'
	  GROUP BY cat.id_top_cat, id_categoria
	  )prodcount";
	  $res = $this->em->findByQuery($q);
	  return $res[0]['num_art'];
	  }
	 */

	private function getNumArt($id_cat)
	{
		$q = " SELECT count(p.id) AS num_art
                FROM prodotti as p
                LEFT JOIN categorie_prodotti AS cp ON cp.id_prodotti=p.id
                WHERE p.id_categoria IN ('{$id_cat}') OR cp.id_categorie IN '{$id_cat}'";
		$res = $this->em->findByQuery($q);
		return $res[0]['num_art'];
	}

	/**
	 * Ritorna l'oggetto categoria con id=$id
	 * @param type $id
	 * @return O_category
	 */
	public function getCategory($id)
	{
		$this->em->cleanAll();
		$this->em->setWhere("id = '$id'");
		$this->em->setLimit(1);
		$this->em->setRepository('O_category');
		$category = $this->em->find();
		// $category[0]->num_art = $this->getNum_art($category[0]->id);
		return $category[0];
	}

	public function getCategories($all = false, $orderby = '')
	{
		$this->em->cleanAll();

		if (!$all) {
			$this->em->setWhere(" 1 AND abilitata='1'");
		}
		if ($orderby != '') {
			$this->em->setOrder_by($orderby);
		} else {
			$this->em->setOrder_by('ordine, name');
		}
		$this->em->setRepository('O_category');

		$categories = $this->em->find();
		//echo $this->em->getLastSQL();

		return $categories;
	}

	/**
	 * Return if product is available
	 * @param int $id_prod
	 * @param int $id_var
	 * @param int $qty
	 * @return boolean
	 */
	public function ProdIsAvailable($id_prod, $id_var, $qty)
	{
		if ($id_var == 0) {
			$q = "SELECT id_disponibilita, quantita_disponibile AS qta FROM prodotti WHERE id = $id_prod";
		} else {
			$q = "SELECT id_disponibilita, disponibilita AS qta FROM varianti_prodotti INNER JOIN prodotti ON prodotti.id = varianti_prodotti.id_prodotto WHERE varianti_prodotti.id = $id_var AND id_prodotto = $id_prod";
		}

		$res = $this->em->findByQuery($q);

		$dispo = $res[0]['id_disponibilita'];
		$qta_mag = $res[0]['qta'];

		if ($dispo == 1 && $qta_mag == 0) { //quantità illimitata
			return true;
		}

		//        if (isset($_SESSION['cart0']) && $_SESSION['cart'] != false) { //Se esiste un carrello in sessione lo recupero
		//            $cart = $_SESSION['cart'];
		//        }
		//        else
		//        {
		//            $id_user = '';
		//            if (isset($_SESSION['user_id']) && $_SESSION['user_id']!='') {
		//                $id_user = $_SESSION['user_id'];
		//            }
		//            $OrdersModel = Loader::loadModel('Orders', 'Orders', 'BlueShop');
		//            $cart = $OrdersModel->getCart($id_user, session_id(),'order');  //Cerco nel db se l'utente ha già un carrello creato
//
		//            if (!is_null($cart) && $cart!==false) {
		//                $cart->getDetails();
		//                foreach ($cart->details as $det) {
		//                    if($det->id_prodotto == $id_prod && $det->id_variante == $id_var){
		//                        $qta_mag = $qta_mag - $det->quantita;
		//                    }
		//                }
		//            }
//
		//        }

		if ($dispo == 1 && $qta_mag > 0 && $qta_mag >= $qty || //quantità limitata
				$dispo == 3) {										//in arrivo
			return true;
		}  								//Non disponibile
		return false;
	}

	/**
	 * Genera la stringa per clausola WHERE in SQL (per ricerca prodotti)
	 * @param type $keywords
	 * @return string
	 */
	private function SqlWhere($keywords)
	{
		$where = '';

		$cosa = trim($keywords);
		$cosa = str_replace('.', ' ', $cosa);
		$cosa = str_replace(',', ' ', $cosa);
		$cosa = str_replace('-', ' ', $cosa);

		$cosa = strtoupper(trim($cosa));

		$arr_cosa = explode(' ', $cosa);
		for ($ind = 0; $ind < count($arr_cosa); $ind++) {
			if (strlen($arr_cosa[$ind]) < 3) {
				array_splice($arr_cosa, $ind, 1);
			} else {
				$arr_cosa[$ind] = substr($arr_cosa[$ind], 0, -1);
				$arr_cosa[$ind] = mysql_escape_string($arr_cosa[$ind]);
			}
		}

		if (count($arr_cosa) > 0) {
			$where = ' 1 ';
		}

		for ($ind = 0; $ind < count($arr_cosa); $ind++) {
			$where .= " AND( UPPER(CONCAT_WS(
                CASE WHEN (p.titolo:user_lang!='') THEN p.titolo:user_lang ELSE p.titolo:default_lang END,
                ' ',CASE WHEN (p.descrizione_txt:user_lang!='') THEN p.descrizione_txt:user_lang ELSE p.descrizione_txt:default_lang END,
                ' ',CASE WHEN (c.categoria:user_lang!='') THEN c.categoria:user_lang ELSE c.categoria:default_lang END,
                ' ',m.marca)) LIKE '%" . $arr_cosa[$ind] . "%' 
                OR UPPER(p.codice)='" . strtoupper(mysql_escape_string($cosa)) . "'
                OR UPPER(vp.codice)='" . strtoupper(mysql_escape_string($cosa)) . "' ) ";
		}

		return $where;
	}

	public function getProductSearchForPrice($keywords, $order, $limit, $offset, $lower_price, $upper_price, $prezzi_ivati, $id_cat)
	{
		if (isset($id_cat)) {
			self::$current_cat = $id_cat;
		}

		$query = "SELECT
        p.id,
        p.codice,
        CASE
            WHEN (p.titolo:user_lang != '') THEN p.titolo:user_lang
            ELSE p.titolo:default_lang
        END AS titolo,
        CASE
            WHEN (p.descrizione_txt:user_lang != '') THEN p.descrizione_txt:user_lang
            ELSE p.descrizione_txt:default_lang
        END AS descrizione,
        CASE
            WHEN (p.descrizione_html:user_lang != '') THEN p.descrizione_html:user_lang
            ELSE p.descrizione_html:default_lang
        END AS descrizione_html,
        CASE
            WHEN (p.tag_description:user_lang != '') THEN p.tag_description:user_lang
            ELSE p.tag_description:default_lang
        END AS metadescription,
        CASE
            WHEN (p.tag_keywords:user_lang != '') THEN p.tag_keywords:user_lang
            ELSE p.tag_keywords:default_lang
        END AS metakeywords,
        CASE
            WHEN (c.categoria:user_lang != '') THEN c.categoria:user_lang
            ELSE c.categoria:default_lang
        END AS categoria,
        p.note,
        p.id_categoria,
        p.iva,
        serie,
        p.id_marca,
        p.id_unita_misura,
        p.offerta,
        p.id_disponibilita,
        d.descrizione as disponibilita,
        p.novita,
        p.primo_piano,
        p.quantita_disponibile,
        p.peso,
        p.tipo_vis_varianti,
        p.listino:nlistino_conf_min AS conf_min,
        ROUND(p.listino:nlistino, 2) AS prezzo_listino,
        ROUND(p.listino:nlistino_sc, 2) AS prezzo_vendita,
        p.listino:nlistino AS prezzo_listino_raw,
        p.listino:nlistino_sc AS prezzo_vendita_raw,
        ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100),
                2) AS prezzo_vendita_ivato,
        ROUND((p.listino:nlistino - p.listino:nlistino_sc), 2) AS risparmio,
        1 as withphoto,
        m.marca,
        (SELECT i.id FROM img_prodotti as i WHERE i.id_prodotto=p.id LIMIT 1) as img,

        MIN(vp.l:nlistino_sc) AS prezzo_variante_min,
        MAX(vp.l:nlistino_sc) AS prezzo_variante_max,
        MIN(vp.l:nlistino) AS listino_variante_min,
        MAX(vp.l:nlistino) AS listino_variante_max,
        1 AS prezzi_ivati,
        MIN(vp.id) AS id_variante_def

        FROM
            prodotti p
                INNER JOIN
            disponibilita AS d ON p.id_disponibilita = d.id
                INNER JOIN
            categorie AS c ON p.id_categoria = c.id

                LEFT JOIN
            marche AS m ON m.id = p.id_marca
        ";

		if (!is_null($id_cat)) {
			$query .= ' LEFT JOIN categorie_prodotti AS cp ON cp.id_prodotti=p.id';
		}
		$query .= ' LEFT JOIN varianti_prodotti AS vp ON vp.id_prodotto = p.id ';
		if ($prezzi_ivati) {
			$query .= " AND ROUND( vp.l:nlistino_sc + vp.l:nlistino_sc * p.iva / 100 , 2 ) >='" . $lower_price . "' AND ROUND( vp.l:nlistino_sc + vp.l:nlistino_sc * p.iva / 100 , 2 ) <='" . $upper_price . "'";
		} else {
			$query .= " AND ROUND(MIN(vp.l:nlistino_sc),2)>='" . $lower_price . "' AND ROUND(MIN(vp.l:nlistino_sc),2)<='" . $upper_price . "'";
		}
		$query .= ' WHERE ';

		$where = '1 AND c.abilitata = 1';
		if ($keywords != '') {
			$where = $this->SqlWhere($keywords);
		}

		if ($id_cat != null) {
			if ($where != null) {
				$where .= ' AND (p.id_categoria IN(' . addslashes($id_cat) . ') OR cp.id_categorie IN(' . addslashes($id_cat) . '))';
			} else {
				$where = ' p.id_categoria=IN(' . addslashes($id_cat) . ')';
			}
		}
		if ($prezzi_ivati) {
			$where .= " AND ((p.serie='0' AND ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100) ,2) >='" . $lower_price . "' AND ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100) ,2) <='" . $upper_price . "') OR ("
					. "p.serie = '1' AND ROUND( vp.l:nlistino_sc + vp.l:nlistino_sc * p.iva / 100 , 2 ) >='" . $lower_price . "' AND ROUND( vp.l:nlistino_sc + vp.l:nlistino_sc * p.iva / 100 , 2 ) <='" . $upper_price . "'))";
		} else {
			$where .= " AND ((p.serie='0' AND ROUND(p.listino:nlistino_sc,2)>='" . $lower_price . "' AND ROUND(p.listino:nlistino_sc,2)<='" . $upper_price . "') OR ("
					. "p.serie = '1' AND ROUND(MIN(vp.l:nlistino_sc),2)>='" . $lower_price . "' AND ROUND(MIN(vp.l:nlistino_sc),2)<='" . $upper_price . "'))";
		}

		$where .= ' GROUP BY id';
		switch ($order) {
			case 'prezzo_desc':
				$where .= ' ORDER BY c.ordine, prezzo_variante_min DESC, prezzo_vendita DESC';
				break;
			case 'prezzo_asc':
				$where .= ' ORDER BY c.ordine, prezzo_variante_min ASC, prezzo_vendita ASC';
				break;
			case 'nome_desc':
				$where .= ' ORDER BY c.ordine, :titolo DESC';
				break;
			case 'nome_asc':
				$where .= ' ORDER BY c.ordine, :titolo ASC';
				break;
			case 'novita':
				$where .= ' ORDER BY c.ordine, :novita DESC';
				break;
			default:
				$where .= ' ORDER BY c.ordine, :titolo ASC';
				break;
		}

		if ($limit != 0) {
			$where .= ' LIMIT ' . $limit;
		}
		if ($offset != '') {
			$where .= ' OFFSET ' . $offset;
		}

		$query .= $where;
		$this->em->bindAttributes($query, 'O_product');
		$products = $this->em->findByQuery($query, 'O_product');
		if (is_array($products)) {
			for ($i = 0; $i < count($products); $i++) {
				$products[$i]->prezzo_listino_ivato = $products[$i]->prezzo_listino * (1 + $products[$i]->iva / 100);
				$products[$i]->prezzo_vendita_ivato = $products[$i]->prezzo_vendita * (1 + $products[$i]->iva / 100);
				$products[$i]->prezzo_variante_min_ivato = $products[$i]->prezzo_variante_min * (1 + $products[$i]->iva / 100);
				$products[$i]->prezzo_variante_max_ivato = $products[$i]->prezzo_variante_max * (1 + $products[$i]->iva / 100);
				$products[$i]->listino_variante_min_ivato = $products[$i]->listino_variante_min * (1 + $products[$i]->iva / 100);
			}
		}
		return $products;
	}

	public function getCountProductSearchForPrice($keywords, $prezzi_ivati, $lower_price, $upper_price, $id_cat = null)
	{
		$query = 'SELECT COUNT(DISTINCT(p.id)) AS num
                    FROM prodotti p
                    INNER JOIN disponibilita AS d ON p.id_disponibilita=d.id';
		if ($id_cat != null) {
			$query .= ' INNER JOIN categorie AS c ON p.id_categoria = c.id';
			$query .= ' LEFT JOIN categorie_prodotti AS cp ON cp.id_prodotti=p.id';
		}
		if ($keywords != '') {
			$query .= ' LEFT JOIN marche AS m ON p.id_marca = m.id';
		}
		$query .= ' LEFT JOIN varianti_prodotti AS vp ON vp.id_prodotto = p.id ';
		if ($prezzi_ivati) {
			$query .= " AND ROUND( vp.l:nlistino_sc + vp.l:nlistino_sc * p.iva / 100 , 2 ) >='" . $lower_price . "' AND ROUND( vp.l:nlistino_sc + vp.l:nlistino_sc * p.iva / 100 , 2 ) <='" . $upper_price . "'";
		} else {
			$query .= " AND ROUND(MIN(vp.l:nlistino_sc),2)>='" . $lower_price . "' AND ROUND(MIN(vp.l:nlistino_sc),2)<='" . $upper_price . "'";
		}

		$query .= ' WHERE ';

		$where = '1';
		if ($keywords != '') {
			$where = $this->SqlWhere($keywords);
		}

		if ($id_cat != null) {
			if ($where != null) {
				$where .= ' AND ( p.id_categoria IN(' . addslashes($id_cat) . ') OR cp.id_categorie IN(' . addslashes($id_cat) . '))';
			} else {
				$where = ' p.id_categoria=IN(' . addslashes($id_cat) . ') OR cp.id_categorie IN(' . addslashes($id_cat) . ')';
			}
		}
		if ($prezzi_ivati) {
			$where .= " AND ((p.serie='0' AND ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100) ,2) >='" . $lower_price . "' AND ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100) ,2) <='" . $upper_price . "') OR ("
					. "p.serie = '1' AND ROUND( vp.l:nlistino_sc + vp.l:nlistino_sc * p.iva / 100 , 2 ) >='" . $lower_price . "' AND ROUND( vp.l:nlistino_sc + vp.l:nlistino_sc * p.iva / 100 , 2 ) <='" . $upper_price . "'))";
		} else {
			$where .= " AND ((p.serie='0' AND ROUND(p.listino:nlistino_sc,2)>='" . $lower_price . "' AND ROUND(p.listino:nlistino_sc,2)<='" . $upper_price . "') OR ("
					. "p.serie = '1' AND ROUND(MIN(vp.l:nlistino_sc),2)>='" . $lower_price . "' AND ROUND(MIN(vp.l:nlistino_sc),2)<='" . $upper_price . "'))";
		}

		//$where.=" GROUP BY p.id";

		$query .= $where;
		$this->em->bindAttributes($query, 'O_product');
		$res = $this->em->findByQuery($query);

		$count = $res[0]['num'];
		return $count;
	}

	public function getPriceBound($keywords = null, $id_cat = null)
	{
		//         $query = "SELECT       MIN(ROUND(p.listino:nlistino_sc,2)) AS min_prezzo_vendita,
		//                                MIN(ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100) ,2)) AS min_prezzo_vendita_ivato,
		//                                MAX(ROUND(p.listino:nlistino_sc,2)) AS max_prezzo_vendita,
		//                                MAX(ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100) ,2)) AS max_prezzo_vendita_ivato,
		//                                MIN((SELECT ROUND(MIN(vp.l:nlistino_sc),2) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)) AS prezzo_variante_min,
		//                                MIN((SELECT ROUND( MIN(vp.l:nlistino_sc) + ( MIN(vp.l:nlistino_sc) * p.iva / 100 ), 2 ) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)) AS prezzo_variante_min_ivato,
		//                                MAX((SELECT ROUND( MAX(vp.l:nlistino_sc), 2 ) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)) AS prezzo_variante_max,
		//                                MAX((SELECT ROUND( MAX(vp.l:nlistino_sc) + ( MAX(vp.l:nlistino_sc) * p.iva / 100 ), 2 ) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)) AS prezzo_variante_max_ivato
//
		//                              FROM prodotti p
		//                              INNER JOIN disponibilita AS d ON p.id_disponibilita=d.id
		//                              INNER JOIN categorie AS c ON p.id_categoria = c.id ";
		$query = "SELECT
                        CASE WHEN (p.serie='0')
                            THEN (MIN(ROUND(p.listino:nlistino_sc,2)))
                            ELSE (MIN((SELECT ROUND(MIN(vp.l:nlistino_sc),2) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)))
                        END AS min_prezzo_vendita,
                        
                        CASE WHEN (p.serie='0')
                            THEN (MIN(ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100) ,2)))
                            ELSE (MIN((SELECT ROUND( MIN(vp.l:nlistino_sc) + ( MIN(vp.l:nlistino_sc) * p.iva / 100 ), 2 ) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)))
                        END AS min_prezzo_vendita_ivato,
                        
                        CASE WHEN (p.serie='0')
                            THEN (MAX(ROUND(p.listino:nlistino_sc,2)))
                            ELSE (MAX((SELECT ROUND( MAX(vp.l:nlistino_sc), 2 ) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)))
                        END AS max_prezzo_vendita,
                        
                        CASE WHEN (p.serie='0')
                            THEN (MAX(ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100) ,2)))
                            ELSE (MAX((SELECT ROUND( MAX(vp.l:nlistino_sc) + ( MAX(vp.l:nlistino_sc) * p.iva / 100 ), 2 ) FROM varianti_prodotti vp WHERE vp.id_prodotto = p.id)))
                        END AS max_prezzo_vendita_ivato
                        

                              FROM prodotti p
                              INNER JOIN disponibilita AS d ON p.id_disponibilita=d.id
                              INNER JOIN categorie AS c ON p.id_categoria = c.id ";
		if (!is_null($id_cat)) {
			$query .= 'LEFT JOIN categorie_prodotti AS cp ON cp.id_prodotti=p.id ';
		}
		if ($keywords != null) {
			$query . ' LEFT JOIN marche AS m ON p.id_marca = m.id
                LEFT JOIN varianti_prodotti AS vp ON vp.id_prodotto = p.id  ';
		}
		$where = ' WHERE 1';
		if ($keywords != null) {
			$where = $this->SqlWhere($keywords);
		}

		if ($id_cat != null) {
			if ($where != null) {
				$where .= ' AND (p.id_categoria IN(' . addslashes($id_cat) . ') OR cp.id_categorie IN(' . addslashes($id_cat) . '))';
			} else {
				$where = ' p.id_categoria=IN(' . addslashes($id_cat) . ') OR cp.id_categorie IN(' . addslashes($id_cat) . ')';
			}
		}
		$query .= $where;
		$this->em->cleanAll();
		$this->em->bindAttributes($query, 'O_product');

		//echo $query;
		$result = $this->em->findByQuery($query);
		return $result;
	}

	public function search($keywords, $limit = 10, $offset = 0, $onlyEnabled = true, &$num_rows = 0)
	{
		$pertinenceField = getPertinenceSQL($keywords, 5, 3, 0);

		$query = "SELECT
            p.id,
            p.codice,
            CASE
                WHEN (p.titolo:user_lang != '') THEN p.titolo:user_lang
                ELSE p.titolo:default_lang
            END AS titolo,
            CASE
                WHEN (p.descrizione_txt:user_lang != '') THEN p.descrizione_txt:user_lang
                ELSE p.descrizione_txt:default_lang
            END AS descrizione,
            CASE
                WHEN (p.descrizione_html:user_lang != '') THEN p.descrizione_html:user_lang
                ELSE p.descrizione_html:default_lang
            END AS descrizione_html,
            CASE
                WHEN (p.tag_description:user_lang != '') THEN p.tag_description:user_lang
                ELSE p.tag_description:default_lang
            END AS metadescription,
            CASE
                WHEN (p.tag_keywords:user_lang != '') THEN p.tag_keywords:user_lang
                ELSE p.tag_keywords:default_lang
            END AS metakeywords,
            CASE
                WHEN (c.categoria:user_lang != '') THEN c.categoria:user_lang
                ELSE c.categoria:default_lang
            END AS categoria,
            p.note,
            p.id_categoria,
            p.iva,
            serie,
            p.id_marca,
            p.id_unita_misura,
            p.offerta,
            p.id_disponibilita,
            d.descrizione as disponibilita,
            p.novita,
            p.primo_piano,
            p.quantita_disponibile,
            p.peso,
            p.tipo_vis_varianti,
            p.listino:nlistino_conf_min AS conf_min,
            ROUND(p.listino:nlistino, 2) AS prezzo_listino,
            ROUND(p.listino:nlistino_sc, 2) AS prezzo_vendita,
            ROUND(p.listino:nlistino + (p.listino:nlistino * p.iva / 100), 2) AS prezzo_listino_ivato,
            ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100), 2) AS prezzo_vendita_ivato,
            ROUND((p.listino:nlistino - p.listino:nlistino_sc), 2) AS risparmio,
            p.listino:nlistino AS prezzo_listino_raw,
            p.listino:nlistino_sc AS prezzo_vendita_raw,
            1 as withphoto,
            m.marca,
            (SELECT i.id FROM img_prodotti as i WHERE i.id_prodotto=p.id LIMIT 1) as img,

            MIN(vp.l:nlistino_sc) AS prezzo_variante_min,
            MAX(vp.l:nlistino_sc) AS prezzo_variante_max,
            MIN(vp.l:nlistino) AS listino_variante_min,
            MAX(vp.l:nlistino) AS listino_variante_max,
            1 AS prezzi_ivati,
            MIN(vp.id) AS id_variante_def,

            {$pertinenceField} AS pertinence

            FROM
                prodotti p
                    INNER JOIN
                disponibilita AS d ON p.id_disponibilita = d.id
                    INNER JOIN
                categorie AS c ON p.id_categoria = c.id

                    LEFT JOIN
                marche AS m ON m.id = p.id_marca
                LEFT JOIN 
                varianti_prodotti AS vp ON vp.id_prodotto = p.id 
            ";

		$where = " WHERE p.visibile=1 AND c.abilitata = '1' AND {$pertinenceField} > 0";

		$groupby = 'GROUP BY p.id';

		$orderby = 'ORDER BY pertinence DESC, p.sorting, c.ordine, :titolo ASC';

		$qcount = "SELECT COUNT(*) AS numrows FROM ($query $where $groupby) AS tab";
		$qcount = $this->em->bindAttributes($qcount, 'O_product');
		$resqcount = $this->db->select($qcount, array(':user_lang' => $this->settings->lang_default, ':nlistino' => BS_Session::get_listino()));
		$num_rows = (isset($resqcount[0]['numrows']) ? $resqcount[0]['numrows'] : 0);

		$sqllimit = '';
		if ($limit != 0) {
			$sqllimit = ' LIMIT ' . $limit;
		}
		if ($offset != '') {
			$sqllimit .= ' OFFSET ' . $offset;
		}

		$query = $query . ' ' . $where . ' ' . $groupby . ' ' . $orderby . ' ' . $sqllimit; //echo $query;
		$products = $this->em->findByQuery($query, 'O_product'); //echo $this->em->getLastSQL();
		if (is_array($products)) {
			for ($i = 0; $i < count($products); $i++) {
				$products[$i]->prezzo_variante_min_ivato = $products[$i]->prezzo_variante_min * (1 + $products[$i]->iva / 100);
				$products[$i]->prezzo_variante_max_ivato = $products[$i]->prezzo_variante_max * (1 + $products[$i]->iva / 100);
				$products[$i]->listino_variante_min_ivato = $products[$i]->listino_variante_min * (1 + $products[$i]->iva / 100);
				$products[$i]->seoUrl = UrlUtils::url_slug($products[$i]->titolo) . '-' . $products[$i]->id;
			}
		}
		return $products;
	}

	/**
	 * Search Products
	 * @param string $keywords
	 * @param int $id_categ
	 * @param int $limit
	 * @param string $order ['prezzo_desc', 'prezzo_asc', 'nome_desc', 'nome_asc', 'novita'] di default li ordina per nome asc
	 * @param int $offset
	 * @return O_product
	 * */
	public function getProductSearch($keywords, $order = '', $limit = 10, $offset = '', $id_cat = null, $visibile = true, $params = null, $filter = null, &$num_rows = 0)
	{
		$query = "SELECT
            p.id,
            p.codice,
            CASE
                WHEN (p.titolo:user_lang != '') THEN p.titolo:user_lang
                ELSE p.titolo:default_lang
            END AS titolo,
            CASE
                WHEN (p.descrizione_txt:user_lang != '') THEN p.descrizione_txt:user_lang
                ELSE p.descrizione_txt:default_lang
            END AS descrizione,
            CASE
                WHEN (p.descrizione_html:user_lang != '') THEN p.descrizione_html:user_lang
                ELSE p.descrizione_html:default_lang
            END AS descrizione_html,
            CASE
                WHEN (p.tag_description:user_lang != '') THEN p.tag_description:user_lang
                ELSE p.tag_description:default_lang
            END AS metadescription,
            CASE
                WHEN (p.tag_keywords:user_lang != '') THEN p.tag_keywords:user_lang
                ELSE p.tag_keywords:default_lang
            END AS metakeywords,
            CASE
                WHEN (c.categoria:user_lang != '') THEN c.categoria:user_lang
                ELSE c.categoria:default_lang
            END AS categoria,
            p.note,
            p.id_categoria,
            p.iva,
            serie,
            p.id_marca,
            p.id_unita_misura,
            p.offerta,
            p.id_disponibilita,
            d.descrizione as disponibilita,
            p.novita,
            p.primo_piano,
            p.quantita_disponibile,
            p.peso,
            p.tipo_vis_varianti,
            p.listino:nlistino_conf_min AS conf_min,
            ROUND(p.listino:nlistino, 2) AS prezzo_listino,
            ROUND(p.listino:nlistino_sc, 2) AS prezzo_vendita,
            ROUND(p.listino:nlistino + (p.listino:nlistino * p.iva / 100), 2) AS prezzo_listino_ivato,
            ROUND(p.listino:nlistino_sc + (p.listino:nlistino_sc * p.iva / 100), 2) AS prezzo_vendita_ivato,
            ROUND((p.listino:nlistino - p.listino:nlistino_sc), 2) AS risparmio,
            p.listino:nlistino AS prezzo_listino_raw,
            p.listino:nlistino_sc AS prezzo_vendita_raw,
            1 as withphoto,
            m.marca,
            (SELECT i.id FROM img_prodotti as i WHERE i.id_prodotto=p.id LIMIT 1) as img,

            MIN(vp.l:nlistino_sc) AS prezzo_variante_min,
            MAX(vp.l:nlistino_sc) AS prezzo_variante_max,
            MIN(vp.l:nlistino) AS listino_variante_min,
            MAX(vp.l:nlistino) AS listino_variante_max,
            1 AS prezzi_ivati,
            MIN(vp.id) AS id_variante_def

            FROM
                prodotti p
                    INNER JOIN
                disponibilita AS d ON p.id_disponibilita = d.id
                    INNER JOIN
                categorie AS c ON p.id_categoria = c.id

                    LEFT JOIN
                marche AS m ON m.id = p.id_marca
                LEFT JOIN 
                varianti_prodotti AS vp ON vp.id_prodotto = p.id 
            ";

		if (!is_null($id_cat)) {
			$query .= ' LEFT JOIN categorie_prodotti AS cp ON cp.id_prodotti=p.id';
		}
		$query .= ' WHERE ';
		$where = ' 1 ';
		if ($keywords != '') {
			$qOrderPriority = '(ti) AS orderPriority';
			echo $keywords;

			$where = $this->SqlWhere($keywords);
		}

		if ($id_cat != null) {
			if ($where != null) {
				$where .= ' AND (p.id_categoria IN(' . addslashes($id_cat) . ') OR cp.id_categorie IN(' . addslashes($id_cat) . '))';
			} else {
				$where = ' p.id_categoria=IN(' . addslashes($id_cat) . ') OR cp.id_categorie IN(' . addslashes($id_cat) . ')';
			}
		}
		if (!is_null($visibile) && $visibile == true) {
			$where .= ' AND p.visibile=1 ';
		}
		if (!is_null($params)) {
			$where .= ' AND( ';
			for ($i = 0; $i < count($params); $i++) {
				if (strtolower($params[$i]) == 'vetrina') {
					$params[$i] = 'primo_piano';
				}
				if (strtolower($params[$i]) == 'promo') {
					$params[$i] = 'offerta';
				}

				if ($i == 0) {
					$where .= $params[$i] . '=1';
				} else {
					$where .= ' OR ' . $params[$i] . '=1';
				}
			}
			$where .= ' )';
		}

		if ($filter != '' && $filter != null) {
			$where .= ' AND ( ' . $filter . " ) AND c.abilitata = '1'";
		}

		$where .= ' GROUP BY id';
		switch ($order) {
			case 'prezzo_desc':
				$where .= ' ORDER BY prezzo_variante_min DESC, prezzo_vendita DESC, p.sorting, c.ordine';
				break;
			case 'prezzo_asc':
				$where .= ' ORDER BY prezzo_variante_min ASC, prezzo_vendita ASC, p.sorting, c.ordine';
				break;
			case 'nome_desc':
				$where .= ' ORDER BY p.sorting, c.ordine, :titolo DESC';
				break;
			case 'nome_asc':
				$where .= ' ORDER BY p.sorting, c.ordine, :titolo ASC';
				break;
			case 'novita':
				$where .= ' ORDER BY p.sorting, c.ordine, :novita DESC';
				break;
			case 'random':
				$where .= ' ORDER BY RAND()';
				break;
			default:
				$where .= ' ORDER BY p.sorting, c.ordine, :titolo ASC';
				break;
		}

		$qcount = "SELECT COUNT(*) AS numrows FROM ($query $where) AS tab";
		$qcount = $this->em->bindAttributes($qcount, 'O_product');
		$resqcount = $this->db->select($qcount, array(':user_lang' => $this->settings->lang_default, ':nlistino' => BS_Session::get_listino()));
		$num_rows = $resqcount[0]['numrows'];

		if ($limit != 0) {
			$where .= ' LIMIT ' . $limit;
		}
		if ($offset != '') {
			$where .= ' OFFSET ' . $offset;
		}

		$query = $query . $where;

		//$this->em->setWhere($where);
		//$this->em->setLimit($limit);
		//$this->em->setOffset($offset);
		//$this->em->setGroup_by(" :id");
		//$this->em->setRepository("O_product");
		//$products = $this->em->find();

		$products = $this->em->findByQuery($query, 'O_product');
		//echo $this->em->getLastSQL();
		if (is_array($products)) {
			for ($i = 0; $i < count($products); $i++) {
				$products[$i]->prezzo_variante_min_ivato = $products[$i]->prezzo_variante_min * (1 + $products[$i]->iva / 100);
				$products[$i]->prezzo_variante_max_ivato = $products[$i]->prezzo_variante_max * (1 + $products[$i]->iva / 100);
				$products[$i]->listino_variante_min_ivato = $products[$i]->listino_variante_min * (1 + $products[$i]->iva / 100);
				$products[$i]->seoUrl = Utils::url_slug($products[$i]->titolo) . '-' . $products[$i]->id;
			}
		}
		return $products;
	}

	/**
	 * Ritorna il numero di prodotti in base ai paramenti passati
	 * @param id $id_categ
	 * @param string $flag ['vetrina', 'novita', 'offerta']
	 * @param string $where
	 * @param string $sql_join
	 * @return int
	 */
	public function getCount($id_categ = '', $where = '', $sql_join = '', $visibile = true, $params = null)
	{
		$query = 'SELECT COUNT(DISTINCT(p.id)) AS num
                    FROM prodotti p
                        INNER JOIN disponibilita AS d ON p.id_disponibilita=d.id
                        INNER JOIN categorie AS c ON p.id_categoria = c.id
                        LEFT JOIN marche AS m ON p.id_marca = m.id
                        LEFT JOIN varianti_prodotti AS vp ON p.id = vp.id_prodotto';

		if ($id_categ != '') {
			$query .= ' LEFT JOIN categorie_prodotti AS cp ON cp.id_prodotti=p.id';
		}

		$query .= $sql_join;
		$query .= ' WHERE 1  ';
		if ($id_categ != '') {
			$query .= '  AND ( p.id_categoria  IN (' . $id_categ . ') OR cp.id_categorie  IN (' . $id_categ . ')  )';
		}

		$where = '';
		if (!is_null($visibile) && $visibile == true) {
			$where .= ' AND p.visibile=1 ';
		}
		if (!is_null($params)) {
			$where .= ' AND( ';
			for ($i = 0; $i < count($params); $i++) {
				if ($i == 0) {
					$where .= $params[$i] . '=1';
				} else {
					$where .= ' OR ' . $params[$i] . '=1';
				}
			}
			$where .= ' )';
		}

		if ($where != '') {
			$query .= $where;
		}
		$res = $this->em->findByQuery($query);
		$count = $res[0]['num'];

		self::$num_prod = $count;
		return $count;
	}

	/**
	 * Ritorna il numero di prodotti in base alla stringa
	 * @param string $keywords
	 * @return int
	 */
	public function getCountProductSearch($keywords, $id_cat = null)
	{
		$where = '';

		if ($keywords != '') {
			$where = $this->sqlWhere($keywords);
		}

		if ($id_cat != null) {
			if ($where != null) {
				$where .= ' AND (p.id_categoria IN(' . addslashes($id_cat) . ') OR id_categorie IN(' . addslashes($id_cat) . ')) ';
			} else {
				$where = 'p.id_categoria IN(' . addslashes($id_cat) . ') OR  id_categorie IN(' . addslashes($id_cat) . ')';
			}
		}

		return $this->getCount('', '', $where);
	}

	/**
	 * Ritorna il numero di prodotti appartenenti alla lista
	 * @param int $id_lista
	 * @return int
	 */
	public function getCountWeddingProducts($id_lista)
	{
		$sql_join = 'INNER JOIN lista_nozze_prodotti AS ln ON ln.id_prodotto = p.id';

		$where = 'id_lista_nozze = ' . $id_lista;

		return $this->getCount('', '', $where, $sql_join);
	}

	/**
	 * Ritorna il numero di articoli in base alla ricerca per tags
	 * @param array $aTags
	 * @return int
	 */
	public function getCountProductSearchForTags($aTags)
	{
		$where = '0 ';

		foreach ($aTags as $tag) {
			$where .= " OR LOWER(CONCAT(p.tag_prodotto,'\\r\\n')) LIKE LOWER('%$tag\\r\\n%') ";
		}

		return $this->getCount('', '', $where);
	}

	/**
	 * Ritorna il numero di articoli risultanti dalla ricerca per caratteristiche
	 * @param array $array_caratt
	 * @return int|null
	 */
	public function getCountProductSearchForFeatures($array_caratt)
	{
		$sql_join = '';

		if (count($array_caratt) > 0) {
			$sql_join = ' INNER JOIN (SELECT id_prodotto FROM caratteristiche_prod WHERE 0 ';
			$count_field = 0;
			foreach ($array_caratt as $k => $v) {
				$k = mysql_escape_string($k);
				$v = mysql_escape_string($v);
				$count_field++;
				$sql_join .= " OR (id_caratteristica = $k AND valore = '$v') ";
			}
			$sql_join .= " GROUP BY id_prodotto HAVING COUNT(*) = $count_field) AS caratt ON caratt.id_prodotto = p.id ";

			return $this->getCount('', '', '', $sql_join);
		}
		return null;
	}

	/**
	 * Search Products for tags
	 * @param array $aTags
	 * @param string $order ['prezzo_desc', 'prezzo_asc', 'nome_desc', 'nome_asc', 'novita'] di default li ordina per nome asc
	 * @param int $limit default=10
	 * @param int $offset
	 * @return O_product
	 * */
	public function getProductSearchForTags($aTags, $order = '', $limit = 10, $offset = '', &$num_rows = 0)
	{
		$where = '0 ';

		foreach ($aTags as $tag) {
			$where .= " OR LOWER(CONCAT(p.tag_prodotto,'\\r\\n')) LIKE LOWER('%$tag\\r\\n%') AND c.abilitata = '1'";
		}

		switch ($order) {
			case 'prezzo_desc':
				$this->em->setOrder_by('p.sorting, :ordine_cat, :prezzo_vendita DESC');
				break;
			case 'prezzo_asc':
				$this->em->setOrder_by('p.sorting, :ordine_cat, :prezzo_vendita ASC');
				break;
			case 'nome_desc':
				$this->em->setOrder_by('p.sorting, :ordine_cat, :titolo DESC');
				break;
			case 'nome_asc':
				$this->em->setOrder_by('p.sorting, :ordine_cat, :titolo ASC');
				break;
			case 'novita':
				$this->em->setOrder_by('p.sorting, :ordine_cat, :novita DESC');
				break;
			default:
				$this->em->setOrder_by('p.sorting, :ordine_cat, :titolo ASC');
				break;
		}

		$this->em->setWhere($where);
		$this->em->setGroup_by(' :id');
		$this->em->setLimit('');
		$this->em->setOffset('');
		$this->em->setRepository('O_product');
		$num_rows = $this->em->count();

		$this->em->setWhere($where);
		$this->em->setLimit($limit);
		$this->em->setOffset($offset);
		$this->em->setGroup_by(' :id');
		$this->em->setRepository('O_product');
		$products = $this->em->find();
		return $products;
	}

	/**
	 * Search Products for caratteristiche
	 * @param array $array_caratt
	 * @param string $order ['prezzo_desc', 'prezzo_asc', 'nome_desc', 'nome_asc', 'novita'] di default li ordina per nome asc
	 * @param int $limit default=10
	 * @param int $offset
	 * @return O_product
	 * */
	public function getProductSearchForFeatures($array_caratt, $id_category = '', $order = '', $limit = 10, $offset = '')
	{
		$sql_join = '';

		if (count($array_caratt) > 0) {
			$sql_join = ' INNER JOIN (SELECT id_prodotto FROM caratteristiche_prod WHERE 0 ';
			$count_field = 0;
			foreach ($array_caratt as $k => $v) {
				$k = mysql_escape_string($k);
				$v = mysql_escape_string($v);
				$count_field++;
				$sql_join .= " OR (id_caratteristica = $k AND valore = '$v') ";
			}
			if ($id_category != '') {
				$sql_join .= " AND :id_categoria='{$id_category}'";
			}
			$sql_join .= " GROUP BY id_prodotto HAVING COUNT(*) = $count_field) AS caratt ON caratt.id_prodotto = p.id ";

			switch ($order) {
				case 'prezzo_desc':
					$this->em->setOrder_by('p.sorting, :ordine_cat, :prezzo_vendita DESC');
					break;
				case 'prezzo_asc':
					$this->em->setOrder_by('p.sorting, :ordine_cat, :prezzo_vendita ASC');
					break;
				case 'nome_desc':
					$this->em->setOrder_by('p.sorting, :ordine_cat, :titolo DESC');
					break;
				case 'nome_asc':
					$this->em->setOrder_by('p.sorting, :ordine_cat, :titolo ASC');
					break;
				case 'novita':
					$this->em->setOrder_by('p.sorting, :ordine_cat, :novita DESC');
					break;
				default:
					$this->em->setOrder_by('p.sorting, :ordine_cat, :titolo ASC');
					break;
			}
			$this->em->setWhere($sql_join);
			$this->em->setLimit($limit);
			$this->em->setOffset($offset);
			$this->em->setGroup_by(' :id');
			$this->em->setRepository('O_product');
			$products = $this->em->find();
			return $products;
		}
		return null;
	}

	public function getValueOfFeature($id_caratteristica)
	{
		$this->em->cleanAll();
		$query = "SELECT valore FROM modelli_caratt_valori WHERE id_caratteristica='{$id_caratteristica}' ORDER BY valore ASC";
		$res = $this->em->findByQuery($query);
		return $res;
	}

	/**
	 * Invio email per la richiesta di info su un dato prodotto
	 * @param O_product $product
	 * @param string $msg
	 * @param string $nome
	 * @param string $email_from
	 * @param string $tel
	 * @return bool true|false
	 */
	public static function sendReqInfo($product, $msg, $nome, $email_from, $tel)
	{
		$query = 'SELECT titolo_sito:default_lang AS titolo_sito, destinatario FROM impostazioni LIMIT 1';
		$res = $this->em->findByQuery($query);
		$nomesito = $res[0]['titolo_sito'];
		$email_to = $res[0]['destinatario'];
		$nome_articolo = $product->titolo;
		$codice_articolo = $product->codice;
		$oggetto = "$nomesito - Richiesta informazioni";
		$messaggio = '<div>
                        Da <b>' . $nome . '</b><br />
                        ' . '_telefono' . ' <b>' . $tel . '</b><br />
                        ' . '_email' . ' <b>' . $email_from . '</b><br />
                        ' . '_prod_code' . ': <b>' . $codice_articolo . '</b><br />
                        ' . '_description' . ': <b>' . $nome_articolo . '</b><br />
                        Messaggio inviato:<br />
                        <b>' . $msg . '</b><br />
                    </div>';

		$messaggio = "   <table  width='700px' align='center' style='margin:0 auto;border: 1px solid #333333;'>
                <tr>
                    <td width='600px'>
                        <table class='tabella1'>
                            <thead>
                                <tr>
                                    <th scope='col' style='text-align: left;'>" . '_richiesta_info' . '<br /><br /></th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>' . $messaggio .
				'</td>
                                </tr>
                            </tbody>
                        </table>
                </td>
                </tr>
            </table>';

		$mail_body = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />' . _style . "</head><body>{$messaggio}</body></html>";
		return Mail::send_smtp($email_from, $nome, $email_to, $email_from, $oggetto, $mail_body);
	}

	/**
	 *
	 * @param O_product $product
	 * @param string $email
	 * @param string $email_friend
	 * @param string $msg
	 * @param string $link
	 * @return bool true|false
	 */
	public static function sendToFriend($product, $email, $email_friend, $msg, $link)
	{
		$nome_articolo = $product->titolo;
		$codice_articolo = $product->codice;

		$oggetto = $product->titolo;

		$messaggio = 'Ciao, apri questo link, credo possa interessarti!<br/><br/>
            <a href="' . $link . '">' . $articolo['titolo'] . "</a><br/><br/>$msg";

		$mail_body = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />' . _style . "</head><body>{$messaggio}</body></html>";
		//return self::send_smtp($email, '', $email_friend, $email, $oggetto, $mail_body);
		return Mail::send_smtp($email, '', $email_friend, $email, $oggetto, $mail_body);
	}

	public static function getImgUrl($id, $t = 'prod')
	{
		if ($id != null && $id != '' && $id != false) {
			//$imgpath = WEBROOT . 'module/BlueShop/BlueShop/getImgFromBlob/'.$t.'/';
			$imgpath = 'http://www.desta.it/module/BlueShop/BlueShop/getImgFromBlob/' . $t . '/';
			return $imgpath . $id;
		}
		return '';
	}

	public function getImgContent($id)
	{
		$this->em->cleanAll();
		$this->em->setWhere(":id='{$id}'");
		$this->em->setLimit(1);
		$this->em->setRepository('O_img_product');
		$img = $this->em->find();
		if (!$img) {
			return false;
		}
		return $img[0]->img_big;
	}

	public function getDefaultListino()
	{
		$this->em->cleanAll();
		$this->em->setWhere(":id='1'");
		$this->em->setLimit(1);
		$this->em->setRepository('O_listini');
		$listino = $this->em->find();
		return $listino[0];
	}

	public function getImpostazioni()
	{
		$this->em->cleanAll();
		$this->em->setRepository('O_impostazioni');
		$sito = $this->em->find();
		return $sito[0];
	}

	public function getBreadCrumb($id_cat)
	{
		$breadcrumb = array();
		$categories = $this->getCategories();
		$this->recursiveBreadCrumb($categories, $breadcrumb, $id_cat);
		return array_reverse($breadcrumb);
	}

	private function recursiveBreadCrumb($allcategories, &$breadcrumb, $id_cat)
	{
		$cat_split = explode(',', $id_cat);
		if (count($cat_split) > 0) {
			$id_cat = $cat_split[0];
		}

		for ($i = 0; $i < count($allcategories); $i++) {
			if ($allcategories[$i]->id == $id_cat) {
				$crumb = array();
				$crumb['name'] = $allcategories[$i]->name;
				$crumb['url'] = UrlUtils::getActionUrl('Products/getListProducts/' . $allcategories[$i]->getSeoUrl());
				$crumb['id'] = $id_cat;
				$crumb['parent'] = $allcategories[$i]->id_top_cat;

				array_push($breadcrumb, $crumb);

				if ($allcategories[$i]->id_top_cat != 0) {
					$this->recursiveBreadCrumb($allcategories, $breadcrumb, $allcategories[$i]->id_top_cat);
				}

				break;
			}
		}
	}

	public function getSubCategories($id_cat)
	{
		$rec_cat = array();
		$categories = $this->getCategories();
		$this->recCategories($categories, $rec_cat, $id_cat);
		return $rec_cat;
	}

	private function recCategories($allcategories, &$rec_cat, $id_cat)
	{
		for ($i = 0; $i < count($allcategories); $i++) {
			if ($allcategories[$i]->id_top_cat == $id_cat) {
				$cat = array();
				$cat['name'] = $allcategories[$i]->name;
				if (trim($allcategories[$i]->name) != '-') {
					//$cat['id']=$id_cat;
					$cat['id'] = $allcategories[$i]->id;
					$cat['parent'] = $allcategories[$i]->id_top_cat;

					array_push($rec_cat, $cat);

					if ($allcategories[$i]->id_top_cat != 0) {
						$this->recCategories($allcategories, $rec_cat, $allcategories[$i]->id);
					}
				}
			}
		}
	}

	public function recursiveCategories($allcategories, &$parent_cat, $level, $maxlevel = null)
	{
		if ($maxlevel != null) {
			$maxlevel = (int) $maxlevel;
		}
		for ($i = 0; $i < count($allcategories); $i++) {
			if ($allcategories[$i]->id_top_cat == $parent_cat->id) {
				if (is_null($parent_cat->subcategory)) {
					$parent_cat[$i]->subcategory = array();
				}
				array_push($parent_cat->subcategory, $allcategories[$i]);
				if (!is_null($maxlevel)) {
					if ($level < $maxlevel) {
						$this->recursiveCategories($allcategories, $allcategories[$i], $level + 1, $maxlevel);
					}
				} else {
					$this->recursiveCategories($allcategories, $allcategories[$i], $level + 1, $maxlevel);
				}
			}
		}
	}

	public function getCategoriesLevels($id_cat, $maxlevel, $orderby = '')
	{
		$categories = array();
		$allcategories = $this->getCategories(false, $orderby);

		if ($allcategories !== false) {
			for ($i = 0; $i < count($allcategories); $i++) {
				if ($allcategories[$i]->id_top_cat == $id_cat) {
					array_push($categories, $allcategories[$i]);
					$this->recursiveCategories($allcategories, $allcategories[$i], 1, $maxlevel);
				}
			}
		}

		return $categories;
	}

	public function check($prod_path)
	{
		$allcategories = $this->getCategories();
		$categories = array();
		for ($i = 0; $i < count($allcategories); $i++) {
			$cat[$allcategories[$i]->id] = $allcategories[$i];
		}
		for ($i = 0; $i < count($prod_path) - 1; $i++) {
			$cat = $categories[$prod_path[$i + 1]];
			if ($prod_path[$i] != $cat->id_top_cat) {
				return false;
			}
		}
		return true;
	}

	public function updateCartDetail($det)
	{
		$det->setNew();
		return $this->em->save($det);
	}

	public function getSeoString($obj, $strtitle = '')
	{
		if (is_object($obj) && get_class($obj) == 'O_product') {
			return Utils::url_slug($obj->titolo) . '-' . $obj->id;
		} elseif (is_object($obj) && get_class($obj) == 'O_category') {
			return Utils::url_slug($obj->name) . '-' . $obj->id;
		} elseif (is_numeric($prod) && $strtitle != '') {
			return Utils::url_slug($strtitle) . '-' . $obj;
		}
	}

	public function getIdFromAlias($alias)
	{
		return substr($alias, strrpos($alias, '-') + 1);
	}
}

// PulisciQuery: Restituisce la query pulita da porole inutili(congiunzioni etc.)
function PulisciQuery($queryvar)
{
	// array parole di cui non tener conto nelle ricerche
	$arrayBadWord = array('lo', 'l', 'il', 'la', 'i', 'gli', 'le', 'uno', 'un', 'una', 'un', 'su', 'sul', 'sulla', 'sullo', 'sull', 'in', 'nel', 'nello', 'nella', 'nell', 'con', 'di', 'da', 'dei', 'd', 'della', 'dello', 'del', 'dell', 'che', 'a', 'dal', 'è', 'e', 'per', 'non', 'si', 'al', 'ai', 'allo', 'all', 'al', 'o');
	$queryclean = strtolower($queryvar);
	for ($a = 0; $a < count($arrayBadWord); $a++) {
		// sostituisco bad words con espressioni regolari \b ->solo se parole singole, non facenti parti di altre
		$queryclean = preg_replace("/\b" . $arrayBadWord[$a] . "\b/", '', $queryclean);
	}
	// elimino tutti caratteri non alfanumerici sostituendeli con uno spazio
	$queryclean = preg_replace("/\W/", ' ', $queryclean);
	return $queryclean;
}

// QueryToArray: Restituisce array delle parole chiave da cercare
function QueryToArray($queryvar)
{
	// pulisco query da parole accessorie e caratteri non alfanumerici
	$querypulita = PulisciQuery($queryvar);
	// costruisco l'array contenente tutte le parole da cercare
	$arraySearch = explode(' ', $querypulita);
	// elimino doppioni dall'array
	$arraySearchUnique = array_unique($arraySearch);
	// elimino valori array vuoti o con solo spazi
	$arrayVuoto = array('', ' ');
	$arrayToReturn = array_diff($arraySearchUnique, $arrayVuoto);
	return $arrayToReturn;
}

// CreaQueryRicerca: Creo la query di ricerca.
// peso titolo se non specificato=5, peso testo se non specificato=3
// searchlevel -> 1 o 0. default 1. Se 0 trova parole non complete. Es. cerchi osso?ok anche ossobuco. Se 1 non succede.
function getPertinenceSQL($queryvar, $pesotitolo = 5, $pesotesto = 3, $searchlevel = 1)
{
	$titoloField = "(CASE WHEN (p.titolo:user_lang!='') THEN p.titolo:user_lang ELSE p.titolo:default_lang END)";
	$descrizioneField = "(CASE WHEN (p.descrizione_txt:user_lang!='') THEN p.descrizione_txt:user_lang ELSE p.descrizione_txt:default_lang END)";
	$keywordsField = "(CASE WHEN (p.tag_keywords:user_lang!='') THEN p.tag_keywords:user_lang ELSE p.tag_keywords:default_lang END)";
	$marcaField = "(CASE WHEN (m.marca IS NOT NULL) THEN m.marca ELSE '' END )";

	$pesocodice = 10;
	$pesomarca = 2;
	$pesokeywords = $pesotesto;
	// trasformo la stringa in un array di parole da cercare
	$arrayToFind = QueryToArray($queryvar);
	// numero elementi da cercare
	$elementiToFind = count($arrayToFind);
	// punteggio massimo raggiungibile
	$maxPoint = $elementiToFind * $pesocodice +
			$elementiToFind * $pesotitolo +
			$elementiToFind * $pesotesto +
			$elementiToFind * $pesokeywords +
			$elementiToFind * $pesomarca;

	if ($elementiToFind == 0) {
		return '';
	}
	$query = 'ROUND((';
	$sqlwhere = '';
	// ciclo per ogni parola trovata ($Valore)
	foreach ($arrayToFind as $Indice => $Valore) {
		// se $Valore è presente in titolo instr(titolo, '$Valore') restituirà 1 altrimenti 0
		// moltiplico il valore restituito (1 o 0) per il peso della parola (5 per il titolo, 3 per testo)
		if ($searchlevel == 1) {
			// regexp: uso espressioni regolari. [[:<:]] equivale a \b per separare parole
			$query .= "((p.codice REGEXP '[[:<:]]" . $Valore . "[[:>:]]')>0) * $pesocodice +";
			$query .= "(($titoloField REGEXP '[[:<:]]" . $Valore . "[[:>:]]')>0)*$pesotitolo+";
			$query .= "(($descrizioneField REGEXP '[[:<:]]" . $Valore . "[[:>:]]')>0)*$pesotesto+";
			$query .= "(($keywordsField REGEXP '[[:<:]]" . $Valore . "[[:>:]]')>0)*$pesokeywords+";
			$query .= "(($marcaField REGEXP '[[:<:]]" . $Valore . "[[:>:]]')>0)*$pesomarca+";

		//                                $sqlwhere.="$titoloField REGEXP '[[:<:]]".$Valore."[[:>:]]' OR ";
//                                $sqlwhere.="$descrizioneField REGEXP '[[:<:]]".$Valore."[[:>:]]' OR ";
				//$sqlwhere.="p.titolo_it REGEXP '[[:<:]]".$Valore."[[:>:]]' OR p.descrizione_txt_it REGEXP '[[:<:]]".$Valore."[[:>:]]' OR ";
		} else {
			$query .= "(instr(p.codice, '$Valore') > 0) * $pesocodice +";
			$query .= "(instr($titoloField, '$Valore')>0)*$pesotitolo+";
			$query .= "(instr($descrizioneField, '$Valore')>0)*$pesotesto+";
			$query .= "(instr($keywordsField, '$Valore')>0)*$pesokeywords+";
			$query .= "(instr($marcaField,'$Valore')>0)*$pesomarca+";

//				$sqlwhere.="$titoloField like '%$Valore%' OR $descrizioneField like '%$Valore%' OR ";
		}
	}
//		$sqlwhere=substr($sqlwhere, 0, strlen($sqlwhere)-4);
	// calcolo la percentuale di rilevanza  --> rilevanza*100/$maxPoint
	$query .= "0)*100/$maxPoint,2)";
	return $query;
}
