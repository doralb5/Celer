<?php

class Booking_model extends BaseModel
{
	public function __construct()
	{
		parent::__construct();

		$this->ComponentSettings = BaseComponent::getComponentSettings('Booking', 'Booking');
		if (isset($this->ComponentSettings['db_host'], $this->ComponentSettings['db_name'], $this->ComponentSettings['db_user'], $this->ComponentSettings['db_pass'])
				 
				) {
			$this->setDatabase(new Database('mysql', $this->ComponentSettings['db_host'], $this->ComponentSettings['db_name'], $this->ComponentSettings['db_user'], $this->ComponentSettings['db_pass']));
		}
	}

	public function findRoomsPrices($date_from, $date_end, $guests = 1, $children = 0, $filter = '', $limit = -1, $offset = 0, $sorting = '')
	{
		$this->db->execute("SET @checkin = '$date_from';");
		$this->db->execute("SET @checkout = '$date_end';");
		$this->db->execute("SET @guests = '$guests';");
		$this->db->execute("SET @children = '$children';");

		if ($filter == '') {
			$filter = '1';
		}
		if ($sorting == '') {
			$sorting = 't1.sorting';
		}

		$sql_count_days = "(CASE WHEN date_from = '0000-00-00'
		THEN 
			(DATEDIFF(@checkout, @checkin))
		ELSE
	(
		(CASE WHEN date_from < @checkin AND date_end < @checkout THEN (DATEDIFF(date_end, @checkin)+1) ELSE 0 END)
		+
		(CASE WHEN date_from >= @checkin AND date_end <= @checkout THEN (DATEDIFF(date_end, date_from)) ELSE 0 END)
		+
		(CASE WHEN date_from > @checkin AND date_end > @checkout THEN (DATEDIFF(@checkout, date_from)) ELSE 0 END)
		+
		(CASE WHEN date_from <= @checkin AND date_end >= @checkout THEN (DATEDIFF(@checkout, @checkin)) ELSE 0 END)
	) END)";

		$q = "SELECT t1.*, t1.id AS room_type_id, cms_BookingPlace.name AS place, t1.name AS type, @checkin AS checkin, @checkout AS checkout, @guests AS guests, @children AS children,
($sql_count_days) AS nights,
SUM($sql_count_days * price_per_day) AS base_price,
SUM($sql_count_days * price_per_additional_guest * (@guests - 1)) AS total_additional_price,
SUM($sql_count_days * price_per_child * (@children)) AS total_children_price,
(SUM($sql_count_days * price_per_day) + SUM($sql_count_days * price_per_additional_guest * (@guests - 1)) + SUM($sql_count_days * price_per_child * @children)) AS final_price,
(CASE WHEN SUM($sql_count_days * price_per_day) is null OR SUM($sql_count_days * price_per_day) = '0' THEN '1' ELSE '0' END) AS no_price,

(
SELECT GROUP_CONCAT(timages.filename ORDER BY timages.is_default DESC , timages.sorting ASC SEPARATOR ';') FROM cms_BookingRoomTypeImage AS timages WHERE timages.room_type_id = t1.id
) AS images

FROM cms_BookingRoomType t1
INNER JOIN cms_BookingPlace ON cms_BookingPlace.id = t1.place_id
LEFT JOIN (
	SELECT cms_BookingRoomPrice.*
            FROM cms_BookingRoomPrice
            WHERE
                cms_BookingRoomPrice.date_from <= @checkin AND date_end > @checkin
            OR cms_BookingRoomPrice.date_end > @checkout AND cms_BookingRoomPrice.date_from <= @checkout
                OR cms_BookingRoomPrice.date_from = '0000-00-00'
        ) AS t2 ON t1.id = t2.room_type_id

WHERE @guests + @children <= sleeps AND $filter
GROUP BY t1.id
ORDER BY $sorting";

		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingAvailRoomPrice');
		$this->em->loadByQuery($q);

		$RoomsPrices = $this->em->getEntities();

		$RoomAvail = $this->getRoomAvailability($date_from, $date_end, $guests, $children, '', true);

		foreach ($RoomsPrices as &$room) {
			$images = explode(';', $room->images);
			$arr_img = array();
			foreach ($images as $img) {
				$arr_img[] = $img;
			}
			$room->images = $arr_img;
			$room->avail_qty = $RoomAvail[$room->id]['avail_qty'];
		}

		return $RoomsPrices;
	}

	public function getRoomAvailability($date_from, $date_end, $guests = 1, $children = 0, $filter = '', $output_array = false)
	{
		$this->db->execute("SET @checkin = '$date_from';");
		$this->db->execute("SET @checkout = '$date_end';");
		$this->db->execute("SET @guests = '$guests';");
		$this->db->execute("SET @children = '$children';");

		if ($filter == '') {
			$filter = '1';
		}
		$sorting = 'cms_BookingRoomType.sorting';

		$q = "SELECT cms_BookingRoomType.*, COUNT(DISTINCT broom.id) AS tot_qty, (SUM(CASE WHEN (cms_BookingStatus.available = '0'
            AND (@checkin BETWEEN booking.reservation_start AND booking.reservation_end
            OR @checkout BETWEEN booking.reservation_start AND booking.reservation_end)) THEN 1 ELSE 0 END )) AS busy_qty,
            (COUNT(DISTINCT broom.id) - (SUM(CASE WHEN (cms_BookingStatus.available = '0'
            AND (@checkin BETWEEN booking.reservation_start AND booking.reservation_end
            OR @checkout BETWEEN booking.reservation_start AND booking.reservation_end)) THEN 1 ELSE 0 END ))) AS avail_qty
FROM cms_BookingRoom AS broom 
INNER JOIN cms_BookingRoomType ON cms_BookingRoomType.id = broom.room_type_id
LEFT JOIN cms_Booking AS booking ON broom.id = booking.room_id 
LEFT JOIN cms_BookingStatus ON cms_BookingStatus.id = booking.status_id
WHERE @guests  + @children <= sleeps AND $filter
GROUP BY broom.room_type_id
ORDER BY $sorting;";

		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomAvail');
		$this->em->loadByQuery($q);

		$availRooms = $this->em->getEntities();

		if ($output_array) {
			$array = array();
			foreach ($availRooms as $r) {
				$array[$r->id] = Utils::ObjectToArray($r);
			}
			return $array;
		}

		return $availRooms;
	}

	public function getDefaultBookingStatus()
	{ //TODO: completare....
		return 1;
	}

	public function getAccomodations($filter = '', $limit = -1, $offset = 0, $sorting = '')
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingAccomodation');
		$this->em->loadByFilter($filter, $limit, $offset, $sorting);
		return $this->em->getEntities();
	}

	public function getMaxGuests()
	{
		$result = $this->db->select(' SELECT MAX(max_guests) AS max_guests FROM cms_BookingAccomodation ');
		if (count($result)) {
			return intval($result[0]['max_guests']);
		}
		return false;
	}

	public function getRoomTypes($id_place = null)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomType');
		$sorting = 'sorting, name';
		$filter = (!is_null($id_place)) ? "place_id='$id_place'" : '';
		$this->em->loadByFilter($filter, -1, 0, $sorting);
		$roomtypes = $this->em->getEntities();
		return $roomtypes;
	}

	public function getRoomImagesByType($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoomTypeImage');
		$filter = 'room_type_id=' . $id;
		$this->em->loadByFilter($filter);
		$res = $this->em->getEntities();
		return $res;
	}

	public function getRoom($id)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('BookingRoom');
		$this->em->loadById($id);
		$room = $this->em->getEntities();
		return (count($room)) ? $room[0] : null;
	}

	public function getBookingList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('Booking');
		$this->em->loadByFilter($limit, $offset, $filter, $order);
		$res = $this->em->getEntities();
		return $res;
	}

	public function saveBooking($booking)
	{
		$this->em->setRepository('Booking');
		$this->em->setEntity('Booking');
		if ($booking->id == '') {
			$booking->created_time = date('Y-m-d H:i');
		}
		$res = $this->em->persist($booking);
		return $res;
	}
}
