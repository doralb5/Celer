<?php

require_once DOCROOT . ENTITIES_PATH . 'Business/Business.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessOffer.php';
require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessOfferImages.php';

class Businesses_Model extends BaseModel
{
	private $lastCounter;
	public static $lastBusinessResult;
	public $ComponentSettings;

	public function __construct()
	{
		parent::__construct();

		$this->ComponentSettings = BaseComponent::getComponentSettings('Businesses', 'Businesses');
		if (isset($this->ComponentSettings['db_host'], $this->ComponentSettings['db_name'], $this->ComponentSettings['db_user'], $this->ComponentSettings['db_pass'])
				 
				) {
			$this->setDatabase(new Database('mysql', $this->ComponentSettings['db_host'], $this->ComponentSettings['db_name'], $this->ComponentSettings['db_user'], $this->ComponentSettings['db_pass']));
		}
	}

	private static function setBusinessArray($array)
	{
		self::$lastBusinessResult = $array;
	}

	public static function getBusinessArray()
	{
		return self::$lastBusinessResult;
	}

	public function getComments($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessRating');
		$busRat_tbl = TABLE_PREFIX . 'BusinessRating';
		$sql = "SELECT * FROM $busRat_tbl WHERE id_biz=" . $id . " AND enabled = '1' ORDER BY publish_date DESC";
		$result = $this->db->select($sql);
		return $result;
	}

	public function getLastComments($filter = '', $limit = 30, $offset = 0, $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessRating');
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$res = $this->em->getEntities();
		return $res;
	}

	public function saveBusinessOffer($offer)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOffer');
		$res = $this->em->persist($offer);
		return $res;
	}

	public function getPaymentMethodsOfBusiness($id_business)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessPaymentsMethod');
		$this->em->loadByFilter("id_business='$id_business'");
		$current_payments = $this->em->getEntities();
		return $current_payments;
	}

	public function savePaymentMethod($id_business, $payment_ids = array())
	{
		$current_payments = $this->getPaymentMethodsOfBusiness($id_business);
		foreach ($current_payments as $currPay) {
			if (!in_array($currPay->id_payment, $payment_ids)) {
				echo "deleting $currPay->id";
				$this->em->delete('BusinessPaymentsMethod', $currPay->id);
			}
		}

		foreach ($payment_ids as $newPaym) {
			if (!Utils::in_ObjectArray($current_payments, 'id_payment', $newPaym)) {
				$paymentmethod = new BusinessPaymentsMethod_Entity;
				$paymentmethod->id_business = $id_business;
				$paymentmethod->id_payment = $newPaym;
				$this->em->persist($paymentmethod);
			}
		}
	}

	public function getOpenHours($id = null)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOpenHours');
		$this->em->loadByFilter("business_id='$id'", -1, 0, 'day');
		$res = $this->em->getEntities();
		return $res;
	}

	public function saveOpeningHours($openhours)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOpenHours');
		$res = $this->em->persist($openhours);
		return $res;
	}

	public function getPayMethods()
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('tabBusinessPayMethod');
		$this->em->loadByFilter();
		$payments = $this->em->getEntities();
		return $payments;
	}

	public function getOffer($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOffer');
		$this->em->loadById($id);
		$offer = $this->em->getEntities();
		$images = explode(',', $offer[0]->allimages);
		$imgkey = array();
		$imgvalue = array();
		foreach ($images as $key => $img) {
			if ($key % 2 == 0) {
				$imgvalue[] = $img;
			} else {
				$imgkey[] = $img;
			}
		}
		$images_with_id = array_combine($imgkey, $imgvalue);
		$offer[0]->images = (count($images_with_id) > 0 && $images_with_id != '') ? $images_with_id : array();
		return (count($offer)) ? $offer[0] : null;
	}

	public function deleteOffer($id = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOffer');
		$res = $this->em->delete('BusinessOffer', $id);
		return $res;
	}

	public function getOfferList($filter = '', $limit = 30, $offset = 0, $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOffer');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		if (count($result)) {
			$this->lastCounter = count($result);
		}

		foreach ($result as $key => &$res) {
			$images = explode(',', $res->allimages);
			$imgkey = array();
			$imgvalue = array();
			foreach ($images as $key => $img) {
				if ($key % 2 == 0) {
					$imgvalue[] = $img;
				} else {
					$imgkey[] = $img;
				}
			}
			if (count($imgkey) == count($imgvalue)) {
				$images_with_id = array_combine($imgkey, $imgvalue);
			} else {
				$images_with_id = array();
			}
			$res->images = (count($images_with_id) > 0 && $images_with_id != '') ? $images_with_id : array();
		}
		return $result;
	}

	public function getOfferImageById($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOfferImages');

		$this->em->loadById($id);
		$result = $this->em->getEntities();
		return $result[0];
	}

	public function getOfferImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOfferImages');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		//$BusinessImage_Table = TABLE_PREFIX . BusinessImage_Entity::TABLE_NAME;
		//$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $BusinessImage_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ""));
		//$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function deleteOfferImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOfferImages');
		$res = $this->em->delete('BusinessOfferImages', $id);
		return $res;
	}

	public function updateCoverImage($id, $file)
	{
		$col_update = array('cover_image' => $file);
		$result = $this->db->update('cms_Subdomain', $col_update, "target_id = {$id} AND type = 'B' ");
		if (count($result)) {
			return true;
		}
		return false;
	}

	public function getBusinessOpenHours($id_business = '')
	{

		//$BusinessOpenHours_Table = TABLE_PREFIX . BusinessOpenHours_Entity::TABLE_NAME;

		if (is_numeric($id_business) && $id_business != '') {
			$filter = ' business_id  = ' . $id_business;
		} else {
			return false;
		}
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOpenHours');
		$order = 'day';
		$this->em->loadByFilter($filter, 21, $offset = 0, $order = '');
		$result = $this->em->getEntities();
		return $result;
	}

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$Business_Table = TABLE_PREFIX . Business_Entity::TABLE_NAME;
		$BC_Table = TABLE_PREFIX . BusinessCateg_Entity::TABLE_NAME;

		$this->em->setRepository('Business');
		$this->em->setEntity('Business');

		if (isset($this->ComponentSettings['filter_city']) && CMSSettings::$webdomain != 'cittanelweb.it') {
			if ($filter != '') {
				$filter .= ' AND ';
			}
			$filter .= ' ' . $Business_Table . ".location = '" . $this->ComponentSettings['filter_city'] . "' ";
		}

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		//echo "<!-- "; var_dump($this->em->errorInfo()); echo "-->";

		$this->lastCounter = $this->em->countByFilter($filter);

		//echo "<!--Result count: " . count($result) . "-->";
		//        $tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Business_Table
		//                      LEFT JOIN $BC_Table ON $BC_Table.id_business = $Business_Table.id
		//                      WHERE 1 " . (($filter != '') ? "AND $filter" : ""));
//
		//        $this->lastCounter = $tot_count[0]['tot'];

		foreach ($result as &$res) {
			$res->images = $this->getBusinessImages(100, 0, "id_business = {$res->id}");
			$res->categories = $this->getBusinessCategories(100, 0, "id_business = {$res->id}");
		}

		self::setBusinessArray($result);
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	/**
	 * Return Business Entity or Null if id not exists.
	 * @param type $id
	 * @return Business_Entity | null
	 */
	public function getBusiness($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');
		$this->em->loadById($id);
		$Business = $this->em->getEntities();
		if (count($Business)) {
			$Business[0]->images = $this->getBusinessImages(100, 0, "id_business = {$Business[0]->id}");
			$Business[0]->categories = $this->getBusinessCategories(100, 0, "id_business = {$Business[0]->id}");
			$Business[0]->open_hours = $this->getBusinessOpenHours($Business[0]->id);
		}
		self::setBusinessArray($Business);
		return (count($Business)) ? $Business[0] : null;
	}

	public function saveBusiness($Business)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');
		$res = $this->em->persist($Business);
		return $res;
	}

	public function deleteBusiness($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');
		$res = $this->em->delete('Business', $id);
		return $res;
	}

	public function search($keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');

		require_once DOCROOT . LIBS_PATH . 'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Business_Entity::getSelectQueryObj($this->db);
		if ($sorting != '') {
			$q->orderBy($sorting);
		}
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter, 0);
		//echo "<!--" . $query->getQuery() . "-->";

		$this->lastCounter = $this->em->countByQuery($query->getQuery());

		$query->limit($limit);
		$query->offset($offset);
		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		foreach ($res as &$item) {
			$item->image_url = $this->ComponentSettings['image_baseurl'] . Utils::genMediaUrl('businesses/' . $item->image);
		}
		self::setBusinessArray($res);
		return $res;
	}

	public function getCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCategory');

		if ($order == '') {
			$order = 'parent_id DESC, category ASC';
		}

		$locationFilter = '';
		if (isset($this->ComponentSettings['filter_city']) && CMSSettings::$webdomain != 'cittanelweb.it') {
			$locationFilter = " AND location = '" . $this->ComponentSettings['filter_city'] . "' ";
		}

		//        $this->em->loadByFilter($filter, $limit, $offset, $order);

		$q = "SELECT cms_BusinessCategory.*, COUNT(DISTINCT biz_id) AS total_businesses
FROM 
cms_BusinessCategory LEFT JOIN
(
SELECT id_category, b1.id AS biz_id, enabled, publish_date, expire_date, location
FROM cms_Business AS b1
WHERE (enabled = '1' AND publish_date < NOW() AND expire_date > NOW() $locationFilter )

UNION DISTINCT

SELECT cms_BusinessCateg.id_category AS id_category, cms_BusinessCateg.id_business AS biz_id, b2.enabled, b2.publish_date, b2.expire_date, b2.location
FROM cms_Business AS b2
INNER JOIN cms_BusinessCateg ON cms_BusinessCateg.id_business = b2.id
WHERE (enabled = '1' AND publish_date < NOW() AND expire_date > NOW() $locationFilter )

) AS t1 ON cms_BusinessCategory.id = t1.id_category "
				. (($filter != '') ? " WHERE $filter" : '')
				. 'GROUP BY cms_BusinessCategory.id '
				. (($order != '') ? " ORDER BY $order" : '');

		$limitstr = (($limit != -1) ? " LIMIT $offset, $limit" : '');
		//echo $q . $limitstr;
		$this->em->loadByQuery($q . $limitstr);
		$result = $this->em->getEntities();

		$this->lastCounter = $this->em->countByQuery($q);

//
		//        $BusinessCategory_Table = TABLE_PREFIX . BusinessCategory_Entity::TABLE_NAME;
		//        $tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $BusinessCategory_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ""));
		//        $this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getCategory($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCategory');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveCategory($category)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCategory');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCategory');
		$res = $this->em->delete('BusinessCategory', $id);
		return $res;
	}

	public function getBusinessImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessImage');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		//$BusinessImage_Table = TABLE_PREFIX . BusinessImage_Entity::TABLE_NAME;
		//$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $BusinessImage_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ""));
		//$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getBusinessImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessImage');
		$this->em->loadById($id);
		$evImage = $this->em->getEntities();
		return (count($evImage)) ? $evImage[0] : null;
	}

	public function saveBusinessImage($evImage)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessImage');
		$res = $this->em->persist($evImage);
		return $res;
	}

	public function deleteBusinessImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessImage');
		$res = $this->em->delete('BusinessImage', $id);
		return $res;
	}

	public function getMostClickedCateg($limit, $offset = 0)
	{
		$Business_Tbl = TABLE_PREFIX . 'Business';
		$BCateg_Tbl = TABLE_PREFIX . 'BusinessCategory';

		$sql = "SELECT categ.id, categ.category, COUNT(bus.id) AS quantity
                FROM $BCateg_Tbl categ
                LEFT JOIN $Business_Tbl bus ON bus.id_category = categ.id
                GROUP BY bus.id_category
                ORDER BY COUNT(bus.id_category) DESC, categ.category
                LIMIT $offset, $limit";
		$result = $this->db->select($sql);
		return $result;
	}

	public function getBusinessCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCateg');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getBusinessByUserId($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');
		$this->em->loadByFilter('id_user =' . $id);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getBusinessCategory($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCateg');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveBusinessCategory($category)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCateg');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteBusinessCategory($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessCateg');
		$res = $this->em->delete('BusinessCateg', $id);
		return $res;
	}

	public function getStatictics($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessStatistic');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getStatistic($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessStatistic');
		$this->em->loadById($id);
		$stat = $this->em->getEntities();
		return (count($stat)) ? $stat[0] : null;
	}

	public function saveStatistic($stat)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessStatistic');
		$res = $this->em->persist($stat);
		return $res;
	}

	public function deleteStatistic($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessStatistic');
		$res = $this->em->delete('BusinessStatistic', $id);
		return $res;
	}

	public function getBusStatictics($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessStatistics');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getBusStatistic($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessStatistics');
		$this->em->loadById($id);
		$stat = $this->em->getEntities();
		return (count($stat)) ? $stat[0] : null;
	}

	public function saveBusStatistic($stat)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessStatistics');
		$res = $this->em->persist($stat);
		return $res;
	}

	public function deleteBusStatistic($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessStatistics');
		$res = $this->em->delete('BusinessStatistics', $id);
		return $res;
	}

	public function incrementStatistic($id_business, $type)
	{
		$BStat_tbl = TABLE_PREFIX . 'BusinessStatistics';
		switch ($type) {
			case 'impression':
				$sql = "UPDATE $BStat_tbl SET impressions = impressions + 1 WHERE id_business = {$id_business}";
				break;
			case 'view':
				$sql = "UPDATE $BStat_tbl SET views = views + 1 WHERE id_business = {$id_business}";
				break;
			case 'subdomain_view':
				$sql = "UPDATE $BStat_tbl SET subdomain_views = subdomain_views + 1 WHERE id_business = {$id_business}";
				break;
			case 'message':
				$sql = "UPDATE $BStat_tbl SET messages = messages + 1 WHERE id_business = {$id_business}";
				break;
			case 'subdomain_message':
				$sql = "UPDATE $BStat_tbl SET subdomain_messages = subdomain_messages + 1 WHERE id_business = {$id_business}";
				break;
			case 'website_click':
				$sql = "UPDATE $BStat_tbl SET website_clicks = website_clicks + 1 WHERE id_business = {$id_business}";
				break;
			case 'facebook_click':
				$sql = "UPDATE $BStat_tbl SET facebook_clicks = facebook_clicks + 1 WHERE id_business = {$id_business}";
				break;
			case 'linkedin_click':
				$sql = "UPDATE $BStat_tbl SET linkedin_clicks = linkedin_clicks + 1 WHERE id_business = {$id_business}";
				break;
			case 'instagram_click':
				$sql = "UPDATE $BStat_tbl SET instagram_clicks = instagram_clicks + 1 WHERE id_business = {$id_business}";
				break;
			case 'featured_impression':
				$sql = "UPDATE $BStat_tbl SET featured_impressions = featured_impressions + 1 WHERE id_business = {$id_business}";
				break;
			case 'featured_click':
				$sql = "UPDATE $BStat_tbl SET featured_clicks = featured_clicks + 1 WHERE id_business = {$id_business}";
				break;
		}

		$res = $this->db->execute($sql);
		return $res;
	}

	private function hierarchyCategory($category, &$category_name)
	{
		if (!is_null($category->parent_id)) {
			$categ = $this->getCategory($category->parent_id);
			$category_name = $categ->category . ' <b>&raquo</b> ' . $category_name;
			$this->hierarchyCategory($categ, $category_name);
		} else {
			return;
		}
	}

	public function saveMessage($business_id, $name, $email, $tel, $message)
	{
		require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessMessage.php';

		$msg = new BusinessMessage_Entity();
		$msg->id_business = $business_id;
		$msg->name = $name;
		$msg->email = $email;
		$msg->tel = $tel;
		$msg->fromip = $_SERVER['REMOTE_ADDR'];
		$msg->useragent = $_SERVER['HTTP_USER_AGENT'];
		$msg->message = $message;

		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessMessage');
		$res = $this->em->persist($msg);

		return $res;
	}

	public function getMessage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessMessage');
		$this->em->loadById($id);
		$message = $this->em->getEntities();
		return (count($message)) ? $message[0] : null;
	}

	public function setMessageRead($id, $readed = true)
	{
		require_once DOCROOT . ENTITIES_PATH . 'Business/BusinessMessage.php';

		$msg = $this->getMessage($id);
		if (!is_null($msg)) {
			$msg->readed = ($readed === true) ? '1' : '0';
			$this->em->setRepository('Business');
			$this->em->setEntity('BusinessMessage');
			$res = $this->em->persist($msg);
			return $res;
		}

		return false;
	}

	public function saveBusinessImported($Business)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessImported');
		$res = $this->em->persist($Business);
		return $res;
	}

	public function saveOfferImage($image)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOfferImages');
		$res = $this->em->persist($image);
		return $res;
	}

	public function getBusinessOfferImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOfferImages');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		return $result;
	}

	public function getBusinessOffer($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessOffer');
		$this->em->loadById($id);
		$BusinessOffer = $this->em->getEntities();
		if (count($BusinessOffer)) {
			$BusinessOffer[0]->images = $this->getBusinessOfferImages(100, 0, " id_biz = {$BusinessOffer[0]->id}");
		}
		return (count($BusinessOffer)) ? $BusinessOffer[0] : null;
	}

	public function getProfiles($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessProfile');

		if ($order == '') {
			$order = 'id';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$BusinessProf_Table = TABLE_PREFIX . BusinessProfile_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $BusinessProf_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];

		return $result;
	}

	public function getProfile($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessProfile');
		$this->em->loadById($id);
		$profile = $this->em->getEntities();
		return (count($profile)) ? $profile[0] : null;
	}

	public function getAlbanianLocations()
	{
		$locations = array('Shkoder', 'Tropoje', 'Puke', 'Kukes', 'Lezhe', 'Mirdite', 'Diber', 'Kurbin', 'Mat', 'Kruje', 'Bulqize', 'Durres', 'Tirane', 'Kavaje', 'Elbasan', 'Librazhd', 'Gramsh', 'Fier', 'Lushnje', 'Berat', 'Skrapar', 'Pogradec', 'Korce', 'Kolonje', 'Vlore', 'Tepelene', 'Permet', 'Gjirokaster', 'Sarande');
		return $locations;
	}

	public function getItalianLocations()
	{
		$locations = array('Vibo Valentia');
		return $locations;
	}

	public function saveTempImage($temp_image)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		$res = $this->em->persist($temp_image);
		return $res;
	}

	public function getTempImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		$this->em->loadById($id);
		$Image = $this->em->getEntities();
		return (count($Image)) ? $Image[0] : null;
	}

	public function deleteTempImage($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		$res = $this->em->delete('TempImage', $id);
		return $res;
	}

	public function getTempImages($limit = 999, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('TempImage');
		if ($order == '') {
			$order = 'id';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getSubDomain($filter = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		$this->em->loadByFilter($filter);
		$result = $this->em->getEntities();
		return count($result) ? $result[0] : null;
	}

	public function getSubdomainByName($subdomain = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		if (isset($this->ComponentSettings['db_name'])) {
			$subdom_tbl = $this->ComponentSettings['db_name'] . '.' . TABLE_PREFIX . 'Subdomain';
		} else {
			$subdom_tbl = DB_NAME . '.' . TABLE_PREFIX . 'Subdomain';
		}
		$sql = "SELECT COUNT('subdomain') as count FROM $subdom_tbl WHERE subdomain='" . $subdomain . "'";
		$result = $this->db->select($sql, array(':subdomain' => $subdomain));
		return count($result) ? $result[0] : null;
	}

	public function getSubdomainBySubdomain($subdomain = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		if (isset($this->ComponentSettings['db_name'])) {
			$subdom_tbl = $this->ComponentSettings['db_name'] . '.' . TABLE_PREFIX . 'Subdomain';
		} else {
			$subdom_tbl = DB_NAME . '.' . TABLE_PREFIX . 'Subdomain';
		}
		$sql = "SELECT *  FROM $subdom_tbl WHERE subdomain='" . $subdomain . "' or subdomain = SUBSTRING_INDEX('" . $subdomain . "' , '.' , -2) or subdomain = SUBSTRING_INDEX('" . $subdomain . "' , '.' , -3)";
		//var_dump($sql);
		$result = $this->db->select($sql, array(':subdomain' => $subdomain));
		return count($result) ? $result[0] : null;
	}

	public function saveSubdomain($subdomain)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		$res = $this->em->persist($subdomain);
		return $res;
	}

	public function deleteSubdomain($id)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		$res = $this->em->delete('Subdomain', $id);
		return $res;
	}

	public function saveRating($rating)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessRating');
		return $this->em->persist($rating);
	}

	public function getRating($filter = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessRating');
		$this->em->loadByFilter($filter);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getRatingValue($id_biz)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessRating');
		$busRat_tbl = TABLE_PREFIX . 'BusinessRating';
		$sql = "SELECT COUNT('id_biz') as count, SUM(rating) as sum FROM $busRat_tbl WHERE id_biz=:id_biz";
		$result = $this->db->select($sql, array(':id_biz' => $id_biz));
		return count($result) ? $result[0] : null;
	}

	public function saveStickerRecord($business_sticker)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessSticker');
		return $this->em->persist($business_sticker);
	}

	public function checkTokenSticker($token)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessSticker');
		$filter = "token='" . $token . "'";
		$this->em->loadByFilter($filter, 1, 0);
		$result = $this->em->getEntities();

		return $result[0];
	}

	public function getStickerByBusinessID($id_business = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('BusinessSticker');
		$filter = "id_business='" . $token . "'";
		$this->em->loadByFilter($filter, 1, 0);
		$result = $this->em->getEntities();
		return $result[0];
	}

	public function getMessages($id)
	{
		$this->em->setRepository('Business'); //runs till here!
		$this->em->setEntity('BusinessMessage');
		$busMsg_tbl = TABLE_PREFIX . 'BusinessMessage';
		$sql = "SELECT * FROM $busMsg_tbl WHERE id_business= " . $id . ' ORDER BY creation_date DESC';
		$result = $this->db->select($sql);
		return $result;
	}

	//    public function getMessage($id) {
	//        $this->em->setRepository('Business');
	//        $this->em->setEntity('BusinessMessage');
	//        $this->em->loadById($id);
	//        $message = $this->em->getEntities();
	//        return (count($message)) ? $message[0] : null;
	//    }

	public function getEmailByName($subdomain = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Business');
		$biz_tbl = DB_NAME . '.' . TABLE_PREFIX . 'Business';
		$sql = "SELECT COUNT('subdomain') as count FROM $biz_tbl WHERE business_mail ='" . $subdomain . "'";
		$result = $this->db->select($sql, array(':subdomain' => $subdomain));
		return count($result) ? $result[0] : null;
	}

	public function getSubdomainByBizId($biz = '')
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		$subdom_tbl = DB_NAME . '.' . TABLE_PREFIX . 'Subdomain';
		$sql = "SELECT COUNT('subdomain') as count FROM $subdom_tbl WHERE target_id='" . $biz . "'";
		$result = $this->db->select($sql, array(':subdomain' => $subdomain));
		return count($result) ? $result[0] : null;
	}

	public function getImageBaseUrl($thumbs = false)
	{
		$image_baseurl = $this->ComponentSettings['image_baseurl'];
		if ($image_baseurl == '') {
			if (!$thumbs) {
				return Utils::genMediaUrl('businesses', true) . '/';
			}
			
			return Utils::genMediaUrl('thumbs/businesses', true) . '/';
		}
		if (!$thumbs) {
			return $image_baseurl;
		}
	}
}
