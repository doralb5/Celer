<?php

require_once DOCROOT . ENTITIES_PATH . 'Business/Subdomain.php';

class Businesses_sub_Model extends Businesses_Model
{
	public $ComponentSettings;

	public function __construct()
	{
		parent::__construct();

		$this->ComponentSettings = BaseComponent::getComponentSettings('Businesses', 'Businesses');
		if (isset($this->ComponentSettings['db_host'], $this->ComponentSettings['db_name'], $this->ComponentSettings['db_user'], $this->ComponentSettings['db_pass'])
				 
				) {
			$this->setDatabase(new Database('mysql', $this->ComponentSettings['db_host'], $this->ComponentSettings['db_name'], $this->ComponentSettings['db_user'], $this->ComponentSettings['db_pass']));
		}
	}
}
