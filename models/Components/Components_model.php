<?php

require_once DOCROOT . ENTITIES_PATH . 'Component/Component.php';
require_once DOCROOT . ENTITIES_PATH . 'User/User.php';
require_once DOCROOT . ENTITIES_PATH . 'User/UserPermission.php';

class Components_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');

		if ($order == '') {
			$order = 'sorting,id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Component_Table = TABLE_PREFIX . Component_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Component_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getComponentByName($name)
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');
		$this->em->loadByFilter("name = '$name'");
		$component = $this->em->getEntities();
		return (count($component)) ? $component[0] : null;
	}

	public function getComponent($id)
	{
		$this->em->setRepository('Component');
		$this->em->setEntity('Component');
		$this->em->loadById($id);
		$component = $this->em->getEntities();
		return (count($component)) ? $component[0] : null;
	}
}
