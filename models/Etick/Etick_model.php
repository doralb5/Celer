<?php

class Etick_Model extends BaseModel
{
	private $lastCounter;

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getEvents($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickEvent');
		if ($order == '') {
			$order = ' start_date DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getEvent($id = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickEvent');
		$this->em->loadById($id);
		$event = $this->em->getEntities();
		return (count($event)) ? $event[0] : null;
	}

	public function saveEvent($event)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickEvent');
		$res = $this->em->persist($event);
		//print_r($res);exit;
		return $res;
	}

	public function deleteEvent($id)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickEvent');
		$res = $this->em->delete('EtickEvent', $id);
		return $res;
	}

	public function getPlaces($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPlace');
		if ($order == '') {
			$order = ' name ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getPlace($id)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPlace');
		$this->em->loadById($id);
		$place = $this->em->getEntities();
		return (count($place)) ? $place[0] : null;
	}

	public function savePlace($place)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPlace');
		$res = $this->em->persist($place);
		//print_r($res);exit;
		return $res;
	}

	public function saveSeat($seat)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickSeat');
		$res = $this->em->persist($seat);
		//print_r($res);exit;
		return $res;
	}

	public function getPrices($limit = 300, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPrice');
		$order = ' price ASC';
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getZones($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickZona');
		if ($order == '') {
			$order = ' name ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getReservationBySeatName($seat_name, $id_event, $user_category = null)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReservation');
		EtickReservation_entity::setUserCategory($user_category);
		$filter = "seat.element_id = '" . addslashes($seat_name) . "' AND eve.id = '$id_event' ";
		$this->em->loadByFilter($filter, 1, 0);
		$result = $this->em->getEntities();
		if (count($result) === 1) {
			return $result[0];
		}
		 
		return $result;
	}

	public function getReservations($limit = 30, $offset = 0, $filter = '', $order = '', $user_category = null)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReservation');
		EtickReservation_entity::setUserCategory($user_category);
		if ($order == '') {
			$order = 'seat.name';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getReservation($id = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReservation');
		$this->em->loadById($id);
		$reservation = $this->em->getEntities();
		return (count($reservation)) ? $reservation[0] : null;
	}

	public function saveReservation($reservation)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReservation');
		$res = $this->em->persist($reservation);
		return $res;
	}

	public function getUserCategories($limit = 50, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserCategory');
		if ($order == '') {
			$order = ' category ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getSeats($limit = 9999, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickSeat');
		if ($order == '') {
			$order = ' name ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function deleteReservation($id)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReservation');
		$res = $this->em->delete('EtickReservation', $id);
		return $res;
	}

	public function getReserveTimeRules($limit = -1, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReserveTime');
		if ($order == '') {
			$order = ' id ASC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		return $result;
	}

	public function getReserveTimeRule($id = '')
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReserveTime');
		$this->em->loadById($id);
		$reservation = $this->em->getEntities();
		return (count($reservation)) ? $reservation[0] : null;
	}

	public function ArchiveReservation($reservation)
	{
		require_once ENTITIES_PATH . 'Etick/EtickReservationArchive.php';
		$archive_reservation = new EtickReservationArchive_entity();
		$archive_reservation->confirmed = $reservation->confirmed;
		$archive_reservation->id_user = $reservation->id_user;
		$archive_reservation->id_event = $reservation->id_event;
		$archive_reservation->id_seat = $reservation->id_seat;
		$archive_reservation->creation_date = $reservation->creation_date;
		$archive_reservation->price = $reservation->price;
		$archive_reservation->note = $reservation->note;
		$archive_reservation->delete_date = date('Y-m-d H:i:s', time());
		$archive_reservation->reservation_id = $reservation->id;
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickReservationArchive');
		$res = $this->em->persist($archive_reservation);
		if (!is_array($res)) {
			$this->em->setRepository('Etick');
			$this->em->setEntity('EtickReservation');
			$deleted = $this->em->delete('EtickReservation', $reservation->id);
			return $deleted;
		}
		return $res;
	}

	public function savePrice($price)
	{
		$this->em->setRepository('Etick');
		$this->em->setEntity('EtickPrice');
		$res = $this->em->persist($price);
		return $res;
	}
}
