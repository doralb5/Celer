<?php

require_once DOCROOT . ENTITIES_PATH . 'Event/Event.php';

class Events_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('Event');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Event_Table = TABLE_PREFIX . Event_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Event_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getEvent($id)
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('Event');
		$this->em->loadById($id);
		$Event = $this->em->getEntities();
		return (count($Event)) ? $Event[0] : null;
	}

	public function saveEvent($Event)
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('Event');
		$res = $this->em->persist($Event);
		return $res;
	}

	public function deleteEvent($id)
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('Event');
		$res = $this->em->delete('Event', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('Event');

		require_once DOCROOT . LIBS_PATH . 'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Event_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('EventCategory');

		if ($order == '') {
			$order = 'parent_id DESC, id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$EventCategory_Table = TABLE_PREFIX . EventCategory_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $EventCategory_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];

		//Marrim dhe numrin e dokumenteve
		$Event_Table = TABLE_PREFIX . Event_Entity::TABLE_NAME;
		foreach ($result as &$categ) {
			$tot_ser = $this->db->select("SELECT COUNT(*) AS total FROM $Event_Table WHERE id_category = :categ", array(':categ' => $categ->id));
			$categ->total_events = $tot_ser[0]['total'];
			$this->hierarchyCategory($categ, $categ->category);
		}

		return $result;
	}

	public function getCategory($id)
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('EventCategory');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveCategory($category)
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('EventCategory');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('EventCategory');
		$res = $this->em->delete('EventCategory', $id);
		return $res;
	}

	public function getEventImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('EventImage');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$EventImage_Table = TABLE_PREFIX . EventImage_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $EventImage_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getEventImage($id)
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('EventImage');
		$this->em->loadById($id);
		$evImage = $this->em->getEntities();
		return (count($evImage)) ? $evImage[0] : null;
	}

	public function saveEventImage($evImage)
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('EventImage');
		$res = $this->em->persist($evImage);
		return $res;
	}

	public function deleteEventImage($id)
	{
		$this->em->setRepository('Event');
		$this->em->setEntity('EventImage');
		$res = $this->em->delete('EventImage', $id);
		return $res;
	}

	private function hierarchyCategory($category, &$category_name)
	{
		if (!is_null($category->parent_id)) {
			$categ = $this->getCategory($category->parent_id);
			$category_name = $categ->category . ' <b>&raquo</b> ' . $category_name;
			$this->hierarchyCategory($categ, $category_name);
		} else {
			return;
		}
	}
}
