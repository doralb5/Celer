<?php

require_once DOCROOT . ENTITIES_PATH . 'Menu/Menu.php';

class Menus_Model extends BaseModel
{
	public function getMenu($id)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('Menu');
		$this->em->loadById($id);
		$Menu = $this->em->getEntities();
		return (count($Menu)) ? $Menu[0] : null;
	}

	public function getMenuDetails($filter = '')
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuDetail');
		$this->em->loadByFilter($filter);
		$result = $this->em->getEntities();
		return $result;
	}

	public function existMenuDetail($filter = '')
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuDetail');
		$this->em->loadByFilter($filter);
		$result = $this->em->getEntities();
		return (count($result) ? true : false);
	}

	public function getMenuDetail($id)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuDetail');
		$this->em->loadById($id);
		$detail = $this->em->getEntities();
		return (count($detail)) ? $detail[0] : null;
	}

	public function getMenuItems($filter = '')
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuItem');
		$this->em->loadByFilter($filter, 100, 0, 'parent_id ASC, sorting');
		$result = $this->em->getEntities();
		return $result;
	}

	public function getMenuItem($id)
	{
		$this->em->setRepository('Menu');
		$this->em->setEntity('MenuItem');
		$this->em->loadById($id);
		$item = $this->em->getEntities();
		return (count($item)) ? $item[0] : null;
	}
}
