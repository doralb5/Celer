<?php

require_once DOCROOT . ENTITIES_PATH . 'Newsletter/Subscriber.php';

class Subscribers_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('Subscriber');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$Subscriber_Table = TABLE_PREFIX . Subscriber_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $Subscriber_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function getSubscriber($id)
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('Subscriber');
		$this->em->loadById($id);
		$Subscriber = $this->em->getEntities();
		return (count($Subscriber)) ? $Subscriber[0] : null;
	}

	public function saveSubscriber($Subscriber)
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('Subscriber');
		$res = $this->em->persist($Subscriber);
		return $res;
	}

	public function deleteSubscriber($id)
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('Subscriber');
		$res = $this->em->delete('Subscriber', $id);
		return $res;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('Subscriber');

		require_once DOCROOT . LIBS_PATH . 'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}

		$q = Subscriber_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('SubscriberCategory');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$SubscriberCategory_Table = TABLE_PREFIX . SubscriberCategory_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $SubscriberCategory_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getCategory($id)
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('SubscriberCategory');
		$this->em->loadById($id);
		$category = $this->em->getEntities();
		return (count($category)) ? $category[0] : null;
	}

	public function saveCategory($category)
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('SubscriberCategory');
		$res = $this->em->persist($category);
		return $res;
	}

	public function deleteCategory($id)
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('SubscriberCategory');
		$res = $this->em->delete('SubscriberCategory', $id);
		return $res;
	}

	public function getSubsCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('SubscriberSCateg');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getSubsCateg($id)
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('SubscriberSCateg');
		$this->em->loadById($id);
		$subscateg = $this->em->getEntities();
		return (count($subscateg)) ? $subscateg[0] : null;
	}

	public function saveSubsCateg($subscateg)
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('SubscriberSCateg');
		$res = $this->em->persist($subscateg);
		return $res;
	}

	public function deleteSubsCateg($id)
	{
		$this->em->setRepository('Newsletter');
		$this->em->setEntity('SubscriberSCateg');
		$res = $this->em->delete('SubscriberSCateg', $id);
		return $res;
	}
}
