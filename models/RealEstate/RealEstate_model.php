<?php

require_once DOCROOT . ENTITIES_PATH . 'Real_Estate/RealEstate.php';

class RealEstate_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstate');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		$this->lastCounter = $this->em->countByFilter($filter);
		foreach ($result as &$res) {
			$res->images = $this->getRealEstateImages(100, 0, " cms_RealEstateIMage.id_realestate = {$res->id} ");
		}
		return $result;
	}

	public function getRealEstate($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstate');
		$this->em->loadById($id);
		$RealEstate = $this->em->getEntities();
		$RealEstate[0]->images = $this->getRealEstateImages(100, 0, " cms_RealEstateIMage.id_realestate = {$RealEstate[0]->id} ");
		return (count($RealEstate)) ? $RealEstate[0] : null;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstate');
		require_once DOCROOT . LIBS_PATH . 'QuerySearch.php';
		$se = new QuerySearch($this->db);
		if ($filter != '') {
			$se->where($filter);
		}
		$q = Business_Entity::getSelectQueryObj($this->db);
		if ($sorting != '') {
			$q->orderBy($sorting);
		}
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);
		$query->limit($limit);
		$query->offset($offset);
		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		self::setBusinessArray($res);
		return $res;
	}

	public function getRealEstateImages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateImage');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function saveRealEstate($RealEstate)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstate');
		$res = $this->em->persist($RealEstate);
		return $res;
	}

	public function deleteRealEstate($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstate');
		$res = $this->em->delete('RealEstate', $id);
		return $res;
	}

	public function getContract($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateContract');
		$this->em->loadById($id);
		$contract = $this->em->getEntities();
		return (count($contract)) ? $contract[0] : null;
	}

	public function getContracts($limit = 10, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateContract');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getTypologies($limit = 10, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateTypology');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getTypology($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateTypology');
		$this->em->loadById($id);
		$contract = $this->em->getEntities();
		return (count($contract)) ? $contract[0] : null;
	}

	public function getCurrencies($limit = 10, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateTypology');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$BusinessContract_Table = TABLE_PREFIX . RealEstateContract_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $RealEstateCurrency_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getCurrency($id)
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateCurrency');
		$this->em->loadById($id);
		$contract = $this->em->getEntities();
		return (count($contract)) ? $contract[0] : null;
	}

	public function getFloors($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateFloor');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getHeating($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateHeating');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getBuildings($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Real_Estate');
		$this->em->setEntity('RealEstateBuildingState');
		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getLocations($id_contract = '')
	{
		$filter = ' AND  1 ';
		if ($id_contract != '') {
			$filter = " AND id_contract = '" . $id_contract . "' ";
		}
		$res = $this->db->select("SELECT DISTINCT (location)  FROM cms_RealEstate  WHERE  enabled = '1' " . $filter);
		return $res;
	}

	public function getLocalities($city = '', $id_contract)
	{
		if ($city == '') {
			$filter = ' AND 1 ';
		} else {
			$filter = " AND cms_RealEstate.location = '" . $city . "' ";
		}
		$res = $this->db->select("SELECT DISTINCT (locality)  FROM cms_RealEstate  WHERE  enabled = '1' AND id_contract =  '" . $id_contract . "' " . $filter);
		return $res;
	}

	public function getMinMaxPrice($contract = '')
	{
		$filter = ' AND 1 ';
		if ($contract != '') {
			$filter = "  AND  enabled = '1' AND id_contract = '" . $contract . "'";
		}
		$res = $this->db->select('SELECT MAX(price) AS max_price ,MIN(price) AS min_price FROM cms_RealEstate WHERE 1 ' . $filter);
		return $res;
	}

	public function getMinMaxSurface($contract = '')
	{
		$filter = ' AND 1 ';
		if ($contract != '') {
			$filter = "  AND  enabled = '1' AND id_contract = '" . $contract . "'";
		}
		$res = $this->db->select('SELECT MAX(square_meter) AS max_surface ,MIN(square_meter) AS min_surface FROM cms_RealEstate WHERE 1 ' . $filter);
		return $res;
	}
}
