<?php

require_once DOCROOT . LIBS_PATH . 'BaseModel.php';

class sbd_Model extends BaseModel
{
	private static $biz = null;

	public function __construct()
	{
		parent::__construct();
	}

	private function setBusinessByDomain($domain)
	{
		$this->em->setRepository('Business');
		$this->em->setEntity('Subdomain');
		$filter = "subdomain = '$domain'";
		$this->em->loadByFilter($filter, 1);
		$res = $this->em->getEntities();
		//var_dump($this->em->loadByFilter($filter, 1));exit;
		if (count($res)) {
			$subdomain = $res[0];
			$this->em->setEntity('Business');
			$this->em->loadByFilter("id = {$subdomain->target_id}");
			$resbiz = $this->em->getEntities();
			if (count($resbiz)) {
				$biz = $resbiz[0];
				$biz->subdomain = $subdomain;
				self::$biz = $biz;
			} else {
				self::$biz = null;
			}
		} else {
			self::$biz = null;
		}
	}

	public function getBusinessByDomain($domain)
	{
		if (is_null(self::$biz)) {
			$this->setBusinessByDomain($domain);
		}
		return self::$biz;
	}
}
