<?php

require_once DOCROOT . ENTITIES_PATH . 'User/User.php';

class Facebook_Model extends BaseModel
{
	public function existFbId($fb_id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$this->em->loadByFilter('facebook_id = :fbid', 1, 0, '', array(':fbid' => $fb_id));
		$user = $this->em->getEntities();
		return (count($user)) ? true : false;
	}

	public function getFbUser($fb_id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$this->em->loadByFilter("facebook_id = '$fb_id'", 1, 0);
		$result = $this->em->getEntities();
		return count($result) ? $result[0] : null;
	}
}
