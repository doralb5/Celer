<?php

require_once DOCROOT . ENTITIES_PATH . 'StaticBlock/StaticBlock.php';

class StaticBlocks_Model extends BaseModel
{
	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlock');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getStaticBlock($id)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlock');
		$this->em->loadById($id);
		$StaticBlock = $this->em->getEntities();
		return (count($StaticBlock)) ? $StaticBlock[0] : null;
	}

	public function getStaticBlockContents($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlockContent');

		if ($order == '') {
			$order = 'id DESC';
		}
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getStaticBlockContent($id)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlockContent');
		$this->em->loadById($id);
		$staticBlockContent = $this->em->getEntities();
		return (count($staticBlockContent)) ? $staticBlockContent[0] : null;
	}

	public function existContent($id_block, $lang)
	{
		$this->em->setRepository('StaticBlock');
		$this->em->setEntity('StaticBlockContent');
		$this->em->loadByFilter("id_static_block = $id_block AND lang = '$lang'");
		$result = $this->em->getEntities();
		return (count($result)) ? true : false;
	}
}
