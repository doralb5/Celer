<?php

require_once DOCROOT . ENTITIES_PATH . 'User/User.php';

class Users_Model extends BaseModel
{
	private $lastCounter;

	public function getList($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$User_Table = TABLE_PREFIX . User_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $User_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function getLastCounter()
	{
		return $this->lastCounter;
	}

	public function search(
	$keywords, $searchFields = array(array('field' => 'id', 'peso' => 100)), $filter = '', $sorting = '', $limit = 10, $offset = 0)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');

		require_once DOCROOT . LIBS_PATH . 'QuerySearch.php';
		$se = new QuerySearch($this->db);

		$q = User_Entity::getSelectQueryObj($this->db);
		$query = $se->getSearchQuery($q, $keywords, $searchFields, $this->lastCounter);

		if ($sorting != '') {
			$query->orderBy($sorting);
		}
		$query->limit($limit);
		$query->offset($offset);

		$this->em->loadByQuery($query->getQuery());
		$res = $this->em->getEntities();
		return $res;
	}

	public function getUser($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$this->em->loadById($id);
		$user = $this->em->getEntities();
		return (count($user)) ? $user[0] : null;
	}

	public function getUserCategory($id)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserCategory');
		$this->em->loadById($id);
		$user = $this->em->getEntities();
		return (count($user)) ? $user[0] : null;
	}

	public function getUserCategories($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('User');
		$this->em->setEntity('UserCategory');

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();

		$UserCateg_Table = TABLE_PREFIX . UserCategory_Entity::TABLE_NAME;
		$tot_count = $this->db->select("SELECT COUNT(*) AS tot FROM $UserCateg_Table WHERE 1 " . (($filter != '') ? "AND $filter" : ''));
		$this->lastCounter = $tot_count[0]['tot'];
		return $result;
	}

	public function saveUser($user)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$res = $this->em->persist($user);
		return $res;
	}

	public function existEmail($email)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$this->em->loadByFilter('email LIKE :email', 1, 0, '', array(':email' => $email));
		$user = $this->em->getEntities();
		return (count($user)) ? true : false;
	}

	public function getUserByEmail($email)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$this->em->loadByFilter('email = :email', 1, 0, '', array(':email' => $email));
		$user = $this->em->getEntities();
		return (count($user)) ? $user[0] : false;
	}

	public function existUsername($username)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$this->em->loadByFilter('username = :username ', 1, 0, '', array(':username' => $username));
		$user = $this->em->getEntities();
		return (count($user)) ? true : false;
	}

	public function getUserByUsername($username)
	{
		$this->em->setRepository('User');
		$this->em->setEntity('User');
		$this->em->loadByFilter('username = :username or email = :username', 1, 0, '', array(':username' => $username));
		$user = $this->em->getEntities();
		return (count($user)) ? $user[0] : false;
	}

	public function getBrandInfo()
	{

		//Query per tek databaza e serv.bluehat.al
		$sql = "SELECT Brand.* FROM Brand
                LEFT JOIN Client ON(Client.id_brand = Brand.id)
                LEFT JOIN Website ON(Website.id_client = Client.id)
                WHERE Website.domain = '" . DOMAIN . "'";
		$res = $this->db->select($sql);
		if (count($res)) {
			return $res[0];
		}
		return null;
	}
}
