<?php

require_once DOCROOT . ENTITIES_PATH . 'Page/Page.php';
require_once DOCROOT . ENTITIES_PATH . 'Page/Block.php';
require_once DOCROOT . ENTITIES_PATH . 'Page/Module.php';
require_once DOCROOT . ENTITIES_PATH . 'Component/Component.php';
require_once DOCROOT . ENTITIES_PATH . 'Generic/Template.php';

class WebPage_Model extends BaseModel
{
	public function getHomePage($lang)
	{
		$Page_Table = TABLE_PREFIX . Page_Entity::TABLE_NAME;
		$sql = "SELECT $Page_Table.id FROM $Page_Table WHERE home = '1'";
		$result = $this->db->select($sql);
		if (count($result)) {
			return $this->getPageById($result[0]['id'], $lang);
		}
		return null;
	}

	public function getPageByAlias($alias, $lang)
	{
		$Page_Table = TABLE_PREFIX . Page_Entity::TABLE_NAME;
		$PageContent_Table = TABLE_PREFIX . PageContent_Entity::TABLE_NAME;

		$sql = "SELECT p.id,pc.title FROM $PageContent_Table AS pc "
				. "INNER JOIN $Page_Table AS p ON (p.id=pc.id_page) WHERE alias=:a AND lang=:lang AND enabled='1'";
		$result = $this->db->select($sql, array(':a' => $alias, ':lang' => $lang));

		if (count($result)) {
			return $this->getPageById($result[0]['id'], $lang);
		}
		 
		return null;
	}

	public function getPageById($id, $lang)
	{
		$Page_Table = TABLE_PREFIX . Page_Entity::TABLE_NAME;
		$PageContent_Table = TABLE_PREFIX . PageContent_Entity::TABLE_NAME;
		$Component_Table = TABLE_PREFIX . Component_Entity::TABLE_NAME;

		$sql = "SELECT p.*, pc.alias,pc.title, pc.lang, pc.meta_description, pc.content, pc.meta_title, pc.meta_keywords, cmp.name As component FROM $PageContent_Table AS pc "
				. "INNER JOIN $Page_Table AS p ON (p.id=pc.id_page) "
				. "LEFT JOIN $Component_Table AS cmp ON (p.id_component=cmp.id) "
				. 'WHERE id_page=:id AND lang=:lang';

		$result = $this->db->select($sql, array(':id' => $id, ':lang' => $lang));

		if (count($result)) {
			$p = new Page();

			$p->setId($result[0]['id']);
			$p->setIsHome($result[0]['home']);
			$p->setAlias($result[0]['alias']);
			$p->setName($result[0]['name']);
			$p->setContentHeading($result[0]['title']);
			$p->setShowContentHeading($result[0]['show_content_heading']);
			$p->setComponent($result[0]['component']);
			$p->setAction($result[0]['action']);
			$p->setParameters(($result[0]['parameters'] == '' || $result[0]['parameters'] == null) ? array() : json_decode($result[0]['parameters'], true));
			$p->setActionParams(($result[0]['action_params'] == '' || $result[0]['action_params'] == null) ? array() : explode('/', $result[0]['action_params']));
			$p->setLayout($result[0]['layout']);
			$p->setRobots($result[0]['robots']);
			$p->setMeta_title($result[0]['meta_title']);
			$p->setMeta_keywords($result[0]['meta_keywords']);
			$p->setMeta_description($result[0]['meta_description']);
			$p->setContent($result[0]['content']);

			return $p;
		}
		return null;
	}

	public function getPositions($id_page, $template = '')
	{
		$Block_Table = TABLE_PREFIX . Block_Entity::TABLE_NAME;
		$PageBlock_Table = TABLE_PREFIX . PageBlock_Entity::TABLE_NAME;
		$Module_Table = TABLE_PREFIX . Module_Entity::TABLE_NAME;

		$sql = "SELECT b.*, md.name AS module FROM $Block_Table AS b "
				. "LEFT JOIN $PageBlock_Table AS pb ON (b.id = pb.id_block) "
				. "LEFT JOIN $Module_Table AS md ON (md.id = b.id_module) "
				. "WHERE (id_page = :id_page OR always = :always) AND b.enabled = '1' AND md.enabled = '1' ORDER BY b.sorting";
		$result = $this->db->select($sql, array(':id_page' => $id_page, ':always' => '1'));
		$pos_array = array();
		for ($i = 0; $i < count($result); $i++) {
			$posName = $result[$i]['position'];

			if (!array_key_exists($posName, $pos_array)) {
				$pos_array[$posName] = new Position($posName, $id_page);
			}

			$moduleName = $result[$i]['module'];
			ob_start();
			$module = Loader::loadModule($moduleName, $result[$i]['id'], $posName);
			$params = !is_null($result[$i]['parameters']) ? json_decode($result[$i]['parameters'], true) : array();
			if ($template != '') {
				$module->setTemplate($template);
			}
			$module->execute($params);
			$block_class = $result[$i]['class'];
			$block_content = ob_get_contents();
			ob_end_clean();

			$pos_array[$posName]->setBlock($result[$i]['id'], $block_content, $block_class);
		}
		return $pos_array;
	}

	public function getPageContent($component, $action, $parameters, $lang)
	{
		$PageContent_Table = TABLE_PREFIX . PageContent_Entity::TABLE_NAME;
		$Page_table = TABLE_PREFIX . Page_Entity::TABLE_NAME;
		$Component_table = TABLE_PREFIX . Component_Entity::TABLE_NAME;

		$sql = "SELECT DISTINCT * FROM $PageContent_Table AS pc "
				. "INNER JOIN $Page_table AS p ON(p.id = pc.id_page) "
				. "LEFT JOIN $Component_table as cmp ON (cmp.id = p.id_component) "
				. 'WHERE cmp.name =:c AND p.action=:a AND pc.lang=:l ';

		if (!is_null($parameters)) {
			$sql .= " AND p.action_params = '{$parameters}' ";
		}

		$result = $this->db->select($sql, array(':c' => $component, ':a' => $action, ':l' => $lang));

		if (count($result)) {
			return $result[0];
		}
		return null;
	}

	public function getPageContentByPage($id_page, $lang)
	{
		$PageContent_Table = TABLE_PREFIX . PageContent_Entity::TABLE_NAME;
		$Page_table = TABLE_PREFIX . Page_Entity::TABLE_NAME;
		$Component_table = TABLE_PREFIX . Component_Entity::TABLE_NAME;

		$sql = "SELECT DISTINCT * FROM $PageContent_Table AS pc "
				//. "INNER JOIN $Page_table AS p ON(p.id = pc.id_page) "
				//. "LEFT JOIN $Component_table as cmp ON (cmp.id = p.id_component) "
				. 'WHERE pc.id_page =:id_page AND pc.lang=:l ';

		//if (!is_null($parameters)) {
		//$sql .= " AND p.action_params = '{$parameters}' ";
		//}

		$result = $this->db->select($sql, array(':id_page' => $id_page, ':l' => $lang));

		if (count($result)) {
			return $result[0];
		}
		return null;
	}

	public function getPageAliases($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('PageAlias');
		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getPages($limit = 30, $offset = 0, $filter = '', $order = '')
	{
		$this->em->setRepository('Page');
		$this->em->setEntity('Page');

		if ($order == '') {
			$order = 'id DESC';
		}

		$this->em->loadByFilter($filter, $limit, $offset, $order);
		$result = $this->em->getEntities();
		return $result;
	}

	public function getTemplateByName($name)
	{
		$this->em->setRepository('Generic');
		$this->em->setEntity('Template');
		$name = addslashes($name);
		$this->em->loadByFilter("name = '$name'", 1);
		$result = $this->em->getEntities();
		return $result[0];
	}
}

class Page
{
	private $id;
	private $isHome;
	private $alias;
	private $name;
	private $contentHeading;
	private $show_content_heading;
	private $component;
	private $action;
	private $parameters = array();
	private $action_params = array();
	private $layout;
	private $robots;
	private $meta_title;
	private $meta_keywords;
	private $meta_description;
	private $content;

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setIsHome($isHome)
	{
		$this->isHome = $isHome;
	}

	public function setAlias($alias)
	{
		$this->alias = $alias;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setComponent($component)
	{
		$this->component = $component;
	}

	public function setAction($action)
	{
		$this->action = $action;
	}

	public function setParameters($parameters)
	{
		$this->parameters = $parameters;
	}

	public function setActionParams($action_params)
	{
		$this->action_params = $action_params;
	}

	public function setLayout($layout)
	{
		$this->layout = $layout;
	}

	public function setRobots($robots)
	{
		$this->robots = $robots;
	}

	public function getMeta_title()
	{
		return $this->meta_title;
	}

	public function getContent()
	{
		return $this->content;
	}

	public function setMeta_title($meta_title)
	{
		$this->meta_title = $meta_title;
	}

	public function setContent($content)
	{
		$this->content = $content;
	}

	public function setMeta_keywords($meta_keywords)
	{
		$this->meta_keywords = $meta_keywords;
	}

	public function setMeta_description($meta_description)
	{
		$this->meta_description = $meta_description;
	}

	public function getId()
	{
		return $this->id;
	}

	public function isHome()
	{
		return $this->isHome;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getAlias()
	{
		return $this->alias;
	}

	public function setContentHeading($text)
	{
		$this->contentHeading = $text;
	}

	public function getContentHeading()
	{
		return $this->contentHeading;
	}

	public function setShowContentHeading($val)
	{
		if ($val) {
			$this->show_content_heading = true;
		} else {
			$this->show_content_heading = false;
		}
	}

	public function showContentHeading()
	{
		return $this->show_content_heading;
	}

	public function getTitle()
	{
		return $this->getContentHeading();
	}

	public function getComponent()
	{
		return $this->component;
	}

	public function getAction()
	{
		return $this->action;
	}

	public function getParameters()
	{
		return $this->parameters;
	}

	public function getActionParams()
	{
		return $this->action_params;
	}

	public function getLayout()
	{
		return $this->layout;
	}

	public function getRobots()
	{
		return $this->robots;
	}

	public function getMeta_keywords()
	{
		return $this->meta_keywords;
	}

	public function getMeta_description()
	{
		return $this->meta_description;
	}
}
