<?php

require_once LIBS_PATH . 'StringUtils.php';

class Announces extends BaseModule
{
	public function __construct($block_id = '', $position = '', $package = '')
	{
		parent::__construct($block_id, $position, $package);
		$this->ComponentSettings = BaseComponent::getComponentSettings('Announces', 'Announces');
	}

	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';
		$featured = (isset($parameters['featured'])) ? $parameters['featured'] : '';
		$id_category = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';
		$id_NewsSection = (isset($parameters['news_section'])) ? $parameters['news_section'] : '';
		$dynamic_category = (isset($parameters['dynamic_category'])) ? $parameters['dynamic_category'] : '';

		//HeadHTML::setTitleTag('Announces' . ' | ' . CMSSettings::$website_title);
		$this->view->set('parameters', $parameters);
		$this->view->set('settings', $this->ComponentSettings);
		$announces = $this->model->getAnnounces();
		$this->view->set('announces', $announces);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'announces-list';
		$this->view->render($view);
	}
}
