<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/vertical-carousel.css'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.bootstrap.newsbox.js'); ?>
<?php $image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : ''; ?>
<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 85;
$w = (isset($parameters['w'])) ? $parameters['w'] : 400;
$h = (isset($parameters['h'])) ? $parameters['h'] : 300;
?>

<div class="row">
    <div class="col-md-12">
        <div class="module-title ">
            <h2 class="homepage-featured-title" style="text-transform: uppercase;">[$Last_Announces]</h2>

            <a class="button" href="<?= Utils::getComponentUrl('Announces/insertAnnounce') ?>" style="background:#c90000;">
                <h2 class="right-services" style="text-transform: uppercase;">[$Insert_Announce]</h2>
            </a>
            <a class="button" href="<?= Utils::getComponentUrl('Announces/listAnnounces') ?>">
                <h2 class="right-services" style="text-transform: uppercase;">[$All_m]</h2>
            </a>

        </div>
    </div>
</div>

<?php if (count($announces)) {
	?>

	<div id="vertical-slider">
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<div class="">
						<div class="row">
							<div class="col-xs-12">
								<ul class="last-announces" style="height: auto !important;">
									<?php foreach ($announces as $ann) {
		?>
										<li class="news-item">
											<a href="<?= Utils::getComponentUrl('Announces/show_announce/' . Utils::url_slug($ann->title . '-' . $ann->id)) ?>" class="title_target">  
												<div class="listing__column">
													<?php if ($ann->image != '') {
			?>
														<img  class="listing__image" src="<?= Utils::genThumbnailUrl('announce/' . $ann->image, $w, $h, array('zc' => 1), false, $image_baseurl) ?>" />
													<?php
		} else {
			?>
														<!--Default NoImage-->

														<img  class="listing__image" src="<?= Utils::genThumbnailUrl('announce/announce-category/' . $ann->category_image, $w, $h, array('zc' => '1')) ?>" alt="" style="background: #cccccc;">
													<?php
		} ?>
												</div>
											</a> 
											<a href="<?= Utils::getComponentUrl('Announces/show_announce/' . Utils::url_slug($ann->title . '-' . $ann->id)) ?>" class="title_target">  

												<div class="ann-content">
													<div>
														<span class="title"><?= StringUtils::CutString(strip_tags($ann->title), $taglia_title) ?></span>
													</div>

													<div>
														<span class="date"><?= date('H:i , d', strtotime($ann->publish_date)) . ' ' . Utils::AlbanianWordMonth(date('n', strtotime($ann->publish_date)), 'ucfirst') . ' ' . date('Y', strtotime($ann->publish_date)); ?></span>
													</div>
												</div>
											</a> 
										</li>
									<?php
	} ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="footer"> </div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function () {
			$(".last-announces").bootstrapNews({
				newsPerPage: 10,
				autoplay: true,
				pauseOnHover: true,
				direction: 'down',
				newsTickerInterval: 8000,
				onToDo: function () {
					//console.log(this);
				}
			});

		});
	</script>


<?php
} else {
		?>

	<div class="noresult">[$No_Elements_Available]</div>

	<?php
	}?>