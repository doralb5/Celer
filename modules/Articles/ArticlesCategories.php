<?php

require_once DOCROOT . ENTITIES_PATH . 'Article/ArticleCategory.php';

class ArticlesCategories extends BaseModule
{
	public function execute($params = array())
	{
		$title = (isset($params['title']) && $params['title'] != '') ? $params['title'] : '[$Categories]';
		$hide_empty_cat = (isset($params['hide_empty_cat']) && $params['hide_empty_cat'] == '1') ? true : false;
		$view = (isset($params['view']) && $params['view'] != '') ? $params['view'] : 'CategoryList-standard';

		$filter = 'parent_id IS NULL OR parent_id = 0';
		//        if($hide_empty_cat) {
		//            $filter .= " ......";
		//        }

		$model = Loader::getModel('Articles');
		$categories = $model->getCategories(-1, 0, $filter, 'category');

		$this->view->set('title', $title);
		$this->view->set('categories', $categories);
		$this->view->render($view);
	}
}
