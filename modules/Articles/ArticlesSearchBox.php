<?php

class ArticlesSearchBox extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-articles-search';
		$id_section = (isset($parameters['id_section'])) ? $parameters['id_section'] : null;
		$id_category = (isset($parameters['id_category'])) ? $parameters['id_category'] : null;

		$url = Utils::getComponentUrl('Articles/articles_list');
		if (!is_null($id_category)) {
			$url .= "/$id_category";
		}
		$this->view->set('action_url', $url);
		$this->view->render($view);
	}
}
