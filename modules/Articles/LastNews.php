<?php

class LastNews extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';
		$featured = (isset($parameters['featured'])) ? $parameters['featured'] : '';
		$id_category = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';
		$id_NewsSection = (isset($parameters['id_section'])) ? $parameters['id_section'] : '';
		$dynamic_category = (isset($parameters['dynamic_category'])) ? $parameters['dynamic_category'] : '';
		$sql_filter = (isset($parameters['sql_filter'])) ? trim($parameters['sql_filter']) : '';

		if ($dynamic_category == 1 && Articles_Component::$currCategory != '') {
			$id_category = Articles_Component::$currCategory;
		}

		$articles_md = Loader::getModel('Articles');

		if ($order == '') {
			$order = 'publish_date DESC';
		}

		$filter = TABLE_PREFIX . ArticleDetail_Entity::TABLE_NAME . ".lang = '" . FCRequest::getLang() . "' ";

		if ($sql_filter != '') {
			$filter .= ' AND (' . $sql_filter . ') ';
		}

		if ($featured == '1') {
			$filter .= "AND featured = '2' OR (featured = '1' AND featured_until > NOW()) ";
		} elseif ($featured == '0') {
			$filter .= "AND featured = '0' ";
		}

		$category = null;
		$categories = array();

		if (is_integer($id_category)) {
			$filter .= 'AND ' . Article_Entity::$table_prefix . Article_Entity::TABLE_NAME . ".id_category = '{$id_category}' ";
			$category = $articles_md->getCategory($id_category);
			$categories = array($category);
		} elseif (is_array($id_category)) {
			if (count($id_category) == 1 && $id_category[0] == '') {
				$categories = $articles_md->getCategories(-1, 0, '');
			} else {
				$subcategories_ids = $articles_md->getSubCategoriesIds($id_category, true);
				$filter .= 'AND ' . Article_Entity::$table_prefix . Article_Entity::TABLE_NAME . ".id_category IN ('" . implode("','", $subcategories_ids) . "') ";
				$categories = $articles_md->getCategories(-1, 0, "id IN ('" . implode("','", $id_category) . "')");
			}
		}

		if (count($categories) == 1) {
			$category = $categories[0];
		}

		$this->view->set('category', $category);
		$this->view->set('categories', $categories);

		if ($id_NewsSection != '') {
			$filter .= "AND sec.id = '{$id_NewsSection}' ";
		}

		if (!isset($parameters['section_title'])) {
			$parameters['section_title'] = '';
		}
		$this->view->set('parameters', $parameters);

		$articles = $articles_md->getList($limit, 0, $filter, $order);

		$this->view->set('articles', $articles);

		$title = (isset($parameters['title']) && $parameters['title'] != '') ? $parameters['title'] : '[$Last_News]';
		$this->view->set('title', $title);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-lastnews';

		$this->view->render($view);
	}
}
