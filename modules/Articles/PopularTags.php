<?php

class PopularTags extends BaseModule
{
	public function execute($params = array())
	{
		$title = (isset($params['title']) && $params['title'] != '') ? $params['title'] : '[$Tags]';

		$model = Loader::getModel('Articles');
		$arr_tags = $model->getPopularTags(10);

		$this->view->set('title', $title);
		$this->view->set('tags', $arr_tags);
		$this->view->render('PopularTags');
	}
}
