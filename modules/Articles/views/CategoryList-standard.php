<div class="CategoryList-standard">
	<?php if ($title != '') {
	?>
		<div class="panel_header">
			<h4><?= $title ?></h4>
		</div>
	<?php
} ?>

	<?php if (count($categories)) {
		?>
		<div class="panel_body">
			<ul class="category-list">
				<?php foreach ($categories as $cat) {
			?>
					<li><a href="<?= $cat->url ?>"><?= $cat->category ?> <span><?= $cat->total_articles ?></span></a></li>
				<?php
		} ?>
			</ul>
		</div>
	<?php
	} ?>
</div>
