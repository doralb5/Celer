<div class="PopularTags-standard">
	<?php if ($title != '') {
	?>
		<div class="panel_header">
			<h4><?= $title ?></h4>
		</div>
	<?php
} ?>

    <div class="panel_body">
        <div class="tags-inner">
			<?php foreach ($tags as $tag) {
		?>
				<a class="ui tag" href="<?= $tag['url'] ?>"><?= $tag['title'] ?></a>
			<?php
	} ?>
        </div>
    </div>
</div>