<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/card-list.css'); ?>
<?php
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';
?>
<?php $show_date = (isset($parameters['show_date'])) ? $parameters['show_date'] : 1; ?>
<?php $show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0; ?>
<?php $show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0; ?>
<?php $show_Subtitle = (isset($parameters['show_Subtitle'])) ? $parameters['show_Subtitle'] : 1; ?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 400;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 350;
$button = (isset($parameters['button'])) ? $parameters['button'] : 'primary';
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 3;
$col_size = 12 / $cols;
?>

<!--/* --------------- Card List - MDB---------------*/-->

<div id="card-list-news">

	<?php if ($title != '') {
	?>
		<div class="main-title-description">
			<h1 class="page-header header-title"><?= $title ?></h1>
		</div>
	<?php
} ?>




	<?php if (count($articles)) {
		?>


		<?php
		$i = 0;
		foreach ($articles as $article) {
			?>



			<article class="wow fadeInLeft animated ">
				<section>
					<div class="date">
						<div class="row">

							<div class="col-md-2 col-xs-12">
								<?php if ($show_date) {
				?>
									<time>
										<?php
										if (FCRequest::getLang() == 'al') {
											echo Utils::AlbanianWordMonth(date('d', strtotime($article->publish_date))) . ' ' . date('j, Y', strtotime($article->publish_date));
										} else {
											echo date('d', strtotime($article->publish_date));
										} ?>
										<span> [$<?php
											if (FCRequest::getLang() == 'al') {
												echo Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date))) . ' ' . date('j, Y', strtotime($article->publish_date));
											} else {
												echo date('M', strtotime($article->publish_date));
											} ?>]</span>

									</time>
								<?php
			} ?>
							</div>

							<div class="col-md-10 col-xs-12">
								<div class="news-content">

									<a class="article-title" href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">
										<strong><?= StringUtils::CutString(strip_tags($article->title), $taglia_title) ?></strong> 
									</a>

									<div><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></div>
								</div>
							</div>

						</div>
					</div>
				</section>
			</article>

			<?php
		}
	}
	?>
</div>