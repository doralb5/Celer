<!-- START NEWSTRICKER -->
<div class="container">
    <div class="newstricker_inner">
        <div class="trending"><strong><?= $parameters['section_title'] ?></strong></div>
        <div class="owl-carousel owl-theme NewsTicker" id="NewsTicker-<?= $block_id ?>">
			<?php foreach ($articles as $article) {
	?>
				<div class="item"><a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>"><?= $article->title ?></a></div>
				<?php
} ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var owl = $("#NewsTicker-<?= $block_id ?>");
        owl.owlCarousel({
            autoplay: true,
            autoplayTimeout: 5000,
            items: 1,
            loop: true,
            transitionStyle: "goDown",
            dots: false,
            nav: true,
            navText: [
                "<i class='ti-angle-left'></i>",
                "<i class='ti-angle-right'></i>"
            ]
        });
    });
</script>

<!--  END OF /. NEWSTRICKER -->