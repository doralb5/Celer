<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 1000;
$h = (isset($parameters['h'])) ? $parameters['h'] : 725;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 200;
?>


<div id="last-news">


    <div class="row">


        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="main-content pright wow fadeInLeft animated" data-wow-delay=".5s">
						<?php if (count($articles) > 1) {
	?>
							<div class="main-title">

								<a class="" href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[1]->title . '-' . $articles[1]->id) ?>">

									<h5 class="title">
										<?= $articles[1]->title ?>
									</h5>

								</a>
							</div>

							<div class="main-description">
								<p class="description"><?= StringUtils::CutString(strip_tags($articles[1]->content), $taglia_content) ?></p>

							</div>
						<?php
} ?>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="main-content pright wow fadeInLeft animated" data-wow-delay=".6s">
						<?php if (count($articles) > 2) {
		?>
							<div class="main-title">

								<a class="" href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[2]->title . '-' . $articles[2]->id) ?>">
									<h5 class="title">
										<?= $articles[2]->title ?>

								</a>  
								</h5>
							</div>

							<div class="main-description">
								<p class="description"><?= StringUtils::CutString(strip_tags($articles[2]->content), $taglia_content) ?></p>

							</div>
						<?php
	} ?>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-md-8">
            <div class="main-content-picture pcenter wow fadeInLeft animated" data-wow-delay=".7s">
				<?php if (count($articles) > 0) {
		?>

					<a class="" href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[0]->title . '-' . $articles[0]->id) ?>">
						<div class="main-picture">
							<img alt="" class="img-responsive" src="<?= Utils::genThumbnailUrl("articles/{$articles[0]->id_category}/" . $articles[0]->image, $w, $h, array('zc' => 1)) ?>"/>
						</div>

					</a>

					<div class="main-title">
						<a class="" href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[0]->title . '-' . $articles[0]->id) ?>">
							<h5 class="title">
								<?= $articles[0]->title ?>

						</a>  
					</div>
				<?php
	} ?>
            </div>
        </div>
    </div>

</div>

