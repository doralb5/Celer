<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 700;
$h = (isset($parameters['h'])) ? $parameters['h'] : 350;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 255;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 255;
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 3;
$col_size = 12 / $cols;
?>

<?php if (count($articles)) {
	?>
	<div class="article-list">

		<?php
		$col_count = 0;
	foreach ($articles as $article) {
		?>
			<div class="single-entry col-md-<?= $col_size ?> <?= ($col_count % $cols == 0) ? 'clearfix' : '' ?>">
				<div class="row">
		<?php if ($article->image != '') {
			?>
						<div class="col-md-12">
							<div class="image">
								<a class=""
								   href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>">
									<img alt="" class="img-responsive"
										 src="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, $w, $h, array('zc' => 1)) ?>"/>
								</a>
							</div>
						</div>
		<?php
		} else {
			?>
						<div class="col-md-12">
							<div class="image">
								<a class=""
								   href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>">
									<img alt="" class="img-responsive"
										 src="/templates/Portal01/images/details-690x460-2.jpg" class="img-responsive""/>
								</a>
							</div>
						</div> 

		<?php
		} ?>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h3 class="title"><a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>"><?= strip_tags($article->title); ?> </a></h3>
						<div class="info">
		<?php if ($article->show_author) {
			?><span class="author"><?= $article->author; ?></span> - <?php
		} ?>
							<span class="date"><?= date('j [$M], Y', strtotime($article->publish_date)) ?></span>
						</div>
						<p class="description"><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></p>
					</div>
				</div>
				<hr class="news-divider">
			</div>

			<?php
			$col_count++;
	} ?>

	</div>
<?php
} ?>
<div class="post-footer clearfix"> 
    <div class="row thm-margin float-right">
			<?php if ($totalElements > $elements_per_page) {
		?>
			<div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
				<?php $prev = '<span class="ti-angle-left"></span>'; ?>
				<?php $next = '<span class="ti-angle-right"></span>'; ?>
			<?php Pager::printPager($page, $totalElements, $elements_per_page, null, $prev, $next); ?>
			</div>
<?php
	} ?>
    </div>
</div>



<style>
    .clearfix {
        clear: left;
    }
    .article-list .single-entry { margin-bottom: 10px; }
    .article-list .single-entry .image { margin-bottom: 10px; }
    .article-list .single-entry h3.title {
        margin-top: 0;
        margin-bottom: 5px;
    }
    .article-list .single-entry .title a {  }
    .article-list .single-entry .info { font-size: 0.8em; color: #b5b5b5;}
    .article-list .single-entry .info .author { color: #050505; }
    .article-list .single-entry .info .date {  }
    .article-list .single-entry .description { font-size: 1.2em; }
    .news-divider {  }
</style>