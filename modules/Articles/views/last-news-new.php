<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 700;
$h = (isset($parameters['h'])) ? $parameters['h'] : 350;
//$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 80;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 100;
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 3;
$col_size = 12 / $cols;
?>

<?php if (count($articles)) {
	?>
	<div class="article-list">

		<?php
		$col_count = 0;
	foreach ($articles as $article) {
		?>
			<div class="single-entry col-md-<?= $col_size ?> <?= ($col_count % $cols == 0) ? 'clearfix' : '' ?>">
				<div class="row">
		<?php if ($article->image != '') {
			?>
						<div class="col-md-4">
							<div class="row image">
								<a class=""
								   href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>">
									<img alt="" class="img-responsive"
										 src="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, $w, $h, array('zc' => 1)) ?>"/>
								</a>
							</div>
						</div>
		<?php
		} ?>
					<div class="col-md-8">

						<h4 class="title"><a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>"><?= strip_tags($article->title); ?> <?= StringUtils::CutString(strip_tags($article->title), $taglia_title) ?></a></h4>
						<div class="info">
		<?php if ($article->show_author) {
			?><span class="author"><?= $article->author; ?></span> - <?php
		} ?>
							<span class="date"><?= date('F j, Y', strtotime($article->publish_date)) ?></span>
						</div>
						<p class="description"><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></p>

					</div>
				</div>
			</div>
			<?php
			$col_count++;
	} ?>

	</div>
<?php
} ?>



<style>
    .clearfix {
        clear: left;
    }
    .article-list .single-entry { margin-bottom: 10px; }
    .article-list .single-entry .image { margin-bottom: 10px; }
    .article-list .single-entry h4.title {
        margin-top: 0;
        margin-bottom: 5px;
    }
    .article-list .single-entry .title a {  }
    .article-list .single-entry .info { font-size: 0.8em; color: #b5b5b5;}
    .article-list .single-entry .info .author { color: #050505; }
    .article-list .single-entry .info .date {  }

</style>