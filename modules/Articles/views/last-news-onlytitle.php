<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 50;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 250;
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="title-category news-lista category-name">Ultime Notizie</h1>
    </div>
</div>

<?php foreach ($articles as $article) {
	?>
	<div class="row">
		<div class="col-md-12 all-news">
			<div class="news-lista">
				<p class="title-onlytitle"><a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>"><?= StringUtils::CutString(strip_tags($article->title), $taglia_title) ?></a></p>
				<p class="description-news"><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></p>
				<p class="date-news1" style="float: left;">
					<?= date('j [$F] Y', strtotime($article->publish_date)) ?>
				</p>
				<a class="btn-read-more" style="float: right;"
				   href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">Leggi Tutto</a>
			</div>
		</div>
	</div>
<?php
} ?>
