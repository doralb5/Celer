<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/slider-pro.min.css'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.sliderPro.min.js'); ?>
<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 400;
$h = (isset($parameters['h'])) ? $parameters['h'] : 200;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 100;
?>
<style>
    #slider-pro .sp-thumbnail-image-container {
        width: 100px;
        height: 80px;
        overflow: hidden;
        float: left;
    }

    #slider-pro .sp-thumbnail-image {
        height: 100%;
    }

    #slider-pro .sp-thumbnail-text {
        width: 170px;
        float: right;
        padding: 8px;
        background-color: #F0F0F0;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    #slider-pro .sp-thumbnail-title {
        margin-bottom: 5px;
        text-transform: uppercase;
        color: #333;
    }

    #slider-pro .sp-thumbnail-description {
        font-size: 14px;
        color: #333;
    }

    #slider-pro .sp-thumbnail-title {
        margin-bottom: 5px;
        text-transform: uppercase;
        color: #c5020a;
        font-size: 11px;
        font-weight: bold;
    }

    #slider-pro .sp-thumbnail-description {
        font-size: 12px;
        color: #333;
    }

    @media (max-width: 500px) {
        #slider-pro .sp-thumbnail {
            text-align: center;
        }

        #slider-pro .sp-thumbnail-image-container {
            display: none;
        }

        #slider-pro .sp-thumbnail-text {
            width: 120px;
        }

        #slider-pro .sp-thumbnail-title {
            font-size: 12px;
            text-transform: uppercase;
        }

        #slider-pro .sp-thumbnail-description {
            display: none;
        }
    }
</style>


<script type="text/javascript">
    $(document).ready(function ($) {
        $('#slider-pro').sliderPro({
            width: 670,
            height: 330,
            orientation: 'vertical',
            loop: false,
            arrows: true,
            buttons: false,
            thumbnailsPosition: 'right',
            thumbnailPointer: true,
            thumbnailWidth: 290,
            breakpoints: {
                800: {
                    thumbnailsPosition: 'bottom',
                    thumbnailWidth: 270,
                    thumbnailHeight: 100
                },
                500: {
                    thumbnailsPosition: 'bottom',
                    thumbnailWidth: 120,
                    thumbnailHeight: 100
                }
            },
            fade: true,
            fullScreen: true
        });
    });
</script>

<div id="slider-pro" class="slider-pro">
    <div class="sp-slides">
		<?php foreach ($articles as $article) {
	?>
			<div class="sp-slide">
				<?php if ($article->image != '') {
		?>
					<img class="sp-image" src="../src/css/images/blank.gif"
						 data-src="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, 1000, 750, array('zc' => 1)) ?>"
						 data-retina="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, $w, $h, array('zc' => 1)) ?>"/>
					 <?php
	} else {
		?>
					<img class="sp-image" src="../src/css/images/blank.gif"
						 data-src="<?= WEBROOT . MEDIA_ROOT ?>/noimg.png"
						 data-retina="<?= WEBROOT . MEDIA_ROOT ?>/noimg.png"/>
					 <?php
	} ?>
				<div class="sp-caption"><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?> <a
						class="btn btn-sm btn-<?= $button ?> btn-read-more"
						href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">[$read_more]</a>
				</div>
			</div>

		<?php
} ?>
    </div>

    <div class="sp-thumbnails">
		<?php foreach ($articles as $article) {
		?>
			<div class="sp-thumbnail">
				<div class="sp-thumbnail-image-container">
					<?php if ($article->image != '') {
			?>
						<img class="sp-thumbnail-image"
							 src="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, $w, $h, array('zc' => 1)) ?>"/>
						 <?php
		} else {
			?>
						<img class="sp-thumbnail-image"
							 src="<?= WEBROOT . MEDIA_ROOT ?>/noimg.png"/>
						 <?php
		} ?>
				</div>
				<div class="sp-thumbnail-text">
					<div
						class="sp-thumbnail-title"><?= StringUtils::CutString(strip_tags($article->title), $taglia_title) ?></div>
					<div
						class="sp-thumbnail-description"><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></div>
				</div>
			</div>
		<?php
	} ?>
    </div>
</div>