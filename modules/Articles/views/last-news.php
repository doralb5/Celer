<?php HeadHTML::addStylesheet(WEBROOT . $this->view_path . 'css/lastnews.css'); ?>

<?php require_once LIBS_PATH . 'StringUtils.php'; ?>

<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 400;
$h = (isset($parameters['h'])) ? $parameters['h'] : 200;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 100;
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 3;
$button = (isset($parameters['button'])) ? $parameters['button'] : 'primary';
$col_size = 12 / $cols;
$show_Subtitle = (isset($parameters['show_Subtitle'])) ? $parameters['show_Subtitle'] : 0;
?>
<style>
    @media (min-width: 767px) {
        .lastnews .thumbnail {
        }
    }

    .lastnews .thumbnail {
        padding: 0px;
        margin-bottom: 0px;
        border: 0px solid white;
        border-radius: 0px;
    }

    .lastnews #newsCarousel {
        padding: 10px;
    }

    ul.thumbnails li {
        margin-bottom: 0px;
    }

    @media (max-width: 768px) {
        .lastnews ul.thumbnails {
            padding-left: 0px;
        }

        .lastnews #newsCarousel {
            margin-right: 0px;
        }

        .lastnews .well {
            padding: 0px;
        }
    }

    .carousel-control {
        background: none !important;
        color: #CACACA;
        font-size: 2.3em;
        text-shadow: none;
        margin-top: 0px;
        width: 5%;
    }

    .carousel-control.left i {
        margin-left: -60px;
    }

    .carousel-control.right i {
        margin-right: -60px;
    }

    @media (max-width: 768px) {
        .carousel-control.left i, .carousel-control.right i {
            display: none;
        }
    }

    ul.pager {
        display: none;
    }

    ul {
        list-style-type: none;
        -webkit-padding-start: 0px;
    }

    .button_align {
        text-align: center;
    }

    .fff {
        margin: 30px 0;
    }

    #newsCarousel .carousel-inner {
        padding: 0;
    }

    .lastnews .fff {
        transition: .8s;
        background: white;
        box-shadow: 0 6px 20px rgba(0,0,0,0.2);
    }

    .lastnews .fff .caption {
        padding: 0 10px 10px 10px;
    }
    .lastnews .fff:hover {
        moz-box-shadow: 0px 15px 20px 0px rgba(0,0,0,0.25);
        -webkit-box-shadow: 0px 15px 20px 0px rgba(0,0,0,0.25);
        box-shadow: 0px 15px 20px 0px rgba(0,0,0,0.25);
        /* transform: perspective(400px); */
    }

    .carousel-control:hover { color: #4a4a4a; }

    .lastnews p.news-content {
        min-height: 40px;
    }

</style>

<div class="lastnews">
    <div class="">
        <div class="carousel slide" id="newsCarousel">
            <div class="carousel-inner">

				<?php
				$i = 0;
				foreach ($articles as $article) {
					?>
					<?php if ($i == 0) {
						?>
						<div class="item active">
							<ul class="thumbnails">
								<div class="row">
								<?php
					} ?>
								<?php if ($i % $cols == 0 && $i != 0) {
						?>
								</div>
							</ul>
						</div>
						<div class="item">
							<ul class="thumbnails">
								<div class="row">
								<?php
					} ?>

								<li class="col-md-<?= $col_size ?> wow fadeInUp animated" data-wow-delay=".5s">
									<div class="fff">
										<?php if ($article->image != '') {
						?>
											<div class="thumbnail">
												<a class="hover-img"
												   href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>">
													<img alt=""
														 src="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, $w, $h, array('zc' => 1)) ?>"/>
													<i class="fa fa-link box-icon-big round hover-icon"></i>
												</a>
											</div>
										<?php
					} ?>
										<div class="caption">
											<div class="content">
												<h4 class="news-title"><?= StringUtils::CutString(strip_tags($article->title), $taglia_title) ?></h4>
												<?php if ($show_Subtitle) {
						?>
													<h5 class="news-subtitle">
														<?= $article->subtitle ?>
													</h5>
												<?php
					} ?>

												<p class="news-content"><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></p>
											</div>
											<div class="button_align">
												<a class="btn btn-sm btn-<?= $button ?> btn-read-more-custom"
												   href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>">[$read_more]</a>
											</div>
										</div>
									</div>
								</li>
								<?php
								$i++;
				}
							?>
                        </div>
                    </ul>
                </div>
            </div>

			<?php if (count($articles) > $cols) {
								?>
				<nav>
					<ul class="newscontrol-box ">
						<li><a data-slide="prev" href="#newsCarousel" class="left carousel-control"><i
									class="glyphicon glyphicon-chevron-left"></i></a></li>
						<li><a data-slide="next" href="#newsCarousel" class="right carousel-control"><i
									class="glyphicon glyphicon-chevron-right"></i></a></li>
					</ul>
				</nav>
			<?php
							} ?>
        </div>
    </div>
</div>

<script>
    $('#newsCarousel').carousel({
        interval: 10000
    })
</script>
