<?php if (count($articles)) {
	?>
	<ul class="content tabs-content" id="most-today">
		<?php for ($i = 0; $i < count($articles); $i++) {
		?>
			<li>
			<span class="count"><?= sprintf('%02d', $i + 1) ?></span>
			<span class="text">
				<a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[$i]->title . '-' . $articles[$i]->id) ?>"><?= $articles[$i]->title ?></a>
			</span>
		</li>
	<?php
	} ?>
	</ul>
<?php
} ?>