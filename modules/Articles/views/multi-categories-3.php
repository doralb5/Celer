<?php
require_once(LIBS_PATH . 'StringUtils.php');

$parameters['id_category'] = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';

if ($parameters['id_category'] !== '' && count($articles)) {
	$categories = (is_array($parameters['id_category'])) ? $parameters['id_category'] : array($parameters['id_category'] => $articles[0]->category);
} else {
	$categories = array();
}
?>

<!-- START POST CATEGORY STYLE THREE (More news) -->
<div class="post-inner">
    <!-- post header -->
    <div class="post-head">
        <h2 class="title"><strong><?= $parameters['section_title'] ?></strong></h2>
        <div class="filter-nav">
            <ul>
                <li><a class="active" href="#">[$All]</a></li>
				<?php
				foreach ($categories as $idcat => $cat) {
					echo '<li><a href="#' . $idcat . '">' . $cat . "</a></li>\n";
				}
				?>
            </ul>
        </div>
    </div>
    <!-- post body -->
    <div class="post-body">
        <div class="owl-carousel owl-theme" id="post-slider-3">
			<?php include 'multi-categories-item-2.php' ?>

			<?php
			if (count($categories) > 1) {
				foreach ($categories as $idcat => $cat) {
					$parameters['id_category'] = $idcat;
					$parameters['view'] = 'multi-categories-item';
					$widget = Loader::loadModule('Articles/LastNews');
					$widget->execute($parameters);
				}
			}
			?>
        </div>
    </div>
    <!-- footer post -->
    <div class="post-footer">
        <div class="row thm-margin">
            <div class="col-md-12 thm-padding text-center"><a class="more-btn" href="#">[$Others_news]</a></div>
            <!--            <div class="hidden-xs col-sm-4 col-md-3 thm-padding">
                            <div class="social">
                                <ul>
                                    <li>
                                        <div class="share transition"><a class="ico fb" href="#" target="_blank"><i class="fa fa-facebook"></i></a> <a class="ico tw" href="#" target="_blank"><i class="fa fa-twitter"></i></a> <a class="ico gp" href="#" target="_blank"><i class="fa fa-google-plus"></i></a> <a class="ico pin" href="#" target="_blank"><i class="fa fa-pinterest"></i></a> <i class="ti-share ico-share"></i></div>
                                    </li>
                                    <li><a href="#"><i class="ti-heart"></i></a></li>
                                    <li><a href="#"><i class="ti-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>-->
        </div>
    </div>
</div>