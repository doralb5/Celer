<?php
require_once(LIBS_PATH . 'StringUtils.php');

$parameters['id_category'] = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';

if ($parameters['id_category'] !== '' && count($articles)) {
	$categories = (is_array($parameters['id_category'])) ? $parameters['id_category'] : array($parameters['id_category'] => $articles[0]->category);
} else {
	$categories = array();
}
?>

<!-- START CARD POST -->
<div class="row row-m">
	<?php include 'multi-categories-item-3.php' ?>

	<?php
	if (count($categories) > 1) {
		foreach ($categories as $idcat => $cat) {
			$parameters['id_category'] = $idcat;
			$parameters['view'] = 'multi-categories-item';
			$widget = Loader::loadModule('Articles/LastNews');
			$widget->execute($parameters);
		}
	}
	?>
</div>
<!-- END OF /. CARD POST -->

