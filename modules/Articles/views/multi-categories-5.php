<?php
require_once(LIBS_PATH . 'StringUtils.php');

$parameters['id_category'] = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';

if ($parameters['id_category'] !== '' && count($articles)) {
	$categories = (is_array($parameters['id_category'])) ? $parameters['id_category'] : array($parameters['id_category'] => $articles[0]->category);
} else {
	$categories = array();
}
?>

<!-- START POST CATEGORY STYLE FIVE (Latest articles ) -->
<div class="post-inner">
    <!--post header-->
    <div class="post-head">
        <h2 class="title"><strong><?= $parameters['section_title'] ?></strong></h2>
    </div>
    <!-- post body -->
    <div class="post-body">
		<?php include 'multi-categories-item-4.php' ?>

		<?php
		if (count($categories) > 1) {
			foreach ($categories as $idcat => $cat) {
				$parameters['id_category'] = $idcat;
				$parameters['view'] = 'multi-categories-item';
				$widget = Loader::loadModule('Articles/LastNews');
				$widget->execute($parameters);
			}
		}
		?>
    </div>
    <!-- /. post body -->
    <div class="post-footer">
        <div class="row thm-margin">
            <div class="col-xs-12 col-sm-12 col-md-12 thm-padding"><a class="more-btn" href="<?= Utils::getComponentUrl('Articles/articles_list/' . $category->id); ?>">[$Others_news]</a></div>
        </div>
    </div>
</div>
<!-- END OF /. POST CATEGORY STYLE FIVE (Latest articles ) -->