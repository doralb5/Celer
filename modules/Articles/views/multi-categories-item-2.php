<?php
$w = (isset($parameters['width'])) ? $parameters['width'] : 345;
$h = (isset($parameters['height'])) ? $parameters['height'] : 242;
$w_mini = $w / 2;
$h_mini = $h / 2;
?>

<?php if (count($articles)) {
	?>
	<div class="item">
		<div class="row">
			<?php for ($i = 0; $i < count($articles); $i++) {
		?>
				<div class="col-sm-6 main-post-inner post-inner bord-right">
					<div class="more-post"><a class="news-image" href="#"><img alt="" class="img-responsive" src="<?= Utils::genThumbnailUrl("articles/{$articles[$i]->id_category}/" . $articles[$i]->image, 620, 370, array('zc' => 1)) ?>" /></a>
						<div class="post-text"><span class="post-category"><?= $articles[$i]->category ?></span>
							<h4><a href="#"><?= $articles[$i]->title ?></a></h4>
							<ul class="authar-info">
								<li><i class="ti-timer"></i> <?= date('d-m-Y H:i', strtotime($articles[$i]->publish_date)); ?></li>
							</ul>
						</div>
					</div>
				</div>
			<?php
	} ?>
		</div>
	</div>
	<?php
}?>