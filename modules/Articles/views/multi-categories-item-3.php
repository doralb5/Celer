<?php
$w = (isset($parameters['width'])) ? $parameters['width'] : 345;
$h = (isset($parameters['height'])) ? $parameters['height'] : 242;
$w_mini = $w / 2;
$h_mini = $h / 2;
?>

<?php if (count($articles)) {
	?>
	<div class="col-sm-6 col-p">
		<div class="posts card-post">

			<div class="post-grid post-grid-item">
				<figure class="posts-thumb">
					<span class="post-category"><?= $articles[0]->category ?></span>
					<a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[0]->title . '-' . $articles[0]->id) ?>">
						<img alt="" src="<?= Utils::genThumbnailUrl("articles/{$articles[0]->id_category}/" . $articles[0]->image, 378, 270, array('zc' => 1)) ?>" class="img-responsive"/></a></figure>
				<div class="posts-inner">
					<h6 class="posts-title"><a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[$i]->title . '-' . $articles[0]->id) ?>"><?= $articles[0]->title ?></a></h6>
					<ul class="authar-info">
						<li><i class="ti-timer"></i> <?= date('d-m-Y H:i', strtotime($articles[0]->publish_date)); ?></li>
					</ul>
					<p><?= StringUtils::CutString($articles[0]->content, 220) ?></p>
				</div>
				<!--div class="posts__footer card__footer">
					<div class="post-author">
						<figure class="post-author-avatar hidden-xs"><img alt="Post Author Avatar" src="/templates/Portal01/images/avatar-1.jpg" /></figure>
						<div class="post-author-info ">
							<h4 class="post-author-name">Naeem Khan</h4>
						</div>
					</div>
					<ul class="post-meta">
						<li class="meta-item "><i class="ti-eye"></i> 2369</li>
					</ul>
				</div-->
			</div>
		</div>
	</div>

	<?php if (count($articles)) {
		?>
		<div class="col-sm-6 col-p">
			<div class="news-list">
				<div class="posts">
					<?php for ($i = 1; $i < count($articles); $i++) {
			?>                    
						<ul>
							<li class="post-grid">
								<div class="posts-inner"><span class="post-category"><?= $articles[$i]->category ?></span>
									<h6 class="posts-title"><a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[$i]->title . '-' . $articles[$i]->id) ?>"><?= $articles[$i]->title ?></a></h6>
									<ul class="authar-info">
										<li><i class="ti-timer"></i> <?= date('d-m-Y H:i', strtotime($articles[$i]->publish_date)); ?></li>
									</ul>
									<p><?= StringUtils::CutString($articles[0]->content, 100) ?></p>
								</div>
							</li>
						</ul>
					<?php
		} ?>            
				</div>
			</div>
		</div>
	<?php
	} ?>
<?php
} ?>

