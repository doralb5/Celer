<?php
$w = (isset($parameters['width'])) ? $parameters['width'] : 345;
$h = (isset($parameters['height'])) ? $parameters['height'] : 242;
$w_mini = $w / 2;
$h_mini = $h / 2;
?>

<?php if (count($articles)) {
	?>
	<?php for ($i = 0; $i < count($articles); $i++) {
		?>
		<div class="news-list-item articles-list">
			<div class="img-wrapper"><a class="thumb" href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[0]->title . '-' . $articles[0]->id) ?>"><img alt="" class="img-responsive" src="<?= Utils::genThumbnailUrl("articles/{$articles[$i]->id_category}/" . $articles[$i]->image, 218, 150, array('zc' => 1)) ?>" /></a></div>
			<div class="post-info-2">
				<h4><a class="title" href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[0]->title . '-' . $articles[0]->id) ?>"><?= $articles[$i]->title ?></a></h4>
				<ul class="authar-info">
					<li><i class="ti-timer"></i> <?= date('d-m-Y H:i', strtotime($articles[$i]->publish_date)); ?></li>
				</ul>
				<p class="hidden-sm"><?= StringUtils::CutString($articles[0]->content, 220) ?></p>
			</div>
		</div>
	<?php
	} ?>
	<?php
}?>