<?php
$w = (isset($parameters['width'])) ? $parameters['width'] : 345;
$h = (isset($parameters['height'])) ? $parameters['height'] : 242;
$w_mini = intval($w / 2);
$h_mini = intval($h / 2);
?>

<?php if (count($articles)) {
	?>
	<div class="item">
		<div class="row">
			<div class="col-sm-6 main-post-inner bord-right">
				<article>
					<?php
					if ($articles[0]->image == '') {
						$img = "http://via.placeholder.com/{$w}x{$h}?text=No+Image";
					} else {
						$img = Utils::genThumbnailUrl("articles/{$articles[0]->id_category}/" . $articles[0]->image, $w, $h, array('zc' => 1));
					} ?>
					<figure><a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[0]->title . '-' . $articles[0]->id) ?>"><img alt="" class="img-responsive" src="<?= $img ?>" /></a> <span class="post-category"><?= $articles[0]->category ?></span></figure>

					<div class="post-info">
						<h3><a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[0]->title . '-' . $articles[0]->id) ?>"><?= $articles[0]->title ?></a></h3>

						<ul class="authar-info">
							<li><?= date('d-m-Y H:i', strtotime($articles[0]->publish_date)); ?></li>
						</ul>

						<p><?= StringUtils::CutString(strip_tags($articles[0]->content), 180) ?></p>
					</div>
				</article>
			</div>

			<?php if (count($articles) > 1) {
						?>
				<div class="col-sm-6">
					<div class="news-list">
						<?php for ($i = 1; $i < count($articles); $i++) {
							?>
							<?php
							if ($articles[$i]->image == '') {
								$img = "http://via.placeholder.com/{$w_mini}x{$h_mini}?text=No+Image";
							} else {
								$img = Utils::genThumbnailUrl("articles/{$articles[$i]->id_category}/" . $articles[$i]->image, $w_mini, $h_mini, array('zc' => 1));
							} ?>
							<div class="news-list-item">
								<div class="img-wrapper"><a class="thumb" href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[$i]->title . '-' . $articles[$i]->id) ?>"><img alt="" class="img-responsive" src="<?= $img ?>" /> </a>

									<a class="thumb" href="#"> </a></div>

								<div class="post-info-2">
									<h5><a class="title" href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[$i]->title . '-' . $articles[$i]->id) ?>"><?= $articles[$i]->title ?></a></h5>

									<ul class="authar-info">
										<li><?= date('d-m-Y H:i', strtotime($articles[$i]->publish_date)); ?></li>
									</ul>
								</div>
							</div>
						<?php
						} ?>
					</div>
				</div>
			<?php
					} ?>
		</div>
	</div>
	<?php
}?>