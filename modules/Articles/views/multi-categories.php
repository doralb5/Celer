<?php
require_once(LIBS_PATH . 'StringUtils.php');

$parameters['id_category'] = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';
?>

<div class="post-inner">
    <div class="post-head">
        <h2 class="title"><strong><?= $parameters['section_title'] ?></strong></h2>
		<?php if (count($categories) > 1) {
	?>
			<div class="filter-nav">
				<ul>
					<li><a class="active" href="#">[$All]</a></li>
					<?php
					foreach ($categories as $cat) {
						echo '<li><a href="#' . $cat->id . '">' . $cat->category . "</a></li>\n";
					} ?>
				</ul>
			</div>
		<?php
} ?>
    </div>

	<?php if (count($articles)) {
						?>

		<div class="post-body">
			<div class="owl-carousel owl-theme post-slider" id="post-slider-<?= $block_id ?>">

				<?php include 'multi-categories-item.php' ?>

				<?php
				if (count($categories) > 1) {
					foreach ($categories as $idcat => $cat) {
						$parameters['id_category'] = $idcat;
						$parameters['view'] = 'multi-categories-item';
						$widget = Loader::loadModule('Articles/LastNews');
						$widget->execute($parameters);
					}
				} ?>

			</div>
		</div>


	<?php
					} ?>

    <!-- Post footer -->
    <div class="post-footer">
        <div class="row thm-margin">
            <div class="col-xs-12 col-sm-12 col-md-12 thm-padding"><a class="more-btn" href="<?= Utils::getComponentUrl('Articles/articles_list/' . $category->id); ?>">[$Others_news]</a></div>
        </div>
    </div>
</div>

<?php
$jscode = <<<EOF
    //Post carousel
    var owl = $("#post-slider-$block_id");
    owl.owlCarousel({
        nav: true,
        loop: false,
        items: 1,
        dots: false,
        transitionStyle: "fade",
        navText: [
            "<i class='ti-angle-left'></i>",
            "<i class='ti-angle-right'></i>"
        ]
    });
EOF;
HeadHTML::addScript($jscode);
?>