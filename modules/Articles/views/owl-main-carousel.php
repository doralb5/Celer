<?php $w = ($parameters['w']) ? $parameters['w'] : 700; ?>
<?php $h = ($parameters['h']) ? $parameters['h'] : 400; ?>


<?php if (count($articles) > 1) {
	?>
	<div class="slider-wrapper">
		<div class="owl-carousel owl-theme owl-slider" id="owl-slider-<?= $block_id; ?>">
			<!-- Slider item one -->
		<?php
} ?>
		<?php foreach ($articles as $article) {
		?>
			<div class="item">
				<div class="slider-post post-height-1">
					<a class="news-image" href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>">
						<img alt="" class="img-responsive" src="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, $w, $h, array('zc' => 1)) ?>" />
					</a>
					<div class="post-text">
						<span class="post-category"><?= $article->category ?></span>
						<h2>
							<a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id) ?>"><?= $article->title ?></a>
						</h2>
						<ul class="authar-info">
							<li class="date"><?= date('d-m-Y H:i', strtotime($article->publish_date)); ?></li>
						</ul>
					</div>
				</div>
			</div>
		<?php
	} ?>
		<?php if (count($articles) > 1) {
		?>
		</div>
	</div>

	<?php
	$jscode = <<<EOF
        var owlslider = $("#owl-slider-$block_id");
        owlslider.owlCarousel({
            items: 1,
            loop: true,
            transitionStyle: "fade",
            autoplay: true,
            autoplayTimeout: 4000,
            nav: true,
            dots: false,
            navText: [
                "<i class='ti-angle-left'></i>",
                "<i class='ti-angle-right'></i>"
            ]
        });
EOF;

		HeadHTML::addScript($jscode);
	}
