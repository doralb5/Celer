<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/pgwslider.jquery.json'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/pgwslider.min.js'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/pgwslider.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/pgwslider.min.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/periodic-three-news-carousel.css'); ?>



<?php $w = (isset($parameters['w'])) ? $parameters['w'] : 1000; ?>
<?php $h = (isset($parameters['h'])) ? $parameters['h'] : 600; ?>

<?php if (count($articles) > 0) {
	?>
	<div class="container">
		<div class="row news-slider-wrapper">
			<div class="col-md-12">
				<ul class="pgwSlider">
					<?php
					for ($i = 0; $i < count($articles); $i++) {
						$article = $articles[$i];
						$link = Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($article->title . '-' . $article->id); ?>
						<li>
							<a href="<?= $link ?>">
								<img src="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, $w, $h, array('zc' => 1)) ?>" class ="img-responsive">
								<span><?= strip_tags($article->title) ?></span>
							</a>
						</li>

					<?php
					} ?>
				</ul>
			</div>
		</div>
	</div>                
<?php
} ?>

<script>
    $(document).ready(function () {
        $('.pgwSlider').pgwSlider();
    });

</script>


