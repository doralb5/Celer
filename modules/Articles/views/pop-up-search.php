<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.magnific-popup.js'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/popuo-box.css'); ?>

<a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="glyphicon glyphicon-search"> </i></a>

<!---//pop-up-box---->
<div id="small-dialog" class="mfp-hide">
    <div class="search-top">
        <form method="get" action="<?= $action_url ?>"> 
            <div class="login-search">
                <input type="submit" name="search" value="">
                <input type="text" name="query" value="[$search]" onfocus="this.value = '';" onblur="if (this.value == '') {
                            this.value = 'Search..';
                        }">		
            </div>
        </form>
        <p><?= CMSSettings::$website_title ?></p>
    </div>				
</div>


<script>
    $(document).ready(function () {
        $('.popup-with-zoom-anim').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });

    });
</script>
