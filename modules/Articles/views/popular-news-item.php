<?php if (count($articles)) {
	?>
	<?php for ($i = 0; $i < count($articles); $i++) {
		?>
		<div class="p-post">
			<h4>
				<a href="<?= Utils::getComponentUrl('Articles/show_article') . '/' . Utils::url_slug($articles[$i]->title . '-' . $articles[$i]->id) ?>"><?= $articles[$i]->title ?></a>
			</h4>
			<ul class="authar-info">
				<li class="date">
					<a href="#"><i class="ti-timer"></i> <?= date('d-m-Y H:i', strtotime($articles[$i]->publish_date)); ?></a>
				</li>
			</ul>
		</div>
	<?php
	} ?>
<?php
} ?>