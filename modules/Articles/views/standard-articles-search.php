<?php HeadHTML::AddStylesheet('standard-articles-search.css'); ?>


<div class="article-search-box">
    <form method="get" action="<?= $action_url ?>"> 
        <div class="input-group">
            <input type="text" name="query" class="form-control your-class search-bar-styled" placeholder="[$search_placeholder]" value="<?= (@$_GET['query']) ?>">
            <span class="input-group-btn">
                <button name="search" class="btn btn-default cls-height" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            </span>
        </div>
    </form>
</div>