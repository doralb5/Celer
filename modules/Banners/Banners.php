<?php

class Banners extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';

		$id_category = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';

		$filter = "enabled = '1' ";
		if ($id_category != '') {
			$filter .= "AND id_category = $id_category";
		}

		$banners = $this->model->getList($limit, 0, $filter, $order);
		$this->view->set('banners', $banners);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT . '/banners');

		$this->view->set('parameters', $parameters);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-banners';
		$this->view->render($view);
	}
}
