<?php if (count($banners)) {
	?>
	<?php $w = (isset($parameters['w']) && $parameters['w'] > 0) ? $parameters['w'] : 1000 ?>
	<?php $h = (isset($parameters['h']) && $parameters['h'] > 0) ? $parameters['h'] : 350 ?>
	<?php $interval = (isset($parameters['interval']) && $parameters['interval'] > 0) ? $parameters['interval'] : 3000 ?>
	<?php $show_caption = (isset($parameters['show_caption'])) ? $parameters['show_caption'] : 0 ?>
	<style>
		#myCarousel_<?= $block_id ?>,
		#myCarousel_<?= $block_id ?> .item,
		#myCarousel_<?= $block_id ?> .active {
			height: 100%;
		}

		#myCarousel_<?= $block_id ?> .carousel-inner {
			height: 100%;
		}

		/* Background images are set within the HTML using inline CSS, not here */

		#myCarousel_<?= $block_id ?> .fill {
			width: 100%;
			height: 100%;
			background-position: center;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
			-o-background-size: cover;
		}
		#myCarousel_<?= $block_id ?> .hidden {
			display: none;
		}
		/*Background Carousel*/

		.carousel-fade .carousel-inner .item {
			transition-property: opacity;
		}
		.carousel-fade .carousel-inner .item,
		.carousel-fade .carousel-inner .active.left,
		.carousel-fade .carousel-inner .active.right {
			opacity: 0;
		}
		.carousel-fade .carousel-inner .active,
		.carousel-fade .carousel-inner .next.left,
		.carousel-fade .carousel-inner .prev.right {
			opacity: 1;
		}
		.carousel-fade .carousel-inner .next,
		.carousel-fade .carousel-inner .prev,
		.carousel-fade .carousel-inner .active.left,
		.carousel-fade .carousel-inner .active.right {
			left: 0;
			transform: translate3d(0, 0, 0);
		}

	</style>

	<!-- Full Page Image Background Carousel Header -->
	<header id="myCarousel_<?= $block_id ?>" class="carousel-fade carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators hidden">
			<?php
			$i = 0;
	foreach ($banners as $banner) {
		?>
				<li data-target="#myCarousel_<?= $block_id ?>" data-slide-to="<?= $i ?>" class="<?= ($i == 0) ? 'active' : '' ?>"></li>
				<?php
				$i++;
	} ?>
		</ol>

		<!-- Wrapper for Slides -->
		<div class="carousel-inner" role="listbox">

			<?php
			$i = 0;
	foreach ($banners as $banner) {
		?>
				<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('banners/' . $banner['filename'], 0, 0, '', true)); ?>

		                <!--                        <div class="item <?= ($i == 0) ? 'active' : '' ?>">
		                                            <div class="fill" style="background-image:url('<?= Utils::genThumbnailUrl('banners/' . $banner['filename'], $w, $h, array('zc' => 1)) ?>');"></div>
		                                            <div class="carousel-caption">
		                                            </div>
		                                        </div>-->


				<div class="item <?= ($i == 0) ? 'active' : '' ?>">
					<a href="<?= $banner['target_url'] ?>"><img src="<?= Utils::genThumbnailUrl('banners/' . $banner['filename'], $w, $h, array('zc' => 1)) ?>" width="100%"/></a>

					<?php if ($show_caption) {
			?>
						<div class="carousel-caption">
							<?= $banner['name'] ?>
						</div>
					<?php
		} ?>

				</div>

				<?php
				$i++;
	} ?>
		</div>

		<!-- Controls -->
		<a class="left carousel-control hidden" href="#myCarousel_<?= $block_id ?>" data-slide="prev">
			<span class="icon-prev"></span>
		</a>
		<a class="right carousel-control hidden" href="#myCarousel_<?= $block_id ?>" data-slide="next">
			<span class="icon-next"></span>
		</a>

	</header>

	<script type="text/javascript">
		$(document).ready(function () {
			$('#myCarousel_<?= $block_id ?>').carousel({
				interval: <?= $interval ?>,
				cycle: true
			});
		});
	</script>


<?php
} ?>