<?php if (count($banners)) {
	?>
	<style type="text/css">
		/*
		SKIN ADV
		*/

		body {
			background-image: url("{MEDIA_PATH}banners/<?= $banners[0]['filename'] ?>");
			background-repeat: no-repeat;
			background-position: center top;
			background-attachment: fixed;
			margin-top: -5px;
			box-shadow: 0px 2px 5px rgba(100, 100, 100, 0.49);
		}

		a#skinlink {
			position: absolute;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			display: block;
			z-index: 1;
		}

		#skinlink span {
			display: none;

		}

		/*
		AGGIUSTO IL CONTAINER
		*/

		div.container {
			position: relative;
			z-index: 2;
		}
	</style>

	<a id="skinlink" href="<?= $banners[0]['target_url'] ?>" target="_blank">
	</a>
<?php
} ?>