<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 400;
$h = (isset($parameters['h'])) ? $parameters['h'] : 150;
$blank = (isset($parameters['blank'])) ? $parameters['blank'] : '_blank';
?>
<style>
    .banner-float-list > ul {
        margin: 0;
        padding: 0px;
        list-style: none;
        background-color:  transparent;
    }
    .banner-float-list > ul > li {
        padding: 0;
        float: left;
        padding: 0 4px;
    }
    .banner-float-list > ul > li:first-child {
        padding-right: 4px;
        padding-left: 0px;
    }  
    .banner-float-list > ul > li:last-child {
        padding-left: 4px;
        padding-right: 0px;
    } 
    .banner-float-list > ul > li img{
        width: 100%;
    }

</style>

<?php if (count($banners)) {
	?>
	<div class="conainer">
		<div class="banner-float-list">
			<ul class="row" style="padding: 0px; width: 100%; list-style-type: none;">
				<?php
				foreach ($banners as $banner) {
					?>
					<li class="col-md-4 col-xs-12 banner-img">
						<a  href='<?= $banner->target_url ?>' target="<?= $blank ?>"><img src='<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h) ?>'  alt='<?= $banner->name ?>'  /></a>

					</li>
				<?php
				} ?>
			</ul>
		</div>
	</div>

<?php
} ?> 
