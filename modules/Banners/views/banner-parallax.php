<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/banner-parallax.css');
HeadHtml::AddJS('jarallax.js');

$width_md = (isset($parameters['w'])) ? $parameters['w'] : 2000;
$height_md = (isset($parameters['h'])) ? $parameters['h'] : intval($width_md / 3);
$width_sm = intval($width_md / 4);
$height_sm = intval($height_md / 4);

$padding_em = (isset($parameters['padding_em'])) ? $parameters['padding_em'] : 17;
$padding_sm_em = (isset($parameters['padding_em'])) ? $parameters['padding_em'] : 6;

if (count($banners)) {
	?>


	<?php $background_url_md = Utils::genThumbnailUrl('banners/' . $banners['0']->filename, $width_md, 0, array('q' => 95)) ?>
	<?php $background_url_sm = Utils::genThumbnailUrl('banners/' . $banners['0']->filename, $width_sm, 0, array('q' => 95)) ?>

	<style>
		/*Picture*/

		@media (min-width: 500px){
			.homeslider-jarallax-<?= $block_id ?>{
				padding: <?= $padding_em ?>em 0;
				background: url('<?= $background_url_md ?>') no-repeat 0px 0px;
				background-size:cover;
				color: #ffffff;
				height: <?= $height_md ?>px;
			}
		}
		@media (max-width: 500px){
			.homeslider-jarallax-<?= $block_id ?>{
				padding: <?= $padding_sm_em ?>em 0;
				background: url('<?= $background_url_sm ?>') no-repeat 0px 0px;
				background-size:cover;
				color: #ffffff;
				height: <?= $height_sm ?>px;
			}
		}

		.homeslider-jarallax-<?= $block_id ?> .paralaxMask {
			background: rgba(0, 149, 229, 0);
		}
		.homeslider-jarallax-<?= $block_id ?> span {
			color: #ff2121;
		}

	</style>

	<?php
}
?>

<div class="homeslider-jarallax-<?= $block_id ?> banner-jarallax">
    <div class="paralaxMask">&nbsp;</div>
    <div class="container">
        <div class="parallax-content">
			<?php
			if (isset($banners[0])) {
				echo '<div class="text1">' . $banners[0]->text1 . '</div>';
				echo '<div class="text2">' . $banners[0]->text2 . '</div>';
				echo '<div class="text3">' . $banners[0]->text3 . '</div>';
			}
			?>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        /* init Jarallax */
        if ($(window).width() > 500) {
            $('.homeslider-jarallax-<?= $block_id ?>').jarallax({
                speed: 0.5,
                imgWidth: <?= $width_md ?>,
                imgHeight: <?= $height_md ?>
            });
        } else {
            $('.homeslider-jarallax-<?= $block_id ?>').jarallax({
                speed: 0.5,
                imgWidth: <?= $width_sm ?>,
                imgHeight: <?= $height_sm ?>
            });
        }
    });

</script>
