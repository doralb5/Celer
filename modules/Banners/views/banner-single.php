<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 0;
$h = (isset($parameters['h'])) ? $parameters['h'] : 0;
$blank = (isset($parameters['blank'])) ? $parameters['blank'] : '_blank';
?>
<?php if (count($banners)) {
	?>
	<div class="single-banner">
		<?php
		foreach ($banners as $banner) {
			?>
			<?php if ($banner->target_url != '') {
				?>
				<a href="<?= $banner->target_url ?>" target="<?= $blank ?>"><img
						src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h) ?>" width="100%"/>
				</a>
			<?php
			} else {
				?>
				<img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h) ?>" width="100%"/>

			<?php
			} ?>
		<?php
		} ?>
	</div>
<?php
} ?>