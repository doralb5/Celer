<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 300;
$h = (isset($parameters['h'])) ? $parameters['h'] : 300;
$blank = (isset($parameters['blank'])) ? $parameters['blank'] : '_blank';
?>

<?php if (count($banners)) {
	?>
	<div class="banners">
		<ul class="row" style="list-style-type: none; margin: 0; padding-left: 0px;   padding-top: 0px;">
			<?php
			foreach ($banners as $banner) {
				?>
				<li class="vertical-li col-xs-12 col-sm-12">
					<a href="<?= $banner->target_url ?>" target="<?= $blank ?>">
						<img  class="imagemargin" width="100%" src='<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h, array('far' => '1', 'bg' => 'FFFFFF')) ?>'  alt='<?= $banner->name ?>' /></a>
					</a>
				</li>
			<?php
			} ?>
		</ul>
	</div>
<?php
} ?> 