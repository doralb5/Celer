<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 360;
$h = (isset($parameters['h'])) ? $parameters['h'] : 220;
?>

<?php if (count($banners)) {
	?>
	<ul class="row">
		<?php
		foreach ($banners as $banner) {
			?>
			<li class="col-md-4 col-sm-4 col-xs-12">
				<img  class="img-rounded imagemargin" width="100%" src='<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h) ?>'  alt='<?= $banner->name ?>' /></a>

			</li>
		<?php
		} ?>
	</ul>


<?php
} ?> 
