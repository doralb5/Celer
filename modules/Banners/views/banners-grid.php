<?php $w = (isset($parameters['w']) && $parameters['w'] > 0) ? $parameters['w'] : 400 ?>
<?php $h = (isset($parameters['h']) && $parameters['h'] > 0) ? $parameters['h'] : 200 ?>


<div id="banners">
	<?php foreach ($banners as $banner) {
	?>
		<div class="row">
			<div class="col-md-12">

				<div class="main-banners-content pleft wow fadeInLeft animated" data-wow-delay=".8s">

					<div class="main-picture">
						<img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h, array('zc' => 1)) ?>" width="100%"/>
					</div>


					<div class="main-title">
						<?= $banner->text1 ?>
					</div>

				</div>

			</div>
		</div>
	<?php
} ?>
</div>