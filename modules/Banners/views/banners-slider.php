<?php if (count($banners)) {
	?>
	<?php $w = (isset($parameters['w']) && $parameters['w'] > 0) ? $parameters['w'] : 1000 ?>
	<?php $h = (isset($parameters['h']) && $parameters['h'] > 0) ? $parameters['h'] : 350 ?>
	<?php $interval = (isset($parameters['interval']) && $parameters['interval'] > 0) ? $parameters['interval'] : 5000 ?>
	<?php $show_caption = (isset($parameters['show_caption'])) ? $parameters['show_caption'] : 0 ?>
	<?php $show_controls = (isset($parameters['show_controls'])) ? $parameters['show_controls'] : 0 ?>

	<style>
		.carousel-fade .carousel-inner .next,
		.carousel-fade .carousel-inner .prev,
		.carousel-fade .carousel-inner .active.left,
		.carousel-fade .carousel-inner .active.right {
			left: 0;
			transform: translate3d(0, 0, 0);
		}

		/*Background Carousel*/

		.carousel-fade .carousel-inner .item {
			transition-property: opacity;
		}
		.carousel-fade .carousel-inner .item,
		.carousel-fade .carousel-inner .active.left,
		.carousel-fade .carousel-inner .active.right {
			opacity: 0;
		}
		.carousel-fade .carousel-inner .active,
		.carousel-fade .carousel-inner .next.left,
		.carousel-fade .carousel-inner .prev.right {
			opacity: 1;
		}
		.carousel-fade .carousel-inner .next,
		.carousel-fade .carousel-inner .prev,
		.carousel-fade .carousel-inner .active.left,
		.carousel-fade .carousel-inner .active.right {
			left: 0;
			transform: translate3d(0, 0, 0);
		}

		.description {
			position: absolute;
			z-index: 5;
			display: block;
			top: 12em;
			background: hsla(201,100%,45%,0);
			padding: 20px;
			border-radius: 2px;
		}

	</style>

	<div id="myCarousel_<?= $block_id ?>" class="carousel-fade carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<?php
			$i = 0;
	foreach ($banners as $banner) {
		?>
				<li data-target="#myCarousel_<?= $block_id ?>" data-slide-to="<?= $i ?>" class="<?= ($i == 0) ? 'active' : '' ?>"></li>
				<?php
				$i++;
	} ?>
		</ol>

		<!-- Wrapper for Slides -->
		<div class="carousel-inner">

			<?php
			$i = 0;
	foreach ($banners as $banner) {
		?>
				<?php HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl('banners/' . str_replace('%2F', '/', urlencode($banner->filename)), 0, 0, array(), true)); ?>

		                                                        <!--            <div class="item <?= ($i == 0) ? 'active' : '' ?>">
		                                                        <div class="fill" style="background-image:url('<?= MEDIA_ROOT . 'banners/' . $banner->filename ?>');"></div>
		                                                        <div class="carousel-caption">
		                                                        </div>
		                                                        </div>-->

				<div class="item <?= ($i == 0) ? 'active' : '' ?>">
					<?php if ($banner->target_url != '') {
			?>
						<a href="<?= $banner->target_url ?>"><img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h, array('zc' => 1)) ?>" width="100%"/></a>
					<?php
		} else {
			?>
						<img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h, array('zc' => 1)) ?>" width="100%"/>

						<div class="container">    
							<?php if ($banner->text1 || $banner->text2 != '') {
				?>
								<div class="description">
									<p class="first-text wow fadeInUp animated" data-wow-delay=".5s"><?= $banner->text1 ?></p>
									<label class=""></label>
									<p class="second-text wow fadeInUp animated" data-wow-delay="1s"><?= $banner->text2 ?></p>
								</div>
							<?php
			} ?>
						</div>


					<?php
		} ?>
					<?php if ($show_caption) {
			?>
						<div class="carousel-caption">
							<?= $banner->name ?>
						</div>
					<?php
		} ?>

				</div>

				<?php
				$i++;
	} ?>

		</div>

		<!-- Controls -->
		<?php if ($show_controls) {
		?>

			<a class="left carousel-control " href="#myCarousel_<?= $block_id ?>" data-slide="prev"><span class="icon-prev"></span>  
				<a class="right carousel-control " href="#myCarousel_<?= $block_id ?>" data-slide="next"><span class="icon-next"></span></a>
				<?php
	} ?> 

	</div>

	<script type="text/javascript">
		$(document).ready(function () {
			$('#myCarousel_<?= $block_id ?>').carousel({
				interval: <?= $interval ?>,
				pause: 'hover',
				cycle: true
			});
		});
	</script>

<?php
} ?>
