<?php
if (count($banners)) {
	$effect = (isset($parameters['effect'])) ? $parameters['effect'] : 'bulb';
	$interval = (isset($parameters['interval'])) ? $parameters['interval'] : '5000';

	$w = (isset($parameters['w'])) ? $parameters['w'] : 0;
	$h = (isset($parameters['h'])) ? $parameters['h'] : 0; ?>
	<script>
		function slidersize() {
			var h = $(window).height();
			var w = $(window).width();
			if (w <= 499) {
				$(".cs3 .cs3-slide img").css("width", w);
				$(".cs3 .cs3-slide img").css("height", h / 2);

				$(".cs3 ").css("width", w);
				$(".cs3 ").css("height", h / 2);
			} else {
				$(".cs3 .cs3-slide img").css("width", w);
				$(".cs3 .cs3-slide img").css("height", h);

				$(".cs3 ").css("width", w);
				$(".cs3 ").css("height", h);
			}
		}
		$(document).ready(function () {
			slidersize();
			window.onresize = slidersize;

		});
	</script>

	<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/idangerous.chopslider-3.4.css'); ?>
	<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/idangerous.chopslider-3.4.min.js'); ?>

	<div class="cs3-wrap cs3-skin-no">
		<div class="cs3">
			<?php foreach ($banners as $banner) {
		?>

				<?php if ($banner->target_url != '') {
			?>
					<div class="cs3-slide"><a href="<?= $banner->target_url ?>"><img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h) ?>"/></a></div>
				<?php
		} else {
			?>
					<div class="cs3-slide"><img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h) ?>"/></div>
				<?php
		} ?>

			<?php
	} ?>
			<div class="cs3-pagination-wrap">
				<div class="cs3-pagination"></div>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function () {
			var cs3 = $('.cs3').cs3({
				pagination: {
					container: '.cs3 .cs3-pagination'
				},
				autoplay: {
					enabled: true,
					delay: <?= $interval ?>
				},
				responsive: true,
				effects: "<?= $effect ?>"
			});

		});
	</script>

<?php
} ?>
