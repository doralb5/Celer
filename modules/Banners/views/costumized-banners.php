<?php $w = (isset($parameters['w']) && $parameters['w'] > 0) ? $parameters['w'] : 0 ?>
<?php $h = (isset($parameters['h']) && $parameters['h'] > 0) ? $parameters['h'] : 0 ?>
<div class="content-top">
    <div class="row">
        <div class="col-md-6">
			<?php
			if (count($banners)) {
				for ($i = 0; $i < count($banners); $i = $i + 2) {
					?>

					<div class="col-3 wow fadeInLeft animated" data-wow-delay=".5s">
						<a href="#" class="b-link-stroke b-animate-go  thickbox">
							<img
								src="<?= WEBROOT . MEDIA_ROOT . 'banners/' . $banners[$i]->filename ?>"
								class="img-responsive" alt=""/>
							<div class="<?= ($i == 0) ? 'b-wrapper1 long-img' : 'col-pic' ?>">
								<p class="b-animate b-from-right b-delay03 "><?= $banners[$i]->text1 ?></p>
								<label class="b-animate b-from-right b-delay03 "></label>
								<h3 class="b-animate b-from-left b-delay03 "><?= $banners[$i]->text2 ?></h3>
							</div>
						</a>
					</div>

					<?php
				}
			}
			?>
        </div>
        <div class="col-md-6">
			<?php
			if (count($banners)) {
				for ($i = 1; $i < count($banners); $i = $i + 2) {
					?>
					<div class="col-3 wow fadeInRight animated" data-wow-delay=".5s">
						<a href="#" class="b-link-stroke b-animate-go  thickbox">
							<img
								src="<?= WEBROOT . MEDIA_ROOT . 'banners/' . $banners[$i]->filename ?>"
								class="img-responsive" alt=""/>
							<div class="<?= ($i == 1) ? 'col-pic' : 'b-wrapper1 long-img' ?>">
								<p class="b-animate b-from-right b-delay03 "><?= $banners[$i]->text1 ?></p>
								<label class="b-animate b-from-right b-delay03 "></label>
								<h3 class="b-animate b-from-left b-delay03 "><?= $banners[$i]->text2 ?></h3>
							</div>
						</a>
					</div>
					<?php
				}
			}
			?>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
</div>
