<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/owl-carousel-v2.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/owl-theme.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'owl-carousel-v2.js'); ?>
<?php $w = (isset($parameters['w']) && $parameters['w'] > 0) ? $parameters['w'] : 1000 ?>
<?php $h = (isset($parameters['h']) && $parameters['h'] > 0) ? $parameters['h'] : 350 ?>

<style>
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }
    #bar{
        width: 0%;
        max-width: 100%;
        height: 4px;
        background: #7fc242;
    }
    #progressBar{
        width: 100%;
        background: #EDEDED;
    }
</style>

<?php if (count($banners)) {
	?>

	<div id="demo">
		<div class="row">
			<div class="col-md-12">
				<div id="owl-demo" class="owl-carousel">
					<?php foreach ($banners as $banner) {
		?>
						<div class="item">
							<a href="<?= $banner->target_url ?>">
								<img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h, array('zc' => 1)) ?>" alt="">
							</a>
						</div>
					<?php
	} ?> 

				</div>
			</div>
		</div>
	</div>

<?php
} ?>
<script>
    $(document).ready(function () {

        var time = 7; // time in seconds

        var $progressBar,
                $bar,
                $elem,
                isPause,
                tick,
                percentTime;

        //Init the carousel
        $("#owl-demo").owlCarousel({
            slideSpeed: 500,
            paginationSpeed: 500,
            singleItem: true,
            afterInit: progressBar,
            afterMove: moved,
            startDragging: pauseOnDragging
        });

        //Init progressBar where elem is $("#owl-demo")
        function progressBar(elem) {
            $elem = elem;
            //build progress bar elements
            buildProgressBar();
            //start counting
            start();
        }

        //create div#progressBar and div#bar then prepend to $("#owl-demo")
        function buildProgressBar() {
            $progressBar = $("&lt;div&gt;", {
                id: "progressBar"
            });
            $bar = $("&lt;div&gt;", {
                id: "bar"
            });
            $progressBar.append($bar).prependTo($elem);
        }

        function start() {
            //reset timer
            percentTime = 0;
            isPause = false;
            //run interval every 0.01 second
            tick = setInterval(interval, 10);
        }
        ;

        function interval() {
            if (isPause === false) {
                percentTime += 1 / time;
                $bar.css({
                    width: percentTime + "%"
                });
                //if percentTime is equal or greater than 100
                if (percentTime >= 100) {
                    //slide to next item 
                    $elem.trigger('owl.next')
                }
            }
        }

        //pause while dragging 
        function pauseOnDragging() {
            isPause = true;
        }

        //moved callback
        function moved() {
            //clear interval
            clearTimeout(tick);
            //start again
            start();
        }

        //uncomment this to make pause on mouseover 
        // $elem.on('mouseover',function(){
        //   isPause = true;
        // })
        // $elem.on('mouseout',function(){
        //   isPause = false;
        // })

    });
</script>
