<?php HeadHtml::linkStylesheet('owl.carousel.css'); ?>
<?php HeadHtml::linkJS('owl.carousel.js'); ?>
<?php $itemc = (isset($parameters['itemc']) && $parameters['itemc'] > 0) ? $parameters['itemc'] : 5 ?>
<?php $width = (isset($parameters['width']) && $parameters['width'] > 0) ? $parameters['width'] : 100 ?>
<?php $height = (isset($parameters['height']) && $parameters['height'] > 0) ? $parameters['height'] : 70 ?>

<?php if (count($banners)) {
	?>
	<div class="owl-carousel owl-theme owl-image-edit-width">
		<?php foreach ($banners as $banner) {
		?>
			<div class="item text-center">
				<a href="<?= $banner->target_url ?>">
					<img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $width, $height) ?>">
				</a>
			</div>
		<?php
	} ?>
	</div>
<?php
} ?>


<script>
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            items: <?= $itemc ?>,
            loop: true,
            margin: 15,
            stagePadding: 0,
            nav: false,
            autoPlay: 3000, //Vecchia versione OWL
            autoplay: true, //Nuova versione OWL
            autoplayTimeout: 3000,
            autoplayHoverPause: false,
            dots: false,
            pagination: false
        });
    });
</script>