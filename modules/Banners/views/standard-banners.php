<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 360;
$h = (isset($parameters['h'])) ? $parameters['h'] : 220;
?>

<?php if (count($banners)) {
	?>

	<!-- Standard Banners -->
	<div id="slides<?= $block_id ?>">
		<ul class="slides-container">
			<?php
			foreach ($banners as $banner) {
				?>
				<li>
					<img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h) ?>" alt="<?= $banner->name ?>">
					<div class="container"></div>
				</li>
			<?php
			} ?>
		</ul>
	</div>
	<script>

		$('#slides').superslides({
			hashchange: false,
			animation: 'fade',
			animation_easing: 'easing',
			animation_speed: 'slow',
			play: 8000
		});

	</script>


<?php
} ?> 