
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/superslides.css'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.animate-enhanced.min.js'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.superslides.min.js'); ?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 0;
$h = (isset($parameters['h'])) ? $parameters['h'] : 0;
?>
<?php if (count($banners)) {
	?>
	<!--Slider-->
	<div id="slides">
		<ul class="slides-container">
			<?php foreach ($banners as $banner) {
		?>

				<?php if ($banner->target_url != '') {
			?>
					<li>
						<a href="<?= $banner->target_url ?>"><img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h) ?>" width="100%"/></a>
						<div class="container">

						</div>
					</li>
				<?php
		} else {
			?>
					<li>
						<img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $w, $h) ?>" width="100%" alt="">

						<div class="container">

						</div>
					</li>
				<?php
		} ?>
			<?php
	} ?>
		</ul>
	</div>

	<script>
		$('#slides').superslides({
			hashchange: false,
			animation: 'fade',
			animation_easing: 'easing',
			animation_speed: 'slow',
			play: 8000
		});

	</script>


<?php
} ?>
