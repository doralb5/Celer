<?php

class BrandsList extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->model = Loader::getModel('BSProducts', 'BlueShopV1');

		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 0;
		$order = (isset($parameters['order'])) ? $parameters['order'] : false;

		$brands = $this->model->getBrandsList($limit, $order);
		$this->view->set('brands', $brands);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'brands-list';
		$this->view->render($view);
	}
}
