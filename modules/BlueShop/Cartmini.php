<?php

class Cartmini extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->model = Loader::getModel('Orders', 'BlueShopV1');

		// @var $cart O_cart
		$qta = 0;
		if (isset($_SESSION['cart0']) && $_SESSION['cart'] != false) {
			$cart = $_SESSION['cart'];
			$qta = $cart->qta;
		} else {
			$user_id = '';
			if (isset($_SESSION['user_id'])) {
				$user_id = $_SESSION['user_id'];
			}
			$cart = $this->model->getCart($user_id, session_id());
			if (!$cart) {
				$qta = 0;
			} else {
				$qta = $cart->qta;
			}
		}

		$this->view->set('cart', $cart);
		$this->view->set('qta', (int) $qta);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'cartmini';
		$this->view->render($view);
	}
}
