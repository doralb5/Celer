<?php

class CategoriesMenu extends BaseModule
{
	public function __construct($block_id, $position, $package)
	{
		parent::__construct($block_id, $position, $package);
		require_once CONFIG_PATH . 'blueshop.php';
		$this->model = Loader::getModel('BSProducts', 'BlueShopV1');
	}

	public function execute($parameters = array())
	{
		if (isset($_GET['id_cat'])) {
			$id = $_GET['id_cat'];
		} else {
			$id = (isset($parameters['id'])) ? $parameters['id'] : 0;
		}
		$params = array(
			'id_cat' => $id,
			'explode' => 3,
			'view' => 'default',
			'baseAlias' => 'default',
			'withName' => true,
			'withNum_Art' => false,
			'withImg' => false,
			'name' => '',
			'orderby' => 'default'
		);
		$params_view = array(
			'baseAlias' => 'default',
			'withName' => true,
			'withNum_Art' => false,
			'withImg' => false,
			'name' => '',
			'class' => '',
			'back' => false
		);

		foreach ($parameters as $k => $v) {
			$params[$k] = $v;
			$params_view[$k] = $v;
		}

		if ($params['orderby'] != 'default') {
			$categories = $this->model->getCategoriesLevels($params['id_cat'], $params['explode'], $params['orderby']);
		} else {
			$categories = $this->model->getCategoriesLevels($params['id_cat'], $params['explode']);
		}

		for ($i = 0; $i < count($categories); $i++) {
			if ($categories[$i]->withphoto == '1') {
				$categories[$i]->img = $this->model->getImgUrl($categories[$i]->id, 'cat');
			} else {
				$categories[$i]->img = null;
			}
			$categories[$i]->link = Utils::getComponentUrl('BlueShop/Products/getListProducts/' . $categories[$i]->getSeoUrl(), true);
		}

		if (isset($_GET['id_cat']) && $_GET['id_cat'] != 0) {
			$category = $this->model->getCategory($params['id_cat']);
			//$this->view->top_category=$this->model->getCategory($category->id_top_cat);
			$this->view->set('top_category', $category);
		} else {
			$this->view->set('top_category', null);
		}

		$this->view->set('categories', $categories);
		$this->view->set('parameters', $parameters);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'categories-menu';
		$this->view->set('currentCat', $this->model->getCategory(BSProducts_Model::getCurrentCat()));
		$this->view->render($view);

		return;

		$currentLang = FCRequest::getLang();
		$allCategories = $this->model->getProdCategories(' abilitata=1 ', ' ordine,categoria_it desc ');
		$this->view->set('allCategories', $allCategories);

		$mainCategs = $this->model->getProdCategories(' abilitata=1 and id_top_cat = 0 ', ' ordine,categoria_it desc ');
		$this->view->set('mainCategs', $mainCategs);

		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'categories-menu';
		$this->view->render($view);
	}
}
