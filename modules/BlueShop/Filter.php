<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Filter
 *
 * @author alessandro
 */
class Filter extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->model = Loader::getModel('BSProducts', 'BlueShopV1');
		$params = array(
			'id' => 0,
			'name' => 'Filtro',
			'view' => 'default',
			'baseAlias' => 'default'
		);
		$params_view = array(
			'name' => 'Filto',
			'baseAlias' => 'default'
		);
		//        if(!$this->PositionParams){
//
		//        }
		//        else {
		//            foreach ($params as $key => $value){
		//                if(array_key_exists($key, $this->PositionParams)){
		//                    $params[$key]=$this->PositionParams[$key];
		//                }
		//            }
		//            foreach ($params_view as $key => $value){
		//                if(array_key_exists($key, $this->PositionParams)){
		//                    $params_view[$key]=$this->PositionParams[$key];
//
		//                }
		//            }
		//        }
		$valori = $this->model->getValueOfFeature($params['id']);
		$this->view->set('valori', $valori);
		$this->view->set('parameters', $parameters);
		$this->view->set('params', $params);
		$this->view->set('params_view', $params_view);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'filter';
		$this->view->render($view);
	}
}
