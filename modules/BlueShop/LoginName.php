<?php

class LoginName extends BaseModule
{
	public function execute($parameters = array())
	{
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'login';
		$this->view->render($view);
	}
}
