<?php

class ProductList extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->model = Loader::getModel('BSProducts', 'BlueShopV1');
		$params_view = array(
			'prezzi_ivati' => true,
			'all_products' => 1,
			'featured' => false,
			'offerta' => false,
			'novita' => false,
			'classe' => '',
			'order' => 'prezzo_desc',
			'w_img' => 400,
			'h_img' => 400,
			'cols' => 4,
			'limit' => 8,
			'title' => '',
			'showFlags' => false,
			'flag' => '', //vetrina, novita, promo
			'tags' => '', //tags del prodotto separati da virgola
			'filter' => '', //keywords - stringa generica di ricerca
			'pagination' => 'true',
			'currency' => BS_session::getValuta(),
			'curr_exchange' => BS_session::getCambio()
		);

		$params_view['limit'] = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$featured = (isset($parameters['featured'])) ? $parameters['featured'] : '';
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';

		$params = array('primo_piano');

		//$num_products = $this->model->getCount('', '', '', true, $params);
		$num_products = 0;
		$products = $this->model->getProductSearch('', $params_view['order'], $params_view['limit'], '', null, true, $params, '', $num_products);
		if (is_array($products)) {
			for ($i = 0; $i < count($products); $i++) {
				if (!is_null($products[$i])) {
					$products[$i]->img_url = $this->model->getImgUrl($products[$i]->img);
					$products[$i]->getCaratteristiche();
				} else {
					unset($products[$i]);
				}
			}
		}
		$params_view['valuta'] = BS_session::getValuta();
		$params_view['cambio'] = BS_session::getCambio();
		$this->view->set('num_art', $num_products);
		$this->view->set('products', $products);
		//        foreach ($products as $product) {
		//            print_r($product->img);
		//            echo '<br>';
		//        }
		//        exit;
		$this->view->set('num_page', ceil($num_products));
		$this->view->set('params', $params_view);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'products-list';
		$this->view->render($view);
	}

	//    private function oldAction($parameters = array()) {
//
//        $limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
//        $featured = (isset($parameters['featured'])) ? $parameters['featured'] : '';
//        $order = (isset($parameters['order'])) ? $parameters['order'] : '';
//
//        $products_md = Loader::getModel('Products', 'BlueShop');
//
//        $filter = "1 ";
//        if ($featured != '') {
//            $filter .= "AND featured = '{$featured}' ";
//        }
//
//        $products = $products_md->getList($limit, 0, $filter, $order);
//        $this->view->set('parameters', $parameters);
//        $this->view->set('products', $products);
//        $view = (isset($parameters['view'])) ? $parameters['view'] : 'products-list';
//        $this->view->render($view);
//
//    }
}
