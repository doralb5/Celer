<?php

class SearchProducts extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->model = Loader::getModel('BSProducts', 'BlueShopV1');

		$products = $this->model->getProductSearch('');

		$this->view->set('products', $products);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'search-products';
		$this->view->render($view);
	}
}
