<?php

class ReservedArea extends BaseModule
{
	public function execute($parameter = array())
	{
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'reserved-area';
		$this->view->render($view);
	}
}
