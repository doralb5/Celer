

<div id="cartmini" class="cartmini">

    <div class="cart-title"><i class="fa fa-shopping-cart"></i> Carello</div>

    <div class="cart-summary">
        <a href="">1 Prodotto - 5.00 €</a>

        <div class="cart-list">

            <ul>
                <li>
                    <a href=""><img class="img_prodotto" src="http://www.italiatotaste.com/module/BlueShop/BlueShop/getImgFromBlob/prod/2643?w=60&h=60&bg=ffffff"/></a>
                    <a href="">
                        <div class="cart-detail">
                            <div class="cart-detail-title">
                                Sapori Antichi Capperi in Aceto vaso da 314 cc                            
                            </div>
                            <div class="cart-detail-price">
                                5.00 €                            
                            </div>
                        </div>
                    </a>
                </li>
            </ul>

            <div class="cart-footer">
                <div class="cartmini-tot">5.00 €</div>
                <a class="button-mini btn-checkout" href="">Checkout</a>
            </div>
            <!--            Il tuo carrello è vuoto-->
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {

        var MenuTimeout;
        var cartMenu;
        cartMenu = $('#cartmini .cart-summary');
        var cartMenuList;
        cartMenuList = $('#cartmini .cart-summary > .cart-list');

        cartMenu.mouseenter(function () {
            clearTimeout(MenuTimeout);
            cartMenuList.slideDown('normal', 'swing');
        });

        cartMenu.mouseleave(function () {
            MenuTimeout = window.setTimeout(function () {
                cartMenuList.stop().slideUp('fast', 'linear')
            }, 500);
        });
    });


    function doScroll()
    {

        var scroll = $(".cart_content_mini_scroll");
        for (var i = 0; i < scroll.length; i++)
        {
            var target = $(scroll[i]);
            var height = target.prop("scrollHeight");
            cart_animate = false;


            var css_height = target.css("height").replace("px", "");
            if (height > css_height)
            {
                if (target.parent().attr("class") != "scroll-container")
                {

                    target.wrap('<div class="scroll-container"></div>');
                    var container = target.parent();
                    container.append('<div class="scroll-slider-wrapper" style="padding:0"><div class="scroll-slider" style="height:' + (css_height - 26) + 'px"></div></div>');
                    var scroll = container.find(".scroll-slider");

                    scroll.slider({
                        orientation: "vertical",
                        animate: 400,
                        start: checkType,
                        slide: doSlide,
                        change: doSlide,
                        value: 100
                    });

                    scroll.parent().fadeIn(200);
                } else
                {
                    var container = target.parent();
                    var scroll = container.find(".scroll-slider");
                    scroll.parent().fadeIn(200);
                }

                target.parent().bind('mousewheel',
                        function (e)
                        {
                            var delta = 5;
                            if (e.originalEvent.wheelDelta / 120 > 0)
                            {
                                var scroll = $(this).find(".scroll-slider");
                                var value = scroll.slider("value") + e.originalEvent.wheelDelta * delta / 120;

                                if (value <= 100)
                                {
                                    scroll.slider("value", value);
                                } else
                                {
                                    scroll.slider("value", 100);
                                }

                            } else
                            {
                                var scroll = $(this).find(".scroll-slider");
                                var value = scroll.slider("value") + e.originalEvent.wheelDelta * delta / 120;

                                if (value > 0)
                                {
                                    scroll.slider("value", value);
                                } else
                                {
                                    scroll.slider("value", 0);
                                }
                            }
                        });

            }
        }
    }

</script>
