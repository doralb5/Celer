<?php require_once('functions.php'); ?>

<nav class="navbar navbar-default" id="customized-menu">
    <div class="
         ">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class='nav navbar-nav level_1'>
				<?php
				$mainCategs = array();
				foreach ($categories as $cat) {
					if ($cat->id_top_cat == 0) {
						array_push($mainCategs, $cat);
					}
				}

				foreach ($mainCategs as $categ) {
					$subCategs = array(); // hasChild($categories, $categ->id);

					if (count($subCategs)) {
						?>
						<li class="dropdown mega-dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
															  aria-haspopup="true"
															  aria-expanded="false"><?= $categ->name ?></a>


							<ul class="dropdown-menu mega-dropdown-menu">
								<div class="row">
									<div class="col-md-<?= ($categ->img != '') ? 10 : 12 ?>">
										<?php foreach ($subCategs as $subCat) {
							?>
											<?php if ($subCat->name == '-') {
								?>
												<!--<div class="cat_sep"></div>-->
											<?php
							} else {
								?>
												<li><a href="<?= Utils::getComponentUrl('BlueShop/Products/products_list') . '/' . $subCat->getSeoUrl() ?>"><?= $subCat->name ?></a></li>
											<?php
							} ?>

										<?php
						} ?>
									</div>
								</div>

								<?php
								if ($categ->img != '') {
									?>
									<div class="col-md-2">
										<div class="right-div pull-right">
											<img src="<?= $categ->img ?>"/>
										</div>
									</div>
								<?php
								} ?>

							</ul>

						</li>
					<?php
					} else {
						?>
						<li><a href="#"><?= $categ->name ?></a></li>
						<?php
					}
				}
				?>
            </ul>
        </div>
    </div>
</nav>

<style>
    .mega-dropdown {
        position: static !important;
    }

    #customized-menu ul.dropdown-menu {
        /* border-left: 1px solid grey; */
        width: 100%;
        background-color: rgba(255,255,255,0.95);
        padding: 15px 0;
        border-radius: 0px;
        box-shadow: 0 6px 10px rgba(0, 0, 0, 0.1);
        border: 1px solid rgba(0, 0, 0, 0);
    }
    #customized-menu ul.dropdown-menu li {
        padding: 2px 0 2px 5px;
        float: left;
        width: 33%;
        margin-right: 0px;
        list-style: none;
        overflow: hidden;
        white-space: nowrap;
    }
    #customized-menu ul.dropdown-menu li:first-child { 
        margin-top: -5px;
    }
    #customized-menu ul.dropdown-menu li:last-child { 
        margin-bottom: -5px;
    }

    #customized-menu ul.dropdown-menu a {
        text-transform: capitalize;
        color: #3e3e3e;
        text-decoration: none;
        font-size: 13px;
        line-height: 2.5em;
        transition: 0.2s;
    }
    #customized-menu ul.dropdown-menu a:hover {
        color: black;
    }
    #customized-menu ul.dropdown-menu .cat_sep {
        height: 20px;
        overflow: hidden;
        float: left;
        width: 165px;
        margin-right: 4px !important;
        font-size: 13px;
        background: url('http://desta.weweb.al/data/desta.weweb.al/media/images/sep_li.png') no-repeat center center;
    }

    #customized-menu ul.dropdown-menu li:hover {
        transition: none;
        /*background: rgba(238,232,197,0.4);*/
        cursor: pointer;
    }

    /*    nav#customized-menu .right-div {
            position: absolute;
            top: 0;
            left: 170px;
            padding: 10px;
        }*/

    #customized-menu {
        background-color: transparent;
    }

    .navbar-default .navbar-nav > li > a {
        color: white !important;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: bold;
    }

    .navbar-default .navbar-nav > li > a:hover {
        color: #E5B951 !important;
    }

    .navbar-default .navbar-nav > li.open > a:focus {
        color: #E5B951 !important;
        background-color: transparent;
    }

    #customized-menu ul.level_1 {
        float: none;
        text-align: center;
    }

    #customized-menu ul.level_1 > li {
        display: inline-block;
        float: none;
    }


</style>
