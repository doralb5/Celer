<?php

function hasChild($allCategs, $id)
{
	$retArray = array();
	foreach ($allCategs as $c) {
		if ($c->id_top_cat == $id) {
			array_push($retArray, $c);
		}
		$retArray = array_merge($retArray, hasChild($c->subcategory, $id));
	}

	return $retArray;
}

function printMenu($categories, $allCategories)
{
	global $i;
	$i++;

	if ($i == 1) {
		echo "<ul class='nav navbar-nav level_$i'>";
	} elseif ($i == 2) {
		echo "<ul class='dropdown-menu multi-level level_$i'>";
	} else {
		echo "<ul class='dropdown-menu level_$i'>";
	}

	foreach ($categories as $categ) {
		$retArray = hasChild($allCategories, $categ->id);
		if (count($retArray)) {
			if ($i == 2) {
				echo '<li class="dropdown-submenu">';
			} else {
				echo ' <li class="dropdown">';
			}

			echo '<a href = "#" class="dropdown-toggle" data-toggle = "dropdown" role = "button" aria-haspopup = "true"
                       aria-expanded = "false" > ' . $categ->categoria_it . '</a>';
			printMenu($retArray, $allCategories);
			$i--;
			echo '</li>';
		} else {
			echo '<li><a href = "' . Utils::getComponentUrl('BlueShop/Products/products_list') . $categ->id . '" > ' . $categ->categoria_it . '</a ></li > ';
		}
	}
	echo '</ul>';
}
