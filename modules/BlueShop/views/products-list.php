<?php
require_once COMPONENTS_PATH . 'BlueShop' . DS . 'Products.php';
$no_photo_url = 'http://www.desta.it/content/media/images/noimage.jpg';
?>
<!--products-->
<div class="products">
    <div class="content-mid">
        <h3 class="wow bounceInUp animated">PRODOTTI IN EVIDENZA</h3>
        <label class="line"></label>
        <div class="container">

			<?php if (count($products)) {
	?>

				<div class="mid-popular">
					<div class="banner-carousel">
						<div id="owl-carousel" class="owl-carousel text-center">
							<?php $i = 0; ?>

							<?php
							$prd = new Products_Component();

	foreach ($products as $product) {
		$i++; ?>

								<div class=" item-grid simpleCart_shelfItem item">
									<div class="mid-pop">
										<div class="pro-img wow fadeInLeft animated" data-wow-delay="1.1s">
											<?php if ($product->img_url != '') {
			?>
												<img style="width:100%" class="lazyOwl" src="<?= $product->img_url ?>?w=<?= $params['w_img'] ?>&h=<?= $params['h_img'] ?>&zc=1&far=1&bg=FFFFFF" />
											<?php
		} else {
			?>
												<img style="width:100%" class="lazyOwl" src="<?= $no_photo_url ?>" />
											<?php
		} ?>
											<div class="zoom-icon ">
												<h6>
													<a href="<?= Utils::getComponentUrl('BlueShop/Products/product_details') . "/{$product->id}" ?>"><?= $product->titolo ?></a>
												</h6>

												<?php
												if (is_null($product->prezzo_variante_min)) {
													$prezzo = ($product->prezzi_ivati) ? $product->prezzo_vendita_ivato : $product->prezzo_vendita;
												} else {
													echo "<p class='price-start-from'>a partire da</p>";
													$prezzo = ($product->prezzi_ivati) ? $product->prezzo_variante_min_ivato : $product->prezzo_variante_min;
												} ?>
												<p><label><?= Utils::price_format($prezzo, $params['currency']) ?></label></p>

												<a href="<?= Utils::getComponentUrl('BlueShop/Products/product_details') . "/{$product->id}" ?>" class="buy-now">Acquista</a>
											</div>
										</div>
									</div>
								</div>
								<?php if ($i % 4 == 0) {
													?>
									<div class="clearfix"></div>
								<?php
												} ?>

							<?php
	} ?>


							<div class="clearfix"></div>
						</div>

					</div>
				</div>

			<?php
} ?>

        </div>
    </div>
</div>
<!--//products-->

<script>
    $(document).ready(function () {
        $("#owl-carousel").owlCarousel({
            center: true,
            loop: false,
            items: 3,
            lazyLoad: false,
            autoPlay: true,
            pagination: false,
            slideBy: 3,
            scrollPerPage: true,
            navigation: true

        });
    });
</script>