<?php

printMenu($mainCategs, $allCategories);

$i = -1;

function printMenu($categories, $allCategories)
{
	global $i;
	$i++;
	echo "<ul class=level_'$i'>";
	foreach ($categories as $categ) {
		$retArray = hasChild($allCategories, $categ->id);
		if (count($retArray)) {
			echo '<li>' . $categ->categoria_en;
			printMenu($retArray, $allCategories);
			echo '</li>';
		} else {
			echo '<li>' . $categ->categoria_en . '</li>';
		}
	}
	echo '</ul>';
}

function hasChild($allCategs, $id)
{
	$retArray = array();
	foreach ($allCategs as $c) {
		if ($c->id_top_cat == $id) {
			array_push($retArray, $c);
		}
	}

	return $retArray;
}
