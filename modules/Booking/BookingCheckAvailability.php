<?php

class BookingCheckAvailability extends BaseModule
{
	public function __construct($block_id = '', $position = '', $package = '')
	{
		parent::__construct($block_id, $position, $package);

		$this->model = Loader::getModel('Booking');
		$this->id_place = 2;
	}

	public function execute($params = array())
	{
		$this->view->set('url_form_action', Utils::getComponentUrl('Booking/booking'));

		$this->view->set('roomtypes_options', $this->model->getRoomTypes());
		$this->view->set('guests_options', $this->model->getMaxGuests());
		$this->view->render('checkavailability-box');
	}
}
