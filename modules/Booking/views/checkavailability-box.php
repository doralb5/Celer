<?php
$checkin = (isset($_GET['checkin']) ? $_GET['checkin'] : date('Y-m-d'));
$checkout = (isset($_GET['checkout']) ? $_GET['checkout'] : date('Y-m-d', strtotime(date('Y-m-d') . ' + 2 days')));
$guests = (isset($_GET['guests']) ? $_GET['guests'] : '1');
$children = (isset($_GET['children']) ? $_GET['children'] : '0');
?>

<!-- Search Bar Start -->
<div class="search-bar">
    <div class="search-bar-inner">
        <form action="<?= $url_form_action; ?>" method="get">
            <input type="hidden" name="check_availability" value="1"/>
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group"><label for="checkin">[$Check_In]</label> <input name="checkin" class="flatpickr flatpickr-input" id="checkin" placeholder="[$Checkin_Date]" type="text" value="<?= $checkin ?>" /></div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group"><label for="checkout">[$Check_Out]</label> <input name="checkout" class="flatpickr flatpickr-input" id="checkout" placeholder="[$Checkout_Date]" type="text"  value="<?= $checkout ?>"/></div>
                </div>

                <div class="col-lg-1">

                    <div class="form-group"><label for="guests">[$Guests]</label> 
                        <select name="guests" class="form-control" id="guests">
							<?php
							$max = 1;
							foreach ($roomtypes_options as $type) {
								if ($max < $type->sleeps) {
									$max = $type->sleeps;
								}
							}
							?>

							<?php
							for ($i = 1; $i <= $max; $i++) {
								$selected = ($guests == $i) ? 'selected' : ''; ?>
								<option <?= $selected ?>><?= $i ?></option>
							<?php
							} ?>
                        </select>
                    </div>

                </div>

                <div class="col-lg-1">

                    <div class="form-group"><label for="children">[$Children]</label> 
                        <select name="children" class="form-control" id="children">

							<?php
							for ($i = 0; $i <= $max - 1; $i++) {
								$selected = ($children == $i) ? 'selected' : ''; ?>
								<option <?= $selected ?>><?= $i ?></option>
							<?php
							} ?>
                        </select>
                    </div>

                </div>

                <div class="col-lg-4">
                    <div class="form-group"><label>&nbsp;</label><button class="btn btn-danger btn-block">[$Check_Availability]</button></div>
                </div>
            </div>
        </form>

        <div class="clearfix">&nbsp;</div>
    </div>
</div>
<!--/ Search Bar End -->


<style>
    #guests,#children {
        padding: 5px 4px;
    }
</style>

<script>
    $(document).ready(function () {
        // flatpickr Calendar configuration
        flatpickr(".flatpickr", {minDate: "<?= date('Y-m-d') ?>"});

        $('#checkin').on('change', function () {
            nextday = new Date($('#checkin').val());
            nextday.setDate(nextday.getDate() + 1);
            $('#checkout').flatpickr({defaultDate: nextday, minDate: nextday});
        });
    });
</script>