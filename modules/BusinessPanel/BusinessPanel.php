<?php

class BusinessPanel extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 1;
		$id_user = null;
		if (isset($_SESSION['user_auth']['id'])) {
			$id_user = (isset($parameters['id_user'])) ? $parameters['id_user'] : $_SESSION['user_auth']['id'];
		}
		$order = (isset($parameters['order'])) ? $parameters['order'] : 'publish_date desc';
		$an_model = Loader::getModel('Annonuces');
		$Business_tbl = TABLE_PREFIX . 'Business';
		$bs_model = Loader::getModel('Businesses');
		$us_model = Loader::getModel('Users');
		$filter = ' 1 ';

		if (!is_null($id_user)) {
			$filter .= "AND ($Business_tbl.id_user = {$id_user}) ";
		}

		$businesses = $bs_model->getList($limit, 0, $filter, $order);

		if (!is_null($id_user)) {
			$user = $us_model->getUser($id_user);
			$this->view->set('user', $user);
		}

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'admin-menu';

		$this->view->set('parameters', $parameters);
		$this->view->set('businesses', $businesses);
		$this->view->render($view);
	}
}
