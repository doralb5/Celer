<?php

class LoginMenu extends BaseModule
{
	public function execute($parameters = array())
	{
		if (UserAuth::checkFrontLoginSession()) {
			$user_logged = true;
			$user = UserAuth::getLoginSession();
		} else {
			$user_logged = false;
			$user = null;
		}

		$this->view->set('user_logged', $user_logged);
		$this->view->set('user', $user);

		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'login-widget';
		$this->view->render($view);
	}
}
