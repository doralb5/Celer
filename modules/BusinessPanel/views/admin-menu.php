<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/admin-menu.css'); ?>

<?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
	?>
	<?php
	$w = 205;
	$h = 205; ?>

	<div id="admin-menu">
		<!--        <div class="profile-sidebar" data-spy="affix" data-offset-top="500" data-offset-bottom="785">-->
		<div class="profile-sidebar">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">

				<?php if ($_SESSION['user_auth']['image'] != '') {
		?>
					<img src="<?= Utils::genThumbnailUrl('users/' . $user->image, $w, $h) ?>" class="img-responsive" alt="">
				<?php
	} else {
		?>
					<img src="http://www.netirane.al/data/netirane.al/media/avatar.png" class="avatar img-circle img-thumbnail" alt="avatar">
				<?php
	} ?>

			</div>

			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">
					<?= ucwords($_SESSION['user_auth']['firstname']) ?> <?= ucwords($_SESSION['user_auth']['lastname'][0]) ?>.
				</div>
				<div class="profile-usertitle-job">
					[$admin_panel]
				</div>
			</div>
			<!-- END SIDEBAR USER TITLE -->
			<!--             SIDEBAR BUTTONS 
						<div class="profile-userbuttons">
							<a href="/mesazhet">
								<button type="button" class="btn btn-danger btn-sm">Mesazhet</button>
							</a>
							<a href="/recensionet">
								<button type="button" class="btn btn-warning btn-sm">Recensionet</button>
							</a>
						</div>-->
			<!-- END SIDEBAR BUTTONS -->
			<!-- SIDEBAR MENU -->
			<div class="profile-usermenu">
				<ul class="nav">
					<li class="active">
						<a href="<?= Utils::getComponentUrl('Users/dashboard') ?>">
							<i class="glyphicon glyphicon-home"></i>
							[$main_page]</a>
					</li>
					<li>
						<a href="<?= Utils::getComponentUrl('Users/edit_account') ?>">
							<i class="glyphicon glyphicon-user"></i>
							[$edit_profile] </a>
					</li>
					<li>
						<a href="<?= Utils::getComponentUrl('Businesses/business_list_u/') ?>">
							<i class="glyphicon glyphicon-briefcase"></i>
							[$my_bussines] </a>
					</li>
					<li>
						<a href="<?= Utils::getComponentUrl('Businesses/ListOffertsUser/') ?>">
							<i class="glyphicon glyphicon-shopping-cart"></i>
							[$my_offers]</a>
					</li>
					<li>
						<a href="<?= Utils::getComponentUrl('Businesses/edit_offer/') ?>">
							<i class="glyphicon glyphicon-shopping-cart"></i>
							[$add_offer]  </a>
					</li>
					<li>
						<a href="<?= Utils::getComponentUrl('Announces/listAnnouncesUser/') ?>">
							<i class="glyphicon glyphicon-flag"></i>
							[$my_notifications] </a>
					</li>
					<li>
						<a href="<?= Utils::getComponentUrl('Users/logout_u') ?>">
							<i class="glyphicon glyphicon-off"></i>
							[$logout] </a>
					</li>
				</ul>
			</div>
			<!-- END MENU -->
		</div>
	</div>

<?php
} else {
		?>

	<div id="login-widget">
		<div class="login-heading"><h1 class="login-title text-center">[$enter_in_portal]</h1>

		</div>

		<div class="login-body">
			<form role="form"  method="POST" action="<?= Utils::getComponentUrl('Users/Login') ?>" >

				<div style="margin-bottom: 12px" class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					<input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username ose email">                                        
				</div>

				<div style="margin-bottom: 12px" class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<input id="login-password" type="password" class="form-control" name="password" placeholder="fjalekalimi">
				</div>

				<input type="hidden" value="<?= md5('rk') ?>" name="login">
				<button type="submit"  class="btn btn-primary btn-block">[$log_in]</button>

				<hr style="margin-top:10px;margin-bottom:10px;" >



				<div class="login-footer">
					[$dont_have_account] 
					<a href="<?= Utils::getComponentUrl('Users/register') ?>">
						[$sign_up_here]
					</a>
				</div>


			</form>
		</div>
	</div>

<?php
	} ?>
