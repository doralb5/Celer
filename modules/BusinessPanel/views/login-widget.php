<?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
	?>

	<style>

		.panel-heading {
			border-top-left-radius: 0;
			border-top-right-radius: 0;
		}
		.input-group-addon {
			color: #ffffff;
			background: #c90000;
		}
		.panel {
			border-radius: 0px;
		}
	</style>


	<div class="panel panel-default">
		<div class="panel-heading"><h3 class="panel-title text-center"><strong>Log In </strong></h3>

		</div>

		<div class="panel-body">
			<form role="form" method="POST" action="<?= Utils::getComponentUrl('Users/Login') ?>">
				<div class="alert alert-danger">
					<a class="close" data-dismiss="alert" href="#">×</a>Username ose Fjalkalim i gabuar!
				</div>
				<div style="margin-bottom: 12px" class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					<input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username ose email">                                        
				</div>

				<div style="margin-bottom: 12px" class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<input id="login-password" type="password" class="form-control" name="password" placeholder="fjalekalimi">
				</div>


				<button type="submit" class="btn btn-primary btn-block">Hyr</button>

				<hr style="margin-top:10px;margin-bottom:10px;" >

				<div class="form-group">

					<div style="font-size:85%">
						Nuk keni llogari? 
						<a href="<?= Utils::getComponentUrl('Users/register') ?>">
							Rregjistrohuni ketu
						</a>
					</div>

				</div> 
			</form>
		</div>
	</div>
	<?php
}?>