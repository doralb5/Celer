<?php

class BusinessesMap extends BaseModule
{
	public function __construct($block_id = '', $position = '', $package = '')
	{
		parent::__construct($block_id, $position, $package);
		$this->ComponentSettings = BaseComponent::getComponentSettings('Businesses', 'Businesses');
	}

	public function execute()
	{
		$locations = array();
		if (isset($_SESSION['locations'])) {
			$locations = unserialize($_SESSION['locations']); // $_SESSION['locations'] is set in bussiness.php component
			if (count($locations) == 1) { // if there is only one bussines
				$location = $locations;
				$locations = array();
				$locations[0] = $location;
			}
		}
		$this->view->set('locations', $locations);
		$this->view->set('settings', $this->ComponentSettings);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'businesses-map';
		$this->view->render($view);
	}
}
