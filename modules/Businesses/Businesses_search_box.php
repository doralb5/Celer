<?php

class Businesses_search_box extends BaseModule
{
	public function execute($parameters = array())
	{
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'business_search_box';
		$category_id = (isset($parameters['category_id'])) ? $parameters['category_id'] : '';
		$featured = (isset($parameters['featured'])) ? $parameters['featured'] : '';
		$order = (isset($parameters['order'])) ? $parameters['order'] : 'publish_date desc';
		$Business_Table = TABLE_PREFIX . 'Business';
		$filter = "1 AND $Business_Table.enabled = '1'  AND $Business_Table.publish_date < NOW() AND $Business_Table.expire_date > NOW() ";

		$bs_model = Loader::getModel('Businesses');

		$businesses = $bs_model->getList(30, 0, $filter, $order);
		$this->view->set('action_url', Utils::getComponentUrl('Businesses/b_list'));
		$this->view->set('image_baseurl', $bs_model->getImageBaseUrl());
		$this->view->set('thumbs_baseurl', $bs_model->getImageBaseUrl(true));
		$this->view->set('businesses', $businesses);
		$this->view->render($view);
	}
}
