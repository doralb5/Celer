<?php

class LastBusinessesOffers extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 16;
		$order = (isset($parameters['order'])) ? $parameters['order'] : 'publish_date desc';
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'last-businesses-offer';

		$BusinessOffer_Table = TABLE_PREFIX . 'BusinessOffer';
		$filter = "1 AND $Business_Table.creation_date < NOW() AND $Business_Table.expiration_date > NOW() ";

		$bs_model = Loader::getModel('Businesses');

		$offers = $bs_model->getOfferList($limit, 0, $filter, $order);

		$this->view->set('parameters', $parameters);
		$this->view->set('offers', $offers);
		$this->view->render($view);
	}
}
