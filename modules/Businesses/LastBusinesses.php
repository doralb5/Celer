<?php

class LastBusinesses extends BaseModule
{
	public function __construct($block_id = '', $position = '', $package = '')
	{
		parent::__construct($block_id, $position, $package);
		$this->ComponentSettings = BaseComponent::getComponentSettings('Businesses', 'Businesses');
	}

	public function execute($parameters = array())
	{
		echo '<!-- Start lastBusiness Module-->';

		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 16;
		$pb_date = (isset($parameters['pb_date'])) ? $parameters['pb_date'] : 0;
		$category_id = (isset($parameters['category_id'])) ? $parameters['category_id'] : '';
		$featured = (isset($parameters['featured'])) ? $parameters['featured'] : '';
		$order = (isset($parameters['order'])) ? $parameters['order'] : 'publish_date desc';
		$view = (isset($parameters['view'])) ? $parameters['view'] : '';
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		$Business_Table = TABLE_PREFIX . 'Business';
		$filter = "1 AND $Business_Table.enabled = '1'  AND $Business_Table.publish_date < NOW() AND $Business_Table.expire_date > NOW() ";
		if ($category_id != '') {
			$filter .= "AND $Business_Table.id_category = $category_id ";
		}
		if ($featured != '') {
			$filter .= "AND $Business_Table.featured = '$featured' ";
		}

		$bs_model = Loader::getModel('Businesses');

		$businesses = $bs_model->getList($limit, 0, $filter, $order);

		//$category = $bs_model->getCategoriesofBusiness($category_id);
		$category = null;
		$this->view->set('category', $category);

		$this->view->set('settings', $this->ComponentSettings);
		$this->view->set('parameters', $parameters);
		$this->view->set('businesses', $businesses);
		$this->view->render($view);
		echo '<!-- end lastBusiness Module-->';
	}
}
