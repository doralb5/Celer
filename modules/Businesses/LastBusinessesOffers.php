<?php

class LastBusinessesOffers extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 16;
		$order = (isset($parameters['order'])) ? $parameters['order'] : 'creation_date desc';
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'last-businesses-offer';
		$this->ComponentSettings = BaseComponent::getComponentSettings('Businesses', 'Businesses');
		$image_baseurl = (isset($this->ComponentSettings['image_baseurl'])) ? $this->ComponentSettings['image_baseurl'] : '';

		$Business_Table = TABLE_PREFIX . Business_Entity::TABLE_NAME;
		$filter = "$Business_Table.id_profile = 3";

		$bs_model = Loader::getModel('Businesses');

		$offers = $bs_model->getOfferList($filter, $limit, 0, $order);

		$this->view->set('image_baseurl', $image_baseurl);
		$this->view->set('parameters', $parameters);
		$this->view->set('offers', $offers);
		$this->view->render($view);
	}
}
