<?php

class LastRecensions extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 16;
		$order = (isset($parameters['order'])) ? $parameters['order'] : 'creation_date desc';
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'last-recensions';
		$this->ComponentSettings = BaseComponent::getComponentSettings('Businesses', 'Businesses');
		$image_baseurl = (isset($this->ComponentSettings['image_baseurl'])) ? $this->ComponentSettings['image_baseurl'] : '';
		$BusinessRating_Table = TABLE_PREFIX . BusinessRating_Entity::TABLE_NAME;
		$filter = "$BusinessRating_Table.rating = 5 AND $BusinessRating_Table.enabled = '1' AND $BusinessRating_Table.comment != ''";
		$order = 'publish_date DESC';

		$bs_model = Loader::getModel('Businesses');

		$comments = $bs_model->getLastComments($filter, $limit, 0, $order);

		$this->view->set('image_baseurl', $image_baseurl);
		$this->view->set('parameters', $parameters);
		$this->view->set('comments', $comments);
		$this->view->render($view);
	}
}
