<?php

class Most_clicked_categ extends BaseModule
{
	public function execute($parameters = array())
	{
		$view = (isset($parameters['view']) ? $parameters['view'] : 'most_clicked_categories');
		$limit = (isset($parameters['limit']) ? $parameters['limit'] : 10);

		$bs_model = Loader::getModel('Businesses');

		$categories = $bs_model->getMostClickedCateg($limit);
		$this->view->set('categories', $categories);
		$this->view->render($view);
	}
}
