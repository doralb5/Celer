<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/search-tab-sm.css');
?>

<?php // HeadHTML::addMeta('keywords', 'biznes,tirane,dyqan,hotel,restorant,market,bar,palester,kerkim');?>
<?php //HeadHTML::addMeta('description', 'Motori i kerkimit te bizneseve ne Tirane');?>

<?php
$query = isset($_GET['query']) ? $_GET['query'] : '';
?>

<!-- Business Search Box -->

<script type="application/ld+json">

    {
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "http://<?= $_SERVER['HTTP_HOST'] ?>/",
    "potentialAction": {
    "@type": "SearchAction",
    "target": "<?= Utils::getComponentUrl('Businesses/b_list') ?>?query={search_term_string}",
    "query-input": "required name=search_term_string"
    }
    }
</script>

<div id="search-tab-sm">
    <form id="form-header" method="get" action="<?= $action_url ?>" class="">
        <div class="search-input">
            <input type="text" class="search typeaheadtop" id="search" placeholder="[$Search_businesses_on_portal] <?= CMSSettings::$webdomain ?>"  name="query" value="<?= $query ?>"/>
            <span class="highlight"></span>
            <span class="bar"></span>
<!--            <label>[$Search_businesses_on_portal] <?= CMSSettings::$webdomain ?></label> -->
            <span class="glyphicon glyphicon-search"></span>
<!--            <span class="glyphicon glyphicon-remove"></span>-->
        </div>
    </form>
</div>
<script>
    var dataSource = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('category', 'company_name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?= Utils::getComponentUrl('Businesses/ajx_advanceSearch') ?>?query=%QUERY',
            wildcard: '%QUERY'
        }

    });
    dataSource.initialize();
    $('.typeaheadtop').typeahead({
        hint: true,
        minLength: 1,
        highlight: true
    }, {
        name: 'businesses',
        display: function (item) {
            return (item.company_name).toLowerCase()
        },
        source: dataSource.ttAdapter(),
        limit: 10,
        templates: {
            empty: [
                '<div class="empty">No Matches Found</div>'].join('\n'),
            suggestion: function (data) {
                if (data.image != '' || data.image != null) {
                    var image = data.image;
                    if (image != null) {
                        image = image.split("/");
                        if (image.length == 3) {
                            image.splice(2, 0, "150x150");
                        }
                        if (image.length == 2) {
                            image.splice(1, 0, "150x150");
                        }
                    }
                }
                if (image == null || image.toString() == "") {
                    image = "150x150/default.jpg";
                }
<?php
if ($_SERVER['HTTP_HOST'] == 'cittanelweb.it' || $_SERVER['HTTP_HOST'] == 'www.cittanelweb.it') {
	$remote_image = Utils::genMediaUrl('thumbs/businesses/', true);
} else {
	$remote_image = 'http://www.neshqiperi.al/data/neshqiperi.al/media/thumbs/businesses/';
}
$http_host = Utils::getComponentUrl('Businesses/b_show');
?>
                return '<div class="BusinessCard u-cf Typeahead-suggestion Typeahead-selectable">' +
                        '<a href="<?= $http_host ?>/' + data.url_slug + '" >' +
                        '<img class="BusinessCard-avatar" src="<?= $remote_image ?>' + image.toString().replace(/,/g, '/') + '?far=1&bg=FFFFFF">' +
                        '<div class="BusinessCard-details">' +
                        '<div class="BusinessCard-Name">' + data.company_name + '</div>' +
                        '<div class="BusinessCard-description">' + data.category + '</div>' +
                        '</div></a>' +
                        '</div>'
            }
        }
    });

    $('input.typeaheadtop').keypress(function (e) {
        if (e.which == 13) {
            $('#form-header').submit();
            return true;
        }
    });
</script>
<script>
    'use strict';

    $(function () {

        $('.search-input input').blur(function () {

            if ($(this).val()) {
                $(this)
                        .find('~ label, ~ span:nth-of-type(n+3)')
                        .addClass('not-empty');
            } else {
                $(this)
                        .find('~ label, ~ span:nth-of-type(n+3)')
                        .removeClass('not-empty');
            }
        });


        $('.search-input input ~ span:nth-of-type(4)').click(function () {
            $('.search-input input').val('');
            $('.search-input input')
                    .find('~ label, ~ span:nth-of-type(n+3)')
                    .removeClass('not-empty');
        });

        if ($('.search-input input').val()) {
            $('.search-input input').find('~ label, ~ span:nth-of-type(n+3)').addClass('not-empty');
        } else {
            $('.search-input input').find('~ label, ~ span:nth-of-type(n+3)').removeClass('not-empty');
        }

    });

</script>
