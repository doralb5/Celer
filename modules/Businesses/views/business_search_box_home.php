<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/search-box.css'); ?>


<?php
$query = isset($_GET['query']) ? $_GET['query'] : '';
?>
<!-- Business Search Box Home -->
<div id="search-top">
    <div class="button_box2">
        <div class="col-md-12">
            <h3 class="cosa typing-text" style="color:white;margin-top: 0px;text-shadow: 0px 3px 6px rgba(0, 0, 0, 0.75);">
                [$search_text] </h3></div>
        <div class="">
            <div class="col-md-6">
                <form method="get" action="<?= $action_url ?>" class="form-wrapper-2 cf-home wow fadeInLeft animated " data-wow-delay=".5s">

                    <div class="">
                        <div class="input-group">
                            <input type="text" class="cosa" name="query" placeholder="[$what]" value="<?= $query ?>"
                                   required="">

                            <span class="input-group-btn">
                                <button type="submit" name="search">[$search]</button>
                            </span>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-md-3">
                <form class="form-wrapper-2 cf-home cf-height wow bounceIn animated " data-wow-delay="3s">
                    <div class="">
                        <a class="button" href="<?= Utils::getComponentUrl('Businesses/insert_business') ?>">
                            [$InsertFree]
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
