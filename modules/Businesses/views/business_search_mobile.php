<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/search-box.css'); ?>

<?php
$query = isset($_GET['query']) ? $_GET['query'] : '';
?>
<!-- Search Box Mobile -->
<div class="collapse" id="collapseSearch">
    <div class="main-search-top">
        <div id="search-top-mobile">
            <div class="button_box2">
                <form method="get" action="<?= $action_url ?>" class="form-wrapper-2 cf">

                    <div class="col-md-12 col-sm-12">
                        <div class="input-group">
                            <input type="text" class="cosa" name="query" placeholder="[$what]" value="<?= $query ?>" required="">

                            <span class="input-group-btn">
                                <button type="submit" name="search">[$search]</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <a class="button" href="<?= Utils::getComponentUrl('Businesses/insert_business'); ?>">
                            [$InsertFree]
                        </a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
