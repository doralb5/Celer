<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/business-map.css'); ?>
<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php $image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : ''; ?>
<?php if (isset($locations) || count($locations) > 1 || (count($locations) == 1 && $locations[0]->coordinates != '')) {
	?>
	<?php
	if (isset($settings['google_key']) && $settings['google_key'] != '') {
		HeadHtml::LinkJS('https://maps.googleapis.com/maps/api/js?key=' . $settings['google_key'] . '&callback=initMap', true, true);
	} elseif (isset(CMSSettings::$google_maps_api) && CMSSettings::$google_maps_api != '') {
		HeadHtml::LinkJS(Utils::googleMapsJsLink(), true, true);
	} else {
		HeadHtml::LinkJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyBEiE-_79eSxQl4b1tdrEichm8vTLw4VC0&callback=initMap', true, true);
	} ?>

	<div id="map-wrapper" class="gp-wrap gpovh">
		<div id="map"></div>
		<div id="log"></div>
		<div id="map-controls">
			<div id="half-map" class="half-map map-control"><span class="icon-resize-full"><i class="fa fa-expand" aria-hidden="true"></i></span><div class="mctext">[$half_map]</div></div>
			<div class="mapc-sep"></div>
			<div id="full-map" class="full-map map-control"><span class="icon-resize-full-alt"><i class="fa fa-arrows-alt" aria-hidden="true"></i></span><div class="mctext">[$full_map]</div></div>
			<div class="mapc-sep"></div>
			<div id="center-map" class="center-map map-control"><span class="icon-center"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></span><div class="mctext">[$centered_map]</div></div>
			<div class="mapc-sep"></div>
			<div id="draggable-map" class="draggable-map map-control"><span class="icon-draggable"><i class="fa fa-lock" aria-hidden="true"></i></span><div class="mctext">[$map_lock]</div></div>
			<div class="mapc-sep"></div>
			<div id="close-map" class="close-map map-control"><span class="icon-cancel"><i id="closemap" class="fa fa-times" aria-hidden="true"></i></span><div id="closetext"class="mctext">[$map_close]</div></div>
		</div>
	</div>
	<script>
		$("#half-map").click(function () {
			$("#half-map").attr('class', 'full-map map-control mc-inactive');
			$("#full-map").attr('class', 'full-map map-control');
			$("#close-map").attr('class', 'full-map map-control');
			$("#map").css('transition', '1s');
			$("#map").css('height', '50vh');
		});

		$("#full-map").click(function () {
			$("#full-map").attr('class', 'full-map map-control mc-inactive');
			$("#half-map").attr('class', 'full-map map-control');
			$("#close-map").attr('class', 'full-map map-control');
			$('#map').css('transition', '1s');

			//find height
			var h = Number(window.innerHeight);
			var x = $('#pos-helper').css('height');
			x = x.replace("px", "");
			x = Number(x);
			var map_height = h - x;
			$("#map").css('height', map_height + 'px');
			$("#map").css('transition', '1s');

		});

		$("#center-map").click(function () {
			google.maps.addEventListener('click', function () {
				map.setCenter('Tirane,Albania');
			});
		});

		$("#close-map").click(function () {
			$("#close-map").attr('class', 'full-map map-control');
			$("#half-map").attr('class', 'full-map map-control');
			$("#full-map").attr('class', 'full-map map-control');
			$('#map').fadeToggle("slow", "linear");
			$('#map').css('transition', '1s');

		});


		jQuery('#closemap').click(function (event) {
			if ($("#map").is(":visible")) {
				$("#closemap").attr("class", "fa fa-chevron-down");
				$("#closetext").text("SHFAQ");
			} else if (!$("#map").is(":visible")) {
				$("#closemap").attr("class", "fa fa-times");
				$("#closetext").text("MBYLL");

			}

		});
	</script>




	<script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerwithlabel/src/markerwithlabel.js"></script>

	<script type="text/javascript">
		var locations = [
	<?php
	$counter = 1;
	foreach ($locations as $loc) {
		$loc->company_name = str_replace("'", "\'", $loc->company_name);
		$loc->address = str_replace("'", "\'", $loc->address);

		if ($loc->image != '') {
			$fullImage = Utils::genThumbnailUrl('businesses' . DS . $loc->image, 200, 100, array('zc' => 1), false, $image_baseurl);
		} else {
			$fullImage = '';
		}

		if ($loc->coordinates != '') {
			list($lat, $lng) = explode(',', $loc->coordinates);
			$id_biz = $loc->id;

			echo "[ '{$loc->company_name}' , {$lat} , {$lng} , {$counter} , '{$loc->address}', '{$id_biz}' , '{$fullImage}' ],\n";

			$counter++;
		}
	} ?>
		];

	<?php
	$lat = (isset($parameters['map_lat'])) ? $parameters['map_lat'] : '41.328251';
	$lng = (isset($parameters['map_lng'])) ? $parameters['map_lng'] : '19.818472';

	$map_center = "$lat, $lng";
	if (count($locations) > 0) {
		$map_center = $locations[0]->coordinates;
	} ?>

		console.log(locations);
		//  new map
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 14,
			center: new google.maps.LatLng(<?= $map_center ?>),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false,
			draggable: true
		});

		//  var infowindow = new google.maps.InfoWindow();
		var infowindow = new google.maps.InfoWindow({
			maxWidth: 400
		});

		var marker, i;
		var markerImage = 'marker.png';
		var contentString = [];

		for (i = 0; i < locations.length; i++) {


			marker = new MarkerWithLabel({
				// marker position
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map,
				draggable: false,
				raiseOnDrag: true,
				labelContent: (i + 1).toString(),
				labelAnchor: new google.maps.Point(15, 65),
				labelClass: "labels", // the CSS class for the label
				labelInBackground: false,
				icon: 'http://www.netirane.al/data/netirane.al/media/marker1.png'
			});

			contentString[i] = '<div id="iw-container">' +
					'<div class="iw-content">' + (locations[i][6] != "" ? '<img src="' + locations[i][6] + '" alt="" height="100" width="200">' : '') +
					'<div class="iw-subTitle">' + locations[i][0] + '</div>' +
					'<p><i class="fa fa-map-marker" style="padding-right: 5px;"></i>' + locations[i][4] + '<br>' +
					'</div>' +
					'<div class="iw-bottom-gradient"></div>' +
					'</div>';



			google.maps.event.addListener(marker, 'click', (function (marker, i) {
				return function () {
					infowindow.setContent(contentString[i]);
					infowindow.open(map, marker);
				}
			})

					(marker, i));
		}
		google.maps.event.addListener(infowindow, 'domready', function () {




			// Reference to the DIV that wraps the bottom of infowindow
			var iwOuter = $('.gm-style-iw');

			/* Since this div is in a position prior to .gm-div style-iw.
			 * We use jQuery and create a iwBackground variable,
			 * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
			 */
			var iwBackground = iwOuter.prev();

			// Removes background shadow DIV
			iwBackground.children(':nth-child(2)').css({'display': 'none'});

			// Removes white background DIV
			iwBackground.children(':nth-child(4)').css({'display': 'none'});

			// Moves the infowindow 45px to the right.
			iwOuter.parent().parent().css({left: '45px'});

			// Moves the shadow of the arrow 76px to the left margin.
			iwBackground.children(':nth-child(1)').attr('style', function (i, s) {
				return s + 'left: 76px !important;'
			});

			// Moves the arrow 76px to the left margin.
			iwBackground.children(':nth-child(3)').attr('style', function (i, s) {
				return s + 'left: 76px !important;'
			});

			// Changes the desired tail shadow color.
			iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0) 0px 1px 6px', 'z-index': '1'});

			// Reference to the div that groups the close button elements.
			var iwCloseBtn = iwOuter.next();

			// Apply the desired effect to the close button
			iwCloseBtn.css({width: '25px', height: '25px', opacity: '1', right: '50px', top: '3px', border: '6px solid #ffffff', 'border-radius': '13px', 'box-shadow': '0 0 5px #9c9c9c'});

			// If the content of infowindow not exceed the set maximum height, then the gradient is removed.
			//    if($('.iw-content').height() < 140){
			//      $('.iw-bottom-gradient').css({display: 'none'});
			//    }

			// The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
			iwCloseBtn.mouseout(function () {
				$(this).css({opacity: '1'});
			});
		});


		//  map style
		var styles = [{"stylers": [{"saturation": -100}]}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#0099dd"}]}, {"elementType": "labels", "stylers": [{"visibility": "off"}]}, {"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"color": "#aadd55"}]}, {"featureType": "road.highway", "elementType": "labels", "stylers": [{"visibility": "on"}]}, {"featureType": "road.arterial", "elementType": "labels.text", "stylers": [{"visibility": "on"}]}, {"featureType": "road.local", "elementType": "labels.text", "stylers": [{"visibility": "on"}]}, {}];
		map.set('styles', styles);


		google.maps.event.addDomListener(window, 'load');


	</script>

	<?php
}
?>
