<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/last-businesses-slider.css'); ?>

<?php HeadHTML::AddJS('noframework.waypoints.min.js'); ?>
<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 4;
$col_size = 12 / $cols;
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 500;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 150;
$show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0;
$title = isset($parameters['title']) ? $parameters['title'] : '';
$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';
?>


<?php if ($title != '' && count($items)) {
	?>
	<div class="main-title-description">
		<h1 class="page-header text-center">[$<?= $title ?>]</h1>
	</div>
<?php
} ?>
<div class="gallery-carousel">
    <div id="owl-gallery" class="owl-carousel text-center">
		<?php if (count($businesses) > 0) {
		?>
			<?php foreach ($businesses as $business) {
			?>

				<div class="item pic">
					<div class="back-div">

						<?php if ($business->image != '') {
				?>
							<figure>
								<a class="hover-img"
								   onclick="addStatistic(<?= $business->id ?>, 'featured_click')"
								   href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business->company_name . '-' . $business->id)) ?>"
								   title="">
									<img src="<?= Utils::genThumbnailUrl('businesses/' . $business->image, $w, $h, array('far' => '1', 'bg' => 'FFFFFF'), false, $image_baseurl) ?>"
										 class="lazyOwl" alt="">

								</a>
							</figure>
						<?php
			} else {
				?>
							<figure>
								<a class="hover-img"
								   onclick="addStatistic(<?= $item->id ?>, 'featured_click')"
								   href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business->company_name . '-' . $business->id)) ?>"
								   title="">
									<img class="lazyOwl"
										 src="<?= WEBROOT . $this->view_path ?>img/default.jpg"
										 style="width:100%">
								</a>
							</figure>
						<?php
			} ?>


						<div class="alldiv-text pic-caption bottom-to-top">

							<div class="main-comune">
								<h5 class="main-title1">
									<a href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business->company_name . '-' . $business->id)) ?>"
									   title="" onclick="addStatistic(<?= $business->id ?>, 'featured_click')">
										   <?= StringUtils::CutString(strip_tags($business->company_name), $taglia_title) ?>

									</a>
								</h5>
							</div>

							<div class="location-height">
								<p><?= StringUtils::CutString(strip_tags($item->description), $taglia_content); ?><?php //var_dump($item->descritpion);exit;?></p>
							</div>

							<?php if ($show_visitors) {
				?>
								<div class="vizitors">
									<b><span><i class="fa fa-eye"></i> <?= $item->counter_views ?></span></b>
								</div>
							<?php
			} ?>
						</div>


					</div>
				</div>
			<?php
		} ?>
		<?php
	} else {
		?>
			<div class="noresult">[$No_Elements_Available]</div>
		<?php
	} ?>
    </div>
</div>


<!-- Owl-Carousel-JavaScript -->
<script>
    $(document).ready(function () {
        $("#owl-gallery").owlCarousel({
            items: 6,
            lazyLoad: true,
            autoPlay: true,
            pagination: false,
            loop: true,
        });
    });
</script>





<!--    <div class="pic">
                    <img src="img/01.jpg" class="pic-image" alt="Pic">
                    <span class="pic-caption bottom-to-top">
                        <h1 class="pic-title">Bottom to Top</h1>
                        <p>Hi, this is a simple example =D</p>
                    </span>
                </div>-->
