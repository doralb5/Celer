<?php require_once DOCROOT . LIBS_PATH . 'StringUtils.php'; ?>
<?php $image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : ''; ?>
<div class="container marketing">
    <div class="row">
        <div class="col-md-12" data-wow-delay="0.2s">
			<?php if (count($businesses)) {
	?>
				<div class="carousel slide" data-ride="carousel" id="quote-carousel">
					<ol class="carousel-indicators">

						<?php $i = 0;
	foreach ($businesses as $business) {
		?>
							<li data-target="#quote-carousel" data-slide-to="<?= $i ?>" class='<?= ($i == 0) ? 'active' : '' ?>'>

								<img class="img-responsive " src="<?= Utils::genThumbnailUrl('businesses/' . $business['logo'], 200, 200, array('far' => '1', 'bg' => 'FFFFFF'), false, $image_baseurl) ?>">

							</li>
							<?php $i++;
	} ?>
					</ol>

					<div class="carousel-inner text-center ">

	<?php $i = 0;
	foreach ($businesses as $business) {
		?>
							<div class="item <?= ($i == 0) ? 'active' : '' ?>">
								<blockquote>
									<div class="row">
										<div class="col-sm-8 col-sm-offset-2">
											<h2 class="main-heading"><?= $business['company_name'] ?></h2>
											<small><?= StringUtils::TagliaStringa(strip_tags($business['description']), 90) ?></small>
											<br>
											<a class="btn btn-info btn-sm" href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business['company_name'] . '-' . $business['id'])) ?>" role="button">[$reveal_the_offers] »</a>
										</div>
									</div>
								</blockquote>
							</div>
						<?php $i++;
	} ?>

					</div>
				<?php if (count($businesses) > 1) {
		?>
						<a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
						<a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
				<?php
	} ?>
				</div>
<?php
} else {
		?>
				<div class="">[$No_Result]</div>
<?php
	} ?>
        </div>
    </div>
</div>

