<?php
//var_dump($comments);
?>
<style>


    .rating-lg,.rating-md,.rating-sm,.rating-xs {
        color:#676767;
        font-size:0;
        line-height:1;
    }
    .rating-lg>.stars,.rating-md>.stars,.rating-sm>.stars,.rating-xs>.stars {
        color:#e6e6e6;
        display:inline-block;
        margin-right:.25em;
        position:relative;
        vertical-align:top;
        white-space:nowrap;
    }
    .rating-lg>.stars>.rate,.rating-md>.stars>.rate,.rating-sm>.stars>.rate,.rating-xs>.stars>.rate {
        color:#ffbf00;
        left:0;
        overflow:hidden;
        position:absolute;
        top:0;
    }
    .rating-lg>.stars i,.rating-md>.stars i,.rating-sm>.stars i,.rating-xs>.stars i {
        display:inline-block;
    }
    .rating-lg>p,.rating-md>p,.rating-sm>p,.rating-xs>p {
        display:inline-block;
        margin-bottom:0;
    }
    .rating-inline.rating-lg>p,.rating-inline.rating-md>p,.rating-inline.rating-sm>p,.rating-inline.rating-xs>p {
        display:inline;
    }
    .rating-lg>.stars {
        font-size:32px;
    }
    .rating-lg>p {
        font-size:18px!important;
        line-height:32px!important;
        margin-bottom:0!important;
    }
    .rating-lg>.stars {
        width:120px;
    }
    .rating-lg>p>strong {
        font-size:20px;
    }
    .rating-md>.stars {
        font-size:25px;
    }
    .rating-md>p {
        font-size:14px!important;
        line-height:25px!important;
        margin-bottom:0!important;
    }
    .rating-md>.stars {
        width:95px;
    }
    .rating-sm>.stars {
        font-size:20px;
    }
    .rating-sm>p {
        font-size:14px!important;
        line-height:20px!important;
        margin-bottom:0!important;
    }
    .rating-sm>.stars {
        width:99px;
    }
    .rating-xs>.stars {
        font-size:18px;
    }
    .rating-xs>p {
        font-size:12px!important;
        line-height:18px!important;
        margin-bottom:0!important;
    }
    .rating-xs>.stars {
        width:65px;
    }
    .rating-overview {
        list-style:none;
        max-width:300px;
    }
    .rating-overview .rating-overview-row .rate {
        display:inline-block;
        width:30%;
    }
    .rating-overview .rating-overview-row .count {
        color:#4d4d4d;
        display:inline-block;
        padding-left:5%;
        width:15%;
    }
    .rating-overview .rating-overview-row .graph {
        -webkit-box-shadow:inset 0 1px 0 rgba(0,0,0,0.05);
        box-shadow:inset 0 1px 0 rgba(0,0,0,0.05);
        width:50%;
        background:#e6e6e6;
        border-radius:2px;
        display:inline-block;
        height:10px;
        position:relative;
    }
    .rating-overview .rating-overview-row .graph span {
        background:#80ba27;
        border:1px solid #71a523;
        border-radius:2px;
        height:100%;
        position:absolute;
        top:0;
        left:0;
    }
    .rating-overview .rating-overview-row>a {
        display:block;
        text-decoration:none;
    }
    .rating-overview .rating-overview-row>a:hover .rate {
        text-decoration:underline;
    }
    .rating-overview .rating-overview-row.active {
        font-weight:bold;
    }
    #reviewForm_stars {
        display:none;
    }
    .no-touch #reviewForm_stars {
        display:block;
    }
    #reviewForm_stars #rateit-stars {
        width:150px;
        height:30px;
        display:inline-block;
        margin-right:15px;
        position:relative;
        vertical-align:middle;
    }
    #reviewForm_stars #rateit-stars i {
        display:inline-block;
    }
    #reviewForm_stars #rateit-stars i:before {
        font-size:40px;
        line-height:30px;
    }
    #reviewForm_stars #rateit-stars .rateit-range {
        color:#ccc;
        outline:0;
    }
    #reviewForm_stars #rateit-stars .rateit-range .rateit-hover,#reviewForm_stars #rateit-stars .rateit-range .rateit-selected {
        width:0;
        height:30px;
        color:#ffbf00;
        cursor:pointer;
        overflow:hidden;
        position:absolute;
        top:0;
        left:0;
    }
    #reviewForm_stars #rateit-hover {
        background:#999;
        border-bottom-right-radius:2px;
        border-top-right-radius:2px;
        color:white;
        display:inline-block;
        font-weight:bold;
        line-height:20px;
        margin:2px 0;
        padding:3px 10px 3px 5px;
        position:relative;
        vertical-align:middle;
    }
    #reviewForm_stars #rateit-hover:before {
        content:"";
        border-color:transparent;
        border-right-color:#999;
        border-style:solid;
        border-width:13px 13px 13px 0;
        height:0;
        position:absolute;
        top:0;
        left:-13px;
        width:0;
    }

    .result {
        border-bottom:1px solid #e6e6e6;
        padding: 20px;
    }
    .result>a {
        display:block;
        padding-top:13.33333333px;
    }
    .result>a h3 {
        color:#07c;
    }
    .result>a:hover {
        text-decoration:none;
    }
    .result>a:hover h3 span.result-title {
        color:#0068b3;
        text-decoration:underline;
    }
    .result .result-img {
        float:left!important;
    }
    @media(max-width:767px) {
        .result .result-img {
            width:50px;
            height:50px;
            display:block;
            position:relative;
            text-align:center;
            margin-right:15px;
        }
        .result .result-img>img {
            margin:auto;
            max-height:100%;
            max-width:100%;
            position:absolute;
            top:0;
            right:0;
            bottom:0;
            left:0;
        }
    }
    @media(min-width:768px) {
        .result .result-img {
            width:100px;
            height:100px;
            display:block;
            position:relative;
            text-align:center;
            margin-right:40px;
        }
        .result .result-img>img {
            margin:auto;
            max-height:100%;
            max-width:100%;
            position:absolute;
            top:0;
            right:0;
            bottom:0;
            left:0;
        }
    }
    .result h3 {
        margin-bottom:0;
        margin-top:0;
    }
    @media(max-width:767px) {
        .result h3 {
            margin-left:65px;
        }
    }
    @media(min-width:768px) {
        .result h3 {
            margin-left:130px;
        }
    }
    @media(max-width:767px) {
        .result [class^="rating-"] {
            margin-left:65px;
        }
    }
    @media(min-width:768px) {
        .result [class^="rating-"] {
            margin-left:130px;
        }
    }
    .result .result-models {
        color:#4d4d4d;
        margin-top:5px;
    }
    .result .result-txt {
        color:#4d4d4d;
        margin-top:10px;
        margin-bottom:20px;
    }
    @media(min-width:768px) {
        .result .result-txt {
            margin-left:140px;
        }
    }




</style>
<?php foreach ($comments as $comm) {
	?>
	<!--<div class="result " rel="39644">
		<a href="/p/lexus-rx-2003-present.html" class="clearfix" target="_blank" data-gae="{&quot;act&quot;: &quot;CategoryClick&quot;, &quot;cat&quot;: &quot;SUV / 4WD&quot;, &quot;lab&quot;: &quot;lexus-rx-2003-present&quot;}">
		<div class="result-img">
			<img src="<?= Utils::genThumbnailUrl('businesses/' . $comm->image, 100, 53, array(), true, $image_baseurl) ?>" alt="<?= $comm->company_name ?>">
		</div>
			
		<h3>
			<span class="result-title"><?= $comm->name ?></span>        
		</h3>
		<div class="rating-sm">
			<div class="stars">
				<div class="rate" style="width:92%" title="4.6 of 5">
				<i class="fa fa-star star-on-png"></i><i class="fa fa-star star-on-png"></i><i class="fa fa-star star-on-png"></i><i class="fa fa-star star-on-png"></i><i class="fa fa-star star-on-png"></i>    
				</div>

				<i class="fa fa-star star-on-png"></i><i class="fa fa-star star-on-png"></i><i class="fa fa-star star-on-png"></i><i class="fa fa-star star-on-png"></i><i class="fa fa-star star-on-png"></i>
			</div>
			</div>

			<div class="result-models">
	<?= $comm->company_name ?>
			</div>

			<div class="result-txt">
				<strong>Latest Review:</strong>
	<?= $comm->comment ?>
			</div>
		</a>
	</div>-->
<?php
} ?>