<?php
require_once LIBS_PATH . 'StringUtils.php';
HeadHTML::AddJS('noframework.waypoints.min.js');
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/last-companies.css');
$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';
?>

<?php
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 3;
$col_size = 12 / $cols;
$w = (isset($parameters['w'])) ? $parameters['w'] : 600;
$h = (isset($parameters['h'])) ? $parameters['h'] : 350;
$taglia_company_name = (isset($parameters['taglia_company_name'])) ? $parameters['taglia_company_name'] : 25;
?>
<style>
    .carousel-control{
        margin-top: 120px !important;
    }
</style>
<script type="text/javascript">


    function addStatistic(id_business, type) {

        $.ajax({
            type: "POST",
            url: "<?= Utils::getComponentUrl('Businesses/ajx_addStatistic') ?>",
            data: {
                'id_business': id_business,
                'type': type,
            },
            success: function (data) {
            },
            error: function (e) {
                console.log(e);
            }
        });

    }

    function ratingData(id_biz) {

        $.ajax({
            type: "POST",
            url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValueBList') ?>",
            dataType: 'json',
            data: {
                'id_biz': id_biz,
            },
            success: function (data) {

                if (data.rate != 0) {
                    for (var i = 0; i <= data.rate; i++)
                        $('#star_' + i + '_' + id_biz).attr("class", "fa fa-star star-on-png");
                    if (data.half == 1)
                        $('#star_' + i + "_" + id_biz).attr("class", "fa fa-star-half-o star-on-png");
                }

            },
            error: function (e) {
                console.log(e);
            }
        });
        return false;
    }

</script>
<div class="row">
    <div class="col-md-12">
        <div class="module-title ">
            <h2 class="homepage-featured-title" style="text-transform: uppercase;">[$SUGESTED_BUSINESSES]</h2>

            <a class="button" href="<?= Utils::getComponentUrl('Businesses/insert_business') ?>" style="background:#c90000;">
                <h2 class="right-services" style="text-transform: uppercase;">[$add_business]</h2>
            </a>
            <a class="button" href="<?= Utils::getComponentUrl('Businesses/categ_list') ?>">
                <h2 class="right-services" style="text-transform: uppercase;">[$all]</h2>
            </a>

        </div>
    </div>
</div>


<?php if (count($businesses)) {
	?>

	<div class="lastcompany">
		<?php
		$i = 0;
	foreach ($businesses as $business) {
		?>

			<?php if ($i == 0) {
			?>

				<!-- Slide -->
				<div class="item active">
					<ul class="thumbnails">
						<div class="row">
						<?php
		} ?>
						<?php if ($i % $cols == 0 && $i != 0) {
			?>
						</div>
					</ul>
				</div>
				<div class="item">
					<ul class="thumbnails">
						<div class="row">

						<?php
		} ?>
						<div class="col-md-<?= $col_size ?> news-business">
							<li class="" id="businessDiv_<?= $business->id ?>">
								<div class="back-div">
									<?php if (in_array($business->id_profile, array(3))) {
			?>
										<div class="ribbon-wrapper-green">
											<div class="ribbon-green">PREMIUM</div>
										</div>
									<?php
		} ?>
									<div class="post-thumbnail-3">
										<?php if ($business->image != '') {
			?>
											<figure>
												<a class="hover-img"
												   onclick="addStatistic(<?= $business->id ?>, 'featured_click')"
												   href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business->company_name . '-' . $business->id)) ?>"
												   title="">
													<img
														src="<?= Utils::genThumbnailUrl('businesses/' . $business->image, $w, $h, array('far' => '1', 'bg' => 'FFFFFF'), false, $image_baseurl) ?>"
														class="img-responsive wp-post-image" alt="">

												</a>
											</figure>
										<?php
		} ?>
									</div>



									<div class="alldiv-text">

										<div class="main-comune">
											<h5 class="main-title1">
												<a href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business->company_name . '-' . $business->id)) ?>"
												   title="" onclick="addStatistic(<?= $business->id ?>, 'featured_click')">
													   <?= StringUtils::CutString(strip_tags($business->company_name), $taglia_company_name) ?>
												</a>
											</h5>
										</div>
										<span class="main-comune"> 
											<?php
											for ($j = 0; $j < count($business->id_category); $j++) {
												if ($j > 0) {
													break;
												} ?>
												<a href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business->company_name . '-' . $business->id)) ?>">
													<?php
													if (!is_null($business->category) && $business->category != '') {
														echo $business->category;
													} elseif (isset($business->categories) && count($business->categories)) {
														echo $business->categories[0]->id;
													} ?>
												</a>
											<?php
											} ?>
										</span>

										<div class="location-height">
											<div class="main-addres text-center">
												<?php if (in_array($business->id_profile, array(2, 3, 5))) {
												?>
													<div class="rate-it rate-cus rate-featured">
														<i class="fa fa-star-o star-on-png"
														   id="star_1_<?= $business->id ?>"></i>

														<i data-alt="2" class="fa fa-star-o star-on-png"
														   id="star_2_<?= $business->id ?>"></i>

														<i data-alt="3" class="fa fa-star-o star-on-png"
														   id="star_3_<?= $business->id ?>"></i>

														<i data-alt="4" class="fa fa-star-o star-on-png"
														   id="star_4_<?= $business->id ?>"></i>

														<i data-alt="5" class="fa fa-star-o star-on-png"
														   id="star_5_<?= $business->id ?>"></i>
													</div>
													<script>ratingData(<?= $business->id ?>);</script>

												<?php
											} ?>
											</div>
										</div>
									</div>
								</div>
							</li>
						</div>
						<script>


							var businessDiv_<?= $business->id ?> = false;
							var waypoint = new Waypoint({
								element: document.getElementById('businessDiv_<?= $business->id ?>'),
								handler: function () {
									if (businessDiv_<?= $business->id ?> == false) {
										//addStatistic(<?= $business->id ?>, 'featured_impression');
										businessDiv_<?= $business->id ?> = true;
									}
								},
								offset: '50%'
							})


						</script>
						<?php
						$i++;
	} ?>
				</div>
			</ul>
			<!--        </div>
				</div>-->
		</div>
		<!-- Controls -->
		<!--  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="icon-prev"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="icon-next"></span>
		  </a>-->
	</div>

<?php
} else {
		?>
	<div class="noresult">[$No_Elements_Available]</div>
<?php
	} ?>


<!--<script>
    $('#carousel-example-generic').carousel({wrap: true});
</script>-->