<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/right-sidebar.css');
?>

<div class="sidebar-categories" data-spy="afix" data-offset-top="5" data-offset-bottom="520">

    <h5 class="sidebar-categories-title">
        [$MOST_CLICKED_CATEGORIES]
    </h5>

    <ul class="piu-cliccate sidebar-categories-list">
		<?php foreach ($categories as $categ) {
	?>
			<li>
				<a class="ln-black" href="<?= Utils::getComponentUrl('Businesses/b_list') . '/' . Utils::url_slug($categ['category']) . "-{$categ['id']}" ?>"><?= $categ['category'] ?></a>
			</li>

		<?php
} ?>

    </ul>

    <a class="morecateg" href="<?= Utils::getComponentUrl('Businesses/categ_list') ?>">
        [$show_all_categories]...
    </a>

</div>