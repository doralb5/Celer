<style>
    .search-background img {
        width: 100%;
    }
    .well-searchbox {
        min-height: 20px;
        min-width: 500px;
        padding: 20px;
        position: absolute;
        z-index: 80;
        top: 90px;
        left: 0px;
        background: rgba(227, 227, 227, 0.5);
        margin-bottom: 20px;
        border: 1px solid #e7e7e7;
        border-radius: 2px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
    }

    .well-searchbox label {
        color: black;
    }
</style>
<div class="well-searchbox">
    <form class="form-horizontal" role="form">
        <div class="form-group">
            <label class="col-md-4 control-label">Provincia o comune</label>
            <div class="col-md-8">
                <input type="text" class="form-control" placeholder="Inserisci un comune">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Contratto</label>
            <div class="col-md-8">
                <select class="form-control" placeholder="Country">

                    <option value="">Affitto</option>
                    <option value="">Vendita</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Tipologia</label>
            <div class="col-md-8">
                <select class="form-control" placeholder="Province">
                    <option value="">Qualsiasi</option>
                    <option value="4">Appartamento</option>
                    <option value="5">Attico / Mansarda</option>
                    <option value="6">Box / Garage</option>
                    <option value="7">Casa indipendente</option>
                    <option value="31">Loft / Open Space</option>
                    <option value="10">Palazzo / Stabile</option>
                    <option value="11">Rustico / Casale</option>
                    <option value="12">Villa</option>
                    <option value="13">Villetta a schiera</option>
                    <option value="14">Altro</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Prezzo Massimo</label>
            <div class="col-md-8">
                <input type="number" class="form-control" placeholder="Euro (€)">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Superficie Minima</label>
            <div class="col-md-8">
                <input type="number" class="form-control" placeholder="Metri quadri (m²)">
            </div>
        </div>
        <div class="col-sm-offset-4 col-sm-5">
            <button type="submit" class="btn btn-success">Cerca</button>
        </div>
    </form>
</div>