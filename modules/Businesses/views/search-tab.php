<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/search-tab.css');
?>

<?php
$query = isset($_GET['query']) ? $_GET['query'] : '';
?>
<div id="portal-search">
    <div id="search-tab-md">
        <div class="tabbable custom-tabs tabs-animated  flat flat-all hide-label-980 shadow track-url auto-scroll">
            <!--            <ul class="nav nav-tabs">
                            <li class="active"><a class="active" data-toggle="tab" href="#panel1"><span>[$Business]</span></a></li>
                            <li><a data-toggle="tab" href="#panel2"><i class="fa fa-bell-o"></i></i>&nbsp;<span>Njoftime</span></a></li>
                            <li><a data-toggle="tab" href="#panel3"><i class="fa fa-home"></i></i>&nbsp;<span>Prona</span></a></li>
                            <li><a data-toggle="tab" href="#panel4"><i class="fa fa-calendar-check-o"></i></i>&nbsp;<span>Evente</span></a></li>
							
                        </ul>
            -->
            <div class="tab-content ">
                <div class="tab-pane active" id="panel1">
                    <div class="row">
                        <div class="col-md-12">

                            <form method="get" action="<?= $action_url ?>" class="">
                                <div class="module-search" id="bloodhound">
                                    <fieldset>
                                        <!--<label for="title"><i class="fa fa-search"></i></i>&nbsp;&nbsp;[$Search_businesses_on_portal] <?= CMSSettings::$webdomain ?></label>-->
                                        <input type="text" class="search typeahead" id="search" placeholder="[$Search_businesses_on_portal]" name="query" value="<?= $query ?>" autofocus=""/>
                                        <div class="underline"></div>
                                    </fieldset>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button class="btn btn-xs btn-default" type="submit" name="search">[$search]</button>
                                        <a class="btn btn-xs btn-default" href="<?= Utils::getComponentUrl('Businesses/insert_business/') ?>">[$InsertYourBusiness]</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!--
                                <div class="tab-pane" id="panel2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1 class="tab-main-title"><i class="fa fa-search"></i></i>&nbsp;&nbsp;[$Search_announces_on_portal] <?= CMSSettings::$webdomain ?></h1>
                                            <label>Username</label> <input class="input-block-level" type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="panel3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1 class="tab-main-title"><i class="fa fa-search"></i></i>&nbsp;&nbsp;[$Search_realestates_on_portal] <?= CMSSettings::$webdomain ?></h1>
                                            <label>Email</label> <input class="input-block-level" type="text" />
                                        </div>
                                    </div>
                                </div>
				
                                <div class="tab-pane" id="panel4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1 class="tab-main-title"><i class="fa fa-search"></i></i>&nbsp;&nbsp;[$Search_events_on_portal] <?= CMSSettings::$webdomain ?></h1>
                                            <label>Name</label> <input class="input-block-level" id="" type="text" value="" />
                                        </div>
                                    </div>
                                </div>
                -->
            </div>
        </div>
    </div>
</div>

<script>

    var dataSource = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('category', 'company_name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?= Utils::getComponentUrl('Businesses/ajx_advanceSearch') ?>?query=%QUERY',
            wildcard: '%QUERY'
        }

    });
    dataSource.initialize();
    $('.typeahead').typeahead({
        hint: true,
        minLength: 1,
        highlight: true
    }, {
        name: 'businesses',
        display: function (item) {
            return (item.company_name).toLowerCase()
        },
        source: dataSource.ttAdapter(),
        limit: 10,
        templates: {
            empty: [
                '<div class="empty">Nessun elemento trovato</div>'].join('\n'),
            suggestion: function (data) {
                if (data.image != '' || data.image != null) {
                    var image = data.image;
                    if (image != null) {
                        image = image.split("/");
                        if (image.length == 3) {
                            image.splice(2, 0, "150x150");
                        }
                        if (image.length == 2) {
                            image.splice(1, 0, "150x150");
                        }
                    }
                }
                if (image == null || image.toString() == "") {
                    image = "150x150/default.jpg";
                }
<?php
if ($_SERVER['HTTP_HOST'] == 'cittanelweb.it' || $_SERVER['HTTP_HOST'] == 'www.cittanelweb.it') {
	$remote_image = Utils::genMediaUrl('thumbs/businesses/', true);
} elseif ($_SERVER['HTTP_HOST'] == 'netirane.al' || $_SERVER['HTTP_HOST'] == 'www.netirane.al') {
	$thumbs_baseurl = 'http://www.neshqiperi.al/data/neshqiperi.al/media/thumbs/businesses/';
}
$http_host = Utils::getComponentUrl('Businesses/b_show');
?>
                return '<div class="BusinessCard u-cf Typeahead-suggestion Typeahead-selectable">' +
                        '<a href="<?= $http_host ?>/' + data.url_slug + '" >' +
                        '<img class="BusinessCard-avatar" src="<?= $thumbs_baseurl ?>' + image.toString().replace(/,/g, '/') + '?far=1&bg=FFFFFF">' +
                        '<div class="BusinessCard-details">' +
                        '<div class="BusinessCard-Name">' + data.company_name + '</div>' +
                        '<div class="BusinessCard-description">' + data.category + '</div>' +
                        '</div></a>' +
                        '</div>'
            }
        }
    });

</script>

<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });

        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });

    }).call(this);



    $(document).ready(function () {
        $('input').each(function () {
            if ($(this).val()) {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(window).load(function () {
                $('input:-webkit-autofill').each(function () {

                    if ($(this).length > 0 || $(this).val().length > 0) {
                        $(this).addClass('filled');
                    } else {
                        $(this).removeClass('filled');
                    }
                });
            });
        }
    });
</script>
