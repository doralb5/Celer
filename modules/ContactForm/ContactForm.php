<?php

class ContactForm extends BaseModule
{
	public function __construct($block_id, $position, $package)
	{
		parent::__construct($block_id, $position, $package);
	}

	public function execute($params = array())
	{
		$this->view->set('params', $params);
		$this->view->set('ajx_action_url', Utils::getComponentUrl('ContactForm/ajx_send_message'));
		$view = (isset($params['view'])) ? $params['view'] : 'ContactForm-standard';
		$this->view->render($view);
	}
}
