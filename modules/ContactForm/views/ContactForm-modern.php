<?php
$show_title = isset($params['show_title']) ? $params['show_title'] : 1;
$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';
?>
<!--contact-->
<section class="<?= ($show_title) ? 'section-padding' : '' ?>" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<?php if ($show_title) {
	?>
					<h2 class="ser-title">[$Contact_us]</h2>
					<hr class="botm-line" />
				<?php
} ?>
            </div>

            <div class="col-md-4 col-sm-4" style="font-size:15px;">

                <h3>[$Contact_information]</h3>

                <div class="space">&nbsp;</div>
				<?php if (CMSSettings::$company_address != '') {
		?>
					<p><i class="fa fa-map-marker fa-fw pull-left fa-2x"></i><?= CMSSettings::$company_address ?></p>
				<?php
	} ?>

                <div class="space">&nbsp;</div>

                <p><i class="fa fa-envelope-o fa-fw pull-left fa-2x"></i><?= CMSSettings::$company_mail ?></p>

                <div class="space">&nbsp;</div>
				<?php if (CMSSettings::$company_tel != '') {
		?>

					<p><i class="fa fa-phone fa-fw pull-left fa-2x"></i><?= CMSSettings::$company_tel ?></p>
				<?php
	} ?>

				<?php if (CMSSettings::$webdomain == 'domaltech.eu') {
		?>
					<div class="space">&nbsp;</div>
					<p><img alt="" src="/data/domal.bluehat.al/media/numeroverde_domal.png" /></p>

				<?php
	} ?>


            </div>

            <div class="col-md-8 col-sm-8 marb20">
                <div class="contact-info">
                    <h3 class="cnt-ttl">[$request_contact]</h3>

                    <div class="space">&nbsp;</div>

                    <div id="sendmessage"></div>

                    <div id="errormessage"></div>

                    <form role="form" id="contactForm" method="post">
						<?php if (isset($success)) {
		?>
							<div class="alert alert-success" role="alert">
								<i class="fa fa-check"></i>
								<?= $success ?>
							</div>
						<?php
	} ?>


                        <div class="form-group">
                            <input class="form-control br-radius-zero" data-msg="Please enter at least 4 chars" data-rule="minlen:4" id="name" name="name" placeholder="[$name_placehoder]" type="text" value="<?= $name ?>" required />
                            <div class="validation">&nbsp;</div>
                        </div>

                        <div class="form-group">
                            <input class="form-control br-radius-zero" data-msg="Please enter a valid email" data-rule="email" id="email" name="email" placeholder="[$email_placehoder]" type="email" value="<?= $email ?>" required/>
                            <div class="validation">&nbsp;</div>
                        </div>

                        <div class="form-group">
                            <textarea class='form-control br-radius-zero' name='message' id="message" style="max-width:100%" rows="10" cols="100" placeholder="[$message_placehoder]" required><?= $message ?></textarea>
                            <div class="validation">&nbsp;</div>
                        </div>

                        <div class="form-action">
                            <input type="hidden" name="send" value="1" />
                            <button class="btn btn-form" type="submit" id="contactSubmit" value="1">[$send_request]</button>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</section>

<script>

    $(document).ready(function () {

        $("#contactSubmit").click(function (e) {

            // Prevent default posting of form - put here to work in case of errors
            e.preventDefault();

            // setup some local variables
            var $form = $('#contactForm');

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            // Serialize the data in the form
            var serializedData = $form.serialize();

            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            request = $.ajax({
                url: "<?= $ajx_action_url ?>",
                type: "post",
                data: serializedData
            });

            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                $('#sendmessage').html(' ');
                $('#sendmessage').css('display', 'none');
                $('#errormessage').html(' ');
                $('#errormessage').css('display', 'none');

                var resp = JSON.parse(response);

                if ('msg' in resp) {
                    $('#sendmessage').html(resp['msg']);
                    $('#sendmessage').css('display', 'block');
                } else {
                    $('#errormessage').html(resp['error']);
                    $('#errormessage').css('display', 'block');
                }

            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                // Log the error to the console
                console.log(
                        "The following error occurred: " +
                        textStatus, errorThrown
                        );
                $('#errormessage').html("The following error occurred: " + textStatus);
                $('#errormessage').css('display', 'block');
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });

        });
    });

</script>

<!--/ contact-->