<?php

class CustomForm extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('mail_view', (isset($parameters['mail_view'])) ? $parameters['mail_view'] : '');
		$this->view->set('mail_subject', (isset($parameters['mail_subject'])) ? $parameters['mail_subject'] : '');
		$this->view->set('mail_recipient', (isset($parameters['mail_recipient'])) ? $parameters['mail_recipient'] : '');

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-custom-form';

		$this->view->set('form_action', Utils::getComponentUrl('CustomForm/sendForm'));
		$this->view->render($view);
	}
}
