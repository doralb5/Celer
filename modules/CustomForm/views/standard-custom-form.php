<?php HeadHTML::AddJS('validator.min.js'); ?>
<form id="acquista-form" class="" action="<?= Utils::getComponentUrl('CustomForm/ajax_SendForm') ?>" method="post" data-toggle="validator" role="form">
    <input type="hidden" name="mail_view" value="<?= $mail_view ?>" />
    <input type="hidden" name="mail_subject" value="<?= $mail_subject ?>" />
    <input type="hidden" name="mail_recipient" value="<?= $mail_recipient ?>" />
    <input type="hidden" name="send" value="1" />
    <div class="form-group">
        <label>[$Name]</label>
        <input class="form-control" required  name="cf_firstname" data-placement="top" data-minlength="3" data-error="Questo formato non è valido!" type="text">
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label>[$Surname]</label>
        <input class="form-control" required  name="cf_lastname" data-minlength="3" data-placement="top"  type="text" data-error="Questo formato non è valido">
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label>[$Email]</label>
        <input class="form-control" required  name="cf_email"   data-placement="top" type="email" data-error="Questo formato non è valido">
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label>[$Telephone]</label>  
        <input class="form-control" required name="cf_phone" placeholder="0123456789" data-placement="top" type="tel" pattern="([0-9]{7,})" data-error="Digitare solo i numeri (senza spazio tra)">
        <div class="help-block with-errors"></div>
    </div>



    <div class="form-group text-center send-acquista">
        <button type="submit" class="btn btn-success btn-submit">[Accept]</button>
        <div class="ajax_message"></div>
    </div>
</form>
<script type="text/javascript">
    $('#acquista-form').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // handle the invalid form...
        } else {
            // everything looks good!
            $.ajax({
                type: 'post',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                success: function (data) {
                    if (data.success === true)
                        $('.ajax_message').html('<div class="ajax_message_success">' + data.message + '</div>');
                    else {
                        console.log(data);
                        $('.ajax_message').html('<div class="ajax_message_error">' + data.message + '</div>');
                    }
                },
                error: function (response) {
                    console.log(response);
                }
            });
            e.preventDefault();
            return false;
        }
    });

</script>
