<?php

class LastEvents extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';
		$featured = (isset($parameters['featured'])) ? $parameters['featured'] : '';
		$id_category = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';

		$events_md = Loader::getModel('Events');

		if ($order == '') {
			$order = 'creation_date DESC';
		}

		$filter = '1 ';
		if ($featured != '') {
			$filter .= "AND featured = '{$featured}' ";
		}

		if ($id_category != '') {
			$filter .= "AND id_category = '{$id_category}' ";

			$category = $events_md->getCategory($id_category);
			$this->view->set('category', $category);
		}

		$events = $events_md->getList($limit, 0, $filter, $order);
		$this->view->set('parameters', $parameters);
		$this->view->set('events', $events);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'last-events';
		$this->view->render($view);
	}

	// Function renderEventesByDate created for AJAX load events by selected start_date on EventsCalendar module . Doralb Kurti - 14-10-2016

	public function renderEventsByDate($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';
		$filter = '1 ';
		$events_md = Loader::getModel('Events');
		$events = $events_md->getList($limit, 0, $filter, $order);
		$this->view->set('events', $events);
		$view = 'events-list-by-date';
		$this->view->render($view);
	}
}
