<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<?php
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 350;
//$location = (isset($_POST['location'])) ? $_POST['location'] : '';
//$category_id = (isset($_POST['category_id'])) ? $_POST['category_id'] : '';
//$subcategory_id = (isset($_POST['subcategory_id'])) ? $_POST['subcategory_id'] : '';
//$start_date = (isset($_POST['start_date'])) ? $_POST['start_date'] : '';
//$pay = (isset($_POST['pay'])) ? $_POST['pay'] : '';
?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 800;
?>
<?php $currency = (isset($_POST['currency'])) ? $_POST['currency'] : ((isset($event['currency'])) ? $event['currency'] : 'EUR'); ?>


<!--<script type="text/javascript">
    'function' != typeof loadGsLib && (loadGsLib = function () {
        var e = document.createElement("script");
        e.type = "text/javascript", e.async = !0, e.src = '//api.at.getsocial.io/widget/v1/gs_async.js?id=080ddc';
        var t = document.getElementsByTagName("script")[0];
        t.parentNode.insertBefore(e, t)
    })();
</script>-->
<style>
    .body-content {
        background-color: rgba(255, 255, 255, 0);
        padding: 0px;
    }
    .calendar {
        right: 15px;
    }
</style>


<div class="page-event">

	<?php
	if (count($events)) {
		$totalElements = 0;
		$elements_per_page = 10;
		foreach ($events as $event) {
			$start_date = $_POST['start_date'];
			$timeString = strtotime($event->start_date);
			$dateOfEvent = date('Y-m-d', $timeString);
			if ($dateOfEvent == $start_date) {
				$totalElements++; ?>
				<?php
				$date = new DateTime($event->start_date);
				$end_date = new DateTime($event->end_date);
				$categoryPath = ((!is_null($event->id_category)) ? "/{$event->id_category}" : ''); ?>
				<div class="corniz wow fadeInLeft animated" data-wow-delay=".5s">  
					<div class="row">

						<div class="col-xs-12 col-sm-4 col-lg-4 col-md-4 picture-event">			
							<a href="<?= Utils::getComponentUrl('Events/show_event/' . Utils::url_slug($event->title) . '-' . $event->id) ?>"  class="event-titull">	
								<?php if ($event->image != '') {
					?>
									<img  src="<?= Utils::genThumbnailUrl("events{$categoryPath}/{$event->image}", $w, $h, array('zc' => 1)) ?>" class="img-responsive" >	<?php
				} ?>
							</a>

						</div>


						<div class="col-xs-12 col-sm-8 col-lg-8 col-md-8"> 

							<div class="content-event"> 

								<h1 class="event-title">
									<a href="<?= Utils::getComponentUrl('Events/show_event/' . Utils::url_slug($event->title) . '-' . $event->id) ?>"  class="event-titull">
										<?= $event->title ?>
									</a> 
								</h1>
								<hr class="break-line">
								<div class="social">

									<!--                            <div class="business-social-share">
																		<div class="getsocial gs-inline-group"></div>
																	</div>-->

									<span class="paragrafi"><i class="fa fa-map-marker"></i> &nbsp;<?= $event->location ?></span>
									<div class="paragrafi"><i class="fa fa-clock-o"></i> &nbsp;[$<?= strtolower($date->format('F')); ?>]&nbsp;<?= $date->format('H:i') ?> - <?= $end_date->format('H:i') ?></div>


								</div>
								<hr class="break-line">
								<p class="paragrafi">
									<?= StringUtils::CutString(strip_tags($event->description), $taglia_content) ?>
								</p>

							</div>
							<p class="calendar"><?= $date->format('d'); ?><em>[$<?= strtolower($date->format('F')); ?>]</em></p>
						</div>

					</div>
				</div>
				<?php
			}
		} ?> 
		<?php if ($totalElements > $elements_per_page) {
			?>
			<div class="col-md-12 pagination">
				<?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
			</div>
		<?php
		} ?>
	<?php
	} else {
		?>
		<p class="text-center">[$text_description]</p>
	<?php
	} ?>
</div>
<script>
    jQuery('#datetimepicker').datetimepicker({
        timepicker: false,
        format: 'd.m.Y'
    });
</script>

