<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 450;
?>

<div id="last-events">

	<?php foreach ($events as $event) {
	?>

		<div class="row">
			<div class ="col-md-12">

				<div class="main-content pright wow fadeInLeft animated" data-wow-delay=".9s">

					<span class="icon-type">
						<i aria-hidden="true" class="fa fa-calendar f-icons"></i>
					</span> 

					<div class="main-picture">
						<img alt="" class="img-responsive" src="<?= Utils::genThumbnailUrl("events/{$event->id_category}/" . $event->image, $w, $h, array('zc' => 1)) ?>"/>
					</div>


					<a href="<?= Utils::getComponentUrl('Events/show_event') . '/' . Utils::url_slug($event->title) . '-' . $event->id; ?>">
						<div class="main-title"> 
							<?= $event->title ?> 
						</div>
					</a>
				</div >

			</div >
		</div >
	<?php
} ?>

</div >