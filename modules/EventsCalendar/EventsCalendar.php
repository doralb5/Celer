<?php

class EventsCalendar extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';
		$filter = '1 ';

		$events_md = Loader::getModel('Events');

		$events = $events_md->getList($limit, 0, $filter, $order);
		$this->view->set('events', $events);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-calendar';
		$this->view->render($view);
	}

	public function renderCalendar()
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';
		$filter = '1 ';
		$events_md = Loader::getModel('Events');
		$events = $events_md->getList($limit, 0, $filter, $order);
		$this->view->set('events', $events);
		$view = 'standard-calendar-table';
		$this->view->render($view);
	}
}
