

<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/calendartheme.css'); ?>


<div id="calendar"><div class="navigation">
        <a class="prev" href="index.php?month=10-2016" onclick="$('#calendar').load('index.php?month=10-2016&amp;_r=' + Math.random()); return false;"></a>
        <div class="title">November, 2016</div>
        <a class="next" href="index.php?month=12-2016" onclick="$('#calendar').load('index.php?month=12-2016&amp;_r=' + Math.random()); return false;"></a>
    </div>

    <table>
        <tbody><tr>

            <th class="weekday">sun</th>
            <th class="weekday">mon</th>
            <th class="weekday">tue</th>
            <th class="weekday">wed</th>
            <th class="weekday">thu</th>
            <th class="weekday">fri</th>
            <th class="weekday">sat</th>
            </tr>
            <tr>
            <td class=""><a href="javascript: void(0)">30</a></td>
            <td class=""><a href="javascript: void(0)">31</a></td>
            <td class="current"><a href="javascript: void(0)">1</a></td>
            <td class="current"><a href="javascript: void(0)">2</a></td>
            <td class="current"><a href="javascript: void(0)">3</a></td>
            <td class="current"><a href="javascript: void(0)">4</a></td>
            <td class="current"><a href="javascript: void(0)">5</a></td></tr>
            <tr>
            <td class="current"><a href="javascript: void(0)">6</a></td>
            <td class="current"><a href="javascript: void(0)">7</a></td>
            <td class="current"><a href="javascript: void(0)">8</a></td>
            <td class="current"><a href="javascript: void(0)">9</a></td>
            <td class="current"><a href="javascript: void(0)">10</a></td>
            <td class="current"><a href="javascript: void(0)">11</a></td>
            <td class="current"><a href="javascript: void(0)">12</a></td>
            </tr>
            <tr>
            <td class="current"><a href="javascript: void(0)">13</a></td>
            <td class="current"><a href="javascript: void(0)">14</a></td>
            <td class="current"><a href="javascript: void(0)">15</a></td>
            <td class="current"><a href="javascript: void(0)">16</a></td>
            <td class="current"><a href="javascript: void(0)">17</a></td>
            <td class="current"><a href="javascript: void(0)">18</a></td>
            <td class="current"><a href="javascript: void(0)">19</a></td>
            </tr>
            <tr>
            <td class="current"><a href="javascript: void(0)">20</a></td>
            <td class="current"><a href="javascript: void(0)">21</a></td>
            <td class="current"><a href="javascript: void(0)">22</a></td>
            <td class="current"><a href="javascript: void(0)">23</a></td>
            <td class="current"><a href="javascript: void(0)">24</a></td>
            <td class="current"><a href="javascript: void(0)">25</a></td>
            <td class="current"><a href="javascript: void(0)">26</a></td>
            </tr>
            <tr>
            <td class="current"><a href="javascript: void(0)">27</a></td>
            <td class="current"><a href="javascript: void(0)">28</a></td>
            <td class="current"><a href="javascript: void(0)">29</a></td>
            <td class="current"><a href="javascript: void(0)">30</a></td>
            <td class=""><a href="javascript: void(0)">1</a></td>
            <td class=""><a href="javascript: void(0)">2</a></td>
            <td class=""><a href="javascript: void(0)">3</a></td>
            </tr>
            <tr>
            <td class=""><a href="javascript: void(0)">4</a></td>
            <td class=""><a href="javascript: void(0)">5</a></td>
            <td class=""><a href="javascript: void(0)">6</a></td>
            <td class=""><a href="javascript: void(0)">7</a></td>
            <td class=""><a href="javascript: void(0)">8</a></td>
            <td class=""><a href="javascript: void(0)">9</a></td>
            <td class=""><a href="javascript: void(0)">10</a></td>
            </tr>
        </tbody>
    </table>
</div>

