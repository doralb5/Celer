<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/calendartheme.css'); ?>


<script type="text/javascript">
    function getCalendar(target_div, year, month) {
        $.ajax({
            type: 'POST',
            url: '<?= Utils::getComponentUrl('Events/ajx_calendar/'); ?>',
            data: 'year=' + year + '&month=' + month,
            success: function (html) {
                $('#' + target_div).html(html);
            }
        });
    }



    function getEventsByStartDate(target_div, start_date) {
        $.ajax({
            type: 'POST',
            url: '<?= Utils::getComponentUrl('Events/events_list_by_start_date/'); ?>',
            data: 'start_date=' + start_date,
            success: function (html) {
                $('#' + target_div).html(html);
            }
        });
    }


</script>




<?php
if (isset($_POST['year'], $_POST['month'])) {
	$year = $_POST['year'];
	$month = $_POST['month'];
} else {
	$year = date('Y');
	$month = date('m');
	$dateObj = DateTime::createFromFormat('!m', $month);
	$monthName = $dateObj->format('F');
	$dateYear = ($year != '') ? $year : date('Y');
	$dateMonth = ($month != '') ? $month : date('m');
	$date = $dateYear . '-' . $dateMonth . '-01';
	$currentMonthFirstDay = date('N', strtotime($date));
	$totalDaysOfMonth = cal_days_in_month(CAL_GREGORIAN, $dateMonth, $dateYear);
	$totalDaysOfMonthDisplay = ($currentMonthFirstDay == 7) ? ($totalDaysOfMonth) : ($totalDaysOfMonth + $currentMonthFirstDay);
	$boxDisplay = ($totalDaysOfMonthDisplay <= 35) ? 35 : 42; ?>







	<div id="calendar_div">
		<form method="POST" action="">
			<div id="calendar"><div class="navigation">
					<a  class="prev" href="javascript:void(0);" onclick="getCalendar('right-modules', '<?php echo date('Y', strtotime($date . ' - 1 Month')); ?>', '<?php echo date('m', strtotime($date . ' - 1 Month')); ?>');"></a>
					<div class="title"><?php echo $monthName . ' , ' . $year; ?> </div>
					<a class="next" href="javascript:void(0);" onclick="getCalendar('right-modules', '<?php echo date('Y', strtotime($date . ' + 1 Month')); ?>', '<?php echo date('m', strtotime($date . ' + 1 Month')); ?>');"></a>
				</div>
				<table>
					<tbody>
						<tr>

						<th class="weekday">sun</th>
						<th class="weekday">mon</th>
						<th class="weekday">tue</th>
						<th class="weekday">wed</th>
						<th class="weekday">thu</th>
						<th class="weekday">fri</th>
						<th class="weekday">sat</th>
						</tr>



						<?php
						$dayCount = 1;
	for ($week = 1; $week <= $boxDisplay / 7; $week++) {
		echo '<tr>';

		for ($cb = 1; $cb <= 7; $cb++) {
			if ((($cb >= $currentMonthFirstDay + 1 || $currentMonthFirstDay == 7) || ($week != 1)) && $dayCount <= $totalDaysOfMonth) {
				$currentDate = $dateYear . '-' . $dateMonth . '-' . $dayCount;
				$eventNum = 0;

				foreach ($events as $ev) {
					$timeString = strtotime($ev->start_date);
					$dateOfEvent = date('Y-m-d', $timeString);
					if ($dateOfEvent == $currentDate) {
						$eventNum++;
					}
					$eventslink = Utils::getComponentUrl('Events/events_list/');
				}

				if (strtotime($currentDate) == strtotime(date('Y-m-d'))) {
					print "<td class=\"currentDay\"  date=\" $currentDate \" ><a href=\"javascript:void(0);\" onclick=\"getEventsByStartDate('showlist', '$currentDate')\" >";
				} elseif ($eventNum > 0) {
					print "<td class=\"hasEvent\"  date=\" $currentDate \" ><a href=\"javascript:void(0);\" onclick=\"getEventsByStartDate('showlist', '$currentDate')\" >";
				} else {
					echo '<td date="' . $currentDate . '" >';
				}

				//Date cell
				echo '<span>';
				echo $dayCount;
				echo '</span>';
				echo '</a> </td> ';
				$dayCount++;
			} else {
				?>

									<td><span>  &nbsp;</span></td>


									<?php
			}
		}
		echo '</tr>';
	}
}
					?>
            </table>
            </tbody>
        </div>
    </form>

</div>
