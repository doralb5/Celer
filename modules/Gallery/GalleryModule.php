<?php

class GalleryModule extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';
		$id_category = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';
		$type = (isset($parameters['type'])) ? $parameters['type'] : '';
		$customizedCategory = (isset($parameters['customizedCategory'])) ? $parameters['customizedCategory'] : 0;
		$handleSearch = (isset($parameters['handleSearch'])) ? $parameters['handleSearch'] : 0;
		$show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0;

		$gallery_md = Loader::getModel('Gallery');

		if ($order == '') {
			$order = 'publish_date DESC ';
		}
		$filter = '1 ';
		if ($id_category != '') {
			$filter .= "AND id_category = '{$id_category}' ";
		} elseif ($customizedCategory) {
			$item = Gallery_Model::getCurrentItem();
			$filter .= "AND id_category = '{$item->id_category}' ";
		}

		if ($type != '') {
			$filter .= "AND type = '{$type}' ";
		}
		if ($handleSearch && isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$GalleryItem_tbl = TABLE_PREFIX . 'GalleryItem';
			$GalleryCateg_tbl = TABLE_PREFIX . 'GalleryCategory';
			$searchFields = array(
				//array('field' => "{$GalleryItem_tbl}.id", 'peso' => 100),
				array('field' => "{$GalleryItem_tbl}.title", 'peso' => 90),
				array('field' => "{$GalleryCateg_tbl}.category", 'peso' => 70),
				array('field' => "{$GalleryItem_tbl}.description", 'peso' => 60),
			);
			$items = $gallery_md->search($_REQUEST['query'], $searchFields, $filter, $order, $limit);
		} else {
			$items = $gallery_md->getList($limit, 0, $filter, $order);
		}

		$this->view->set('items', $items);
		$this->view->set('images', $items);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-lastimages';

		$this->view->set('parameters', $parameters);

		if ($handleSearch && isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
			$this->view->render($view);
		} elseif ($handleSearch == '0') {
			$this->view->render($view);
		}
	}
}
