<?php

class LastGalleryImages extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';
		$id_category = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';

		$gallery_md = Loader::getModel('Gallery');

		if ($order == '') {
			$order = 'publish_date DESC';
		}

		$filter = '1';
		if ($id_category != '') {
			$filter .= "AND id_category = '{$id_category}'";
		}

		$images = $gallery_md->getList($limit, 0, $filter, $order);
		$this->view->set('images', $images);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-lastimages';
		$this->view->render($view);
	}
}
