<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/filtered-gallery.css'); ?>
<?php //HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/filtered-gallery.js');?>

<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 1200;
$h = (isset($parameters['h'])) ? $parameters['h'] : 0;
$w_mini = 480;
$h_mini = 290;
$show_title = (isset($parameters['show_title'])) ? $parameters['show_title'] : 0;
$show_description = (isset($parameters['show_description'])) ? $parameters['show_description'] : 0;
$show_gallery_button = (isset($parameters['show_gallery_button'])) ? $parameters['show_gallery_button'] : 0;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$albums_to_show = (isset($parameters['albums_to_show'])) ? $parameters['albums_to_show'] : 'id';
?>

<section class="portfolioFour bg-img-port bg-fixed" id="portfolio-4">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="dart-headingstyle-one dart-mb-10">
                    <h2 class="dart-heading text-center white">Featured Works</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 text-center dart-mb-20">
                <div class="button-group filters-button-group clearfix">
                    <button class="button active filter-button" data-filter="all">Show All</button>
                    <button class="button filter-button" data-filter="1">Graphic Design</button>
                    <button class="button filter-button" data-filter="2">Photography</button>
                    <button class="button filter-button" data-filter="3">Web Design</button>
                    <button class="button filter-button" data-filter="html">HTML</button></div>
            </div>
        </div>

        <div class="row no-gutter">

			<?php foreach ($images as $img) {
	?>

				<div class="col-lg-4 col-sm-4 col-xs-12 filter <?= $img->id_category ?>">
					<div class="portfolio_4">
						<div class="ImageWrapper"><img alt="" class="img-responsive" src="<?= Utils::genThumbnailUrl("gallery/$img->id_category/" . $img->item_name, $w_mini, $h_mini, array('zc' => 1)) ?>"><!-- thumb image -->
							<div class="ImageOverlay">&nbsp;</div>

							<div class="Buttons_icon"><span class="WhiteRounded"><a class="item-box" href="<?= Utils::genThumbnailUrl("gallery/$img->id_category/" . $img->item_name, $w) ?>" rel="alternate"><i class="fa fa-expand"></i></a> </span></div>

							<div class="item-info">
								<div class="headline dart-fs-16"><?= $img->title ?></div>

								<div class="date"><?= $img->description ?></div>
							</div>
						</div>
					</div>
				</div>

			<?php
} ?>

        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 dart-pt-30 text-center"><a class="btn transparent rd-stroke-btn border_1px dart-btn-sm" href="#">Load More</a></div>
        </div>
    </div>
</section>