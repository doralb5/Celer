
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/slider-pro.min.css'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.sliderPro.min.js'); ?>



<script type="text/javascript">
	$( document ).ready(function( $ ) {
		$( '#example5' ).sliderPro({
			width: 1000,
			height: 440,
			orientation: 'vertical',
			loop: false,
			arrows: true,
			buttons: false,
			thumbnailsPosition: 'right',
			thumbnailPointer: true,
			thumbnailWidth: 290,
			breakpoints: {
				800: {
					thumbnailsPosition: 'bottom',
					thumbnailWidth: 270,
					thumbnailHeight: 100
				},
				500: {
					thumbnailsPosition: 'bottom',
					thumbnailWidth: 120,
					thumbnailHeight: 50
				}
			}
		});
	});
</script>



	<div id="example5" class="slider-pro">
		<div class="sp-slides">
			<div class="sp-slide">
				<img class="sp-image" src="../src/css/images/blank.gif"
					data-src="http://bqworks.com/slider-pro/images/image1_medium.jpg"
					data-retina="http://bqworks.com/slider-pro/images/image1_large.jpg"/>

				<div class="sp-caption">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
			</div>

	        <div class="sp-slide">
	        	<img class="sp-image" src="../src/css/images/blank.gif"
	        		data-src="http://bqworks.com/slider-pro/images/image2_medium.jpg"
	        		data-retina="http://bqworks.com/slider-pro/images/image2_large.jpg"/>

				<div class="sp-caption">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
			</div>

			<div class="sp-slide">
				<img class="sp-image" src="../src/css/images/blank.gif"
					data-src="http://bqworks.com/slider-pro/images/image3_medium.jpg"
					data-retina="http://bqworks.com/slider-pro/images/image3_large.jpg"/>

				<div class="sp-caption">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</div>
			</div>

			<div class="sp-slide">
				<img class="sp-image" src="../src/css/images/blank.gif"
					data-src="http://bqworks.com/slider-pro/images/image4_medium.jpg"
					data-retina="http://bqworks.com/slider-pro/images/image4_large.jpg"/>

				<div class="sp-caption">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
			</div>

			

			
		</div>

		<div class="sp-thumbnails">
			<div class="sp-thumbnail">
				<div class="sp-thumbnail-image-container">
					<img class="sp-thumbnail-image" src="http://bqworks.com/slider-pro/images/image1_thumbnail.jpg"/>
				</div>
				<div class="sp-thumbnail-text">
					<div class="sp-thumbnail-title">Lorem ipsum</div>
					<div class="sp-thumbnail-description">Dolor sit amet, consectetur adipiscing</div>
				</div>
			</div>


			<div class="sp-thumbnail">
				<div class="sp-thumbnail-image-container">
					<img class="sp-thumbnail-image" src="http://bqworks.com/slider-pro/images/image4_thumbnail.jpg"/>
				</div>
				<div class="sp-thumbnail-text">
					<div class="sp-thumbnail-title">Ullamco oris</div>
					<div class="sp-thumbnail-description">Nisi ut aliquip ex ea commodo consequat</div>
				</div>
			</div>

			<div class="sp-thumbnail">
				<div class="sp-thumbnail-image-container">
					<img class="sp-thumbnail-image" src="http://bqworks.com/slider-pro/images/image5_thumbnail.jpg"/>
				</div>
				<div class="sp-thumbnail-text">
					<div class="sp-thumbnail-title">Duis aute</div>
					<div class="sp-thumbnail-description">Irure dolor in reprehenderit</div>
				</div>
			</div>

			<div class="sp-thumbnail">
				<div class="sp-thumbnail-image-container">
					<img class="sp-thumbnail-image" src="http://bqworks.com/slider-pro/images/image6_thumbnail.jpg"/>
				</div>
				<div class="sp-thumbnail-text">
					<div class="sp-thumbnail-title">Esse cillum</div>
					<div class="sp-thumbnail-description">Dolore eu fugiat nulla pariatur excepteur</div>
				</div>
			</div>

			
		</div>
    </div>
