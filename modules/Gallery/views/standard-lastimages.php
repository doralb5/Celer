<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/owl-carousel.css'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/owl-carousel.js'); ?>
<?php
$w = (isset($parameters['w'])) ? $parameters['w'] : 210;
$h = (isset($parameters['h'])) ? $parameters['h'] : 210;
$thumbopt = array();
$imagetype = (isset($parameters['image_type'])) ? $parameters['image_type'] : '';
switch ($imagetype) {
	case 'zc':
		$thumbopt = array('zc' => 1);
		break;
	case 'prop':
		break;
	case 'same':
		$thumbopt = array('far' => 1, 'bg' => 'ffffff');
		break;
}
?>

<?php if (count($images)) {
	?>

	<div class="banner-carousel">
		<div id="owl-carousel_<?= $block_id ?>" class="owl-carousel text-center">
			<?php foreach ($images as $img) {
		?>
				<div class="item">
					<img class="lazyOwl" src="<?= Utils::genThumbnailUrl("gallery/$img->id_category/" . $img->item_name, $w, $h, $thumbopt) ?>" alt="">
				</div>
			<?php
	} ?>
		</div>
	</div>

	<script>
		$(document).ready(function () {
			$("#owl-carousel_<?= $block_id ?>").owlCarousel({
				items: 5,
				lazyLoad: true,
				autoPlay: true,
				pagination: true,
			});
		});
	</script>

<?php
} ?>
