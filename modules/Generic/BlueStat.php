<?php

class BlueStat extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-bluestat';
		$this->view->render($view);
	}
}
