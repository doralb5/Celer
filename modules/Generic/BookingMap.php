<?php

class BookingMap extends BaseModule
{
	public $biz;

	public function __construct($block_id = '', $position = '', $package = '')
	{
		parent::__construct($block_id, $position, $package);
	}

	public function execute($params = array())
	{
		$google_api_key = CMSSettings::$google_maps_api;
		$this->view->set('google_api_key', $google_api_key);
		$this->view->render('maps');
	}
}
