<?php

require_once DOCROOT . CONTROLLERS_PATH . 'WebPage' . DS . 'WebPage.php';

class BreadCrumb extends BaseModule
{
	public function execute($parameters = array())
	{
		WebPage::$breadcrumb->setWithSymbol(true);
		WebPage::$breadcrumb->setSymbol('&nbsp;&raquo;&nbsp;');

		$view = isset($parameters['view']) ? $parameters['view'] : '';

		if ($view != '') {
			$this->view->render($view);
		} else {
			WebPage::$breadcrumb->render();
		}
	}
}
