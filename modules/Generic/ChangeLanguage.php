<?php

class ChangeLanguage extends BaseModule
{
	public function execute($parameters = array())
	{
		if (isset($_POST['ch_lang'])) {
			$language = $_POST['ch_lang'];

			if (!is_numeric(array_search($language, CMSSettings::$available_langs))) {
				return;
			}

			$request = $_SERVER['REQUEST_URI'];
			//$request = FCRequest::getUrl();
			$url_arguments = '';
			if (strpos($_SERVER['REQUEST_URI'], '?') !== false) {
				$url_arguments = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?'), strlen($_SERVER['REQUEST_URI']));
				$request = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?'));
			}

			$request = explode('/', trim($request, '/'));

			if (is_numeric(array_search($request[0], CMSSettings::$available_langs))) {
				$wasLang = $request[0];
				array_shift($request);

				$webpage_md = Loader::getModel('WebPage');
				if ($request[0] == '') {
					$page = $webpage_md->getHomePage($wasLang);
				} else {
					$page = $webpage_md->getPageByAlias($request[0], $wasLang);

					$wasAlias = $request[0];
					array_shift($request);
				}

				//$component = $page->getComponent();
				//$action = $page->getAction();
				//if (count($page->getActionParams())) {
				// $parameters = implode('/', $page->getActionParams());
				//} else {
				//   $parameters = null;
				//}
				$result = $webpage_md->getPageContentByPage($page->getId(), $language);
				//$result = $webpage_md->getPageContent($component, $action, $parameters, $language);

				$alias = (!is_null($result)) ? $result['alias'] : $wasAlias;
				array_unshift($request, $alias);
			}
			$request = '/' . implode('/', $request) . $url_arguments;
			$request = Utils::genUrl($request, true, $language);
			Utils::RedirectTo($request);
		}

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-change-language';
		$this->view->render($view);
	}
}
