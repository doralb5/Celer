<?php

class CookiesBar extends BaseModule
{
	public function execute($parameters = array())
	{
		if (isset($parameters['id_page'])) {
			$page_md = Loader::getModel('WebPage');
			$page = $page_md->getPageById($parameters['id_page'], FCRequest::getLang());
			$alias = $page->getAlias();
			$link = Utils::genUrl($alias, true);
		} elseif (isset($parameters['id_article'])) {
			$link = Utils::getComponentUrl("Articles/show_article/{$parameters['id_article']}");
		} elseif (isset($parameters['id_block'])) {
			$link = Utils::getComponentUrl("StaticBlocks/printBlock/{$parameters['id_block']}");
		} else {
			$link = '';
		}

		$this->view->set('link', $link);
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-cookies-bar';
		$this->view->render($view);
	}
}
