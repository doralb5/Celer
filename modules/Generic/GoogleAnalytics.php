<?php

class GoogleAnalytics extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'googleAnalytics';
		$this->view->render($view);
	}
}
