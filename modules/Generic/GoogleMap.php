<?php

class GoogleMap extends BaseModule
{
	public $biz;

	public function __construct($block_id = '', $position = '', $package = '')
	{
		parent::__construct($block_id, $position, $package);
	}

	public function execute($params = array())
	{
		if (isset($params['coordinates']) && $params['coordinates'] != '') {
			$this->view->set('coordinates', $params['coordinates']);
		}
		if (isset($params['marker_icon']) && $params['marker_icon'] != '') {
			$this->view->set('marker_icon', $params['marker_icon']);
		}
		$google_api_key = CMSSettings::$google_maps_api;
		$this->view->set('google_api_key', $google_api_key);
		$this->view->render('googlemap');
	}
}
