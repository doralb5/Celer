<?php

class Logo extends BaseModule
{
	public function execute($parameters = array())
	{
		$logo_file = CMSSettings::$logo;
		$this->view->set('logo_file', $logo_file);

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		$this->view->set('parameters', $parameters);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-logo';
		$this->view->render($view);
	}
}
