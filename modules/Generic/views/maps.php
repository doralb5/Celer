<div class="wrap-map" id="wrap-map" style="width:100%; height: 100%; min-height:500px;">&nbsp;</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?= $google_api_key ?>&callback=initMap"></script><script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        // Basic options for a simple Google Map
        var featureOpts = [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#444444"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#46bcec"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ];
        var markerPosition = new google.maps.LatLng(38.719279, 16.024844);
        var mapOptions = {
            zoom: 13,
            scrollwheel: false,

            // The latitude and longitude to center the map (always required)
            center: markerPosition,

            // How you would like to style the map. 
            styles: featureOpts
        };

        var mapElement = document.getElementById('wrap-map');

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it

        var marker = new google.maps.Marker({
            position: markerPosition,
            nid: "marker-1",
            title: "marker-1",
            map: map,
            icon: 'http://bluepalacelandro.it/data/bluepalace.weweb.al/media/bluepalace-icon.png'
        });

        var oldCenter = map.getCenter();

        // Center change
        google.maps.event.addListener(map, 'dragend', function () {
            oldCenter = map.getCenter();
        });

        //Center map
        google.maps.event.addDomListener(window, 'resize', function () {
            map.setCenter(oldCenter);
            oldCenter = map.getCenter();
        });
    }
</script>