<?php
$id_site = (isset($parameters['id_site'])) ? $parameters['id_site'] : null;
?>

<?php if (!is_null($id_site)) {
	?>
	<!-- Piwik -->
	<script type="text/javascript">
		var _paq = _paq || [];
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function () {
			var u = "//www.bluestat.it/panel/";
			_paq.push(['setTrackerUrl', u + 'piwik.php']);
			_paq.push(['setSiteId', <?= $id_site ?>]);
			var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
			g.type = 'text/javascript';
			g.async = true;
			g.defer = true;
			g.src = u + 'piwik.js';
			s.parentNode.insertBefore(g, s);
		})();
	</script>
	<noscript><p><img src="//www.bluestat.it/panel/piwik.php?idsite=<?= $id_site ?>" style="border:0;" alt=""/></p>
	</noscript>
	<!-- End Piwik Code -->
<?php
} else {
		?>
	<script>
		console.log("ID Site not setted for Bluestat.");
	</script>
<?php
	} ?>