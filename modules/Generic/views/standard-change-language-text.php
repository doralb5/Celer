<style>
    .flag-button {
        background-color: transparent;
        display: inline-block;
        width: 34px;
        height: 34px;
        border: 1px solid white;
        border-radius: initial;
        text-transform: uppercase;
        padding: 5px;
        color: #fff;
        margin: 0 2px;
    }
    button.flag-button:hover {
        background-color: rgba(255,255,255,0.8);
    }
    button.flag-button.active {
        background-color: white;
        color: #243a6d;
    }
    button.flag-button.active span {

    }
    button.flag-button span {

    }

</style>

<div class="lang_btn">

    <form method="post" action="">
		<?php foreach (CMSSettings::$available_langs as $lang) {
	?>
			<button type="submit" value="<?= $lang ?>" name="ch_lang" class="flag-button btn btn-xs <?= (FCRequest::getLang() == $lang) ? 'active' : '' ?>"><span><?= $lang ?></span></button>
				<?php
} ?>
    </form>

</div>