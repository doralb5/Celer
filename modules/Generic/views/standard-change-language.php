<div class="lang_btn">

    <form method="post" action="">
		<?php foreach (CMSSettings::$available_langs as $lang) {
	?>
			<button type="submit" value="<?= $lang ?>" name="ch_lang" class="flag-button <?= (FCRequest::getLang() == $lang) ? 'active' : '' ?>"><img src="<?= WEBROOT . TEMPLATES_PATH . $this->template_name . '/images/' . $lang ?>.png" width="25px"></button>
			<?php
} ?>
    </form>

</div>