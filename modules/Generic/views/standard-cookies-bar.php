<?php
if (file_exists(DOCROOT . DATA_DIR . $this->view_path . 'css/jquery.cookiebar.css')) {
	HeadHTML::AddStylesheet(WEBROOT . DATA_DIR . $this->view_path . 'css/jquery.cookiebar.css');
} else {
	HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/jquery.cookiebar.css');
}
HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.cookiebar.js');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $.cookieBar({
            message: '[$cookiePopup_text]',
            acceptButton: true,
            acceptText: 'OK',
            acceptFunction: null,
            declineButton: false,
            declineText: '[$cookiePopup_disableButton]',
            declineFunction: null,
            policyButton: true,
            policyText: '[$cookiePopup_policyButton]',
            policyURL: '<?= $link ?>',
            autoEnable: true,
            acceptOnContinue: false,
            acceptOnScroll: false,
            acceptAnyClick: false,
            expireDays: 365,
            renewOnVisit: false,
            forceShow: false,
            effect: 'slide',
            element: 'body',
            append: false,
            fixed: false,
            bottom: false,
            zindex: '',
            domain: '<?= $_SERVER['HTTP_HOST'] ?>',
            referrer: '<?= $_SERVER['HTTP_HOST'] ?>'
        });
    });
</script>
