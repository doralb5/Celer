<?php
if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone')) {
	$w = 150;
	$h = 0;
} elseif (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
	$w = 150;
	$h = 0;
} else {
	$w = (isset($parameters['width'])) ? $parameters['width'] : 0;
	$h = (isset($parameters['height'])) ? $parameters['height'] : 0;
}
?>
<?php if ($logo_file != '') {
	?>
	<div class="logo">
		<a id="logo_<?= $block_id ?>" href="<?= Utils::genUrl('/') ?>">
			<img class="img_logo" src="<?= Utils::genThumbnailUrl($logo_file, $w, $h, array('q' => 100)) ?>"/>
		</a>
	</div>
<?php
} ?>
