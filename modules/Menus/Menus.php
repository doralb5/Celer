<?php

class Menus extends BaseModule
{
	public function execute($parameters = array())
	{
		$id_menu = isset($parameters['id_menu']) ? $parameters['id_menu'] : '';

		$currentLang = FCRequest::getLang();

		$parentItems = $this->model->getMenuItems("id_menu = {$id_menu} AND parent_id IS NULL AND lang = '{$currentLang}' AND enabled= '1'");

		$webpage_md = Loader::getModel('WebPage');
		foreach ($parentItems as &$item) {
			if ($item->type == 'Page') {
				$item->page = $webpage_md->getPageById($item->target, CMSSettings::$default_lang);
			}
		}

		$this->hierarchyMenuItems($parentItems);

		$menu = $this->model->getMenu($id_menu);
		$this->view->set('menu', $menu);

		$this->view->set('MenuItems', $parentItems);

		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-menu';
		$this->view->render($view);
	}

	private function hierarchyMenuItems(&$items)
	{
		foreach ($items as &$item) {
			if ($item->type == 'Page') {
				$webpage_md = Loader::getModel('WebPage');
				$item->page = $webpage_md->getPageById($item->target, FCRequest::getLang());
			} else {
				$webpage_md = Loader::getModel('WebPage');
				$item->page = $webpage_md->getPageById($item->target, FCRequest::getLang());
			}

			$item->subitems = $this->model->getMenuItems("parent_id = {$item->id} AND enabled = '1'");
			$this->hierarchyMenuItems($item->subitems);
		}
	}
}
