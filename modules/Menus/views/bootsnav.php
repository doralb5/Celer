<?php
$show_brand = isset($parameters['show_brand']) ? $parameters['show_brand'] : false;
$show_logo = isset($parameters['show_logo']) ? $parameters['show_logo'] : true;
?>
<nav class="navbar navbar-default navbar-sticky navbar-mobile bootsnav">
    <div class="container">     
        <div class="attr-nav">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar_<?= $block_id ?>"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

			<?php if ($show_logo && CMSSettings::$logo != '') {
	?>
				<a class="navbar-brand " href="/"><img class="img-responsive" src="<?= WEBROOT . MEDIA_ROOT . CMSSettings::$logo ?>"></a>
			<?php
} ?>

			<?php if ($show_brand && false) {
		?>
				<a class="navbar-brand visible-xs-block" href="#"><?= CMSSettings::$website_title ?></a>
			<?php
	} ?>
        </div>
        <div id="navbar_<?= $block_id ?>" class="navbar-collapse collapse">
            <ul class="nav navbar-nav menu-items">

				<?php foreach ($MenuItems as $menuItem) {
		?>


					<?php if (count($menuItem->subitems)) {
			?>

						<?php
						$act = 0;
			foreach ($menuItem->subitems as $subitem) {
				if ($subitem->type == 'Page') {
					if ($subitem->page) {
						if (count($subitem->page->getActionParams())) {
							$params = '/' . implode('/', $subitem->page->getActionParams());
						} else {
							$params = '';
						}
						if ($subitem->page->getComponent() != '') {
							$target = Utils::getComponentUrl($subitem->page->getComponent() . '/' . $subitem->page->getAction() . $params . (($subitem->parameters != '') ? '/' . $subitem->parameters : ''));
						} else {
							$target = Utils::genUrl($subitem->page->getAlias());
						}
					}
					//E inicializojme sa per fillim
					$isActive = 1;
					Utils::MenuPrintActive($target, $isActive);
					if ($isActive) {
						$act = 1;
						break;
					}
				}

				$isActive = 1;
				Utils::MenuPrintActive($subitem->target, $isActive);
				if ($isActive) {
					$act = 1;
					break;
				}
			}

			if ($menuItem->page->isHome()) {
				if ($menuItem->page->getComponent() != '') {
					$target = Utils::getComponentUrl($menuItem->page->getAlias()) . (($menuItem->parameters != '') ? '/' . $menuItem->parameters : '');
				} else {
					$target = Utils::genUrl($menuItem->page->getAlias());
				}
			} else {
				if (count($menuItem->page->getActionParams())) {
					$params = implode('/', $menuItem->page->getActionParams());
				} else {
					$params = '';
				}

				if ($menuItem->page->getComponent() != '') {
					$target = Utils::getComponentUrl($menuItem->page->getComponent() . '/' . $menuItem->page->getAction() . (($params != '') ? '/' . $params : '') . (($menuItem->parameters != '') ? '/' . $menuItem->parameters : ''));
				} else {
					$target = Utils::genUrl($menuItem->page->getAlias());
				}
			} ?>
						<li class="dropdown <?= ($act) ? 'active' : '' ?>">
							<a id="menu_item_<?= $menuItem->id ?>"
							   <a href="<?= $target ?>" class="dropdown-toggle first-anchor" data-toggle="dropdown" role="button"
							   aria-haspopup="true"
							   aria-expanded="false"><span><?= $menuItem->item ?></span></a>
								<ul class="dropdown-menu">
									<?php
									foreach ($menuItem->subitems as $subitem) {
										if ($subitem->page) {
											if (count($subitem->page->getActionParams())) {
												$params = '/' . implode('/', $subitem->page->getActionParams());
											} else {
												$params = '';
											}
										} ?>

										<?php
										if ($subitem->type == 'Page') {
											if ($subitem->page->getComponent() != '') {
												$target = Utils::getComponentUrl($subitem->page->getComponent() . '/' . $subitem->page->getAction() . $params . (($subitem->parameters != '') ? '/' . $subitem->parameters : ''));
											} else {
												$target = Utils::genUrl($subitem->page->getAlias());
											} ?>
											<li <?= Utils::MenuPrintActive($target) ?>><a
													href="<?= $target ?>"><?= $subitem->item ?></a>
											</li>
											<?php
											continue;
										} else {
											$target = $subitem->target; ?>
											<li <?= Utils::MenuPrintActive($target) ?>><a
													href="<?= $target ?>"><?= $subitem->item ?></a>
											</li>
											<?php
											continue;
										} ?>


										<li <?= Utils::MenuPrintActive($subitem->target) ?>><a
												href="<?= $subitem->target ?>"><?= $subitem->item ?></a></li>
										<?php
									} ?>
								</ul>
						</li>

						<?php
						continue;
		} ?>

					<?php
					if ($menuItem->type == 'Page') {
						if ($menuItem->page->isHome()) {
							if ($menuItem->page->getComponent() != '') {
								$target = Utils::getComponentUrl($menuItem->page->getAlias()) . (($menuItem->parameters != '') ? '/' . $menuItem->parameters : '');
							} else {
								$target = Utils::genUrl($menuItem->page->getAlias());
							}
						} else {
							if (count($menuItem->page->getActionParams())) {
								$params = implode('/', $menuItem->page->getActionParams());
							} else {
								$params = '';
							}

							if ($menuItem->page->getComponent() != '') {
								$target = Utils::getComponentUrl($menuItem->page->getComponent() . '/' . $menuItem->page->getAction() . (($params != '') ? '/' . $params : '') . (($menuItem->parameters != '') ? '/' . $menuItem->parameters : ''));
							} else {
								$target = Utils::genUrl($menuItem->page->getAlias());
							}
						} ?>


						<li <?= Utils::MenuPrintActive($target); ?> ><a id="menu_item_<?= $menuItem->id ?>" class="first-anchor"
																		href="<?= $target ?>"><span><?= $menuItem->item ?></span></a>
						</li>

						<?php
						continue;
					} ?>
					<li ><a href="<?= $menuItem->target ?>"
							class="first-anchor"
							target="_blank"><?= $menuItem->item ?></a></li>
					<?php
	} ?>

            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>

