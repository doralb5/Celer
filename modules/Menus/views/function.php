<?php

function printActive($target, &$isActive = null)
{
	$current_Url = WebPage::currentUrl();
	$target = removeSpecialChars($target);

	if ($current_Url == $target) {
		$isActive = true;
		return 'class="active"';
	}
	$isActive = false;
	return '';
}

function removeSpecialChars($string)
{
	if (strpos($string, '#') !== false) {
		$string = substr($string, 0, strpos($string, '#'));
	}

	if (substr($string, -1) == '/') {
		$string = rtrim($string, '/');
	}
	return $string;
}
