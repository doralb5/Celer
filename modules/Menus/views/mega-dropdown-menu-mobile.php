<nav class="navbar nav_bottom_mobile" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header nav_2">
        <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

    </div> 
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
        <ul class="nav navbar-nav nav_1">
            <!--<li><a class="color" href="index.html">Home</a></li>-->

            <li class="dropdown mega-dropdown active">
                <a class="color1 dropdown-toggle" href="#" data-toggle="dropdown">abbigliamento<span class="caret"></span></a>				
                <div class="dropdown-menu fadeInDown animated">
                    <div class="menu-top">
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu1</h4>
                                <ul>
                                    <li><a href="product.html">Accessories</a></li>
                                    <li><a href="product.html">Bags</a></li>
                                    <li><a href="product.html">Caps & Hats</a></li>
                                    <li><a href="product.html">Hoodies & Sweatshirts</a></li>

                                </ul>	
                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu2</h4>
                                <ul>
                                    <li><a href="product.html">Jackets & Coats</a></li>
                                    <li><a href="product.html">Jeans</a></li>
                                    <li><a href="product.html">Jewellery</a></li>
                                    <li><a href="product.html">Jumpers & Cardigans</a></li>
                                    <li><a href="product.html">Leather Jackets</a></li>
                                    <li><a href="product.html">Long Sleeve T-Shirts</a></li>
                                </ul>	
                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu3</h4>
                                <ul>
                                    <li><a href="product.html">Shirts</a></li>
                                    <li><a href="product.html">Shoes, Boots & Trainers</a></li>
                                    <li><a href="product.html">Sunglasses</a></li>
                                    <li><a href="product.html">Sweatpants</a></li>
                                    <li><a href="product.html">Swimwear</a></li>
                                    <li><a href="product.html">Trousers & Chinos</a></li>

                                </ul>	

                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu4</h4>
                                <ul>
                                    <li><a href="product.html">T-Shirts</a></li>
                                    <li><a href="product.html">Underwear & Socks</a></li>
                                    <li><a href="product.html">Vests</a></li>
                                    <li><a href="product.html">Jackets & Coats</a></li>
                                    <li><a href="product.html">Jeans</a></li>
                                    <li><a href="product.html">Jewellery</a></li>
                                </ul>	
                            </div>							
                        </div>
                        <div class="col1 col5">
                            <img src="images/me.png" class="img-responsive" alt="">
                        </div>
                        <div class="clearfix"></div>
                    </div>                  
                </div>				
            </li>
            <li class="dropdown mega-dropdown active">
                <a class="color2 dropdown-toggle" href="#" data-toggle="dropdown">prima comunione<span class="caret"></span></a>				
                <div class="dropdown-menu mega-dropdown-menu fadeInDown animated">
                    <div class="menu-top">
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu1</h4>
                                <ul>
                                    <li><a href="product.html">Accessories</a></li>
                                    <li><a href="product.html">Bags</a></li>
                                    <li><a href="product.html">Caps & Hats</a></li>
                                    <li><a href="product.html">Hoodies & Sweatshirts</a></li>

                                </ul>	
                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu2</h4>
                                <ul>
                                    <li><a href="product.html">Jackets & Coats</a></li>
                                    <li><a href="product.html">Jeans</a></li>
                                    <li><a href="product.html">Jewellery</a></li>
                                    <li><a href="product.html">Jumpers & Cardigans</a></li>
                                    <li><a href="product.html">Leather Jackets</a></li>
                                    <li><a href="product.html">Long Sleeve T-Shirts</a></li>
                                </ul>	
                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu3</h4>

                                <ul>
                                    <li><a href="product.html">Shirts</a></li>
                                    <li><a href="product.html">Shoes, Boots & Trainers</a></li>
                                    <li><a href="product.html">Sunglasses</a></li>
                                    <li><a href="product.html">Sweatpants</a></li>
                                    <li><a href="product.html">Swimwear</a></li>
                                    <li><a href="product.html">Trousers & Chinos</a></li>

                                </ul>	

                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu4</h4>
                                <ul>
                                    <li><a href="product.html">T-Shirts</a></li>
                                    <li><a href="product.html">Underwear & Socks</a></li>
                                    <li><a href="product.html">Vests</a></li>
                                    <li><a href="product.html">Jackets & Coats</a></li>
                                    <li><a href="product.html">Jeans</a></li>
                                    <li><a href="product.html">Jewellery</a></li>
                                </ul>	
                            </div>							
                        </div>
                        <div class="col1 col5">
                            <img src="images/me1.png" class="img-responsive" alt="">
                        </div>
                        <div class="clearfix"></div>
                    </div>                  
                </div>				
            </li>
            <li><a class="color3" href="product.html">consumabili</a></li>
            <li><a class="color4" href="404.html">paramenti</a></li>
            <li><a class="color5" href="typo.html">vasi sacri</a></li>
            <li><a class="color5" href="typo.html">metalli</a></li>
            <li><a class="color5" href="typo.html">legno</a></li>
            <!--<li ><a class="color6" href="contact.html">mondo desta</a></li>-->
        </ul>
    </div><!-- /.navbar-collapse -->

</nav>