<nav class="navbar nav_bottom" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header nav_2">
        <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

    </div> 
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
        <ul class="nav navbar-nav nav_1">
            <!--<li><a class="color" href="index.html">Home</a></li>-->

            <li class="dropdown mega-dropdown active">
                <a class="color1 dropdown-toggle" href="#" data-toggle="dropdown">abbigliamento<span class="caret"></span></a>				
                <div class="dropdown-menu fadeInDown animated">
                    <div class="menu-top">
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu1</h4>
                                <ul>
                                    <li><a href="#">Accessories</a></li>
                                    <li><a href="#">Bags</a></li>
                                    <li><a href="#">Caps & Hats</a></li>
                                    <li><a href="#">Hoodies & Sweatshirts</a></li>

                                </ul>	
                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu2</h4>
                                <ul>
                                    <li><a href="#">Jackets & Coats</a></li>
                                    <li><a href="#">Jeans</a></li>
                                    <li><a href="#">Jewellery</a></li>
                                    <li><a href="#">Jumpers & Cardigans</a></li>
                                    <li><a href="#">Leather Jackets</a></li>
                                    <li><a href="#">Long Sleeve T-Shirts</a></li>
                                </ul>	
                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu3</h4>
                                <ul>
                                    <li><a href="#">Shirts</a></li>
                                    <li><a href="#">Shoes, Boots & Trainers</a></li>
                                    <li><a href="#">Sunglasses</a></li>
                                    <li><a href="#">Sweatpants</a></li>
                                    <li><a href="#">Swimwear</a></li>
                                    <li><a href="#">Trousers & Chinos</a></li>

                                </ul>	

                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu4</h4>
                                <ul>
                                    <li><a href="#">T-Shirts</a></li>
                                    <li><a href="#">Underwear & Socks</a></li>
                                    <li><a href="#">Vests</a></li>
                                    <li><a href="#">Jackets & Coats</a></li>
                                    <li><a href="#">Jeans</a></li>
                                    <li><a href="#">Jewellery</a></li>
                                </ul>	
                            </div>							
                        </div>
                        <div class="col1 col5">
                            <img src="images/me.png" class="img-responsive" alt="">
                        </div>
                        <div class="clearfix"></div>
                    </div>                  
                </div>				
            </li>
            <li class="dropdown mega-dropdown active">
                <a class="color2 dropdown-toggle" href="#" data-toggle="dropdown">prima comunione<span class="caret"></span></a>				
                <div class="dropdown-menu mega-dropdown-menu fadeInDown animated">
                    <div class="menu-top">
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu1</h4>
                                <ul>
                                    <li><a href="product">Accessories</a></li>
                                    <li><a href="product">Bags</a></li>
                                    <li><a href="product">Caps & Hats</a></li>
                                    <li><a href="product">Hoodies & Sweatshirts</a></li>

                                </ul>	
                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu2</h4>
                                <ul>
                                    <li><a href="#">Jackets & Coats</a></li>
                                    <li><a href="#">Jeans</a></li>
                                    <li><a href="#">Jewellery</a></li>
                                    <li><a href="#">Jumpers & Cardigans</a></li>
                                    <li><a href="#">Leather Jackets</a></li>
                                    <li><a href="#">Long Sleeve T-Shirts</a></li>
                                </ul>	
                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu3</h4>

                                <ul>
                                    <li><a href="#">Shirts</a></li>
                                    <li><a href="#">Shoes, Boots & Trainers</a></li>
                                    <li><a href="#">Sunglasses</a></li>
                                    <li><a href="#">Sweatpants</a></li>
                                    <li><a href="#">Swimwear</a></li>
                                    <li><a href="#">Trousers & Chinos</a></li>

                                </ul>	

                            </div>							
                        </div>
                        <div class="col1">
                            <div class="h_nav">
                                <h4>Submenu4</h4>
                                <ul>
                                    <li><a href="#">T-Shirts</a></li>
                                    <li><a href="#">Underwear & Socks</a></li>
                                    <li><a href="#">Vests</a></li>
                                    <li><a href="#">Jackets & Coats</a></li>
                                    <li><a href="#">Jeans</a></li>
                                    <li><a href="#">Jewellery</a></li>
                                </ul>	
                            </div>							
                        </div>
                        <div class="col1 col5">
                            <img src="images/me1.png" class="img-responsive" alt="">
                        </div>
                        <div class="clearfix"></div>
                    </div>                  
                </div>				
            </li>
            <li><a class="color3" href="#">consumabili</a></li>
            <li><a class="color4" href="#">paramenti</a></li>
            <li><a class="color5" href="#">vasi sacri</a></li>
            <li><a class="color5" href="#">metalli</a></li>
            <li><a class="color5" href="#">legno</a></li>
            <!--<li ><a class="color6" href="contact.html">mondo desta</a></li>-->
        </ul>
    </div><!-- /.navbar-collapse -->

</nav>