<?php
$show_brand = isset($parameters['show_brand']) ? $parameters['show_brand'] : false;
$show_logo = isset($parameters['show_logo']) ? $parameters['show_logo'] : true;
?>


<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar_<?= $block_id ?>"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

					<?php
					if ($show_logo && CMSSettings::$logo != '') {
						$logo = WEBROOT . MEDIA_ROOT . CMSSettings::$logo;
						$logo_inverse = WEBROOT . MEDIA_ROOT . ((CMSSettings::$logo_inverse != '') ? CMSSettings::$logo_inverse : CMSSettings::$logo); ?>
						<div class="logo">
							<a class="navbar-brand " href="/"><img class="img-responsive" src="<?= $logo ?>"></a>
							<a class="navbar-brand-collapsed" href="/"><?php if (CMSSettings::$logo != '') {
							?><img src="<?= $logo_inverse ?>" class="img-responsive" ><?php
						} ?></a>
						</div>
					<?php
					} ?>

					<?php if ($show_brand || false) {
						?>
						<a class="navbar-brand visible-xs-block" href="#"><?= CMSSettings::$website_title ?></a>
					<?php
					} ?>
                </div>

                <div id="navbar_<?= $block_id ?>" class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav menu-items">

						<?php printRecursiveMenuItems($MenuItems); ?>

                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>

</nav>

<?php

function printRecursiveMenuItems($items)
{
	foreach ($items as $item) {
		$target = getTargetByItem($item);
		switch ($item->type) {
			case 'Page':
				$target = Utils::genUrl($item->page->getAlias());
				break;
			case 'External Link':
				$target = $item->target;
				break;
			case 'No Link':
				$target = '#';
				break;
		}

		$active_class = (isActiveRecursively($item)) ? 'active' : '';

		if ($item->type == 'Divider') {
			echo "<li class=\"divider $active_class\"></li>\n";
		} else {
			echo "<li class=\"$active_class\">";
			if (count($item->subitems) == 0) {
				echo "<a href=\"$target\" class=\"\">{$item->item}</a>";
			} else {
				echo "<a href=\"$target\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">{$item->item} <b class=\"caret\"></b></a>\n\n";
				echo "<ul class=\"dropdown-menu\">\n";
				printRecursiveMenuItems($item->subitems);
				echo "</ul>\n\n";
			}
			echo "</li>\n";
		}
	}
}

function getTargetByItem($item)
{
	switch ($item->type) {
		case 'Page':
			$target = Utils::genUrl($item->page->getAlias());
			break;
		case 'External Link':
			$target = $item->target;
			break;
		case 'No Link':
			$target = '#';
			break;
		default:
			$target = '';
	}
	return $target;
}

function isActiveRecursively($item)
{
	$target = getTargetByItem($item);
	$active = Utils::isActivePage($target);
	if ($active) {
		return true;
	}

	if (count($item->subitems)) {
		foreach ($item->subitems as $subitem) {
			$active = isActiveRecursively($subitem);
			if ($active) {
				return true;
			}
		}
	}
	return false;
}
?>

<script>
    $(document).ready(function () {
        $('.navbar a.dropdown-toggle').on('click', function (e) {
            var $el = $(this);
            var $parent = $(this).offsetParent(".dropdown-menu");
            $(this).parent("li").toggleClass('open');

            if (!$parent.parent().hasClass('nav')) {
                $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
            }

            $('.nav li.open').not($(this).parents("li")).removeClass("open");

            return false;
        });
    });
</script>