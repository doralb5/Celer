<?php
require_once 'function.php';
$show_title = isset($params['show_title']) ? $params['show_title'] : true;
?>
<?php if (count($MenuItems)) {
	?>
	<div class="simple-menu">
		<?php if ($show_title == true) {
		?>
			<div class="menu-title"><h3><?= $menu->name ?></h3></div>
		<?php
	} ?>
		<ul class="menu">
			<?php foreach ($MenuItems as $menuItem) {
		?>
				<?php
				if ($menuItem->type == 2) {
					if ($menuItem->page->isHome()) {
						$target = Utils::getComponentUrl($menuItem->page->getAlias());
					} else {
						if (count($menuItem->page->getActionParams())) {
							$params = implode('/', $menuItem->page->getActionParams());
						} else {
							$params = '';
						}

						$target = Utils::getComponentUrl($menuItem->page->getComponent() . '/' . $menuItem->page->getAction() . (($params != '') ? '/' . $params : '') . (($menuItem->parameters != '') ? '/' . $menuItem->parameters : ''));
					} ?>
					<li <?= printActive($target) ?>><a href="<?= $target ?>"><?= $menuItem->item ?></a></li>
					<?php
					continue;
				} ?>

				<li <?= printActive($menuItem->target) ?>><a href="<?= $menuItem->target ?>" target="_blank"><?= $menuItem->item ?></a></li>

			<?php
	} ?>
		</ul>
	</div>
<?php
} ?>
