<?php

class Subscription extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-subscription';
		$this->view->render($view);
	}
}
