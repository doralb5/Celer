<?php

class AdvancedFilters extends BaseModule
{
	public $my_view = 'realestate_search';
	public $id_contract = 1;

	public function setMyView($view)
	{
		$this->my_view = $view;
	}

	public function setIdContract($id_contract)
	{
		$this->id_contract = $id_contract;
	}

	public function execute()
	{
	}

	public function AdvancedFilters()
	{
		$view = $this->my_view;

		$Real_Estate_Model = Loader::getModel('Real_Estate');
		$locations = $Real_Estate_Model->getLocations();

		$location_array = array();
		foreach ($locations as $row) {
			foreach ($row as $key => $value) {
				array_push($location_array, $value);
			}
		}

		$id_contract = $this->id_contract;
		$min_max_prices = $Real_Estate_Model->getMinMaxPrice($id_contract);
		$min_max_surface = $Real_Estate_Model->getMinMaxSurface($id_contract);

		$contracts = $Real_Estate_Model->getContracts(-1);
		$currencies = $Real_Estate_Model->getCurrency(-1);
		$typologies = $Real_Estate_Model->getTypologies(-1);
		$floors = $Real_Estate_Model->getFloors(-1);
		$heating = $Real_Estate_Model->getHeating(-1);
		$building_states = $Real_Estate_Model->getbuildings(-1);

		$this->view->set('id_contract', $id_contract);
		$this->view->set('min_max_prices', $min_max_prices);
		$this->view->set('min_max_surface', $min_max_surface);

		$this->view->set('locations', $location_array);
		$this->view->set('contracts', $contracts);
		$this->view->set('currencies', $currencies);
		$this->view->set('typologies', $typologies);
		$this->view->set('floors', $floors);
		$this->view->set('heating', $heating);
		$this->view->set('building_states', $building_states);
		$this->view->render($view);
	}
}
