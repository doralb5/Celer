<?php

class LastRealEstate extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 16;
		$featured = (isset($parameters['featured']) && $parameters['featured'] != '0') ? " AND featured = '1' " : '  AND 1 ';
		$Real_Estate_Table = TABLE_PREFIX . 'RealEstate';
		$filter = (isset($parameters['filter'])) ? $parameters['filter'] : " 1 AND $Real_Estate_Table.enabled = '1'  AND $Real_Estate_Table.publish_date < NOW()  $featured ";
		$order = (isset($parameters['order'])) ? $parameters['order'] : ' publish_date desc ';
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'last-real-estate';
		$Real_Estate_Model = Loader::getModel('Real_Estate');

		$real_estates = $Real_Estate_Model->getList($limit, 0, $filter, $order);
		$category = null;
		$this->view->set('real_estates', $real_estates);
		$this->view->set('parameters', $parameters);
		$this->view->render($view);
	}
}

//  public $id;
//    public $title;
//    public $address;
//    public $id_contract;
//    public $id_typology;
//    public $price;
//    public $id_currency;
//    public $description;
//    public $date;
//    public $image;
//    public $id_user;
//    public $coordinates;
//    public $email;
//    public $enabled = 1;
//    public $featured = 0;
//    public $notified = 0;
//    public $location;
//    public $square_meter;
//    public $rooms;
//    public $bathrooms;
//    public $garden;
//    public $furnished;
//    public $balcony;
//    public $tarrace;
//    public $id_heating;
//    public $id_floor;
//    public $id_building_state;
//
//    public $images = array();
//
//    public $contract_type;
//    public $typology;
//    public $currency;
//    public $floor;
//    public $heating;
//    public $building;
//
