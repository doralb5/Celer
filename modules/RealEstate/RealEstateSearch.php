<?php

class RealEstateSearch extends BaseModule
{
	public $my_view = 'realestate_search';
	public $id_contract = 1;

	public function setView($view)
	{
		$this->my_view = $view;
	}

	public function setIdContract($id_contract)
	{
		$this->id_contract = $id_contract;
	}

	public function advancedFilters()
	{
		$view = $this->my_view;

		$Real_Estate_Model = Loader::getModel('Real_Estate');

		$id_contract = $_GET['contract'];
		$locations = $this->locations($Real_Estate_Model->getLocations());
		$min_max_prices = $Real_Estate_Model->getMinMaxPrice($id_contract);
		$min_max_surface = $Real_Estate_Model->getMinMaxSurface($id_contract);

		$contracts = $Real_Estate_Model->getContracts(-1);
		$currencies = $Real_Estate_Model->getCurrency(-1);
		$typologies = $Real_Estate_Model->getTypologies(-1);
		$floor = $Real_Estate_Model->getFloors(-1);
		$heating = $Real_Estate_Model->getHeating(-1);
		$building_states = $Real_Estate_Model->getbuildings(-1);

		$this->view->set('locations', $locations);
		$this->view->set('id_contract', $id_contract);
		$this->view->set('min_max_prices', $min_max_prices);
		$this->view->set('min_max_surface', $min_max_surface);

		$this->view->set('contracts', $contracts);
		$this->view->set('currencies', $currencies);
		$this->view->set('typologies', $typologies);
		$this->view->set('floor', $floor);
		$this->view->set('heating', $heating);
		$this->view->set('building_states', $building_states);
		$this->view->render($view);
	}

	public function execute($parameters = array())
	{
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'realestate_search';

		$id_contract_sell = (isset($parameters['id_contract_sell'])) ? $parameters['id_contract_sell'] : '';
		$id_contract_rent = (isset($parameters['id_contract_rent'])) ? $parameters['id_contract_rent'] : '';
		$id_contract_rentBuy = (isset($parameters['id_contract_rentBuy'])) ? $parameters['id_contract_rentBuy'] : '';

		$Real_Estate_Model = Loader::getModel('Real_Estate');

		$locations = $this->locations($Real_Estate_Model->getLocations());
		$locations_sell = $this->locations($Real_Estate_Model->getLocations($id_contract_sell));
		$locations_rent = $this->locations($Real_Estate_Model->getLocations($id_contract_rent));
		$locations_rentBuy = $this->locations($Real_Estate_Model->getLocations($id_contract_rentBuy));

		$min_max_prices_sell = $Real_Estate_Model->getMinMaxPrice($id_contract_sell);
		$min_max_prices_rent = $Real_Estate_Model->getMinMaxPrice($id_contract_rent);
		$min_max_prices_rentBuy = $Real_Estate_Model->getMinMaxPrice($id_contract_rentBuy);

		$min_max_surface_sell = $Real_Estate_Model->getMinMaxSurface($id_contract_sell);
		$min_max_surface_rent = $Real_Estate_Model->getMinMaxSurface($id_contract_rent);
		$min_max_surface_rentBuy = $Real_Estate_Model->getMinMaxSurface($id_contract_rentBuy);

		$contracts = $Real_Estate_Model->getContracts(-1);
		$currencies = $Real_Estate_Model->getCurrency(-1);
		$typologies = $Real_Estate_Model->getTypologies(-1);
		$floor = $Real_Estate_Model->getFloors(-1);
		$heating = $Real_Estate_Model->getHeating(-1);
		$building_states = $Real_Estate_Model->getbuildings(-1);

		$this->view->set('min_max_prices_sell', $min_max_prices_sell);
		$this->view->set('min_max_prices_rent', $min_max_prices_rent);
		$this->view->set('min_max_prices_rentBuy', $min_max_prices_rentBuy);

		$this->view->set('id_contract_sell', $id_contract_sell);
		$this->view->set('id_contract_rent', $id_contract_rent);
		$this->view->set('id_contract_rentBuy', $id_contract_rentBuy);

		$this->view->set('min_max_surface_sell', $min_max_surface_sell);
		$this->view->set('min_max_surface_rent', $min_max_surface_rent);
		$this->view->set('min_max_surface_rentBuy', $min_max_surface_rentBuy);

		$this->view->set('locations_sell', $locations_sell);
		$this->view->set('locations_rent', $locations_rent);
		$this->view->set('locations_rentBuy', $locations_rentBuy);

		$this->view->set('locations', $locations);
		$this->view->set('contracts', $contracts);
		$this->view->set('currencies', $currencies);
		$this->view->set('typologies', $typologies);
		$this->view->set('floor', $floor);
		$this->view->set('heating', $heating);
		$this->view->set('building_states', $building_states);
		$this->view->set('parameters', $parameters);
		$this->view->render($view);
	}

	private function locations($locations)
	{
		$location_array = array();
		foreach ($locations as $row) {
			foreach ($row as $key => $value) {
				array_push($location_array, $value);
			}
		}
		return $location_array;
	}
}
