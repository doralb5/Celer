<script src="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.min.js"></script>
<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.indigo-pink.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">	 

<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/advanced-filters.css'); ?>
<?php
$min_price = ($min_max_prices[0]['min_price'] != null) ? $min_max_prices[0]['min_price'] : 1;
$max_price = ($min_max_prices[0]['max_price'] != null) ? $min_max_prices[0]['max_price'] : 100000;

$min_surface = ($min_max_surface[0]['min_surface'] != null) ? $min_max_surface[0]['min_surface'] : 1;
$max_surface = ($min_max_surface[0]['max_surface'] != null) ? $min_max_surface[0]['max_surface'] : 100;

$price = (isset($_GET['price'])) ? $_GET['price'] : $max_price;
$surface = (isset($_GET['square_meter'])) ? $_GET['square_meter'] : $min_surface;
$city = (isset($_GET['city'])) ? $_GET['city'] : '';
$id_typology = (isset($_GET['typology'])) ? $_GET['typology'] : '';
$terrace = (isset($_GET['terrace'])) ? 'checked' : '';
$garden = (isset($_GET['garden'])) ? 'checked' : '';
$furnished = (isset($_GET['furnished'])) ? 'checked' : '';
$balcony = (isset($_GET['balcony'])) ? 'checked' : '';

$energetic_class = (isset($_GET['energetic_class'])) ? $_GET['energetic_class'] : '';
$id_building_state = (isset($_GET['id_building'])) ? $_GET['id_building'] : '';
$id_floor = (isset($_GET['id_floor'])) ? $_GET['id_floor'] : '';
$id_heating = (isset($_GET['id_heating'])) ? $_GET['id_heating'] : '';
$rooms = (isset($_GET['rooms'])) ? $_GET['rooms'] : '';
$bathrooms = (isset($_GET['bathrooms'])) ? $_GET['bathrooms'] : '';
?>


<div id="portal-search">
    <div>

        <!--Tab 1-->
        <div class="tab-pane active" id="panel1">
            <div class="">

                <form  method="GET" action="<?= Utils::getComponentUrl('Real_Estate/ListRealEstate') ?>" class="form-horizontal" role="form">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">[$city]</label>
                            <div class="">
                                <select  onchange="getLocalities()" type="text" id="city" name="city" class="form-control mouse-focused" placeholder="[$type_a_city]"> 
                                    <option value = "">[$select_a_city]</option>
									<?php foreach ($locations as $location) {
	?>
										<option value = "<?= $location ?>" <?php if ($location == $city) {
		echo ' selected ';
	} ?>><?= $location ?></option>
									<?php
} ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <label class="control-label">[$locality]</label>
                            <div class="">
                                <select  type="text" id="localities" name="locality" class="form-control mouse-focused" placeholder="[$type_a_locality]"> 
                                    <option value = "">[$select_a_city]</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">[$contract]</label>
                            <div class="">
                                <select onchange="getTypologies(this.value)"  type="text" id="contact" name="contract" class="form-control mouse-focused" placeholder="[$type_a_city]"> 
									<?php foreach ($contracts as $contract) {
		?>
										<option value = "<?= $contract->id ?>" <?php if ($contract->id == $id_contract) {
			echo ' selected ';
		} ?>><?= $contract->type ?></option>
									<?php
	} ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">[$typology]</label>
                            <div class="">
                                <select  type="text" id="typologies" name="typology" class="form-control mouse-focused" placeholder="[$type_a_city]"> 
                                    <option value="">[$select_a_contract] </option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div id="advanced-form" style="display:none !important;">

                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">[$energetic_class]</label>
                                <div class="">
                                    <select class="form-control mouse-focused" id="energetic_class" name="energetic_class" >
                                        <option value=""  <?php if ($energetic_class == '') {
		echo ' selected ';
	} ?>  >[$All]</option>
                                        <option value="Y" <?php if ($energetic_class == 'Y') {
		echo ' selected ';
	} ?>  >[$requesting]</option>
                                        <option value="X" <?php if ($energetic_class == 'X') {
		echo ' selected ';
	} ?>  >[$missing]</option>
                                        <option value="A" <?php if ($energetic_class == 'A') {
		echo ' selected ';
	} ?>  >[$high] (A, A+, A1-A4) </option>
                                        <option value="BCD" <?php if ($energetic_class == 'BCD') {
		echo ' selected ';
	} ?>  >[$middle] (B, C, D) </option>
                                        <option value="EFG" <?php if ($energetic_class == 'EFG') {
		echo ' selected ';
	} ?>  >[$low] (E, F, G) </option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label class="control-label">[$rooms]</label>
                                <div class="">
                                    <select class="form-control" name="rooms">
                                        <option value="">[$all_quantities]</option>
                                        <option value="1" <?php if ($rooms == '1') {
		echo ' selected ';
	} ?>  >1</option>
                                        <option value="2" <?php if ($rooms == '2') {
		echo ' selected ';
	} ?>>2</option>
                                        <option value="3" <?php if ($rooms == '3') {
		echo ' selected ';
	} ?> >3</option>
                                        <option value="4" <?php if ($rooms == '4') {
		echo ' selected ';
	} ?> >4</option>
                                        <option value="5" <?php if ($rooms == '5') {
		echo ' selected ';
	} ?> >5</option>
                                        <option value="6" <?php if ($rooms == '6') {
		echo ' selected ';
	} ?> >6</option>
                                        <option value="6+" <?php if ($rooms == '6+') {
		echo ' selected ';
	} ?> >&gt; 6</option>

                                    </select>
                                </div>
                            </div>



                            <div class="col-md-6">
                                <label class="control-label">[$floor]</label>
                                <select class="form-control" id="id_category" name="id_floor" >
                                    <option value="">[$All]</option>
									<?php foreach ($floor as $tp) {
		?>
										<option
											value="<?= $tp->id ?>" <?= ($tp->id == $id_floor) ? 'selected' : '' ?>><?= $tp->name ?></option>
										<?php
	} ?>
                                </select>
                            </div>


                            <div class="col-md-6">
                                <label class="control-label">[$building]</label>
                                <select class="form-control" id="id_category" name="id_building" >
                                    <option value="">[$All]</option>
									<?php foreach ($building_states as $tp) {
		?>
										<option
											value="<?= $tp->id ?>" <?= ($tp->id == $id_building_state) ? 'selected' : '' ?>><?= $tp->type ?></option>
										<?php
	} ?>
                                </select>
                            </div>



                            <div class="col-md-6">
                                <label class="control-label">[$heating]</label>
                                <select class="form-control" id="id_category" name="id_heating" >
                                    <option value="">[$All]</option>
									<?php foreach ($heating as $tp) {
		?>
										<option
											value="<?= $tp->id ?>" <?= ($tp->id == $id_heating) ? 'selected' : '' ?>><?= $tp->name ?></option>
										<?php
	} ?>
                                </select>
                            </div>



                            <div class="col-md-6">
                                <label class="control-label">[$bathrooms]</label>
                                <div class="">
                                    <select class="form-control"   name="bathrooms">
                                        <option value="">[$all_quantities]</option>
                                        <option value="1" <?php if ($bathrooms == '1') {
		echo ' selected ';
	} ?> >1</option>
                                        <option value="2" <?php if ($bathrooms == '2') {
		echo ' selected ';
	} ?> >2</option>
                                        <option value="3" <?php if ($bathrooms == '3') {
		echo ' selected ';
	} ?> >3</option>
                                        <option value="3+" <?php if ($bathrooms == '3+') {
		echo ' selected ';
	} ?> >&gt; 3</option>

                                    </select>
                                </div>
                            </div>




                        </div>

                        <br>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$max_price]</label>
                            <div class="col-md-9">
                                <div class="range-slider price">
                                    <input name='price' class="range-slider__range" type="range" min="<?= $min_price ?>" max="<?= $max_price ?>" step="100" value="<?= $price ?>">
                                    <span class="range-slider__value">0</span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="col-md-3 control-label">[$min_surface]</label>
                            <div class="col-md-9">
                                <div class="range-slider square-meter">
                                    <input name='square_meter' class="range-slider__range" type="range" min="<?= $min_surface ?>" max="<?= $max_surface ?>" step="1" value="<?= $surface ?>">
                                    <span class="range-slider__value">0</span>
                                </div>
                            </div>
                        </div>
                        <br>



                        <table class="table">
                            <tr>
                            <td>
                            <label class="mdl-checkbox mdl-js-checkbox" for="terrace">
                                <input name='terrace' type="checkbox" id="terrace" class="mdl-checkbox__input" <?= $terrace ?> >
                                <span class="mdl-checkbox__label">[$terrace]</span>
                            </label>
                            </td>


                            <td>
                            <label class="mdl-checkbox mdl-js-checkbox" for="garden">
                                <input name="garden" type="checkbox" id="garden" class="mdl-checkbox__input"  <?= $garden ?> >
                                <span class="mdl-checkbox__label">[$garden]</span>
                            </label>
                            </td>


                            <td>
                            <label class="mdl-checkbox mdl-js-checkbox" for="furnished">
                                <input  name="furnished" type="checkbox" id="furnished" class="mdl-checkbox__input"  <?= $furnished ?> >
                                <span class="mdl-checkbox__label">[$furnished]</span>
                            </label>
                            </td>

                            <td>
                            <label class="mdl-checkbox mdl-js-checkbox" for="balcony">
                                <input  name="balcony" type="checkbox" id="balcony" class="mdl-checkbox__input"  <?= $balcony ?> >
                                <span class="mdl-checkbox__label">[$balcony]</span>
                            </label>
                            </td>
                            </tr>
                        </table>
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <button type="submit" class="btn btn-success btn-sm btn-block">[$search]</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>



<?php if (isset($_GET['typology'])) {
		?>

	    function LoadTypologies() {
	        var id = $('#contact').val();
	        $.ajax({
	            type: 'POST',
	            url: '<?= Utils::getComponentUrl('Real_Estate/ajx_getTypologies') ?>',
	            data: {
	                'id_contract': id,
	                'get_typology': '<?= $_GET['typology'] ?>'
	            },
	            success: function (typologies) {
	                console.log(typologies);
	                $('#typologies').empty();
	                $('#typologies').append(typologies);
	            },
	            error: function (errors) {
	                console.log(errors);
	            }

	        });


	    }

	    LoadTypologies();

<?php
	} ?>



<?php if (isset($_GET['locality'])) {
		?>
	    function LoadLocalities() {
	        var id = $('#contact').val();
	        var city = $('#city').val();
	        $.ajax({
	            type: 'POST',
	            url: '<?= Utils::getComponentUrl('Real_Estate/ajx_getLocalities') ?>',
	            data: {
	                'city': city,
	                'get_locality': '<?= $_GET['locality'] ?>',
	                'id': id
	            },
	            success: function (localities) {
	                //console.log(localities);
	                $('#localities').empty();
	                $('#localities').append(localities);
	            },
	            error: function (errors) {
	                console.log(errors);
	            }
	        });
	    }
	    LoadLocalities();
<?php
	} ?>
    function getLocalities() {
        var id = $('#contact').val();
        var city = $('#city').val();
        $.ajax({
            type: 'POST',
            url: '<?= Utils::getComponentUrl('Real_Estate/ajx_getLocalities') ?>',
            data: {
                'city': city,
                'id': id
            },
            success: function (localities) {
                //console.log(localities);
                $('#localities').empty();
                $('#localities').append(localities);
            },
            error: function (errors) {
                console.log(errors);
            }

        });

    }


    function getTypologies(contract) {
        var id = $('#contact').val();
        $.ajax({
            type: 'POST',
            url: '<?= Utils::getComponentUrl('Real_Estate/ajx_getTypologies') ?>',
            data: {
                'id_contract': id
            },
            success: function (localities) {
                //console.log(localities);
                $('#typologies').empty();
                $('#typologies').append(localities);
            },
            error: function (errors) {
                console.log(errors);
            }

        });


    }

    $(document).ready(function () {
        $('.checkbox-ripple').rkmd_checkboxRipple();
        change_checkbox_color();
    });

    (function ($) {

        $.fn.rkmd_checkboxRipple = function () {
            var self, checkbox, ripple, size, rippleX, rippleY, eWidth, eHeight;
            self = this;
            checkbox = self.find('.input-checkbox');

            checkbox.on('mousedown', function (e) {
                if (e.button === 2) {
                    return false;
                }

                if ($(this).find('.ripple').length === 0) {
                    $(this).append('<span class="ripple"></span>');
                }
                ripple = $(this).find('.ripple');

                eWidth = $(this).outerWidth();
                eHeight = $(this).outerHeight();
                size = Math.max(eWidth, eHeight);
                ripple.css({'width': size, 'height': size});
                ripple.addClass('animated');

                $(this).on('mouseup', function () {
                    setTimeout(function () {
                        ripple.removeClass('animated');
                    }, 200);
                });

            });
        }

    }(jQuery));

    function change_checkbox_color() {
        $('.color-box .show-box').on('click', function () {
            $(".color-box").toggleClass("open");
        });

        $('.colors-list a').on('click', function () {
            var curr_color = $('main').data('checkbox-color');
            var color = $(this).data('checkbox-color');
            var new_colot = 'checkbox-' + color;

            $('.rkmd-checkbox .input-checkbox').each(function (i, v) {
                var findColor = $(this).hasClass(curr_color);

                if (findColor) {
                    $(this).removeClass(curr_color);
                    $(this).addClass(new_colot);
                }

                $('main').data('checkbox-color', new_colot);

            });
        });
    }









// AutoComplete Form
    var availableTags = <?php echo json_encode($locations); ?>;

    $("#city").autocomplete({
        source: availableTags,
        minLength: 1,
        select: function (event, ui) {
            event.preventDefault();
            $("#city").val(ui.item.label);
        }
    });

// Range Slide
    var rangeSlider = function () {
        var slider = $('.range-slider'),
                range = $('.range-slider__range'),
                value = $('.range-slider__value');

        slider.each(function () {

            value.each(function () {
                var value = $(this).prev().attr('value');
                $(this).html(numberFormat(value, ',', '.'));
            });

            range.on('input', function () {
                $(this).next(value).html(numberFormat(this.value, ',', '.'));

            });
        });
    };

    rangeSlider();

    function numberFormat(_number, _sep, thousands_sep) {

        var parts = _number.toString().split(_sep);
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_sep);
        return parts.join(_sep);


        _number = typeof _number != "undefined" && _number > 0 ? _number : "";
        _number = _number.replace(new RegExp("^(\\d{" + (_number.length % 3 ? _number.length % 3 : 0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
        if (typeof _sep != "undefined" && _sep != " ") {
            _number = _number.replace(/\s/g, _sep);
        }
        return _number;
    }



</script>
