<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/last-businesses-owl.css');
HeadHtml::linkStylesheet('owl.carousel.css');
HeadHtml::linkJS('owl.carousel.js');
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 15;
$w = (isset($parameters['w'])) ? $parameters['w'] : 200;
$h = (isset($parameters['h'])) ? $parameters['h'] : 200;
?>

<div class="business-carousel">
    <div id="last-business-owl" class="owl-carousel text-center">
		<?php foreach ($businesses as $business) {
	?>

			<div class="skill-card">
				<header class="skill-card__header">
					<?php if ($business->image != '') {
		?>
						<a class="hover-img" href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business->company_name . '-' . $business->id)) ?>" title="">    
							<img class="skill-card__icon" src="<?= Utils::genThumbnailUrl("businesses/{$business->id_category}/" . $business->image, $w, $h, array('far' => '1', 'bg' => 'FFFFFF')) ?>"/>
						</a>
					<?php
	} ?>
				</header>
				<section class="skill-card__body">
					<h2 class="skill-card__title">
						<a class="hover-img" href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business->company_name . '-' . $business->id)) ?>" title="">

							<?= StringUtils::CutString(strip_tags($business->company_name), $taglia_title) ?>
						</a>
					</h2>


					<!--                        <ul class="skill-card__knowledge">
												<li>+355674881781</li>
												<li>email</li>
												<li>facebook</li>
												<li>website</li>
											</ul>-->
				</section>
			</div>

		<?php
} ?>


    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="text-center align-btn">
            <a href="/lista-e-kategorive" class="btn btn-sm btn-default">Shfaq te gjitha kategorite</a>
        </div>
    </div>
</div>

<!-- Owl-Carousel-JavaScript -->
<script>
    $(document).ready(function () {
        $("#last-business-owl").owlCarousel({
            items: 5,
            lazyLoad: true,
            autoPlay: true,
            pagination: false,
            loop: true,
        });
    });
</script>
