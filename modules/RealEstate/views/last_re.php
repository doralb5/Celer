<?php

function printColor($energetic_class)
{
	$string = null;
	switch ($energetic_class) {
		case 'A':
			$string = 'style="background: #7fb800; color:white; font-weight: bold;"';
			break;
		case 'B':
			$string = 'style="background: #91d100; color:white; font-weight: bold;"';
			break;
		case 'C':
			$string = 'style="background: #ebc400; color:white; font-weight: bold;"';
			break;
		case 'D':
			$string = 'style="background: #eb9d00; color:white; font-weight: bold;"';
			break;
		case 'E':
			$string = 'style="background: #e67300; color:white; font-weight: bold;"';
			break;
		case 'F':
			$string = 'style="background: #d22300; color:white; font-weight: bold;"';
			break;
		case 'G':
			$string = 'style="background: #b80000; color:white; font-weight: bold;"';
	}
	return $string;
}
?>

<?php
require_once LIBS_PATH . 'StringUtils.php';
HeadHTML::AddJS('noframework.waypoints.min.js');
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/last-re.css');
?>

<?php
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 4;
$col_size = 12 / $cols;
$w = (isset($parameters['w'])) ? $parameters['w'] : 650;
$h = (isset($parameters['h'])) ? $parameters['h'] : 350;
$taglia_address = (isset($parameters['taglia_address'])) ? $parameters['taglia_address'] : 100;
?>

<div class="lastcompany">
    <div class="">
        <div class="carousel slide" id="companyCarousel">
            <div class="carousel-inner">

				<?php
				$i = 0;
				foreach ($real_estates as $re) {
					?>

					<?php if ($i == 0) {
						?>
						<div class="item active">
							<ul class="thumbnails">
								<div class="row">
								<?php
					} ?>
								<?php if ($i % $cols == 0 && $i != 0) {
						?>
								</div>
							</ul>
						</div>
						<div class="item">
							<ul class="thumbnails">
								<div class="row">
								<?php
					} ?>

								<li class="col-md-<?= $col_size ?> news-business" id="businessDiv_<?= $re->id ?>">
									<div class="back-div">
										<?php if ($re->featured == '1') {
						?>
											<div class="ribbon-wrapper-green">
												<div class="ribbon-green">[$consigliato]</div>
											</div>
										<?php
					} ?>
										<div class="post-thumbnail-3">
											<?php if ($re->image != '') {
						?>
												<figure>
													<a class="hover-img"
													   href="<?= Utils::getComponentUrl('Real_Estate/RealEstateShow/' . Utils::url_slug($re->title . '-' . $re->id)) ?>"
													   title="<?= $re->title ?>">
														<img
															src="<?= Utils::genThumbnailUrl('realestates/' . $re->image, $w, $h, array('zc' => '1')) ?>"
															class="img-responsive wp-post-image" alt="<?= $re->title ?>">
													</a>
												</figure>
											<?php
					} else {
						?>
												<figure>
													<a class="hover-img" 
													   href="<?= Utils::getComponentUrl('Real_Estate/RealEstateShow/' . Utils::url_slug($re->title . '-' . $re->id)) ?>"
													   title="<?= $re->title ?>">
														<img
															src="<?= WEBROOT . $this->view_path ?>images/default.jpg"
															class="img-responsive wp-post-image" alt="<?= $re->title ?>">

													</a>
												</figure>
											<?php
					} ?>
											<?php if ($re->price != 0) {
						?>
												<?php if ($re->hide_price == 0) {
							?>
													<span class="price">&euro; <?= number_format($re->price, 0, ',', '.') ?></span>
												<?php
						} else {
							?>
													<span class="price"><?= '[$price_hidden]' ?> </span>
													<?php
						}
					} ?>

										</div>

										<span class="main-comune"> 
											<a><?= $re->typology ?> [$in] <?= $re->contract_type ?></a>
										</span>

										<div class="alldiv-text">

											<p class="main-comune">
											<h5 class="main-title1">
												<a href="<?= Utils::getComponentUrl('Real_Estate/RealEstateShow/' . Utils::url_slug($re->title . '-' . $re->id)) ?>"
												   title="<?= $re->title ?>">
													   <?= $re->title ?>
												</a>
											</h5>
											</p>
											<p class="main-addres">
												<i class="fa fa-map-marker" aria-hidden="true"></i>
												<?= StringUtils::CutString(strip_tags($re->location), $taglia_address) ?>
											</p>

											<div class="location-height">
												<div class="row">

													<div class="col-md-8 col-sm-8 col-xs-8">
														<?php if ($re->energetic_class == 'A' || $re->energetic_class == 'B' || $re->energetic_class == 'C' || $re->energetic_class == 'D' || $re->energetic_class == 'E' || $re->energetic_class == 'F' || $re->energetic_class == 'G') {
						?>

															<span class="energetic_class" <?= printColor($re->energetic_class) ?>>
																<?= $re->energetic_class ?>
															</span>

														<?php
					} ?>
														<?php if ($re->energetic_class == 'A' || $re->energetic_class == 'B' || $re->energetic_class == 'C' || $re->energetic_class == 'D' || $re->energetic_class == 'E' || $re->energetic_class == 'F' || $re->energetic_class == 'G') {
						?>
															<div class="specifics-icons">
															<?php
					} else {
						?>
																<div class="">
																<?php
					} ?>
																<?php if ($re->rooms != 0 and $re->rooms != '' and $re->rooms != null) {
						?>
																	<span class="specifics rooms">
																		<i class="fa fa-bed" aria-hidden="true"></i><?= $re->rooms ?>
																	</span>
																<?php
					} ?>

																<?php if ($re->bathrooms != 0 and $re->bathrooms != '' and $re->bathrooms != null) {
						?>
																	<span class="specifics bathrooms">
																		<i class="fa fa-bath" aria-hidden="true"></i><?= $re->bathrooms ?>
																	</span>
																<?php
					} ?>

															</div>
														</div>

														<div class="col-md-4 col-sm-4 col-xs-4">
															<?php if ($re->square_meter != 0) {
						?>
																<span class="specifics square-meter"><?= $re->square_meter ?>m²</span>
															<?php
					} ?>
														</div>


													</div>
												</div>
											</div>
										</div>
								</li>

								<?php
								$i++;
				}
							?>
                        </div>
                    </ul>
                </div>
            </div>

			<?php if (count($real_estates) > 4) {
								?>
				<nav>
					<ul class="newscontrol-box ">
						<li>
							<a data-slide="prev" href="#companyCarousel" class="left carousel-control">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</a>
						</li>
						<li>
							<a data-slide="next" href="#companyCarousel" class="right carousel-control">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
						</li>
					</ul>
				</nav>
			<?php
							} ?>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $('#companyCarousel').carousel({
            interval: 6000
        })
    });
</script>
