<?php HeadHtml::linkJS(Utils::googleMapsJsLink()); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/search-tab.css'); ?>

<?php
$min_price_sell = ($min_max_prices_sell[0]['min_price'] != null) ? $min_max_prices_sell[0]['min_price'] : 1000;
$max_price_sell = ($min_max_prices_sell[0]['max_price'] != null) ? $min_max_prices_sell[0]['max_price'] : 100000;

$min_price_rent = ($min_max_prices_rent[0]['min_price'] != null) ? $min_max_prices_rent[0]['min_price'] : 1;
$max_price_rent = ($min_max_prices_rent[0]['max_price'] != null) ? $min_max_prices_rent[0]['max_price'] : 1000;

$min_price_byuRent = ($min_max_prices_rentBuy[0]['min_price'] != null) ? $min_max_prices_rentBuy[0]['min_price'] : 1000;
$max_price_byuRent = ($min_max_prices_rentBuy[0]['max_price'] != null) ? $min_max_prices_rentBuy[0]['max_price'] : 100000;

$min_surface_sell = ($min_max_surface_sell[0]['min_surface'] != null) ? $min_max_surface_sell[0]['min_surface'] : 1;
$max_surface_sell = ($min_max_surface_sell[0]['max_surface'] != null) ? $min_max_surface_sell[0]['max_surface'] : 100;

$min_surface_rent = ($min_max_surface_rent[0]['min_surface'] != null) ? $min_max_surface_rent[0]['min_surface'] : 1;
$max_surface_rent = ($min_max_surface_rent[0]['max_surface'] != null) ? $min_max_surface_rent[0]['max_surface'] : 100;

$min_surface_byuRent = ($min_max_surface_rentBuy[0]['min_surface'] != null) ? $min_max_surface_rentBuy[0]['min_surface'] : 1;
$max_surface_byuRent = ($min_max_surface_rentBuy[0]['max_surface'] != null) ? $min_max_surface_rentBuy[0]['max_surface'] : 100;
?>


<div id="portal-search">
    <div>
        <div class="tabbable custom-tabs tabs-animated  flat flat-all hide-label-980 shadow track-url auto-scroll">
            <ul class="nav nav-tabs">
                <li class="active"><a class="active" data-toggle="tab" href="#panel1"><span>[$buy]</span></a></li>
                <li><a data-toggle="tab" href="#panel2"><span>[$rent]</span></a></li>
                <li><a data-toggle="tab" href="#panel3"><span>[$rent_to_buy]</span></a></li>
<!--                <li><a data-toggle="tab" href="#panel4"><span>[$sell]</span></a></li>-->
            </ul>

            <div class="tab-content ">
                <!--Tab 1-->
                <div class="tab-pane active" id="panel1">
                    <div class="well-searchbox">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="search-heading">[$search_heading_sell]</h1>
                            </div>
                        </div>

                        <form  method="GET" action="<?= Utils::getComponentUrl('Real_Estate/ListRealEstate') ?>" class="form-horizontal" role="form">

                            <input type="hidden" name='contract' value="<?= $id_contract_sell ?>">


                            <div class="form-group">
                                <label class="col-md-4 control-label">[$city]</label>
                                <div class="col-md-8">
                                    <select onchange="getLocalities(this.value)" type="text" id="city" name="city" class="form-control mouse-focused" placeholder="[$type_a_city]"> 
                                        <option value = "">[$select_a_city]</option>
										<?php foreach ($locations_sell as $location) {
	?>
											<option value = "<?= $location ?>"><?= $location ?></option>
										<?php
} ?>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">[$locality]</label>
                                <div class="col-md-8">
                                    <select  type="text" id="locality" name="locality" class="form-control mouse-focused" placeholder="[$type_a_locality]"> 
                                        <option value = "">[$select_a_city]</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">[$typology]</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="typology" placeholder="[$typology]">
                                        <option value="">[$any]</option>
										<?php foreach ($typologies as $typology) {
		?>
											<option value="<?= $typology->id ?>"><?= $typology->type ?></option>                    
										<?php
	} ?>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">[$max_price]</label>
                                <div class="col-md-8">
                                    <div class="range-slider price">
                                        <input name='price' class="range-slider__range" type="range" min="<?= $min_price_sell ?>" max="<?= $max_price_sell ?>" step="1000" value="<?= $max_price_sell; ?>">
                                        <span class="range-slider__value">0</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">[$SuperficieMinima]</label>
                                <div class="col-md-8">
                                    <div class="range-slider square-meter">
                                        <input name='square_meter' class="range-slider__range" type="range" min="<?= $min_surface_sell ?>" max="<?= $max_surface_sell ?>" step="5" value="<?= $min_surface_sell ?>">
                                        <span class="range-slider__value">0</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-offset-4 col-md-8 col-sm-5">
                                    <button type="submit" class="btn btn-success btn-sm btn-block">[$search]</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!--Tab 2-->
                <div class="tab-pane" id="panel2">
                    <div class="well-searchbox">
                        <h1 class="search-heading">[$search_heading_rent]</h1>
                        <form  method="GET" action="<?= Utils::getComponentUrl('Real_Estate/ListRealEstate') ?>" class="form-horizontal" role="form">


                            <div class="form-group">
                                <label class="col-md-4 control-label">[$city]</label>
                                <div class="col-md-8">
                                    <select onchange="getLocalitiesRent(this.value)" type="text" id="city" name="city" class="form-control mouse-focused" placeholder="[$type_a_city]"> 
                                        <option value = "">[$select_a_city]</option>
										<?php foreach ($locations_rent as $location) {
		?>
											<option value = "<?= $location ?>"><?= $location ?></option>
										<?php
	} ?>

                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-md-4 control-label">[$locality]</label>
                                <div class="col-md-8">
                                    <select  type="text" id="locality_rent" name="locality" class="form-control mouse-focused" placeholder="[$type_a_locality]"> 

                                        <option value = "">[$select_a_city]</option>


                                    </select>
                                </div>
                            </div>










                            <input type="hidden" name='contract' value="<?= $id_contract_rent ?>">
                            <div class="form-group">
                                <label class="col-md-4 control-label">[$typology]</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="typology" placeholder="[$typology]">
                                        <option value="">[$any]</option>
										<?php foreach ($typologies as $typology) {
		?>
											<option value="<?= $typology->id ?>"><?= $typology->type ?></option>                    
										<?php
	} ?>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">[$max_price]</label>
                                <div class="col-md-8 test">
                                    <div class="range-slider  price">
                                        <input name='price' class="range-slider__range " type="range" min="<?= $min_price_rent ?>" max="<?= $max_price_rent ?>" step="1" value="<?= $max_price_rent ?>">
                                        <span class="range-slider__value">0</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Superficie Minima</label>
                                <div class="col-md-8">
                                    <div class="range-slider square-meter">
                                        <input name='square_meter' class="range-slider__range" type="range" min="<?= $min_surface_rent ?>" max="<?= $max_surface_rent ?>" step="1" value="<?= $min_surface_rent ?>">
                                        <span class="range-slider__value">0</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-offset-4 col-md-8 col-sm-5">
                                    <button type="submit" class="btn btn-success btn-sm btn-block">[$search]</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!--Tab 3-->
                <div class="tab-pane" id="panel3">
                    <div class="well-searchbox">
                        <h1 class="search-heading">[$search_heading_rent_to_buy]</h1>
                        <form  method="GET" action="<?= Utils::getComponentUrl('Real_Estate/ListRealEstate') ?>" class="form-horizontal" role="form">



                            <div class="form-group">
                                <label class="col-md-4 control-label">[$city]</label>
                                <div class="col-md-8">
                                    <select onchange="getLocalitiesRentBuy(this.value)" type="text" id="city" name="city" class="form-control mouse-focused" placeholder="[$type_a_city]"> 
                                        <option value = "">[$select_a_city]</option>
										<?php foreach ($locations_rentBuy as $location) {
		?>
											<option value = "<?= $location ?>"><?= $location ?></option>
										<?php
	} ?>

                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-md-4 control-label">[$locality]</label>
                                <div class="col-md-8">
                                    <select  type="text" id="locality_rent_buy" name="locality" class="form-control mouse-focused" placeholder="[$type_a_locality]"> 

                                        <option value = "">[$select_a_city]</option>


                                    </select>
                                </div>
                            </div>


                            <input type="hidden" name='contract' value="<?= $id_contract_rentBuy ?>">


                            <div class="form-group">
                                <label class="col-md-4 control-label">[$typology]</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="typology" placeholder="[$typology]">
                                        <option value="">[$any]</option>
										<?php foreach ($typologies as $typology) {
		?>
											<option value="<?= $typology->id ?>"><?= $typology->type ?></option>                    
										<?php
	} ?>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">[$max_price]</label>
                                <div class="col-md-8 test">
                                    <div class="range-slider price">
                                        <input name='price' class="range-slider__range " type="range" min="<?= $min_price_byuRent ?>" max="<?= $max_price_byuRent ?>" step="1000" value="<?= $max_price_byuRent ?>">
                                        <span class="range-slider__value">0</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Superficie Minima</label>
                                <div class="col-md-8">
                                    <div class="range-slider square-meter">
                                        <input name='square_meter' class="range-slider__range" type="range" min="<?= $min_surface_byuRent ?>" max="<?= $max_surface_byuRent ?>" step="5" value="<?= $min_price_byuRent ?>">
                                        <span class="range-slider__value">0</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-offset-4 col-md-8 col-sm-5">
                                    <button type="submit" class="btn btn-success btn-sm btn-block">[$search]</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>

    function getLocalities(city) {


        $.ajax({
            type: 'POST',
            url: '<?= Utils::getComponentUrl('Real_Estate/ajx_getLocalities') ?>',
            data: {
                'city': city,
                'id': '<?= $id_contract_sell ?>'
            },
            success: function (localities) {
                //console.log(localities);
                $('#locality').empty();
                $('#locality').append(localities);
            },
            error: function (errors) {
                console.log(errors);
            }




        });


    }


    function getLocalitiesRent(city) {


        $.ajax({
            type: 'POST',
            url: '<?= Utils::getComponentUrl('Real_Estate/ajx_getLocalities') ?>',
            data: {
                'city': city,
                'id': '<?= $id_contract_rent ?>'
            },
            success: function (localities) {
                //console.log(localities);
                $('#locality_rent').empty();
                $('#locality_rent').append(localities);
            },
            error: function (errors) {
                console.log(errors);
            }




        });


    }


    function getLocalitiesRentBuy(city) {

        $.ajax({
            type: 'POST',
            url: '<?= Utils::getComponentUrl('Real_Estate/ajx_getLocalities') ?>',
            data: {
                'city': city,
                'id': '<?= $id_contract_rentBuy ?>'
            },
            success: function (localities) {
                //console.log(localities);
                $('#locality_rent_buy').empty();
                $('#locality_rent_buy').append(localities);
            },
            error: function (errors) {
                console.log(errors);
            }




        });


    }





// AutoComplete Form
//    var availableTags = <?php echo json_encode($locations); ?>;
//
//    $("#city").autocomplete({
//        source: availableTags,
//        minLength: 1,
//        select: function (event, ui) {
//            event.preventDefault();
//            $("#city").val(ui.item.label);
//        }
//    });

// Range Slide
    var rangeSlider = function () {
        var slider = $('.range-slider'),
                range = $('.range-slider__range'),
                value = $('.range-slider__value');

        slider.each(function () {

            value.each(function () {
                var value = $(this).prev().attr('value');
                $(this).html(numberFormat(value, ',', '.'));
            });

            range.on('input', function () {
                $(this).next(value).html(numberFormat(this.value, ',', '.'));

            });
        });
    };

    rangeSlider();

    function numberFormat(_number, _sep, thousands_sep) {

        var parts = _number.toString().split(_sep);
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_sep);
        return parts.join(_sep);


        _number = typeof _number != "undefined" && _number > 0 ? _number : "";
        _number = _number.replace(new RegExp("^(\\d{" + (_number.length % 3 ? _number.length % 3 : 0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
        if (typeof _sep != "undefined" && _sep != " ") {
            _number = _number.replace(/\s/g, _sep);
        }
        return _number;
    }



</script>
