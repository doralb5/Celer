<?php

class sbd extends BaseModule
{
	public $biz;
	public $subdomain;

	public function __construct($block_id = '', $position = '', $package = '')
	{
		parent::__construct($block_id, $position, $package);
		$this->model = Loader::getModel('Businesses', 'Businesses');
		//$this->biz = $this->model->getSubdomainBySubdomain($_SERVER['HTTP_HOST']);
		$this->subdomain = $this->model->getSubdomainBySubdomain($_SERVER['HTTP_HOST']);
		$this->biz = $this->model->getBusiness($this->subdomain['target_id']);
		$this->ComponentSettings = BaseComponent::getComponentSettings('Businesses', 'Businesses');
		$image_baseurl = (isset($this->ComponentSettings['image_baseurl'])) ? $this->ComponentSettings['image_baseurl'] : '';
		$this->view->set('image_baseurl', $image_baseurl);
		//        $image_baseurl = '';
		if ($this->ComponentSettings['image_baseurl']) {
			$image_baseurl = $this->ComponentSettings['image_baseurl'];
		}
		$comments = $this->model->getComments($this->biz->id);
		$this->view->set('comments', $comments);
		$this->view->set('business', $this->biz);
		$this->view->set('baseurl', $image_baseurl);

		HeadHTML::setTitleTag($this->biz->company_name);

		//        $_SESSION['head_html'] = '
//        <meta name="description" content="'. $this->biz->description .'" >
//        <title>'. $this->biz->company_name .'</title>';
	}

	public function execute($parameters = array())
	{
		switch ($parameters['action']) {
			case 'logo':
				return $this->printLogo();
				break;
			case 'gallery':
				return $this->printGallery();
				break;
			case 'name':
				return $this->printName();
				break;
			case 'stars':
				return $this->printStars();
				break;
			case 'name':
				return $this->printSocial();
				break;
			case 'menu':
				return $this->printMenu();
				break;
			case 'banner-parallax':
				return $this->printBannerParallax();
				break;
			case 'banner-text':
				return $this->printBannerText();
				break;
			case 'description':
				return $this->printDescription();
				break;
			case 'map':
				return $this->printMap();
				break;
			case 'adwords':
				return $this->printAdwords();
				break;
			case 'video':
				return $this->printVideo();
				break;
			case 'contact-phone':
				return $this->printContactPhone();
				break;
			case 'contact-parallax':
				return $this->printContactParallax();
				break;
			case 'contact-footer':
				return $this->printContactFooter();
				break;
			case 'businesses-related':
				return $this->printBusinessesRelated();
				break;
			case 'social':
				return $this->printSocial();
				break;
			case 'rating':
				return $this->printRatings();
				break;
			case 'reviews-stat':
				return $this->printReviewsStat();
				break;
			case 'add-reviews':
				return $this->printAddReviews();
				break;
			case 'reviews':
				return $this->printReviews();
				break;
			case 'offers':
				return $this->printOffers();
				break;
			case 'payment':
				return $this->printPayment();
				break;
			case 'hours':
				return $this->printHours();
				break;
			case 'facebook-page':
				return $this->printFacebookPage();
				break;
		}
	}

	private function printLogo()
	{
		$this->view->set('image', $this->biz->image);
		$this->view->render('sbd-logo');
	}

	private function printName()
	{
		$this->view->set('description', $this->biz->description);
		$this->view->set('image', $this->biz->image);
		$this->view->set('company_name', $this->biz->company_name);
		$this->view->render('sbd-name');
	}

	private function printStars()
	{
		$this->view->set('rating', $this->biz->rating);
		$this->view->render('sbd-stars');
	}

	private function printMenu()
	{
		$this->view->render('sbd-menu');
	}

	private function printBannerParallax()
	{
		$cover_image = $this->subdomain['cover_image'];
		$this->view->set('cover_image', $cover_image);
		$this->view->render('sbd-banner-parallax');
	}

	private function printBannerText()
	{
		$this->view->set('company_name', $this->biz->company_name);
		$this->view->render('sbd-banner-text');
	}

	private function printContactPhone()
	{
		if (isset($_POST['submit_contact'])) {
			if ($this->sendMessage()) {
				$this->view->set('message_sended', true);
			} else {
				$this->view->set('message_sended', false);
			}
		}
		$this->view->render('sbd-contact-phone');
	}

	private function printContactParallax()
	{
		if (isset($_POST['submit_contact'])) {
			if ($this->sendMessage()) {
				$this->view->set('message_sended', true);
			} else {
				$this->view->set('message_sended', false);
			}
		}
		$this->view->render('sbd-contact-parallax');
	}

	private function printDescription()
	{
		$this->view->set('company_name', $this->biz->company_name);
		$this->view->set('image', $this->biz->image);
		$this->view->set('description', $this->biz->description);
		$this->view->set('image', $this->biz->image);
		$this->view->render('sbd-description');
	}

	private function printMap()
	{
		$this->view->set('image', $this->biz->image);
		$this->view->set('coordinates', $this->biz->coordinates);
		$this->view->render('sbd-map');
	}

	private function printAdwords()
	{
		$this->view->render('sbd-adwords');
	}

	private function printVideo()
	{
		$this->view->set('youtube_link', $this->biz->youtube_link);
		$this->view->set('facebook', $this->biz->facebook);
		$this->view->set('instagram', $this->biz->instagram);
		$this->view->set('twitter', $this->biz->twitter);
		$this->view->set('google', $this->biz->google);
		$this->view->set('linkedin', $this->biz->linkedin);
		$this->view->render('sbd-video');
	}

	private function printContactFooter()
	{
		$this->view->set('address', $this->biz->address);
		$this->view->set('email', $this->biz->email);
		$this->view->set('mobile', $this->biz->mobile);
		$this->view->set('company_name', $this->biz->company_name);
		$this->view->set('facebook', $this->biz->facebook);
		$this->view->set('instagram', $this->biz->instagram);
		$this->view->set('twitter', $this->biz->twitter);
		$this->view->set('google', $this->biz->google);
		$this->view->set('linkedin', $this->biz->linkedin);

		$website = $this->biz->subdomain;
		if ($this->biz->website == '' || $this->biz->id_profile == '1' || $this->biz->id_profile == '4') {
			$website = $this->biz->subdomain;
		} else {
			$website = $this->biz->website;
		}
		if (preg_match('#https?://#', $website) === 0) {
			$website = 'http://' . $website;
		}
		$this->view->set('website', $website);

		$this->view->render('sbd-contact-footer');
	}

	//********************Bussinesses Related************//
	private function printBusinessesRelated()
	{
		$businesses_related = $this->getBusinessRelated($this->biz->id_category, $this->biz->id);
		$this->view->set('businesses_related', $businesses_related);
		$this->view->render('sbd-businesses-related');
	}

	private function getBusinessRelated($category_id, $id_biz)
	{
		$filter = " cms_Business.id_category = '" . $category_id . "' AND  cms_Business.id != '" . $id_biz . "' AND cms_Business.enabled = '1'";
		return $this->model->getList($limit = 12, 0, $filter);
	}

	private function printSocial()
	{
		$this->view->set('facebook', $this->biz->facebook);
		$this->view->set('instagram', $this->biz->instagram);
		$this->view->set('twitter', $this->biz->twitter);
		$this->view->set('google', $this->biz->google);
		$this->view->set('linkedin', $this->biz->linkedin);
		$this->view->render('sbd-social');
	}

	private function printRatings()
	{
		$this->view->set('rating_count', $this->biz->rating_count);
		$this->view->set('total_rating', $this->biz->total_rating);
		$this->view->render('sbd-ratings');
	}

	private function printReviewsStat()
	{
		$this->view->render('sbd-reviews-stat');
	}

	private function printAddReviews()
	{
		$this->view->render('sbd-add-reviews');
	}

	private function printReviews()
	{
		//public $rating  - business entity
		$this->view->set('rating', $this->biz->rating);
		$this->view->render('sbd-reviews');
	}

	private function printOffers()
	{
		$business = $this->biz->id;
		$offers = $this->model->getOfferList(' id_biz = ' . $this->biz->id, 10, 0);
		$offers_count = $this->model->getLastCounter();

		$this->view->set('id_profile', $this->biz->id_profile);
		$this->view->set('business', $business);
		$this->view->set('offers', $offers);
		$this->view->set('offers_count', $offers_count);
		$this->view->render('sbd-offers');
	}

	private function printPayment()
	{
		$payment = $this->model->getPaymentMethodsOfBusiness($this->biz->id);
		$this->view->set('payment', $payment);
		$this->view->render('sbd-payment');
	}

	private function printHours()
	{
		$openh = $this->biz->open_hours;

		$openh_ordered = array();
		for ($day = 0; $day < 7; $day++) {
			$h = Utils::in_ObjectArray($openh, 'day', $day);
			if (!$h) {
				$h = new BusinessOpenHours_Entity();
				$h->business_id = $this->biz->id;
				$h->day = "$day";
				$h->closed = '1';
			}
			$openh_ordered[$day] = $h;
		}
		$this->view->set('openh', $openh_ordered);
		$this->view->render('sbd-hours');
	}

	private function printGallery()
	{
		$this->view->set('images', $this->biz->images);
		$this->view->render('sbd-gallery');
	}

	private function printFacebookPage()
	{
		$this->view->set('facebook', $this->biz->facebook);
		$this->view->render('sbd-facebook-page');
	}

	private function sendMessage()
	{
		if (isset($_POST['submit_contact'])) {
			$data['name'] = trim($_POST['name']);
			$data['email'] = trim($_POST['email']);
			$data['tel'] = trim($_POST['tel']);
			$data['message'] = trim($_POST['message']);
			$bizComponent = Loader::loadComponent('Businesses', 'Businesses');
			$res = $bizComponent->contactForm($this->biz->id, $this->biz, $data);
			unset($_POST['submit_contact']);
			if ($res) {
				return true;
			}
			return false;
		}
		return false;
	}
}
