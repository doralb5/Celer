<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'inc/css/business-related.css');
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 20;
$image_baseurl = (isset($settings['image_baseurl'])) ? $settings['image_baseurl'] : '';
?>

<div class="gallery-carousel">
    <div id="owl-gallery" class="owl-carousel text-center">
        <?php if (count($businesses_related) > 0) {
	?>
            <?php foreach ($businesses_related as $business_related) {
		?>

                <div class="skill-card">
                    <header class="skill-card__header">
                        
                        <?php if ($business_related->image != '') {
			?>
                            <a class="hover-img" href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business_related->company_name . '-' . $business_related->id)) ?>" title="">    
                                <img class="skill-card__icon" src="<?= Utils::genThumbnailUrl('businesses/' . $business_related->image, 180, 180, array(), true, $baseurl)?>"/>
                            </a>
                        <?php
		} else {
			?>
                            <a class="hover-img" href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business_related->company_name . '-' . $business_related->id)) ?>" title="">    
                                <img class="skill-card__icon" src="<?= Utils::genThumbnailUrl('businesses/default.jpg', 180, 180, array(), true, $baseurl)?>" alt="<?= strip_tags($business_related->company_name) ?>"/>
                            </a>
                        <?php
		} ?>
                    </header>
                    <section class="skill-card__body">
                        <h2 class="skill-card__title">
                            <a class="hover-img" href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($business_related->company_name . '-' . $business_related->id)) ?>" title="">

                                <?= StringUtils::CutString(strip_tags($business_related->company_name), $taglia_title) ?>
                            </a>
                        </h2>

                        
                        <!--                        <ul class="skill-card__knowledge">
                                                    <li>+355674881781</li>
                                                    <li>email</li>
                                                    <li>facebook</li>
                                                    <li>website</li>
                                                </ul>-->
                    </section>
                </div>


            <?php
	} ?>
        <?php
} ?>


    </div>
</div>


<!-- Owl-Carousel-JavaScript -->
<script>
    $(document).ready(function () {
        $("#owl-gallery").owlCarousel({
            items: 6,
            lazyLoad: true,
            autoPlay: true,
            pagination: false,
            loop: true,
        });
    });
</script>