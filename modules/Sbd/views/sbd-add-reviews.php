<div class="business-body alert-grey">

    <!--***** Rate It *****-->
    <section id="rates">
        <center>
            <h1 class="add-review-title">Jepni vleresimin tuaj</h1>
            <hr class="botm-line">
            <div style="cursor:pointer" class="rating-div">
                <i class="fa fa-star-o star-on-png  fa-3x ratings_stars" id="star_1"></i>

                <i data-alt="2" class="fa fa-star-o star-on-png  fa-3x ratings_stars" id="star_2"></i>

                <i data-alt="3" class="fa fa-star-o star-on-png  fa-3x ratings_stars" id="star_3"></i>

                <i data-alt="4" class="fa fa-star-o star-on-png  fa-3x ratings_stars" id="star_4"></i>

                <i data-alt="5" class="fa fa-star-o star-on-png fa-3x ratings_stars" id="star_5"></i>
            </div>
        </center>

    </section>


</div>