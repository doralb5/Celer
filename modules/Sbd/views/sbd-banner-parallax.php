<?php
if ($business->id_profile == 3 || $business->id_profile == 5) {
	if ($cover_image != null) {
		$image = Utils::genThumbnailUrl('businesses/covers/' . $cover_image, 1700, 540, array('zc' => '1'), true, $baseurl);
	}
} else {
	$image = 'https://c1.staticflickr.com/9/8899/28023274240_cd360dd86a_b.jpg';
}
?>


<style>

    .header-info{
        padding: 5em 0;
        /*background: url(http://subdomains.netirane.al/img/bg.jpg) no-repeat 0px 0px;*/
        background: url("<?= $image ?>") no-repeat 0px 0px;
        /*	background: url(https://c1.staticflickr.com/9/8899/28023274240_cd360dd86a_b.jpg) no-repeat 0px 0px;*/
        background-size:cover;
        color: #ffffff;
    }

</style>
<!--<div class="header-info-jarallax header-info">-->


