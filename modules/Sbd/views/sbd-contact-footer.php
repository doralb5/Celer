<div class="footer-info-content">

    <h1 class="contact-footer-title" class="big wow fadeInDown animated " data-wow-delay=".5s"><?= $company_name ?> </h1>
    <hr class="botm-line">

    <div class="footer-info">

		<?php if ($address != '') {
	?>
			<!-- Adress -->
			<h5 class="wow zoomIn animated " data-wow-delay="1s"><span class="glyphicon glyphicon-map-marker"></span> <?= $address ?> </h5>
		<?php
} ?>
		<?php if ($mobile != '') {
		?>
			<!-- Phone -->
			<h5 class="wow zoomIn animated " data-wow-delay="1.5s"><span class="glyphicon glyphicon-phone"></span> <a href="tel:<?= $mobile ?>"><?= $mobile ?></a></h5>
		<?php
	} ?>
		<?php if ($email != '') {
		?>
			<!-- Email -->
			<h5 class="wow zoomIn animated " data-wow-delay="2s"><span class="glyphicon glyphicon-envelope"></span> <a href="mailto:<?= $email ?>"><?= $email ?></a></h5>
		<?php
	} ?>
		<?php if ($website != '') {
		?>
			<!-- Website -->
			<h5 class="wow zoomIn animated " data-wow-delay="2.5s"><span class="glyphicon glyphicon-globe"></span> <a href="<?= $website ?>" target="_blank">Websiti i <?= $company_name ?></a></h5>
		<?php
	} ?>
    </div>
</div>