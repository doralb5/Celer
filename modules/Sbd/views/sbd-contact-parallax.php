<div class="contact-jarallax contact-info">
    <div class="paralaxMask"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 ">
                <div class="contact-content">
                    <center><h2>
                            <i class="fa fa-paper-plane-o iconRounded" aria-hidden="true"></i><br>
                            Kontakt
                        </h2></center>
                    <form method="POST" onsubmit="send_message(<?= $business->id ?>) & addStatistic(<?= $business->id ?>, 'subdomain_message');
                            return false;">
                        <input type="hidden" name="submit_contact" value="1"/>
                        <div class="row">
                            <div class="col-md-12">
                                <center><div id="div_response"></div></center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="login-email">
                                    <fieldset>
                                        <input type="text" class="msg-name" id="name"  name="name" required=""/>
                                        <label for="name">Emër</label>
                                        <div class="underline"></div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="login-email">
                                    <fieldset>
                                        <input type="text" class="msg-email" id="email"  name="email" required=""/>
                                        <label for="email">Email</label>
                                        <div class="underline"></div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="login-email">
                                    <fieldset>
                                        <input type="text" class="msg-tel" id="tel"  name="tel" required=""/>
                                        <label for="tel">Telefon</label>
                                        <div class="underline"></div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <textarea class="form-control wow fadeInDown animated " data-wow-delay=".3s" id="message" name="message" placeholder="Mesazhi" rows="5" required></textarea><br>

                        <div class="row">
                            <div class="col-md-12">
                                <center>
                                    <br/>
                                    <input class="btn btn-default wow fadeInDown animated " data-wow-delay=".5s" type="submit" value="Dergo"/><br/>
                                </center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>