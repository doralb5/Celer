<div class="iphone-position">
    <div class="iphone big wow fadeInUpBig animated " data-wow-delay=".5s">
        <div class="iphone-top">
            <span class="camera"></span>
            <span class="sensor"></span>
            <span class="speaker"></span>
        </div>
        <div class="top-bar"></div>
        <div class="iphone-screen">

            <div class="header-msg">
                <h2 class="header-msg-title">Kontakt</h2>
            </div>
            <div class="contact-content">
                <form method="POST" onsubmit="">
                    <input type="hidden" name="submit_contact" value="1" />
                    <div class="row">
                        <div class="col-md-12">
                            <center><div id="div_response"></div></center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="login-email">
                                <fieldset>
                                    <input type="text" class="msg-name" id="name"  name="name" required=""/>
                                    <label for="name">Emër</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="login-email">
                                <fieldset>
                                    <input type="text" class="msg-email" id="email"  name="email" required=""/>
                                    <label for="email">Email</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="login-email">
                                <fieldset>
                                    <input type="text" class="msg-tel" id="tel"  name="tel" required=""/>
                                    <label for="tel">Telefon</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                    <textarea class="form-control wow fadeInDown animated " data-wow-delay=".3s" id="message" name="message" placeholder="Mesazhi" rows="5" required></textarea><br>

                    <input class="btn send-msg-btn-overlay" data-wow-delay=".5s" type="submit" value="Dergo"/><br/>

                </form>
            </div>


        </div>
        <div class="buttons">
            <span class="on-off"></span>
            <span class="sleep"></span>
            <span class="up"></span>
            <span class="down"></span>
        </div>
        <div class="bottom-bar"></div>
        <div class="iphone-bottom">
            <span><i class="fa fa-paper-plane-o send-msg-btn" aria-hidden="true"></i></span>
        </div>
    </div>
</div>