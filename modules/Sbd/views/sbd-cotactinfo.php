<div class="iphone-position">
    <div class="iphone big wow fadeInUpBig animated " data-wow-delay=".5s">
        <div class="iphone-top">
            <span class="camera"></span>
            <span class="sensor"></span>
            <span class="speaker"></span>
        </div>
        <div class="top-bar"></div>
        <div class="iphone-screen">
<!--                                <iframe src="http://www.netirane.al/al/detajet-e-biznesit/1889" frameborder="0" width="100%" height="100%"></iframe>-->

            <div class="header-msg">
                <h2 class="header-msg-title">Kontakt</h2>
            </div>
            <div class="contact-content">
                <form method="POST" onsubmit="send_message(1889) & addStatistic(1889, 'subdomain_message');
                        return false;">
                    <input type="hidden" name="company_name" value="<?= $company_name ?>"/>
                    <div class="row">
                        <div class="col-md-12">
                            <center><div id="div_response"></div></center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="login-email">
                                <fieldset>
                                    <input type="text" class="msg-name" id="name"  name="name" required=""/>
                                    <label for="username">Emër</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="login-email">
                                <fieldset>
                                    <input type="text" class="msg-email" id="email"  name="email" required=""/>
                                    <label for="username">Email</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="login-email">
                                <fieldset>
                                    <input type="text" class="msg-tel" id="tel"  name="email" required=""/>
                                    <label for="username">Telefon</label>
                                    <div class="underline"></div>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                    <textarea class="form-control wow fadeInDown animated " data-wow-delay=".3s" id="message" name="message" placeholder="Mesazhi" rows="5" required></textarea><br>

                    <input class="btn send-msg-btn-overlay" data-wow-delay=".5s" type="submit" value="Dergo"/><br/>

                </form>
            </div>


        </div>
        <div class="buttons">
            <span class="on-off"></span>
            <span class="sleep"></span>
            <span class="up"></span>
            <span class="down"></span>
        </div>
        <div class="bottom-bar"></div>
        <div class="iphone-bottom">
            <span><i class="fa fa-paper-plane-o send-msg-btn" aria-hidden="true"></i></span>
        </div>
    </div>
</div>