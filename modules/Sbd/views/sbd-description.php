<div class="pin text-center">
    <img class="img-circle description-image"  src="<?= Utils::genThumbnailUrl('businesses/' . $image, 100, 100, array(), true, $baseurl) ?>"/>
</div>

<h1 class="text-center company-name"><?= $company_name ?></h1>
<hr class="botm-line" style="margin: 20px auto !important;">
<p class="company-description"><?= $description ?></p>
