<?php
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';

$w = (isset($parameters['w'])) ? $parameters['w'] : 1000;
$h = (isset($parameters['h'])) ? $parameters['h'] : 0;
?>

<div class="m-p-g">
    <div class="m-p-g__thumbs" data-google-image-layout data-max-height="350">
		<?php foreach ($images as $image) {
	?>
			<img src="<?= Utils::genThumbnailUrl('businesses/additional/' . $image->image, 502, 500, array('zc' => 1), true, $baseurl) ?>" data-full="<?= Utils::genThumbnailUrl('businesses/additional/' . $image->image, $w, $h, array(), true, $baseurl) ?>" class="m-p-g__thumbs-img" />
		<?php
} ?>
    </div>
    <div class="m-p-g__fullscreen"></div>
</div>

<script>
    var elem = document.querySelector('.m-p-g');

    document.addEventListener('DOMContentLoaded', function () {
        var gallery = new MaterialPhotoGallery(elem);
    });
</script>
