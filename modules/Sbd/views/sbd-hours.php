<style>
    .closed {
        color: red;
        background: #202020;
        border-radius: 2px;
    }
    .closed .hour {
        color: #c90004;
        font-size: 17px;
        text-transform: uppercase;
    }
</style>

<?php
if ($business->id_profile == 3 || $business->id_profile == 5) {
	?>
	<?php if (!is_null($openh) && count($openh)) {
		?>
		<div class="business-hours">
			<h1 class="hours-title">Orari</h1>
			<hr class="botm-line">
			<ul class="list-unstyled opening-hours">
				<?php
				for ($i = 1; $i <= 7; $i++) {
					if ($i == 7) {
						$i = 0;
					}
					$op = $openh[$i]; ?>
					<li class="timetable <?php if ($op->closed) {
						?> closed <?php
					} ?><?= (date('w') == $i) ? 'today' : ''; ?>">
						<table class="flat-table">
							<tbody>
								<tr>
								<td class="day"><i class="fa fa-clock-o" aria-hidden="true"></i> <?= Utils::numbertoweekdate($op->day, FCRequest::getLang()) ?></td>
								<?php if (($op->closed || $op->hours24) != 1) {
						?>
									<td class="hour">
										<?= date('H:i', strtotime($op->open_start)) ?>
										<?php
										if ($op->pause_start != null && $op->pause_end != null) {
											echo ' - ' . date('H:i', strtotime($op->pause_start));
											echo '&nbsp;&nbsp;&nbsp;';
											echo date('H:i', strtotime($op->pause_end));
										} ?>
										<?= ' - ' . date('H:i', strtotime($op->open_end)); ?></td>
								<?php
					} else {
						?>
									<?php if ($op->closed) {
							?>
										<td class="hour">[$closed]</td>
									<?php
						} else {
							?>
										<td class="hour">[$hours24]</td>
									<?php
						} ?>
								<?php
					} ?>
								</tr>
							</tbody>
						</table>
					</li>
					<?php
					if ($i == 0) {
						break;
					}
				} ?>
			</ul>
		</div>

	<?php
	} ?>

<?php
} ?>