<div class="navbar-header page-scroll">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>
<div name="logo" class="logo">
    <img class="thumbnail wow zoomIn animated " data-wow-delay=".5s" src="<?= Utils::genThumbnailUrl('businesses/' . $image, 180, 180, array(), true, $baseurl) ?>"/>
</div>