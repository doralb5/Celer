


<div id="map" class="map"></div>

<div class="col-md-6 col-md-offset-3 text-center iphone-mockup">
    <img src="<?= Utils::genThumbnailUrl('iPhone6-Gold.png', 0, 0, array()) ?>" width="100%;">
</div>


<script>
    /**
     * Called on the intiial page load.
     */

    var marker;
    function init() {
        var mapCenter = new google.maps.LatLng(<?= $coordinates ?>);
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: mapCenter,
            scrollwheel: false,
            draggable: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        marker = new RichMarker({
            position: mapCenter,
            map: map,
            draggable: false,
            content:
                    '<div class="my-marker wow bounceInDown animated " data-wow-delay="1s"> ' +
                    '<div class="pin">' +
                    '<img class="img-circle"  src="<?= Utils::genThumbnailUrl('businesses/' . $image, 180, 180, array(), true, $baseurl) ?>"/>' +
                    '</div>' +
                    '<div class="pulse"></div>' +
                    '</div>'
        });

        //  map style
        styles = [{"stylers": [{"saturation": -100}]}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#0099dd"}]}, {"elementType": "labels", "stylers": [{"visibility": "off"}]}, {"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"color": "#aadd55"}]}, {"featureType": "road.highway", "elementType": "labels", "stylers": [{"visibility": "on"}]}, {"featureType": "road.arterial", "elementType": "labels.text", "stylers": [{"visibility": "on"}]}, {"featureType": "road.local", "elementType": "labels.text", "stylers": [{"visibility": "on"}]}, {}];
        map.set('styles', styles);


        google.maps.event.addListener(marker, 'position_changed', function () {
            log('Marker position: ' + marker.getPosition());
        });
    }
    // Register an event listener to fire when the page finishes loading.
    google.maps.event.addDomListener(window, 'load', init);
</script>