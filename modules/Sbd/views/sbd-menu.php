
<div class="menu">


    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse ">

        <ul class="nav navbar-nav navbar-right">
            <li>
                <a class="page-scroll wow bounceInLeft animated " data-wow-delay="1.5s" href="#page-top">Home</a>
            </li>
            <li>
                <a class="page-scroll wow bounceInLeft animated " data-wow-delay="1.5s" href="#offers">Oferta</a>
            </li>
            <li>
                <a class="page-scroll wow bounceInLeft animated " data-wow-delay="1.2s" href="#about">Rreth Nesh</a>
            </li>
            <li>
                <a class="page-scroll wow bounceInLeft animated " data-wow-delay=".8s" href="#gallery">Galeria</a>
            </li>
            <li>
                <a class="page-scroll wow bounceInLeft animated " data-wow-delay=".5s" href="#contact">Kontakt</a>
            </li>
        </ul>

    </div>
</div>