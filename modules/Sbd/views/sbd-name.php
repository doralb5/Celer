<?php
HeadHTML::addMetaProperty('og:type', 'website');
HeadHTML::addMetaProperty('og:title', $business->company_name);
HeadHTML::addMetaProperty('og:description', htmlentities(Utils::trim_text($business->description, 200)));
HeadHTML::addMetaProperty('og:url', Utils::genUrl('', true));
HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl(str_replace(' ', '%20', 'businesses/' . $business->image), 900, 600, array(), true, $image_baseurl));
?>


<h1 class="company-name">
    <a href="/" class="wow zoomIn animated " data-wow-delay="1s"><?= $company_name ?></a>
</h1>