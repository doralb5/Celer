<?php HeadHTML::AddStylesheet(WEBROOT . COMPONENTS_PATH . 'Businesses/' . VIEWS_PATH . 'css/offer-details-style.css'); ?>
<?php
$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$tel = isset($_POST['tel']) ? $_POST['tel'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';
?>
<?php
if (in_array($id_profile, array(3)) && ($offers_count != null)) {
	?>
	<div id="Business-Offers">
		<div class="business_std_container" style="margin-top: 0px;" >
			<div class="content">
				<div class="row">

					<!--***** Business Offers *****-->
					<div class="col-md-12 business-body alert-grey">
						<style>
							#Business-Offers .main-offer-info {background: #1f1f1f;padding: 10px 10px;}
							#Business-Offers .business-offer-name {
								font-size:12px;
								margin-top: 0px;
								margin-bottom: 0px;
							}
							#Business-Offers .business-offer-name a{
								font-size: 10px;
								line-height: 0;
								letter-spacing: 1px;
								color: #828282;
								text-transform: uppercase;
								text-align: left;
							}
							#Business-Offers .business-offer-title a{
								color:white;
							}
							#Business-Offers .business-offer-title {
								font-size:20px;
								margin-top: 5px;
								margin-bottom: 0px;
								text-align: left;
							}
							#Business-Offers .btn-xs {
								padding: 10px 10px;
							}

							#Business-Offers hr {
								margin-top: 10px;
								margin-bottom: 10px;
							}
							#Business-Offers .owl-theme .owl-controls .owl-page span {
								background: #ffffff;
							}
							.offer-price {
								margin-top: 7px;
							}
							.modal{
								color:black;   
							}
							.modal-header {
								padding-bottom: 5px;
							}
							.modal-footer {
								padding: 0;
							}
							.modal-footer .btn-group button {
								height:40px;
								border-top-left-radius : 0;
								border-top-right-radius : 0;
								border: none;
								border-right: 1px solid #ddd;
							}
							.modal-footer .btn-group:last-child > button {
								border-right: 0;
							}

						</style>
						<!-- Modal Offers Preview -->
						<?php
						if (!is_null($offers)) {
							?>
							<!-- Ofertat -->
							<?php
							foreach ($offers as $offer) {
								?>
								<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-body">
												<!-- content goes here -->
												<form id="business-info-form" method="POST">
													<div class="row">
														<div class="preview col-md-6">

															<div class="preview-pic tab-content">
																<?php $first = true;
								foreach ($offer->images as $key => $img) {
									?>
																	<div class="tab-pane <?= $first == true ? 'active' : '' ?>" id="pic-<?= $key ?>"><img src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 500, 300, array(), true, $image_baseurl) ?>" /></div>
																	<?php $first = false;
								} ?>
															</div>
															<ul class="preview-thumbnail nav nav-tabs">
																<?php foreach ($offer->images as $key => $img) {
									?>
																	<li class="<?= $key == 0 ? 'active' : '' ?>"><a data-target="#pic-<?= $key ?>" data-toggle="tab"><img src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 400, 400, array(), true, $image_baseurl) ?>" /></a></li>
			<?php
								} ?>
															</ul>

														</div>

														<div class="col-md-6">
															<h3 class="modal-title" id="lineModalLabel"><?= $offer->title ?></h3>
															<h1 class="business-offer-name">
																<a href="<?= Utils::getComponentUrl('Businesses/b_show/' . Utils::url_slug($offer->business_name . '-' . $offer->id_biz)) ?>"><?= $offer->business_name ?></a>
															</h1>
															<hr>
															<div class="offer-price" style="float:none";>
																<?php if ($offer->final_price != '') {
									?>
																	<div class="price"><?= $offer->price ?>&nbsp;<?= $offer->currency ?></div>
																	<div class="final-price"><?= $offer->final_price ?>&nbsp;<?= $offer->currency ?></div>
																<?php
								} else {
									?>
																	<span class="only-price"><?= $offer->price ?>&nbsp;<?= $offer->currency ?></span>                                                   
			<?php
								} ?>
															</div>
															<hr>
															<h5 class="titlecatege"><?= strip_tags($offer->description) ?></h5>


														</div>
													</div>

												</form>
											</div>
											<!--***** Send Request *****-->
											<section id="send-request-<?= $offer->id ?>" style="display:none;">
												<fieldset>
													<legend class="scheduler-border" id="row6">[$send_a_request]</legend>
													<div class="col-md-10 col-md-offset-1">
														<div id="reqRespond" ></div>
														<form class="form-request" class="post-request" id="<?= $offer->id ?>" action="" method="post">
															<input type="hidden" name="title" value="<?= $offer->title ?>" />
															<div class="row">
																<div class="col-md-6">
																	<div class="business-insertion">
																		<fieldset>
																			<input type="text" class="name required" id="name"  name="name-request" <?php if (isset($_SESSION['user_auth'])) {
									?> value="<?= $_SESSION['user_auth']['firstname'] . ' ' . $_SESSION['user_auth']['lastname'] ?>" <?php
								} ?> data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." required="" />
																			<label for="name">[$Name]*</label>
																			<div class="underline"></div>
																		</fieldset>
																	</div>
																	<div class="business-insertion">
																		<fieldset>
																			<input type="email" class="email required" id="email"  name="email-request" value="<?= $email ?>" data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@email.com)" required="" />
																			<label for="email">[$Email]*</label>
																			<div class="underline"></div>
																		</fieldset>
																	</div>
																	<div class="business-insertion">
																		<fieldset>
																			<input type="text" class="tel required" id="tel"  name="tel-request"  value="<?= $tel ?>" data-placement="top" data-trigger="manual" data-content="Must be a valid phone number" required="" />
																			<label for="tel">[$Tel]*</label>
																			<div class="underline"></div>
																		</fieldset>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="">
																		<textarea  rows="7" cols="100" class="form-control send-msg-textarea" id="message-request" name="message-request" ><?= $message ?></textarea>
																	</div>
																</div>
															</div>
															<br>
															<center><div class="g-recaptcha" id="recaptcha1" required=""></div></center>
															<br>
															<input type="hidden" name="request" value="1">
															<div class="form-group">
																<center>
																	<button type="submit" name="send-request" id_biz="<?= $offer->id_biz ?>" class="btn btn-default btn-sm">[$Send]</button>
																	<a class="btn btn-default btn-sm" id="close-request-<?= $offer->id ?>">[$Close]</a>
																</center>
																<p class="help-block pull-left text-danger hide" id="form-error">&nbsp; Ndodhi nje gabim. </p>
															</div>
														</form>
													</div>
												</fieldset>
											</section>
											<div class="modal-footer">
												<div class="btn-group btn-group-justified" role="group" aria-label="group button">
													<div class="btn-group" role="group">
														<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Mbyll</button>
													</div>
													<div class="btn-group" role="group">
														<button type="button" id="savedata" class="btn btn-default btn-hover-green request-<?= $offer->id ?>" data-action="save" name="save" role="button">Request Offer</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<script>
									$(".request-<?= $offer->id ?>").click(function () {
										$("#send-request-<?= $offer->id ?>").slideToggle("slow");
									});
									$("#close-request-<?= $offer->id ?>").click(function () {
										$("#send-request-<?= $offer->id ?>").slideToggle("slow");
									});
								</script>
		<?php
							} ?>
							<script>
								$(document).ready(function () {

									$('.form-request').on('submit', function (e) {
										// show that something is loading
										$('#reqRespond').html("<b>Loading response...</b>");
										$.ajax({
											type: 'POST',
											url: '<?= Utils::getComponentUrl('Businesses/ajx_offer_requestinfo/') ?>' + this.id,
											data: $(this).serialize()
										})
												.done(function (data) {
													// show the response
													$('#reqRespond').html(data);
												})
												.fail(function () {
													// just in case posting your form failed
													alert("Posting failed.");
												});
										e.preventDefault();
										// to prevent refreshing the whole page page
										return false;

									});
								});
							</script>
	<?php
						} ?>
						<h1 class="offer-title">OFERTAT E BIZNESIT</h1>
						<hr class="botm-line">
						<div id="owl-offers" class="owl-carousel">
							<?php if (!is_null($offers)) {
							?>
								<?php $taglia_offer_content = (isset($parameters['taglia_offer_content'])) ? $parameters['taglia_offer_content'] : 125; ?>
								<!-- Ofertat -->
		<?php foreach ($offers as $offer) {
								?>

									<div id="carousel-example-generic-<?= $offer->id ?>" class="carousel slide offers-carousel" data-ride="carousel" data-interval="false">
										<div class="carousel-inner">
											<?php
											$first = true;
								if (count($offer->images)) {
									foreach ($offer->images as $img) {
										?>
													<div class="item <?= ($first) ? 'active' : ''; ?> srle">
														<img src="<?= Utils::genThumbnailUrl('businesses/offers/' . $img, 500, 300, array(), true, $image_baseurl) ?>" alt="" class="img-responsive">
													</div>
													<?php $first = false;
									} ?>
											<?php
								} else {
									?>
												<div class="item <?= ($first) ? 'active' : ''; ?> srle">
													<img src="<?= Utils::genThumbnailUrl('businesses/offers/default.jpg', 500, 300, array(), true) ?>" alt="" class="img-responsive">
												</div>
			<?php
								} ?>
										</div>
			<?php if (count($offer->images) > 1) {
									?>
											<a class="left carousel-control" href="#carousel-example-generic-<?= $offer->id ?>" role="button" data-slide="prev">
												<span class="glyphicon glyphicon-chevron-left"></span>
											</a>
											<a class="right carousel-control" href="#carousel-example-generic-<?= $offer->id ?>" role="button" data-slide="next">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</a>
			<?php
								} ?>
										<div class="main-offer-info">
											<h1 class="business-offer-title">
												<a title=""><?= $offer->title ?></a>
											</h1>
										</div>


										<div class="alldiv-text">
											<div class="offer-price">
												<?php if ($offer->final_price != '') {
									?>
													<div class="price"><?= Utils::price_format($offer->price, $offer->currency, true, false); ?></div>
													<div class="final-price"><?= Utils::price_format($offer->final_price, $offer->currency, true, false); ?></div>
			<?php
								} else {
									?>
													<span class="only-price"><?= Utils::price_format($offer->price, $offer->currency, true, false); ?></span>                                                   
			<?php
								} ?>
											</div>
											<span style=" float: right; ">
												<a href="" class="btn btn-xs btn-default bussiness-comments" data-target="#Modal" data-toggle="modal" >Me Shume</a>
											</span>                                      
										</div>
									</div>
		<?php
							} ?>


								<script>
									// thumbnails.carousel.js jQuery plugin
									;
									(function (window, $, undefined) {

										var conf = {
											center: true,
											backgroundControl: false
										};

										var cache = {
											$carouselContainer: $('.thumbnails-carousel').parent(),
											$thumbnailsLi: $('.thumbnails-carousel li'),
											$controls: $('.thumbnails-carousel').parent().find('.carousel-control')
										};

										function init() {
											cache.$carouselContainer.find('ol.carousel-indicators').addClass('indicators-fix');
											cache.$thumbnailsLi.first().addClass('active-thumbnail');

											if (!conf.backgroundControl) {
												cache.$carouselContainer.find('.carousel-control').addClass('controls-background-reset');
											} else {
												cache.$controls.height(cache.$carouselContainer.find('.carousel-inner').height());
											}

											if (conf.center) {
												cache.$thumbnailsLi.wrapAll("<div class='center clearfix'></div>");
											}
										}

										function refreshOpacities(domEl) {
											cache.$thumbnailsLi.removeClass('active-thumbnail');
											cache.$thumbnailsLi.eq($(domEl).index()).addClass('active-thumbnail');
										}

										function bindUiActions() {
											cache.$carouselContainer.on('slide.bs.carousel', function (e) {
												refreshOpacities(e.relatedTarget);
											});

											cache.$thumbnailsLi.click(function () {
												cache.$carouselContainer.carousel($(this).index());
											});
										}

										$.fn.thumbnailsCarousel = function (options) {
											conf = $.extend(conf, options);
											init();
											bindUiActions();
											return this;
										}

									})(window, jQuery);

									$('.thumbnails-carousel').thumbnailsCarousel();

								</script>
	<?php
						} ?>
						</div>

						<!-- Owl-Carousel-JavaScript -->
						<script>
							$(document).ready(function () {
								$("#owl-offers").owlCarousel({
									items: 3,
									lazyLoad: false,
									autoPlay: false,
									pagination: true,
									loop: false,

									responsiveClass: true,
									responsive: {
										0: {
											items: 1,
											nav: true
										},
										600: {
											items: 3,
											nav: false
										},
										1000: {
											items: 3,
											nav: true,
											loop: false
										}
									}

								});
							});





						</script>

					</div>
					<!--***** END Business Offers *****-->
				</div>
			</div>
		</div>
	</div>
<?php
} ?>
