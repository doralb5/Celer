<?php
if ($business->id_profile == 3 || $business->id_profile == 5) {
	?>
	<div class="payment">
		<h1 class="payment-title">Menyra e Pageses</h1>
		<hr class="botm-line" style="margin:20 auto;">
		<?php foreach ($payment as $p) {
		?>
			<!-- Payment Method -->
			<?php if ($p->id_payment === '1') {
			?>
				<i class="pf pf-cash" title="Kesh"></i>
			<?php
		} ?>
			<?php if ($p->id_payment === '2') {
			?>
				<i class="pf pf-bank-transfer" title="Bank Transfert"></i>
			<?php
		} ?>
			<?php if ($p->id_payment === '3') {
			?>
				<i class="pf pf-credit-card" title="Kredit Kard"></i>
			<?php
		} ?>
			<?php if ($p->id_payment === '4') {
			?>
				<i class="pf pf-card" title="Debit Kard"></i>
			<?php
		} ?>
			<?php if ($p->id_payment === '5') {
			?>
				<i class="pf pf-paypal" title="Paypal"></i>
			<?php
		} ?>

		<?php
	} ?>
	</div>
<?php
} ?>