<!--***** Reviews *****-->
<div id="reviews" class="add-review" style="display: block;">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <section >
                <fieldset id="fieldset-review">
                    <!--                    <legend class="scheduler-border">Shkruani nje koment</legend>-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center"><h1 class="add-review-title">Shtoni nje recension</h1></div>
                            <form id="r-form" name="r-form"><input type="hidden" name="rating" value="undefined">
                                <input type="hidden" name="id_biz" value="<?= $business->id ?>">
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="business-insertion">
                                            <fieldset>
                                                <input type="text" class="review-name" id="r-name" name="name" required="">
                                                <label for="r-name" class="">Emër</label>
                                                <div class="underline"></div>
                                            </fieldset>
                                        </div>

                                        <div class="business-insertion">
                                            <fieldset>
                                                <input type="email" class="email required" id="r-email" name="email" required="">
                                                <label for="r-email">Email</label>
                                                <div class="underline"></div>
                                            </fieldset>
                                        </div>

                                        <div class="business-insertion">
                                            <fieldset>
                                                <input type="text" class="tel required" id="r-tel" name="tel" required="">
                                                <label for="r-tel">Telefoni*</label>
                                                <div class="underline"></div>
                                            </fieldset>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <textarea class="form-control send-msg-textarea" id="r-comment" name="comment" placeholder="Lini komentin tuaj" rows="7" required=""></textarea>
                                        <br>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <center>

                                            </center>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <center>
                                            <input class="btn btn-default filled" name="s-review" id="s-review" value="Dergo" style="width: inherit;">

                                            <a class="btn btn-default" id="skip-reviews" type="clear">Anullo</a> 
                                        </center>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </fieldset>
            </section>
        </div>
    </div>
</div>


<div class="col-md-12 show-rating">

    <div class="card">
        <div id="showrate" class="card__num"></div>
    </div>

    <!--    <div class="rate-it rate-cus">
            <i class="fa fa-star-o star-on-png" id="star_1_<?= $business->id ?>"></i>
	
            <i data-alt="2" class="fa fa-star-o star-on-png"
               id="star_2_<?= $business->id ?>"></i>
	
            <i data-alt="3" class="fa fa-star-o star-on-png"
               id="star_3_<?= $business->id ?>"></i>
	
            <i data-alt="4" class="fa fa-star-o star-on-png"
               id="star_4_<?= $business->id ?>"></i>
	
            <i data-alt="5" class="fa fa-star-o star-on-png"
               id="star_5_<?= $business->id ?>"></i>
        </div>-->

    <div id="average" style="color: #ffffff;font-weight: normal;"></div>
</div>








<!-- Rating system -->
<script>
    $(document).ready(function () {


        function checkFieldsIfEmpty() {
            var count = 0;

            if ($('#r-name').val() == '') {
                count++;
            }
            if ($('#r-email').val() == '') {
                count++;
            }
            if ($('#r-tel').val() == '') {
                count++;
            }
            if ($('#r-comment').val() == '') {
                count++;
            }
            return count;

        }


        function hideMessages() {
            setTimeout(function () {
                $("#msg-success").hide('slow');
            }, 3500);
            setTimeout(function () {
                $(".alert-danger").hide('slow');
            }, 3500);
        }

        $("#s-review").click(function () {

            if (checkFieldsIfEmpty() > 0) {
                $('#reviews').prepend('<p class="msg-success alert-danger alert-costumized" name="messageajx" >[$all_fields_are_required]</p>');
                hideMessages();

            } else {

                $.ajax({
                    type: "POST",
                    url: '<?= Utils::getComponentUrl('Businesses/ajx_reviewBusiness') ?>',
                    data: $("#r-form").serialize(),
                    success: function (data) {
                        console.log(data);
                        $('#reviews').append(data);
                        $("#fieldset-review").hide('slow');
                        hideMessages();
                        hideMessages();
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            }
        });
        setTimeout(function () {
            $(".alert-costumized").hide('slow');
        }, 3500);
        function addStatistic(id_business, type) {
            $("#reviews").hide();
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_addStatistic') ?>",
                data: {
                    'id_business': id_business,
                    'type': type,
                },
                success: function (data) {
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function rate(rating, id_biz) {
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_rateBusiness') ?>",
                data: {
                    'id_biz': id_biz,
                    'rating': rating
                },
                success: function (data) {
                    var arr = data.split(",");
                    console.log(data);

                    var showError = document.getElementById('Error');

                    if (arr[0] !== '' && showError === null) {
                        $('#r-form').prepend('<div id="Error" class="alert alert-info today" style="background: lightseagreen;border-radius: 3px;">' + arr[0] + '</div>');

                        setTimeout(function () {
                            $('#Error').remove();
                        }, 5000);
                    }
                    $('#r-form').prepend('<input type="hidden" name="id" value="' + arr[0] + '" />');
                    $('#r-form').prepend('<input type="hidden" name="id_biz" value="' + arr[1] + '" />');
                    $('#r-form').prepend('<input type="hidden" name="rating" value="' + arr[2] + '" />');
                    //location.href = '<?= Utils::getComponentUrl('Businesses/review_business') ?>';
                    //success
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function controllCookie(id_biz) {

            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_checkRatingCookie') ?>",
                data: {
                    'id_biz': id_biz,
                },
                success: function (rating) {
                    //console.log(rating);
                    if (rating != 0) {
                        for (var i = 1; i <= rating; i++) {
                            $('#star_' + i).attr("class", "fa fa-star star-on-png fa-3x ratings_stars");
                        }
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function ratingDataText(id_biz) {
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValue') ?>",
                data: {
                    'id_biz': id_biz,
                },
                success: function (data) {
                    d = data.split("|");
                    $('#average').html("[$rating]: " + d[0] + " - [$rating_count]: " + d[1]);
                },
                error: function (e) {
                    $('#average').html("Pati probleme");
                    console.log(e);
                }
            });
        }

        function ratingDataTextShow(id_biz) {
            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValue') ?>",
                data: {
                    'id_biz': id_biz,
                },
                success: function (data) {
                    d = data.split("|");
                    $('#showrate').html(d[0]);
                },
                error: function (e) {
                    $('#average').html("Pati probleme");
                    console.log(e);
                }
            });
        }

        function ratingDataStar(id_biz) {

            $.ajax({
                type: "POST",
                url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValueBList') ?>",
                dataType: 'json',
                data: {
                    'id_biz': id_biz,
                },
                success: function (data) {

                    if (data.rate != 0) {
                        for (var i = 0; i <= data.rate; i++)
                            $('#star_' + i + '_' + id_biz).attr("class", "fa fa-star star-on-png");
                        if (data.half == 1)
                            $('#star_' + i + "_" + id_biz).attr("class", "fa fa-star-half-o star-on-png");
                    }

                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        // reviews toggle
        $("#skip-reviews").click(function () {
            $("#reviews").slideToggle("slow");
        });
        // message toggle

        $(".contact-open").click(function () {
            $("#send-msg").slideToggle("slow");
        });
        $(".request-open").click(function () {
            $("#send-request").slideToggle("slow");
        });
        $("#skip-request").click(function () {
            $("#send-request").slideToggle("slow");
        });
        $("#skip-msg").click(function () {
            $("#send-msg").slideToggle("slow");
        });
        $(document).ready(function () {
            //yje ne title mbushi
            ratingDataStar(<?= $business->id ?>);
            //jep mesataren e rating te bere
            ratingDataText(<?= $business->id ?>);
            //shfaq mesataren e rating
            ratingDataTextShow(<?= $business->id ?>);
            //kontroll cookie nqs ka votuar
            controllCookie(<?= $business->id ?>);
            addStatistic(<?= $business->id ?>, 'view');
            $('#website_link').click(function () {
                addStatistic(<?= $business->id ?>, 'website_click');
            });
            $('#facebook_link').click(function () {
                addStatistic(<?= $business->id ?>, 'facebook_click');
            });
            $('#linkedin_link').click(function () {
                addStatistic(<?= $business->id ?>, 'linkedin_click');
            });
            $('#instagram_link').click(function () {
                addStatistic(<?= $business->id ?>, 'instagram_click');
            });
//            var checkifclicked = false;
//            var countclicked = null;
//            //hover
//            $('.ratings_stars').hover(
//                //mouseover
//                function () {
//                        $(this).prevAll().andSelf().attr("class", "fa fa-star star-on-png  fa-3x ratings_stars"); 
//                },
//                //mouseout
//                        function () {
//                            if(checkifclicked === false){
//                                $(this).prevAll().andSelf().attr("class", "fa fa-star-o star-on-png fa-3x ratings_stars");
//                            } else {
//                                for (i = countclicked+1; i < 6; i++) {
//                                    $('#star_'+i).removeClass('fa-star');
//                                    $('#star_'+i).addClass('fa-star-o');
//                                  }
//                            }
//                        }
//                );
            //click
            $('.ratings_stars').click(
                    function () {
                        //checkifclicked = true;
                        var rating = $(this).prevAll().andSelf().length;
                        var stars = $(this).parent().children('i');
                        //countclicked = rating;
                        for (i = 0; i < rating + 1; i++) {
                            $('#star_' + i).removeClass('fa-star-o');
                            $('#star_' + i).addClass('fa fa-star star-on-png  fa-3x ratings_stars');
                        }
                        for (i = rating + 1; i < 6; i++) {
                            $('#star_' + i).removeClass('fa-star');
                            $('#star_' + i).addClass('fa-star-o');
                        }

                        $("#reviews").show("slow");

                        rate(rating,<?= $business->id ?>);
                        //$('.ratings_stars').bind();
                        ratingDataText(<?= $business->id ?>);
                        ratingDataStar(<?= $business->id ?>);
                    }
            );
        });

    });
</script>