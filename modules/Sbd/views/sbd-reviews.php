<h1 class="reviews-title">Recensionet e Biznesit</h1>
<hr class="botm-line">
<div class="owl-carousel wow fadeInDown animated" data-wow-delay=".2s" id="review_carousel">

	<?php
	foreach ($comments as $review) {
		if ($review['name'] != '' && $review['comment'] != '') {
			?>
			<div class="item-bottom">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">

						<div class="client-pic">
							<img class="img_res" src="http://www.netirane.al/data/netirane.al/media/avatar.png" alt="">
						</div>
						<p class="client-name"> <?= $review['name'] ?> </p>
						<p class="review-star">
						<span class="rate-it rate-cus">
							<?php
							$i = $review['rating'];
			$j = 5 - $i;
			$delay = 0.5;
			for ($u = 1; $u <= $i; $u++) {
				?>

								<i class="fa fa-star star-on-png wow zoomIn animated  " data-wow-delay="<?= $delay ?>"></i>
								<?php
								$delay += 0.25;
			}
			$delay = 1.5 - $delay;
			for ($b = 1; $b <= $j; $b++) {
				?>

								<i class="fa fa-star-o star-on-png  wow zoomIn animated" data-wow-delay="<?= $delay ?>"></i>

								<?php
								$delay += 0.25;
			} ?>
						</span>
						</p>
						<p class="review-desc">
							"<?= $review['comment'] ?>"
						</p>
						<p class="testimonial-date"><?= date('d', strtotime($review['publish_date'])) ?>   <?= Utils::AlbanianWordMonth(date('m', strtotime($review['publish_date']))) ?> <?= date('Y', strtotime($review['publish_date'])) ?></p>  

					</div>
				</div>
			</div>

			<?php
		}
	}
	?>

</div>


<?php list($lat, $lng) = explode(',', $business->coordinates); ?>
<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "LocalBusiness",
    "image": "<?= Utils::genThumbnailUrl('businesses/' . $business->image, 750, 500, array(), true, $baseurl); ?>",
    "name": "<?= $business->company_name ?>",
    "address" : "<?= $business->address ?>",
    "priceRange" : "medium",
    "url": "<?= $business->website ?>",
    "telephone" : "<?= $business->phone ?>",

    "geo": {
    "@type": "GeoCoordinates",
    "latitude": <?= $lat ?>,
    "longitude":  <?= $lng ?>
    }

	<?php if ($business->rating_count > 0) {
		?>
		,
		"aggregateRating": {
		"@type": "AggregateRating",
		"ratingValue": "<?= $business->rating ?>",
		"reviewCount": "<?= $business->rating_count ?>"
		}
	<?php
	} ?>

    }
</script>




<script>
    $("#review_carousel").owlCarousel({
        items: 3,
        lazyLoad: true,
        nav: true,
        navigationText: false,
        pagination: false,
        autoPlay: 5000,
        singleItem: true,
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                loop: false
            }
        }

    });
</script>