<!--Business Social Networks -->
<div class=" social-icons"> 
	<?php if ($facebook != '') {
	?>
		<a target="_blank" href="<?= $facebook ?>">
			<i class="fa fa-facebook-square fa-size wow bounceIn animated " data-wow-delay="1s" aria-hidden="true"></i>
		</a>
	<?php
} ?>

	<?php if ($linkedin != '') {
		?>
		<a target="_blank" href="<?= $linkedin ?>">
			<i class="fa fa-linkedin-square fa-size wow bounceIn animated " data-wow-delay="1.25s" aria-hidden="true"></i>
		</a>

	<?php
	} ?>

	<?php if ($instagram != '') {
		?>
		<a target="_blank" href="<?= $instagram ?>">
			<i class="fa fa-instagram fa-size wow bounceIn animated " data-wow-delay="1.5s" aria-hidden="true"></i>
		</a>
	<?php
	} ?>

	<?php if ($twitter != '') {
		?>
		<a target="_blank" href="<?= $twitter ?>">
			<i class="fa fa-twitter-square fa-size wow bounceIn animated " data-wow-delay="1.5s" aria-hidden="true"></i>
		</a>
	<?php
	} ?>


	<?php if ($google != '') {
		?>
		<a target="_blank" href="<?= $google ?>">
			<i class="fa fa-google-plus-square fa-size wow bounceIn animated " data-wow-delay="1.5s" aria-hidden="true"></i>
		</a>

	<?php
	} ?>


</div>