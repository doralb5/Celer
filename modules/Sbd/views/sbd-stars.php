<div class="rate-it rate-cus">
	<?php
	$emptyStars = 5;
	if ($rating > 0) {
		$rating = number_format($rating, 2);
		$modulus = fmod($rating, 1);
		$modulus = number_format($modulus, 2);
		$fullStars = $rating - $modulus;
		$modulus = floatval($modulus);
		$emptyStars = 5 - $fullStars;
		for ($i = 0; $i < $fullStars; $i++) {
			?>
			<i class="fa fa-star star-on-png wow zoomIn animated  " data-wow-delay="0.5"></i>
			<?php
		}
		if ($modulus > 0) {
			$emptyStars = $emptyStars - 1; ?>
			<i class="fa fa-star-half-o star-on-png wow zoomIn animated  " data-wow-delay="0.5"></i>
			<?php
		}
	}
	for ($i = 0; $i < $emptyStars; $i++) {
		?>
		<i class="fa fa-star-o star-on-png wow zoomIn animated  " data-wow-delay="0.5"></i>
		<?php
	}
	?>
</div>
