<!-- Video-BG -->
<section id="bg-video" class="video-section">
    <div class="cover">
        <div class="hi">
            <!-- socials -->
            <div class="container">
                <div class="row">

					<?php
					$cols = 0;
					if ($facebook != null || $facebook != '') {
						$cols++;
					}
					if ($instagram != null || $instagram != '') {
						$cols++;
					}
					if ($linkedin != null || $linkedin != '') {
						$cols++;
					}
					if ($twitter != null || $twitter != '') {
						$cols++;
					}
					if ($google != null || $google != '') {
						$cols++;
					}

					switch ($cols) {
						case '0': $md = 2;
							break;
						case '1': $md = 2;
							break;
						case '2': $md = 2;
							break;
						case '3': $md = 4;
							break;
						case '4': $md = 6;
							break;
						default: $md = 6;
					}
					?>

                    <div class="col-md-<?= $md ?> col-md-offset-<?= (12 - $md) / 2 ?>">
                        <div class="at-grid" data-column="<?= $cols ?>">

							<?php if ($facebook != null || $facebook != '') {
						?>
								<div class="at-column">
									<div class="at-user">
										<div class="at-user__avatar"><a href="<?= $facebook ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
									</div>
								</div>
							<?php
					} ?>
							<?php if ($instagram != null || $instagram != '') {
						?>
								<div class="at-column">
									<div class="at-user">
										<div class="at-user__avatar"><a href="<?= $instagram ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></div>
									</div>
								</div>
							<?php
					} ?>
							<?php if ($twitter != null || $twitter != '') {
						?>
								<div class="at-column">
									<div class="at-user">
										<div class="at-user__avatar"><a href="<?= $twitter ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
									</div>
								</div>
							<?php
					} ?>
							<?php if ($linkedin != null || $linkedin != '') {
						?>
								<div class="at-column">
									<div class="at-user">
										<div class="at-user__avatar"><a href="<?= $linkedin ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></div>
									</div>
								</div>
							<?php
					} ?>
							<?php if ($google != null || $google != '') {
						?>
								<div class="at-column">
									<div class="at-user">
										<div class="at-user__avatar"><a href="<?= $google ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></div>
									</div>
								</div>
							<?php
					} ?>

                        </div>
                    </div>
                </div>
            </div>
            <!-- //team -->
        </div>
    </div>
    <div class="tv">
        <div class="screen mute" id="tv"></div>
    </div>
</section>
<script>
<?php
if ($youtube_link == '') {
						$youtube_link = 'YUU_MKTET_o';
					}
?>
    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/player_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var tv,
            playerDefaults = {
                autoplay: 1,
                autohide: 1,
                modestbranding: 0,
                rel: 0,
                showinfo: 0,
                controls: 0,
                disablekb: 1,
                enablejsapi: 0,
                iv_load_policy: 3
            };
    var vid = [{
            'videoId': "<?= $youtube_link ?>",
            'startSeconds': 0,
            'endSeconds': 50,
            'suggestedQuality': 'hd720'
        }],
            randomVid = Math.floor(Math.random() * vid.length),
            currVid = randomVid;

    $('.hi em:last-of-type').html(vid.length);

    function onYouTubePlayerAPIReady() {
        tv = new YT.Player('tv', {events: {'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange}, playerVars: playerDefaults});
    }

    function onPlayerReady() {
        tv.loadVideoById(vid[currVid]);
        tv.mute();
    }

    function onPlayerStateChange(e) {
        if (e.data === 1) {
            $('#tv').addClass('active');
            $('.hi em:nth-of-type(2)').html(currVid + 1);
        } else if (e.data === 2) {
            $('#tv').removeClass('active');
            if (currVid === vid.length - 1) {
                currVid = 0;
            } else {
                currVid++;
            }
            tv.loadVideoById(vid[currVid]);
            tv.seekTo(vid[currVid].startSeconds);
        }
    }

    function vidRescale() {

        var w = $(window).width() + 200,
                h = $(window).height() + 200;

        if (w / h > 16 / 9) {
            tv.setSize(w, w / 16 * 9);
            $('.tv .screen').css({'left': '0px'});
        } else {
            tv.setSize(h / 9 * 16, h);
            $('.tv .screen').css({'left': -($('.tv .screen').outerWidth() - w) / 2});
        }
    }

    $(window).on('load resize', function () {
        vidRescale();
    });

    $('.hi span:first-of-type').on('click', function () {
        $('#tv').toggleClass('mute');
        $('.hi em:first-of-type').toggleClass('hidden');
        if ($('#tv').hasClass('mute')) {
            tv.mute();
        } else {
            tv.unMute();
        }
    });

    $('.hi span:last-of-type').on('click', function () {
        $('.hi em:nth-of-type(2)').html('~');
        tv.pauseVideo();
    });
</script>