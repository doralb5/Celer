<?php

class ShareButtons extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'share-buttons';
		$this->view->render($view);
	}
}
