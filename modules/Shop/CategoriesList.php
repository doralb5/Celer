<?php

class CategoriesList extends BaseModule
{
	public function __construct($block_id, $position, $package)
	{
		parent::__construct($block_id, $position, $package);
		$this->model = Loader::getModel('Shop');
	}

	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 999999;
		$order = (isset($parameters['order'])) ? $parameters['order'] : 'category ASC';
		$categories = $this->model->getCategories($limit, 0, '', $order);

		$this->hierarchyCategorytems($categories);

		$this->view->set('categories', $categories);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'categories-list';
		$this->view->render($view);
	}

	private function hierarchyCategorytems(&$items)
	{
		foreach ($items as &$item) {
			$item->subitems = $this->model->getCategories(30, 0, "parent_id = {$item->id}");
			$this->hierarchyCategorytems($item->subitems);
		}
	}
}
