<?php

class LastProducts extends BaseModule
{
	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 16;
		$category_id = (isset($parameters['category_id'])) ? $parameters['category_id'] : '';
		$featured = (isset($parameters['featured'])) ? $parameters['featured'] : '';
		$order = (isset($parameters['order'])) ? $parameters['order'] : 'publish_date desc';
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'last-products';
		//$id = (isset($parameters['id'])) ? $parameters['id'] : '';
		$Products_Table = TABLE_PREFIX . 'shop_Product';

		$filter = "1 AND $Products_Table.enabled = '1'  AND $Products_Table.publish_date < NOW() ";

		if ($category_id != '') {
			$filter .= "AND $Products_Table.id_category = $category_id ";
		}

		if ($featured != '') {
			$filter .= "AND $Products_Table.featured = '$featured' ";
		}
		$shop_model = Loader::getModel('Shop');

		$i = 0;
		$features = $shop_model->getFeatures();
		$ft_filter = '';
		foreach ($features as $ft) {
			if (isset($_GET['ft_' . $ft->feature]) && $_GET['ft_' . $ft->feature] != '') {
				$value = $_GET['ft_' . $ft->feature];

				if ($i != 0) {
					$ft_filter .= 'OR';
				}

				$ft_filter .= " ($ProdFeat_Table.id_feature = {$ft->id} AND $ProdFeat_Table.value='$value') ";
				$i++;
			}
		}

		if ($ft_filter != '') {
			$filter .= "AND ( $ft_filter )";
		}

		//        $product =  $shop_model->getProduct($id);
//
//
		//         $features = $shop_model->getFeatures();
		//        foreach ($features as $feat) {
		//            $prodFeat =  $shop_model->getProdFeatures(20, 0, "id_product = $id AND id_feature = {$feat->id}");
		//            if (count($prodFeat)) {
		//                $feat->value = $prodFeat[0]->value;
		//            } else {
		//                $feat->value = null;
		//            }
		//        }
		//        $this->view->set('features', $features);
//
//
		$products = $shop_model->getList($limit, 0, $filter, $order);
        
		$this->view->set('features', $features);
		$this->view->set('parameters', $parameters);
		$this->view->set('products', $products);
		//   $this->view->set('product', $product);
		$this->view->render($view);
	}
}
