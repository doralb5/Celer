<?php

class ProductRequestInformation extends BaseModule
{
	public function execute($parameters = array())
	{
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'product-request-information';

		$shop_model = Loader::getModel('Shop');

		$id = Shop_Component::$currProduct;

		$product = $shop_model->getProduct($id);

		$this->view->set('product', $product);

		$url = 'Shop/send_email_request';

		$this->view->set('action_url', Utils::getComponentUrl($url));
		$this->view->set('id_prod', $id);
		$this->view->render($view);
	}
}
