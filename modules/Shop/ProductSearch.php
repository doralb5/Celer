<?php

class ProductSearch extends BaseModule
{
	public function __construct($block_id, $position, $package)
	{
		parent::__construct($block_id, $position, $package);
		$this->model = Loader::getModel('Shop');
	}

	public function execute($parameters = array())
	{
		$id_type = (isset($parameters['id_type'])) ? $parameters['id_type'] : 0;
		$this->view->set('action_url', Utils::getComponentUrl('Shop/products_list'));

		$types = $this->model->getTypes();
		$this->view->set('types', $types);

		$brands = $this->model->getBrands();
		$this->view->set('brands', $brands);

		$features = $this->model->getFeatures(50, 0, "id_type = $id_type");
		foreach ($features as &$feature) {
			$feature->options = $this->model->getFeatureOptions($feature->id);
		}

		$this->view->set('features', $features);
		$this->view->set('parameters', $parameters);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'products-search';
		$this->view->render($view);
	}
}
