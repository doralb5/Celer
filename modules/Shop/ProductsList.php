<?php

class ProductsList extends BaseModule
{
	public function __construct($block_id, $position, $package)
	{
		parent::__construct($block_id, $position, $package);
		$this->model = Loader::getModel('Shop');
	}

	public function execute($parameters = array())
	{
		$limit = (isset($parameters['limit'])) ? $parameters['limit'] : 10;
		$order = (isset($parameters['order'])) ? $parameters['order'] : '';
		$featured = (isset($parameters['featured'])) ? $parameters['featured'] : '';
		$id_category = (isset($parameters['id_category'])) ? $parameters['id_category'] : '';
		$id_type = (isset($parameters['id_type'])) ? $parameters['id_type'] : '';
		$id_brand = (isset($parameters['id_brand'])) ? $parameters['id_brand'] : '';

		if ($order == '') {
			$order = 'creation_date DESC';
		}

		$filter = '1 ';
		if ($featured != '') {
			$filter .= "AND featured = '{$featured}' ";
		}

		if ($id_category != '') {
			$filter .= "AND id_category = '{$id_category}' ";
		}

		if ($id_type != '') {
			$filter .= "AND id_type = '{$id_type}' ";
		}

		if ($id_brand != '') {
			$filter .= "AND id_brand = '{$id_brand}' ";
		}

		$products = $this->model->getList($limit, 0, $filter, $order);
		$this->view->set('parameters', $parameters);
		$this->view->set('products', $products);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'products-list';
		$this->view->render($view);
	}
}
