<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/categories-list.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/jquery-accordion-menu.css'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery-accordion-menu.js'); ?>
<?php HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.cookie.js'); ?>
<?php require_once LIBS_PATH . 'StringUtils.php'; ?>
<style>
    .content {
        width: 260px;
        margin: 100px auto;
    }
    .colors {
        width: 260px;
        float: left;
        margin: 20px auto;
    }
    .colors a {
        width: 43.3px;
        height: 30px;
        float: left;
    }
    .colors .blue {
        background: #4A89DC;
    }
    .colors .green {
        background: #03A678;
    }
    .colors .red {
        background: #ED5565;
    }
    .colors .white {
        background: #fff;
    }
    .colors .black {
        background: #292929;
    }
    .jquery-accordion-menu {
        margin-bottom: 20px;
        float: none;
    }
    .jquery-accordion-menu ul li {
        float: none;
        display: inline-block;
    }
    .jquery-accordion-menu>ul>li.active>a,
    .jquery-accordion-menu>ul>li:hover>a {
        border-bottom-color: #111;
        color: #111;
        background: #fff;
    }
    .jquery-accordion-menu>ul>li.active>a .submenu-indicator,
    .jquery-accordion-menu>ul>li:hover>a .submenu-indicator {
        color: #111;
    }
    .jquery-accordion-menu ul li a.active .submenu-indicator {
        color: #111;
    }
    .jquery-accordion-menu .submenu-indicator {
        color: #ddd;
        transition: color .2s ease-in-out;
        font-size: 25px;
    }
    .jquery-accordion-menu ul ul.submenu>li>a {
        background: #fff;
        font-size: 14px;
    }
    .jquery-accordion-menu ul ul.submenu>li>a:hover {
        border-bottom-color: #111;
        color: #111;
    }
    .jquery-accordion-menu ul li>a.active .submenu-indicator {
        color: #111;
    }
    .jquery-accordion-menu ul li>a.active {
        background-color: #fff !important;
        border-bottom-color: #111;
    }
    .jquery-accordion-menu ul ul.submenu>li>a {
        padding-left: 50px;
    }
    .jquery-accordion-menu ul ul.submenu>li>ul.submenu>li>a {
        padding-left: 100px;
    }
    .jquery-accordion-menu ul li a {
        background: #fff;
        padding-top: 13px;
        padding-left: 0;
        padding-bottom: 13px;
        font-size: 15px;
        color: #111;
        border-bottom: solid 1px #ddd;
        transition: border-bottom-color .2s ease-in-out;
        white-space: normal;
        display: inline-block;
        float: none;
    }
    .jquery-accordion-menu>ul>li:last-child>a {
        border-bottom: none;
    }
    .jquery-accordion-menu ul ul.submenu li:hover > a {
        border-left-color: #fff;
    }
    .jquery-accordion-menu ul ul.submenu li > a {
        border-left-color: #fff;
    }
    .jquery-accordion-menu ul li a.active {
        background-color: #fff;
        color: #111;
    }
</style>

<div class="category-list">
    <h4>[$CategoryList]<i class="fa fa-chevron-down pull-right visible-xs visible-sm" aria-hidden="true"></i></h4>
</div>
<div id="collapse-category-list">
    <div id="jquery-accordion-menu" class="jquery-accordion-menu">
        <ul>
			<?php printRecursiveCategoryItems($categories); ?>
        </ul>  
    </div>
</div>    
<?php

function printRecursiveCategoryItems($items, $parent_id = null)
{
	foreach ($items as $item) {
		if (!is_null($parent_id) && $item->parent_id == $parent_id || is_null($parent_id) && is_null($item->parent_id)) {
			$target = getTargetCatByItem($item);
			$active_class = (isActiveRecursivelyCat($item)) ? 'active' : '';

			echo "<li id='{$item->id}' class=\"$active_class\">";
			if (count($item->subitems) == 0) {
				echo "<a href=\"$target \" class=\"$active_class\">{$item->category}</a>";
			} else {
				echo "<a href=\"$target\" class=\"\" >{$item->category}</a>\n\n";
				echo "<ul class=\"submenu\">\n";
				printRecursiveCategoryItems($item->subitems, $item->id);
				echo "</ul>\n\n";
			}
			echo "</li>\n";
		}
	}
}

function isActiveRecursivelyCat($item)
{
	$target = Utils::getComponentUrl('Shop/products_list/' . Utils::url_slug($item->category) . '-' . $item->id);
	$active = Utils::isActivePage($target);
	if ($active) {
		return true;
	}
	if (count($item->subitems)) {
		foreach ($item->subitems as $subitem) {
			$active = isActiveRecursivelyCat($subitem);
			if ($active) {
				return true;
			}
		}
	}
	return false;
}

function getTargetCatByItem($item)
{
	$target = Utils::getComponentUrl('Shop/products_list/' . Utils::url_slug($item->category) . '-' . $item->id);
	return $target;
}
?>

<script type="text/javascript">
    jQuery(document).ready(function () {
        if ($(window).width() < 992) {
            $('#collapse-category-list').addClass('collapse');
            $('.category-list h4, .category-list i').attr({'data-toggle': 'collapse', 'data-target': '#collapse-category-list'});
        }
        $("#collapse-category-list").on("hide.bs.collapse", function () {
            $('.category-list i').removeClass('fa-chevron-up');
            $('.category-list i').addClass('fa-chevron-down');
        });
        $("#collapse-category-list").on("show.bs.collapse", function () {
            $('.category-list i').removeClass('fa-chevron-down');
            $('.category-list i').addClass('fa-chevron-up');
        });
        jQuery("#jquery-accordion-menu").jqueryAccordionMenu();
        jQuery(".colors a").click(function () {
            if ($(this).attr("class") != "default") {
                $("#jquery-accordion-menu").removeClass();
                $("#jquery-accordion-menu").addClass("jquery-accordion-menu").addClass($(this).attr("class"));
            } else {
                $("#jquery-accordion-menu").removeClass();
                $("#jquery-accordion-menu").addClass("jquery-accordion-menu");
            }
        });
    });
</script>
<script>
    function setPatCookie(PatCookieVal) {
        $.cookie('PatCookieName', PatCookieVal, {
            expires: 365,
            path: '/'
        });
    }

    $(".jquery-accordion-menu ul li a").click(function () {
        var PatCookieVal = $(this).parent().attr('id');
        $(this).siblings('li').removeClass('active');
        $(this).addClass('active');
        setPatCookie(PatCookieVal);
    });

    $(document).ready(function () {
        $(".jquery-accordion-menu ul li").each(function () {
            var id_of_class = $(this).attr('id');
            if ($.cookie('PatCookieName') == id_of_class) {
                $(this).children('a').addClass('active submenu-indicator-minus');
                $(this).parents('.submenu').css("display", "block")
                if ($(this).children('a').attr("class") != "default") {
                    $(this).parents('*[class=""]').children('a').addClass('active submenu-indicator-minus');
                    $(this).children('a').removeClass('active submenu-indicator-minus');
                }

            } else {
                $(this).removeClass('active submenu-indicator-minus');
            }
        });

    });
</script>