<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/last-products.css'); ?>
<?php
require_once DOCROOT . LIBS_PATH . 'StringUtils.php';
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 3;
$col_size = 12 / $cols;
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 500;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 150;

$items = array();
foreach ($products as $pro) {
	ob_start();
	include 'single-product.php';
	$items[] = ob_get_clean();
}
?>

<section id="last-products">
	<?php include DOCROOT . VIEWS_PATH . 'layouts/' . 'Carousel_view.php'; ?>
</section>