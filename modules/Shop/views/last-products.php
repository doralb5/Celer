<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/last-products.css'); ?>
<?php require_once LIBS_PATH . 'StringUtils.php'; ?>

<?php $w = (isset($parameters['w'])) ? $parameters['w'] : 800; ?>
<?php $h = (isset($parameters['h'])) ? $parameters['h'] : 500; ?>
<?php $taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30; ?>
<?php
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 3;
$col_size = 12 / $cols;
?>

<main id="last-products">
    <div class="row">
		<?php
		$i = 0;
		foreach ($products as $pro) {
			?>

			<?php if ($i % $cols == 0 && $i != 0) {
				?>
				<div class="clearfix"></div>
			<?php
			} ?>

			<div class="col-md-<?= $col_size ?>">
				<div class="product-card">
					<h4 class="title">
						<?= StringUtils::CutString(strip_tags($pro->name), $taglia_title) ?>
					</h4>
					<!-- IMAGE -->
					<div class="img-wrapper">
						<a href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>" title="">


							<?php if ($pro->image != '') {
				?>
								<img src="<?= Utils::genThumbnailUrl('products/' . $pro->image, $w, $h, array('zc' => '1')) ?>" alt="">

							<?php
			} else {
				?>

								<img src="<?= Utils::genThumbnailUrl('products/' . 'no-thumb.png', $w, $h, array('zc' => '1')) ?>" alt="">

							<?php
			} ?>

						</a>
					</div>

					<!-- CARD FRONT CONTENT -->
					<div class="details">

						<div class="detail">
							<span class="feature">[$category]</span>
							<span class="category"><a href="<?= Utils::getComponentUrl('Shop/products_list/' . Utils::url_slug($pro->category) . '-' . $pro->id_category) ?>"><?= $pro->category ?></a></span>
						</div>

						<div class="detail">
							<span class="feature">[$price]</span>
							<span class="price">€ <?php
								if ($pro->final_price != 0) {
									echo number_format($pro->final_price, 0, ',', '.');
								} else {
									echo number_format($pro->price, 0, ',', '.');
								} ?></span>
						</div>

						<!--                        <div class="detail">
						<?php foreach ($features as $ft) {
									?>
															<li><?= ucfirst($ft->feature) ?>: <?= $ft->value ?></li><br>
	<?php
								} ?>
												</div>-->


						<div class="footer text-center">
							<a class="btn btn-sm btn-danger btn-readall" href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>">[$more_details]</a>
						</div>
					</div>
				</div>
			</div>

<?php
		} ?>
    </div>
</main>
