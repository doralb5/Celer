<?php
$name = (isset($_POST['firstname'])) ? $_POST['firstname'] : '';
$lastname = (isset($_POST['lastname'])) ? $_POST['lastname'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
$tel = (isset($_POST['tel'])) ? $_POST['tel'] : '';
$id_prod = (isset($_POST['productid'])) ? $_POST['productid'] : $id_prod;
?>


<div class="row">

    <form method="post" action="<?= $action_url ?>" enctype="multipart/form-data" >

        <input type="hidden" name="productid"  value="<?= $id_prod ?>">

        <div id="contact-form" class="form-container" data-form-container>
            <div class="row">
                <div class="form-title">
                    <span>Richiedi Informazioni</span>
                </div>
            </div>
            <div class="input-container">
                <div class="row">
                    <span class="req-input" >
                        <span class="input-status" data-toggle="tooltip" data-placement="top" title="Nome"> </span>
                        <input type="text" name="name" value="<?= $name ?>" data-min-length="8" placeholder="Nome" required="">
                    </span>
                </div>

                <div class="row">
                    <span class="req-input" >
                        <span class="input-status" data-toggle="tooltip" data-placement="top" title="Cognome"> </span>
                        <input type="text" name="lastname" value="<?= $lastname ?>" data-min-length="8" placeholder="Cognome" required="">
                    </span>
                </div>
                <div class="row">
                    <span class="req-input">
                        <span class="input-status" data-toggle="tooltip" data-placement="top" title="eMail"> </span>
                        <input type="email" name ="email" value="<?= $email ?>" placeholder="eMail" required="" >
                    </span>
                </div>
                <div class="row">
                    <span class="req-input">
                        <span class="input-status" data-toggle="tooltip" data-placement="top" title="Numero di telefono"> </span>
                        <input type="tel" name="tel" value="<?= $tel ?>"placeholder="Telefono" required="">
                    </span>
                </div>
                <div class="row">
                    <span class="req-input message-box">
                        <span class="input-status" data-toggle="tooltip" data-placement="top" title="Scrivi un messaggio"> </span>
                        <textarea type="textarea" name="message"  data-min-length="10" placeholder="Messaggio"></textarea>
                </div>
                <div class="row submit-row">
                    <button type="submit" name="send" class="btn btn-block submit-form">INVIA RICHIESTA</button>
                </div>
            </div>
        </div>


    </form>
</div>

<!--New Form -->



<style>

    .form-container .req-input .input-status {
        display: inline-block;
        height: 40px;
        width: 40px;
        float: left;	
    }

    .form-container .input-status::before{
        content: " ";
        height:20px;
        width:20px;
        position:absolute;
        top:10px;
        left:10px;
        color:white;
        border-radius:50%;
        background:white;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;

    }

    .form-container .input-status::after{
        content: " ";
        height:10px;
        width:10px;
        position:absolute;
        top:15px;
        left:15px;
        color:white;
        border-radius:50%;
        background:#be1e2d;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
    }

    .form-container .req-input{
        width:100%;
        float:left;
        position:relative;
        background: white;
        height:40px;
        display:inline-block;
        border-radius:0px;
        margin:5px 0px;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
    }

    .form-container div .row .invalid:hover{
        background:#EF9A9A;
    }

    .form-container .invalid .input-status:after {
        width:20px;
        height:4px;
        background:white;
        border-radius:0px;
        top:19px;
        left:10px;
        -ms-transform: rotate(-45deg); /* IE 9 */
        -webkit-transform: rotate(-45deg); /* Chrome, Safari, Opera */
        transform: rotate(-45deg);
    }

    .form-container div .row  .valid:hover{
        background:#A5D6A7;
    }

    .form-container div .row .valid {
        background:#81C784;

    }

    .form-container .valid .input-status:after {
        border-radius:0px;
        width: 17px;
        height: 4px;
        background: white;
        top: 16px;
        left: 15px;
        -ms-transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        transform: rotate(-45deg);
    }

    .form-container .valid .input-status:before {
        border-radius:0px;
        width: 11px;
        height: 4px;
        background:white;
        top: 19px;
        left: 10px;
        -ms-transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    .form-container .input-container{
        padding:0px 20px;
    }

    .form-container .row-input{
        padding:0px 5px;
    }

    .form-container .req-input.input-password{
        margin-bottom:0px;
    }
    .form-container .req-input.confirm-password{
        margin-top:0px;
    }

    .form-container {
        margin-bottom:20px;
        margin-left:20px;
        margin-right:20px;
        margin-top:0px;
        padding:20px;
        border-radius:0px;
        background:rgb(190, 30, 45);
        color:white;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
    }

    .form-container .form-title{
        font-size:25px;
        color:inherit;
        text-align:center;
        margin-bottom:10px;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
    }

    .form-container .submit-row{
        padding:0px 0px;
    }

    .form-container .btn.submit-form{
        margin-top:15px;
        padding:12px;
        background : #000000;
        color:white;
        border-radius:0px;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
    }

    .form-container .btn.submit-form:focus{
        outline:0px;
        color:white;
    }

    .form-container .btn.submit-form:hover{
        background:#5f5758;
        color:white;
    }

    .form-container .tooltip.top .tooltip-arrow {
        border-top-color:#00BCD4 !important;
    }

    .form-container .tooltip.top.tooltip-invalid .tooltip-arrow {
        border-top-color:#E57373 !important;
    }

    .form-container .tooltip.top.tooltip-invalid .tooltip-inner::before{
        background:#E57373;
    }
    .form-container .tooltip.top.tooltip-invalid .tooltip-inner{
        background:#FFEBEE !important;
        color:#E57373;
    }

    .form-container .tooltip.top.tooltip-valid .tooltip-arrow {
        border-top-color:#81C784 !important;
    }

    .form-container .tooltip.top.tooltip-valid .tooltip-inner::before{
        background:#81C784;
    }
    .form-container .tooltip.top.tooltip-valid .tooltip-inner{
        background:#E8F5E9 !important;
        color:#81C784;
    }

    .form-container .tooltip.top .tooltip-inner::before{
        content:" ";
        width:100%;
        height:6px;
        background:#00BCD4;
        position:absolute;
        bottom:5px;
        right:0px;
    }
    .form-container .tooltip.top .tooltip-inner{
        border:0px solid #00BCD4;
        background:#E0F7FA !important;
        color:#00ACC1;
        font-weight:bold;
        font-size:13px;
        border-radius:0px;
        padding:10px 15px;
    }
    .form-container .tooltip {
        max-width:150px;
        opacity:1 !important;
    }

    .form-container .message-box{
        width:100%;
        height:auto;
    }

    .form-container textarea:focus,.form-container textarea:hover{
        background:#fff;
        outline:none;
        border:0px;
    }

    .form-container .req-input textarea {
        max-width:calc(100% - 50px);
        width: 100%;
        height: 80px;
        border: 0px;
        color: #777;
        padding: 10px 9px 0px 9px;
        float:left;

    }
    .form-container input[type=text]:focus, .form-container input[type=password]:focus, .form-container input[type=email]:focus, .form-container input[type=tel]:focus, .form-container select{
        background:#fff;
        color:#777;
        border-left:0px;
        outline:none;
    }

    .form-container input[type=text]:hover,.form-container input[type=password]:hover,.form-container input[type=email]:hover,.form-container input[type=tel]:hover, . form-container select{
        background:#fff;
    }

    .form-container input[type=text], .form-container input[type=password], .form-container input[type=email],input[type=tel], form-container select{
        width:calc(100% - 50px);
        float:left;
        border-radius:0px;
        border:0px solid #ddd;
        padding:0px 9px;
        height:40px;
        line-height:40px;
        color:#777;
        background:#fff;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
    }

</style>
