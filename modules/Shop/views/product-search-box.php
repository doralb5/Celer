<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/product.css');

$id_brand = isset($_GET['id_brand']) ? $_GET['id_brand'] : '';
?>

<div class="product-searchbox-container">
    <form action="<?= $action_url ?>" method="get">
        <input class="search-input" name="query" type="text" placeholder="[$Search]..."/>
    </form>
</div>