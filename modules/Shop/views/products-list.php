<?php HeadHTML::AddStylesheet(WEBROOT . DATA_DIR . $this->view_path . 'css/icon-font.css'); ?>
<?php HeadHTML::AddStylesheet(WEBROOT . DATA_DIR . $this->view_path . 'css/shop-style.css'); ?>


<style>

    @media (min-width: 767px) {
        .lastcompany .thumbnail {
        }
    }

    .lastcompany .thumbnail {
        padding: 0px;
        margin-bottom: 0px;
        border: 0px solid white;
        border-radius: 0px;
    }

    .lastcompany #companyCarousel {
        margin-right: 0px;
    }

    ul.thumbnails li {
        margin-bottom: 0px;
    }

    @media (max-width: 768px) {
        .lastcompany ul.thumbnails {
            padding-left: 0px;
        }

        .lastcompany #companyCarousel {
            margin-right: 0px;
        }

        .lastcompany .well {
            padding: 0px;
        }
    }

    .carousel-control {
        background: none !important;
        color: #CACACA;
        font-size: 2.3em;
        text-shadow: none;
        margin-top: 0px;
        width: 5%;
    }

    .carousel-control.left i {
        margin-left: -65px;
    }

    .carousel-control.right i {
        margin-right: -65px;
    }

    @media (max-width: 768px) {
        .carousel-control.left i, .carousel-control.right i {
            display: none;
        }
    }

    ul.pager {
        display: none;
    }

    ul {
        list-style-type: none;
        -webkit-padding-start: 0px;
    }

    .button_align {
        text-align: center;
    }

    .fff {
        margin: 30px 0;
    }

    .product-list {
        box-shadow: 0 0px 1px 0px rgba(158, 36, 48, 0.85);
        overflow: hidden;
        display: block;
        position: relative;
        z-index: 2;
        margin: 0 0 10px 0;
        border: 1px solid #ab888c;
        border-radius: 2px;
    }
</style>


<!--NEW-->

<?php $w = (isset($parameters['w'])) ? $parameters['w'] : 100; ?>
<?php $h = (isset($parameters['h'])) ? $parameters['h'] : 100; ?>
<?php $cols = (isset($parameters['cols'])) ? $parameters['cols'] : 4; ?>

<div class="product_list">
    <div class="row">
        <div class="col-md-12">
            <ul class="product-list-ul">
				<?php
				foreach ($products as $pro) {
					?>
					<li class="product-list-li">
						<div class="row">
							<div class="product-list">
								<div class="col-md-3">
									<?php if ($pro->image != '') {
						?>
										<a href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>"
										   title="">
											<img
												src="<?= Utils::genThumbnailUrl('products/' . $pro->image, $w, $h, array('zc' => '1')) ?>"
												alt="">
										</a>
									<?php
					} ?>
								</div>
								<div class="col-md-4">
									<h5 class="main-title1">
										<a href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>"
										   title="">
											<b>   <?= $pro->name ?> </b>
										</a>
										<br>
										<br>
										<?php
										for ($j = 0; $j < count($pro->id_category); $j++) {
											if ($j > 0) {
												break;
											}
											if (!is_null($pro->category) && $pro->category != '') {
												?>
												<a href="<?=
												Utils::getComponentUrl('Shop/products_list/'
														. '' . Utils::url_slug($pro->category) . '-' . $pro->id_category)
												?>">
													   <?= $pro->category; ?>
												</a>
											<?php
											} ?>

										<?php
										} ?>

								</div>
								<div class="col-md-5">
									<h5 class="main-title1">
										<?= $pro->price ?> €
									</h5>
									<br>
									<h5 class="main-title1" style="float:right">
										<a href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>">
											Maggiori dettagli</a>
									</h5>
								</div>
							</div>
						</div>
					</li>
				<?php
				} ?>
            </ul>
        </div>
    </div>
</div>
