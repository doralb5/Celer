<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/icon-font.css'); ?>
<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/shop-style.css');

$id_brand = isset($_GET['id_brand']) ? $_GET['id_brand'] : '';
$id_model = isset($_GET['id_model']) ? $_GET['id_model'] : '';
$status = (isset($_GET['status'])) ? $_GET['status'] : '';

$show_status = (isset($parameters['status'])) ? $parameters['status'] : 0;
?>
<div id="sidebar">
    <div class="sidebar-content">
        <div class="head-side-bar">
            <h4>[$SearchProduct]</h4>
        </div>
        <form id="search" action="<?= $action_url ?>" class="bluepanel form-inline" role="form" method="get">
            <div class="search-form">
				<?php if ($show_status) {
	?>
					<div class="select">
						<select name="status" id="status">
							<option value="">[$SelectCarStatus]</option>
							<option value="new" class="clik" <?= ($status == 'new') ? 'selected' : '' ?>>[$New]</option>
							<option value="used" class="clik" <?= ($status == 'used') ? 'selected' : '' ?>>[$Used]
							</option>
						</select>
					</div>
				<?php
} ?>

                <div class="select">
                    <select name="id_brand" id="id_brand" onchange="refreshModelsList();">
                        <option value="">[$SelectCarType]</option>
						<?php foreach ($brands as $br) {
		?>
							<option value="<?= $br->id ?>"
									class="clik" <?= ($id_brand == $br->id) ? 'selected' : '' ?>><?= $br->brand ?> </option>
								<?php
	} ?>
                    </select>
                </div>
                <div class="select">
                    <select name="id_model" id="id_model">
                        <option value="">[$SelectCarModel]</option>
                    </select>
                </div>
				<?php foreach ($features as $feature) {
		?>
					<div class="select">
						<select name="ft_<?= $feature->feature ?>" id="types">
							<option value="">[$<?= ucfirst($feature->feature) ?>]</option>
							<?php foreach ($feature->options as $opt) {
			?>
								<option value="<?= $opt->value ?>"
										class="clik" <?= (isset($_GET['ft_' . $feature->feature]) && $_GET['ft_' . $feature->feature] == $opt->value) ? 'selected' : ''; ?>><?= $opt->value ?></option>
									<?php
		} ?>
						</select>
					</div>
				<?php
	} ?>
                <div>
                    <button type="submit" name="search" class="submit-button">[$SearchNow]</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>

    function refreshModelsList() {
        $.ajax({
            url: "<?= Utils::getComponentUrl('Shop/ajx_getModels') ?>/" + $('#id_brand').val(),
            dataType: "json",
            success: function (data) {
                var options, index, select, option;
                select = document.getElementById('id_model');
                select.options.length = 0;
                options = data.options;
                select.options.add(new Option("[$SelectCarModel]", ""));
                for (index = 0; index < options.length; ++index) {
                    option = options[index];
                    select.options.add(new Option(option.text, option.value));
                }
                $('#id_model').val('<?= $id_model ?>');
            },
            error: function (e) {
                select = document.getElementById('id_model');
                select.options.length = 0;
                select.options.add(new Option('[$SelectCarModel]', ''));
                console.log('Error: ' + e);
            }
        });
    }
<?php if ($id_brand != '') {
		?>
	    refreshModelsList();
<?php
	} ?>
</script>