<div class="product-card">
    <h4 class="title">
		<?= StringUtils::CutString(strip_tags($pro->name), $taglia_title) ?>
    </h4>
    <!-- IMAGE -->
    <div class="img-wrapper">
        <a href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>" title="">


			<?php if ($pro->image != '') {
	?>
				<img src="<?= Utils::genThumbnailUrl('products/' . $pro->image, $w, $h, array('zc' => '1')) ?>" alt="">

			<?php
} else {
		?>

				<img src="<?= Utils::genThumbnailUrl('products/' . 'no-thumb.png', $w, $h, array('zc' => '1')) ?>" alt="">

			<?php
	} ?>

        </a>
    </div>

    <!-- CARD FRONT CONTENT -->
    <div class="details">

        <div class="detail">
            <span class="category"><a href="<?= Utils::getComponentUrl('Shop/products_list/' . Utils::url_slug($pro->category) . '-' . $pro->id_category) ?>"><?= $pro->category ?></a></span>
			<?php
			if ($pro->final_price != 0) {
				$price = Utils::price_format($pro->final_price, 'EUR');
			} else {
				$price = Utils::price_format($pro->price, 'EUR');
			}
			?>
            <span class="price"><?= ($price > 0) ? $price : '' ?></span>
        </div>

        <!--                        <div class="detail">
		<?php foreach ($features as $ft) {
				?>
	                        <li><?= ucfirst($ft->feature) ?>: <?= $ft->value ?></li><br>
		<?php
			} ?>
                </div>-->


        <div class="footer text-center">
            <a class="btn btn-sm btn-default btn-readall" href="<?= Utils::getComponentUrl('Shop/show_product/' . Utils::url_slug($pro->name . '-' . $pro->id)) ?>">[$more_details]</a>
        </div>
    </div>
</div>