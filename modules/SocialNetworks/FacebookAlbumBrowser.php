<?php

class FacebookAlbumBrowser extends BaseModule
{
	public function execute($parameters = array())
	{
		$account = isset($parameters['account']) ? $parameters['account'] : null;
		$only_album = isset($parameters['only_album']) ? $parameters['only_album'] : '';

		if ($account != '') {
			$this->view->set('account', $account);
			$this->view->set('thumbnailSize', (isset($parameters['thumbnailSize']) ? $parameters['thumbnailSize'] : '210'));
			$this->view->set('lightbox', (isset($parameters['lightbox']) ? $parameters['lightbox'] : 'true'));
			$this->view->set('comment_nr', (isset($parameters['comment_nr']) ? $parameters['comment_nr'] : '2'));
			$this->view->set('only_album', $only_album);

			//Placeholders
			$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

			$this->view->set('parameters', $parameters);

			$view = (isset($parameters['view'])) ? $parameters['view'] : 'single-album';
			$this->view->render($view);
		} else {
			echo 'Url Facebook Albums NOT DEFINED!';
		}
	}
}
