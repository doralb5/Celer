<?php

class FacebookComments extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'facebook-comments';
		$this->view->render($view);
	}
}
