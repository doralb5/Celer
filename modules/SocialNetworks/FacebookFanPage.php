<?php

class FacebookFanPage extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'facebook-fanpage';
		$this->view->render($view);
	}
}
