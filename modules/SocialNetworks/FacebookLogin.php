<?php

class FacebookLogin extends BaseModule
{
	public function execute($parameters = array())
	{
		$app_id = isset($parameters['app_id']) ? $parameters['app_id'] : '1726438044257214';
		$app_secret = isset($parameters['app_secret']) ? $parameters['app_secret'] : '1a0291be854de5f79a2cf865f70d1e5c';

		$fb = new Facebook\Facebook(array(
			'app_id' => "$app_id",
			'app_secret' => "$app_secret",
			'default_graph_version' => 'v2.2',
		));

		$helper = $fb->getRedirectLoginHelper();

		$permissions = array('email', 'public_profile');

		$callback_url = Utils::getComponentUrl('Facebook/facebook_callback');
		$loginUrl = $helper->getLoginUrl($callback_url, $permissions);

		$this->view->set('loginUrl', htmlspecialchars($loginUrl));

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);

		$this->view->set('parameters', $parameters);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-fb-login';
		$this->view->render($view);
	}
}
