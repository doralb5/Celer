<?php

class FacebookPixelCode extends BaseModule
{
	public function execute($parameters = array())
	{
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'facebook-pixel-code';
		$this->view->render($view);
	}
}
