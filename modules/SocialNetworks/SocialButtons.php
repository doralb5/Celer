<?php

class SocialButtons extends BaseModule
{
	public function execute($parameters = array())
	{
		$SocialButtons = array();
		$exclude = array('view');

		foreach ($parameters as $p => $v) {
			if (!in_array($p, $exclude)) {
				if (substr($v, 0, 7) != 'http://' && substr($v, 0, 8) != 'https://') {
					$v = 'http://' . $v;
				}

				$label = '';
				switch ($p) {
					case 'facebook':
						$label = 'Facebook';
						$class = 'icoFacebook';
						break;
					case 'twitter':
						$label = 'Twitter';
						$class = 'icoTwitter';
						break;
					case 'google-plus':
						$label = 'Google +';
						$class = 'icoGoogle';
						break;
					case 'linkedin':
						$label = 'LinkedIn';
						$class = 'icoLinkedin';
						break;
					case 'instagram':
						$label = 'Instagram';
						$class = 'icoInstagram';
						break;
					case 'pinterest':
						$label = 'Pinterest';
						$class = 'icoPinterest';
						break;
					case 'flickr':
						$label = 'Flickr';
						$class = 'icoFlickr';
						break;
					case 'skype':
						$label = 'Skype';
						$class = 'icoSkype';
						break;
					case 'tripadvisor':
						$label = 'Tripadvisor';
						$class = 'icoTripadvisor';
						break;
					case 'tumblr':
						$label = 'Tumblr';
						$class = 'icoTumblr';
						break;
					case 'youtube':
						$label = 'Youtube';
						$class = 'icoYoutube';
						break;
					case 'vimeo':
						$label = 'Vimeo';
						$class = 'icoVimeo';
						break;
					case 'envelope':
						$label = 'Mail';
						$class = 'icoMail';
						break;
					default:
						$label = '';
						$class = '';
						break;
				}

				array_push($SocialButtons, array('link' => $v, 'icon' => $p, 'label' => $label, 'class' => $class));
			}
		}

		$this->view->set('SocialButtons', $SocialButtons);
		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-social-buttons';
		$this->view->render($view);
	}
}
