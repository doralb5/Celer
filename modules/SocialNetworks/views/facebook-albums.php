<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/jquery.fb.albumbrowser.css');
HeadHTML::AddJS(WEBROOT . $this->view_path . 'js/jquery.fb.albumbrowser.js');

$account = isset($parameters['account']) ? $parameters['account'] : null;
$onlyAlbum = isset($parameters['onlyAlbum']) ? $parameters['onlyAlbum'] : null;
$showComments = isset($parameters['showComments']) ? $parameters['showComments'] : true;
$commentsLimit = isset($parameters['commentsLimit']) ? $parameters['commentsLimit'] : 4;
$showAccountInfo = isset($parameters['showAccountInfo']) ? $parameters['showAccountInfo'] : true;
$showImageCount = isset($parameters['showImageCount']) ? $parameters['showImageCount'] : true;
$showImageText = isset($parameters['showImageText']) ? $parameters['showImageText'] : true;
$lightbox = isset($parameters['lightbox']) ? $parameters['lightbox'] : true;
$likeButton = isset($parameters['likeButton']) ? $parameters['likeButton'] : true;
$shareButton = isset($parameters['shareButton']) ? $parameters['shareButton'] : true;
$thumbnailSize = isset($parameters['thumbnailSize']) ? $parameters['thumbnailSize'] : true;
?>

<?php if (!is_null($account)) {
	?>
	<div class="fb-album-container"></div>
	<script type="text/javascript">
		$(document).ready(function () {
			$(".fb-album-container").FacebookAlbumBrowser({
				account: "<?= $account ?>",
				accessToken: "775908159169504|cYEIsh0rs25OQQC8Ex2hXyCOut4",
				onlyAlbum: "<?= $onlyAlbum ?>",
				showComments: <?= $showComments ?>,
				commentsLimit: <?= $commentsLimit ?>,
				showAccountInfo: <?= $showAccountInfo ?>,
				showImageCount: <?= $showImageCount ?>,
				showImageText: <?= $showImageText ?>,
				skipEmptyAlbums: true,
				skipAlbums: ["Profile Pictures", "Timeline Photos"],
				thumbnailSize: <?= $thumbnailSize ?>,
				albumsPageSize: 0,
				photosPageSize: 0,
				lightbox: <?= $lightbox ?>,
				photosCheckbox: false,
				pluginImagesPath: "<?= WEBROOT . $this->view_path . 'img' ?>",
				likeButton: <?= $likeButton ?>,
				shareButton: <?= $shareButton ?>,
				addThis: "ra-52638e915dd79612",
				photoChecked: function (photo) {
					//console.log("PHOTO CHECKED: " + photo.id + " - " + photo.url + " - " + photo.thumb);
					//console.log("CHECKED PHOTOS COUNT: " + this.checkedPhotos.length);
				},
				photoUnchecked: function (photo) {
					//console.log("PHOTO UNCHECKED: " + photo.id + " - " + photo.url + " - " + photo.thumb);
					//console.log("CHECKED PHOTOS COUNT: " + this.checkedPhotos.length);
				},
				albumSelected: function (photo) {
					//console.log("ALBUM CLICK: " + photo.id + " - " + photo.url + " - " + photo.thumb);
				},
				photoSelected: function (photo) {
					//console.log("PHOTO CLICK: " + photo.id + " - " + photo.url + " - " + photo.thumb);
				}
			});

		});
	</script>
<?php
} else {
		?>
	<script>
		console.log("You have to set an account to show the facebook album.");
	</script>
<?php
	} ?>
