<?php
$script = '<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/sq_AL/sdk.js#xfbml=1&version=v2.6&appId=103033870062953";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, \'script\', \'facebook-jssdk\'));</script>';

HeadHTML::addScript($script);

$href = (isset($parameters['href'])) ? $parameters['href'] : null;
$width = (isset($parameters['width'])) ? $parameters['width'] : null;
$class = (isset($parameters['class'])) ? $parameters['class'] : '';
$numposts = (isset($parameters['numposts'])) ? $parameters['numposts'] : 4;
?>

<?php if (!is_null($href)) {
	?>

	<div class="fb-comments <?= $class ?>" data-href="<?= $href ?>" data-numposts="<?= $numposts ?>" data-width="<?= $width ?>"></div>

<?php
} else {
		?>
	<script>
		console.error("You have to define a url for facebook comments.");
	</script>
<?php
	} ?>
