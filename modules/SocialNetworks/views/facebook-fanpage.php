<?php
$href = (isset($parameters['data-href'])) ? $parameters['data-href'] : null;
$width = (isset($parameters['data-width'])) ? $parameters['data-width'] : '';
$height = (isset($parameters['data-height'])) ? $parameters['data-height'] : 0;
$tabs = (isset($parameters['data-tabs'])) ? $parameters['data-tabs'] : '';
$small_header = (isset($parameters['data-small-header']) && $parameters['data-small-header'] == '0') ? false : true;
$adapt_container_width = (isset($parameters['data-adapt-container-width']) && $parameters['data-adapt-container-width'] == '0') ? false : true;
$hide_cover = (isset($parameters['data-hide-cover']) && $parameters['data-hide-cover'] == '0') ? false : true;
$show_facepile = (isset($parameters['data-show-facepile']) && $parameters['data-show-facepile'] == '0') ? false : true;
$language = (isset($parameters['language'])) ? $parameters['language'] : 'en_US';
?>

<?php if (!is_null($href)) {
	?>

	<div class="fb-page pleft wow fadeInLeft animated" data-wow-delay="1.1s"
		 data-href="<?= $href ?>"
		 <?php if ($width != '') {
		?>data-width="<?= $width ?>"<?php
	} ?>
		 <?php if ($height > 0) {
		?>data-height="<?= $height ?>"<?php
	} ?>
		 <?php if ($tabs != '') {
		?>data-tabs="<?= $tabs ?>"<?php
	} ?>
		 data-small-header="<?= $small_header ?>"
		 data-adapt-container-width="<?= $adapt_container_width ?>"
		 data-hide-cover="<?= $hide_cover ?>"
		 data-show-facepile="<?= $show_facepile ?>">
	</div>

	<div id="fb-root"></div>
	<script>(function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
				return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/<?= $language ?>/sdk.js#xfbml=1&version=v2.6&appId=103033870062953";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

<?php
} else {
		?>
	<script>
		console.log("You have to define a fanpage url.");
	</script>
<?php
	} ?>
