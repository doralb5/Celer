
<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/horoscope-slider.css'); ?>


<section id="horoscope">



    <div class="row">
        <div class="col-md-12" data-wow-delay="0.2s">
            <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                <!-- Bottom Carousel Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/dashi.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/demi.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/binjaket.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="3"><img class="img-responsive " src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/gaforrja.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="4"><img class="img-responsive" src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/luani.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="5"><img class="img-responsive" src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/virgjeresha.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="6"><img class="img-responsive " src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/peshorja.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="7"><img class="img-responsive" src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/akrepi.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="8"><img class="img-responsive" src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/shigjetari.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="9"><img class="img-responsive " src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/bricjapi.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="10"><img class="img-responsive" src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/ujori.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="11"><img class="img-responsive" src="http://www.netirane.al/modules/SocialNetworks/views/img/horoscope/peshqit.png" alt="">
                    </li>
                </ol>

                <!-- Carousel Slides / Quotes -->
                <div class="carousel-inner text-center">

					<?php echo $text ?>


                    <!-- Quote 1 -->
                    <!--                        <div class="item active">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Sot nuk do keni asgjë për çfarë te qaheni ju te dashuruarit. Do keni mirëkuptim ne lidhjen qe keni krijuar dhe do jeni te dy palët me humor te mire. Për çdo gjë do bini dakord me njeri-tjetrin. Beqaret do kenë një dite disi te paqëndrueshme dhe do e kenë me tepër mendjen tek puna. Buxheti do jete goxha i mire, por nuk duhet te shpenzoni pafund për veshje dhe te mbeteni me xhepat bosh.</p>
                                                            <small>DASHI</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 2 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Here pas here do dalin probleme sot mes jush dhe atij qe keni ne krah. Nuk do merreni vesh mire me te dhe nga ana tjetër pa dhënë shpjegime e arsyetime do doni te dale e juaja. Beqaret do kenë ndryshime drastike ne jetën e tyre pas një takimi mbresëlënës qe do realizojnë. Financat nuk do jene te shkëlqyera, por përgjithësisht nuk do kenë probleme.</p>
                                                            <small>DEMI</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 3 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Sot do jeni me te ndjeshëm se kurrë ju te dashuruarit dhe çdo gjest i partnerit do ndikoje drejtpërdrejt tek ndjenjat tuaja. Për fat te mire ai do përkujdeset jashtëzakonisht dhe do ju beje te ndiheni me te lumturit ne bote. Beqaret do kenë një dite te mbushur me risi dhe emocione. Financat do mbeten te stabilizuara, kështu qe mund t'i kryeni pa frike shpenzimet e domosdoshme.</p>
                                                            <small>BINJAKET</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 4 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Sot duhet te bëni te pamundurën për ta mbrojtur jetën tuaj ne çift sepse shume persona do ndërhyjnë tek ajo dhe do mundohen t'ua prishin harmoninë. Ju qe jeni ende beqare do merrni propozime, por ka rrezik t'i refuzoni te gjitha. Ne planin financiar do keni disa probleme te vogla, por asnjë gjë për te vene alarmin dhe për te marre borxhe. Këshillat e te afërmve duhet t'i dëgjoni me kujdes.</p>
                                                            <small>GAFORRJA</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 5 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Merkuri do jete planeti me i begate për jetën tuaj ne çift gjate kësaj dite dhe ai do krijoje një klime harmonike dhe te ngrohte. Do mund te flisni edhe për disa tema delikate, por me rëndësi për te ardhmen. Beqaret nuk do mund te qëndrojnë indiferente ndaj ftesave qe do iu bëhen dhe me shume gjase do kenë edhe ndryshime. Buxhetin do e keni gjithë kohës ne vëmendje dhe nuk do keni probleme.</p>
                                                            <small>LUANI</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 6 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Me bekimin e gjithë yjeve, jeta juaj ne çift ka për te qene e jashtëzakonshme sot. Do ndiheni vërtet mire pranë tij dhe ndonjëherë nuk do ju besohet se jeni ne realitet. Beqaret do bëjnë te pamundurën sot qe te joshin dike dhe sa me shpejt te fillojnë një lidhje. Një pjese do kenë fat. Ne planin financiar nuk do dini si ta mbani situatën ne kontroll dhe mund te keni edhe probleme serioze.</p>
                                                            <small>VIRGJERESHA</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 7 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Sot nuk do jeni shume te duruar ju te dashuruarit dhe kjo mund te sjelle debate me partnerin. Nëse nuk ndryshoni sjellje sa me shpejt situata do ndërlikohet gjithnjë e me shume. Beqaret do arrijnë te joshin një person interesant dhe do fillojnë me ta edhe lidhje. Gjithsesi mire është qe t'i hedhin hapat me kujdes. Buxheti do mbetet i mire nëse jeni te matur dhe te rezervuar.</p>
                                                            <small>PESHORJA</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 8 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Dita e sotme ka për te qene e veçante për te dashuruarit. Do merren vendime tejet te rëndësishme te cilat do e ndryshojnë te ardhmen përgjithmonë. Beqaret nuk do e kërkojnë me ngulm dashurinë, por gjithçka do ndodhe si me magji ne momentin qe me pak e presin. Planetët do jene mbështetës për financat dhe gjendja do vije duke u përmirësuar.</p>
                                                            <small>AKREPI</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 9 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Marrëdhënia juaj me partnerin do ketë përmirësime gjate kësaj dite. Do e kuptoni me ne fund se edhe ju keni pasur gabimet tuaja ne gjithçka qe ka ndodhur dhe do kërkoni falje me çdo lloj mënyrë. Beqareve do iu jepen mundësi shume te mira, por për një arsye apo për një tjetër ata nuk do përfitojnë. Financat, fale ndihmëses se Neptunit do fillojnë te përmirësohen.</p>
                                                            <small>SHIGJETARI</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 10 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Do jeni mjaft kritike ndaj partnerit tuaj sot dhe kjo mund ta nxehe here pas here ambientin ne çift. Mos e teproni sepse mund te keni probleme serioze. Beqaret jo vetëm qe do realizojnë takime, por mund te kenë edhe ndryshime rrënjësore ne jetën sentimentale. Ne planin financiar do jeni me fat dhe gjendja do ketë me shume përmirësime nga sa mendonit.</p>
                                                            <small>BRICJAPI</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 11 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                           <p>Ju te dashuruarit bëni atë qe do ndieni sot dhe mos mendoni për asnjë se çfarë mund te thonë te tjerët. E rëndësishme është qe ju dhe partneri te ndiheni sa me mire. Beqaret nuk do kenë një dite rutine, përkundrazi mund ta gjejnë edhe me shpejt nga sa mendonin shpirtin e tyre binjak. Ne planin financiar do kaloni një mini krize për shkak te pamaturisë te treguar se fundmi.</p>
                                                            <small>BRICJAPI</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                             Quote 12 
                                            <div class="item">
                                                <blockquote>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
					
                                                            <p>Sot nuk duhet te shqetësoheni aspak ju te dashuruarit sepse planetët do mendojnë për gjithçka dhe do ju bëjnë gjithë kohës te lumtur. Beqaret duhet te tregohen te duruar sepse po u nxituan do bëjnë gabime me pasoja për te ardhmen. Buxhetin nuk do arrini dot ta përmirësoni edhe pse ne fakt do bëni te pamundurën. Mundohuni te paktën te mos shpenzoni shume te majme.</p>
                                                            <small>PESHQIT</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>-->

                </div>

                <!-- Carousel Buttons Next/Prev -->
                <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
    </div>



</section>

