<style>
    .header-social ul {
        margin: 0;
        -webkit-padding-start: 0px;
    }
    .header-social li {
        display: inline-block;
        padding: 0;
        vertical-align: middle;
    }

    .header-social li a {
        color: #74b9ed;
        padding: 0 .3em;
        font-size: 1.2em;
        transition: .2s;
    }
    .header-social li a:hover {
        color: white;
    }


</style>


<?php if (count($SocialButtons)) {
	?>
	<div class="header-social">
		<ul>
			<?php foreach ($SocialButtons as $Button) {
		?>
				<li>
					<a href="<?= $Button['link'] ?>" target="_blank"><i class="ic1 wow bounceIn animated fa fa-<?= $Button['icon'] ?>" data-wow-delay="1s"></i></a>
				</li>
			<?php
	} ?>
		</ul>
	</div>
<?php
} ?>