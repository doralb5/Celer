<link rel="stylesheet" href="http://rawgit.com/dejanstojanovic/Facebook-Album-Browser/master/src/jquery.fb.albumbrowser.css">



<?php
$account = isset($parameters['account']) ? $parameters['account'] : null;
$onlyAlbum = isset($parameters['onlyAlbum']) ? $parameters['onlyAlbum'] : null;
$showComments = isset($parameters['showComments']) ? $parameters['showComments'] : true;
$commentsLimit = isset($parameters['commentsLimit']) ? $parameters['commentsLimit'] : 4;
$showAccountInfo = isset($parameters['showAccountInfo']) ? $parameters['showAccountInfo'] : true;
$showImageCount = isset($parameters['showImageCount']) ? $parameters['showImageCount'] : true;
$showImageText = isset($parameters['showImageText']) ? $parameters['showImageText'] : true;
$lightbox = isset($parameters['lightbox']) ? $parameters['lightbox'] : true;
$likeButton = isset($parameters['likeButton']) ? $parameters['likeButton'] : true;
$shareButton = isset($parameters['shareButton']) ? $parameters['shareButton'] : true;
$thumbnailSize = isset($parameters['thumbnailSize']) ? $parameters['thumbnailSize'] : true;
$album_name = isset($parameters['album_name']) ? $parameters['album_name'] : '';
?>

<?php if (!is_null($account)) {
	?>
	<div class="fb-album-container"></div>
	<?php
	$album_name = explode(',', $album_name);

	$fullname = '';
	foreach ($album_name as $album) {
		if ($album != '') {
			$fullname .= '"' . $album . '",';
		}
	}
	$fullname = rtrim($fullname, ','); ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(".fb-album-container").FacebookAlbumBrowser({
			account: "<?= $account ?>",
					accessToken: "1752427211655481|Fw_BJyPgT3_4unszRQmYmcnVTUA",
					includeAlbums: [<?= $fullname ?>],
	<?php if ($fullname == '') {
		?>
				onlyAlbum: "<?= $only_album ?>",
	<?php
	} ?>
			showComments: <?= $showComments ?>,
					commentsLimit: <?= $commentsLimit ?>,
			showAccountInfo: <?= $showAccountInfo ?>,
					showImageCount: <?= $showImageCount ?>,
					showImageText: <?= $showImageText ?>,
					skipEmptyAlbums: true,
			skipEmptyAlbums: true,
					thumbnailSize: <?= $thumbnailSize ?>,
					albumsPageSize: 0,
					photosPageSize: 0,
			lightbox: <?= $lightbox ?>,
					photosCheckbox: false,
					pluginImagesPath: "<?= WEBROOT . $this->view_path . 'img' ?>",
					likeButton: <?= $likeButton ?>,
			shareButton: <?= $shareButton ?>,
					addThis: "ra-52638e915dd79612",
			}
			);

		});
	</script>
<?php
} else {
		?>
	<script>
		console.log("You have to set an account to show the facebook album.");
	</script>
<?php
	} ?>
<script type="text/javascript" src="http://rawgit.com/dejanstojanovic/Facebook-Album-Browser/master/src/jquery.fb.albumbrowser.js"></script>