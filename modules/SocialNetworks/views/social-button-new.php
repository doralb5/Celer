<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/social-buttons-new.css'); ?>


<?php if (count($SocialButtons)) {
	?>
	<div class="social-buttons pleft wow fadeInLeft animated" data-wow-delay="1.2s">
		<ul class="social-network social-circle">
			<?php foreach ($SocialButtons as $Button) {
		?>
				<li>
					<a href="<?= $Button['link'] ?>" class="<?= $Button['class'] ?>" target="_blank">
						<i class="fa fa-<?= $Button['icon'] ?>"></i>
					</a>
				</li>
			<?php
	} ?>
		</ul>
	</div>
<?php
} ?>