<?php
HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/social-buttons.css');
?>

<?php
foreach ($SocialButtons as $sb) {
	${$sb['icon'] . '_sq'} = isset($parameters[$sb['icon'] . '_sq']) ? $parameters[$sb['icon'] . '_sq'] : '';
}
?>

<?php if (count($SocialButtons)) {
	?>
	<div class="social-buttons eva-social-icon">
		<ul class = "social">
			<?php
			foreach ($SocialButtons as $Button) {
				if (strpos($Button['icon'], '_sq') !== false) {
					continue;
				} ?>
				<li>
					<a href = "<?= $Button['link'] ?>" target="_blank">
						<i class = "fa fa-<?= $Button['icon'] . ${$Button['icon'] . '_sq'} ?>"></i>
						<span><?= $Button['label'] ?></span>
					</a>
				</li>
			<?php
			} ?>
		</ul>
	</div>
<?php
} ?>