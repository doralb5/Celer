<?php HeadHTML::AddModCSS('Generic', 'social-buttons.css'); ?>

<?php if (count($social_buttons)) {
	?>
	<div class="social-buttons eva-social-icon">
		<ul class = "social">
			<?php foreach ($SocialButtons as $Button) {
		?>
				<li>
					<a href = "<?= $Button['link'] ?>" target="_blank">
						<i class = "fa fa-<?= $Button['icon'] ?>"></i>
						<span><?= $Button['label'] ?></span>

					</a>
				</li>
			<?php
	} ?>
		</ul>
	</div>
<?php
} ?>