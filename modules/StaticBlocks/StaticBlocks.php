<?php

class StaticBlocks extends BaseModule
{
	public function execute($parameters = array())
	{
		$id_block = null;
		if (isset($parameters['id_block']) && $parameters['id_block'] != '') {
			$id_block = $parameters['id_block'];
			$currentLang = FCRequest::getLang();
			$filter = "id_static_block = {$id_block} AND lang = '{$currentLang}'";

			$sb = $this->model->getList(1, 0, "id = $id_block AND enabled = '1'");

			if (count($sb) == 0) {
				$content = '';
			} else {
				$blockContents = $this->model->getStaticBlockContents(1, 0, $filter);

				if (count($blockContents)) {
					$content = $blockContents[0]->content;
				} else {
					$content = '';
				}
			}
		} else {
			$content = 'STATIC BLOCK NOT DEFINED!!!';
		}

		//Placeholders
		$this->view->placeholder('MEDIA_PATH')->setVal(WEBROOT . MEDIA_ROOT);
		$this->view->placeholder('WEBSITE')->setVal(CMSSettings::$website_title);
		$this->view->placeholder('EMAIL')->setVal(CMSSettings::$EMAIL_ADMIN);
		$this->view->placeholder('COMPANY')->setVal(CMSSettings::$company_name);
		$this->view->placeholder('LOGO')->setVal(CMSSettings::$logo);

		if (isset(CMSSettings::$facebook)) {
			$this->view->placeholder('SET_FACEBOOK')->setVal(CMSSettings::$facebook);
		}
		if (isset(CMSSettings::$company_name)) {
			$this->view->placeholder('SET_COMP_NAME')->setVal(CMSSettings::$company_name);
		}
		if (isset(CMSSettings::$company_address)) {
			$this->view->placeholder('SET_COMP_ADDRESS')->setVal(CMSSettings::$company_address);
		}
		if (isset(CMSSettings::$company_mail)) {
			$this->view->placeholder('SET_COMP_MAIL')->setVal(CMSSettings::$company_mail);
		}
		if (isset(CMSSettings::$company_tel)) {
			$this->view->placeholder('SET_COMP_TEL')->setVal(CMSSettings::$company_tel);
		}

		$this->view->set('parameters', $parameters);

		$view = (isset($parameters['view'])) ? $parameters['view'] : 'standard-static-block';
		$this->view->setContent($content);
		$this->view->set('id_static_block', $id_block);
		$this->view->render($view);
	}
}
