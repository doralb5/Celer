<!DOCTYPE HTML PUBLIC>
<html>
    <head>
        <meta http-equiv='content-type' content='text/html; charset=UTF-8' />
        <title>jQuery UI Chat</title>

        <!-- Bootstrap 3.3.4 -->
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="https://github.com/lovelle/jquery-chat/blob/master/templates/AdminLTE/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <link id='theme' rel='stylesheet' />
        <link rel='stylesheet' href='https://github.com/lovelle/jquery-chat/blob/master/css/tipsy.css' />
        <link rel='stylesheet' href='https://github.com/lovelle/jquery-chat/blob/master/css/chat.css' />

        <script src='https://jquery-chat.herokuapp.com/socket.io/socket.io.js'></script>
        <script src='https://github.com/lovelle/jquery-chat/blob/master/js/jquery-1.11.3.min.js'></script>
        <script src='https://github.com/lovelle/jquery-chat/blob/master/js/jquery-ui-1.10.4.custom.min.js'></script>
        <script src='https://github.com/lovelle/jquery-chat/blob/master/js/jquery.slimscroll.min.js'></script>
        <script src='https://github.com/lovelle/jquery-chat/blob/master/js/jquery.tipsy.js'></script>
        <script src='https://github.com/lovelle/jquery-chat/blob/master/js/jquery.main.js'></script>
        <script src='https://github.com/lovelle/jquery-chat/blob/master/config.js'></script>
        <script src='https://github.com/lovelle/jquery-chat/blob/master/i18n_en.js'></script>
    </head>

    <body>
        <h1 align='center'>Demo website :-)</h1>
    </body>
</html>