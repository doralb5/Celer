<?php

class LoginHeader extends BaseModule
{
	public function execute($parameters = array())
	{
		if (UserAuth::checkFrontLoginSession()) {
			$user_logged = true;
			$user = UserAuth::getLoginSession();
		} else {
			$user_logged = false;
			$user = null;
		}

		if (isset($parameters['dashboard_url'])) {
			$url_dashboard = Utils::getComponentUrl($parameters['dashboard_url']);
		} else {
			$url_dashboard = Utils::getComponentUrl('Users/edit_account');
		}

		$this->view->set('user_logged', $user_logged);
		$this->view->set('user', $user);

		$this->view->set('url_logout', Utils::getComponentUrl('Users/logout'));
		$this->view->set('url_login', Utils::getComponentUrl('Users/login'));
		$this->view->set('url_signin', Utils::getComponentUrl('Users/register'));
		$this->view->set('url_edit_profile', Utils::getComponentUrl('Users/edit_account'));
		$this->view->set('url_dashboard', $url_dashboard);

		$this->view->set('parameters', $parameters);
		$view = (isset($parameters['view'])) ? $parameters['view'] : 'login';
		$this->view->render($view);
	}
}
