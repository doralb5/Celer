<?php

class LoginRibbon extends BaseModule
{
	public function execute($parameters = array())
	{
		if (UserAuth::checkFrontLoginSession()) {
			$user_logged = true;
			$user = UserAuth::getLoginSession();
			$this->view->set('user', $user);
			$this->view->set('parameters', $parameters);
			$view = (isset($parameters['view'])) ? $parameters['view'] : 'user-ribbon';
			$this->view->render($view);
		} else {
			$user_logged = false;
			$user = null;
		}
	}
}
