<?php HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/LoginMenu-standard.css'); ?>
<div class="LoginMenu-standard">   
	<?php if ($user_logged) {
	?>
		<span class="signin-btn"><span style="text-transform: capitalize;"></span> 
			<ul class="pull-right">
				<li class="dropdown">
					<a href="#" class="" data-toggle="dropdown"><?= $user['firstname'] ?> <?= $user['lastname'][0] ?>.

						<?php if (isset($_SESSION['user_auth']['image']) && $_SESSION['user_auth']['image'] != '') {
		?>
							<img src = "<?= Utils::genThumbnailUrl('users/' . $user['image'], 100, 100, array('zc' => 1)) ?>" class="avatar img-circle img-thumbnail" alt="avatar">
						<?php
	} else {
		?>
							<img src="<?= WEBROOT . $this->view_path ?>img/avatar.png" class="avatar img-circle img-thumbnail" alt="avatar">
						<?php
	} ?> <b class="caret"></b></a>


					<ul class="dropdown-menu">
						<li><a href="<?= $url_dashboard ?>"><i class="glyphicon glyphicon-home"></i> [$dashboard]</a></li>
						<li><a href="<?= $url_edit_profile ?>"><i class="glyphicon glyphicon-user"></i> [$edit_profile]</a></li>
						<li class="divider"></li>
						<li><a href="<?= $url_logout ?>"><i class="glyphicon glyphicon-off"></i> [$logout]</a></li>
					</ul>
				</li>
			</ul>
		</span>
	<?php
} else {
		?>
		<span class="signin-btn"><a class="btn" href="<?= $url_signin ?>">[$signin]</a></span> <span class="login-btn"><a class="btn btn-login" href="<?= $url_login ?>">[$login]</a></span>
	<?php
	} ?>
</div>



