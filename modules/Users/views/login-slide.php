<?php require_once LIBS_PATH . 'UserAuth.php'; ?>

<?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
	?>
	<div id="pb_4">
		<div class="collapse" id="collapseLogin" aria-expanded="false" style="height: 0px;">
			<div class="main-login-top">
				<div class="row">
					<div class="col-md-6 accedi">
						<h4 style="text-transform: capitalize;"><?= $_SESSION['user_auth']['firstname'] ?> ! </h4>

						<br>
						<a class="btn btn-sm btn-default" href="<?= Utils::getComponentUrl('Businesses/business_list_u') ?>">Paneli juaj</a>
					</div>

					<div class="col-md-6 register">
						<h4>Dil nga llogaria</h4>
						<br>
						<a class="btn btn-sm btn-default" href="<?= Utils::getComponentUrl('Users/logout') ?>">Logout</a>
					</div>

				</div>
			</div>
		</div>

		<div class="collapse" id="collapseInsert" aria-expanded="false" style="height: 0px;">
			<div class="main-login-top">
				<div class="row">
					<ul>
						<li class="col-md-6 home-post" style="">
							<a class="categories invert" href="/shto-biznes">
								<span class="fa fa-briefcase fa-align"></span>
							</a>
							<p>
								<a class="btn btn-sm btn-default" href="/shto-biznes">Shtoni biznes</a>
							</p>
						</li>
						<li class="col-md-6 home-post" style="">
							<a class="categories invert" href="<?= Utils::getComponentUrl('Businesse/insert_business/') ?>">
								<span class="fa fa-flag fa-align"></span>
							</a>
							<p>
								<a class="btn btn-sm btn-default" href="<?= Utils::getComponentUrl('Announces/insertAnnounce/') ?>">Shtoni Njoftim</a>
							</p>
						</li>

					</ul>

				</div>
			</div>
		</div>
	</div>  


<?php
} else {
		?>

	<div id="pb_4">
		<div class="collapse" id="collapseLogin" aria-expanded="false" style="height: 0px;">
			<div class="main-login-top">
				<div class="row">

					<div class="col-md-6 accedi">
						<h4>Logohuni ne portalin tone</h4>
						<br>
						<a class="btn btn-sm btn-default" href="/login">Login</a>
					</div>

					<div class="col-md-6 register">
						<h4>Perdorues i ri? Rregjistrohuni tani</h4>
						<br>
						<a class="btn btn-sm btn-default" href="/shto-biznes">Rregjistrohu</a>
					</div>

				</div>
			</div>
		</div>
	</div>  

<?php
	} ?>

