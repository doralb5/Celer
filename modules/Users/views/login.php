
<?php if (isset($_SESSION['user_auth']) && $_SESSION['user_auth']['username'] != '') {
	?>

	<div id="pb_7">
		<div class="log-in">
			<a aria-controls="collapseLogin" aria-expanded="false" data-toggle="collapse" href="#collapseLogin" id="login-btn" role="button" class="collapsed">
				<i aria-hidden="true" class="fa fa-user"></i>
			</a>
		</div>
	<!--        <h1 class="user-logged"><?php //= $_SESSION['user_auth']['firstname']?> </h1> -->
	</div>
	<div id="pb_8">
		<div class="search-mobile">
			<a aria-controls="collapseSearch" aria-expanded="false" data-toggle="collapse" href="#collapseSearch" id="search-btn" role="button" class="collapsed">
				<i aria-hidden="true" class="fa fa-search"></i>
			</a>
		</div>
	</div>
	<div id="pb_7">
		<div class="insert">
			<a aria-controls="collapseInsert" aria-expanded="false" data-toggle="collapse" href="#collapseInsert" id="insert" role="button" class="collapsed">
				<i aria-hidden="true" class="fa fa-plus"></i>
			</a>
		</div>
	</div>        
<?php
} else {
		?>
	<div id="pb_7">
		<div class="log-in">
			<a aria-controls="collapseLogin" aria-expanded="false" data-toggle="collapse" href="#collapseLogin" id="login-btn" role="button" class="collapsed">
				<i aria-hidden="true" class="fa fa-user"></i>
			</a>
		</div>
	</div>
	<div id="pb_8">
		<div class="search-mobile">
			<a aria-controls="collapseSearch" aria-expanded="false" data-toggle="collapse" href="#collapseSearch" id="search-btn" role="button" class="collapsed">
				<i aria-hidden="true" class="fa fa-search"></i>
			</a>
		</div>
	</div>
	<div id="pb_9">
		<div class="scopri-servici">
			<a aria-controls="collapseServices" aria-expanded="false" data-toggle="collapse" href="#collapseServices" id="services-btn" role="button" class="collapsed">
				<i aria-hidden="true" class="fa fa-plus"></i> 
			</a>
		</div>
	</div>   
<?php
	} ?>
