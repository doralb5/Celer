<?php

class offers extends BaseModel
{
	public function __construct($block_id = '', $position = '', $package = '')
	{
		parent::__construct($block_id, $position, $package);
		$this->model = Loader::getModel('BusinessOffer');
	}
}
