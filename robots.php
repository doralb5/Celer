<?php

header('Content-Type:text/plain');

include "./config/definitions.php";
include "./config/config.php";

$http_proto = (!isset($_SERVER['HTTPS']) || empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off') ? 'http' : 'https';

echo "User-agent: *" . "\n";
echo "Disallow: /admin/" . "\n";
echo "Sitemap: " . $http_proto . "://" . $_SERVER['HTTP_HOST'] . "/" . "sitemap.xml";
