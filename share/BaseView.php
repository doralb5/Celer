<?php

require_once DOCROOT . LIBS_PATH . 'Position.php';

class BaseView {

	private $vars = array();
	private $template_name = 'admin_t2';
	private $template_path = '';
	private $template_params = array();
	public $view_path;
	private $layout = null;
	private $layout_file = null;
	private $positions = array();
	private $render_template = true;
	private $id_page;
	private $lang;
	private $lang_path;
	private $arrPlaceholders = array();
	private $terms;
	protected $content;
	private $DATA_DIR;
	private $ignore_widget_placeholder = false;

	function __construct() {
		require_once 'HeadHTML.php';

		$this->layout = null;
		$this->layout_file = null;
		$this->DATA_DIR = (defined('DATA_DIR')) ? DATA_DIR : DEFAULT_DATA_DIR;

		if (!ADMIN) {
			$this->setTemplate(CMSSettings::$template, CMSSettings::$template_options);
			//$this->setLayout(CMSSettings::$layout);
			HeadHTML::setTemplate(CMSSettings::$template);
		} else {
			$this->setTemplate(CMSSettings::$admin_template);
			//$this->setLayout(CMSSettings::$admin_layout);
			HeadHTML::setTemplate(CMSSettings::$admin_template);
		}

		$this->setViewPath(VIEWS_PATH);
		$this->setLangPath(LANGS_PATH);
	}

	/**
	 * if true the render function include template html
	 * @param boolean $render_template
	 */
	public function renderTemplate($render_template = true) {
		$this->render_template = $render_template;
	}

	public function setViewPath($viewpath) {

		if (file_exists($viewpath) || file_exists($this->DATA_DIR . $viewpath)) {
			$this->view_path = $viewpath;
			return true;
		} else {
			return false;
		}
	}

	public function setLangPath($langpath) {
		if (is_dir($langpath) || is_dir($this->DATA_DIR . $langpath)) {
			$this->lang_path = $langpath;
			if (!is_null($this->lang)) {
				$this->loadTerms();
			}
			return true;
		} else {
			return false;
		}
	}

	public function render($view = '', $return_output = false) {

		$output = $this->loadView($view);

		if ($output !== false) {


			if ($this->layout_file != null || false) {
				$content = $output;
				ob_start();
				include($this->layout_file);
				$output = ob_get_contents();
				ob_end_clean();
			}

			$output = $this->replace_vars($output);

			if ($return_output == false) {
				echo $output;
			} else {
				return $output;
			}
		} else {
			echo "View File not found! [" . $view . '.php' . "]";
			return false;
		}
	}

	private function loadView($view = '') {

		extract($this->vars);

		/* Set $content that I use in view file before include it */
		$content = $this->content;

		//echo "Search ViewFile in ''" . DOCROOT . $this->DATA_DIR . $this->view_path . $view . '.php';
		//echo "Search ViewFile in ''" . $this->view_path . $view . '.php';
		//echo "Search ViewFile in ''" . VIEWS_PATH . $view . '.php';

		if ($view != '') {
			ob_start();

			if (file_exists($view)) {
				//echo "try to include $view";exit;
				include $view;
			} else {

				if (file_exists(DOCROOT . $this->DATA_DIR . $this->view_path . $view . '.php')) {
					include DOCROOT . $this->DATA_DIR . $this->view_path . $view . '.php';
				} elseif (file_exists($this->view_path . $view . '.php')) {
					include $this->view_path . $view . '.php';
				} elseif (file_exists(VIEWS_PATH . $view . '.php')) {
					include VIEWS_PATH . $view . '.php';
				} else {
					return false;
				}
			}
			$output = ob_get_contents();
			ob_end_clean();
		}

		return $output;
	}

	/**
	 * 
	 * @param string $view
	 * @param boolean $return_output
	 * @return string
	 */
	public function render_old($view, $return_output = false) {
		$content = $this->content;

		extract($this->vars);

		ob_start();

		//echo "<!--" . DOCROOT . $this->DATA_DIR . $this->view_path . $view . '.php'. "-->";
		if (file_exists(DOCROOT . $this->DATA_DIR . $this->view_path . $view . '.php')) {
			include DOCROOT . $this->DATA_DIR . $this->view_path . $view . '.php';
		} elseif (file_exists($this->view_path . $view . '.php')) {
			include $this->view_path . $view . '.php';
		} elseif (file_exists(VIEWS_PATH . $view . '.php')) {
			include VIEWS_PATH . $view . '.php';
		} else {
			echo "View File not found! [" . $view . '.php' . "]";
		}


		$content = ob_get_contents();
		ob_end_clean();

		$content = $this->replace_vars($content);

		if ($return_output == false) {
			if ($this->render_template) {
				$filename = TEMPLATES_PATH . $this->template_name . '/' . $this->layout_file;
				if (is_file($this->DATA_DIR . $filename)) {
					include $this->DATA_DIR . $filename;
				} elseif (is_file($filename)) {
					include $filename;
				}
			} else
				echo $content;
			return;
		} else {
			if ($this->render_template) {
				ob_start();
				include 'templates/' . $this->template_name . '/' . $this->layout_file;
				$content = ob_get_contents();
				ob_end_clean();
				$content = $this->replace_vars($content);
			}
			return $content;
		}
	}

	public function set($varname, $value) {
		$this->vars[$varname] = $value;
	}

	public function setTemplate($name, $params = array()) {

		if ($name != '') {

			//Check in the data folder
			$templ_path = $this->DATA_DIR . TEMPLATES_PATH . $name . '/';
			if (is_dir($templ_path)) {
				$this->template_name = $name;
				$this->template_path = WEBROOT . $templ_path;
				$this->template_params = $params;
				return true;
			}

			$templ_path = TEMPLATES_PATH . $name . '/';

			if (is_dir($templ_path)) {
				$this->template_name = $name;
				$this->template_path = WEBROOT . $templ_path;
				$this->template_params = $params;
				return true;
			} else {
				if (SHOW_ERRORS) {
					echo "TEMPLATE $name NOT FOUND!";
				}
				return false;
			}
		} else {
			$this->template_name = 'admin_t1';
			$this->template_path = WEBROOT . TEMPLATES_PATH . '/admin_t1/';
			$this->template_params = $params;
		}
	}

	public function setLayout($layout = null) {
		if ($layout == '' || $layout == null) {
			$this->layout = null;
			$this->layout_file = null;
			return;
		}

		$this->layout = $layout;
		$layout_file = $layout . '.php';

		$template_path = trim(str_replace($this->DATA_DIR, '', $this->template_path), '/') . '/';

		if (is_file(DOCROOT . $this->DATA_DIR . $template_path . $layout . '.php')) {
			$this->layout_file = DOCROOT . $this->DATA_DIR . $template_path . $layout . '.php';
			return true;
		}

		if (is_file(DOCROOT . $template_path . $layout . '.php')) {
			$this->layout_file = DOCROOT . $template_path . $layout . '.php';
			return true;
		} else {
			if (SHOW_ERRORS) {
				echo "<pre>LAYOUT $name NOT FOUND!<br/></pre>";
			}
			return false;
		}
	}

	public function setLang($lang) {
		$this->lang = $lang;
		$this->loadTerms();
	}

	private function loadTerms() {

		if (file_exists($this->lang_path . $this->lang . '.ini')) {
			$this->terms = parse_ini_file($this->lang_path . $this->lang . '.ini');
		}

		if (file_exists($this->DATA_DIR . $this->lang_path . $this->lang . '.ini')) {
			$arrTerms = parse_ini_file($this->DATA_DIR . $this->lang_path . $this->lang . '.ini');
			foreach ($arrTerms as $term => $val) {
				$this->terms[$term] = $val;
			}
		}
	}

	public function getTerm($term) {

		return (isset($this->terms[$term])) ? $this->terms[$term] : $term;
	}

	public function setPositions($arrPositions) {
		$this->positions = $arrPositions;
	}

	private function getPosition($name) {
		if (key_exists($name, $this->positions)) {
			return $this->positions[$name];
		} else {
			return new Position($name, $this->id_page);
		}
	}

	public function placeholder($name) {

		$phObj = null;

		foreach ($this->arrPlaceholders as $phname => $ph) {
			if ($phname == $name) {
				$phObj = $ph;
				break;
			}
		}

		if (is_null($phObj)) {
			$phObj = new View_Placeholder($name);
			$this->arrPlaceholders[$name] = $phObj;
		}

		return $phObj;
	}

	public function setIdPage($id) {
		$this->id_page = $id;
	}

	function setContent($content = '') {
		$this->content = $content;
	}

	protected function replace_vars($string) {

		//Sostituisco il risultato di un array se trovo nel testo la stringa {:xxxx[yyy]}
		while (preg_match('~\{\:([a-zA-Z0-9\_]*)\[([a-zA-Z0-9\_]*)\]?\}~sU', $string, $m)) {
			$string = str_replace($m[0], ( isset($vars[$m[1]][$m[2]]) ? $vars[$m[1]][$m[2]] : ''), $string);
		}
		//Sostituisco il risultato di una variabile se trovo nel testo la stringa {:xxxx}
		while (preg_match('~\{\:([a-zA-Z0-9\_]*)?\}~sU', $string, $m)) {
			$string = str_replace($m[0], ( isset($vars[$m[1]]) ? $vars[$m[1]] : ''), $string);
		}

		if (count($this->terms)) {

			//Sostituisco le variabili dei termini multilingua [$variabile]
			while (preg_match('~\[\$(\w+)\]~', $string, $m)) {

				if (!is_null($this->getTerm($m[1]))) {
					$string = str_replace($m[0], $this->getTerm($m[1]), $string);
				} else {
					$string = str_replace($m[0], '__' . $m[1] . '__', $string);
				}
			}
		}

		//Sosituisco i Placeholders es. {MARCATORE}
		$temp_string = $string;
		while (preg_match('~{(\w+)}~', $temp_string, $m)) {
			if (isset($this->arrPlaceholders[$m[1]]))
				$string = str_replace($m[0], $this->arrPlaceholders[$m[1]]->getVal(), $string);

			$temp_string = str_replace($m[0], $m[1], $temp_string);
		}

		//Sosituisco i Placeholders es. [[MARCATORE]]
		$temp_string = $string;
		while (preg_match('~\[\[(\w+)\]\]~', $temp_string, $m)) {
			if (isset($this->arrPlaceholders[$m[1]]))
				$string = str_replace($m[0], $this->arrPlaceholders[$m[1]]->getVal(), $string);

			$temp_string = str_replace($m[0], $m[1], $temp_string);
		}


		if (!$this->ignore_widget_placeholder) {
			$string = self::replaceWidgets($string);
		}

		return $string;
	}

	public function ignoreWidgetPlaceHolder($ignore = true) {
		if ($ignore) {
			$this->ignore_widget_placeholder = true;
		}
	}

	public static function replaceWidgets($string) {
		//Sosituisco i Placeholders es. {{widget type="WidgetType" param1="test"}} con Render Widget
		$temp_string = $string;
		while (preg_match('~\{\{widget (.*)\}\}~', $temp_string, $m)) {
			$quot_replace = str_replace("&quot;", '"', $m[0]);
			$string = str_replace($m[0], $quot_replace, $string);
			$temp_string = str_replace($m[0], "--", $string);
		}
		$temp_string = $string;
		while (preg_match('~\{\{widget type\=\"([\w|/]+)\"\s+(.*)\}\}~', $temp_string, $m)) {

			$widget_output = 'Error: Widget Not Loaded!';
			$widgetType = $m[1];
			if ($widget_controller = Loader::loadModule($widgetType)) {
				$wparams = [];
				while (preg_match('~(\w+)=\"([^\"]+)\"~', $m[2], $wpar)) {
					$wparams[$wpar[1]] = $wpar[2];

					$m[2] = str_replace($wpar[0], "", $m[2]);
				}

				ob_start();
				$widget_controller->execute($wparams);
				$widget_output = ob_get_contents();
				ob_end_clean();
			}

			$string = str_replace($m[0], $widget_output, $string);

			$temp_string = str_replace($m[0], "", $temp_string);
		}
		return $string;
	}

	public function renderError($err_code) {
		$this->render('/views/errors/' . $err_code);
	}

}

class View_Placeholder {

	private $name = null;
	private $value = null;
	private $htmlentities = true;

	function __construct($name) {
		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}

	public function setVal($val) {
		if ($this->htmlentities)
			$val = nl2br(htmlentities($val));
		$this->value = $val;
	}

	public function getVal() {
		return $this->value;
	}

}
