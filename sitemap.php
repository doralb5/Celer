<?php

define('WEBROOT', '/');
include "./config/definitions.php";
include "./config/config.php";

if (file_exists(DOCROOT . DATA_DIR . 'sitemap.xml')) {
	header('Content-type: text/xml');
	readfile(DOCROOT . DATA_DIR . 'sitemap.xml');
} else {
	header("HTTP/1.0 404 Not Found");
	die();
}