<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? (CMSSettings::$favicon != '') ? HeadHTML::printFavicon(CMSSettings::$favicon) : ''; ?>

        <!-- Bootstrap -->
		<? HeadHTML::linkStylesheet('bootstrap.css'); ?>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- font awesome -->
		<? HeadHTML::linkStylesheet('font-awesome.min.css'); ?>

        <!--FlexSlider css-->
		<? HeadHTML::linkStylesheet('flexslider.css'); ?>

        <!-- style css -->
		<? HeadHTML::linkStylesheet('style.css'); ?>
        <!-- custom css -->
		<? HeadHTML::linkStylesheet('custom.css'); ?>
		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- jQuery -->
		<? HeadHtml::linkJS("jquery.min.js"); ?>

		<?= $this->getTemplate()->head_extra; ?>

    </head>

    <body id="page-top">

		<?
		if ($this->getPosition('pos-nav')->countBlocks() > 0) {
			$this->getPosition('pos-nav')->printContent();
		}
		?>

		<? if ($this->getPosition('pos-banner')->countBlocks() > 0) { ?>
			<section class="slider-banner">
				<div class="container-fluid">
					<div class="row">
						<? $this->getPosition('pos-banner')->printContent(); ?>
					</div>
				</div>
			</section>
		<? } ?>
