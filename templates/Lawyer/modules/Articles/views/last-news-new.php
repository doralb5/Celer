<? require_once LIBS_PATH . 'StringUtils.php'; ?>
<?

$w = (isset($parameters['w'])) ? $parameters['w'] : 700;
$h = (isset($parameters['h'])) ? $parameters['h'] : 350;
//$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 80;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 100;
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 3;
$col_size = 12 / $cols;
?>

<? if(count($articles)) {?>
    <? foreach ($articles as $article) { ?>
        <article class="blog-post clearfix">
            <div class="row">
                <div class="col-md-6 col-sm-6 img-left">
                    <? if ($article->image != '') { ?>
                     <a class=""
                        href="<?= Utils::getComponentUrl('Articles/show_article') . "/" . Utils::url_slug($article->title . '-' . $article->id) ?>">
                         <img alt="" class="img-responsive"
                              src="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, $w, $h, array('zc' => 1)) ?>"/>
                     </a>  
                    <?}else{?> 
                        <a class=""
                        href="<?= Utils::getComponentUrl('Articles/show_article') . "/" . Utils::url_slug($article->title . '-' . $article->id) ?>">
                            <img src="/data/avvocato.weweb.al/media/images/blog-554x330.jpg" class="img-responsive">
                        </a>
                    <?}?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="blog-content">
                        <div class="date">
                            <p><i class="fa fa-calendar"></i><span><?= date('F j, Y', strtotime($article->publish_date)) ?></span></p>
                        </div>
                         <?if($article->show_author){?>
                            <div class="author">
                                <p><span><i class="fa fa-user"></i><?=$article->author;?></span></p>
                            </div>
                         <?}?>
                        <div class="blog-title">
                            <h3 class="title"><a href="<?= Utils::getComponentUrl('Articles/show_article') . "/" . Utils::url_slug($article->title . '-' . $article->id) ?>"><?= strip_tags($article->title); ?> <?= StringUtils::CutString(strip_tags($article->title), $taglia_title) ?></a></h3>

                        </div>
                        <div class="blog-text">
                            <p> <?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></p>
                        </div>
                        <div class="read-btn">
                            <a href="<?= Utils::getComponentUrl('Articles/show_article') . "/" . Utils::url_slug($article->title . '-' . $article->id) ?>" class="btn"> Continue Reading <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    <?}?>
<?}?>






