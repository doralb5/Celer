<?
$show_title = isset($params['show_title']) ? $params['show_title'] : 1;
$name = isset($_POST['name']) ? $_POST['name'] : "";
$email = isset($_POST['email']) ? $_POST['email'] : "";
$message = isset($_POST['message']) ? $_POST['message'] : "";
?>
<!--contact-->
<section class="<?= ($show_title) ? 'section-padding' : '' ?>" id="contact">
    <div class="contact-info">
        <div id="sendmessage"></div>
        <div id="errormessage"></div>
        <form role="form" id="contactForm" method="post">
            <? if (isset($success)) { ?>
                <div class="alert alert-success" role="alert">
                    <i class="fa fa-check"></i>
                    <?= $success ?>
                </div>
           <? } ?>
            <div class="form-group">
                <input class="form-control br-radius-zero" data-msg="Please enter at least 4 chars" data-rule="minlen:4" id="name" name="name" placeholder="[$name_placehoder]" type="text" value="<?= $name ?>" required />
                <div class="validation">&nbsp;</div>
            </div>
            <div class="form-group">
                <input class="form-control br-radius-zero" data-msg="Please enter a valid email" data-rule="email" id="email" name="email" placeholder="[$email_placehoder]" type="email" value="<?= $email ?>" required/>
                <div class="validation">&nbsp;</div>
            </div>
            <div class="form-group">
                <textarea class='form-control br-radius-zero' name='message' id="message" style="max-width:100%" rows="10" cols="100" placeholder="[$message_placehoder]" required><?= $message ?></textarea>
                <div class="validation">&nbsp;</div>
            </div>        
            <div class="form-action">
                <input type="hidden" name="send" value="1" />
                <button class="btn btn-form" type="submit" id="contactSubmit" value="1">[$send_request]</button>
            </div>
        </form>
    </div>          
</section>

<script>

    $(document).ready(function () {
        
        $("#contactSubmit").click(function (e) {
            
            // Prevent default posting of form - put here to work in case of errors
            e.preventDefault();

            // setup some local variables
            var $form = $('#contactForm');

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            // Serialize the data in the form
            var serializedData = $form.serialize();

            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            request = $.ajax({
                url: "<?= $ajx_action_url ?>",
                type: "post",
                data: serializedData
            });
            
            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                $('#sendmessage').html(' ');
                $('#sendmessage').css('display', 'none');
                $('#errormessage').html(' ');
                $('#errormessage').css('display', 'none');

                var resp = JSON.parse(response);
                
                if ('msg' in resp) {
                    $('#sendmessage').html(resp['msg']);
                    $('#sendmessage').css('display', 'block');
                } else {
                    $('#errormessage').html(resp['error']);
                    $('#errormessage').css('display', 'block');
                }

            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                // Log the error to the console
                console.log(
                        "The following error occurred: " +
                        textStatus, errorThrown
                        );
                $('#errormessage').html("The following error occurred: " + textStatus);
                $('#errormessage').css('display', 'block');
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });

        });
    });

</script>

<!--/ contact-->