<?
$show_brand = isset($parameters['show_brand']) ? $parameters['show_brand'] : false;
$show_logo = isset($parameters['show_logo']) ? $parameters['show_logo'] : true;
?>

<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                
                
                <? if ($show_logo) {
                    $logo = WEBROOT . MEDIA_ROOT . CMSSettings::$logo;
                ?>
                    <!-- Logo Here -->
                    <a class="navbar-brand page-scroll" href="/"><img class="img-responsive" src="<?= $logo ?>"></a>
                <? } ?>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <? printRecursiveMenuItems($MenuItems);?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


<?

    function printRecursiveMenuItems($items) {
        foreach ($items as $item) {
            
            $target = getTargetByItem($item);
            switch ($item->type) {
                case "Page" :
                    $target = Utils::genUrl($item->page->getAlias());
                    break;
                case "External Link" :
                    $target = $item->target;
                    break;
                case "No Link" :
                    $target = '#';
                    break;
                    
            }
            
            $active_class = (isActiveRecursively($item)) ? "active" : "";
            
            if($item->type == 'Divider') {
                echo "<li class=\"divider $active_class\"></li>\n";
            } else {
                echo "<li class=\"$active_class\">";
                if(count($item->subitems) == 0){
                    echo "<a href=\"$target\" class=\"page-scroll\">{$item->item}</a>";
                } else {
                    echo "<a href=\"$target\" class=\"dropdown-toggle page-scroll\" data-toggle=\"dropdown\">{$item->item} <b class=\"caret\"></b></a>\n\n";
                    echo "<ul class=\"dropdown-menu\">\n";
                    printRecursiveMenuItems($item->subitems);
                    echo "</ul>\n\n";
                }
                echo "</li>\n";
            }
        }
    }
    
    function getTargetByItem($item) {
        switch ($item->type) {
            case "Page" :
                $target = Utils::genUrl($item->page->getAlias());
                break;
            case "External Link" :
                $target = $item->target;
                break;
            case "No Link" :
                $target = '#';
                break;
            default :
                $target = '';
        }
        return $target;
    }
    
    function isActiveRecursively($item) {
        $target = getTargetByItem($item);
        $active = Utils::isActivePage($target);
        if($active)
            return true;
        
        if(count($item->subitems)) {
            foreach ($item->subitems as $subitem) {
                $active = isActiveRecursively($subitem);
                if($active) {
                    return true;
                }
            }
        }
        return false;
    }

?>