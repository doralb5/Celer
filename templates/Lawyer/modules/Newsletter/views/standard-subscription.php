 <div class="newsletter" id="subscirbe">
    <h2>[$GetNewsletterUpdate]-<br> [$answers_needed] </h2>
    <form class="form-inline">
        <div class="form-group">
          <input type="email" class="form-control" id="inputEmail2" placeholder="Put your email">
        </div>
        <button id="subscribe-btn" type="submit" class="btn btn-default fs-18">[$Submit]</button>
        <div class="col-md-12">
        <p style="color: #c9302c; text-align: center; font-size: 17px; display: none; margin-top: 15px;" id="error"></p>
        </div>
        <div class="col-md-12">
          <p style="color: green; text-align: center; font-size: 17px; margin-top: 15px; display: none" id="message"></p>
        </div>
    </form>
</div>
<!--div id="subscribe">
    <div class="">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="wow fadeInDown animated newsletter-heading" data-wow-delay=".5s">[$GetNewsletterUpdate]</h3>
            </div>
            <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2 newsletter-input">
                <div class="input-group wow shake animated " data-wow-delay="2s" style="margin-top: 8px;">
                    <input id="email" type="text" class="form-control input-lg" placeholder="[$Enter_your_mail]">
                    <span class="input-group-btn">
                        <button id="subscribe-btn" class="btn btn-danger btn-lg" type="submit"><i class="fa fa-envelope"></i>
                        </button>
                    </span>
                </div>
            </div>
            <div class="col-md-12">
                <p style="color: #c9302c; text-align: center; font-size: 17px; display: none; margin-top: 15px;" id="error"></p>
            </div>
            <div class="col-md-12">
                <p style="color: green; text-align: center; font-size: 17px; margin-top: 15px; display: none" id="message"></p>
            </div>
        </div>
    </div>
</div-->



<script>

<?
$call_url = Utils::getComponentUrl("Newsletter/Subscribers/ajx_subscribe") . "/";
?>
    $(document).ready(function () {
        $('#subscribe-btn').click(function () {
            
            $.ajax("<?= $call_url ?>" + $('#email').val())
                    .done(function (data) {
                        if (data.status == 1) {

                            if (data.message != undefined) {
                                $('#message').html('<span class="msg-span">' + data.message + '</span>');
                                $('#message').show("slow", function () {
                                    setTimeout(function () {
                                        $("#message").hide('slow');
                                    }, 2500);
                                });
                            }
                        } else {
                            if (data.message != undefined) {
                                $('#error').html('<span class="error-span">' + data.message + '</span>');
                                $('#error').show("slow", function () {
                                    setTimeout(function () {
                                        $("#error").hide('slow');
                                    }, 2500);
                                });
                            }
                        }
                    })
                    .fail(function (error) {
                        console.log(error);
                    })
        });
    });
</script>

