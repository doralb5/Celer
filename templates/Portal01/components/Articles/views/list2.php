<? HeadHTML::AddStylesheet($this->template_path . $this->view_path . 'css/list2-default.css'); ?>
<?
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';
?>
<? $show_date = (isset($parameters['show_date'])) ? $parameters['show_date'] : 1; ?>
<? $show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0; ?>
<? $show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0; ?>
<?
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 400;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 255;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 350;
$button = (isset($parameters['button'])) ? $parameters['button'] : 'primary';
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 2;
$col_size = 12 / $cols;
?>

<!--/* --------------- News List v2---------------*/-->

<div class="row row-m">
    <div class=" col-md-12  col-p  main-content">
        <div class="theiaStickySidebar">
            <div class="post-inner">
                    <? if ($title != '') { ?>
                        <div class="post-head">
                            <h2 class="title"><strong><?= $title ?></strong></h2>
                        </div>
                    <? } ?>

                <div class="post-body">
                    <? if (count($articles)) { ?>
                        <?
                        $i = 0;
                        foreach ($articles as $article) {
                            ?>
                            <div class="news-list-item articles-list">
                                    <div class="img-wrapper thumb">
                                        <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">
                                            <?
                                            $categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : '');
                                            ?>
                                            <? if ($article->image != '') { ?>
                                                <img src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", $w, $h, array('zc' => 1)) ?>" class="img-responsive"></a>
                                            <? }else{ ?>
                                                <img src="//via.placeholder.com/<?=$w.'x'.$h?>" class="img-responsive" />
                                            <?}?>
                                        </a>
                                    </div>
                                <div class="post-info-2">
                                    <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>"> <h4><?= $article->title ?></h4></a>
                                    <p class="hidden-sm">
                                        <? if ($show_date) { ?>   
                                            <small><?
                                                if (FCRequest::getLang() == 'al') {
                                                    echo Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date))) . ' ' . date('j, Y', strtotime($article->publish_date));
                                                } else {
                                                    echo date('j [$F] Y', strtotime($article->publish_date));
                                                }
                                                ?> - 
                                            </small>
                                        <? } ?>
                                        <?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?>
                                    </p>
                                </div>
                            </div>
                            <?
                            $i++;
                        }
                        ?>

                    <? } else { ?>
                        <p class="noresult">[$NoResults]</p>
                    <? } ?>
                </div>


                <div class="post-footer"> 
                    <div class="row thm-margin float-right">
                        <? if ($totalElements > $elements_per_page) { ?>
                            <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                                <? $prev = '<span class="ti-angle-left"></span>'; ?>
                                <? $next = '<span class="ti-angle-right"></span>'; ?>
                                <?php Pager::printPager($page, $totalElements, $elements_per_page, null, $prev, $next); ?>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
