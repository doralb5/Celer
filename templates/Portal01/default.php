<!DOCTYPE html>
<html lang="en">
    <head>
		<?php include 'header.php'; ?>
    </head>
    <body class="materials-skin" id="page-top" >
        <!-- PAGE LOADER -->
        <div class="se-pre-con"></div>
        <!-- *** START PAGE HEADER SECTION *** -->
        <header>
            <!-- START HEADER TOP SECTION -->
            <div class="header-top">
                <div class="container">
                    <div class="row">
						<? $this->getPosition('pos-header-top')->printContent(); ?>
                    </div> <!-- end of /. row -->
                </div> <!-- end of /. container -->
            </div>
            <!-- END OF /. HEADER TOP SECTION -->
            <!-- START MIDDLE SECTION -->
            <div class="header-mid hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
							<? Loader::loadModule('Generic/Logo')->execute(); ?>
                        </div>
                        <div class="col-sm-8">
							<? $this->getPosition('pos-header')->printContent(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OF /. MIDDLE SECTION -->

            <!-- START NAVIGATION -->

			<? $this->getPosition('pos-nav')->printContent(); ?>

            <!-- END OF/. NAVIGATION -->
        </header>
        <!-- *** END OF /. PAGE HEADER SECTION *** -->
        <!-- *** START PAGE MAIN CONTENT *** -->
        <main class="page_main_wrapper">

			<? if (WebPage::showContentHeading() && WebPage::getContentHeading() != '') { ?>
				<!-- START PAGE TITLE --> 
				<div class="page-title">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6">
								<h1><strong><?= WebPage::getContentHeading(); ?></strong></h1>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<? WebPage::$breadcrumb->render(); ?>
							</div>
						</div>
					</div>
				</div>
				<!-- END OF /. PAGE TITLE --> 
			<? } ?>

			<? $this->getPosition('pos-content-top')->printContent(); ?>
            <!-- END OF /. POST BLOCK SECTION -->
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p main-content">
                        <div class="theiaStickySidebar">

							<?
							if ($content != '') {
//                                echo "<div class='post-inner'>\n";
//                                    echo "<div class='post-body'>\n";
								echo $content;
//                                    echo "</div>\n";
//                                echo "</div>\n";
							}
							?>
							<? $this->getPosition('pos-content-middle-left')->printContent(); ?>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <div class="col-sm-4 col-p rightSidebar">
                        <div class="theiaStickySidebar">
							<? $this->getPosition('pos-content-middle-right')->printContent(); ?>
                        </div>
                    </div>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
						<? $this->getPosition('pos-content-middle-center')->printContent(); ?>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row row-m">
                    <div class="col-sm-8 main-content col-p">
                        <div class="theiaStickySidebar">
							<? $this->getPosition('pos-content-bottom-left')->printContent(); ?>
                        </div>
                    </div>
                    <div class="col-sm-4 rightSidebar col-p">
						<? $this->getPosition('pos-content-bottom-right')->printContent(); ?>
                    </div>
                </div>
            </div>
            <section class="articles-wrapper">
                <div class="container">
                    <div class="row row-m">
                        <div class="col-sm-8 main-content col-p">
                            <div class="theiaStickySidebar">
								<? $this->getPosition('pos-content-prefooter-left')->printContent(); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 rightSidebar col-p">
							<? $this->getPosition('pos-content-prefooter-right')->printContent(); ?>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
						<? $this->getPosition('pos-content-bottom-center')->printContent(); ?>
                    </div>
                </div>
            </div>
        </main>
        <!-- *** END OF /. PAGE MAIN CONTENT *** -->
        <!-- START FOOTER -->
        <footer>
            <div class="container">
                <div class="row">
					<? $this->getPosition('footer')->printContent(); ?>
                </div>
            </div>
        </footer>
        <!-- END OF /. FOOTER -->
        <!-- START SUB FOOTER -->
        <div class="sub-footer">
            <div class="container">
				<? $this->getPosition('footer-line')->printContent(); ?>
            </div>
        </div>
        <!-- END OF /. SUB FOOTER -->
    </body>
	<?php include 'footer.php'; ?> 
</html>