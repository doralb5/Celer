<!-- jquery ui js -->
<? HeadHtml::linkJS("jquery-ui.min.js"); ?>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<? HeadHtml::linkJS("bootstrap.min.js"); ?>
<!-- Bootsnav js -->
<? HeadHtml::linkJS("bootsnav.js"); ?>
<!-- theia sticky sidebar -->
<? HeadHtml::linkJS("theia-sticky-sidebar.js"); ?>
<!-- Skycons js -->
<? HeadHtml::linkJS("skycons.js"); ?>
<!-- youtube js -->
<? HeadHtml::linkJS("RYPP.js"); ?>
<!-- owl-carousel js -->
<? HeadHtml::linkJS("owl.carousel.min.js"); ?>
<!-- custom js -->
<? HeadHtml::linkJS("custom.js"); ?>
<script>
    $(document).ready(function () {
        //Skyicon
        var icons = new Skycons({"color": "#fff"}),
                list = [
                    "clear-day", "clear-night", "partly-cloudy-day",
                    "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                    "fog"
                ],
                i;

        for (i = list.length; i--; )
            icons.set(list[i], list[i]);

        icons.play();
    });
</script>  
<?
HeadHTML::printJSLinks();
HeadHTML::printScripts();
