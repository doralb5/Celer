<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? (CMSSettings::$favicon != '') ? HeadHTML::printFavicon(CMSSettings::$favicon) : ''; ?>

        <!-- jquery ui css -->
		<? HeadHTML::linkStylesheet('jquery-ui.min.css'); ?>
        <!-- Bootstrap -->
		<? HeadHTML::linkStylesheet('bootstrap.min.css'); ?>
        <!--Animate css-->
		<? HeadHTML::linkStylesheet('animate.min.css'); ?>
        <!-- Navigation css-->
		<? HeadHTML::linkStylesheet('bootsnav.css'); ?>
        <!-- youtube css -->
		<? HeadHTML::linkStylesheet('RYPP.css'); ?>
        <!-- font awesome -->
		<? HeadHTML::linkStylesheet('font-awesome.min.css'); ?>
        <!-- themify-icons -->
		<? HeadHTML::linkStylesheet('themify-icons.css'); ?>
        <!-- weather-icons -->
		<? HeadHTML::linkStylesheet('weather-icons.min.css'); ?>
        <!-- flat icon -->
		<? HeadHTML::linkStylesheet('flaticon.css'); ?>
        <!-- style css -->
		<? HeadHTML::linkStylesheet('style.css'); ?>
        <!-- owl-carousel -->
		<? HeadHTML::linkStylesheet('owl.carousel.css'); ?>
		<? //HeadHTML::linkStylesheet('owl.theme.css');?>
		<? HeadHTML::linkStylesheet('owl.theme.default.css'); ?>
		<? //HeadHTML::linkStylesheet('owl.transitions.css');?>
        <!-- custom css -->
		<? HeadHTML::linkStylesheet('custom.css'); ?>

		<? HeadHTML::printStylesheetLinks(); ?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<? HeadHtml::linkJS("jquery.min.js"); ?>

    </head>