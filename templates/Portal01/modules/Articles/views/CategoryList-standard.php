<div class="panel_inner categories-widget">
    <?if($title!=''){?>
    <div class="panel_header">
        <h4><?=$title?></h4>
    </div>
    <?}?>

    <? if(count($categories)){?>
    <div class="panel_body">
        <ul class="category-list">
            <? foreach ($categories as $cat) {?>
                <li><a href="<?=$cat->url?>"><?=$cat->category?> <span><?=$cat->total_articles?></span></a></li>
            <? } ?>
        </ul>
    </div>
    <? }?>
</div>
