<div class="panel_inner">
    <?if($title!=''){?>
        <div class="panel_header">
            <h4><?=$title?></h4>
        </div>
    <?}?>

    <? if(count($tags)){?>
    <div class="panel_body">
        <div class="tags-inner">
            <? foreach ($tags as $tag) {?>
                <a class="ui tag" href="<?=$tag['url']?>"><?=$tag['title']?></a>
            <?}?>
        </div>
    </div>
    <?}?>
</div>