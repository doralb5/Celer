<? require_once LIBS_PATH . 'StringUtils.php'; ?>
<?
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 255;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 255;
?>

<div class="last-news-onlytitle panel_inner">
    
    <?if($title!=''){?>
    <div class="panel_header">
        <h4><?=$title?></h4>
    </div>
    <?}?>
    
    <div class="panel-body">
        <ul>
            <? foreach ($articles as $article) { ?>
                <li>
                    <h5 class="title"><a href="<?= Utils::getComponentUrl('Articles/show_article') . "/" . Utils::url_slug($article->title . '-' . $article->id) ?>"><?= StringUtils::CutString(strip_tags($article->title), $taglia_title) ?></a></h5>
                    <span class="description"><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></span>
                </li>
            <? } ?>
        </ul>
    </div>
    
</div>

