<? //HeadHtml::linkStylesheet("owl.carousel.css"); ?>
<? //HeadHtml::linkJS("owl.carousel.js"); ?>

<? $itemc = (isset($parameters['itemc']) && $parameters['itemc'] > 0) ? $parameters['itemc'] : 5 ?>
<? $width = (isset($parameters['w']) && $parameters['w'] > 0) ? $parameters['w'] : 200 ?>
<? $height = (isset($parameters['h']) && $parameters['h'] > 0) ? $parameters['h'] : 80 ?>

<? if (count($banners)) { ?>
    <div class="owl-carousel owl-theme owl-image-edit-width">
        <? foreach ($banners as $banner) { ?>
            <div class="item text-center">
                <a href="<?= $banner->target_url ?>">
                    <img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $width, $height) ?>">
                </a>
            </div>
        <? } ?>
    </div>
<? } ?>


<script>
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            items: <?= $itemc ?>,
            loop: true,
            margin: 10,
            stagePadding: 0,
            nav: false,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:false,
            dots: false,
            pagination: false
        });
    });
</script>