<? HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/articles-style.css'); ?>
<?
require_once LIBS_PATH . 'StringUtils.php';
require_once LIBS_PATH . 'Pager.php';
?>
<? $show_date = (isset($parameters['show_date'])) ? $parameters['show_date'] : 1; ?>
<? $show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0; ?>
<? $show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0; ?>
<?
$w = (isset($parameters['w'])) ? $parameters['w'] : 800;
$h = (isset($parameters['h'])) ? $parameters['h'] : 400;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30;
$title = (isset($parameters['title'])) ? $parameters['title'] : '';
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 350;
$button = (isset($parameters['button'])) ? $parameters['button'] : 'primary';
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 2;
$col_size = 12 / $cols;
?>

<!--/* --------------- News List v2---------------*/-->

<style>
    .thumbnail {
        padding: 0px;
        margin-bottom: 0px;
        border: 0px solid white;
        border-radius: 0px;
    }

    .button_align {
        text-align: center;
    }

    .fff {
        margin: 30px 0;
        box-shadow: 0 1px 2px rgba(0,0,0,0.2);
        transition: .5s;
    }
    .fff:hover {
    -moz-box-shadow: 0px 5px 10px 0px rgba(0,0,0,0.25);
    -webkit-box-shadow: 0px 5px 10px 0px rgba(0,0,0,0.25);
    box-shadow: 0px 5px 10px 0px rgba(0,0,0,0.2);
    /* transform: perspective(400px); */
}
    .fff .caption {
        padding: 0 10px 10px 10px;
    }

    .fff .meta-tags {
        padding: 5px 10px 5px 10px;
        border-bottom: 1px solid #efefef;
    }
</style>
<? if ($title != '') { ?>
<!--    <div class="main-title-description">
        <h1 class="page-header header-title"><?= $title ?></h1>
    </div>-->
<? } ?>

<? if (count($articles)) { ?>
    <div id="article-list">
        <div class="item items">
            <div class="thumbnails">
                <div class="row">
                    <?
                    $i = 0;
                    foreach ($articles as $article) {
                        ?>

                        <? if ($i % $cols == 0 && $i != 0) { ?>
                            <div class="clearfix"></div>
                        <? } ?>

                        <div class="col-md-<?= $col_size ?> news-item">
                            <div class="fff">
                                <? if ($article->image != '') { ?>
                                    <!--/* --------------- Image ---------------*/-->
                                    <div class="thumbnail">
                                        <a class="hover-img"
                                           href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">
                                               <?
                                               $categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : '');
                                               ?>
                                            <img alt=""
                                                 src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", $w, $h, array('zc' => 1)) ?>"/>
                                        </a>
                                    </div>
                                    <!--/* --------------- Tags ---------------*/-->
                                    <div class="meta-tags">
                                        <? if ($show_date) { ?>
                                            <span>
                                                <?
                                                if (FCRequest::getLang() == 'al') {
                                                    echo Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date))) . ' ' . date('j, Y', strtotime($article->publish_date));
                                                } else {
                                                    echo date('j [$F] Y', strtotime($article->publish_date));
                                                }
                                                ?>
                                            </span>
                                        <? } ?>
                                        <? if ($show_visitors) { ?>
                                            <span><i class="fa fa-eye"></i> 0</span>
                                        <? } ?>
                                        <? if ($show_commentNr) { ?>
                                            <span><i class="fa fa-comments"></i>
                                                <a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">0</a>
                                            </span>
                                        <? } ?>
                                    </div>
                                <? } ?>
                                <!--/* --------------- Content ---------------*/-->
                                <div class="caption">
                                    <h4 class="title"><a href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>"><?= strip_tags($article->title) ?></a></h4>

                                    <p class="content"><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></p>
                                    <div class="button_align">
                                        <a class="btn btn-sm btn-<?= $button ?> btn-read-more"
                                           href="<?= Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title . '-' . $article->id)) ?>">[$ReadMore]</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <?
                        $i++;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<? } else { ?>
    <p class="noresult">[$NoResults]</p>
<? } ?>
<!--/* --------------- Pagination ---------------*/-->
<? if ($totalElements > $elements_per_page) { ?>
    <div class="col-md-12 pagination">
        <?php Pager::printPager($page, $totalElements, $elements_per_page); ?>
    </div>
<? } ?>


<div class="col-sm-12 col-p  main-content">
    <div class="theiaStickySidebar">
        <div class="post-inner">
            <!--post header-->
            <? if ($title != '') { ?>
                <div class="post-head">
                    <h2 class="title"><strong> <?= $title?></strong></h2>
                </div>
            <?}?>
            <!-- post body -->
            
            <? if(count($articles)){?>
             <div class="post-body">
                <div class="news-list-item articles-list">
                    
                    <?if ($article->image != ''){?>
                        <div class="img-wrapper">
                            <a href="<?=  Utils::getComponentUrl('Articles/show_article/' . Utils::url_slug($article->title .'-' . $article->id)) ?>" class="thumb">
                            <img src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", $w, $h, array('zc' => 1)) ?>" alt="" class="img-responsive"></a>
                        </div>
                    <?}?>
                    
                    <div class="post-info-2">
                        <h4><a href="#" class="title">There are many variations of passages of Lorem Ipsum available, but the majority have</a></h4>
                        <ul class="authar-info">
                            <li><i class="ti-timer"></i> May 15, 2016</li>
                            <li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>
                        </ul>
                        <p class="hidden-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley...</p>
                    </div>
                </div>
            </div>
            <!-- /. post body -->
            <!-- Post footer -->
            <div class="post-footer"> 
                <div class="row thm-margin">
                    <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                        <!-- pagination -->
                        <ul class="pagination">
                            <li class="disabled"><span class="ti-angle-left"></span></li>
                            <li class="active"><span>1</span></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li class="disabled"><span class="extend">...</span></li><li>
                            <li><a href="#">12</a></li>
                            <li><a href="#"><i class="ti-angle-right"></i></a></li>
                        </ul> <!-- /.pagination -->
                    </div>
                </div>
            </div> 
            <? } else {?>  
                <p class=""> [$NoResults]</p>
            <? } ?>
        </div>
        
    </div>
</div>