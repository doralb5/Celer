<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? //HeadHTML::printFavicon(); ?>

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet('bootstrap.min.css'); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHTML::linkStylesheet('responsive.css'); ?>
		<? HeadHtml::linkStylesheet('custom.css'); ?>



		<? HeadHTML::printStylesheetLinks(); ?>
		<? HeadHtml::linkJS("jquery-1.12.4.min.js"); ?>

      <!-- <style type="text/css">
            #banner {
                background: url('<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>') no-repeat fixed;
                background-size: contain;
            }
        </style>-->
    </head>
    <body>
        <div id="fh5co-wrapper">
            <div id="fh5co-page">
                <header id="fh5co-header-section" class="sticky-banner">
                    <div class="container-fluid">
                        <div class="nav-header">
							<? $this->getPosition('pos-nav')->printContent(); ?>
                        </div>
                    </div>
                </header>

                <!-- main content-->
                <div id="main-content">
					<? if ($this->getPosition('pos-banner')->countBlocks() > 0 || $this->getPosition('pos-banner-info')->countBlocks() > 0) { ?>
						<!--banner-->
						<section id="banner" class="banner">
							<div class="bg-color">
								<? if ($this->getPosition('pos-banner')->countBlocks() > 0) { ?>
									<div class="banner-background">
										<? $this->getPosition('pos-banner')->printContent(); ?>
									</div>
								<? } ?>
								<? if ($this->getPosition('pos-banner-info')->countBlocks() > 0) { ?>
									<div class="container">
										<div class="row">
											<div class="banner-info">
												<? $this->getPosition('pos-banner-info')->printContent(); ?>
											</div>
										</div>
									</div>
								<? } ?>
							</div>
						</section>
						<!--/ banner-->
					<? } ?>


                    <div class="ulockd-inner-home">
                        <div class="container text-center">
                            <div class="row">
                                <div class="ulockd-inner-conraimer-details">
									<? if (WebPage::showContentHeading() && WebPage::getContentHeading() != '') { ?>
										<div class="col-md-12">
											<div id="#content-heading" class="content-heading">
												<h2 class="head-title">
													<?= WebPage::getContentHeading(); ?>
												</h2>

											</div>
										</div>
									<? } ?>
                                    <div class="col-md-12">
                                        <div class="ulockd-icd-layer">
											<? WebPage::$breadcrumb->render(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

								<? if (WebPage::showContentHeading() && WebPage::getContentHeading() != '') { ?>
									<div id="#content-heading" class="content-heading">
										<h2 class="head-title">
											<?= WebPage::getContentHeading(); ?>
										</h2>
										<hr class="botm-line" />
									</div>
								<? } ?>

                                <!-- ### pos-ctop ### -->
								<? $this->getPosition('pos-ctop')->printContent(); ?>
                                <!-- ### //pos-ctop ### -->

                                <!-- ### content ### -->
								<?= $content; ?>
                                <!-- ### //content ### -->
                            </div>
                        </div>
                    </div>

                    <section>
                        <!-- ### pos-ctop-full ### -->
						<? $this->getPosition('pos-ctop-full')->printContent(); ?>
                        <!-- ### //pos-ctop-full ### -->
                    </section>

                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- ### pos-cmiddle ### -->
								<? $this->getPosition('pos-cmiddle')->printContent(); ?>
                                <!-- ### //pos-cmiddle ### -->
                            </div>
                        </div>
                    </div>

                    <section>
                        <div class="full-width">
                            <!-- ### pos-cmiddle-full ### -->
							<? $this->getPosition('pos-cmiddle-full')->printContent(); ?>
                            <!-- ### //pos-cmiddle-full ### -->
                        </div>
                    </section>
                    <div class="container">
                        <!-- ### pos-cbottom ### -->
						<? $this->getPosition('pos-cbottom')->printContent(); ?>
                        <!-- ### //pos-cbottom ### -->
                    </div>
                </div>

                <section>
                    <!-- ### pos-cbottom-full ### -->
					<? $this->getPosition('pos-cbottom-full')->printContent(); ?>
                    <!-- ### //pos-cbottom-full ### -->
                </section>

                <!--footer-->
                <footer id="footer">
					<? if ($this->getPosition('top-footer')->countBlocks()) { ?>
						<div class="top-footer container-fluid">
							<? $this->getPosition('top-footer')->printContent() ?>
						</div>
					<? } ?>
                    <div class="footer container">
						<? $this->getPosition('footer')->printContent() ?>
                    </div>

                </footer>
                <div class="footer-line">
					<? $this->getPosition('footer-line')->printContent() ?>	
                </div>
                <!--/ footer-->

				<? HeadHtml::linkJS("bootstrap.min.js"); ?>
				<? HeadHtml::linkJS("bootsnav.js"); ?>
				<? HeadHtml::linkJS("parallax.js"); ?>
				<? HeadHtml::linkJS("scrollto.js"); ?>
				<? HeadHtml::linkJS("jquery-scrolltofixed-min.js"); ?>
				<? HeadHtml::linkJS("jquery.counterup.js"); ?>
				<? HeadHtml::linkJS("gallery.js"); ?>
				<? HeadHtml::linkJS("wow.min.js"); ?>
				<? HeadHtml::linkJS("slider.js"); ?>
				<? HeadHtml::linkJS("video-player.js"); ?>
				<? HeadHtml::linkJS("jquery.barfiller.js"); ?>
				<? // HeadHtml::linkJS("timepicker.js"); ?>
				<? HeadHtml::linkJS("tweetie.js"); ?>

				<? //HeadHtml::linkJS("color-switcher.js"); ?>
				<? HeadHtml::linkJS("script.js"); ?>


				<? // HeadHtml::linkJS("fancybox.js"); ?>
				<? // HeadHtml::linkJS("flipclock.min.js"); ?>
				<? // HeadHtml::linkJS("fullcalendar.min.js"); ?>
				<? // HeadHtml::linkJS("googlemaps.js"); ?>
				<? // HeadHtml::linkJS("jflickrfeed.min.js"); ?>
				<? // HeadHtml::linkJS("jquery-ui.min.js"); ?>
				<? // HeadHtml::linkJS("validator.js"); ?>

				<? HeadHTML::printJSLinks(); ?>

                <div class="powered-by-container container">
                    <div class="row">
                        <div class="col-md-12 text-center space-bottom">
                            <a href="<?= BRAND_URL ?>"><img src="<?= BRAND_LOGO ?>" /></a>
                        </div>
                    </div>
                </div>
                </body>
                </html>
