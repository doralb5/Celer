<a class="scrollToHome" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- Wrapper End -->
<? HeadHtml::linkJS("parallax.js"); ?>
<? HeadHtml::linkJS("scrollto.js"); ?>
<? HeadHtml::linkJS("jquery-scrolltofixed-min.js"); ?>
<? HeadHtml::linkJS("jquery.counterup.js"); ?>
<? HeadHtml::linkJS("gallery.js"); ?>
<? HeadHtml::AddJS("slider.js"); ?>
<? HeadHtml::linkJS("wow.min.js"); ?>
<? HeadHtml::linkJS("video-player.js"); ?>
<? HeadHtml::linkJS("jquery.barfiller.js"); ?>
<? HeadHtml::linkJS("timepicker.js"); ?>
<? //HeadHtml::linkJS("tweetie.js"); ?>
<!-- Custom script for all pages --> 
<? //HeadHtml::linkJS("color-switcher.js"); ?>

<? HeadHtml::linkJS("script.js"); ?>

<div class="powered-by-container container-fluid ulockd-l2-powered-by">
    <div class="row">
        <div class="col-md-12 text-center space-bottom">
            <a href="<?= BRAND_URL ?>"><img src="<?= BRAND_POWERED_INVERSE ?>" /></a>
        </div>
    </div>
</div>

</body>
</html>