<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
        <!-- css file -->
		<? HeadHTML::linkStylesheet('bootstrap.min.css'); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>    
		<? HeadHtml::linkStylesheet("colors/default.css"); ?>
        <!-- Responsive stylesheet -->
		<? HeadHTML::linkStylesheet('responsive.css'); ?>
		<? HeadHtml::linkStylesheet('custom.css'); ?>
        <!-- Title -->
		<? HeadHTML::printTitle(); ?>
        <!-- Favicon -->
		<? (CMSSettings::$favicon != '') ? HeadHTML::printFavicon(CMSSettings::$favicon) : ''; ?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

		<? HeadHTML::printStylesheetLinks(); ?>


		<? HeadHtml::linkJS("jquery-1.12.4.min.js"); ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("bootsnav.js"); ?>

		<? HeadHTML::printJSLinks(); ?>

		<?= $this->template->head_extra; ?>

    </head>
    <body>

        <div class="wrapper">