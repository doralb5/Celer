<? include 'header.php'; ?>

<div class="preloader"></div>
<div class="header-top-two">
    <div class="container">
		<? $this->getPosition('header-top')->printContent(); ?>
    </div>
</div>

<!-- Header Styles -->
<div class="header-nav">
    <div class="main-header-nav-two navbar-scrolltofixed">
        <div class="container">
			<? $this->getPosition('pos-nav')->printContent(); ?>
        </div>
    </div>
</div>

<!-- Slider -->
<div class="ulockd-home-slider">
    <div class="container-fluid">
        <div class="row">
			<? $this->getPosition('pos-banner')->printContent(); ?>
        </div>
    </div>
</div>

<? if (WebPage::showContentHeading() && WebPage::getContentHeading() != '') { ?>
	<div class="ulockd-inner-home">
		<div class="text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<div id="#content-heading" class="content-heading">
							<h2 class="head-title">
								<?= WebPage::getContentHeading(); ?>
							</h2>
						</div>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<? WebPage::$breadcrumb->render(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<? } ?>

<!-- ### pos-ctop ### -->
<div class='container'>
	<? $this->getPosition('pos-ctop')->printContent(); ?>
</div>
<!-- ### //pos-ctop ### -->

<!-- ### pos-ctop-full ### -->
<? $this->getPosition('pos-ctop-full')->printContent(); ?>
<!-- ### //pos-ctop-full ### -->

<!-- ### content ### -->
<div class="container">
	<?= $content; ?>
    <!-- ### //content ### -->
</div>
<!-- ### pos-cbottom ### -->
<div class='container'>
	<? $this->getPosition('pos-cbottom')->printContent(); ?>
</div>
<!-- ### //pos-cbottom ### -->

<!-- ### pos-cbottom-full ### -->
<? $this->getPosition('pos-cbottom-full')->printContent(); ?>
<!-- ### //pos-cbottom-full ### -->

<!-- ### top-footer ### -->
<div class='container'>
	<? $this->getPosition('top-footer')->printContent(); ?>
</div>
<!-- ### //top-footer ### -->
<!-- ### footer ### -->
<section class="ulockd-footer style2">
    <div class="container">
		<? $this->getPosition('footer')->printContent(); ?>
    </div>
</section>
<!-- ### //footer ### -->

<section class="ulockd-l2-copy-right">
    <div class="container">
		<? $this->getPosition('footer-line')->printContent(); ?>
    </div>
</section>

<? include 'footer.php'; ?>