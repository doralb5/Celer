<? if (count($banners)) { ?>
    <? $w = (isset($parameters['w']) && $parameters['w'] > 0) ? $parameters['w'] :  2000?>
    <? $h = (isset($parameters['h']) && $parameters['h'] > 0) ? $parameters['h'] : 700 ?>
    <? $interval = (isset($parameters['interval']) && $parameters['interval'] > 0) ? $parameters['interval'] : 5000 ?>
    <? $show_caption = (isset($parameters['show_caption'])) ? $parameters['show_caption'] : 0 ?>
    <? $show_controls = (isset($parameters['show_controls'])) ? $parameters['show_controls'] : 0 ?>
    
<style>
        .carousel-fade .carousel-inner .next,
        .carousel-fade .carousel-inner .prev,
        .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
            left: 0;
            transform: translate3d(0, 0, 0);
        }

        /*Background Carousel*/

        .carousel-fade .carousel-inner .item {
            transition-property: opacity;
        }
        .carousel-fade .carousel-inner .item,
        .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
            opacity: 0;
        }
        .carousel-fade .carousel-inner .active,
        .carousel-fade .carousel-inner .next.left,
        .carousel-fade .carousel-inner .prev.right {
            opacity: 1;
        }
        .carousel-fade .carousel-inner .next,
        .carousel-fade .carousel-inner .prev,
        .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
            left: 0;
            transform: translate3d(0, 0, 0);
        }
        
        .description {
    position: absolute;
    z-index: 5;
    display: block;
    top: 0;
    padding: 20px;    
}

</style>
    
    <div id="myCarousel_<?= $block_id ?>" class="carousel-fade carousel slide ulockd-main-slider2" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?
            $i = 0;
            foreach ($banners as $banner) {
                ?>
                <li data-target="#myCarousel_<?= $block_id ?>" data-slide-to="<?= $i ?>" class="<?= ($i == 0) ? 'active' : '' ?>"></li>
                <?
                $i++;
            }
            ?>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">

            <?
            $i = 0;
            foreach ($banners as $banner) {
                ?>
                <? HeadHTML::addMetaProperty("og:image", Utils::genThumbnailUrl('banners/' . str_replace('%2F', '/', urlencode($banner->filename)), 0,0,[],true)); ?>

                                                <!--            <div class="item <?= ($i == 0) ? 'active' : '' ?>">
                                                <div class="fill" style="background-image:url('<?= MEDIA_ROOT . 'banners/' . $banner -> filename ?>');"></div>
                                                <div class="carousel-caption">
                                                </div>
                                                </div>-->

                <div class="item <?= ($i == 0) ? 'active' : '' ?>">
                    <? if ($banner->target_url != '') { ?>
                        <a href="<?= $banner -> target_url ?>"><img src="<?= Utils::genThumbnailUrl('banners/' . $banner -> filename, $w, $h, array('zc' => 1)) ?>" width="100%"/></a>
                    <? } else { ?>
                        <img src="<?= Utils::genThumbnailUrl('banners/' .$banner -> filename, $w, $h, array('zc' => 1)) ?>" width="100%"/>
                    
                        <div class="description caption style1">    
                            <?if ($banner->text1 || $banner->text2 != '' ||  $banner->text3 != ''){?>
                                <div class="ulockd-slider-text1 wow fadeInUp" data-wow-duration="300ms" data-wow-delay=".3s" style="visibility: visible; animation-duration: 300ms; animation-delay: 0.3s; animation-name: fadeInUp;"><?= $banner->text1 ?></div>
                                <div class="ulockd-slider-text2 wow fadeInUp" data-wow-duration="600ms" data-wow-delay=".6s" style="visibility: visible; animation-duration: 600ms; animation-delay: 0.6s; animation-name: fadeInUp;"><?= $banner->text2 ?></div>
                                <div class="ulockd-slider-text3 wow fadeInUp" data-wow-duration="900ms" data-wow-delay=".9s" style="visibility: visible; animation-duration: 900ms; animation-delay: 0.9s; animation-name: fadeInUp;"><?= $banner->text3 ?></div>
                            <?}?>
                        </div>    
                    <? } ?>
                    <? if ($show_caption) { ?>
                        <div class="carousel-caption">
                            <?= $banner -> name ?>
                        </div>
                    <? } ?>

                </div>

                <?
                $i++;
            }
            ?>

        </div>
        
        <!-- Controls -->
        <? if ($show_controls) { ?>
        
            <a class="left carousel-control " href="#myCarousel_<?= $block_id ?>" data-slide="prev"><span class="icon-prev"></span>  
            <a class="right carousel-control " href="#myCarousel_<?= $block_id ?>" data-slide="next"><span class="icon-next"></span></a>
       <? } ?> 
        
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myCarousel_<?= $block_id ?>').carousel({
                interval: <?= $interval ?>,
                pause: 'hover',
                cycle: true
            });
        });
    </script>

<? } ?>
