<? 
$show_brand = isset($parameters['show_brand']) ? $parameters['show_brand'] : false;
$show_logo = isset($parameters['show_logo']) ? $parameters['show_logo'] : true;
?>

<nav class="navbar navbar-default bootsnav ulockd-menu-style2">
			        <div class="container ulockd-pdng0">
			            <!-- Start Header Navigation -->
			            <div class="navbar-header ulockd-ltwo">
			                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar_<?= $block_id ?>">
			                    <i class="fa fa-bars"></i>
			                </button>
                                        <? if ($show_logo) {
                                        $logo = WEBROOT . MEDIA_ROOT . CMSSettings::$logo;
                                        $logo_inverse = WEBROOT . MEDIA_ROOT .  ( (CMSSettings::$logo_inverse != '') ? CMSSettings::$logo_inverse : CMSSettings::$logo );
                                        ?>
                                        <a class="navbar-brand ulockd-main-logo2" href="<?= Utils::genUrl('/');?>">
                                            <?if(CMSSettings::$logo != ''){?><img src="<?= $logo ?>" class="logo img-responsive" alt=""><?}?>
                                        </a>
                                        <? }?>
			            </div>
			            <!-- End Header Navigation -->

			            <!-- Collect the nav links, forms, and other content for toggling -->
			            <div class="collapse navbar-collapse" id="navbar_<?= $block_id ?>">
			                <ul class="nav navbar-nav navbar-right">
                                            
                                            <? printRecursiveMenuItems($MenuItems);?>
			                    
			                </ul>
			            </div>
			            <!-- /.navbar-collapse -->
			        </div>
			    </nav>




<?

    function printRecursiveMenuItems($items) {
        foreach ($items as $item) {
            
            $target = getTargetByItem($item);
            switch ($item->type) {
                case "Page" :
                    $target = Utils::genUrl($item->page->getAlias());
                    break;
                case "External Link" :
                    $target = $item->target;
                    break;
                case "No Link" :
                    $target = '#';
                    break;
                    
            }
            
            $active_class = (isActiveRecursively($item)) ? "active" : "";
            
            if($item->type == 'Divider') {
                echo "<li class=\"divider $active_class\"></li>\n";
            } else {
                if(count($item->subitems) == 0){
                    echo "<li class=\"$active_class\">";
                    echo "<a href=\"$target\" class=\"\">{$item->item}</a>";
                    echo "</li>\n";
                } else {
                    echo "<li class=\"dropdown $active_class\">";
                    echo "<a href=\"$target\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">{$item->item}</a>\n\n";
                    echo "<ul class=\"dropdown-menu\">\n";
                    printRecursiveMenuItems($item->subitems);
                    echo "</ul>\n\n";
                    echo "</li>\n";
                }
            }
        }
    }
    
    function getTargetByItem($item) {
        switch ($item->type) {
            case "Page" :
                $target = Utils::genUrl($item->page->getAlias());
                break;
            case "External Link" :
                $target = $item->target;
                break;
            case "No Link" :
                $target = '#';
                break;
            default :
                $target = '';
        }
        return $target;
    }
    
    function isActiveRecursively($item) {
        $target = getTargetByItem($item);
        $active = Utils::isActivePage($target);
        if($active)
            return true;
        
        if(count($item->subitems)) {
            foreach ($item->subitems as $subitem) {
                $active = isActiveRecursively($subitem);
                if($active) {
                    return true;
                }
            }
        }
        return false;
    }

?>