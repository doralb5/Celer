<!-- Footer Section -->



<footer class="footer" id="footer"><!-- Section id-->
    <div class="container">
        <div class="row">
			<? $this->getPosition('footer')->printContent(); ?>
        </div>
    </div>
    <div class="copyright clearfix">
        <div class="">
			<? $this->getPosition('footer-line')->printContent(); ?>
        </div>
    </div>
</footer>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<? HeadHtml::linkJS("ie10-viewport-bug-workaround.js"); ?>

<!-- Bootstrap Core JavaScript -->
<? HeadHtml::linkJS("bootstrap.min.js"); ?>

<!-- Plugin JavaScript -->
<? HeadHtml::linkJS("jquery-easing/1.3/jquery.easing.min.js"); ?>
<? HeadHtml::linkJS("scrollreveal.min.js"); ?>

<!-- Theme JavaScript -->
<? HeadHtml::linkJS("theme.js"); ?>

<!-- custom JavaScript -->
<? HeadHtml::linkJS("custom.js"); ?>

<!-- FlexSlider -->
<? HeadHtml::linkJS("jquery.flexslider-min.js"); ?>


<?
HeadHTML::printJSLinks();
HeadHTML::printScripts();
?>
<div class="powered-by-container container">
    <div class="row">
        <div class="col-md-12 text-center space-bottom">
            <a href="<?= BRAND_URL ?>"><img src="<?= BRAND_LOGO ?>" /></a>
        </div>
    </div>
</div>
</body>
</html>