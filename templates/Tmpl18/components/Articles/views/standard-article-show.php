<? HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/articles-style.css'); ?>
<? HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/social-media-share.css'); ?>


<?
require_once LIBS_PATH . 'StringUtils.php';
$categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : '');
?>
<? HeadHTML::addMetaProperty('og:title', $article->title); ?>
<? HeadHTML::addMetaProperty('og:type', 'article'); ?>
<? HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl("articles/{$article->id_category}/" . urlencode($article->image), 1000, 0, array(), true)); ?>
<? HeadHTML::addMetaProperty('og:url', "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>
<? HeadHTML::addMetaProperty('og:description', StringUtils::CutString(strip_tags($article->content), 200)); ?>

<?
/* * *************************** modificare ******************** */
if (!is_null($article->id_section)) {
    $show_Author = false;
    $show_Date = false;
    $show_ShareButton = false;
    $show_Comments = false;
}
/* * ************************************************************ */

$show_Title = ($article->show_title == 'no') ? false : true;
$show_Subtitle = ($article->show_subtitle == 'no') ? false : true;
$show_Author = ($article->show_author == 'no') ? false : true;
$show_Date = ($article->show_date == 'no') ? false : true;
$show_ShareButton = ($article->show_share == 'no') ? false : true;
$show_Comments = ($article->show_comments == 'no') ? false : true;
$show_visitors = (isset($parameters['show_visitors'])) ? $parameters['show_visitors'] : 0;
$show_commentNr = (isset($parameters['show_commentNr'])) ? $parameters['show_commentNr'] : 0;
?>
<section class="blog-single" id="blog_s_post"><!-- Section id-->
    <div class="container">
         <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="blog-posts single-post">
                    <article class="post post-large blog-single-post">
                            <div class="post-image">
                                <img class="img-responsive" src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/{$article->image}", 1500, 0, array()) ?>" alt="">
                            </div>
                          <? if ($show_Date) { ?>
                            <div class="post-date">
                                <?
                                    if (FCRequest::getLang() == 'al') {
                                        echo '<span class="day">'. date('j', strtotime($article->publish_date)) .'</span>'. ' ' .'<span class="month">' . Utils::AlbanianWordMonth(date('m', strtotime($article->publish_date))) .'</span>';
                                    } else {
                                        echo '<span class="day">'. date('j', strtotime($article->publish_date)) .'</span>'. ' ' .'<span class="month">' . date('[$F]', strtotime($article->publish_date)) .'</span>';
                                    }
                                ?>
                            </div>
                          <?}?>
                        <div class="post-content">
                            <h2><?= $article->title ?></h2>
                             <? if ($show_Author) { ?>
                                <div class="post-meta">
                                    <span> [$by]   <?= $article->author; ?></span>
                                </div>
                             <?}?>
                            <p><?= $article->content ?></p>
                        </div>
                        <div class="additional-images">
                          <?
                            foreach ($article->images as $img) {
                                $categoryPath = ((!is_null($article->id_category)) ? "/{$article->id_category}" : '');
                                ?>
                                <div class="col-md-2 col-sm-3 col-xs-6 single-image">
                                    <a class="thumbnail"
                                       href="<?= Utils::genThumbnailUrl("articles{$categoryPath}/additional/{$img->image}", 700, 0) ?>"
                                       data-lightbox="lightbox" data-gallery="multiimages" data-title="">
                                        <img class="additional-img"
                                             src="<?= Utils::genThumbnailUrl("articles{$categoryPath}/additional/{$img->image}", 200, 200, array('zc' => '1')) ?>"/></a>
                                </div>
                            <? } ?>
                        </div>    
                    </article>
                </div>
            </div>        
        </div>
 
        <? if ($show_ShareButton) { ?>   
            <div class="share-buttons-container">
                <h3 class="share-text"><i class="fa fa-share"></i>[$share_in_social_media]</h3>
                <ul class="social_links_second list-unstyled">
                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'FacebookShare',600,250); return false;" target="_blank"" class="btn btn_facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/home?status=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'LinkedinShare',600,250); return false;" target="_blank" class="btn btn_twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://plus.google.com/share?url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'GooglePlusShare',600,250); return false;" target="_blank"" class="btn btn_google-plus"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= htmlentities(WebPage::currentUrl()) ?>" OnClick="PopupCenter(this.href,'LinkedinShare',600,250); return false;"  target="_blank" class="btn btn_linkedin"><i class="fa fa fa-linkedin"></i></a></li>
                </ul>    
            </div>
        <? } ?>
       
    </div>
</section>


