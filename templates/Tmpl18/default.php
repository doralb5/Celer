<?php include 'header.php'; ?>

<? if (WebPage::showContentHeading() && WebPage::getContentHeading() != '') { ?>
	<div class="page_title_ctn"> 
		<div class="container">
			<h2><?= WebPage::getContentHeading(); ?></h2>
			<? WebPage::$breadcrumb->render(); ?>
		</div>
	</div>
<? } ?>

<!-- ### pos-ctop ### -->
<? $this->getPosition('pos-ctop')->printContent(); ?>
<!-- ### //pos-ctop ### -->
<div class="container">
    <!-- ### content ### -->
	<?= $content; ?>
    <!-- ### //content ### -->
</div>

<!-- ### pos-ctop-full ### -->
<? $this->getPosition('pos-ctop-full')->printContent(); ?>
<!-- ### //pos-ctop-full ### -->


<? if ($this->getPosition('pos-cmiddle')->countBlocks() > 0) { ?>
	<!-- ### pos-cmiddle ### -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<? $this->getPosition('pos-cmiddle')->printContent(); ?>
			</div>
		</div>
	</div>
	<!-- ### //pos-cmiddle ### -->
<? } ?>

<!-- ### pos-cmiddle-full ### -->
<? $this->getPosition('pos-cmiddle-full')->printContent(); ?>
<!-- ### //pos-cmiddle-full ### -->

<? if ($this->getPosition('pos-cbottom')->countBlocks() > 0 || ($this->getPosition('pos-cbottom-full')->countBlocks() )) { ?>
	<!-- ### pos-cbottom ### -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<? $this->getPosition('pos-cbottom')->printContent(); ?>
			</div>
		</div>
	</div>
	<!-- ### //pos-cbottom ### -->


	<!-- ### pos-cbottom-full ### -->
	<? $this->getPosition('pos-cbottom-full')->printContent(); ?>
	<!-- ### //pos-cbottom-full ### -->
<? } ?>

<?php include 'footer.php'; ?>      
