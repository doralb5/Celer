<!-- top-footer-->
<? if ($this->getPosition('top-footer')->countBlocks()) { ?>
	<div class="top-footer">
		<? $this->getPosition('top-footer')->printContent() ?>
	</div>
<? } ?>
<!-- //top-footer -->



<footer class="footerOne">
	<? $this->getPosition('footer')->printContent() ?>

    <div class="footer-bottom-section">
        <div class="container">
            <div class="row">
                <hr class="tail">
                <div class="col-md-12">
					<? $this->getPosition('footer-line')->printContent() ?>	
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/ footer-->




<div class="container powered-by-container">
    <div class="row">
        <div class="col-md-12 text-center space-bottom">
            <a href="<?= BRAND_URL ?>"><img src="<?= BRAND_LOGO ?>" /></a>
        </div>
    </div>
</div>

<!-- Javascript -->
<? HeadHtml::linkJS("ie10-viewport-bug-workaround.js"); ?>
<? HeadHTML::linkJS("jquery-1.12.4.min.js") ?>
<? HeadHtml::linkJS("bootstrap.min.js"); ?>
<? HeadHtml::linkJS("divineartnav.js"); ?>
<? HeadHtml::linkJS("vendor/prettyPhoto/js/jquery.prettyPhoto.js"); ?>
<? HeadHtml::linkJS("vendor/masterslider/jquery.easing.min.js"); ?>
<? HeadHtml::linkJS("vendor/masterslider/masterslider.min.js"); ?>
<? HeadHtml::linkJS("custom.js"); ?>
<? HeadHtml::linkJS("demo.js"); ?>
<? HeadHtml::linkJS("wow.min.js"); ?>

<? HeadHtml::printJSLinks(); ?>

<? HeadHtml::printScripts(); ?>

<script>
    wow = new WOW(
            {
                boxClass: 'wow',
                animateClass: 'animated',
                offset: 30,
                mobile: true,
                live: true
            }
    )
    wow.init();
</script>
</body>
</html>
