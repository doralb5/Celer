<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? (CMSSettings::$favicon != '') ? HeadHTML::printFavicon(CMSSettings::$favicon) : ''; ?>
        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("vendor/prettyPhoto/css/prettyPhoto.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("skins/all-skins.css"); ?>
		<? HeadHtml::linkStylesheet("custom.css"); ?>
		<? HeadHtml::linkStylesheet("vendor/masterslider/style/masterslider.css"); ?>
		<? HeadHtml::linkStylesheet("vendor/masterslider/skins/default/style.css"); ?>
		<? HeadHtml::linkStylesheet("vendor/masterslider/style/ms-fullscreen.css"); ?>
		<? HeadHtml::linkStylesheet("vendor/masterslider/style/style.css"); ?>
		<? HeadHtml::linkStylesheet("animate.css"); ?>


		<? HeadHTML::printStylesheetLinks(); ?>

		<?= $this->getTemplate()->head_extra; ?>

    </head>
    <body>
		<? if ($this->getPosition('pos-nav')->countBlocks() > 0) { ?>
			<div class="fixed-top <?= ($this->getPosition('pos-banner')->countBlocks() > 0) ? 'no-background' : ''; ?>">
				<? $this->getPosition('pos-nav')->printContent(); ?>
			</div>    
		<? } ?>

		<? if ($this->getPosition('pos-banner')->countBlocks() > 0) { ?>
			<!--Slider-->
			<section class="dart-no-padding-tb">
				<? if ($this->getPosition('pos-banner')->countBlocks() > 0) { ?>
					<? $this->getPosition('pos-banner')->printContent(); ?>
				<? } ?>
			</section>
			<!--/ Slider-->
		<? } else { ?>
			<div class="nav-space"></div>
		<? } ?>
        