//[Master Javascript]

//Project:	Bonito - Multipage Html Responsive Template
//Version:	1.1
//Last change:	15/08/2017
//Primary use:	Bonito - Multipage Html Responsive Template 


//Template script here

jQuery(function ($) {
    "use strict";

// Counter
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });

}); // End of use strict




