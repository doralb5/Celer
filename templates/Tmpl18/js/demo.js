//[Master Javascript]

//Project:	Bonito - Multipage Html Responsive Template
//Version:	1.1
//Last change:	15/08/2017
//Primary use:	Bonito - Multipage Html Responsive Template 


//Template script here

jQuery(function ($) {
    "use strict";


// Counter
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });



// prettyPhoto
    $("a[rel^='alternate']").prettyPhoto();

//Filter	
    $(".filter-button").on('click', function (event) {

        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');

        var value = $(this).attr('data-filter');

        if (value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        } else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.' + value).hide('3000');
            $('.filter').filter('.' + value).show('3000');

        }
    });



}); // End of use strict




