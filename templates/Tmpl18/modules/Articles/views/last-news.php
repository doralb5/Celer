<? HeadHTML::addStylesheet(WEBROOT . $this->view_path . 'css/lastnews.css'); ?>

<? require_once LIBS_PATH . 'StringUtils.php'; ?>

<?
$w = (isset($parameters['w'])) ? $parameters['w'] : 400;
$h = (isset($parameters['h'])) ? $parameters['h'] : 200;
$taglia_title = (isset($parameters['taglia_title'])) ? $parameters['taglia_title'] : 30;
$taglia_content = (isset($parameters['taglia_content'])) ? $parameters['taglia_content'] : 100;
$cols = (isset($parameters['cols'])) ? $parameters['cols'] : 3;
$button = (isset($parameters['button'])) ? $parameters['button'] : 'primary';
$col_size = 12 / $cols;
$show_Subtitle = (isset($parameters['show_Subtitle'])) ? $parameters['show_Subtitle'] : 0;
?>

<section class="blogstyle-1 dart-pt-30">
    <div class="container">
        <div class="row">
            <?
                $i = 0;
                foreach ($articles as $article) {
            ?>
            <div class="col-md-4 col-sm-4">
                <article class="blog-post-container clearfix">
                    <? if ($article->image != '') { ?>
                        <a href ="<?= Utils::getComponentUrl('Articles/show_article') . "/" . Utils::url_slug($article->title . '-' . $article->id) ?>">
                            <div class="post-thumbnail"><img alt="Image" class="img-responsive " src="<?= Utils::genThumbnailUrl("articles/{$article->id_category}/" . $article->image, $w, $h, array('zc' => 1)) ?>"></div>
                        </a>
                     <?}?>
                    <div class="blog-content">
                        <div class="dart-header">
                            <h4 class="dart-title"><a href="<?= Utils::getComponentUrl('Articles/show_article') . "/" . Utils::url_slug($article->title . '-' . $article->id) ?>"><?= StringUtils::CutString(strip_tags($article->title), $taglia_title) ?></a></h4>
                        </div>
                        <div class="dart-content">
                            <p><?= StringUtils::CutString(strip_tags($article->content), $taglia_content) ?></p>
                        </div>               
                    </div>
                </article>
            </div>
            <?}?>   
        </div>
    </div>
</section>
