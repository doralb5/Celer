<? if (count($banners)) { ?>

    <? $w = (isset($parameters['w']) && $parameters['w'] > 0) ? $parameters['w'] :  2000?>
    <? $h = (isset($parameters['h']) && $parameters['h'] > 0) ? $parameters['h'] : 900 ?>
    <? $interval = (isset($parameters['interval']) && $parameters['interval'] > 0) ? $parameters['interval'] : 5000 ?>
    <? $show_caption = (isset($parameters['show_caption'])) ? $parameters['show_caption'] : 0 ?>
    <? $show_controls = (isset($parameters['show_controls'])) ? $parameters['show_controls'] : 0 ?>

<div class="master-slider ms-skin-default" id="masterslider">
    
<?
$i = 0;
foreach ($banners as $banner) {
    ?>
    <? HeadHTML::addMetaProperty("og:image", Utils::genThumbnailUrl('banners/' . str_replace('%2F', '/', urlencode($banner->filename)), 0,0,[],true)); ?>
    
    <div class="ms-slide slide-4">
        <img src="<?=$this->template_path."css/vendor/masterslider/style/blank.gif"?>" data-src="<?= Utils::genThumbnailUrl('banners/' .$banner->filename, $w, $h, array('zc' => 1));?>" />
        
        <? if($banner->text1 || $banner->text2 || $banner->text3){ ?>
            <div class="ms-layer box" data-duration="1300" data-ease="easeOutBack" data-effect="skewleft(15,250)">
            <? if ($banner->text1) echo $banner->text1; ?>
            <? if ($banner->text2) echo $banner->text2; ?>
            <? if ($banner->text3) echo $banner->text3; ?>
            </div>
        <?}?>
    </div>

    <?
    $i++;
}
?>

    
</div>

<? $code = <<<EOF
// Masterslider 
    var slider = new MasterSlider();
        slider.control('arrows' ,{insertTo:'#masterslider'});  
        slider.control('bullets'); 
        slider.setup('masterslider' , {
            width:1024,
            height:767,
            space:5,
            view:'fade',
            layout:'fullscreen',
            fullscreenMargin:0,
            speed:100,
            overPause:false,
            autoplay:true,
    });
EOF;
?>
<? HeadHTML::addScript($code)?>
<?}?>