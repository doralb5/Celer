<? HeadHtml::AddStylesheet("owl.carousel.min.css"); ?>
<? HeadHtml::AddJS("owl.carousel.min.js"); ?>
<? $itemc = (isset($parameters['itemc']) && $parameters['itemc'] > 0) ? $parameters['itemc'] : 5 ?>
<? $width = (isset($parameters['width']) && $parameters['width'] > 0) ? $parameters['width'] : 100 ?>
<? $height = (isset($parameters['height']) && $parameters['height'] > 0) ? $parameters['height'] : 70 ?>

<? if (count($banners)) { ?>
    <div class="owl-carousel owl-theme">
        <? foreach ($banners as $banner) { ?>
            <div class="item">
                <div class="image text-center">
                    <?= ($banner->target_url!='') ? "<a href=\"<?= $banner->target_url\">" : "" ?>
                        <img src="<?= Utils::genThumbnailUrl('banners/' . $banner->filename, $width, $height) ?>">
                    <?= ($banner->target_url!='') ? "</a>" : "" ?>
                </div>
            </div>
        <? } ?>
    </div>

<? $carousel = <<<EOF
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            items: $itemc,
            loop: true,
            margin: 25,
            nav: false,
            autoWidth: false,
            autoPlay: 5000, //Vecchia versione OWL
            autoplay:true,  //Nuova versione OWL
            autoplayTimeout: 5000,
            autoplayHoverPause: false,
            autoplaySpeed: 1500,
            dots: false,
            pagination: false,
            responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },            
            960:{
                items:$itemc
            },
            1200:{
                items:$itemc
            }
        }
        });
    });
EOF;
?>

<? HeadHTML::addScript($carousel)?>

<? } ?>

