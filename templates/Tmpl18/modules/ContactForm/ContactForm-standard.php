<?
$show_title = isset($params['show_title']) ? $params['show_title'] : 1;
$name = isset($_POST['name']) ? $_POST['name'] : "";
$email = isset($_POST['email']) ? $_POST['email'] : "";
$message = isset($_POST['message']) ? $_POST['message'] : "";
?>
<!--contact-->

<section id="form_4" class="form_section_4 bg_23 bg-fixed dart-pt-30 dart-pb-10">		
    	<div class="container">
        	<div class="row form_body">
                <div class="col-md-12">
                	<div class="dart-headingstyle-one dart-mb-30">
                    	<h2 class="dart-heading text-center white">Keep in Touch With Us</h2>
                  	</div>
                </div>
            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                	<div class="form_heading text-left form-heading_1">
                    	<h2 class="dart-fs-16 dart-fw-400">Bonito Design Company </h2>
                    </div>
                	<div class="form_panel">
                           <form class="dart-form-9">
                           	<div class="row">
                            	<div class="form-group col-sm-12">
                                	<p>Yeah, Bonito is great and this is the fun part. We'd love to talk about your project or just chat about Bonito. So either fill out the form, pick up the phone</p>
                            	</div>
                            </div>
                            <div class="row">
                             <div class="form-group col-sm-12">
                                	<ul class="list-unstyled">
                                   		<li><i class="fa fa-home"></i>123, Lorem Road, Ipsum ble, New York, USA</li>
                                        <li><i class="fa fa-phone"></i>123 456 7890</li>
                                        <li><i class="fa fa-cloud"></i>123 456 7890</li>
                                        <li><i class="fa fa fa-envelope-o"></i>hello@bonito.com</li>
                                    </ul>
                                </div></div>
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <ul class="social_links_4 list-unstyled">
                                        <li><a href="#"><i class="fa  fa-facebook"></i></a></li>
                                        <li class="active"><a href="#"><i class="fa  fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa  fa-pinterest"></i></a></li>
                                        <li><a href="#"><i class="fa  fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa  fa-tumblr"></i></a></li> 
                                        <li><a href="#"><i class="fa  fa-dribbble"></i></a></li>
                                        <li><a href="#"><i class="fa  fa-vimeo"></i></a></li>
                                        <li><a href="#"><i class="fa  fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                          </div>    
                          </form>
                	</div>
                </div>	
                
                
               	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                	<div class="form_panel">
                           <form class="dart-form-10" action="send_email.php" name="contact" method="post">
                           <div class="row">
                           		<div class="col-sm-12">
                                    <div class="form-group inner-addon right-addon">
                                         <i class="fa fa-user"></i>
                                        <input type="text" class="form-control" id="name" placeholder="Name*" name="InputName" required="">  
                                    </div>
                                </div>
                           </div>
                           <div class="row">
                           		<div class="col-sm-12">
                                    <div class="form-group inner-addon right-addon">
                                        <i class="fa fa-comments-o"></i>
                                        <input type="email" class="form-control" id="email" placeholder="Email*" name="InputEmail" required="">
                                	</div>
                                </div>
                           </div>
                           <div class="row">
                           	<div class="form-group col-sm-12">
                                   <textarea class="form-control form-control-multiline" rows="10" name="InputMessage" required=""></textarea>
                                </div>
                           </div>
                           <div class="row">
                           	  <div class="form-group col-sm-12">
                                   <button type="submit" class="btn transparent ret-stroke-btn border_1px dart-btn-sm">SUBMIT CONTACT</button>
                              </div>
                           </div>     
                          </form>
                	</div>
                </div>	
            </div>
       </div>
   </section>

<!--section class="<?= ($show_title) ? 'section-padding' : '' ?>" id="contact">
            
                <? if ($show_title) { ?>
                    <h2 class="ser-title">[$Contact_us]</h2>
                    <hr class="botm-line" />
                <? } ?>

            
            <? if(!isset($params['show_contact_info']) || $params['show_contact_info'] != '0') {?>
            <div class="col-md-4 col-sm-4" style="font-size:15px;">

                <h3>[$Contact_information]</h3>

                <div class="space">&nbsp;</div>
                <? if (CMSSettings::$company_address != "") { ?>
                    <p><i class="fa fa-map-marker fa-fw pull-left fa-2x"></i><?= CMSSettings::$company_address ?></p>
                <? } ?>

                <div class="space">&nbsp;</div>

                <p><i class="fa fa-envelope-o fa-fw pull-left fa-2x"></i><?= CMSSettings::$company_mail ?></p>

                <div class="space">&nbsp;</div>
                <? if (CMSSettings::$company_tel != "") { ?>

                    <p><i class="fa fa-phone fa-fw pull-left fa-2x"></i><?= CMSSettings::$company_tel ?></p>
                <? } ?>
            </div>

                <div class="col-md-8 col-sm-8 marb20">
            <? } else {
                echo '<div class="col-sm-12 marb20">';
            } ?>
            
                <div class="contact-info">
                    <h3 class="cnt-ttl">[$request_contact]</h3>

                    <div class="space">&nbsp;</div>

                    <div id="sendmessage"></div>

                    <div id="errormessage"></div>

                    <form role="form" id="contactForm" method="post">
                        <? if (isset($success)) { ?>
                            <div class="alert alert-success" role="alert">
                                <i class="fa fa-check"></i>
                                <?= $success ?>
                            </div>
                        <? } ?>


                        <div class="form-group">
                            <input class="form-control br-radius-zero" data-msg="Please enter at least 4 chars" data-rule="minlen:4" id="name" name="name" placeholder="[$name_placehoder]" type="text" value="<?= $name ?>" required />
                            <div class="validation">&nbsp;</div>
                        </div>

                        <div class="form-group">
                            <input class="form-control br-radius-zero" data-msg="Please enter a valid email" data-rule="email" id="email" name="email" placeholder="[$email_placehoder]" type="email" value="<?= $email ?>" required/>
                            <div class="validation">&nbsp;</div>
                        </div>

                        <div class="form-group">
                            <textarea class='form-control br-radius-zero' name='message' id="message" style="max-width:100%" rows="10" cols="100" placeholder="[$message_placehoder]" required><?= $message ?></textarea>
                            <div class="validation">&nbsp;</div>
                        </div>

                        <div class="form-action">
                            <input type="hidden" name="send" value="1" />
                            <button class="btn btn-form" type="submit" id="contactSubmit" value="1">[$send_request]</button>
                        </div>
                    </form>


                </div>
            </div>
</section>

<script>

    $(document).ready(function () {
        
        $("#contactSubmit").click(function (e) {
            
            // Prevent default posting of form - put here to work in case of errors
            e.preventDefault();

            // setup some local variables
            var $form = $('#contactForm');

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            // Serialize the data in the form
            var serializedData = $form.serialize();

            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            request = $.ajax({
                url: "<?= $ajx_action_url ?>",
                type: "post",
                data: serializedData
            });
            
            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                $('#sendmessage').html(' ');
                $('#sendmessage').css('display', 'none');
                $('#errormessage').html(' ');
                $('#errormessage').css('display', 'none');

                var resp = JSON.parse(response);
                
                if ('msg' in resp) {
                    $('#sendmessage').html(resp['msg']);
                    $('#sendmessage').css('display', 'block');
                } else {
                    $('#errormessage').html(resp['error']);
                    $('#errormessage').css('display', 'block');
                }

            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                // Log the error to the console
                console.log(
                        "The following error occurred: " +
                        textStatus, errorThrown
                        );
                $('#errormessage').html("The following error occurred: " + textStatus);
                $('#errormessage').css('display', 'block');
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });

        });
    });

</script>

<!--/ contact-->