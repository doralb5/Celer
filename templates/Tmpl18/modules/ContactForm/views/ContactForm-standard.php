<?
$show_title = isset($params['show_title']) ? $params['show_title'] : 1;
$name = isset($_POST['name']) ? $_POST['name'] : "";
$email = isset($_POST['email']) ? $_POST['email'] : "";
$message = isset($_POST['message']) ? $_POST['message'] : "";
?>
<!--contact-->

<section id="form_4" class="form_section_4 bg_23 bg-fixed dart-pt-30 dart-pb-10">		
    	<div class="container">
        	<div class="row form_body">
                <? if ($show_title) { ?>
                    <div class="col-md-12">
                            <div class="dart-headingstyle-one dart-mb-30">
                            <h2 class="dart-heading text-center white">[$Contact_us]</h2>
                            </div>
                    </div>
                <?}?>

                <? if(!isset($params['show_contact_info']) || $params['show_contact_info'] != '0') {?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form_heading text-left form-heading_1">
                            <h2 class="dart-fs-16 dart-fw-400">[$Contact_information] </h2>
                        </div>
                        <div class="form_panel">
                           <form class="dart-form-9">
                            <div class="row">
                                <div class="form-group col-sm-12">
                                        <ul class="list-unstyled">
                                        <? if (CMSSettings::$company_address != "") { ?>    
                                            <li><i class="fa fa-home"></i><?= CMSSettings::$company_address ?></li>
                                        <?}?>
                                        <? if (CMSSettings::$company_tel != "") { ?>
                                            <li><i class="fa fa-phone"></i> <?= CMSSettings::$company_tel?></li>
                                        <?}?>
                                        <li><i class="fa fa fa-envelope-o"></i><?= CMSSettings::$company_mail ?></li>
                                    </ul>
                                </div>
                            </div>                              
                          </form>
                        </div>
                    </div>	
                <?}?>        
                
               	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        
                        <div id="sendmessage"></div>
                        <div id="errormessage"></div>
                	
                        <div class="form_panel">
                           <form class="dart-form-10" role="form" id="contactForm" method="post">
                            <? if (isset($success)) { ?>
                               <div class="row"> 
                                    <div class="col-sm-12">
                                        <div class="alert alert-success" role="alert">
                                            <i class="fa fa-check"></i>
                                            <?= $success ?>
                                        </div>
                                    </div>    
                               </div>
                            <? } ?>
                               
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group inner-addon right-addon">
                                         <i class="fa fa-user"></i>
                                            <input class="form-control br-radius-zero" data-msg="Please enter at least 4 chars" data-rule="minlen:4" id="name" name="name" placeholder="[$name_placehoder]" type="text" value="<?= $name ?>" required />
                                    </div>
                                    <div class="validation">&nbsp;</div>
                                </div>
                           </div>
                           <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group inner-addon right-addon">
                                        <i class="fa fa-comments-o"></i>
                                            <input class="form-control br-radius-zero" data-msg="Please enter a valid email" data-rule="email" id="email" name="email" placeholder="[$email_placehoder]" type="email" value="<?= $email ?>" required/>
                                    </div>
                                    <div class="validation">&nbsp;</div>
                                </div>
                           </div>
                           <div class="row">
                           	<div class="form-group col-sm-12">
                                    <textarea class='form-control br-radius-zero' name='message' id="message" style="max-width:100%" rows="10" cols="100" placeholder="[$message_placehoder]" required><?= $message ?></textarea>
                                </div>
                               <div class="validation">&nbsp;</div>
                           </div>
                           <div class="row">
                                <div class="form-group col-sm-12">
                                    <input type="hidden" name="send" value="1" />
                                    <button type="submit" class= "btn btn-form transparent ret-stroke-btn border_1px dart-btn-sm" id="contactSubmit" value="1">[$send_request]</button>
                                </div>
                           </div>     
                          </form>
                	</div>
                </div>	
            </div>
       </div>
   </section>




<? ob_start();?>
    $(document).ready(function () {
        
        $("#contactSubmit").click(function (e) {
            
            // Prevent default posting of form - put here to work in case of errors
            e.preventDefault();

            // setup some local variables
            var $form = $('#contactForm');

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            // Serialize the data in the form
            var serializedData = $form.serialize();

            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            request = $.ajax({
                url: "<?= $ajx_action_url ?>",
                type: "post",
                data: serializedData
            });
            
            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                $('#sendmessage').html(' ');
                $('#sendmessage').css('display', 'none');
                $('#errormessage').html(' ');
                $('#errormessage').css('display', 'none');

                var resp = JSON.parse(response);
                
                if ('msg' in resp) {
                    $('#sendmessage').html(resp['msg']);
                    $('#sendmessage').css('display', 'block');
                } else {
                    $('#errormessage').html(resp['error']);
                    $('#errormessage').css('display', 'block');
                }

            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                // Log the error to the console
                console.log(
                        "The following error occurred: " +
                        textStatus, errorThrown
                        );
                $('#errormessage').html("The following error occurred: " + textStatus);
                $('#errormessage').css('display', 'block');
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });

        });
    });
<? $script = ob_get_contents();?>
<? ob_end_clean();?>

<? HeadHTML::addScript($script);?>

<!--/ contact-->