<?
$show_brand = isset($parameters['show_brand']) ? $parameters['show_brand'] : false;
$show_logo = isset($parameters['show_logo']) ? $parameters['show_logo'] : true;

?>

<nav class="navbar navbar-default navbar-fixed white-nav no-background divinnav">

        <!-- Start Top Search -->
        <div class="top-search">
            <div class="container">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control" placeholder="[$Search]">
                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                </div>
            </div>
        </div>
        <!-- End Top Search -->

        <div class="container">

            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>

                <? if ($show_logo && CMSSettings::$logo != '') {
                    $logo = WEBROOT . MEDIA_ROOT . CMSSettings::$logo;
                    $logo_inverse = WEBROOT . MEDIA_ROOT .  ( (CMSSettings::$logo_inverse != '') ? CMSSettings::$logo_inverse : CMSSettings::$logo );

                    ?>
                    <a class="navbar-brand" href="/">
                            <img src="<?= $logo ?>" class="logo logo-display" alt="">
                            <img src="<?= $logo_inverse ?>" class="logo logo-scrolled" alt="">
                    </a>
                <? } ?>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                    <? printRecursiveMenuItems($MenuItems);?>
                    <li>
                        <?
                        $lang = Loader::loadModule("Generic/ChangeLanguage");
                        $lang->execute();
                        ?>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>   

        <!-- Start Side Menu -->

        <!-- End Side Menu -->
    </nav>
<?
    function printRecursiveMenuItems($items) {
        foreach ($items as $item) {

            $target = getTargetByItem($item);
            switch ($item->type) {
                case "Page" :
                    $target = Utils::genUrl($item->page->getAlias());
                    break;
                case "External Link" :
                    $target = $item->target;
                    break;
                case "No Link" :
                    $target = '#';
                    break;

            }

            $active_class = (isActiveRecursively($item)) ? "active" : "";

            if($item->type == 'Divider') {
                echo "<li class=\"divider $active_class\"></li>\n";
            } else {
                if(count($item->subitems) == 0){
                    echo "<li class=\"$active_class\">";
                    echo "<a href=\"$target\" class=\"\">{$item->item}</a>";
                } else {
                    echo "<li class=\"$active_class dropdown\">";
                    echo "<a href=\"$target\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">{$item->item}</a>\n\n";
                    echo "<ul class=\"dropdown-menu animated fadeOutUp\">\n";
                    printRecursiveMenuItems($item->subitems);
                    echo "</ul>\n\n";
                }
                echo "</li>\n";
            }
        }
    }

    function getTargetByItem($item) {
        switch ($item->type) {
            case "Page" :
                $target = Utils::genUrl($item->page->getAlias());
                break;
            case "External Link" :
                $target = $item->target;
                break;
            case "No Link" :
                $target = '#';
                break;
            default :
                $target = '';
        }
        return $target;
    }

    function isActiveRecursively($item) {
        $target = getTargetByItem($item);
        $active = Utils::isActivePage($target);
        if($active)
            return true;

        if(count($item->subitems)) {
            foreach ($item->subitems as $subitem) {
                $active = isActiveRecursively($subitem);
                if($active) {
                    return true;
                }
            }
        }
        return false;
    }

?>

