<? HeadHTML::AddStylesheet(WEBROOT . $this->view_path . 'css/newsletter-style.css'); ?>

<div class="col-md-12 col-sm-12 text-center">
    <div class="newsletter-seb">
        <form class="form-inline"><span>[$GetNewsletterUpdate]</span>
            <div class="form-group"><input class="form-control" id="email" placeholder="[$Enter_your_mail]" type="text"></div>
            <button type="submit" id="subscribe-btn">[$sign_up]</button></form>
    </div>
    <div class="col-md-12">
        <p style="color: #c9302c; text-align: center; font-size: 17px; display: none; margin-top: 15px;" id="error"></p>
    </div>
    <div class="col-md-12">
        <p style="color: green; text-align: center; font-size: 17px; margin-top: 15px; display: none" id="message"></p>
    </div>
</div>

<? $newsletter = <<<EOF
    $(document).ready(function () {
        $('#subscribe-btn').click(function () {
            $.ajax("<?= $call_url ?>" + $('#email').val())
                .done(function (data) {
                    if (data.status == 1) {

                        if (data.message != undefined) {
                            $('#message').html('<span class="msg-span">' + data.message + '</span>');
                            $('#message').show("slow", function () {
                                setTimeout(function () {
                                    $("#message").hide('slow');
                                }, 2500);
                            });
                        }
                    } else {
                        if (data.message != undefined) {
                            $('#error').html('<span class="error-span">' + data.message + '</span>');
                            $('#error').show("slow", function () {
                                setTimeout(function () {
                                    $("#error").hide('slow');
                                }, 2500);
                            });
                        }
                    }
                })
                .fail(function (error) {
                    console.log(error);
                })
        });
    });
EOF;
?>
<? HeadHTML::addScript($newsletter)?>



