<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? (CMSSettings::$favicon != '') ? HeadHTML::printFavicon(CMSSettings::$favicon) : ''; ?>



        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkJS("ie-emulation-modes-warning.js"); ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("vendor/prettyPhoto/prettyPhoto.css"); ?>
		<? HeadHtml::linkStylesheet("vendor/woocommerce-FlexSlider/flexslider.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("custom.css"); ?>




		<? HeadHTML::printJSLinks(); ?>



<!--        <style type="text/css">
            #banner {
                background: url('<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>') no-repeat fixed;
                background-size: contain;
            }
        </style>-->


		<?= $this->getTemplate()->head_extra; ?>

    </head>
    <body id="page-top">
		<? if ($this->getPosition('pos-nav')->countBlocks() > 0) { ?>
			<div class="container">
				<div class="row">
					<div class="nav-costumization">
						<? $this->getPosition('pos-nav')->printContent(); ?>
					</div>
				</div>
			</div>
		<? } ?>

		<? if ($this->getPosition('pos-banner')->countBlocks() > 0 || $this->getPosition('pos-banner-info')->countBlocks() > 0) { ?>
			<!--banner-->
			<section id="banner" class="banner">
				<div class="bg-color">
					<? if ($this->getPosition('pos-banner')->countBlocks() > 0) { ?>
						<div class="banner-background">
							<? $this->getPosition('pos-banner')->printContent(); ?>
						</div>
					<? } ?>

					<? if ($this->getPosition('pos-banner-info')->countBlocks() > 0) { ?>
						<div class="container">
							<div class="row">
								<div class="banner-info">
									<? $this->getPosition('pos-banner-info')->printContent(); ?>
								</div>
							</div>
						</div>
					<? } ?>

				</div>
			</section>
			<!--/ banner-->
			<?
		}?>