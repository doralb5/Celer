//[Master Javascript]

//Project:	Aura - Multipage Html Responsive Template
//Version:	1.1
//Last change:	28/03/2017
//Primary use:	Aura - Multipage Html Responsive Template 


//Template script here

$(document).ready(function () {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').on('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 70)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 71
    });

    // Closes the Responsive Menu on Menu Item Click
//	$('.navbar-collapse ul li a').on('click', function(event) {
//		$(this).closest('.collapse').collapse('toggle');
//	});

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })


    // Initialize and Configure Scroll Reveal Animation

    window.sr = ScrollReveal();
    sr.reveal('.sr-icons', {
        duration: 600,
        scale: 0.3,
        distance: '0px'
    }, 200);
    sr.reveal('.sr-button', {
        duration: 1000,
        delay: 200
    });
    sr.reveal('.sr-contact', {
        duration: 600,
        scale: 0.3,
        distance: '0px'
    }, 300);


    // Counter
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });



    // prettyPhoto

    $("a[rel^='alternate']").prettyPhoto();

    //Script to Activate the Carousel

    $('.flexslider').flexslider({
        animation: "fade",
        controlNav: false,
        directionNav: false,
    });


    $(window).on("load", function () {
        $('.logoslide').flexslider({
            animation: "slide",
            animationLoop: true,
            animationSpeed: 600,
            slideshowSpeed: 2000,
            controlNav: false,
            directionNav: false,
            itemWidth: 234,
            itemMargin: 5
        });
    });

}); // End of use strict
