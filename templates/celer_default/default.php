<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'CelerCMS'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
        <link rel="icon" href="<?= WEBROOT . DATA_DIR ?>favicon.ico">

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("menu.css"); ?>
        <link href="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/3.3.0/ekko-lightbox.min.css" rel="stylesheet">


		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js") ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
        <script src="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/3.3.0/ekko-lightbox.min.js"></script>
		<? HeadHTML::printJSLinks(); ?>
    </head>

    <body>

		<? if ($this->getPosition('celer-header')->countBlocks()) { ?>
			<div id="celer-header" class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('celer-header')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		<? } ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 clr_default_text">
                    <img src="<?= WEBROOT . TEMPLATES_PATH . $this->template_name . DS . 'images' . DS . 'celer_logo.png' ?>" alt=""/>
                </div>
            </div>
        </div>

		<? if ($this->getPosition('celer-content')->countBlocks()) { ?>
			<div id="celer-content" class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('celer-content')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		<? } ?>

		<? if ($this->getPosition('celer-footer')->countBlocks()) { ?>
			<div id="celer-footer" class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('celer-footer')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		<? } ?>






    </body>    
</html>
