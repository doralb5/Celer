<?php include 'header.php'; ?>
<!-- Middle Body -->
<? if ($content != '' || $this->getPosition('pos-middle')->countBlocks()) { ?>
	<div id="pos-cmiddle" class="container-fluid">
		<div class="container cmiddle">
			<div class="row">
				<div class="col-md-12">
					<div class="body-container">
						<?
						$this->getPosition('pos-middle')->printContent();
						?>

						<?= $content ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<? } ?>

<?php include 'footer.php'; ?>
