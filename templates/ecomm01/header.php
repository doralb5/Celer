<!DOCTYPE html>
<html lang="it">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
        <link rel="icon" href="../../favicon.ico">

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("animate.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("style4.css"); ?>
		<? HeadHtml::linkStylesheet("menu.css"); ?>
		<? HeadHtml::linkStylesheet("superslides.css"); ?>
		<? HeadHtml::linkStylesheet("owl.carousel.css"); ?>
		<? HeadHtml::linkStylesheet("jstarbox.css"); ?>
		<? HeadHtml::linkStylesheet("chocolat.css"); ?>
		<? HeadHtml::linkStylesheet("popuo-box.css"); ?>
        <link href="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/3.3.0/ekko-lightbox.min.css" rel="stylesheet">

		<? HeadHTML::printStylesheetLinks(); ?>


        <!-- Javascript -->
		<? HeadHtml::linkJS("jquery.min.js"); ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("owl.carousel.js"); ?>
		<? HeadHtml::linkJS("jquery.animate-enhanced.min.js"); ?>
		<? HeadHtml::linkJS("jquery.easing.1.3.js"); ?>
		<? HeadHtml::linkJS("fx.js"); ?>
		<? HeadHtml::linkJS("jquery.superslides.min.js"); ?>
		<? HeadHtml::linkJS("easing.js"); ?>
		<? HeadHtml::linkJS("scrolling-nav.js"); ?>
		<? HeadHtml::linkJS("wow.min.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHtml::linkJS("materialize.min.js"); ?>
		<? HeadHtml::linkJS("simpleCart.min.js"); ?>
		<? HeadHtml::linkJS("jquery.chocolat.js"); ?>
		<? HeadHtml::linkJS("jstarbox.js"); ?>
		<? HeadHtml::linkJS("jquery.magnific-popup.js"); ?>
        <script src="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/3.3.0/ekko-lightbox.min.js"></script>

		<? HeadHTML::printJSLinks(); ?>
        <script>
            wow = new WOW(
                    {
                        boxClass: 'wow', // default
                        animateClass: 'animated', // default
                        offset: 30, // default
                        mobile: true, // default
                        live: true        // default
                    }
            );
            wow.init();
        </script>
    </head>
    <body>

        <!-- Header Section -->
        <div id="header">

            <!--login + social-->
			<? if ($this->getPosition('pos-up-menu')->countBlocks() != 0 || $this->getPosition('pos-social-button')->countBlocks() != 0) { ?>
				<header id="main-header" class="header-top navbar-fixed-top animated">
					<div id="" class="container-fluid">
						<div class="container">
							<div class="row">

								<? if ($this->getPosition('pos-up-menu')->countBlocks()) { ?>
									<div class="col-sm-5 col-md-offset-2 col-xs-12">
										<?
										$this->getPosition('pos-up-menu')->printContent();
										?>
									</div>
								<? } ?>

								<? if ($this->getPosition('pos-social-button')->countBlocks()) { ?>
									<div class="col-sm-5 col-xs-12">
										<?
										$this->getPosition('pos-social-button')->printContent();
										?>
									</div>
								<? } ?>

							</div>
						</div>
					</div>
				</header>
			<? } ?>

            <!-- Logo -->    
            <div class="container">
				<?
				$this->getPosition('pos-logo')->printContent();
				?>
            </div>            




            <!--Menu -->
			<? if ($this->getPosition('pos-menu')->countBlocks()) { ?>
				<header id="main-header-menu" class="navbar-fixed-top animated">
					<div id="" class="container-fluid">
						<div class="container">
							<?
							$this->getPosition('pos-menu')->printContent();
							?>
						</div>
					</div>
				</header>
			<? } ?>

            <!-- Fullwidth Slider -->
			<? if ($this->getPosition('pos-slider')->countBlocks()) { ?>
				<div id="pos-htop" class="wrapper">
					<?
					$this->getPosition('pos-slider')->printContent();
					?>
				</div>
			<? } ?>



            <!-- Bottom Header -->
			<? if ($this->getPosition('pos-hbottom')->countBlocks()) { ?>
				<div id="pos-hbottom" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12  content-top">
								<?
								$this->getPosition('pos-hbottom')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

        </div>
