<?php include 'header.php'; ?>
<? if ($this->getPosition('pos-helper')->countBlocks()) { ?>
	<div id="pos-helper" class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?
					$this->getPosition('pos-helper')->printContent();
					?>  
				</div>
			</div>
		</div>
	</div>
<? } ?>
<!-- Middle Body -->
<? if ($content != '' || $this->getPosition('pos-middle')->countBlocks()) { ?>
	<div id="pos-cmiddle" class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="body-container">
						<?
						$this->getPosition('pos-middle')->printContent();
						?>

						<?= $content ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<? } ?>

<!-- Section 2 -->
<? if ($this->getPosition('pos-bottom-1')->countBlocks()) { ?>
	<div id="pos-section-2" class="wrapper">    
		<?
		$this->getPosition('pos-bottom-1')->printContent();
		?> 
	</div>
<? } ?>

<!-- Bottom Body -->
<? if ($this->getPosition('pos-bottom-2')->countBlocks()) { ?>
	<div id="pos-bottom-2" class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?
					$this->getPosition('pos-bottom-2')->printContent();
					?>
				</div>
			</div>
		</div>
	</div>
<? } ?>


<?php include 'footer.php'; ?>
