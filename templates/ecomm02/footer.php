

<!-- Footer Section -->
<div id="footer" class="footer">
    <div class="footer-middle">

        <!-- Middle Footer -->
		<? if ($this->getPosition('pos-fmiddle')->countBlocks()) { ?>
			<div id="pos-fmiddle" class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('pos-fmiddle')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		<? } ?>

        <!-- Bottom Footer -->
		<? if ($this->getPosition('pos-fbottom')->countBlocks()) { ?>
			<div id="pos-fbottom" class="footer-bottom" class="container">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-sm-10 col-xs-12 copyright">
							<?
							$this->getPosition('pos-fbottom')->printContent();
							?>
						</div>

					</div>
				</div>
			</div>
		<? } ?>
    </div>
</div>



</body>
</html>

