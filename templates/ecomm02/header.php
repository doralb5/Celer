<!DOCTYPE html>
<html lang="it">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
        <link rel="icon" href="../../favicon.ico">

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("animate.css"); ?>
		<? HeadHtml::linkStylesheet("style2.css"); ?>
		<? HeadHtml::linkStylesheet("style4.css"); ?>
		<? HeadHtml::linkStylesheet("menu.css"); ?>
		<? HeadHtml::linkStylesheet("superslides.css"); ?>
		<? HeadHtml::linkStylesheet("owl.carousel.css"); ?>
		<? HeadHtml::linkStylesheet("jstarbox.css"); ?>
		<? HeadHtml::linkStylesheet("chocolat.css"); ?>
		<? HeadHtml::linkStylesheet("popuo-box.css"); ?>
		<? HeadHtml::linkStylesheet("flexslider.css"); ?>
        <link href="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/3.3.0/ekko-lightbox.min.css" rel="stylesheet">

		<? HeadHTML::printStylesheetLinks(); ?>


        <!-- Javascript -->
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<? HeadHtml::linkJS("jquery.min.js"); ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("jquery.easing.1.3.js"); ?>
		<? HeadHtml::linkJS("jquery.superslides.min.js"); ?>
		<? HeadHtml::linkJS("jquery.magnific-popup.js"); ?>
		<? HeadHtml::linkJS("jquery.animate-enhanced.min.js"); ?>
		<? HeadHtml::linkJS("jquery.chocolat.js"); ?>
		<? HeadHtml::linkJS("jquery.flexslider.js"); ?>
		<? HeadHtml::linkJS("fx.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHtml::linkJS("easing.js"); ?>
		<? HeadHtml::linkJS("scrolling-nav.js"); ?>
		<? HeadHtml::linkJS("wow.min.js"); ?>
		<? HeadHtml::linkJS("owl.carousel.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHtml::linkJS("materialize.min.js"); ?>
		<? HeadHtml::linkJS("simpleCart.min.js"); ?>
		<? HeadHtml::linkJS("jstarbox.js"); ?>
		<? HeadHtml::linkJS("imagezoom.js"); ?>
        <script src="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/3.3.0/ekko-lightbox.min.js"></script>

		<? HeadHTML::printJSLinks(); ?>

        <script type="text/javascript">
            jQuery(function () {
                jQuery('.starbox').each(function () {
                    var starbox = jQuery(this);
                    starbox.starbox({
                        average: starbox.attr('data-start-value'),
                        changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                        ghosting: starbox.hasClass('ghosting'),
                        autoUpdateAverage: starbox.hasClass('autoupdate'),
                        buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                        stars: starbox.attr('data-star-count') || 5
                    }).bind('starbox-value-changed', function (event, value) {
                        if (starbox.hasClass('random')) {
                            var val = Math.random();
                            starbox.next().text(' ' + val);
                            return val;
                        }
                    })
                });
            });
        </script>
        <script>
            wow = new WOW(
                    {
                        boxClass: 'wow', // default
                        animateClass: 'animated', // default
                        offset: 30, // default
                        mobile: true, // default
                        live: true        // default
                    }
            )
            wow.init();



        </script>

        <script>
            $(document).ready(function () {
                $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });

            });
        </script>
    </head>
    <body>
        <!-- Fullwidth Slider -->
		<? if ($this->getPosition('pos-slider')->countBlocks()) { ?>
			<div id="pos-htop" class="wrapper">
				<?
				$this->getPosition('pos-slider')->printContent();
				?>
			</div>
		<? } ?>

        <!-- Header Section -->
        <div id="header">
            <div id="main-header" class="">
                <!--header-->
                <div class="header wow fadeIn" data-wow-delay="1s">
                    <div class="header-top">
                        <div class="container-fluid">

                        </div>
                    </div>

                    <div class="head-top">
                        <div class="container-fluid">

                            <div class="col-md-1 col-sm-2 logo">
								<?
								$this->getPosition('pos-logo')->printContent();
								?>
                            </div>






                            <div class="col-md-2 social">		
                                <div class="header-social">		
									<?
									$this->getPosition('pos-social-button')->printContent();
									?>
                                </div>

                                <div class="header-login">
                                    <ul>
                                        <li><a href="/login">Login</a></li>
                                        <li><a href="/wishlist" >
                                                <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                                            </a></li>
                                        <li>
                                            <a href="/checkout"><i class="glyphicon glyphicon-shopping-cart"> </i></a>                        
                                        </li>
                                        <li><a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="glyphicon glyphicon-search"> </i></a></li>
                                    </ul>



                                    <!---//pop-up-box---->
                                    <div id="small-dialog" class="mfp-hide">
                                        <div class="search-top">
                                            <div class="login-search">
                                                <input type="submit" value="">
                                                <input type="text" value="Search.." onfocus="this.value = '';" onblur="if (this.value == '') {
                                                            this.value = 'Search..';
                                                        }">		
                                            </div>
                                            <p>Desta</p>
                                        </div>				
                                    </div>
                                    <script>
                                        $(document).ready(function () {
                                            $('.popup-with-zoom-anim').magnificPopup({
                                                type: 'inline',
                                                fixedContentPos: false,
                                                fixedBgPos: true,
                                                overflowY: 'auto',
                                                closeBtnInside: true,
                                                preloader: false,
                                                midClick: true,
                                                removalDelay: 300,
                                                mainClass: 'my-mfp-zoom-in'
                                            });

                                        });
                                    </script>	
                                </div>
                            </div>


							<?
							$this->getPosition('pos-mobile-menu')->printContent();
							?>


                            <div class="col-md-9 col-sm-8 col-xs-12">
                                <div class="h_menu4">
									<?
									$this->getPosition('pos-menu')->printContent();
									?>

                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>	
                    </div>	
                </div>
            </div>


            <!-- Bottom Header -->
			<? if ($this->getPosition('pos-hbottom')->countBlocks()) { ?>
				<div id="pos-hbottom" class="wrapper">
					<div class="container">
						<?
						$this->getPosition('pos-hbottom')->printContent();
						?>
					</div>
				</div>
			<? } ?>

        </div>
