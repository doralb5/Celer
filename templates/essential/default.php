<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHTML::printFavicon(BRAND_FAVICON); ?>

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("flatpickr.min.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->
		<? HeadHTML::linkJS("jquery.min.js") ?>
		<? HeadHtml::linkJS("jquery.easing.min.js"); ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("custom.js"); ?>

		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHTML::printJSLinks(); ?>


<!--        <style type="text/css">
            #banner {
                background: url('<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>') no-repeat fixed;
                background-size: contain;
            }
        </style>-->
    </head>
    <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
        <!--banner-->
        <section id="banner" class="banner">
            <div class="bg-color">

                <div class="container">
                    <div class="row">
                        <div class="nav-costumization">
							<? $this->getPosition('pos-nav')->printContent(); ?>
                        </div>
                    </div>
                </div>
                <div class="banner-background">
					<? $this->getPosition('pos-banner')->printContent(); ?>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="banner-info">
							<? $this->getPosition('pos-banner-info')->printContent(); ?>
                        </div>
                    </div>
                </div>




            </div>
        </section>
        <!--/ banner-->

        <!-- main content-->
        <div id="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
						<? $this->getPosition('pos-ctop')->printContent(); ?>
						<? if (WebPage::showContentHeading() && WebPage::getContentHeading() != '') { ?><div id="#content-heading" class="content-heading"><h2 class="head-title"><?= WebPage::getContentHeading(); ?></h2><hr class="botm-line"></hr></div><? } ?>
						<?= $content; ?>
                    </div>
                </div>
            </div>


			<? $this->getPosition('pos-cmiddle')->printContent(); ?>

            <div class="article-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
							<? $this->getPosition('pos-section-article')->printContent(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ main content-->


		<? $this->getPosition('pos-cbottom')->printContent() ?>

        <!--footer-->
        <footer id="footer">
            <div class="top-footer">
				<? $this->getPosition('top-footer')->printContent() ?>
            </div>
            <div class="footer-line">
				<? $this->getPosition('footer-line')->printContent() ?>	
            </div>
        </footer>
        <!--/ footer-->

		<? HeadHtml::linkJS("flatpickr.js"); ?>
        <script>
            // flatpickr Calendar configuration
            flatpickr(".flatpickr");
        </script>
    </body>
</html>
