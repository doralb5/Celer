<?php include dirname(__FILE__) . "/header.php"; ?>

<!-- Body Section -->
<div id="content" class="conainer-fluid">

    <!-- Banner On Body -->
	<? if ($this->getPosition('pos-banner')->countBlocks()) { ?>
		<div id="pos-banner" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-banner')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Top Body -->
	<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
		<div id="pos-ctop" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-ctop')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Map Section -->
	<? if ($this->getPosition('pos-map')->countBlocks()) { ?>
		<div id="pos-map" class="wrapper">
			<?
			$this->getPosition('pos-map')->printContent();
			?>
			<div class="container-background-bottom container-background-footer hidden-xs hidden-sm"></div>
		</div>
	<? } ?>

    <!-- Middle Body -->
	<? if ($content != '') { ?>
		<div id="pos-cmiddle" class="container-fluid">
			<div class="container">
				<div class="cmiddle">
					<div class="row">
						<div class="col-md-12">
							<div class="body-container">

								<? if (WebPage::$breadcrumb != NULL) WebPage::$breadcrumb->render(); ?>
								<?= $content ?>

								<?
								$this->getPosition('pos-cmiddle')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section-0 -->
	<? if ($this->getPosition('pos-section0')->countBlocks()) { ?>
		<div id="pos-section0" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section0')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 1 -->
	<? if ($this->getPosition('pos-section-featured-businesses')->countBlocks() || $this->getPosition('pos-section-last-announces')->countBlocks()) { ?>
		<div id="pos-section1" class="container-fluid">
			<div class="container">
				<div class="row">

					<? if ($this->getPosition('pos-section-last-announces')->printContent(false) != '') { ?>

						<div class="col-md-8 last-businesses">
							<?
							$this->getPosition('pos-section-featured-businesses')->printContent();
							?>
						</div>
						<div class="col-md-4">
							<?
							$this->getPosition('pos-section-last-announces')->printContent();
							?>
						</div>
					<? } else { ?>
						<div class="col-md-12 last-businesses">
							<?
							$this->getPosition('pos-section-featured-businesses')->printContent();
							?>
						</div>
					<? } ?>

				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section Offers -->
	<? if ($this->getPosition('pos-last-offers')->countBlocks()) { ?>
		<div id="pos-last-offers" class="container-fluid">
			<div class="">
				<?
				$this->getPosition('pos-last-offers')->printContent();
				?>
			</div>
		</div>
	<? } ?>

    <!-- Section 2 -->
	<? if ($this->getPosition('pos-section2')->countBlocks()) { ?>
		<div id="pos-section2" class="">
			<div class="dark-overlay">
				<?
				$this->getPosition('pos-section2')->printContent();
				?>
			</div>
		</div>
	<? } ?>


    <!-- Section 3 -->
	<? if ($this->getPosition('pos-section3')->countBlocks()) { ?>
		<div id="pos-section3" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section3')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 3 -->
	<? if ($this->getPosition('pos-announces')->countBlocks()) { ?>
		<div id="pos-announces" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-announces')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section portals -->
	<? if ($this->getPosition('portals')->countBlocks()) { ?>
		<div id="portals" class="">
			<?
			$this->getPosition('portals')->printContent();
			?>
		</div>
	<? } ?>


    <!-- Section Horoscope -->
	<? if ($this->getPosition('pos-horoscope')->countBlocks()) { ?>
		<div id="pos-horoscope" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-horoscope')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 4 -->
	<? if ($this->getPosition('pos-section4')->countBlocks()) { ?>
		<div id="pos-section4" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section4')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 5 -->
	<? if ($this->getPosition('pos-section5')->countBlocks()) { ?>
		<div id="pos-section5" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section5')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>


	<? if ($this->getPosition('pos-section6')->countBlocks()) { ?>
		<div id="pos-section6" class="">

			<?
			$this->getPosition('pos-section6')->printContent();
			?>

		</div>
	<? } ?>


    <!-- Bottom Body -->
	<? if ($this->getPosition('pos-cbottom')->countBlocks()) { ?>
		<div id="pos-cbottom" class="contanier-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-cbottom')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

</div>

<? $this->getPosition('pos-footer-bg')->printContent(); ?>

<?php include dirname(__FILE__) . "/footer.php"; ?>