

<!-- Footer Section -->
<div id="footer" class="conainer-fluid">

    <!-- Middle Footer -->
	<? //if ($this->getPosition('pos-fmiddle')->countBlocks()) { ?>
    <div id="pos-fmiddle" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<?
					$this->getPosition('pos-fmiddle')->printContent();
					?>
                </div>
            </div>
        </div>
    </div>
	<? //} ?>

    <!-- Top Footer -->
	<? if ($this->getPosition('pos-ftop')->countBlocks()) { ?>
		<div id="pos-ftop" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<?
						$this->getPosition('pos-fcateg')->printContent();
						?>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4 col-xs-12">
								<?
								$this->getPosition('pos-ftop')->printContent();
								?>
							</div>
							<div class="col-md-8 col-xs-12">
								<?
								$this->getPosition('pos-social-fanpage')->printContent();
								?>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
	<? } ?>


    <!-- Bootom Footer -->
    <div id="pos-fbottom" class="contanier-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-12 copyright">
					<?
					$this->getPosition('pos-fbottom')->printContent();
					?>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12 logo">
                    <span class="poweredby">
                        <a href="<?= BRAND_URL ?>" target="_blank">
                            <img src="<?= BRAND_LOGO ?>" alt=""/>
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </div>

</div>

<? HeadHtml::linkJS("jarallax.js"); ?>
<? //HeadHtml::linkJS("bg-video.js"); ?>
<script type="text/javascript">
    /* init Jarallax */
    $('.homepage-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
    /* init Jarallax */
    $('.work-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
    /* init Jarallax */
    $('.advert-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
    /* init Jarallax */
    $('.partners-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
</script>
</body>    
</html>