<!DOCTYPE html>
<html lang="<?= FCRequest::getLang(); ?>">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHTML::printFavicon(BRAND_FAVICON); ?>


        <!-- Stylesheet -->

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

		<? HeadHtml::linkStylesheet("site.css"); ?>
		<? HeadHtml::linkStylesheet("owl.carousel.css"); ?>

		<? HeadHtml::linkStylesheet("mpg.css"); ?>
		<? HeadHtml::linkStylesheet("paymentfont.min.css"); ?>

		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.css"); ?>
		<? HeadHtml::linkStylesheet("animate.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHtml::linkStylesheet("accordion.css"); ?>
		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->

		<? HeadHtml::linkJS("materialize.min.js"); ?>
		<? HeadHtml::linkJS("richmarker.js"); ?>
		<? HeadHtml::linkJS("owl.carousel.js"); ?>
		<? HeadHtml::linkJS("mpg.js"); ?>
		<? HeadHtml::linkJS("bg-video.js"); ?>
		<? HeadHtml::linkJS("easing.js"); ?>
		<? HeadHtml::linkJS("SmoothScroll.min.js"); ?>
		<? HeadHtml::linkJS("scrolling-nav.js"); ?>
		<? HeadHtml::linkJS("wow.min.js"); ?>        
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js") ?>
		<? HeadHTML::linkJS("http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js") ?>
		<? HeadHTML::linkJS("http://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js") ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? // HeadHtml::linkJS("wow.min.js"); ?>
		<? HeadHtml::linkJS("smoothscroll.js"); ?>
		<? HeadHtml::linkJS("list.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHtml::linkJS("demo.js"); ?>
		<? HeadHtml::linkJS("loader.js"); ?>
		<? HeadHtml::linkJS("polyfills.js"); ?>
		<? HeadHtml::linkJS("modernizr-2.6.2.min.js"); ?>
		<? HeadHtml::linkJS("jquery.easing.1.3.js"); ?>
		<? HeadHtml::linkJS("scrolling-nav.js"); ?>
		<? HeadHtml::linkJS("easing.js"); ?>
		<? HeadHTML::printJSLinks(); ?>

		<? if ($this->template_params['background_image'] != "") { ?>
			<style>
				body {
					background-image: url("<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>");
					background-size: cover;
					background-repeat: no-repeat;
					background-attachment: fixed;
					background-position: center; 
				}
			</style>
		<? } ?>
        <script>
//            wow = new WOW(
//                    {
//                        boxClass: 'wow', // default
//                        animateClass: 'animated', // default
//                        offset: 30, // default
//                        mobile: true, // default
//                        live: true        // default
//                    }
//            )
//            wow.init();
        </script>
    <body>
		<? if ($this->getPosition('pos-preloader')->countBlocks()) { ?>
			<?
			$this->getPosition('pos-preloader')->printContent();
			?>     
		<? } ?>

        <!-- Header Section -->
        <div id="header">
			<? if ($this->getPosition('pos-helper')->countBlocks()) { ?>
				<div id="pos-helper" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-helper')->printContent();
								?>  
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Top Header -->
			<? if ($this->getPosition('pos-htop')->countBlocks()) { ?>
				<div id="pos-htop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-htop')->printContent();
								?>  
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Middle Header -->
            <div id="pos-hmiddle" class="container-fluid">
                <header id="main-header" class="navbar-fixed-top animated ">
                    <div class="row">
                        <div class="col-md-12">
							<?
							$this->getPosition('pos-hmiddle')->printContent();
							?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
							<?
							$this->getPosition('pos-logo')->printContent();
							?>
                        </div>

                        <div class="col-md-7">
							<?
							$this->getPosition('pos-search')->printContent();
							?>
                        </div>
                        <div class="col-md-3">
							<?
							$this->getPosition('pos-login')->printContent();
							?>
                        </div>
                    </div>
                </header>
            </div>



            <!-- Bottom Header -->
			<? if ($this->getPosition('pos-hbottom')->countBlocks()) { ?>
				<div id="pos-hbottom" class="contanier-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="header-info-content">    
									<?
									$this->getPosition('pos-banner-text')->printContent();
									?> 
								</div>
							</div>
						</div>
					</div>    

					<?
					$this->getPosition('pos-hbottom')->printContent();
					?>


				</div>
			<? } ?>

        </div>