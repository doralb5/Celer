<?php include dirname(__FILE__) . "/header.php"; ?>




<div class="header-info-jarallax header-info">

    <div class="main-fab">
        <a class="page-scroll " href="#about">
            <button class="md-fab md-accent md-button md-default-theme" ng-transclude="">
                <i class="fa fa-angle-down ion-android-star ng-scope"></i>
                <div class="md-ripple-container"></div>
            </button>
        </a>
    </div>
</div>
</div>

<!-- Description -->
<section id="about" class="about-section">
    <div class="container-fluid description invert-color">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<?
					$this->getPosition('pos-description')->printContent();
					?> 
                </div>
            </div>
        </div>
    </div>
</section>

<section id="offers">
    <div class="container-fluid offers">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<?
					$this->getPosition('pos-offers')->printContent();
					?> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Photo Galley -->
<section id="portals" class="portals-section">
	<?
	$this->getPosition('pos-portals')->printContent();
	?> 
</section>

<div class="clearfix"></div>

<section id="review-stat">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<?
					$this->getPosition('pos-reviews-stat')->printContent();
					?>
                </div>
                <div class="col-md-12">
					<?
					$this->getPosition('pos-add-reviews')->printContent();
					?>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="review">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<?
					$this->getPosition('pos-reviews')->printContent();
					?> 
                </div>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<!-- Contact Parallax -->
<section id="contact-sm" class="contact-sm-section">
	<?
	$this->getPosition('pos-contact-parallax')->printContent();
	?> 
</section>

<!-- Map -->
<section id="google-map" class="map-section">
	<?
	$this->getPosition('pos-map')->printContent();
	?> 
</section>



<!-- Adwords -->
<?
$this->getPosition('pos-adwords')->printContent();
?> 

<div class="clearfix"></div>

<!-- Video & Socials -->
<?
$this->getPosition('pos-video')->printContent();
?> 
<!-- Businesses Related -->
<section>
    <div class="container-fluid offers">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<?
					$this->getPosition('pos-businesses-related')->printContent();
					?> 
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Contact -->
<section id="contact" class="contact-section">
    <div class="container-fluid contactform">
        <div class="container">
            <div id="contact" class="contactform">
                <div class="row">

                    <div class="col-md-4">
                        <!-- Footer Contact Info -->
						<?
						$this->getPosition('pos-contact-footer')->printContent();
						?> 
                    </div>

                    <div class="col-md-4">
						<?
						$this->getPosition('pos-footer-center')->printContent();
						?> 
                    </div>

                    <div class="col-md-4 text-center fb-fanpage">
                        <!-- Facebook Fan Page -->
						<?
						if ($this->getPosition('pos-facebook-page')->printContent(false) != '') {

							$this->getPosition('pos-facebook-page')->printContent();
						}
						?>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="city-vector">
        <img class="" src="http://www.netirane.al/data/netirane.al/media/tirana-black.png" style="width: 100%;color: #e6e5e1;">
    </div>
</section>

<!-- Footer Address -->
<section id="footer" class="footer-section">

    <!--            <div class="container-fluid footer1">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="" src="http://www.netirane.al/data/netirane.al/media/thumbs/1200x800/neTirane-smartphone.png" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
    -->

    <!-- Footer Copyright -->
    <footer class="container-fluid text-center footer2">
        <div class="container">
            <div class="footer-content">
                <div class="row" >
                    <div class="col-md-12">
                        <div class="bis-name">
                            <a href="http://www.neshqiperi.al" class="wow zoomIn animated footer-name" data-wow-delay=".5s">
                                <img class="img_logo wow flash animated " data-wow-delay=".5s" src="http://www.neshqiperi.al/data/neshqiperi.al/media/thumbs/150x75/1483524258neShqiperi-logo.png">
                            </a>
                        </div>
                        <div class="poweredby">
                            <p style="margin: 0 0 5px;">© <?php echo date("Y"); ?> - Mundesuar nga rrjeti 
                                <b><a target="_blank" href="http://neShqiperi.al">neShqiperi.al</a></b>
                            </p>
                            <p style="font-size: 10px;">+355 (0)4 450 0371 - Zogu i Zi, Tirana, Albania</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </footer>
</section>



<script>
    (function () {
        $('.info a.link').click(function () {
            return false;
        });

        $('input').blur(function () {
            if ($(this).val()) {
                return $(this).addClass('filled');
            } else {
                return $(this).removeClass('filled');
            }
        });

    }).call(this);

</script>
</body>

</html>