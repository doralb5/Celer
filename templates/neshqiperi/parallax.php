<?php include dirname(__FILE__) . "/header.php"; ?>

<!-- Body Section -->
<div id="content" class="conainer-fluid parallax-layout">


    <!-- First Position -->
	<? if ($this->getPosition('first-pos')->countBlocks()) { ?>
		<div id="first-pos" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('first-pos')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Top Body -->
	<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
		<div id="pos-ctop" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-ctop')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 3 -->
	<? if ($this->getPosition('pos-section3')->countBlocks()) { ?>
		<div id="pos-section3" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section3')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>


    <!-- Middle Body -->
	<? if ($content != '' || $this->getPosition('pos-cmiddle')->countBlocks()) { ?>
		<div id="pos-parallax" class="">
			<div class="parallax-layout">

				<?= $content ?>

				<?
				$this->getPosition('pos-cmiddle')->printContent();
				?>
			</div>
		</div>
	<? } ?>

    <!-- Section-0 -->
	<? if ($this->getPosition('pos-section0')->countBlocks()) { ?>
		<div id="pos-section0" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section0')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 1 -->
	<? if ($this->getPosition('pos-section1')->countBlocks()) { ?>
		<div id="pos-section1" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section1')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 2 -->
	<? if ($this->getPosition('pos-section2')->countBlocks()) { ?>
		<div id="pos-section2" class="">
			<div class="dark-overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('pos-section2')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<? } ?>




    <!-- Bottom Body -->
	<? if ($this->getPosition('pos-cbottom')->countBlocks()) { ?>
		<div id="pos-cbottom" class="contanier-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-cbottom')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>
	<? $this->getPosition('pos-footer-bg')->printContent(); ?>
</div>

<!-- end BODY -->


<?php include dirname(__FILE__) . "/footer.php"; ?>