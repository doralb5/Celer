<!DOCTYPE html>
<html lang="<?= FCRequest::getLang(); ?>">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHTML::printFavicon(BRAND_FAVICON); ?>


        <!-- css -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<? HeadHtml::linkStylesheet("font-awesome.css"); ?>
		<? HeadHtml::linkStylesheet("site.css"); ?>
		<? HeadHtml::linkStylesheet("owl.carousel.css"); ?>
		<? HeadHtml::linkStylesheet("animate.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHtml::linkStylesheet("mpg.css"); ?>
		<? HeadHtml::linkStylesheet("paymentfont.min.css"); ?>

		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- js -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLqMl2q2qgghALxvvJBlLH8UuSv3LBJCo"></script>
		<? HeadHtml::linkJS("materialize.min.js"); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHtml::linkJS("richmarker.js"); ?>
		<? HeadHtml::linkJS("owl.carousel.js"); ?>
		<? HeadHtml::linkJS("mpg.js"); ?>
		<? HeadHtml::linkJS("bg-video.js"); ?>
		<? HeadHtml::linkJS("easing.js"); ?>
		<? HeadHtml::linkJS("SmoothScroll.min.js"); ?>
		<? HeadHtml::linkJS("scrolling-nav.js"); ?>
		<? HeadHtml::linkJS("wow.min.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHTML::printJSLinks(); ?>


    </head>  

    <body id="page-top">
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <!-- Header -->
        <section id="header" class="header-section">
            <div class="container-fluid header_container">
                <div class="">
                    <div class="row">
                        <div class="col-md-12" >
                            <div class="header" >

								<?
								$this->getPosition('pos-logo')->printContent();
								?> 
                                <div name="company_name" class="name">
									<?
									$this->getPosition('pos-name')->printContent();
									?> 
									<?
									$this->getPosition('pos-stars')->printContent();
									?> 
                                </div>

                                <a class="login-btn" href="http://netirane.al/login" target="_blank"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Login</span></a>

								<?
								$this->getPosition('pos-social')->printContent();
								?> 
								<?
								$this->getPosition('pos-menu')->printContent();
								?> 

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Header Parallax -->
		<?
		$this->getPosition('pos-banner-parallax')->printContent();
		?> 

        <div class="header-info-jarallax header-info">
            <!--Print anyway-->
            <div class="paralaxMask"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="header-info-content">
                            <!-- Award Wreaths -->
							<?
							$this->getPosition('pos-banner-text')->printContent();
							?> 
                        </div>
                    </div>
                    <div class="col-md-4">
						<?
						$this->getPosition('pos-contact-phone')->printContent();
						?> 
                    </div>
                </div>
            </div>
            <div class="main-fab">
                <a class="page-scroll " href="#about">
                    <button class="md-fab md-accent md-button md-default-theme" ng-transclude="">
                        <i class="fa fa-angle-down ion-android-star ng-scope"></i>
                        <div class="md-ripple-container"></div>
                    </button>
                </a>
            </div>
        </div>

        <!-- Description -->
        <section id="about" class="about-section">
            <div class="container-fluid description invert-color">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
							<?
							$this->getPosition('pos-description')->printContent();
							?> 
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </section>
        <!-- Businesses Related -->
        <section>
            <div class="container-fluid offers">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
							<?
							$this->getPosition('pos-businesses-related')->printContent();
							?> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="offers">
            <div class="container-fluid offers">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
							<?
							$this->getPosition('pos-offers')->printContent();
							?> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Photo Galley -->
        <section id="gallery" class="gallery-section">
			<?
			$this->getPosition('pos-gallery')->printContent();
			?> 
        </section>

        <div class="clearfix"></div>

        <section id="review-stat">
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
							<?
							$this->getPosition('pos-reviews-stat')->printContent();
							?>
                        </div>
                        <div class="col-md-12">
							<?
							$this->getPosition('pos-add-reviews')->printContent();
							?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="review">
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
							<?
							$this->getPosition('pos-reviews')->printContent();
							?> 
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="clearfix"></div>

        <!-- Contact Parallax -->
        <section id="contact-sm" class="contact-sm-section">
			<?
			$this->getPosition('pos-contact-parallax')->printContent();
			?> 
        </section>

        <!-- Map -->
        <section id="google-map" class="map-section">
			<?
			$this->getPosition('pos-map')->printContent();
			?> 
        </section>



        <!-- Adwords -->
		<?
		$this->getPosition('pos-adwords')->printContent();
		?> 

        <div class="clearfix"></div>

        <!-- Video & Socials -->
		<?
		$this->getPosition('pos-video')->printContent();
		?> 
        <!-- Contact -->
        <section id="contact" class="contact-section">
            <div class="container-fluid contactform">
                <div class="container">
                    <div id="contact" class="contactform">
                        <div class="row">

                            <div class="col-md-4">
                                <!-- Footer Contact Info -->
								<?
								$this->getPosition('pos-contact-footer')->printContent();
								?>
                                <br/>
                                <div class="col-md-6">
									<?
									$this->getPosition('pos-payment')->printContent();
									?> 
                                </div>
                            </div>

                            <div class="col-md-4">
								<?
								$this->getPosition('pos-footer-center')->printContent();
								?> 
                            </div>

                            <div class="col-md-4 text-center fb-fanpage">
                                <!-- Facebook Fan Page -->
								<?
								if ($this->getPosition('pos-facebook-page')->printContent(false) != '') {

									$this->getPosition('pos-facebook-page')->printContent();
								}
								?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="city-vector">
                <img class="" src="http://www.netirane.al/data/netirane.al/media/tirana-black.png" style="width: 100%;color: #e6e5e1;">
            </div>
        </section>

        <!-- Footer Address -->
        <section id="footer" class="footer-section">

            <!--            <div class="container-fluid footer1">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img class="" src="http://www.netirane.al/data/netirane.al/media/thumbs/1200x800/neTirane-smartphone.png" style="width: 100%;">
                                    </div>
                                </div>
                            </div>
                        </div>
            -->

            <!-- Footer Copyright -->
            <footer class="container-fluid text-center footer2">
                <div class="footer-content">
                    <div class="row" style="width: 100%;">
                        <div class="col-md-6">
                            <div class="bis-name">
                                <a href="http://www.neshqiperi.al" class="wow zoomIn animated footer-name" data-wow-delay=".5s">
                                    <img class="img_logo wow flash animated " data-wow-delay=".5s" src="http://www.neshqiperi.al/data/neshqiperi.al/media/thumbs/150x75/1483524258neShqiperi-logo.png">
                                </a>
                            </div>
                            <div class="poweredby">
                                <p style="margin: 0 0 5px;">© <?php echo date("Y"); ?> - Mundesuar nga rrjeti 
                                    <b><a target="_blank" href="http://neShqiperi.al">neShqiperi.al</a></b>
                                </p>
                                <p style="font-size: 10px;">+355 (0)4 450 0371 - Zogu i Zi, Tirana, Albania</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </section>

<!--        <script src="js/jarallax.js"></script>-->
        <script type="text/javascript">

            //                        $('.header-info-jarallax').jarallax({
            //                            speed: 0.5,
            //                            imgWidth: 1366,
            //                            imgHeight: 768
            //                        })


            //                        $('.contact-jarallax').jarallax({
            //                            speed: 0.5,
            //                            imgWidth: 1366,
            //                            imgHeight: 768
            //                        })
        </script>

        <!-- Rating system -->
        <script>

            //                    function addStatistic(id_business, type) {
            //
            //                        $.ajax({
            //                            type: "POST",
            //                            url: "<?= Utils::getComponentUrl('Businesses/ajx_addStatistic') ?>",
            //                            data: {
            //                                'id_business': id_business,
            //                                'type': type,
            //                            },
            //                            success: function (data) {
            //                            },
            //                            error: function (e) {
            //                                console.log(e);
            //                            }
            //                        });
            //
            //                    }
            //
            //                    function rate(rating, id_biz) {
            //                        $.ajax({
            //                            type: "POST",
            //                            url: "<?= Utils::getComponentUrl('Businesses/ajx_rateBusiness') ?>",
            //                            data: {
            //                                'id_biz': id_biz,
            //                                'rating': rating
            //                            },
            //                            success: function (msg) {
            //                                console.log(msg);
            //                                //success
            //                            },
            //                            error: function (e) {
            //                                console.log(e);
            //                            }
            //                        });
            //                    }
            //                    function controllCookie(id_biz) {
            //
            //                        $.ajax({
            //                            type: "POST",
            //                            url: "<?= Utils::getComponentUrl('Businesses/ajx_checkRatingCookie') ?>",
            //                            data: {
            //                                'id_biz': id_biz,
            //                            },
            //                            success: function (rating) {
            //
            //                                if (rating != 0) {
            //                                    for (var i = 1; i <= rating; i++) {
            //                                        $('#star_' + i).attr("class", "fa fa-star star-on-png fa-3x ratings_stars");
            //                                    }
            //                                    $('.ratings_stars').unbind();
            //                                }
            //
            //                            },
            //                            error: function (e) {
            //                                console.log(e);
            //                            }
            //                        });
            //
            //                    }
            //                    function ratingDataText(id_biz) {
            //                        $.ajax({
            //                            type: "POST",
            //                            url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValue') ?>" ,
            //                            data: {
            //                                'id_biz': id_biz,
            //                            },
            //                            success: function (data) {
            //                                d = data.split("|");
            //                                $('#average').html("Vleresimi: " + d[0] + " - Nr i votave: " + d[1]);
            //
            //                            },
            //                            error: function (e) {
            //                                $('#average').html("Pati probleme");
            //                                console.log(e);
            //                            }
            //                        });
            //                    }
            //                    function ratingDataTextShow(id_biz) {
            //                        $.ajax({
            //                            type: "POST",
            //                            url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValue') ?>,
            //                            data: {
            //                                'id_biz': id_biz,
            //                            },
            //                            success: function (data) {
            //                                d = data.split("|");
            //                                $('#showrate').html(d[0]);
            //
            //                            },
            //                            error: function (e) {
            //                                $('#average').html("Pati probleme");
            //                                console.log(e);
            //                            }
            //                        });
            //                    }
            //
            //
            //
            //                    function ratingData(id_biz) {
            //
            //                        $.ajax({
            //                            type: "POST",
            //                            url: "<?= Utils::getComponentUrl("Businesses/ajx_getRatingValueBList") ?>",
            //                            dataType: 'json',
            //                            data: {
            //                                'id_biz': id_biz,
            //                            },
            //                            success: function (data) {
            //
            //                                if (data.rate != 0) {
            //                                    for (var i = 0; i <= data.rate; i++)
            //                                        $('#star_' + i + '_' + id_biz).attr("class", "fa fa-star star-on-png");
            //                                    if (data.half == 1)
            //                                        $('#star_' + i + "_" + id_biz).attr("class", "fa fa-star-half-o star-on-png");
            //                                }
            //
            //                            },
            //                            error: function (e) {
            //                                console.log(e);
            //                            }
            //                        });
            //                        return false;
            //                    }
            //
            //
            //                    function ratingDataStar(id_biz) {
            //
            //                        $.ajax({
            //                            type: "POST",
            //                            url: "<?= Utils::getComponentUrl('Businesses/ajx_getRatingValueBList') ?>",
            //                            dataType: 'json',
            //                            data: {
            //                                'id_biz': id_biz,
            //                            },
            //                            success: function (data) {
            //
            //                                if (data.rate != 0) {
            //                                    for (var i = 0; i <= data.rate; i++)
            //                                        $('#star_' + i + '_' + id_biz).attr("class", "fa fa-star star-on-png");
            //                                    if (data.half == 1)
            //                                        $('#star_' + i + "_" + id_biz).attr("class", "fa fa-star-half-o star-on-png");
            //                                }
            //
            //                            },
            //                            error: function (e) {
            //                                console.log(e);
            //                            }
            //                        });
            //
            //                    }
            //
            //
            //                    $(document).ready(function () {
            //                        //yje ne title mbushi
            //                        ratingDataStar(1889);
            //                        //jep mesataren e rating te bere
            //                        ratingDataText(1889);
            //                        //shfaq mesataren e rating
            //                        ratingDataTextShow(1889);
            //                        //kontroll cookie nqs ka votuar
            //                        controllCookie(1889);
            //
            //                        addStatistic(1889, 'view');
            //
            //
            //                        $('#website_link').click(function () {
            //                            addStatistic(1889, 'website_click');
            //                        });
            //                        $('#facebook_link').click(function () {
            //                            addStatistic(1889, 'facebook_click');
            //                        });
            //                        $('#linkedin_link').click(function () {
            //                            addStatistic(1889, 'linkedin_click');
            //                        });
            //                        $('#instagram_link').click(function () {
            //                            addStatistic(1889, 'instagram_click');
            //                        });
            //
            //                        //hover
            //                        $('.ratings_stars').hover(
            //                                //mouseover
            //                                        function () {
            //                                            $(this).prevAll().andSelf().attr("class", "fa fa-star star-on-png  fa-3x ratings_stars");
            //                                        },
            //                                        //mouseout
            //                                                function () {
            //                                                    $(this).prevAll().andSelf().attr("class", "fa fa-star-o star-on-png fa-3x ratings_stars");
            //                                                }
            //                                        );
            //
            //                                        //click
            //                                        $('.ratings_stars').click(
            //                                                function () {
            //                                                    $(this).prevAll().andSelf().attr("class", "fa fa-star star-on-png  fa-3x ratings_stars");
            //                                                    var rating = $(this).prevAll().andSelf().length;
            //                                                    rate(rating, 1889);
            //                                                    $('.ratings_stars').unbind();
            //                                                    ratingDataText(1889);
            //                                                    ratingDataStar(1889);
            //                                                }
            //                                        );
            //
            //                                    });
        </script>

        <script>
            (function () {
                $('.info a.link').click(function () {
                    return false;
                });

                $('input').blur(function () {
                    if ($(this).val()) {
                        return $(this).addClass('filled');
                    } else {
                        return $(this).removeClass('filled');
                    }
                });

            }).call(this);

        </script>
    </body>

</html>