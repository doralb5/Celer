<!DOCTYPE html>
<html lang="it">
    <head>
        <!-- New Template on tmpl05 -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHTML::printFavicon(CMSSettings::$favicon); ?>
		<? HeadHTML::addMetaProperty('og:image', Utils::genThumbnailUrl(CMSSettings::$logo, 700, 600, array(), true)); ?>

		<? HeadHTML::printMeta(); ?>


        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.css"); ?>
		<? HeadHtml::linkStylesheet("animate.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("share-button.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHtml::linkStylesheet("accordion.css"); ?>
		<? HeadHTML::printStylesheetLinks(); ?>
        <!-- Javascript -->
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js") ?>
		<? HeadHTML::linkJS("https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js") ?>
		<? HeadHTML::linkJS("https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js") ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? // HeadHtml::linkJS("wow.min.js"); ?>
		<? HeadHtml::linkJS("smoothscroll.js"); ?>
		<? HeadHtml::linkJS("list.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHtml::linkJS("demo.js"); ?>
		<? HeadHtml::linkJS("loader.js"); ?>
		<? HeadHtml::linkJS("polyfills.js"); ?>
		<? HeadHtml::linkJS("typeahead.js"); ?>
		<? HeadHtml::linkJS("modernizr-2.6.2.min.js"); ?>
		<? HeadHtml::linkJS("jquery.easing.1.3.js"); ?>
		<? HeadHtml::linkJS("scrolling-nav.js"); ?>
		<? HeadHtml::linkJS("easing.js"); ?>
		<? HeadHTML::printJSLinks(); ?>
		<? HeadHTML::printScripts(); ?>

		<?= $this->getTemplate()->head_extra; ?>

		<? if ($this->template_params['background_image'] != "") { ?>
			<style>
				body {
					background-image: url("<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>");
					background-size: cover;
					background-repeat: no-repeat;
					background-attachment: fixed;
					background-position: center; 
				}
			</style>
		<? } ?>
        <script>
//            wow = new WOW(
//                    {
//                        boxClass: 'wow', // default
//                        animateClass: 'animated', // default
//                        offset: 30, // default
//                        mobile: true, // default
//                        live: true        // default
//                    }
//            )
//            wow.init();
        </script>
    <body>
		<? if ($this->getPosition('pos-preloader')->countBlocks()) { ?>
			<?
			$this->getPosition('pos-preloader')->printContent();
			?>     
		<? } ?>

        <!-- Header Section -->
        <div id="header">
			<? if ($this->getPosition('pos-helper')->countBlocks()) { ?>
				<div id="pos-helper" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-helper')->printContent();
								?>  
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Top Header -->
			<? if ($this->getPosition('pos-htop')->countBlocks()) { ?>
				<div id="pos-htop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-htop')->printContent();
								?>  
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Middle Header -->
            <div id="pos-hmiddle" class="container-fluid">
                <header id="main-header" class="navbar-fixed-top animated ">
                    <div class="row">
                        <div class="col-md-12">
							<?
							$this->getPosition('pos-hmiddle')->printContent();
							?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
							<?
							$this->getPosition('pos-logo')->printContent();
							?>
                        </div>

                        <div class="col-md-7">
							<?
							$this->getPosition('pos-search')->printContent();
							?>
                        </div>
                        <div class="col-md-3">
							<?
							$this->getPosition('pos-login')->printContent();
							?>
                        </div>
                    </div>
                </header>
            </div>



            <!-- Bottom Header -->
			<? if ($this->getPosition('pos-hbottom')->countBlocks()) { ?>
				<div id="pos-hbottom" class="contanier-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-md-offset-3">

								<?
								$this->getPosition('pos-searchbox')->printContent();
								?>
							</div>
						</div>
					</div>    

					<?
					$this->getPosition('pos-hbottom')->printContent();
					?>


				</div>
			<? } ?>

        </div>