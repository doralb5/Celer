//************ Scroll ToTop **********//
$(document).ready(function () {
    $('body').append('<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span></div>');
    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $('#toTop').click(function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });

});



//************ Lightbox **********//
$(document).ready(function ($) {

    // delegate calls to data-toggle="lightbox"
    $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
        event.preventDefault();
        return $(this).ekkoLightbox({
            onShown: function () {
                if (window.console) {
                    return console.log('Checking our the events huh?');
                }
            },
            onNavigate: function (direction, itemIndex) {
                if (window.console) {
                    return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                }
            }
        });
    });

    //Programatically call
    $('#open-image').click(function (e) {
        e.preventDefault();
        $(this).ekkoLightbox();
    });
    $('#open-youtube').click(function (e) {
        e.preventDefault();
        $(this).ekkoLightbox();
    });

    $(document).delegate('*[data-gallery="navigateTo"]', 'click', function (event) {
        event.preventDefault();
        return $(this).ekkoLightbox({
            onShown: function () {
                var a = this.modal_content.find('.modal-footer a');
                if (a.length > 0) {
                    a.click(function (e) {
                        e.preventDefault();
                        this.navigateTo(2);
                    }.bind(this));
                }
            }
        });
    });
});


//************ Collapsive Elements **********//
$(document).ready(function () {
    $('#services-btn').click(function () {
        $('#collapseLogin').collapse("hide");
        $('#collapseSearch').collapse("hide");
    });
    $('#login-btn').click(function () {
        $('#collapseServices').collapse("hide");
        $('#collapseSearch').collapse("hide");
        $('#collapseInsert').collapse("hide");
    });
    $('#search-btn').click(function () {
        $('#collapseServices').collapse("hide");
        $('#collapseLogin').collapse("hide");
        $('#collapseInsert').collapse("hide");
    });
    $('#insert').click(function () {
        $('#collapseLogin').collapse("hide");
        $('#collapseSearch').collapse("hide");
    });
});


