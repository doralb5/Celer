
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <!-- New Template on tmpl02 -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
        <link rel="icon" href="../../favicon.ico">

		<? HeadHtml::linkStylesheet("animate.css"); ?>
		<? HeadHtml::linkStylesheet("bootstrap.css"); ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("menu.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("superslides.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>

		<? HeadHTML::printStylesheetLinks(); ?>

		<? HeadHtml::linkJS("jquery.min.js"); ?>
		<? HeadHtml::linkJS("jquery.animate-enhanced.min.js"); ?>
		<? HeadHtml::linkJS("jquery.easing.1.3.js"); ?>
		<? HeadHtml::linkJS("jquery.superslides.min.js"); ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("wow.min.js"); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHtml::linkJS("main.js"); ?>

		<? HeadHTML::printJSLinks(); ?>

    </head>


    <body>

        <div id="header">

            <!-- Fullwidth Slider -->
			<? if ($this->getPosition('slider')->countBlocks()) { ?>
				<div id="pos-htop" class="wrapper">
					<?
					$this->getPosition('slider')->printContent();
					?>
				</div>
			<? } ?>


            <div class="container-fluid">
                <div class="row">

                    <!--Menu Toggle-->
                    <a href="#" id="showLeft"> <span class="navClosed"></span> </a>

                    <!--Small Logo-->
                    <div id="brand" class="">
						<?
						$this->getPosition('pos-brand')->printContent();
						?>

                    </div>

                    <!--Sidebar Menu-->
                    <div id="sidebar" class="col-md-3" style="">

                        <!--Menu Block-->
                        <div class="menu">
                            <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">

                                <!--Logo-->
                                <div class="logo">
                                    <a href="/">
                                        <img class="img-responsive" src="<?= WEBROOT . MEDIA_ROOT . CMSSettings::$logo ?>" alt="">
                                    </a>
                                </div>

                                <!--Menu-->
                                <ul class="sidebar-nav">

									<? $this->getPosition('pos-menu')->printContent(); ?>

                                    <a href="//www.bluehat.al" class="poweredby" id="poweredby" target="_blank"> <img src="http://manuelmoscati.com/data/hermesnika.com/media/poweredbybh.png"></a>

                                </ul>

                            </nav>
                        </div>
                    </div>

                    <!--Social test-->
                    <div id="social" class=" float-right">
                        <a href="https://www.facebook.com/manuelmoscati"><i class="fa fa-facebook-square"></i></a>
                        <a href="https://www.instagram.com/manuelmoscati"><i class="fa fa-instagram"></i></a>
                    </div>

                </div>
            </div>
        </div>

        <!-- Content Section -->
        <div id="content">

            <!-- Content Top -->
			<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
				<div id="pos-ctop" class="wrapper">
					<div class="overlay">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<?
									$this->getPosition('pos-ctop')->printContent();
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Content Middle -->
			<? if ($content != '' || $this->getPosition('pos-cmiddle')->countBlocks()) { ?>

				<div id="pos-cmiddle" class="wrapper">

					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?= $content ?>
								<?
								$this->getPosition('pos-cmiddle')->printContent();
								?>
							</div>
						</div>
					</div>

				</div>
			<? } ?>

            <!-- Content Bottom -->
			<? if ($this->getPosition('pos-cbottom')->countBlocks()) { ?>           
				<div id="pos-cbottom" class="wrapper">

					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-cbottom')->printContent();
								?>
							</div>
						</div>
					</div>

				</div>
			<? } ?>

        </div>

        <!-- Footer Section -->
        <div id="footer">
			<? if ($this->getPosition('pos-ftop')->countBlocks()) { ?>

				<?
				$this->getPosition('pos-ftop')->printContent();
				?>

			<? } ?>
        </div>

        <script>
            var menuLeft = document.getElementById('cbp-spmenu-s1'),
                    body = document.body;
            showLeft.onclick = function () {
                classie.toggle(this, 'active');
                classie.toggle(menuLeft, 'cbp-spmenu-open');
                disableOther('showLeft');
            };
            function disableOther(button) {
                if (button !== 'showLeft') {
                    classie.toggle(showLeft, 'disabled');
                }
            }
        </script>
        <script>
            (function ($) {
                // Menu Functions
                $(document).ready(function () {
                    $('#showLeft').click(function (e) {

                        // Set the timeout to the animation length in the CSS.
                        setTimeout(function () {
                            //console.log("timeout set");
                            $('#showLeft > span').toggleClass("navClosed").toggleClass("navOpen");
                        }, 200);
                        e.preventDefault();
                    });
                });
            })(jQuery);
        </script>
    </body>
</html>
