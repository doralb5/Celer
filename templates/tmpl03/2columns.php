<?php include 'header.php'; ?>

<!-- Middle Body -->
<? if ($content != '' || $this->getPosition('pos-cmiddle')->countBlocks()) { ?>
	<div id="pos-cmiddle" class="container-fluid">
		<div class="container cmiddle">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="body-container">
						<?= $content ?>
					</div>
				</div>
				<div class="col-md-4">
					<?
					$this->getPosition('pos-cmiddle')->printContent();
					?>
				</div>
			</div>
		</div>
	</div>
<? } ?>
<?php include 'footer.php'; ?>