<!-- Section 2 -->
<? if ($this->getPosition('pos-section-2')->countBlocks()) { ?>
	<div id="pos-section-2" class="container-fluid">
		<div class="container ">
			<div class="row">
				<div class="col-md-12">
					<?
					$this->getPosition('pos-section-2')->printContent();
					?>
				</div>
			</div>
		</div>
	</div>
<? } ?>

<!-- Bottom Body -->
<? if ($this->getPosition('pos-cbottom')->countBlocks()) { ?>
	<div id="pos-cbottom" class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?
					$this->getPosition('pos-cbottom')->printContent();
					?>
				</div>
			</div>
		</div>
	</div>
<? } ?>

</div>


<!-- Footer Section -->
<div id="footer">

    <!-- Top Footer -->
	<? if ($this->getPosition('pos-ftop')->countBlocks()) { ?>
		<div id="pos-ftop" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-ftop')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Middle Footer -->
	<? //if ($this->getPosition('pos-fmiddle')->countBlocks()) { ?>
    <div id="pos-fmiddle" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
					<?
					$this->getPosition('pos-fmiddle')->printContent();
					?>
                </div>
                <div class="col-md-3">
					<?
					$this->getPosition('pos-social-fanpage')->printContent();
					?>
                </div>
            </div>
        </div>
    </div>
	<? //} ?>

    <!-- Bootom Footer -->

    <div id="pos-fbottom" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-12 copyright">
					<?
					$this->getPosition('pos-fbottom')->printContent();
					?>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12 logo">
                    <span class="poweredby">
                        <a href="<?= BRAND_URL ?>" target="_blank">
                            <img src="<?= BRAND_LOGO ?>" alt=""/>
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </div>

</div>



</body>
</html>
<!--BHCms V1.2 by BlueHat Sh.p.k and Doralb Kurti.-->