<!DOCTYPE html>
<html lang="it">
    <head>
        <!-- New Template on tmpl03 -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
        <link rel="icon" href="../../favicon.ico">

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("animate.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("menu.css"); ?>
		<? HeadHtml::linkStylesheet("superslides.css"); ?>
		<? HeadHtml::linkStylesheet("owl.carousel.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
        <link href="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/3.3.0/ekko-lightbox.min.css" rel="stylesheet">

		<? HeadHTML::printStylesheetLinks(); ?>


        <!-- Javascript -->
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js") ?>
		<? HeadHtml::linkJS("jquery.animate-enhanced.min.js"); ?>
		<? HeadHtml::linkJS("jquery.easing.1.3.js"); ?>
		<? HeadHtml::linkJS("fx.js"); ?>
		<? HeadHtml::linkJS("jquery.superslides.min.js"); ?>
		<? HeadHtml::linkJS("easing.js"); ?>
		<? HeadHtml::linkJS("scrolling-nav.js"); ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("wow.min.js"); ?>
		<? HeadHtml::linkJS("owl.carousel.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHtml::linkJS("materialize.min.js"); ?>

		<? HeadHTML::printJSLinks(); ?>

        <script>
            wow = new WOW(
                    {
                        boxClass: 'wow', // default
                        animateClass: 'animated', // default
                        offset: 30, // default
                        mobile: true, // default
                        live: true        // default
                    }
            )
            wow.init();
        </script>

    </head>

    <body>


        <!-- Cookies Bar -->
		<? if ($this->getPosition('pos-cookies')->countBlocks()) { ?>
			<div id="pos-cookies" class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('pos-cookies')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		<? } ?>


        <!-- Header Section -->
        <div id="header">

            <!-- Fullwidth Slider -->
			<? if ($this->getPosition('pos-slider')->countBlocks()) { ?>
				<div id="pos-htop" class="wrapper">
					<?
					$this->getPosition('pos-slider')->printContent();
					?>
				</div>
			<? } ?>


            <!-- Middle Header -->
			<? if ($this->getPosition('pos-hmiddle')->countBlocks()) { ?>
				<div id="pos-hmiddle" class="container-fluid">
					<?
					$this->getPosition('pos-hmiddle')->printContent();
					?>
				</div>
			<? } ?>

            <!--Menu + Logo-->
			<? if ($this->getPosition('pos-menu')->countBlocks()) { ?>
				<header id="main-header" class="navbar-fixed-top animated">
					<div id="" class="container-fluid">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<?
									$this->getPosition('pos-menu')->printContent();
									?>
								</div>
							</div>
						</div>
					</div>
				</header>
			<? } ?>





            <!-- Bottom Header -->
			<? if ($this->getPosition('pos-hbottom')->countBlocks()) { ?>
				<div id="pos-hbottom" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-hbottom')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

        </div>

        <!-- Body Section -->
        <div id="content">

            <!-- Top Body -->
			<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
				<div id="pos-ctop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-ctop')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Section 1  -->
			<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
				<div id="pos-section-1" class="">
					<div class="dark-overlay">

						<?
						$this->getPosition('pos-section-1')->printContent();
						?>

					</div>
				</div>
			<? } ?>

            <!-- Section 3 -->
			<? if ($this->getPosition('pos-section-3')->countBlocks()) { ?>
				<div id="pos-section-3" class="container-fluid">
					<div class="container ">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-section-3')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Section 4 -->
			<? if ($this->getPosition('pos-section-4')->countBlocks()) { ?>
				<div id="pos-section-4" class="wrapper">
					<div class="dark-overlay">

						<?
						$this->getPosition('pos-section-4')->printContent();
						?>

					</div>
				</div>
			<? } ?>
