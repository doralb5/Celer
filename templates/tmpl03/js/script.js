
/***** Kaon Owl Carousel *****/
$(document).ready(function () {
    $("#kaon_carousel").owlCarousel({
        items: 1,
        lazyLoad: true,
        navigation: true,
        navigationText: true,
        pagination: false,
        autoPlay: 10000,
        singleItem: true

    });
    $('#products').hover(
            //mouseover
                    function () {
                        $(this).find("div.single-product").find("div.product-img").find("img").addClass("blur-effect");
                    },
                    //mouseout
                            function () {
                                $(this).find("div.single-product").find("div.product-img").find("img").removeClass("blur-effect");
                            }
                    );
                });



        /***** Partners Owl Carousel *****/
        $(document).ready(function () {
            $("#owl-partners").owlCarousel({
                items: 8,
                lazyLoad: true,
                autoPlay: true,
                pagination: false,
            });
        });


        /***** Header Menu Fixed *****/
        $(document).ready(function () {
            var cbpAnimatedHeader = (function () {

                var docElem = document.documentElement,
                        header = $('#main-header');
                didScroll = false,
                        changeHeaderOn = 100;

                function init() {
                    window.addEventListener('scroll', function (event) {
                        if (!didScroll) {
                            didScroll = true;
                            setTimeout(scrollPage, 100);
                        }
                    }, false);
                }

                function scrollPage() {
                    var sy = scrollY();
                    if (sy >= changeHeaderOn) {
                        header.addClass('navbar-shrink');
                        // header.addClass('slideInDownBig');
                    } else if (sy == 0) {
                        header.removeClass('navbar-shrink');
                        //header.removeClass('slideInDownBig');
                    }
                    didScroll = false;
                }

                function scrollY() {
                    return window.pageYOffset || docElem.scrollTop;
                }

                init();

            })();
        });


        //************ Scroll ToTop **********//
        $(document).ready(function () {
            $('body').append('<div id="toTop" class="btn btn-info"><span class="fa fa-chevron-up"></span></div>');
            $(window).scroll(function () {
                if ($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });
            $('#toTop').click(function () {
                $("html, body").animate({scrollTop: 0}, 600);
                return false;
            });
        });