<!DOCTYPE html>
<html lang="it">
    <head>
        <!-- New Template on tmpl05 -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
        <link rel="icon" href="<?= CMSSettings::$favicon ?>">
        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.css"); ?>
		<? HeadHtml::linkStylesheet("animate.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHtml::linkStylesheet("accordion.css"); ?>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css" />

		<? HeadHTML::printStylesheetLinks(); ?>
        <!-- Javascript -->
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js") ?>
		<? HeadHTML::linkJS("http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js") ?>
		<? HeadHTML::linkJS("http://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js") ?>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? // HeadHtml::linkJS("wow.min.js"); ?>
		<? HeadHtml::linkJS("smoothscroll.js"); ?>
		<? HeadHtml::linkJS("list.js"); ?>
		<? HeadHtml::linkJS("demo.js"); ?>
		<? HeadHtml::linkJS("loader.js"); ?>
		<? HeadHtml::linkJS("polyfills.js"); ?>
		<? HeadHtml::linkJS("modernizr-2.6.2.min.js"); ?>
		<? HeadHtml::linkJS("jquery.easing.1.3.js"); ?>
		<? HeadHtml::linkJS("scrolling-nav.js"); ?>
		<? HeadHtml::linkJS("easing.js"); ?>
		<? HeadHTML::printJSLinks(); ?>



    <body>
		<? if ($this->getPosition('pos-preloader')->countBlocks()) { ?>
			<?
			$this->getPosition('pos-preloader')->printContent();
			?>     
		<? } ?>

        <!-- Header Section -->
        <div id="header">
			<? if ($this->getPosition('pos-helper')->countBlocks()) { ?>
				<div id="pos-helper" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-helper')->printContent();
								?>  
							</div>
						</div>
					</div>
				</div>
			<? } ?>
            <!-- Middle Header -->
            <div id="pos-hmiddle" class="container-fluid" style="background: #2a2a2a;">
                <div class="container">

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
							<?
							$this->getPosition('pos-logo')->printContent();
							?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Top Body -->
		<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
			<div id="pos-ctop" class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('pos-ctop')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		<? } ?>


        <!-- Middle Body -->
		<? if ($content != '') { ?>
			<div id="pos-cmiddle" class="container-fluid" style="background: #ffffff;">
				<div class="container">
					<div class="cmiddle">
						<div class="row">
							<div class="col-md-12">
								<div class="body-container">

									<?= $content ?>

									<?
									$this->getPosition('pos-cmiddle')->printContent();
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<? } ?>




        <!-- Footer Section -->
        <div id="footer" class="conainer-fluid">

            <!-- Bootom Footer -->
            <div id="pos-fbottom" class="contanier-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12 copyright">
							<?
							$this->getPosition('pos-fbottom')->printContent();
							?>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 logo">
                            <span class="poweredby">
                                <a href="<?= BRAND_URL ?>" target="_blank">
                                    <img src="<?= BRAND_LOGO ?>" alt=""/>
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>    
</html>