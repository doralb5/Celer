<?php include 'header.php'; ?>
<!-- Body Section -->
<div id="content">

    <!-- Top Body -->
	<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
		<div id="pos-ctop" class="wrapper">
			<?
			$this->getPosition('pos-ctop')->printContent();
			?>
		</div>
	<? } ?>

    <!-- Middle Body -->

    <div id="pos-cmiddle" class="container-fluid">
        <div class="container cmiddle">
            <div class="row">
                <div class="col-md-12">
                    <div class="body-container">
						<?
						$this->getPosition('pos-cmiddle')->printContent();
						?>
						<?= $content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Bottom Body -->
	<? if ($this->getPosition('pos-cbottom')->countBlocks()) { ?>
		<div id="pos-cbottom" class="contanier-fluid">
			<div class="darker-overlay">
				<div class="container-fluid">
					<div class="row">
						<div class="">
							<?
							$this->getPosition('pos-cbottom')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

</div>
<?php include 'footer.php'; ?>
        