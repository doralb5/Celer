<!DOCTYPE html>
<html lang="it">
    <head>
        <!-- New Template on tmpl02 -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHTML::printFavicon(BRAND_FAVICON); ?>

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet('style.css'); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHtml::linkStylesheet("menu.css"); ?>

		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js") ?>
		<? HeadHTML::linkJS("http://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js") ?>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRs6TUKT0XI_21c8V_ETOiDtBccf_h2_k&signed_in=true&callback=initMap"></script>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("richmarker.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHTML::printJSLinks(); ?>
        <style>
            body {
                background-image: url("<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>");
                background-size: cover;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center; 
            }
        </style>
    </head>

    <body>

        <!-- Header Section -->

        <div id="header">

            <!-- Top Header -->
			<? if ($this->getPosition('pos-htop')->countBlocks()) { ?>
				<div id="pos-htop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-htop')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Middle Header -->
			<? if ($this->getPosition('pos-hmiddle')->countBlocks()) { ?>
				<div id="pos-hmiddle" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-hmiddle')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Bottom Header -->
			<? if ($this->getPosition('pos-hbottom')->countBlocks()) { ?>
				<div id="pos-hbottom" class="contanier-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-hbottom')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

        </div>