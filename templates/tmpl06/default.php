<!DOCTYPE html>
<html lang="it">
    <head>
        <!-- New Template on tmpl02 -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHTML::printFavicon(BRAND_FAVICON); ?>

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("menu.css"); ?>
        <link href="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/3.3.0/ekko-lightbox.min.css" rel="stylesheet">


		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js") ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
        <script src="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/3.3.0/ekko-lightbox.min.js"></script>
		<? HeadHTML::printJSLinks(); ?>
        <style>
            body {
                background-image: url("<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>");
                background-size: cover;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center; 
            }
        </style>
    </head>

    <body>

        <!-- Header Section -->

        <div id="header">

            <!-- Top Header -->
			<? if ($this->getPosition('pos-htop')->countBlocks()) { ?>
				<div id="pos-htop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-htop')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Middle Header -->
			<? if ($this->getPosition('pos-hmiddle')->countBlocks()) { ?>
				<div id="pos-hmiddle" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-hmiddle')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Bottom Header -->
			<? if ($this->getPosition('pos-hbottom')->countBlocks()) { ?>
				<div id="pos-hbottom" class="contanier-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-hbottom')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

        </div>

        <!-- Body Section -->
        <div id="content">

            <!-- Top Body -->
			<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
				<div id="pos-ctop" class="wrapper">
					<?
					$this->getPosition('pos-ctop')->printContent();
					?>
				</div>
			<? } ?>

            <!-- Middle Body -->

            <div id="pos-cmiddle" class="container-fluid">
                <div class="container cmiddle">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="body-container">
								<?
								$this->getPosition('pos-cmiddle')->printContent();
								?>
								<?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Bottom Body -->
			<? if ($this->getPosition('pos-cbottom')->countBlocks()) { ?>
				<div id="pos-cbottom" class="contanier-fluid">
					<div class="darker-overlay">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<?
									$this->getPosition('pos-cbottom')->printContent();
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

        </div>


        <!-- Footer Section -->
        <div id="footer" class="conainer-fluid">

            <!-- Top Footer -->
			<? if ($this->getPosition('pos-ftop')->countBlocks()) { ?>
				<div id="pos-ftop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-ftop')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Middle Footer -->
			<? //if ($this->getPosition('pos-fmiddle')->countBlocks()) { ?>
            <div id="pos-fmiddle" class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
							<?
							$this->getPosition('pos-fmiddle')->printContent();
							?>
                        </div>
                    </div>
                </div>
            </div>
			<? //} ?>

            <!-- Bootom Footer -->

            <div id="pos-fbottom" class="contanier-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12 copyright">
							<?
							$this->getPosition('pos-fbottom')->printContent();
							?>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 logo">
                            <span class="poweredby">
                                <a href="<?= BRAND_URL ?>" target="_blank">
                                    <img src="<?= BRAND_LOGO ?>" alt=""/>
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script type="text/javascript">
            $(document).ready(function ($) {

                // delegate calls to data-toggle="lightbox"
                $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function () {
                            if (window.console) {
                                return console.log('Checking our the events huh?');
                            }
                        },
                        onNavigate: function (direction, itemIndex) {
                            if (window.console) {
                                return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                            }
                        }
                    });
                });

                //Programatically call
                $('#open-image').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });
                $('#open-youtube').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });

                $(document).delegate('*[data-gallery="navigateTo"]', 'click', function (event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function () {
                            var a = this.modal_content.find('.modal-footer a');
                            if (a.length > 0) {
                                a.click(function (e) {
                                    e.preventDefault();
                                    this.navigateTo(2);
                                }.bind(this));
                            }
                        }
                    });
                });

            });
        </script>
    </body>    
</html>
