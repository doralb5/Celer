<? include 'header.php'; ?>

<div class="content">
    <div class="container-fluid">
        <div class="container">
            <div class="body-container">
				<? if ($this->getPosition('pos-l3')->countBlocks() || $content != '') { ?>
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('pos-l3')->printContent();
							?>
							<? WebPage::$breadcrumb->render(); ?>
							<?= $content ?>
						</div>
					</div>
				<? } ?>
            </div>
        </div>
    </div>
</div>


<? include 'footer.php'; ?>
