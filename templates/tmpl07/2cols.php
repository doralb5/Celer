<? include 'header.php'; ?>

<div class="content">
    <div class="container-fluid">
        <div class="container">
			<? WebPage::$breadcrumb->render(); ?>
            <div class="body-container">
                <div class="row">
					<? if ($this->getPosition('pos-l1')->countBlocks() || $content != '') { ?>
						<div class="col-md-9">
							<?
							$this->getPosition('pos-l1')->printContent();
							?>

							<?= $content ?>
							<div class="row">
								<? if ($this->getPosition('pos-c21')->countBlocks()) { ?>
									<div class="col-md-12 pos-c21">
										<?
										$this->getPosition('pos-c21')->printContent();
										?>
									</div>
								<? } ?>
							</div>
						</div>
					<? } ?>
					<? if ($this->getPosition('pos-c3')->countBlocks()) { ?>
						<div class="col-md-3">
							<?
							$this->getPosition('pos-c3')->printContent();
							?>
						</div>
					<? } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<? include 'footer.php'; ?>