<? include 'header.php'; ?>
<!-- COMP -->
<!-- Content Section -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="body-container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
							<? if ($this->getPosition('pos-c12')->countBlocks()) { ?>
								<div class="col-md-12">
									<?
									$this->getPosition('pos-c12')->printContent();
									?>
								</div>
							<? } ?>
                        </div>

                        <div class="row">
                            <!-- Right Part   -->
							<? // if ($this->getPosition('pos-c1')->countBlocks()) { ?>
                            <!--                                <div class="col-md-9 pos-c1">
							<?
							// $this->getPosition('pos-c1')->printContent();
							?>
                                                            </div>-->
							<? //} ?>

                            <!-- Middle Part   -->
							<? if ($this->getPosition('pos-c2')->countBlocks() || $content != '') { ?>
								<div class="col-md-12 pos-c2">
									<?
									$this->getPosition('pos-c2')->printContent();
									?>
									<? WebPage::$breadcrumb->render(); ?>
									<?= $content ?>
								</div>
							<? } ?>
                        </div>

                        <div class="row">
							<? if ($this->getPosition('pos-c21')->countBlocks()) { ?>
								<div class="col-md-12 pos-c21">
									<?
									$this->getPosition('pos-c21')->printContent();
									?>
								</div>
							<? } ?>
                        </div>
                    </div>

                    <!-- Left Part   -->
					<? if ($this->getPosition('pos-c3')->countBlocks()) { ?>
						<div class="col-md-3 pos-c3">
							<?
							$this->getPosition('pos-c3')->printContent();
							?>
						</div>
					<? } ?>

                </div>
            </div>
        </div>
    </div>
</div>


<? include 'footer.php'; ?>