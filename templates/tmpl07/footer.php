  
<!-- Footer Section -->
<div class="footer">
    <div class="container-fluid">
        <div class="container">

            <!-- Top Footer -->
			<? if ($this->getPosition('pos-f1')->countBlocks()) { ?>
				<div id="top-footer">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('pos-f1')->printContent();
							?>
						</div>
					</div>
				</div>
			<? } ?>
        </div>
    </div>
    <!-- Middle Footer -->
    <div class="container-fluid"  id="middle-footer">
        <div class="container">
            <div>
                <div class="row">
					<? if ($this->getPosition('pos-f2')->countBlocks()) { ?>
						<div class="col-md-8">
							<?
							$this->getPosition('pos-f2')->printContent();
							?>
						</div>
					<? } ?>
					<? if ($this->getPosition('pos-f2')->countBlocks()) { ?>
						<div class="col-md-4">
							<?
							$this->getPosition('pos-f22')->printContent();
							?>
						</div>
					<? } ?>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid" id="bottom-footer">
        <div class="container">
            <!-- Bottom Footer -->
			<? if ($this->getPosition('pos-f3')->countBlocks()) { ?>
				<div >
					<div class="row">
						<div class="col-md-10">
							<?
							$this->getPosition('pos-f3')->printContent();
							?>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12 logo">
							<span class="poweredby">
								<a href="<?= BRAND_URL ?>" target="_blank">
									<img src="<?= BRAND_LOGO ?>" alt=""/>
								</a>
							</span>
						</div>
					</div>
				</div>
			<? } ?>
        </div>
    </div>
</div>
</body>
</html>



