<!DOCTYPE html>
<html lang="it">
    <head>
        <!-- New Template on tmpl07 -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHTML::printFavicon(BRAND_FAVICON); ?>

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("owl.carousel.css"); ?>
		<? HeadHtml::linkStylesheet("animate.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>

		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js") ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("owl.carousel.js"); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHtml::linkJS("jquery.allofthelights.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHtml::linkJS("wow.min.js"); ?>
		<? HeadHTML::printJSLinks(); ?>


        <script>
            wow = new WOW(
                    {
                        boxClass: 'wow', // default
                        animateClass: 'animated', // default
                        offset: 30, // default
                        mobile: true, // default
                        live: true        // default
                    }
            )
            wow.init();
        </script>
        <script>
            $(document).ready(function () {
                $("#owl-gallery").owlCarousel({
                    items: 6,
                    lazyLoad: true,
                    autoPlay: true,
                    pagination: false,
                });
            });
        </script>

    </head>

    <body>

        <!-- Header Section -->
        <div class="header" >

            <div class="container-fluid black-header">
                <div class="container">
                    <!-- First Row   -->
					<? if ($this->getPosition('pos-h1')->countBlocks()) { ?>
						<div class="row">
							<div class="col-md-12 hfirst">
								<div class="bn-title" style="width: auto;float: left;"><h2 style="/* display: inline-block; */">FLASH</h2><span></span></div>
								<?
								$this->getPosition('pos-h1')->printContent();
								?>
							</div>
						</div>
					<? } ?>
                </div> 
            </div>

            <!-- Second  Row   -->
            <div id="main-header" class="animated">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
							<? if ($this->getPosition('pos-h2')->countBlocks()) { ?>
								<div class="col-md-8 hsecond">
									<?
									$this->getPosition('pos-h2')->printContent();
									?>
								</div>
							<? } ?>


							<? if ($this->getPosition('pos-h3')->countBlocks()) { ?>
								<div class="col-md-4 hthird">
									<?
									$this->getPosition('pos-h3')->printContent();
									?>
								</div>
							<? } ?>
                        </div>
                    </div>
                </div> 
            </div>

            <!-- Third Row   -->
            <div class="container-fluid header-background">
                <div class="container">
					<? if ($this->getPosition('pos-h4')->countBlocks()) { ?>
						<div class="row">
							<div class="col-md-12 hfourth">
								<?
								$this->getPosition('pos-h4')->printContent();
								?>
							</div>
						</div>
					<? } ?>
                </div>
            </div>
            <!-- Fourth Row   -->
            <div class="wrapper">
				<? if ($this->getPosition('pos-h6')->countBlocks()) { ?>
					<div class="">
						<div class="hfourth">
							<?
							$this->getPosition('pos-h6')->printContent();
							?>
						</div>
					</div>
				<? } ?>
            </div>
        </div>
