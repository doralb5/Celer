/***** Header Menu Fixed *****/
$(document).ready(function () {
    var cbpAnimatedHeader = (function () {

        var docElem = document.documentElement,
                header = $('#main-header');
        didScroll = false,
                changeHeaderOn = 100;

        function init() {
            window.addEventListener('scroll', function (event) {
                if (!didScroll) {
                    didScroll = true;
                    setTimeout(scrollPage, 100);
                }
            }, false);
        }

        function scrollPage() {
            var sy = scrollY();
            if (sy >= changeHeaderOn) {
                header.addClass('navbar-shrink');
                header.addClass('slideInDown');
            } else if (sy == 0) {
                header.removeClass('navbar-shrink');
                header.removeClass('slideInDown');
            }
            didScroll = false;
        }

        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }

        init();

    })();
});

