<!DOCTYPE html>
<html lang="it">
    <head>
        <!-- New Template on tmpl08 -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHTML::printFavicon(BRAND_FAVICON); ?>

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("menu.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHtml::linkStylesheet("owl.carousel.css"); ?>
		<? HeadHtml::linkStylesheet("bells.css"); ?>
		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->
		<? HeadHTML::linkJS("https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js") ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("jquery.easing.1.3.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHtml::linkJS("owl.carousel.js"); ?>
		<? HeadHtml::linkJS("bells.js"); ?>
		<? HeadHTML::printJSLinks(); ?>

    </head>

    <body>

        <!-- Header Section -->

        <div id="header">
            <!-- Top Header -->
			<? if ($this->getPosition('pos-htop')->countBlocks()) { ?>
				<div id="pos-htop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-htop')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Middle Header -->
			<? if ($this->getPosition('pos-hmiddle')->countBlocks()) { ?>
				<div id="pos-hmiddle" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-hmiddle')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Bottom Header -->
			<? if ($this->getPosition('pos-hbottom')->countBlocks()) { ?>
				<div id="pos-hbottom" class="contanier-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-hbottom')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

        </div>

        <!-- Body Section -->
        <div id="content" class="conainer-fluid">

            <!-- Slider On Body -->
			<? if ($this->getPosition('pos-slider')->countBlocks()) { ?>
				<div id="pos-banner" class="container-fluid">
					<div class="row">
						<?
						$this->getPosition('pos-slider')->printContent();
						?>
					</div>
				</div>
			<? } ?>

            <!-- Top Body -->
			<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
				<div id="pos-ctop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-ctop')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>


            <!-- Middle Body -->
			<? //if ($content != '') { ?>
            <div id="pos-cmiddle" class="container-fluid">
                <div class="container">
                    <div class="cmiddle">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="body-container">

									<? if (Webpage::showContentHeading() && WebPage::getContentHeading() != '') { ?><div id="#content-heading" class="content-heading"><h1><?= WebPage::getContentHeading(); ?></h1></div><? } ?>

									<?
									$this->getPosition('pos-content-top')->printContent();
									?>
                                    <!-- Main Content -->
									<?= $content ?>
                                    <!-- end Main Content -->
									<?
									$this->getPosition('pos-cmiddle')->printContent();
									?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<? //} ?>

            <!-- Section-0 -->
			<? if ($this->getPosition('pos-section0')->countBlocks()) { ?>
				<div id="pos-section0" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-section0')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Section 1 -->
			<? if ($this->getPosition('pos-section1')->countBlocks()) { ?>
				<div id="pos-section1" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-section1')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Parallax Section -->
			<? if ($this->getPosition('pos-parallax')->countBlocks()) { ?>
				<div id="pos-parallax" class="">
					<?
					$this->getPosition('pos-parallax')->printContent();
					?>
				</div>
			<? } ?>

            <!-- Section 2 -->
			<? if ($this->getPosition('pos-section2')->countBlocks()) { ?>
				<div id="pos-section2" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-section2')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>


            <!-- Section 3 -->
			<? if ($this->getPosition('pos-section3')->countBlocks()) { ?>
				<div id="pos-section3" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-section3')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Section 4 -->
			<? if ($this->getPosition('pos-section4')->countBlocks()) { ?>
				<div id="pos-section4" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-section4')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Bottom Body -->
			<? if ($this->getPosition('pos-cbottom')->countBlocks()) { ?>
				<div id="pos-cbottom" class="contanier-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-cbottom')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

        </div>


        <!-- Footer Section -->
        <div id="footer" class="conainer-fluid">

            <!-- Top Footer -->
			<? if ($this->getPosition('pos-ftop')->countBlocks()) { ?>
				<div id="pos-ftop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-ftop')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Middle Footer -->
			<? //if ($this->getPosition('pos-fmiddle')->countBlocks()) {  ?>
            <div id="pos-fmiddle" class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-sm-9 col-xs-12">
							<?
							$this->getPosition('pos-fmiddle')->printContent();
							?>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
							<?
							$this->getPosition('pos-social-fanpage')->printContent();
							?>
                        </div>
                    </div>
                </div>
            </div>
			<? //}  ?>

            <!-- Bootom Footer -->

            <div id="pos-fbottom" class="contanier-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12 copyright">
							<?
							$this->getPosition('pos-fbottom')->printContent();
							?>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 logo">
                            <span class="poweredby">
                                <a href="<?= BRAND_URL ?>" target="_blank">
                                    <img src="<?= BRAND_LOGO ?>" alt=""/>
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script type="text/javascript">
            $(document).ready(function ($) {

                // delegate calls to data-toggle="lightbox"
                $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function () {
                            if (window.console) {
                                return console.log('Checking our the events huh?');
                            }
                        },
                        onNavigate: function (direction, itemIndex) {
                            if (window.console) {
                                return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                            }
                        }
                    });
                });

                //Programatically call
                $('#open-image').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });
                $('#open-youtube').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });

                $(document).delegate('*[data-gallery="navigateTo"]', 'click', function (event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function () {
                            var a = this.modal_content.find('.modal-footer a');
                            if (a.length > 0) {
                                a.click(function (e) {
                                    e.preventDefault();
                                    this.navigateTo(2);
                                }.bind(this));
                            }
                        }
                    });
                });

            });


            $('ul.nav li.dropdown').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(300);
            }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(300);
            });
        </script>
    </body>    
</html>
