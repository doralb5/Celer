<?php include 'header.php'; ?>
<!-- Body Section -->
<div id="content">

    <!-- Top Body -->
	<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
		<div id="pos-ctop" class="wrapper">
			<?
			$this->getPosition('pos-ctop')->printContent();
			?>
		</div>
	<? } ?>

    <!-- Middle Body -->
	<? if ($this->getPosition('pos-left')->countBlocks() || $this->getPosition('pos-middle')->countBlocks() || $this->getPosition('pos-right')->countBlocks() || $content != '') { ?>
		<div id="pos-cmiddle" class="container-fluid">
			<div class="container cmiddle">
				<div class="row">
					<div class="col-md-2">
						<div class="">
							<?
							$this->getPosition('pos-left')->printContent();
							?>
						</div>
					</div>
					<div class="col-md-7">
						<div class="">
							<?
							$this->getPosition('pos-middle')->printContent();
							?>
							<?= $content ?>
						</div>
					</div>
					<div class="col-md-3">
						<div class="">
							<?
							$this->getPosition('pos-right')->printContent();
							?>
						</div>
					</div>

				</div>
			</div>
		</div>
	<? } ?>

    <!-- Bottom Body -->
	<? if ($this->getPosition('pos-cbottom')->countBlocks()) { ?>
		<div id="pos-cbottom" class="contanier-fluid">
			<div class="darker-overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('pos-cbottom')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

</div>
<?php include 'footer.php'; ?>
        