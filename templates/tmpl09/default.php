<? include 'header.php'; ?>


<!-- Body Section -->
<div id="content" class="">

    <!-- Top Body -->
	<? if ($this->getPosition('position-12')->countBlocks() || $this->getPosition('position-search')->countBlocks()) { ?>
		<div id="pos-ctop" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 position-12">
						<? $this->getPosition('position-title')->printContent(); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-12 col-xs-12 position-12">
						<? $this->getPosition('position-search')->printContent(); ?>
					</div>
					<div class="col-md-9 col-sm-12 col-xs-12 position-12">
						<? $this->getPosition('position-12')->printContent(); ?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Middle Body -->
	<? if ($this->getPosition('position-16')->countBlocks() || $content != '') { ?>
		<div id="pos-cmiddle" class="container-fluid">
			<div class="container cmiddle">

				<? if ($content !== "") { ?>
					<div class="row" id='content-bacg'>
						<? if ($this->getPosition('position-16')->countBlocks()) { ?>

							<div class="col-md-8 col-sm-8 col-xs-12 content">
								<?= $content ?>
							</div>

							<div class="col-md-4 col-sm-4 col-xs-12 content1">
								<? $this->getPosition('position-16')->printContent(); ?>
							</div>

						<? } else { ?>
							<div class="col-md-12 col-sm-12 col-xs-12 content">
								<?= $content ?>
							</div>
						<? } ?>
					</div>
				<? } ?>

			</div>
		</div>
	<? } ?>

    <!-- Bottom Body -->
	<? if ($this->getPosition('position-cbottom')->countBlocks()) { ?>
		<div id="pos-cbottom" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<? $this->getPosition('position-cbottom')->printContent(); ?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

</div>




<? include 'footer.php'; ?> 
