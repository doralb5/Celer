<!-- Footer Section -->
<div id="footer" class="">

    <!-- Top Footer -->
    <div id="pos-ftop" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div>

    <!-- Middle Footer -->
    <div id="pos-fmiddle" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">

                </div>
            </div>
        </div>
    </div>



    <!-- Bootom Footer -->
    <div id="pos-fbottom" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-12 copyright">
					<?
					$this->getPosition('position-26')->printContent();
					?>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12 poweredby">
                    <span>
                        <a href="<?= BRAND_URL ?>" target="_blank">
                            <img src="<?= BRAND_LOGO ?>" alt=""/>
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </div>

</div>




</body>
</html>
