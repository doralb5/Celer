<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHTML::printFavicon(BRAND_FAVICON); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHtml::linkStylesheet('bootstrap.min.css'); ?>
		<? HeadHtml::linkStylesheet("http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHTML::linkStylesheet($this->template_path . "libs/datetimepicker-master/jquery.datetimepicker.css"); ?>
		<? HeadHtml::linkStylesheet('style.css'); ?>
		<? HeadHtml::linkStylesheet('menu.css'); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHtml::linkStylesheet("owl.carousel.css"); ?>
		<? HeadHTML::printStylesheetLinks(); ?>

		<? HeadHTML::linkJS('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'); ?>
		<? HeadHTML::linkJS('bootstrap.min.js'); ?>
		<? HeadHTML::linkJS($this->template_path . "libs/datetimepicker-master/jquery.datetimepicker.js"); ?>
		<? HeadHTML::linkJS('scripts.js'); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHtml::linkJS("owl.carousel.js"); ?>
		<? HeadHTML::printJSLinks(); ?>
        <style>
<? if (isset($this->template_params['background_image'])) { ?>
				body {
					background-image: url("<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>");
					background-size: cover;
					background-repeat: repeat;
					background-attachment: fixed;
					background-position: center; 
				}
<? } ?>
        </style>
    </head>
    <body>
        <!-- Header Section -->
        <div id="header">

            <!-- Top Header -->
			<? if ($this->getPosition('position-30')->countBlocks()) { ?>
				<div id="pos-htop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12 col-md-12 col-sm-12 col-xs-12">
								<? $this->getPosition('position-30')->printContent(); ?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Middle Header -->

            <div id="pos-hmiddle" class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3  col-sm-3 col-xs-12">
							<? $this->getPosition('position-28')->printContent(); ?>
                        </div>

                        <div class="col-md-9 col-sm-9 col-xs-12" id="menu">
							<? $this->getPosition('position-29')->printContent(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Banner Header -->
			<? if ($this->getPosition('position-categories')->countBlocks()) { ?>
				<div id="pos-hbottom" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 h-bottom">
								<? $this->getPosition('position-categories')->printContent(); ?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Bottom Header -->
			<? if ($this->getPosition('position-15')->countBlocks()) { ?>
				<div id="pos-hbottom" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 h-bottom">
								<? $this->getPosition('position-15')->printContent(); ?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>


        </div>










