<? include 'header.php'; ?>


<!-- Body Section -->
<div id="content" class="">

    <!-- Top Body -->

    <div id="pos-ctop" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 position-12">
					<? $this->getPosition('position-title')->printContent(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12 position-12">
					<? $this->getPosition('position-search')->printContent(); ?>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12 position-12">
					<?= $content ?>
                </div>
            </div>
        </div>
    </div>


    <!-- Bottom Body -->
	<? if ($this->getPosition('position-cbottom')->countBlocks()) { ?>
		<div id="pos-cbottom" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<? $this->getPosition('position-cbottom')->printContent(); ?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

</div>




<? include 'footer.php'; ?> 
