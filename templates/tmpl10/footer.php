<!-- Footer Section -->
<div id="footer" class="">

    <!-- Top Footer -->
	<? if ($this->getPosition('pos-ftop')->countBlocks()) { ?>
		<div id="pos-ftop" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-ftop')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Middle Footer -->
	<? if ($this->getPosition('pos-fmenu')->countBlocks() || $this->getPosition('pos-finfo')->countBlocks()) { ?>
		<div id="pos-fmiddle" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-8 f-menu">
						<?
						$this->getPosition('pos-fmenu')->printContent();
						?>
					</div>
					<div class="col-md-4 f-info">
						<?
						$this->getPosition('pos-finfo')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Bootom Footer -->

    <div id="pos-fbottom" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-12 copyright">
					<?
					$this->getPosition('pos-fbottom')->printContent();
					?>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12 logo">
                    <span class="poweredby">
                        <a href="<?= BRAND_URL ?>" target="_blank">
                            <img src="<?= BRAND_LOGO ?>" alt=""/>
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

</body>    
</html>
