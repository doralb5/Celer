<!DOCTYPE html>
<html lang="it">
    <head>
        <!-- New Template on tmpl10 -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? HeadHTML::printFavicon(BRAND_FAVICON); ?>

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHtml::linkStylesheet("owl.carousel.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHtml::linkStylesheet("animate.css"); ?>
		<? HeadHtml::linkStylesheet("menu.css"); ?>
		<? HeadHtml::linkStylesheet('style.css'); ?>

		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js") ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("owl.carousel.js"); ?>
		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHtml::linkJS("wow.min.js"); ?>
		<? HeadHtml::linkJS("smoothscroll.js"); ?>
		<? HeadHtml::linkJS("script.js"); ?>
		<? HeadHTML::printJSLinks(); ?>
        <style>
            body {
                background-image: url("<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>");
                background-size: cover;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center; 
            }
        </style>

        <script>
            wow = new WOW(
                    {
                        boxClass: 'wow', // default
                        animateClass: 'animated', // default
                        offset: 30, // default
                        mobile: true, // default
                        live: true        // default
                    }
            )
            wow.init();


        </script>
    </head>

    <body>

        <!-- Header Section -->

        <div id="header">

            <!-- Top Header -->
			<? if ($this->getPosition('pos-htop')->countBlocks()) { ?>
				<div id="pos-htop" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-htop')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Middle Header -->
			<? if ($this->getPosition('pos-logo')->countBlocks() || $this->getPosition('pos-search')->countBlocks() || $this->getPosition('pos-cart')->countBlocks()) { ?>
				<div id="pos-hmiddle" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-3">
								<?
								$this->getPosition('pos-logo')->printContent();
								?>
							</div>
							<div class="col-md-6">
								<?
								$this->getPosition('pos-search')->printContent();
								?>
							</div>
							<div class="col-md-3">
								<?
								$this->getPosition('pos-cart')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

            <!-- Bottom Header -->
			<? if ($this->getPosition('pos-hbottom')->countBlocks()) { ?>
				<div id="pos-hbottom" class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-hbottom')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>

        </div>