<?php include 'header.php'; ?>

<!-- main content-->
<div id="main-content">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!--                        BREADCRUMB POSITION-->
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

				<? if (WebPage::showContentHeading() && WebPage::getContentHeading() != '') { ?>
					<div id="#content-heading" class="content-heading">
						<h2 class="head-title">
							<?= WebPage::getContentHeading(); ?>
						</h2>
						<hr class="botm-line" />
					</div>
				<? } ?>

                <!-- ### pos-ctop ### -->
				<? $this->getPosition('pos-ctop')->printContent(); ?>
                <!-- ### //pos-ctop ### -->

            </div>
        </div>
    </div>

    <section>
        <!-- ### pos-ctop-full ### -->
		<? $this->getPosition('pos-ctop-full')->printContent(); ?>
        <!-- ### //pos-ctop-full ### -->
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-12">
				<?
				$this->getPosition('pos-Lcols')->printContent();
				?>
            </div>
            <div class="col-md-8 col-xs-12">
				<?
				$this->getPosition('pos-Rcols')->printContent();
				?>

				<?= $content ?>

                <!-- ### pos-cmiddle ### -->
				<? $this->getPosition('pos-cmiddle')->printContent(); ?>
                <!-- ### //pos-cmiddle ### -->
            </div>

        </div>
    </div>

    <section>
        <div class="full-width">
            <!-- ### pos-cmiddle-full ### -->
			<? $this->getPosition('pos-cmiddle-full')->printContent(); ?>
            <!-- ### //pos-cmiddle-full ### -->
        </div>
    </section>
</div>
<div class="container">
    <div class="row">
        <!-- ### pos-cbottom ### -->
		<? $this->getPosition('pos-cbottom')->printContent(); ?>
        <!-- ### //pos-cbottom ### -->
    </div>
</div>
<section>
    <!-- ### pos-cbottom-full ### -->
	<? $this->getPosition('pos-cbottom-full')->printContent(); ?>
    <!-- ### //pos-cbottom-full ### -->
</section>
<?php include 'footer.php'; ?>
