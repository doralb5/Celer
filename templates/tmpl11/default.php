<?php include 'header.php'; ?>

<!-- main content-->
<div id="main-content">

    <div class="container">
        <div class="row">
            <div class="col-md-12">

				<? if (WebPage::showContentHeading() && WebPage::getContentHeading() != '') { ?>
					<div id="#content-heading" class="content-heading">
						<h2 class="head-title">
							<?= WebPage::getContentHeading(); ?>
						</h2>
						<hr class="botm-line" />
					</div>
				<? } ?>

				<? if ($this->getPosition('pos-ctop')->countBlocks() > 0) { ?>
					<!-- ### pos-ctop ### -->
					<? $this->getPosition('pos-ctop')->printContent(); ?>
					<!-- ### //pos-ctop ### -->
				<? } ?>

                <!-- ### content ### -->
				<?= $content; ?>
                <!-- ### //content ### -->
            </div>
        </div>
    </div>


	<? if ($this->getPosition('pos-ctop-full')->countBlocks() > 0) { ?>
		<section>
			<!-- ### pos-ctop-full ### -->
			<? $this->getPosition('pos-ctop-full')->printContent(); ?>
			<!-- ### //pos-ctop-full ### -->
		</section>
	<? } ?>

	<? if ($this->getPosition('pos-cmiddle')->countBlocks() > 0) { ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!-- ### pos-cmiddle ### -->
					<? $this->getPosition('pos-cmiddle')->printContent(); ?>
					<!-- ### //pos-cmiddle ### -->
				</div>
			</div>
		</div>
	<? } ?>

	<? if ($this->getPosition('pos-cmiddle-full')->countBlocks() > 0) { ?>
		<section>
			<div class="full-width">
				<!-- ### pos-cmiddle-full ### -->
				<? $this->getPosition('pos-cmiddle-full')->printContent(); ?>
				<!-- ### //pos-cmiddle-full ### -->
			</div>
		</section>
	<? } ?>

	<? if ($this->getPosition('pos-cbottom')->countBlocks() > 0) { ?>
		<div class="container">
			<!-- ### pos-cbottom ### -->
			<? $this->getPosition('pos-cbottom')->printContent(); ?>
			<!-- ### //pos-cbottom ### -->
		</div>
	<? } ?>
</div>

<? if ($this->getPosition('pos-cbottom-full')->countBlocks() > 0) { ?>
	<section>
		<div class="full-width">
			<!-- ### pos-cbottom-full ### -->
			<? $this->getPosition('pos-cbottom-full')->printContent(); ?>
			<!-- ### //pos-cbottom-full ### -->
		</div>
	</section>
<? } ?>
<?php include 'footer.php'; ?>      
