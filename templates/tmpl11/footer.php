<!--footer-->
<footer id="footer">
	<? if ($this->getPosition('top-footer')->countBlocks()) { ?>
		<div class="top-footer container-fluid">
			<? $this->getPosition('top-footer')->printContent() ?>
		</div>
	<? } ?>
    <div class="footer container">
		<? $this->getPosition('footer')->printContent() ?>
    </div>

</footer>
<div class="footer-line">
	<? $this->getPosition('footer-line')->printContent() ?>	
</div>
<!--/ footer-->

<? HeadHtml::linkJS("flatpickr.js"); ?>
<? //HeadHtml::linkJS("flatpickr/l10n/it.js"); ?>
<? HeadHtml::linkJS("jarallax.js"); ?>

<script type="text/javascript">
    /* init Jarallax */
    $('.homepage-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
    /* init Jarallax */
    $('.work-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
    /* init Jarallax */
    $('.advert-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
    /* init Jarallax */
    $('.partners-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
</script>
<div class="powered-by-container container">
    <div class="row">
        <div class="col-md-12 text-center space-bottom">
            <a href="<?= BRAND_URL ?>"><img src="<?= BRAND_LOGO ?>" /></a>
        </div>
    </div>
</div>
</body>
</html>
