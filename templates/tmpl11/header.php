<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? (CMSSettings::$favicon != '') ? HeadHTML::printFavicon(CMSSettings::$favicon) : ''; ?>



        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal") ?>
		<? HeadHTML::linkStylesheet("https://fonts.googleapis.com/css?family=Amatic%20SC") ?>
		<? HeadHTML::linkStylesheet("https://fonts.googleapis.com/css?family=Raleway:400,800") ?>
		<? HeadHTML::linkStylesheet("https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("flatpickr.min.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHtml::linkStylesheet("owl.carousel.min.css"); ?>
		<? HeadHtml::linkStylesheet("owl.theme.default.min.css"); ?>
		<? HeadHtml::linkStylesheet("custom.css"); ?>
		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->
		<? //HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js") ?>
		<? HeadHTML::linkJS("jquery-1.12.4.min.js") ?>
		<? HeadHTML::linkJS("https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js") ?>

		<? HeadHtml::linkJS("jquery.easing.min.js"); ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("owl.carousel.min.js"); ?>
		<? HeadHtml::linkJS("custom.js"); ?>

		<? HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHTML::printJSLinks(); ?>


<!--        <style type="text/css">
            #banner {
                background: url('<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>') no-repeat fixed;
                background-size: contain;
            }
        </style>-->


		<?= $this->getTemplate()->head_extra; ?>

    </head>
    <body>

		<? if ($this->getPosition('pos-nav-top-full')->countBlocks() > 0) { ?>
			<div id="pos-nav-top-full" class="container-fluid">
				<div class="row">
					<? $this->getPosition('pos-nav-top-full')->printContent(); ?>                  
				</div>
			</div>
		<? } ?>    
		<? if ($this->getPosition('pos-nav')->countBlocks() > 0) { ?>
			<div class="container">
				<div class="row">
					<div class="nav-costumization">
						<? $this->getPosition('pos-nav')->printContent(); ?>
					</div>
				</div>
			</div>
		<? } ?>

		<? if ($this->getPosition('pos-banner')->countBlocks() > 0 || $this->getPosition('pos-banner-info')->countBlocks() > 0) { ?>
			<!--banner-->
			<section id="banner" class="banner">
				<div class="bg-color">
					<? if ($this->getPosition('pos-banner')->countBlocks() > 0) { ?>
						<div class="banner-background">
							<? $this->getPosition('pos-banner')->printContent(); ?>
						</div>
					<? } ?>
					<? if ($this->getPosition('pos-banner-info')->countBlocks() > 0) { ?>
						<div class="container">
							<div class="row">
								<div class="banner-info">
									<? $this->getPosition('pos-banner-info')->printContent(); ?>
								</div>
							</div>
						</div>
					<? } ?>
				</div>
			</section>
			<!--/ banner-->
		<? } ?>