$(document).ready(function () {
    if ($(window).width() < 992) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    }

    $(window).scroll(function () {
        if ($(window).width() > 991) {
            if ($(".navbar-default").length) {
                if ($(".navbar-default").offset().top > 50) {
                    $(".navbar-fixed-top").addClass("top-nav-collapse");
                } else {
                    $(".navbar-fixed-top").removeClass("top-nav-collapse");
                }
            }
        }
    });
});
