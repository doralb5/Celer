<?
HeadHtml::AddJS("parallax.min.js");

$width_md = (isset($parameters['w'])) ? $parameters['w'] : 2000;
$height_md = intval( (isset($parameters['h'])) ? $parameters['h'] : intval($width_md / 3) );
$width_sm = intval($width_md / 4);
$height_sm = intval($height_md / 4);

$padding_em = (isset($parameters['padding_em'])) ? $parameters['padding_em'] : 17;
$padding_sm_em = (isset($parameters['padding_em'])) ? $parameters['padding_em'] : 6;

if(count($banners)) {
    $background_url_md = Utils::genThumbnailUrl('banners/' . $banners['0']->filename, $width_md, 0, array('q' => 95));
    $background_url_sm = Utils::genThumbnailUrl('banners/' . $banners['0']->filename, $width_sm, 0, array('q' => 95));
?>

    <div class="parallax-window-<?= $block_id ?>" data-parallax="scroll" data-image-src="<?=$background_url_md?>" style='height: 70vh;'></div>

<?
}
?>
