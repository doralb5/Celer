<? include 'header.php'; ?>

<!-- main content-->
<div id="main-content">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                BREADCRUMB POSITION
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<? $this->getPosition('pos-ctop')->printContent(); ?>
				<? if (WebPage::showContentHeading() && WebPage::getContentHeading() != '') { ?><div id="#content-heading" class="content-heading"><h2 class="head-title"><?= WebPage::getContentHeading(); ?></h2><hr class="botm-line"></hr></div><? } ?>
				<?= $content; ?>
            </div>
        </div>
    </div>


	<? $this->getPosition('pos-cmiddle')->printContent(); ?>


    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<? $this->getPosition('pos-section-article')->printContent(); ?>
            </div>
        </div>
    </div>

</div>
<!--/ main content-->


<? $this->getPosition('pos-cbottom')->printContent() ?>

<? include 'footer.php'; ?>        