<!--footer-->
<footer id="footer">
    <div class="top-footer">
		<? $this->getPosition('top-footer')->printContent() ?>
    </div>
    <div class="footer-line">
		<? $this->getPosition('footer-line')->printContent() ?>	
    </div>
</footer>
<!--/ footer-->

<? HeadHtml::linkJS("flatpickr.js"); ?>
<? HeadHtml::linkJS("jarallax.js"); ?>
<script>
    // flatpickr Calendar configuration
    flatpickr(".flatpickr");
</script>

<script type="text/javascript">
    /* init Jarallax */
    $('.homepage-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
    /* init Jarallax */
    $('.work-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
    /* init Jarallax */
    $('.advert-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
    /* init Jarallax */
    $('.partners-jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
</script>

</body>
</html>
