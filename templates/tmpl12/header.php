<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? //HeadHTML::printFavicon(); ?>

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal") ?>
		<? HeadHtml::linkStylesheet("font-awesome.min.css"); ?>
		<? HeadHTML::linkStylesheet("bootstrap.min.css") ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>
		<? HeadHtml::linkStylesheet("flatpickr.min.css"); ?>
		<? HeadHtml::linkStylesheet("lightbox.css"); ?>
		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js") ?>
		<? HeadHTML::linkJS("https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js") ?>

		<? HeadHtml::linkJS("jquery.easing.min.js"); ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("custom.js"); ?>

		<? //HeadHtml::linkJS("lightbox-2.6.min.js"); ?>
		<? HeadHTML::printJSLinks(); ?>


<!--        <style type="text/css">
            #banner {
                background: url('<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>') no-repeat fixed;
                background-size: contain;
            }
        </style>-->
    </head>
    <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
        <div class="container">
            <div class="row">
                <div class="nav-costumization">
					<? $this->getPosition('pos-nav')->printContent(); ?>
                </div>
            </div>
        </div>
        <!--banner-->
        <section id="banner" class="banner">
            <div class="bg-color">

                <div class="banner-background">
					<? $this->getPosition('pos-banner')->printContent(); ?>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="banner-info">
							<? $this->getPosition('pos-banner-info')->printContent(); ?>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--/ banner-->