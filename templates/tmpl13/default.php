<? include 'header.php'; ?>
<!-- Body Section -->
<div id="content">
    <!--banner-->
    <section id="banner" class="banner">
        <div class="bg-color">

            <div class="banner-background">
				<? $this->getPosition('pos-banner')->printContent(); ?>
            </div>
            <div class="container">
                <div class="row">
                    <div class="banner-info">
						<? $this->getPosition('pos-banner-info')->printContent(); ?>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--/ banner-->
    <!-- Top Body -->
	<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
		<section class="spacer green">
			<div id="pos-ctop" class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('pos-ctop')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		</section>
	<? } ?>

    <!-- Middle Body -->

    <div id="pos-cmiddle" class="container-fluid">
        <div class="container cmiddle">
            <div class="row">
                <div class="col-md-12">
                    <div class="body-container">
						<?
						$this->getPosition('pos-cmiddle')->printContent();
						?>
						<?= $content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Bottom Body -->
	<? if ($this->getPosition('pos-cbottom')->countBlocks()) { ?>
		<section id="services" class="section orange">
			<div id="pos-cbottom" class="container-fluid">
				<div class="darker-overlay">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?
								$this->getPosition('pos-cbottom')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<? } ?>

    <!-- Position Section 1 -->
	<? if ($this->getPosition('pos-section-1')->countBlocks()) { ?>
		<div id="pos-section-1" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section-1')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>
    <!-- Position Section 2 -->
	<? if ($this->getPosition('pos-section-2')->countBlocks()) { ?>
		<div id="pos-section-2">
			<div class="row">
				<div class="col-md-12">
					<?
					$this->getPosition('pos-section-2')->printContent();
					?>
				</div>
			</div>
		</div>
	<? } ?>
    <!-- Position Section 3 -->
	<? if ($this->getPosition('pos-section-3')->countBlocks()) { ?>
		<div id="pos-section-3">
			<div class="row">
				<div class="col-md-12">
					<?
					$this->getPosition('pos-section-3')->printContent();
					?>
				</div>
			</div>
		</div>
	<? } ?>
    <!-- Position Section 4 -->
	<? if ($this->getPosition('pos-section-4')->countBlocks()) { ?>
		<section id="contact" class="section green">
			<div id="pos-section-4" class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('pos-section-4')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		</section>
	<? } ?>

</div>


<? include 'footer.php'; ?>        