<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? //HeadHTML::printFavicon(); ?>

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("bootstrap.css") ?>
		<? HeadHtml::linkStylesheet("default_color.css"); ?>
		<? HeadHtml::linkStylesheet("font-awesome-ie7.css"); ?>
		<? HeadHtml::linkStylesheet("font-awesome.css"); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>






		<? HeadHTML::printStylesheetLinks(); ?>

        <!-- Javascript -->
		<? HeadHTML::linkJS("https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js") ?>

		<? HeadHtml::linkJS("jquery.scrollTo.js"); ?>
		<? HeadHtml::linkJS("jquery.nav.js"); ?>
		<? HeadHtml::linkJS("jquery.localscroll-1.2.7-min.js"); ?>
		<? HeadHtml::linkJS("bootstrap.min.js"); ?>
		<? HeadHtml::linkJS("jquery.prettyPhoto.js"); ?>
		<? HeadHtml::linkJS("isotope.js"); ?>
		<? HeadHtml::linkJS("jquery.flexslider.js"); ?>
		<? HeadHtml::linkJS("inview.js"); ?>
		<? HeadHtml::linkJS("animate.js"); ?>
		<? HeadHtml::linkJS("validate.js"); ?>
		<? HeadHtml::linkJS("custom.js"); ?>

		<? HeadHTML::printJSLinks(); ?>

    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="nav-costumization">
					<? $this->getPosition('pos-nav')->printContent(); ?>
                </div>
            </div>
        </div>


        <!-- navbar -->
        <!--        <div class="navbar-wrapper">
                    <div class="navbar navbar-inverse navbar-fixed-top">
                        <div class="navbar-inner">
                            <div class="container">
								
                                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><col-md- class="icon-bar"></col-md-><col-md- class="icon-bar"></col-md-><col-md- class="icon-bar"></col-md->
                                </a>
                                <h1 class="brand"><a href="index.html">Maxim</a></h1>
								
								
                                <nav class="pull-right nav-collapse collapse">
                                    <ul id="menu-main" class="nav">
                                        <li><a title="team" href="#about">About</a></li>
                                        <li><a title="services" href="#services">Services</a></li>
                                        <li><a title="works" href="#works">Works</a></li>
                                        <li><a title="blog" href="#blog">Blog</a></li>
                                        <li><a title="contact" href="#contact">Contact</a></li>
                                        <li><a href="page.html">Page</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>-->