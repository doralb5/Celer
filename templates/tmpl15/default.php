<?php include dirname(__FILE__) . "/header.php"; ?>

<!-- Body Section -->
<div id="content" class="conainer-fluid">

    <!-- Banner On Body -->
	<? if ($this->getPosition('pos-banner')->countBlocks()) { ?>
		<div id="pos-banner" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-banner')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Top Body -->
	<? if ($this->getPosition('pos-ctop')->countBlocks()) { ?>
		<div id="pos-ctop" class="container-fluid">
			<?
			$this->getPosition('pos-ctop')->printContent();
			?>
		</div>
	<? } ?>

    <!-- Map Section -->
	<? if ($this->getPosition('pos-map')->countBlocks()) { ?>
		<div id="pos-map" class="wrapper">
			<?
			$this->getPosition('pos-map')->printContent();
			?>
			<div class="container-background-bottom container-background-footer hidden-xs hidden-sm"></div>
		</div>
	<? } ?>

    <!-- Middle Body -->
	<? if ($content != '') { ?>
		<div id="pos-cmiddle" class="container-fluid">
			<div class="container">
				<div class="cmiddle">
					<div class="row">
						<div class="col-md-12">
							<div class="body-container">

								<? if (WebPage::$breadcrumb != NULL) WebPage::$breadcrumb->render(); ?>
								<?= $content ?>

								<?
								$this->getPosition('pos-cmiddle')->printContent();
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section-0 -->
	<? if ($this->getPosition('pos-section0')->countBlocks()) { ?>
		<div id="pos-section0" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section0')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 1 -->
	<? if ($this->getPosition('pos-section-featured-businesses')->countBlocks() || $this->getPosition('pos-section-last-announces')->countBlocks()) { ?>
		<div id="pos-section1" class="container-fluid">
			<div class="container">
				<div class="row">

					<? if ($this->getPosition('pos-section-last-announces')->printContent(false) != '') { ?>

						<div class="col-md-8 last-businesses">
							<?
							$this->getPosition('pos-section-featured-businesses')->printContent();
							?>
						</div>
						<div class="col-md-4">
							<?
							$this->getPosition('pos-section-last-announces')->printContent();
							?>
						</div>
					<? } else { ?>
						<div class="col-md-12 last-businesses">
							<?
							$this->getPosition('pos-section-featured-businesses')->printContent();
							?>
						</div>
					<? } ?>

				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section Offers -->
	<? if ($this->getPosition('pos-last-offers')->countBlocks()) { ?>
		<div id="pos-last-offers" class="container-fluid">
			<div class="">
				<?
				$this->getPosition('pos-last-offers')->printContent();
				?>
			</div>
		</div>
	<? } ?>

    <!-- Section 2 -->
	<? if ($this->getPosition('pos-section2')->countBlocks()) { ?>
		<div id="pos-section2" class="">
			<div class="dark-overlay">
				<?
				$this->getPosition('pos-section2')->printContent();
				?>
			</div>
		</div>
	<? } ?>


    <!-- Section 3 -->
	<? if ($this->getPosition('pos-section3')->countBlocks()) { ?>
		<div id="pos-section3" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section3')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 3 -->
	<? if ($this->getPosition('pos-announces')->countBlocks()) { ?>
		<div id="pos-announces" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-announces')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section portals -->
	<? if ($this->getPosition('portals')->countBlocks()) { ?>
		<div id="portals" class="">
			<?
			$this->getPosition('portals')->printContent();
			?>
		</div>
	<? } ?>


    <!-- Section Horoscope -->
	<? if ($this->getPosition('pos-horoscope')->countBlocks()) { ?>
		<div id="pos-horoscope" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-horoscope')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 4 -->
	<? if ($this->getPosition('pos-section4')->countBlocks()) { ?>
		<div id="pos-section4" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section4')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

    <!-- Section 5 -->
	<? if ($this->getPosition('pos-section5')->countBlocks()) { ?>
		<div id="pos-section5" class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?
						$this->getPosition('pos-section5')->printContent();
						?>
					</div>
				</div>
			</div>
		</div>
	<? } ?>


	<? if ($this->getPosition('pos-section6')->countBlocks()) { ?>
		<div id="pos-section6" class="">

			<?
			$this->getPosition('pos-section6')->printContent();
			?>

		</div>
	<? } ?>
	<? if ($_SERVER['HTTP_HOST'] == "netirane.al") { ?>
		<div>
			<style>

				.kontaktoni {
					box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
					top: 360px !important;
					left: 1317px !important;
					position: fixed;
					line-height: 35px;
					border-radius: 3px;
				}
				.fb-message-blue {
					cursor:pointer;
					width: 42px;
					height: 42px;
					position: fixed;
					border-radius: 50px;
					background: #03A9F4;
					display: inline-block;
					white-space: nowrap;
					padding: 13px;
					box-sizing: border-box;
					color: white;
					transition: all 0.2s ease;
					font-family: Roboto, sans-serif;
					text-decoration: none;
					box-shadow: 0 5px 6px 0 rgba(0,0,0,0.2);

				}
				.fb-message-blue i {
					margin-right: 20px;
					transition: margin-right 0.2s ease;
					background-image: url('https://i.imgur.com/hjwozo7.png');
					background-repeat: no-repeat;
					background-size: 17px;
					background-position: 1px 1px;
					top: -1px;
					height: 26px;
					width: 28px;
					padding: 10px 7px;
					float: left;
				}
				.fb-message-blue:hover{
					background-color: #2093ff;
					border: 1px solid #2093ff;
					-webkit-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.2);
					-moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.2);
					box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.2);
				}

			</style>
			<!-- Your send button code -->
			<?
			$useragent = $_SERVER['HTTP_USER_AGENT'];
			if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
				echo "<a href='fb-messenger-public://user-thread/243716722677628' rel='kontakt' class='fb-message-blue' style='bottom: 10px; right: 30px;'><i></i></a>";
			} else {
				?>
				<a
					rel="kontakt"
					class="fb-message-blue" 
					style="bottom: 10px; right: 30px;"
					>
					<i></i>
				</a>
			<? } ?>
			<script>
				$("[rel=kontakt]").popover({
					trigger: 'click',
					placement: 'top',
					html: 'true',
					content: '<textarea class="popover-textarea"></textarea>',
					template: '<iframe class="kontaktoni" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnetiraneal%2F&tabs=messages&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>'
				})
						.on('click', function () {
							//hide any visible comment-popover
							$("[rel=kontakt]").not(this).popover('hide');
							var $this = $(this);
							//close on cancel
							$('.popover-cancel').click(function () {
								$this.popover('hide');
							});

						});
			</script>
		</div>
	<? } ?>
    <!-- Bottom Body -->
	<? if ($this->getPosition('pos-cbottom')->countBlocks()) { ?>
		<div id="pos-cbottom" class="contanier-fluid">
			<div id="fh5co-testimonial" style="background-color:#36a3b9;">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?
							$this->getPosition('pos-cbottom')->printContent();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<? } ?>

</div>

<? $this->getPosition('pos-footer-bg')->printContent(); ?>

<?php include dirname(__FILE__) . "/footer.php"; ?>
