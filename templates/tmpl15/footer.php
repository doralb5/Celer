<footer>
    <div class="top-footer">
		<? $this->getPosition('top-footer')->printContent() ?>
    </div>  
    <!-- ./container -->
</footer>
<a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>

<? HeadHTML::linkJS('jquery.min.js'); ?>
<? HeadHTML::linkJS('jquery.easing.1.3.js'); ?>
<? HeadHTML::linkJS('bootstrap.min.js'); ?>
<? HeadHTML::linkJS('jquery.waypoints.min.js'); ?>
<? HeadHTML::linkJS('sticky.js'); ?>
<? HeadHTML::linkJS('jquery.stellar.min.js'); ?>
<? HeadHTML::linkJS('hoverIntent.js'); ?>
<? HeadHTML::linkJS('jquery.magnific-popup.min.js'); ?>
<? HeadHTML::linkJS('magnific-popup-options.js'); ?>
<? HeadHTML::linkJS('bootstrap-datepicker.min.js'); ?>
<? HeadHTML::linkJS('classie.js'); ?>
<? HeadHTML::linkJS('selectFx.js'); ?>
<? HeadHTML::linkJS('main.js'); ?>

</body>
</html>