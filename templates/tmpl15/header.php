<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? HeadHTML::addMeta('viewport', 'width=device-width, initial-scale=1'); ?>
		<? HeadHTML::addMeta('generator', 'BlueHat sh.p.k.'); ?>
		<? HeadHTML::printMeta(); ?>
		<? HeadHTML::printTitle(); ?>
		<? //HeadHTML::printFavicon(); ?>

        <!-- Stylesheet -->
		<? HeadHTML::linkStylesheet("https://fonts.googleapis.com/css?family=Open+Sans:400,700,300") ?>
		<? HeadHTML::linkStylesheet('animate.css'); ?>
		<? HeadHTML::linkStylesheet('icomoon.css'); ?>
		<? HeadHTML::linkStylesheet("bootstrap.css") ?>
		<? HeadHTML::linkStylesheet('superfish.css'); ?>
		<? HeadHTML::linkStylesheet('magnific-popup.css'); ?>
		<? HeadHTML::linkStylesheet('bootstrap-datepicker.min.css'); ?>
		<? HeadHTML::linkStylesheet('cs-select.css'); ?>
		<? HeadHTML::linkStylesheet('cs-skin-border.css'); ?>
		<? HeadHtml::linkStylesheet("style.css"); ?>

		<? HeadHTML::printStylesheetLinks(); ?>

		<? HeadHtml::linkJS("modernizr-2.6.2.min.js"); ?>
		<? HeadHTML::printJSLinks(); ?>


      <!-- <style type="text/css">
            #banner {
                background: url('<?= $this->template_path . 'images/' . $this->template_params['background_image'] ?>') no-repeat fixed;
                background-size: contain;
            }
        </style>-->
    </head>
    <body>
        <div id="fh5co-wrapper">
            <div id="fh5co-page">

                <header id="fh5co-header-section" class="sticky-banner">
                    <div class="container-fluid">
                        <div class="nav-header">
							<? $this->getPosition('pos-nav')->printContent(); ?>
                        </div>
                    </div>
                </header>


                <!-- navbar -->
                <!--        <div class="navbar-wrapper">
                            <div class="navbar navbar-inverse navbar-fixed-top">
                                <div class="navbar-inner">
                                    <div class="container">
										
                                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><col-md- class="icon-bar"></col-md-><col-md- class="icon-bar"></col-md-><col-md- class="icon-bar"></col-md->
                                        </a>
                                        <h1 class="brand"><a href="index.html">Maxim</a></h1>
										
										
                                        <nav class="pull-right nav-collapse collapse">
                                            <ul id="menu-main" class="nav">
                                                <li><a title="team" href="#about">About</a></li>
                                                <li><a title="services" href="#services">Services</a></li>
                                                <li><a title="works" href="#works">Works</a></li>
                                                <li><a title="blog" href="#blog">Blog</a></li>
                                                <li><a title="contact" href="#contact">Contact</a></li>
                                                <li><a href="page.html">Page</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>-->