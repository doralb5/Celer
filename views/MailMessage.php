<?php

class MailMessage extends BaseView {

	private $from;
	private $from_name;

	function __construct() {
		parent::__construct();
		$this->setLang(FCRequest::getLang());
		$this->setFrom(CMSSettings::$sender_mail);
		$this->setFrom_name(CMSSettings::$webdomain);
		$this->placeholder('SITENAME')->setVal(CMSSettings::$website_title);
		$this->placeholder('PHONE')->setVal(CMSSettings::$company_tel);
		$this->placeholder('URL_WEBSITE')->setVal(str_replace('http://', '', rtrim(CMSSettings::$webdomain)));
		$this->placeholder('URL_LOGO')->setVal(Utils::genThumbnailUrl(CMSSettings::$logo, 150, 0, array(), true));
	}

	function setFrom($from) {
		$this->from = strtolower($from);
	}

	function setFrom_name($from_name) {
		$this->from_name = $from_name;
	}

	public function send($to, $subject, $mailview) {
		$text = $this->render($mailview, true);
		if (Email::sendMail($to, $this->from_name, $this->from, $subject, $text)) {
			return true;
		} else {
			return false;
		}
	}

}
