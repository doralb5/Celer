
<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Maintenance Mode</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
              crossorigin="anonymous">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
              crossorigin="anonymous">

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

        <style>

            body {
                color: #999;
                /* padding: 0em 0 12em 0px; */
                background-color: #0060a9;
                /* background-image: url(../bh-logo.jpg); */
                -webkit-background-size: cover !important;
                -moz-background-size: cover !important;
                -ms-background-size: cover !important;
                -o-background-size: cover !important;
                background-size: contain;
                background-repeat: no-repeat;
                text-align: center;
                width: 100%;
            }
            #header-menu {
                background-color: #FFFFFF;
                margin-bottom: 0px;
                z-index: 2;
                box-shadow: 0 2px 5px 0 rgba(0,0,0,.1);
                -moz-box-shadow: 0 2px 5px 0 rgba(0,0,0,.1);
                -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.3);
                position: relative;
                border-top: 2px solid #1094f6;
            }
            #header-menu .logo {
                padding: 5px 0;
                width: 100%;
            }
            #header-menu .img_logo {
                max-width: 75px;
                width: 100%;
            }
            .warning-content .picture img{
                max-width: 100%;
            }

            @media (max-width: 500px){
                .warning-content .picture img{
                    width: 100%;
                }
                p.last a {
                    font-size: 12px;
                }
                .warning-content {
                    top: 4vh !important;
                }
            }


            h1 {
                font-weight: 100;
                font-size: 20pt;
                color: #555;
                margin-bottom: .6em;
                margin-top: 0.5em;
            }

            p {
                font-weight: lighter;
                font-size: 10pt;
                margin: 0;
                color: #555;
            }

            .warning-content {
                position: relative;
                /* top: 5vh; */
                text-align: center;
                height: 100%;
                width: 100%;
                font-family: Helvetica;
                color: #fff;
                background: rgba(0, 0, 0, 0.25);
                padding: 0em;
                border-radius: 2px;
                box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
                transition: .5s;
                margin: 5em 0;
            }

            .warning-content:hover, .warning-content:focus {
                box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
            }

            .warning-content .picture {
                margin: 0 auto;
                padding: 6em 6em;
                display: table;
            }
            .warning-content .content {
                background: white;
                padding: 25px;
            }


            svg {
                margin: 20px 0;
            }

            #overlay {
                /* background: url(/data/netirane.al/media/overlay.png) repeat; */
                height: 100vh;
                display: block;
                opacity: 1;
                top: 0;
                z-index: 0;
                position: absolute;
                width: 100%;
            }

            p.last {
                border-top: 1px solid #e8e8e8;
                margin-top: 2em;
            }


            /********/


            .btn:after {
                content: '';
                position: absolute;
                border-radius: 50%;
                height: 5em;
                width: 5em;
                top: -1.3em;
                left: 50%;
                margin-left: -2.5em;
                -webkit-box-shadow: inset 0 0 0 5em rgba(255, 255, 255, 0.5);
                -moz-box-shadow: inset 0 0 0 5em rgba(255, 255, 255, 0.5);
                box-shadow: inset 0 0 0 5em rgba(255, 255, 255, 0.5);
                opacity: 0;
                -webkit-transform: scale(0.2);
                -moz-transform: scale(0.2);
                -ms-transform: scale(0.2);
                -o-transform: scale(0.2);
                transform: scale(0.2);
                -webkit-transition: all 150ms cubic-bezier(0.25, 0.1, 0.25, 0.1);
                -moz-transition: all 150ms cubic-bezier(0.25, 0.1, 0.25, 0.1);
                -o-transition: all 150ms cubic-bezier(0.25, 0.1, 0.25, 0.1);
                transition: all 150ms cubic-bezier(0.25, 0.1, 0.25, 0.1);
            }
            .btn:focus:after {
                -webkit-transform: scale(2);
                -moz-transform: scale(2);
                -ms-transform: scale(2);
                -o-transform: scale(2);
                transform: scale(2);
                opacity: 1;
            }
            .keyframe:focus:after {
                -webkit-animation: ripple 300ms linear forwards;
                -moz-animation: ripple 300ms linear forwards;
                -o-animation: ripple 300ms linear forwards;
                animation: ripple 300ms linear forwards;
            }
            lesshat-selector {
                -lh-property: 0; } 
            @-webkit-keyframes ripple{ 0%{ -webkit-transform: scale(0.2); opacity: 0; } 50% { opacity: 1; } 100%{ -webkit-transform: scale(2); opacity: 0; }}
            @-moz-keyframes ripple{ 0%{ -moz-transform: scale(0.2); opacity: 0; } 50% { opacity: 1; } 100%{ -moz-transform: scale(2); opacity: 0; }}
            @-o-keyframes ripple{ 0%{ -o-transform: scale(0.2); opacity: 0; } 50% { opacity: 1; } 100%{ -o-transform: scale(2); opacity: 0; }}
            @keyframes ripple{ 0%{-webkit-transform: scale(0.2);-moz-transform: scale(0.2);-ms-transform: scale(0.2);transform: scale(0.2); opacity: 0; } 50% { opacity: 1; } 100%{-webkit-transform: scale(2);-moz-transform: scale(2);-ms-transform: scale(2);transform: scale(2); opacity: 0; };
            }

            .btn {
                position: relative;
                padding: 15px 25px;
                border: none;
                color: white;
                -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
                -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
                -webkit-border-radius: 2px;
                -webkit-background-clip: padding-box;
                -moz-border-radius: 2px;
                -moz-background-clip: padding;
                border-radius: 2px;
                background-clip: padding-box;
                margin-top: 25px;
                overflow: hidden;
                -webkit-transition: all 300ms cubic-bezier(0.25, 0.1, 0.25, 0.1);
                -moz-transition: all 300ms cubic-bezier(0.25, 0.1, 0.25, 0.1);
                -o-transition: all 300ms cubic-bezier(0.25, 0.1, 0.25, 0.1);
                transition: all 300ms cubic-bezier(0.25, 0.1, 0.25, 0.1);
            }
            .btn:hover {
                -webkit-box-shadow: 0 2px 8px rgba(0, 0, 0, 0.4);
                -moz-box-shadow: 0 2px 8px rgba(0, 0, 0, 0.4);
                box-shadow: 0 2px 8px rgba(0, 0, 0, 0.4);
                color: white;
            }
            .btn:active {
                -webkit-transform: scale(0.98);
                -moz-transform: scale(0.98);
                -ms-transform: scale(0.98);
                -o-transform: scale(0.98);
                transform: scale(0.98);
            }
            .btn.focus, .btn:focus, .btn:hover {
                color: #fff;

            }
            .btn-green {
                background-color: #19a69a;
            }
            .btn-green:hover {
                background-color: #1cbcaf;
            }
            .btn-blue {
                background-color: #1094f6;
            }
            .btn-blue:hover {
                background-color: #299ff7;
            }
            .btn-pink {
                background-color: #ff504d;
            }
            .btn-pink:hover {
                background-color: #ff6967;
            }

            .info {
                padding: 50px 20px;
                width: 80%;
                margin: 0 auto;
                color: #655F5F;
            }
            *:focus {
                outline: 0;
            }

            .footer2 {
                padding: 1.5em 0;
                background-color: #0e0e0e !important;
                color: #9A9A9A !important;
                position: relative;
                bottom: 0;
                display: block;
                width: 100%;
            }




        </style>

        <script>
            window.console = window.console || function (t) {
            };
        </script>
    </head>

    <body translate="no">

        <!-- Overlay -->
        <div id="overlay">
            <span></span>
        </div>

        <!-- Header -->
        <div id="header-menu" class="wrapper">
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="logo"> 
                                <a id="logo_91" href="http://bluehat.al/it/home">
                                    <img class="img_logo" src="http://bluehat.al/data/bluehat.al/media/1462549099bh%20logo.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Content -->
        <div id="page" class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="warning-content">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="picture">
										<? if (CMSSettings::$logo != '' && file_exists(DOCROOT . MEDIA_ROOT . CMSSettings::$logo)) { ?>
											<img class="img-responsive" src="<?= WEBROOT . MEDIA_ROOT . CMSSettings::$logo ?>">
										<? } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="content">
                                        <h1>Maintenance Mode</h1>


                                        <p>
                                            Please forgive the inconvenience. <br>
                                            We are currently initializing our brand new site.
                                        </p>
                                        <p class="last">
                                            <a class="btn btn-lg btn-blue keyframe" href="/admin">Access the management panel</a>

                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Copyright -->
        <!--        <footer class="container-fluid text-center footer2">
                    <div>
                        <p class="wow zoomIn animated" data-wow-delay="1s">Powered by <a  target="_blank" href="http://bluehat.al">Bluehat Sh.p.k.</a> - © <?= date("Y") ?></p>
                    </div>
                </footer>-->

        <script>
            if (document.location.search.match(/type=embed/gi)) {
                window.parent.postMessage("resize", "*");
            }
        </script>

    </body>
</html>