<? HeadHTML::addStylesheet(WEBROOT . $this->view_path . 'css/lastnews.css'); ?>

<? require_once LIBS_PATH . 'StringUtils.php'; ?>

<? $cols = (isset($cols) ? $cols : 4) ?>

<style>
    @media (min-width: 767px) {
        .lastnews .thumbnail {
        }
    }

    .lastnews .thumbnail {
        padding: 0px;
        margin-bottom: 0px;
        border: 0px solid white;
        border-radius: 0px;
    }

    .lastnews #newsCarousel {
        margin: 0 30px 0 30px;
        padding: 10px;
    }

    ul.thumbnails li {
        margin-bottom: 0px;
    }

    @media (max-width: 768px) {
        .lastnews ul.thumbnails {
            padding-left: 0px;
        }

        .lastnews #newsCarousel {
            margin-right: 0px;
        }

        .lastnews .well {
            padding: 0px;
        }
    }

    .carousel-control {
        background: none !important;
        color: #CACACA;
        font-size: 2.3em;
        text-shadow: none;
        margin-top: 0px;
        width: 5%;
    }

    .carousel-control.left i {
        margin-left: -30px;
    }

    .carousel-control.right i {
        margin-right: -30px;
    }

    @media (max-width: 768px) {
        .carousel-control.left i, .carousel-control.right i {
            display: none;
        }
    }

    ul.pager {
        display: none;
    }

    ul {
        list-style-type: none;
        -webkit-padding-start: 0px;
    }

    .button_align {
        text-align: center;
    }

    .fff {
        margin: 30px 0;
    }

    #newsCarousel {
        padding: 0 30px;
    }

    .lastnews .fff {
        transition: .8s;
        background: white;
        box-shadow: 0 6px 20px rgba(0,0,0,0.2);
    }

    .lastnews .fff .caption {
        padding: 0 10px 10px 10px;
    }
    .lastnews .fff:hover {
        moz-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.2);
        -webkit-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.2);
        box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.2);
        /* transform: perspective(400px); */
    }

    .carousel-control:hover { color: #4a4a4a; }

    .lastnews p.news-content {
        min-height: 40px;
    }

</style>


<div class="carousel slide" id="newsCarousel">
    <div class="carousel-inner">

		<?
		$i = 0;
		foreach ($items as $item) {
			?>
			<? if ($i == 0) { ?>
				<div class="item active">
					<ul class="thumbnails">
						<div class="row">
						<? } ?>
						<? if ($i % $cols == 0 && $i != 0) { ?>
						</div>
					</ul>
				</div>
				<div class="item">
					<ul class="thumbnails">
						<div class="row">
						<? } ?>

						<li class="col-md-<?= $col_size ?> wow fadeInUp animated" data-wow-delay=".5s">


							<? echo $item ?>


						</li>
						<?
						$i++;
					}
					?>
                </div>
            </ul>
        </div>
    </div>

	<? if (count($items) > $cols) { ?>
		<nav>
			<ul class="newscontrol-box ">
				<li><a data-slide="prev" href="#newsCarousel" class="left carousel-control"><i
							class="glyphicon glyphicon-chevron-left"></i></a></li>
				<li><a data-slide="next" href="#newsCarousel" class="right carousel-control"><i
							class="glyphicon glyphicon-chevron-right"></i></a></li>
			</ul>
		</nav>
	<? } ?>
</div>


<script>
    $('#newsCarousel').carousel({
        interval: 10000
    })
</script>
