<?php

define('DEVELOPMENT_ENVIRONMENT', false);
define('DEVELOPMENT_MAIL', 'developers@bluehat.al');
define('SHOW_ERRORS', false);

define('MAIN_DB_TYPE', 'mysql');
define('MAIN_DB_HOST', 'localhost');
define('MAIN_DB_NAME', 'unixtech_websites');
define('MAIN_DB_USER', 'unixtech_cms');
define('MAIN_DB_PASS', 'cmsdorijona1.');

if(!defined('WEBROOT')) define('WEBROOT', '/');

$wwwdomain = $_SERVER['HTTP_HOST'];
$domain = (strpos($wwwdomain, 'www.') === 0) ? substr($wwwdomain, 4) : $wwwdomain;


$db = mysqli_connect(MAIN_DB_HOST, MAIN_DB_USER, MAIN_DB_PASS);
mysqli_select_db(MAIN_DB_NAME, $db) or die('Aggiornamento di sicurezza dei sistemi, riprova tra qualche minuto.');

$site = getSiteData($domain);

if($site === false) {
    
    include 'check_subdomains.php';
    $site = getSiteData($domain);
    
    if($site === false) {
        echo "Domain not valid!";
        exit;
    }

}
    if (strtotime($site['expire_date']) < strtotime('now') && $site['expire_date'] != '0000-00-00') {
        echo "SERVIZIO SCADUTO!";
        exit;
    } elseif (!$site['enabled']) {
        echo "SERVIZIO SOSPESO";
        exit;
    } else {
        define('DOMAIN', $site['domain']);

        define('DB_TYPE', 'mysql');
        define('DB_HOST', $site['db_host']);
        define('DB_NAME', $site['db_name']);
        define('DB_USER', $site['db_user']);
        define('DB_PASS', $site['db_pass']);
        define('MEDIA_ROOT', rtrim($site['data_dir'], DS) . DS . 'media' . DS);
        define('CACHE_PATH', MEDIA_ROOT . DS . 'thumbs' . DS);
        define('DATA_DIR', rtrim($site['data_dir'], DS) . DS);

        switch ($site['id_brand']) {
            case 1 :
                define('BRAND_LOGO', WEBROOT . 'commons/images/poweredby/WeWeb-PoweredBy.png');
                define('BRAND_URL', "//www.weweb.al");
                define('BRAND_FAVICON', WEBROOT . "commons/images/weweb.ico");
                define('BRAND_POWERED', WEBROOT . "commons/images/poweredby/WeWeb-PoweredBy.png");
                define('BRAND_POWERED_INVERSE', WEBROOT . "commons/images/poweredby/WeWeb-PoweredBy.png");
                break;
            case 3 :
                define('BRAND_LOGO', WEBROOT . 'commons/images/poweredby/poweredbybh.png');
                define('BRAND_URL', "//www.bluehat.al");
                define('BRAND_FAVICON', WEBROOT . "commons/images/bluehat.ico");
                define('BRAND_POWERED', WEBROOT . "commons/images/poweredby/poweredby.png");
                define('BRAND_POWERED_INVERSE', WEBROOT . "commons/images/poweredby/poweredby_inverse.png");
                break;
            default :
                define('BRAND_LOGO', WEBROOT . 'commons/images/poweredby/WeWeb-PoweredBy.png');
                define('BRAND_URL', "//www.weweb.al");
                define('BRAND_FAVICON', WEBROOT . "commons/images/weweb.ico");
                break;
        }

        define('FTP_HOST', $site['ftp_host']);
        define('FTP_USER', $site['ftp_user']);
        define('FTP_PASS', $site['ftp_pass']);
        //define('DEVELOPMENT_ENVIRONMENT', ($site['dev_mode'] == 1) ? true : false);

        define('MAX_PAGES', ($site['max_pages'] != 0) ? $site['max_pages'] : 1000);
        define('MAX_SUBMENUS', $site['max_submenus']);
        define('QUOTA_WEB', $site['quota_web']);

        define('FB_APPID', '1726438044257214');
        //define('CMS_LOGO', '/data/media/cmslogo.png');
        define('DEFAULT_DATA_DIR', "data" . DS . 'cmsv2.bluehat.al' . DS);
        define('DEFAULT_MEDIA_ROOT', "data" . DS . 'cmsv2.bluehat.al' . DS . 'media' . DS);

        define('TABLE_PREFIX', 'cms_');
    }



function getSiteData($wwwdomain) {
    $domain = (strpos($wwwdomain, 'www.') === 0) ? substr($wwwdomain, 4) : $wwwdomain;
        $qsel = "SELECT
                        `Website`.`domain`,
                        `Website`.`db_host`,
                        `Website`.`db_user`,
                        `Website`.`db_pass`,
                        `Website`.`db_name`,
                        `Website`.`ftp_host`,
                        `Website`.`ftp_user`,
                        `Website`.`ftp_pass`,
                        `Website`.`data_dir`,
                        `Website`.`dev_mode`,
                        `Website`.`expire_date`,
                        `Website`.`enabled`,
                        `Profile`.`max_pages`,
                        `Profile`.`max_submenus`,
                        `Profile`.`quota_web`,
                        `Client`.`id_brand`
                    FROM `serv_db`.`Website`
                    LEFT JOIN `serv_db`.`Profile` ON(`Profile`.`id` = `Website`.`id_profile`)
                    LEFT JOIN `serv_db`.`Client` ON(`Client`.`id` = `Website`.`id_client`)
                    WHERE `Website`.`domain` LIKE '$wwwdomain' OR `Website`.`domain` LIKE '" . $domain . "' LIMIT 1;
                        ";

    $res = mysql_query($qsel);
    if(mysql_num_rows($res)) {
        return mysql_fetch_array($res);
    } else {
        return false;
    }
}